#
# Dockerfile for json-server
#

FROM alpine

WORKDIR /data

RUN set -xe \
    && apk add --no-cache nodejs nodejs-npm \
    && npm install -g json-server \
    && rm -rf /tmp/npm* /var/cache/apk/*

EXPOSE 3001

ENTRYPOINT ["json-server"]
CMD ["--help"]