#!/usr/bin/env bash
if [[ "${DJANGO_DEBUGGER}" == "True" ]]; then
  /usr/local/bin/wait-for-it.sh db:5432 -t 30 && python -m ptvsd --host 0.0.0.0 --port 3000 --wait manage.py runserver --noreload --nothreading 0.0.0.0:8000
else
  # /usr/local/bin/wait-for-it.sh db:5432 -t 30 && python /code/collectstatic.py && python /code/manage.py makemigrations && python /code/manage.py migrate --noinput && gunicorn audit.wsgi -c /code/gunicorn_config.py
  /usr/local/bin/wait-for-it.sh db:5432 -t 30 && gunicorn audit.wsgi -c /code/gunicorn_config.py
fi

