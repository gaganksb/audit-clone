import os

is_s3 = False
with open(".env", "r") as f:
    line = f.readline()
    while line:
        line = f.readline()
        key = line.strip().split("=")[0]
        if key == "ENV":
            value = line.strip().split("=")[1]
            if value == "s3":
                is_s3 = True
                break
if not is_s3:
    os.system("python manage.py collectstatic --noinput")
else:
    print("collectstatic not executed to preserve s3 resources")
