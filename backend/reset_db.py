import os


def main():
    os.system("python manage.py reset_db")
    os.system("python manage.py makemigrations")
    os.system("python manage.py data_import")
    os.system("python manage.py loaddata --app common initial.json")
    os.system("python manage.py loaddata --app auditing initial.json")
    os.system("python manage.py loaddata --app field initial.json")
    os.system("python manage.py loaddata --app partner initial.json")
    os.system("python manage.py loaddata --app project initial.json")
    os.system("python manage.py loaddata --app unhcr initial.json")
    os.system("python manage.py fakedata")
    # os.system('python manage.py import_master_template')
    os.system("python manage.py import_field_member_office")


if __name__ == "__main__":
    main()
