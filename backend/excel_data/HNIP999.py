import xlrd
import csv
from import_utils import xlsx_to_csv

DATA_INDEX = [
    {"index": 0, "name": "BUSINESS_UNIT", "type": "string"},
    {"index": 1, "name": "DESCR", "type": "string"},
    {"index": 2, "name": "HCR_SA_ID", "type": "agreement_number"},
    {"index": 3, "name": "HCR_SA_DATE", "type": "date"},
    {"index": 4, "name": "XLATLONGNAME", "type": "string"},
    {"index": 5, "name": "DESCR100", "type": "string"},
    {"index": 6, "name": "TREE_NODE_START", "type": "string"},
    {"index": 7, "name": "DESCR_1", "type": "string"},
    {"index": 8, "name": "TREE_NODE", "type": "string"},
    {"index": 9, "name": "DESCR1", "type": "string"},
    {"index": 10, "name": "TREE_NODE_1", "type": "string"},
    {"index": 11, "name": "DESCR3", "type": "string"},
    {"index": 12, "name": "HCR_OPERATION_CODE", "type": "string"},
    {"index": 13, "name": "HCR_PILLAR", "type": "string"},
    {"index": 14, "name": "HCR_PILLAR_DESC", "type": "string"},
    {"index": 15, "name": "BUDGET_REF", "type": "string"},
    {"index": 16, "name": "HCR_CC_LIST", "type": "array"},
    {"index": 17, "name": "HCR_GOALS_LIST", "type": "array"},
    {"index": 18, "name": "HCR_PPG_LIST", "type": "array"},
    {"index": 19, "name": "HCR_SIT_LIST", "type": "array"},
    {"index": 20, "name": "OPERATING_UNIT", "type": "string"},
    {"index": 21, "name": "HCR_DESCR", "type": "string"},
    {"index": 22, "name": "PARENT_NODE_NAME", "type": "string"},
    {"index": 23, "name": "HCR_SA_START_DT", "type": "date"},
    {"index": 24, "name": "HCR_SA_COMPL_DT", "type": "date"},
    {"index": 25, "name": "POSTED_BASE_AMT", "type": "number"},
    {"index": 26, "name": "BASE_CURRENCY", "type": "string"},
    {"index": 27, "name": "HCR_IP_INSTALL_AMT", "type": "number"},
    {"index": 28, "name": "HCR_IP_ADJUST_AMT", "type": "number"},
    {"index": 29, "name": "HCR_IP_CANCEL_AMT", "type": "number"},
    {"index": 30, "name": "HCR_IP_IPFR_AMT", "type": "number"},
    {"index": 31, "name": "HCR_IP_REFUND_AMT", "type": "number"},
    {"index": 32, "name": "AMOUNT_1", "type": "number"},
    {"index": 33, "name": "AMOUNT_2", "type": "number"},
    {"index": 34, "name": "HCR_IP_RECV_AMT", "type": "number"},
    {"index": 35, "name": "HCR_IP_WRITOFF_AMT", "type": "number"},
    {"index": 36, "name": "HCR_SUM", "type": "number"},
    {"index": 37, "name": "NAME1", "type": "string"},
    {"index": 38, "name": "ADDRESS1", "type": "string"},
    {"index": 39, "name": "ADDRESS2", "type": "string"},
    {"index": 40, "name": "CITY", "type": "string"},
    {"index": 41, "name": "COUNTRY", "type": "string"},
    {"index": 42, "name": "HCR_FMIS_START_DT", "type": "date"},
    {"index": 43, "name": "HCR_FMIS_SUBMIT_DT", "type": "date"},
    {"index": 44, "name": "XLATLONGNAME_1", "type": "string"},
    {"index": 45, "name": "COMMENT_TEXT", "type": "string"},
    {"index": 46, "name": "HCR_FCS_COMMENTS", "type": "string"},
    {"index": 47, "name": "AUDIT_DATE", "type": "date"},
    {"index": 48, "name": "HCR_SA_STATUS_CL", "type": "string"},
    {"index": 49, "name": "VENDOR_ID", "type": "string"},
]

ROOT_DATA = "/code/excel_data/HNIP999/"
xlsx_files = [
    ROOT_DATA + "HNIP999_2014.xlsx",
    ROOT_DATA + "HNIP999_2015.xlsx",
    ROOT_DATA + "HNIP999_2016.xlsx",
    ROOT_DATA + "HNIP999_2017.xlsx",
    ROOT_DATA + "HNIP999_2018.xlsx",
    ROOT_DATA + "HNIP999_2019.xlsx",
]
csv_path = ROOT_DATA + "HNIP999.csv"
SHEET = "Sheet 1"


def main():
    xlsx_to_csv(xlsx_files, csv_path, SHEET, DATA_INDEX)
    print("HNIP999 Converted to CSV")


if __name__ == "__main__":
    main()
