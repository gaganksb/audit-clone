drop table if exists excel_alltest;

create table excel_alltest
(
  id serial NOT NULL,
  implementer character varying(50),
  description character varying(50),
  implementer_type character varying(50),
  agreement_number character varying(50),
  agreement_type character varying(50),
  procurement NUMERIC (10, 2),
  cash NUMERIC (10, 2),
  staff_costs NUMERIC (10, 2),
  other_accounts NUMERIC (10, 2),
  budget NUMERIC (10, 2),
  CONSTRAINT excel_alltest_pkey PRIMARY KEY (id)
);

COPY excel_alltest(
  implementer,
  description,
  implementer_type,
  agreement_number,
  agreement_type,
  procurement,
  cash,
  staff_costs,
  other_accounts,
  budget
)
FROM '/home/AllTest.csv' DELIMITER ',' CSV HEADER force null 
  implementer,
  description,
  implementer_type,
  agreement_number,
  agreement_type,
  procurement,
  cash,
  staff_costs,
  other_accounts,
  budget;

