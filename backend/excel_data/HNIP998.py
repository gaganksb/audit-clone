import xlrd
import csv
from import_utils import xlsx_to_csv

DATA_INDEX = [
    {"index": 0, "name": "BUSINESS_UNIT", "type": "string"},
    {"index": 1, "name": "DESCR", "type": "string"},
    {"index": 2, "name": "HCR_SA_ID", "type": "agreement_number"},
    {"index": 3, "name": "XLATLONGNAME", "type": "string"},
    {"index": 4, "name": "HCR_OPERATION_CODE", "type": "string"},
    {"index": 5, "name": "BUDGET_REF", "type": "string"},
    {"index": 6, "name": "OPERATING_UNIT", "type": "string"},
    {"index": 7, "name": "HCR_DESCR", "type": "string"},
    {"index": 8, "name": "PARENT_NODE_NAME", "type": "string"},
    {"index": 9, "name": "HCR_PILLAR", "type": "string"},
    {"index": 10, "name": "ACCOUNT", "type": "string"},
    {"index": 11, "name": "CLASS_FLD", "type": "string"},
    {"index": 12, "name": "HCR_CC", "type": "string"},
    {"index": 13, "name": "HCR_GOAL", "type": "string"},
    {"index": 14, "name": "PRODUCT", "type": "string"},
    {"index": 15, "name": "CHARTFIELD1", "type": "string"},
    {"index": 16, "name": "CHARTFIELD2", "type": "string"},
    {"index": 17, "name": "POSTED_BASE_AMT", "type": "number"},
    {"index": 18, "name": "DATE_TO", "type": "date"},
]

ROOT_DATA = "/code/excel_data/HNIP998/"
xlsx_files = [
    ROOT_DATA + "HNIP998_2014.xlsx",
    ROOT_DATA + "HNIP998_2015.xlsx",
    ROOT_DATA + "HNIP998_2016.xlsx",
    ROOT_DATA + "HNIP998_2017.xlsx",
    ROOT_DATA + "HNIP998_2018.xlsx",
    ROOT_DATA + "HNIP998_2019.xlsx",
]
csv_path = ROOT_DATA + "HNIP998.csv"
SHEET = "Sheet 1"


def main():
    xlsx_to_csv(xlsx_files, csv_path, SHEET, DATA_INDEX)
    print("HNIP998 Converted to CSV")


if __name__ == "__main__":
    main()
