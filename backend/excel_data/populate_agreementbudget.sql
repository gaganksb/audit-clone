--POPULATE project_agreementbudget
insert into project_agreementbudget(
	"year",
	"output",
	budget,
	report_date,
    account_id,
	agreement_id,
	cost_center_id,
	goal_id,
	population_planning_group_id,
	situation_id
)
	(select 
		h.budget_ref::int,
		h.class_fld,
		h.posted_base_amt,
		h.date_to,
		ac.id account_id,
		ag.id agreement_id,
		cc.id cost_center_id,
		g.id goal_id,
		ppg.id population_planning_group_id,
		ps.id situation_id
		from excel_hnip998 h
		full outer join project_agreement ag on h.hcr_sa_id = ag.number
		full outer join partner_partner pa on ag.partner_id = pa.id
		full outer join common_costcenter cc on h.hcr_cc = cc.code
		full outer join project_goal g on h.hcr_goal = g.code
		full outer join project_populationplanninggroup ppg on h.product = ppg.code
		full outer join project_situation ps on h.chartfield1 = ps.code
		full outer join common_businessunit bu on h.business_unit = bu.code
		full outer join project_account ac on h.account = ac.code
		where h.budget_ref is not null and 
			bu.id = ag.business_unit_id and 
			h.hcr_sa_id = ag.number);