/*
--POPULATE common_area
insert into common_area(code, name)
	(select distinct tree_node_start, descr_1 from excel_hnip999 where tree_node_start is not null);

--POPULATE common_currency
insert into common_currency(code)
	(select distinct base_currency from excel_hnip999 where base_currency is not null);

--POPULATE common_region
insert into common_region(code, name, area_id)
	(select distinct h.tree_node, h.descr1, a.id
		from excel_hnip999 h 
		left join common_area a on h.tree_node_start = a.code
		where tree_node is not null);

--POPULATE common_country
insert into common_country(code, name)
	(select distinct alpha_3, description from excel_countries);

--POPULATE common_businessunit
insert into common_businessunit(code, name)
	(select distinct business_unit, descr from excel_hnip999 where business_unit is not null);

--POPULATE common_businessunit_countries


--POPULATE common_costcenter
insert into common_costcenter(code)
	(select distinct hcr_cc from excel_hnip998 where hcr_cc is not null);
*/

--POPULATE partner_implementer_type
insert into partner_implementertype(implementer_type) 
	(select distinct parent_node_name from excel_hnip998 where parent_node_name is not null);


--POPULATE partner_partner
insert into partner_partner(legal_name, "number", implementer_type_id)
	(select distinct h.hcr_descr, h.operating_unit, imp.id
		from excel_hnip999 h 
		full outer join partner_implementertype imp on h.parent_node_name = imp.implementer_type
		where h.hcr_descr is not null);


--POPULATE project_cycle
-- insert into project_cycle(year, status_id) values ('2018', 1);


--POPULATE project_selectionsettings
-- insert into project_selectionsettings(
-- 	cycle_id, 
-- 	procurement_perc, 
-- 	cash_perc, 
-- 	staff_cost_perc, 
-- 	emergency_perc, 
-- 	audit_perc, 
-- 	oios_perc,
-- 	risk_threshold) values (1, 0.2, 0.2, 0.2, 0.1, 0, 0, 2.0);


--POPULATE project_preliminarylist
-- insert into project_preliminarylist(
-- 	cycle_id,
-- 	agreement_id,
-- 	cash,
-- 	other,
-- 	procurement,
-- 	staff_cost,
-- 	budget_usd
-- )
-- 	(select 
-- 		cy.id cycle_id,
-- 		ag.id agreement_id,
-- 		al.cash,
-- 		al.other_accounts,
-- 		al.procurement,
-- 		al.staff_costs,
-- 		al.budget
-- 		from excel_alltest al
-- 		join project_agreement ag on al.agreement_number = ag.number
-- 		join partner_partner pa on ag.partner_id = pa.id
-- 		join project_cycle cy on cy.id = 1
-- 		where al.implementer = pa.number);


