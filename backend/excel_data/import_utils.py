import xlrd
import csv


def check(item, index, rownum, file_counter, DATA_INDEX):
    data_type = DATA_INDEX[index]["type"]
    if file_counter == 0 and rownum == 0:
        return item
    if file_counter != 0 and rownum == 0:
        return None
    result = item
    if rownum != 0:
        if data_type == "date":
            if item.strip() == "":
                return ""
        elif data_type == "number":
            if type(item) is not float:
                return ""
            else:
                result = round(item, 2)
        elif data_type == "string":
            result = item.strip()
        elif data_type == "array":
            result = '{"'
            array = item.split(",")
            for i in range(len(array)):
                result = result + array[i].strip() + '","'
            result = result[:-2] + "}"  # '{"item1","item2"}'
        elif data_type == "agreement_number":
            result = item.strip().lstrip("0")
        elif data_type == "false_float":
            if type(item) is float:
                result = str(int(item)).strip()
            else:
                result = item.strip()
        else:
            pass
    return result


def etl(row, rownum, file_counter, DATA_INDEX):
    result = []
    for i in range(len(row)):
        result.append(check(row[i], i, rownum, file_counter, DATA_INDEX))
    return result


def xlsx_to_csv(xlsx_files, csv_path, SHEET, DATA_INDEX):
    csv_file = open(csv_path, "w+")
    writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
    file_counter = 0
    for xlsx_path in xlsx_files:
        workbook = xlrd.open_workbook(xlsx_path)
        sheet = workbook.sheet_by_name(SHEET)
        for rownum in range(sheet.nrows):
            row = etl(sheet.row_values(rownum), rownum, file_counter, DATA_INDEX)
            writer.writerow(row)
        file_counter = file_counter + 1
    csv_file.close()


def xlsx_to_csv_range(
    xlsx_files, csv_path, SHEET, DATA_INDEX, start_col, start_row, end_col, end_row
):
    csv_file = open(csv_path, "w+")
    writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
    file_counter = 0
    for xlsx_path in xlsx_files:
        workbook = xlrd.open_workbook(xlsx_path)
        sheet = workbook.sheet_by_name(SHEET)
        rownum = 0
        for _ in range(start_row, end_row + 1):
            row = etl(
                sheet.row_values(_, start_colx=start_col, end_colx=end_col + 1),
                rownum,
                file_counter,
                DATA_INDEX,
            )
            rownum = rownum + 1
            writer.writerow(row)
        file_counter = file_counter + 1
    csv_file.close()
