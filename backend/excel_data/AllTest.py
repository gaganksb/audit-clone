import xlrd
import csv
from import_utils import xlsx_to_csv_range

DATA_INDEX = [
    {"index": 0, "name": "Implementer", "type": "false_float"},
    {"index": 1, "name": "Description", "type": "string"},
    {"index": 2, "name": "Implementer Type", "type": "string"},
    {"index": 3, "name": "IP Agreement Number", "type": "false_float"},
    {"index": 4, "name": "Agreement Type", "type": "string"},
    {"index": 5, "name": "Procurement", "type": "number"},
    {"index": 6, "name": "Cash", "type": "number"},
    {"index": 7, "name": "Staff Costs", "type": "number"},
    {"index": 8, "name": "Other Accounts", "type": "number"},
    {"index": 9, "name": "Budget (Agreement)", "type": "number"},
]

ROOT_DATA = "/code/excel_data/"
xlsx_files = [
    ROOT_DATA + "Selection.xlsx",
]
csv_path = ROOT_DATA + "AllTest.csv"
SHEET = "All Test"
start_col = 8
start_row = 2
end_col = 17
end_row = 1611


def main():
    xlsx_to_csv_range(
        xlsx_files, csv_path, SHEET, DATA_INDEX, start_col, start_row, end_col, end_row
    )
    print("All Test Converted to CSV")

    # workbook = xlrd.open_workbook(xlsx_files[0])
    # print(workbook.sheet_names())


if __name__ == "__main__":
    main()
