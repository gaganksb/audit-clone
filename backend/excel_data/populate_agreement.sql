--POPULATE field_fieldassessment

alter table field_fieldassessment add excel_id int;

insert into field_fieldassessment(
	business_unit_id,
	partner_id,
	score_id,
	is_remind,
	excel_id
) (select 
		bu.id business_unit_id,
		pa.id partner_id,
		4 as score_id,
		false as is_remind,
		h.id
		from excel_hnip999 h 
		full outer join common_businessunit bu on h.business_unit = bu.code
		full outer join partner_partner pa on h.operating_unit = pa.number
		where h.hcr_sa_id is not null
		order by bu.id);


--POPULATE project_agreement
insert into project_agreement(
	title,
	agreement_date,
	"number",
	operation,
	start_date,
	end_date,
	budget,
	installments_paid,
	agreement_type_id,
	business_unit_id,
	country_id,
	partner_id,
	pillar_id,
	field_assessment_id,
	budget_ref
)
	(select 
		h.descr100, 
		h.hcr_sa_date, 
		h.hcr_sa_id, 
		h.hcr_operation_code, 
		h.hcr_sa_start_dt,
		h.hcr_sa_compl_dt,
		h.posted_base_amt,
		h.hcr_ip_install_amt,
		agty.id agreement_type_id,
		bu.id business_unit_id,
		co.id country_id,
		pa.id partner_id,
		pil.id pillar_id,
		fa.id field_assessment_id,
		cast (h.budget_ref as integer)
		from excel_hnip999 h 
		full outer join project_agreementtype agty on h.xlatlongname = agty.description
		full outer join common_businessunit bu on h.business_unit = bu.code
		full outer join common_country co on h.tree_node_1 = co.code
		full outer join partner_partner pa on h.operating_unit = pa.number
		full outer join project_pillar pil on h.hcr_pillar = pil.code
		full outer join field_fieldassessment fa
		    on bu.id = fa.business_unit_id and pa.id = fa.partner_id and h.id = fa.excel_id
		where h.hcr_sa_id is not null);


alter table field_fieldassessment drop column excel_id;

--POPULATE project_agreementreport
insert into project_agreementreport(
	report_date,
	installments,
	adjustments,
	cancellations,
	reported_by,
	refund,
	receivables,
	write_off,
	agreement_id,
	currency_id
)
	(select 
		h.hcr_sa_date,
		h.hcr_ip_install_amt,
		h.hcr_ip_adjust_amt,
		h.hcr_ip_cancel_amt,
		h.hcr_ip_ipfr_amt,
		h.hcr_ip_refund_amt,
		h.hcr_ip_recv_amt,
		h.hcr_ip_writoff_amt,
		ag.id agreement_id,
		cc.id currency_id
		from excel_hnip999 h
		full outer join project_agreement ag on h.hcr_sa_id = ag.number
		full outer join partner_partner pa on ag.partner_id = pa.id
		full outer join common_businessunit bu on h.business_unit = bu.code
		full outer join common_currency cc on h.base_currency = cc.code
		full outer join common_country co on h.tree_node_1 = co.code
		where h.operating_unit = pa.number and bu.id = ag.business_unit_id and co.id = ag.country_id);

--POPULATE remaining project_account rows
insert into project_account(code) 
	(select distinct account from excel_hnip998 
		where account not in (select code from project_account) and account is not null);
