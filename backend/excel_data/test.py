import xlrd

ROOT_DATA = "/code/excel_data/"
xlsx_files = [
    ROOT_DATA + "Selection.xlsx",
]
csv_path = ROOT_DATA + "Selection.csv"

SHEET = "Analysis Stats 2018- NFF-Prelim"

xlsx_path = xlsx_files[0]

workbook = xlrd.open_workbook(xlsx_path)
sheet = workbook.sheet_by_name(SHEET)


def get_cell_range(start_col, start_row, end_col, end_row):
    return [
        sheet.row_values(row, start_colx=start_col, end_colx=end_col + 1)
        for row in range(start_row, end_row + 1)
    ]


data = get_cell_range(6, 15, 10, 27)  # A16 E28
print(data)
