from django.db import models
from django.apps import apps
from library.Manage_Notification import NotificationHandler
from copy import deepcopy
from django.utils import timezone


class AbstractModel(models.Model):
    created_date = models.DateTimeField(null=True, default=timezone.now)
    last_modified_date = models.DateTimeField(null=True, default=timezone.now)
    user = models.ForeignKey(
        "account.User",
        null=True,
        related_name="%(app_label)s_%(class)s_related",
        default=None,
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        self.notification_model = apps.get_model("notify.Notification")
        self.notification_handler = NotificationHandler
        super().__init__(*args, **kwargs)

    def send_mail(self, **kwargs):
        kwargs["instance"] = self.__dict__
        kwargs["model"] = type(self)
        kwargs["updated_model"] = "{}_{}".format(
            self._meta.app_label, self._meta.model_name
        )

    def save(self, *args, **kwargs):
        old_instance = type(self).objects.filter(pk=self.id).first()
        new_kwargs = deepcopy(kwargs)
        new_kwargs.update({"old_instance": old_instance})
        new_kwargs = deepcopy(kwargs)
        new_kwargs.update({"old_instance": old_instance})
        super().save()

    def update(self, *args, **kwargs):
        old_instance = type(self).objects.filter(pk=self.id).first()
        new_kwargs = deepcopy(kwargs)
        new_kwargs.update({"old_instance": old_instance})
        new_kwargs = deepcopy(kwargs)
        new_kwargs.update({"old_instance": old_instance})
        super().save(*args, **kwargs)
