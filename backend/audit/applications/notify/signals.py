from django.dispatch import receiver
from django.db.models.signals import post_save
from notify.models import NotifiedUser
from common.consts import NOTIFICATION_FREQUENCY_CHOICES

# @receiver(post_save, sender=NotifiedUser)
# def new_notification(sender, instance, **kwargs):
#     if instance.recipient.profile.notification_frequency == NOTIFICATION_FREQUENCY_CHOICES.immediate:
#         from notify.helpers import send_immediate_notification
#         user_id = instance.recipient.id
#         send_immediate_notification(user_id)
