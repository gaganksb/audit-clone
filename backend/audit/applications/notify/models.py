# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from audit.core.abstract_model import AbstractModel

from common.database_fields import FixedTextField
from notify.consts import NotificationType

from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from common.consts import NOTIFICATION_FREQUENCY_CHOICES


class Notification(AbstractModel):
    name = models.CharField(max_length=255)
    description = models.TextField()
    source = FixedTextField(choices=NotificationType.get_choices())
    content_type = models.ForeignKey(
        ContentType, null=True, blank=True, on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey("content_type", "object_id")

    class Meta:
        ordering = ("-created_date",)

    def __str__(self):
        return "Notification {}".format(self.name)


class NotifiedUser(AbstractModel):
    notification = models.ForeignKey(
        Notification, related_name="notified", on_delete=models.CASCADE
    )
    did_read = models.BooleanField(default=False)
    sent_as_email = models.BooleanField(default=False)
    recipient = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="notified", on_delete=models.CASCADE
    )

    class Meta:
        ordering = ("-created_date",)
        unique_together = ("notification", "recipient")

    def __str__(self):
        return "NotifiedUser <{}>".format(self.id)
