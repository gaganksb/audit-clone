class NotificationType(object):
    REMIND_FIELD_OFFICE = "remind_field_office"
    CUSTOM_MESG = "custom_msg"
    USER_INVITATION = "user_invitation"
    AUDIT_FIRM_SELECTION = "audit_firm_selection"
    AGREEMENT_MEMBER_SELECTION = "agreement_member_selection"
    PARTNER_AUDIT_SELECTION = "partner_audit_selection"
    PROJECT_SELECTION_STARTED = "project_selection_started"
    FIELD_ASSESSMENT_ENDED = "field_assessment_ended"
    FINALIZATION_OF_PRELIMINARY_LIST = "finalization_of_preliminary_list"
    END_OF_INTERIM_HQ = "end_of_interim_hq"
    END_OF_INTERIM_FO = "end_of_interim_fo"
    END_OF_INTERIM_PARTNER = "end_of_interim_partner"
    END_OF_FINAL_HQ = "end_of_final_list_hq"
    END_OF_FINAL_PARTNER = "end_of_final_list_partner"
    END_OF_AUDIT_FIRM_ASSIGNMENT = "end_of_audit_firm_assignment"
    NOTIFICATION_OF_ASSIGNMENT_OF_AUDITOR_REVIEWER = (
        "notification_of_assignment_of_auditor_reviewer"
    )
    NOTIFICATION_OF_ASSIGNMENT_OF_AUDITOR_REVIEWER_TO_PPA_WORKGROUP = (
        "notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup"
    )
    REMIND_INTERIM_LIST = "remind_interim_list"
    FIELD_ASSESSMENT_COMPLETED_REVIEWER = "field_assessment_completed_reviewer"
    INTERIM_COMPLETED_REVIEWER = "interim_completed_reviewer"
    MGMT_LETTER_ENABLED = "mgmt_letter_enabled"
    MGMT_LETTER_PUBLISH = "mgmt_letter_publish"
    AGREEMENT_LIST_EXPORT = "agreement_list_export"
    FIELDASSESSMENT_EXPORT = "fieldassessment_export"
    AGREEMENT_SURVEY = "agreement_survey"
    TEAMCOMPOSITION_NOTIFICATION = "remind_teamcomposition_auditor"
    AUDITOR_SURVEY_NOTIFY = "remind_auditor_for_survey"
    AUDITOR_QUALITY_NOTIFY_FINAL = "remind_KPI2_auditor_final"
    AUDITOR_QUALITY_NOTIFY_SUMMARY = "remind_KPI2_auditor_summary"

    @classmethod
    def get_choices(cls):
        return [(getattr(cls, name), name) for name in dir(cls) if name.isupper()]


NOTIFICATION_DATA = {
    NotificationType.REMIND_FIELD_OFFICE: {
        "template_name": "remind_field_office",
        "subject": "Remind Field Office",
    },
    NotificationType.CUSTOM_MESG: {
        "template_name": "custom_msg",
        "subject": "custom_subject",
    },
    NotificationType.USER_INVITATION: {
        "template_name": "user_invitation",
        "subject": "Invitation",
    },
    NotificationType.AUDIT_FIRM_SELECTION: {
        "template_name": "audit_firm_selection",
        "subject": "Audit Firm Selection",
    },
    NotificationType.AGREEMENT_MEMBER_SELECTION: {
        "template_name": "agreement_member_selection",
        "subject": "Agreement Member Selection",
    },
    NotificationType.PARTNER_AUDIT_SELECTION: {
        "template_name": "partner_audit_selection",
        "subject": "Partner Audit Selection",
    },
    NotificationType.PROJECT_SELECTION_STARTED: {
        "template_name": "project_selection_started",
        "subject": "Project Selection Started",
    },
    NotificationType.FIELD_ASSESSMENT_ENDED: {
        "template_name": "field_assessment_ended",
        "subject": "Field Assessment Ended",
    },
    NotificationType.FINALIZATION_OF_PRELIMINARY_LIST: {
        "template_name": "finalization_of_preliminary_list",
        "subject": "Finalization of Preliminary List",
    },
    NotificationType.END_OF_INTERIM_HQ: {
        "template_name": "end_of_interim_hq",
        "subject": "End of Interim HQ",
    },
    NotificationType.END_OF_INTERIM_FO: {
        "template_name": "end_of_interim_fo",
        "subject": "End of Interim FO",
    },
    NotificationType.END_OF_INTERIM_PARTNER: {
        "template_name": "end_of_interim_partner",
        "subject": "End of Interim Partner",
    },
    NotificationType.END_OF_FINAL_HQ: {
        "template_name": "end_of_final_list_hq",
        "subject": "End of Final List HQ",
    },
    NotificationType.END_OF_FINAL_PARTNER: {
        "template_name": "end_of_final_list_partner",
        "subject": "End of Final List Partner",
    },
    NotificationType.END_OF_AUDIT_FIRM_ASSIGNMENT: {
        "template_name": "end_of_audit_firm_assignment",
        "subject": "End of Audit Firm Assignment",
    },
    NotificationType.NOTIFICATION_OF_ASSIGNMENT_OF_AUDITOR_REVIEWER: {
        "template_name": "notification_of_assignment_of_auditor_reviewer",
        "subject": "Notification of Assignment of Auditor Reviewer to a PPA",
    },
    NotificationType.REMIND_INTERIM_LIST: {
        "template_name": "remind_interim_list",
        "subject": "Remind Interim List",
    },
    NotificationType.FIELD_ASSESSMENT_COMPLETED_REVIEWER: {
        "template_name": "field_assessment_completed_reviewer",
        "subject": "Field Assessment Completed From Reviewer",
    },
    NotificationType.INTERIM_COMPLETED_REVIEWER: {
        "template_name": "interim_completed_reviewer",
        "subject": "Interim List Completed From FO Reviewer",
    },
    NotificationType.NOTIFICATION_OF_ASSIGNMENT_OF_AUDITOR_REVIEWER_TO_PPA_WORKGROUP: {
        "template_name": "notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup",
        "subject": "Notification Of Assignment Of Auditor Reviewer To PPA Workgroup",
    },
    NotificationType.MGMT_LETTER_ENABLED: {
        "template_name": "mgmt_letter_enabled",
        "subject": "Management Letter is enabled notification",
    },
    NotificationType.MGMT_LETTER_PUBLISH: {
        "template_name": "mgmt_letter_publish",
        "subject": "Management Letter publish notification",
    },
    NotificationType.AGREEMENT_LIST_EXPORT: {
        "template_name": "agreement_list_export",
        "subject": "Agreement List Export",
    },
    NotificationType.FIELDASSESSMENT_EXPORT: {
        "template_name": "fieldassessment_export",
        "subject": "Field Assessment Export",
    },
    NotificationType.AGREEMENT_SURVEY: {
        "template_name": "agreement_survey",
        "subject": "Agreement Survey",
    },
    NotificationType.TEAMCOMPOSITION_NOTIFICATION: {
        "template_name": "remind_teamcomposition_auditor",
        "subject": "Team Composition Notification",
    },
    NotificationType.AUDITOR_SURVEY_NOTIFY: {
        "template_name": "remind_auditor_for_survey",
        "subject": "Auditor Notification for Survey",
    },
    NotificationType.AUDITOR_QUALITY_NOTIFY_FINAL: {
        "template_name": "remind_KPI2_auditor_final",
        "subject": "Reminder for the Audit Quality",
    },
    NotificationType.AUDITOR_QUALITY_NOTIFY_SUMMARY: {
        "template_name": "remind_KPI2_auditor_summary",
        "subject": "Reminder for the Audit Quality Summary",
    },
}
