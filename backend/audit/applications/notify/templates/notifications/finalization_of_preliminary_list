<p>This message is to inform you about the preliminary selected {{year}} projects for audit.</p>
<p>As usual and according to the Audit Policy, this list is to be validated with the field operations/country offices.
    In consultation with your management, we will appreciate your feedback on the list on the following: </p>
<ol>
    <li>You are kindly requested to review the list for the projects under your purview and indicate if there is any
        project not listed but that you would like to have audited. Such additional project could include, regardless of
        the project values, earmarked projects, or resulting from risk based observations and assessments during your
        verification and monitoring activities. You may also suggest removing a project from the list if there are
        justified reasons (for example if there are plans to reduce considerably the level of budgets/instalments).
        Hence, under either of the scenarios (request to add or to remove a project), please provide a brief description
        of the reasons for each project added or removed. </li>
    <li>Moreover, please provide details related to the <strong>location of projects documents for audit, <u>(i.e. where auditor go for the field audit) and the location of implementation sites
                (i.e. where activities are performed)</u> </strong>. In case the details for focal persons need to be
        adjusted,
        please provide the
        required updates. </li>


    <li>For selected projects of govt partners, if the audit must be conducted by the <u><strong>government auditors
            </strong>(i.e. Auditor
            General for the concerned country)</u>, please provide this note/confirmation so that such audits are not
        commissioned
        to the external audit firms but assigned to the local government auditors.</li>
</ol>
<p>For more details on the above procedure, <a
        href="https://unpp-prod.s3-eu-west-1.amazonaws.com/UNHCR+Integrity+and+Assurance+Module+-+User+Guide+for+Project+Selection+for+Audit.pdf">
        please visit </a></p>
<p>In case of questions, please contact the IMAS team at <a href="mailto:epartner@unhcr.org" target="_blank"
        rel="nofollow noopener">epartner@unhcr.org</a></p>
<p>Thank you very much for your usual support and cooperation.</p>
<p>Best regards,</p>
<p>IMAS Team</p>