from django.urls import path

from .views import (
    NotificationsAPIView,
    NotificationAPIView,
)

urlpatterns = [
    path("", NotificationsAPIView.as_view(), name="notifications"),
    path("<int:pk>/", NotificationAPIView.as_view(), name="notification"),
]
