import json
import django_filters
from django_filters import FilterSet
from django.db import models
from django.apps import apps
from django.db.models import Q
from .models import Notification, NotifiedUser
from auditing.models import AuditWorksplitGroup
from django_filters.filters import (
    CharFilter,
    NumberFilter,
    DateFilter,
    DateRangeFilter,
    BooleanFilter,
)
from django.db.models import (
    F,
    ExpressionWrapper,
    DecimalField,
    Subquery,
    Q,
    Case,
    When,
    Value,
    BooleanField,
    CharField,
    Exists,
    Count,
    OuterRef,
)
from rest_framework import exceptions


class NotificationFilter(FilterSet):

    start = DateFilter(method="filter_start")
    end = DateFilter(method="filter_end")
    did_read = BooleanFilter(field_name="did_read", lookup_expr="exact")

    def filter_start(self, queryset, name, value):
        return queryset.filter(created_date__gte=value)

    def filter_end(self, queryset, name, value):
        return queryset.filter(created_date__lte=value)

    class Meta:
        models = NotifiedUser
        fields = ["did_read", "start", "end"]
