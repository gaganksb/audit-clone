# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import get_object_or_404
from rest_framework import status as statuses
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from common.pagination import TinyResultSetPagination, SmallPagination
from notify.serializers import (
    NotifiedUserSerializer,
    NotificationSerializer,
    NotifiedSerializer,
)
from notify.models import NotifiedUser, Notification
from notify.permissions import IsNotifiedOwner
from .filters import NotificationFilter


class NotificationsAPIView(ListAPIView):

    permission_classes = (IsAuthenticated,)
    serializer_class = NotifiedUserSerializer
    pagination_class = SmallPagination
    filter_backends = (DjangoFilterBackend,)
    filter_class = NotificationFilter

    def get_queryset(self):
        tiny_pagination = self.request.query_params.get("tiny_pagination", None)
        if tiny_pagination is not None:
            self.pagination_class = TinyResultSetPagination
        return (
            NotifiedUser.objects.select_related("notification")
            .filter(recipient=self.request.user)
            .order_by("created_date")
        )

    def patch(self, request, *args, **kwargs):
        serializer = NotifiedSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=statuses.HTTP_400_BAD_REQUEST)

        if serializer.data["mark_all_as_read"]:
            count = NotifiedUser.objects.filter(
                recipient=self.request.user, did_read=False
            ).update(did_read=True)
            msg = "Marked as read ({}) objects.".format(count)
        else:
            msg = "No objects marked as read."
        return Response(msg)


class NotificationAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsNotifiedOwner)
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()

    def patch(self, request, pk, *args, **kwargs):
        notified = get_object_or_404(
            NotifiedUser.objects.select_related("notification"),
            notification_id=pk,
            recipient=request.user,
        )
        if "did_read" in request.data and request.data["did_read"] == True:
            notified.did_read = True
            notified.save()
        return Response({"message": "{0} success".format(request.user.id)})
