# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import defaultdict

from django.conf import settings
from django.core.mail import get_connection, EmailMultiAlternatives
from django.template import loader
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.utils.html import strip_tags
from notify.models import Notification, NotifiedUser
from notify.consts import NOTIFICATION_DATA


def render_notification_template_to_str(template_name, context):
    return loader.get_template("notifications/{}".format(template_name)).render(context)


def send_notification(
    notification_type: str, obj, users, context=None, send_in_feed=True
):
    """
    notification_type - check NotificationType class in const.py or accept custom subject and message body
    obj - object directly associated w/ notification. generic fk to it
    users - users who are receiving notification
    context - context to provide to template for email or body of notification
    send_in_feed - create notification feed element
    """
    notification_payload = NOTIFICATION_DATA.get(notification_type)
    body = render_notification_template_to_str(
        notification_payload.get("template_name"), context
    )
    subject = notification_payload.get("subject")
    if context is not None:
        subject = context.get("subject")
    notification, _ = Notification.objects.get_or_create(
        content_type=ContentType.objects.get_for_model(obj),
        object_id=obj.id,
        source=notification_type,
        description=body,
        name=subject,
    )
    for user in users:
        # Todo: Send notification based on the the user notification frequency
        if send_in_feed and user.is_active == True:
            notified_user, created = NotifiedUser.objects.get_or_create(
                notification=notification, recipient_id=user.id
            )
            notified_user.sent_as_email = False
            notified_user.save()
    return notification


def send_notification_to_users(notified_users):
    aggregated_mail = defaultdict(list)
    mail_to_fullname = dict()
    for user_email, user_fullname, subject, body in notified_users.values_list(
        "recipient__email",
        "recipient__fullname",
        "notification__name",
        "notification__description",
    ):
        aggregated_mail[user_email].append((subject, body))
        mail_to_fullname[user_email] = user_fullname
    connection = get_connection()
    mail_subject = "Notification from the UNHCR Integrity and Assurance Module"
    mail_subject = "{} {}".format(
        "Test - Message" if settings.ENV in ["local", "dev", "uat"] else "",
        mail_subject,
    )
    for email, messages in aggregated_mail.items():
        html_content = loader.get_template(
            "notifications/notification_summary.html"
        ).render(
            {
                "title": mail_subject,
                "user_fullname": mail_to_fullname[email],
                "messages": messages,
            }
        )
        emails = (
            settings.DEFAULT_EMAIL_TO_SEND.split(",")
            if settings.ENV in ["local", "dev", "uat"]
            else [email]
        )
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(
            mail_subject,
            text_content,
            settings.DEFAULT_FROM_EMAIL,
            emails,
            connection=connection,
        )
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    notified_users.update(sent_as_email=True, last_modified_date=timezone.now())


def send_error_notifications(error_msg=None, stack_answer=None):
    connection = get_connection()
    mail_subject = "Stack Error"
    html_content = (
        "<html><body><div>"
        + str(error_msg)
        + "</div><p>"
        + str(stack_answer)
        + "</p></body></html>"
    )
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(
        mail_subject,
        text_content,
        settings.DEFAULT_FROM_EMAIL,
        [
            settings.DEFAULT_EMAIL_TO_SEND.split(",")
            if settings.ENV in ["local", "dev", "uat", "prod"]
            else settings.DEFAULT_EMAIL_TO_SEND.split(",")
        ],
        connection=connection,
    )
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def send_email_with_attachment(notified_users, attachments):

    aggregated_mail = defaultdict(list)
    mail_to_fullname = dict()
    for user_email, user_fullname, subject, body in notified_users.values_list(
        "recipient__email",
        "recipient__fullname",
        "notification__name",
        "notification__description",
    ):
        aggregated_mail[user_email].append((subject, body))
        mail_to_fullname[user_email] = user_fullname
    connection = get_connection()
    mail_subject = "Notification from the UNHCR Integrity and Assurance Module"
    mail_subject = "{} {}".format(
        "Test - Message" if settings.ENV in ["local", "dev", "uat"] else "",
        mail_subject,
    )
    for email, messages in aggregated_mail.items():
        html_content = loader.get_template(
            "notifications/notification_summary.html"
        ).render(
            {
                "title": mail_subject,
                "user_fullname": mail_to_fullname[email],
                "messages": messages,
            }
        )
        text_content = strip_tags(html_content)
        emails = (
            settings.DEFAULT_EMAIL_TO_SEND.split(",")
            if settings.ENV in ["local", "dev", "uat"]
            else [email]
        )
        msg = EmailMultiAlternatives(
            mail_subject,
            text_content,
            settings.DEFAULT_FROM_EMAIL,
            emails,
            connection=connection,
        )
        msg.attach_alternative(html_content, "text/html")
        for attachment in attachments:
            msg.attach_file(attachment["file_path"])
        msg.send()
    notified_users.update(sent_as_email=True, last_modified_date=timezone.now())
