from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command

from common.data import create_permissions
from auditing.data import create_audit_roles
from field.data import create_field_roles
from partner.data import create_partner_roles
from unhcr.data import create_unhcr_roles


class Command(BaseCommand):
    help = "Creates a set of ORM objects for development and staging environment."

    def handle(self, *args, **options):
        create_permissions()
        create_audit_roles()
        create_field_roles()
        create_partner_roles()
        create_unhcr_roles()
        self.stdout.write("Permissions updated.")
