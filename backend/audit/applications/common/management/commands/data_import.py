from __future__ import absolute_import
import os
from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command
from decouple import config

from common.management.commands.data_import import config

IMPORT_UAT = config("IMPORT_UAT")

if IMPORT_UAT == "True" or IMPORT_UAT == True:
    from common.uat_fakedata_util import (
        import_area_region_bu_accounts,
        import_project_agreement_report,
        import_agreement_budget,
        generate_fake_data,
        import_field_assessments,
        import_oios_bu_report,
        import_icq_report,
        pqp_status,
        import_partner_modified,
        # import_fa_progress,
        import_project_cycle,
        import_partner_members,
    )
else:
    from common.new_fakedata_util import (
        import_area_region_bu_accounts,
        import_project_agreement_report,
        import_agreement_budget,
        generate_fake_data,
        import_field_assessments,
        import_oios_bu_report,
        import_icq_report,
        pqp_status,
        import_partner_modified,
        # import_fa_progress,
        import_project_cycle,
        create_test_users,
    )


class Command(BaseCommand):
    help = "Creates a set of ORM objects for development and staging environment."

    def handle(self, *args, **options):
        os.system("python manage.py loaddata --app project initial.json")
        os.system("python manage.py loaddata --app common initial.json")
        os.system("python manage.py loaddata --app field initial.json")
        os.system("python manage.py loaddata --app auditing initial.json")
        os.system("python manage.py loaddata --app icq initial_data.json")
        import_project_cycle()
        import_area_region_bu_accounts()
        generate_fake_data()

        os.system("python manage.py loaddata --app project settings.json")

        import_project_agreement_report()
        import_agreement_budget()
        import_field_assessments()
        import_oios_bu_report()
        import_icq_report()
        pqp_status()
        import_partner_modified()
        create_test_users()
        if IMPORT_UAT == "True" or IMPORT_UAT == True:
            import_partner_members()
        self.stdout.write("Fake projects script done.")
