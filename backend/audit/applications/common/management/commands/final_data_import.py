from __future__ import absolute_import
import os
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Creates a set of ORM objects for development and staging environment."

    def add_arguments(self, parser):
        parser.add_argument("year", type=int)

    def handle(self, *args, **options):
        os.system(
            "python manage.py loaddata --app accounts fixtures/small_db.json"
        )
        # generate_fake_data()
