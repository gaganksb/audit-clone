from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command

from common.new_fakedata_util import import_user_accounts


class Command(BaseCommand):
    help = "Creates a set of ORM objects for development and staging environment."

    def handle(self, *args, **options):
        import_user_accounts()
        self.stdout.write("User account imported.")
