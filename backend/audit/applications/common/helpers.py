from django.db import connection
from project.models import Cycle, AgreementMember
from django.db.models import Q
from account.models import UserTypeRole
from common.enums import UserTypeEnum

PA_MANUALLY_EXCLUDED = "SC12"


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def common_raw_sql(sql):
    cursor = connection.cursor()
    cursor.execute(sql)
    data = dictfetchall(cursor)
    connection.close()
    return data


def filter_project_selection(queryset, year, agreement_type, value):

    """
    Takes  project_agreement queryset and filters the project based on the requirement
    Ex: project included in PRE01 etc

    queryset: Project agreement queryset
    year: year to filter cycle and get the risk_threshold
    agreement_type: pre, int or fin
    value: included or excluded
    """

    if agreement_type not in ["pre", "int", "fin"] or value not in [
        "included",
        "excluded",
        "all",
    ]:
        return queryset.none()
    if queryset.exists() == False:
        return queryset.none()

    cycle_qs = Cycle.objects.filter(year=year)
    if cycle_qs.exists() == False:
        return queryset.none()
    else:
        cycle = cycle_qs.first()

    queryset = queryset.filter(cycle=cycle, is_auditable=True)

    if agreement_type == "pre":
        if value == "included":
            return queryset.filter(Q(pre_sense_check=True))
        elif value == "excluded":
            return queryset.exclude(Q(pre_sense_check=True))
        elif value == "all":
            return queryset
    elif agreement_type == "int":
        if value == "included":
            return queryset.filter(Q(int_sense_check=True))
        elif value == "excluded":
            return queryset.exclude(Q(int_sense_check=True))
        elif value == "all":
            return queryset
    elif agreement_type == "fin":
        if value == "included":
            return queryset.filter(Q(fin_sense_check=True))
        elif value == "excluded":
            return queryset.exclude(Q(fin_sense_check=True))
        elif value == "all":
            return queryset
    else:
        return queryset.none()


def parse_location(location_arr):
    result = []
    for idx, val in enumerate(location_arr):
        res = {}
        if idx % 2 == 0:
            res["city"] = val
        else:
            res["country"] = val

        result.append(res)
    return result


def get_or_create_aggr_member(user, user_type, user_role):
    if user_role == "focal":
        agreement_member, _ = AgreementMember.objects.get_or_create(
            user=user, user_type=user_type, focal=True
        )
        return agreement_member
    elif user_role == "alternate_focal":
        agreement_member, _ = AgreementMember.objects.get_or_create(
            user=user, user_type=user_type, alternate_focal=True
        )
        return agreement_member
    elif user_role == "reviewer":
        agreement_member, _ = AgreementMember.objects.get_or_create(
            user=user, user_type=user_type, reviewer=True
        )
        return agreement_member


def update_user_role(resource_model, resource_data):
    resource_model_data = UserTypeRole.objects.filter(
        user_id=resource_data.user_id, is_default=True
    )
    if resource_model_data.exists() == False:
        resource_model_qs = UserTypeRole.objects.filter(
            user_id=resource_data.user_id,
            resource_model=resource_model,
            resource_id=resource_data.id,
        )
        if resource_model_qs.exists():
            resource_model_qs.update(is_default=True)

def filter_role_based_agreements(user, membership, queryset):
    if membership['type'] == UserTypeEnum.HQ.name:
        return queryset
    elif membership['type'] == UserTypeEnum.FO.name:
        return queryset.filter(business_unit__field_office_id=membership['field_office']['id'])
    elif membership['type'] == UserTypeEnum.PARTNER.name and membership['role'] == "ADMIN":
        return queryset.filter(partner_id=membership['partner']['id'])
    elif membership['type'] == UserTypeEnum.AUDITOR.name and membership['role'] == "ADMIN":
        return queryset.filter(audit_firm_id=membership['audit_agency']['id'])
    elif membership['type'] == UserTypeEnum.AUDITOR.name and membership['role'] == "REVIEWER":
        return queryset.filter(focal_points__user_type="auditing", focal_points__user_id__in=[user.id])
    else:
        return queryset.none()