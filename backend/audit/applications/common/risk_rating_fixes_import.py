import logging
import os
import xlrd
import random
import re
from random import randint
from django.conf import settings
from django.utils import timezone
from django.db.models import Q, Sum
from account.models import User, UserTypeRole
from field.models import (
    FieldOffice,
    Score,
    FieldMember,
    Role as RoleField,
    FieldAssessment,
    FieldAssessmentProgress,
)
from common.models import CostCenter, Country, BusinessUnit, Area, Region, Comment
from partner.models import (
    ImplementerType,
    Partner,
    Modified,
    PartnerMember,
    PQPStatus,
    PQPStatusNew,
    Role as RolePartner,
)
from icq.models import ICQReport
from project.models import (
    Pillar,
    Vendor,
    Operation,
    Goal,
    PopulationPlanningGroup,
    Situation,
    AgreementType,
    Cycle,
    Agreement,
    AgreementReport,
    Output,
    AgreementBudget,
    Account,
    AccountCategory,
    AccountType,
    Currency,
    SenseCheck,
    Status,
    OIOSBusinessUnitReport,
)
from common.consts import AGREEMENT_MEMBER_CHOICES, ACCOUNT_STATUSES, YES_NO
from unhcr.models import *
from .factories import *
from partner.data import import_icq, import_pqp, import_modified, create_partner_roles
from common.data import import_bu, create_permissions
from auditing.data import create_audit_roles
from field.enums import FieldRoleEnum
from field.data import import_field_offices, create_field_roles
from unhcr.data import create_unhcr_roles
from unhcr.enums import UNHCRRoleEnum
from partner.enums import PartnerRoleEnum
from auditing.models import (
    Role as AuditRole,
    AuditMember,
    AuditAgency,
)
from project.tasks import assign_agreement_member

logger = logging.getLogger("console")
from auditing.serializers import WorkSplitGroupSerializer
from datetime import datetime
from decouple import config

from partner.models import Role as PartnerRole
from unhcr.models import Role as UNRole, UNHCRMember

# from common.new_fakedata_util import update_user_role

IMPORT_ALL = config("IMPORT_ALL")
REAL_EMAIL = config("REAL_EMAIL")

ACCOUNT_TYPE_CHOICES = {
    "other_accounts": "Other accounts",
    "procurement": "Procurement",
    "staff_cost": "Staff Costs",
    "cash": "cash",
    "no_budget": "No budget yet in HNIP",
}

n_audit_agencies = 4
n_audit_roles = 2
n_work_groups = 3

n_partners = 4
n_partner_roles = 2

n_unhcr_roles = 3

n = 50

HQ_TEST_ACCOUNTS = [
    {
        "fullname": "Fake HQ Preparer",
        "email": "hq-preparer@unhcr.org",
        "role": UNHCRRoleEnum.PREPARER.name,
    },
    {
        "fullname": "Fake HQ Reviewer",
        "email": "hq-reviewer@unhcr.org",
        "role": UNHCRRoleEnum.REVIEWER.name,
    },
    {
        "fullname": "Fake HQ Approver",
        "email": "hq-approver@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Deogratias Habimana",
        "email": "habimade@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Robert Berenyi",
        "email": "berenyi@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Gianluca Nuzzo",
        "email": "nuzzo@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Anjali Upreti",
        "email": "upreti@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Francesco Sasso",
        "email": "sasso@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Reddy Vishal",
        "email": "reddy@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Annalisa Diana",
        "email": "diana@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
]

FO_TEST_ACCOUNTS = [
    {
        "fullname": "Fake FO Reviewer",
        "email": "fo-reviewer@unhcr.org",
        "role": FieldRoleEnum.REVIEWER.name,
    },
    {
        "fullname": "Fake FO Approver",
        "email": "fo-approver@unhcr.org",
        "role": FieldRoleEnum.APPROVER.name,
    },
]

PARTNER_TEST_ACCOUNTS = [
    {
        "fullname": "Fake Partner admin",
        "email": "partner-admin@unhcr.org",
        "role": PartnerRoleEnum.ADMIN.name,
    },
    {
        "fullname": "Fake Partner Reader",
        "email": "partner-reader@unhcr.org",
        "role": PartnerRoleEnum.REVIEWER.name,
    },
]


def handle_excel_date(excel_date):
    excel_date = int(excel_date)
    dt = datetime.fromordinal(datetime(1900, 1, 1).toordinal() + excel_date - 2)
    return dt
    # print(dt)
    # tt = dt.timetuple()
    # return tt


def import_areas():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Areas")

    for idx_i in range(1, sheet.nrows):
        area = []
        for idx_j in range(sheet.ncols):
            area.append(sheet.cell(idx_i, idx_j).value)
        Area.objects.get_or_create(
            code=area[0],
            name=area[1],
        )


def import_regions():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Regions")

    regions = []

    for idx_i in range(1, sheet.nrows):
        region = []
        for idx_j in range(sheet.ncols):
            region.append(sheet.cell(idx_i, idx_j).value)
        if region not in regions:
            regions.append(region)
            Region.objects.get_or_create(
                code=region[0],
                name=region[1],
            )


def import_countries():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Countries")

    countries = []

    for idx_i in range(1, sheet.nrows):
        country = []
        for idx_j in range(sheet.ncols):
            country.append(sheet.cell(idx_i, idx_j).value)
        if country not in countries:
            countries.append(country)
            Country.objects.get_or_create(
                code=country[0],
                name=country[1],
            )


def import_bu_s():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Unique_BUs")
    bu_s = []
    for idx_i in range(1, sheet.nrows):
        bu = []
        for idx_j in range(sheet.ncols):
            bu.append(sheet.cell(idx_i, idx_j).value)
        if bu not in bu_s:
            bu_s.append(bu)
            total_entry = [i[0] for i in bu_s].count(bu[0])
            if total_entry > 1:
                field_office, _created = FieldOffice.objects.get_or_create(name=bu[3])
                BusinessUnit.objects.get_or_create(
                    code="{0}{1}".format(bu[0], total_entry - 1),
                    name=bu[3],
                    field_office=field_office,
                )
            else:
                field_office, _created = FieldOffice.objects.get_or_create(name=bu[1])
                BusinessUnit.objects.get_or_create(
                    code=bu[0],
                    name=bu[1],
                    field_office=field_office,
                )


def import_region_area():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Relationships")

    idx_area = 2
    idx_region = 3

    region_area_s = []
    errors = []

    for idx_i in range(1, sheet.nrows):
        region_area = []
        for idx_j in [idx_area, idx_region]:
            region_area.append(sheet.cell(idx_i, idx_j).value)
        if region_area not in region_area_s:
            region_area_s.append(region_area)
            try:
                region = Region.objects.get(name=region_area[1])
                area = Area.objects.get(name=region_area[0])
                region.area = area
                region.save()
            except Exception as e:
                logger.error(str(e))
                errors.append(region_area)
    return errors


def import_country_region():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "countries_region_area_bu.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Relationships")

    idx_bu = 0
    idx_region = 3

    country_region_s = []
    errors = []

    for idx_i in range(1, sheet.nrows):
        country_region = []
        for idx_j in [idx_bu, idx_region]:
            country_region.append(sheet.cell(idx_i, idx_j).value)
        if country_region not in country_region_s:
            country_region_s.append(country_region)
            try:
                country = Country.objects.get(code=country_region[0][:-2])
                region = Region.objects.get(name=country_region[1])
                country.region = region
                country.save()
            except Exception as e:
                logger.error(str(e))
                errors.append(country_region)
    return errors


def map_consts(value):
    if value == "active":
        return ACCOUNT_STATUSES[0][0]
    elif value == "not active":
        return ACCOUNT_STATUSES[1][0]
    elif value == "y":
        return YES_NO[0][0]
    elif value == "n":
        return YES_NO[1][0]
    else:
        return None


def import_accounts():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "accounts.xlsx")

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_name("Categories")

    for idx_i in range(1, sheet.nrows):
        item = {
            "code": str(int(sheet.cell(idx_i, 0).value)).lower().strip(),
            "account_type": str(sheet.cell(idx_i, 1).value).lower().strip(),
            "status": map_consts(str(sheet.cell(idx_i, 2).value).lower().strip()),
            "open_item": map_consts(str(sheet.cell(idx_i, 3).value).lower().strip()),
            "budgetary_only": map_consts(
                str(sheet.cell(idx_i, 4).value).lower().strip()
            ),
            "description": str(sheet.cell(idx_i, 5).value).lower().strip(),
            "account_category": str(sheet.cell(idx_i, 6).value).lower().strip(),
        }

        account_category, _created = AccountCategory.objects.get_or_create(
            description=item["account_category"]
        )
        account_type, _created = AccountType.objects.get_or_create(
            description=item["account_type"]
        )

        if not Account.objects.filter(code=item["code"]).exists():
            Account.objects.create(
                code=item["code"],
                account_type=account_type,
                status=item["status"],
                open_item=item["open_item"],
                budgetary_only=item["budgetary_only"],
                description=item["description"],
                account_category=account_category,
            )


"""
Import of Agreement Budget
"""


def import_HNIP999(file):
    cwd = os.getcwd()
    # xls = os.path.join(cwd, "MSRP", file)
    xls = os.path.join(cwd, "audit", "applications", "project", "excel", file)
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)

    keys = []
    for idx_j in range(sheet.ncols):
        keys.append(sheet.cell(0, idx_j).value)

    items = []
    if IMPORT_ALL == "True" or IMPORT_ALL == True:
        nrows = sheet.nrows
    else:
        nrows = 6

    for idx_i in range(1, nrows):
        item = {}
        for idx_j in range(sheet.ncols):
            item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
        items.append(item)
    return items


def import_HNIP998(file):
    cwd = os.getcwd()
    # xls = os.path.join(cwd, "MSRP", file)
    xls = os.path.join(cwd, "audit", "applications", "project", "excel", file)

    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)

    keys = []
    for idx_j in range(sheet.ncols):
        keys.append(sheet.cell(0, idx_j).value)

    items = []
    for idx_i in range(1, sheet.nrows):
        item = {}
        for idx_j in range(sheet.ncols):
            item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
        items.append(item)
    return items


def parse_HNIP999(items):
    agreements = []
    for item in items:
        agreements.append([item["BUSINESS_UNIT"], int(item["HCR_SA_ID"])])
    return agreements


def filter_HNIP998(items, agreements=None):
    if agreements is None:
        res = items
    else:
        res = []
        for item in items:
            if [item["BUSINESS_UNIT"], int(item["HCR_SA_ID"])] in agreements:
                res.append(item)
    return res


def util_decimal(item):
    if type(item) is str:
        _ = item.strip()
        if _ in [""]:
            return 0
        else:
            return float(_)
    return item


def is_None(value):
    if isinstance(value, int):
        return False
    elif isinstance(value, float):
        return False
    elif value.strip() not in [
        "",
    ]:
        return False
    return True


def import_outputs(items):
    outputs = []
    errors = []
    for item in items:
        try:
            if item["CLASS_FLD"] not in outputs:
                outputs.append(item["CLASS_FLD"])
                if item["CLASS_FLD"].strip() not in [
                    "",
                ]:
                    Output.objects.get_or_create(code=item["CLASS_FLD"])
        except Exception as e:
            logger.error(str(e))
            errors.append(item)
    return errors


def import_agreement_budgets(items):
    errors = []
    for item in items:
        try:
            business_unit = BusinessUnit.objects.get(code=item["BUSINESS_UNIT"])
            number = int(item["HCR_SA_ID"])
            if "JORMN" in business_unit.code or "UNHCR" in business_unit.code:
                agreement = Agreement.objects.filter(
                    business_unit__code__icontains=business_unit, number=number
                ).first()
            else:
                agreement = Agreement.objects.filter(
                    business_unit__code=business_unit, number=number
                ).first()
            if agreement is None:
                print(item, "itemitemitem")
            else:
                account = None
                account_row = None if is_None(item["ACCOUNT"]) else int(item["ACCOUNT"])
                if not is_None(item["ACCOUNT"]):
                    account, _created = Account.objects.get_or_create(
                        code=item["ACCOUNT"]
                    )
                output = (
                    None
                    if is_None(item["CLASS_FLD"])
                    else Output.objects.get(code=item["CLASS_FLD"])
                )
                ppg = (
                    None
                    if is_None(item["PRODUCT"])
                    else PopulationPlanningGroup.objects.get(code=item["PRODUCT"])
                )
                situation = (
                    None
                    if is_None(item["CHARTFIELD1"])
                    else Situation.objects.get(code=int(item["CHARTFIELD1"]))
                )
                cost_center = (
                    None
                    if is_None(item["HCR_CC"])
                    else CostCenter.objects.get(code=int(item["HCR_CC"]))
                )
                pillar = (
                    None
                    if is_None(item["HCR_PILLAR"])
                    else Pillar.objects.get(code=item["HCR_PILLAR"])
                )
                goal = (
                    None
                    if is_None(item["HCR_GOAL"])
                    else Goal.objects.get(code=item["HCR_GOAL"])
                )
                budget = util_decimal(item["POSTED_BASE_AMT"])
                report_date = int(item["TO_CHAR(B.DATE_TO,'YYYY-MM-DD')"])
                report_date = datetime.fromordinal(
                    datetime(1900, 1, 1).toordinal() + report_date - 2
                )

                _type = item["ACCOUNT_TYPE"]
                _type in ACCOUNT_TYPE_CHOICES.values()
                value = [
                    i
                    for i, v in ACCOUNT_TYPE_CHOICES.items()
                    if _type is not None and v.lower() == _type.lower()
                ]
                account_type = value[0] if len(value) > 0 else None
                AgreementBudget.objects.create(
                    agreement=agreement,
                    account=account,
                    account_type=account_type,
                    output=output,
                    ppg=ppg,
                    situation=situation,
                    cost_center=cost_center,
                    goal=goal,
                    pillar=pillar,
                    budget=budget,
                    report_date=report_date,
                    year=int(item["BUDGET_REF"]),
                )
        except Exception as e:
            print("Error in import_agreement_budgets", e)
            logger.error(str(e))
            errors.append(item)
    return errors


def import_implementer_types(items):
    implementer_types = []
    for item in items:
        if item["PARENT_NODE_NAME"] not in implementer_types:
            implementer_types.append(item["PARENT_NODE_NAME"])
            ImplementerType.objects.get_or_create(
                implementer_type=item["PARENT_NODE_NAME"],
            )


def import_partners(items):
    partners = []
    for item in items:
        try:
            if item["OPERATING_UNIT"] not in partners:
                partners.append(item["OPERATING_UNIT"])
                implementer_type = ImplementerType.objects.get(
                    implementer_type=item["PARENT_NODE_NAME"],
                )
                Partner.objects.get_or_create(
                    legal_name=item["HCR_DESCR"],
                    number=item["OPERATING_UNIT"],
                    implementer_type=implementer_type,
                )
        except Exception as e:
            logger.error(str(e))


def import_pillars(items):
    pillars = []
    for item in items:
        if item["HCR_PILLAR"] not in pillars:
            pillars.append(item["HCR_PILLAR"])
            Pillar.objects.get_or_create(
                code=item["HCR_PILLAR"],
                description=item["HCR_PILLAR_DESC"],
            )


def import_vendors(items):
    vendors = []
    for item in items:
        if item["VENDOR_ID"] not in vendors:
            vendors.append(item["VENDOR_ID"])
            Vendor.objects.get_or_create(
                code=item["VENDOR_ID"],
            )


def import_operations(items):
    operations = []
    for item in items:
        if item["HCR_OPERATION_CODE"] not in operations:
            operations.append(item["HCR_OPERATION_CODE"])
            Operation.objects.get_or_create(
                code=item["HCR_OPERATION_CODE"],
            )


def import_goals(items):
    goals = []
    for item in items:
        goal_list = item["HCR_GOALS_LIST"].split(", ")
        for goal in goal_list:
            if goal not in goals:
                goals.append(goal)
                Goal.objects.get_or_create(
                    code=goal,
                )


def import_cc_s(items):
    cc_s = []
    for item in items:
        cc_list = str(item["HCR_CC_LIST"]).split(", ")
        for cc in cc_list:
            cc = str(int(float(cc.replace(",", ""))))
            if cc not in cc_s:
                cc_s.append(cc)
                CostCenter.objects.get_or_create(code=cc)


def import_ppg_s(items):
    ppg_s = []
    for item in items:
        ppg_list = item["HCR_PPG_LIST"].split(", ")
        for ppg in ppg_list:
            if ppg not in ppg_s:
                ppg_s.append(ppg)
                PopulationPlanningGroup.objects.get_or_create(
                    code=ppg,
                )


def import_situations(items):
    situations = []
    for item in items:
        situation_list = item["HCR_SIT_LIST"].split(", ")
        for situation in situation_list:
            if situation not in situations:
                situations.append(situation)
                Situation.objects.get_or_create(
                    code=situation,
                )


def import_agreement_types(items):
    agreement_types = []
    for item in items:
        if item["XLATLONGNAME"] not in agreement_types:
            agreement_types.append(item["XLATLONGNAME"])
            AgreementType.objects.get_or_create(
                description=item["XLATLONGNAME"],
            )


def import_budget_refs(items):
    budget_refs = []
    for item in items:
        if item["BUDGET_REF"] not in budget_refs:
            budget_refs.append(item["BUDGET_REF"])
            Cycle.objects.get_or_create(
                budget_ref=item["BUDGET_REF"],
                year=item["BUDGET_REF"],
            )


def import_agreements(items):
    errors = []
    for item in items:
        try:
            title = item["DESCR100"]
            agreement_type = AgreementType.objects.get(description=item["XLATLONGNAME"])
            business_unit = BusinessUnit.objects.get(code=item["BUSINESS_UNIT"])
            partner = Partner.objects.get(number=item["OPERATING_UNIT"])
            number = int(item["HCR_SA_ID"])
            operation = Operation.objects.get(code=item["HCR_OPERATION_CODE"])
            pillar = Pillar.objects.get(code=item["HCR_PILLAR"])
            start_date = item["TO_CHAR(B.HCR_SA_START_DT,'YYYY-MM-DD')"]
            start_date = (
                start_date
                if type(start_date) != float
                else handle_excel_date(start_date)
            )
            end_date = item["TO_CHAR(B.HCR_SA_COMPL_DT,'YYYY-MM-DD')"]
            end_date = (
                end_date if type(end_date) != float else handle_excel_date(end_date)
            )
            cycle = Cycle.objects.get(budget_ref=item["BUDGET_REF"])
            vendor = Vendor.objects.get(code=item["VENDOR_ID"])

            agreement, _created = Agreement.objects.get_or_create(
                title=title,
                agreement_type=agreement_type,
                business_unit=business_unit,
                partner=partner,
                number=number,
                operation=operation,
                pillar=pillar,
                start_date=start_date,
                end_date=end_date,
                cycle=cycle,
                vendor=vendor,
            )

            goals_list = item["HCR_GOALS_LIST"].split(", ")
            for goal in goals_list:
                agreement.goals.add(Goal.objects.get(code=goal))
            cc_list = str(item["HCR_CC_LIST"]).split(", ")
            for cc in cc_list:
                cc = str(int(float(cc.replace(",", ""))))
                agreement.cost_centers.add(CostCenter.objects.get(code=cc))
            ppg_list = item["HCR_PPG_LIST"].split(", ")
            for ppg in ppg_list:
                agreement.ppg_s.add(PopulationPlanningGroup.objects.get(code=ppg))
            situations = item["HCR_SIT_LIST"].split(", ")
            for situation in situations:
                agreement.situations.add(Situation.objects.get(code=situation))

            agreement.save()
        except Exception as e:
            print("Error while importing the data: --- ", e, item)
            logger.error(str(e))
            errors.append(item)

    cycle = Cycle.objects.filter(year=2018)
    # if cycle.exists():
    #     cycle.update(status_id=1)
    #     agreement = cycle.first().agreement_cycle.filter(risk_rating__gte=2.5)
    #     for agree in agreement:
    #         description="fake data for demo"
    #         comment = (
    #             Comment.objects.create(
    #                 message=description,
    #                 object_id=agree.id,
    #                 content_type_id=54,
    #                 comment_type='PRE'
    #             )
    #         )
    #         agree.pre_sense_check_id = 1
    #         agree.pre_comment=comment
    #         agree.status_id=1
    #         agree.save()
    return errors


def import_currency(items):
    currencies = []
    for item in items:
        if item["BASE_CURRENCY"] not in currencies:
            currencies.append(item["BASE_CURRENCY"])
            if item["BASE_CURRENCY"].strip() not in [
                "",
            ]:
                Currency.objects.get_or_create(code=item["BASE_CURRENCY"])


def util_decimal(item):
    if type(item) is str:
        _ = item.strip()
        if _ in [""]:
            return 0
        else:
            return float(_)
    return item


def import_agreement_report(items):
    errors = []
    for item in items:
        try:
            business_unit = BusinessUnit.objects.get(code=item["BUSINESS_UNIT"])
            number = item["HCR_SA_ID"]
            agreement = Agreement.objects.get(
                business_unit=business_unit, number=number
            )
            currency = None
            if item["BASE_CURRENCY"].strip() not in [
                "",
            ]:
                currency = Currency.objects.get(code=item["BASE_CURRENCY"])
            report_date = item["TO_CHAR(B.HCR_SA_DATE,'YYYY-MM-DD')"]
            base = util_decimal(item["SUM(C.POSTED_BASE_AMT)"])
            installments = util_decimal(item["HCR_IP_INSTALL_AMT"])
            adjustments = util_decimal(item["HCR_IP_ADJUST_AMT"])
            cancellations = util_decimal(item["HCR_IP_CANCEL_AMT"])
            ipfr = util_decimal(item["HCR_IP_IPFR_AMT"])
            refund = util_decimal(item["HCR_IP_REFUND_AMT"])
            receivables = util_decimal(item["HCR_IP_RECV_AMT"])
            write_off = util_decimal(item["HCR_IP_WRITOFF_AMT"])

            AgreementReport.objects.get_or_create(
                agreement=agreement,
                currency=currency,
                base=base,
                installments=installments,
                adjustments=adjustments,
                cancellations=cancellations,
                ipfr=ipfr,
                refund=refund,
                receivables=receivables,
                write_off=write_off,
            )
        except Exception as e:
            logger.error(str(e))
            errors.append(item)
    return errors


def import_area_region_bu_accounts():
    print("Importing Areas")
    import_areas()
    print("Importing Regions")
    import_regions()
    print("Importing Countries")
    import_countries()
    print("Importing BUs")
    import_bu_s()
    print("Importing Region-Area")
    import_region_area()
    print("Importing Country-Region")
    import_country_region()
    print("Importing Accounts")
    import_accounts()
    print("Finished")


def import_project_agreement_report():
    for file in ["HNIP999_2020.xlsx"]:
        items = import_HNIP999(file)
        print("Importing Implementer Types")
        import_implementer_types(items)
        print("Importing Partners")
        import_partners(items)
        print("Importing Pillars")
        import_pillars(items)
        print("Importing Vendors")
        import_vendors(items)
        print("Importing Operations")
        import_operations(items)
        print("Importing Goals")
        import_goals(items)
        print("Importing Cost Centers")
        import_cc_s(items)
        print("Importing Population Planning Groups")
        import_ppg_s(items)
        print("Importing Situations")
        import_situations(items)
        print("Importing Agreement Types")
        import_agreement_types(items)
        print("Importing Budget Refs")
        import_budget_refs(items)
        # print('Importing Agreements')
        # import_agreements(items)
        print("Importing Currencies")
        import_currency(items)
        print("Importing Agreement Reports")
        import_agreement_report(items)
        print("Finished")


def import_agreement_budget():
    files = [["HNIP999_2020.xlsx", "HNIP998_2020.xlsx"]]
    for file in files:
        items_999 = import_HNIP999(file[0])
        agreements = parse_HNIP999(items_999)
        items_998 = import_HNIP998(file[1])
        items = filter_HNIP998(items=items_998, agreements=agreements)
        if True:
            print("Importing Outputs")
            import_outputs(items)
            print("Importing Agreement Budgets", file)
            import_agreement_budgets(items)
            print("Finished")


def create_audit_member(member_detail, auditor):
    email = member_detail["email"]
    if REAL_EMAIL == False or REAL_EMAIL == "False":
        email = "{0}@{1}".format(email.split("@")[0], "example.com")
    user, created = User.objects.get_or_create(email=email)
    user.fullname = member_detail["name"]
    user.phone = member_detail["phone"]
    user.set_password("Password@123")
    user.save()
    auditor_member_check = AuditMember.objects.filter(
        audit_agency_id=member_detail["auditor_id"],
        role__role=member_detail["role"],
        user_id=user.id,
    )
    if auditor_member_check.exists() == False:
        role = AuditRole.objects.get(role=member_detail["role"])
        auditor_member = AuditMember(
            user=user,
            role=role,
            audit_agency=auditor,
            date_send_invitation=timezone.now().date(),
        )
        auditor_member.save()
        update_user_role("auditmember", auditor_member)


def import_audit_firm():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "Audit Firms details.xlsx")
    keys = []
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)
    for idx_j in range(sheet.ncols):
        keys.append(sheet.cell(0, idx_j).value)

    for idx_i in range(1, sheet.nrows):
        item = {}
        partner_detail = ""
        for idx_j in range(sheet.ncols):
            item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
        agency_name = item["Audit Firm Legal Name"].strip()
        country_name = item["Country"].strip()
        country = Country.objects.filter(name=country_name)
        auditor, created = AuditAgency.objects.get_or_create(legal_name=agency_name)
        auditor.address_1 = item["Address 1"]
        auditor.address_2 = item["Address 2"]
        auditor.city = item["City"]
        auditor.country = country.last()
        auditor.save()
        auditor_phone = item["Office Phone / Fax"]
        auditor_name = item["Auditor Administrator"]
        auditor_list = item["Audiitor Reviewer"].split()
        auditor_admin_email = item["Email of Auditor Rep\n (Audit Admiistrator)"]
        """ADMIN created"""
        member_detail = {}
        member_detail["email"] = auditor_admin_email
        member_detail["name"] = auditor_name
        member_detail["phone"] = auditor_phone
        member_detail["auditor_id"] = auditor.id
        member_detail["role"] = "ADMIN"
        create_audit_member(member_detail, auditor)

        """READER created"""
        for reviwer in auditor_list:
            member_detail["email"] = reviwer
            member_detail["name"] = reviwer.split("@")[0]
            member_detail["phone"] = ""
            member_detail["auditor_id"] = auditor.id
            member_detail["role"] = "REVIEWER"
            create_audit_member(member_detail, auditor)


def generate_fake_data():
    create_permissions()
    create_field_roles()
    create_partner_roles()
    create_unhcr_roles()
    create_audit_roles()

    admin, created = User.objects.get_or_create(
        fullname="Admin",
        defaults={
            "email": "admin@unhcr.org",
            "is_superuser": True,
            "is_staff": True,
        },
    )
    password = "Passw0rd!"
    admin.set_password(password)
    admin.save()
    print("Superuser {}: {}/{}".format("created", admin.email, password))

    for tester in HQ_TEST_ACCOUNTS:
        email = tester["email"]
        if REAL_EMAIL == False or REAL_EMAIL == "False":
            email = "{0}@{1}".format(email.split("@")[0], "example.com")
        user, created = User.objects.get_or_create(
            fullname=tester["fullname"], email=email
        )

        password = "Password@123"
        user.set_password(password)
        user.save()
        role = Role.objects.get(role=tester["role"])
        unhcr_member, unhcr_created = UNHCRMember.objects.get_or_create(
            role=role, user=user
        )
        update_user_role("unhcrmember", unhcr_member)

    for _ in range(n_audit_agencies):
        audit_agency = AuditAgencyFactory()
        for _ in range(n_audit_roles):
            audit_member = AuditMemberFactory(audit_agency=audit_agency)
            update_user_role("auditmember", audit_member)
    import_audit_firm()
    print("Audit models created")

    # import Partner Member
    partners = Partner.objects.all()
    partner_roles = RolePartner.objects.all()
    for partner in partners:
        for role in partner_roles:
            partnermember = PartnerMemberFactory(partner=partner, role=role)
            update_user_role("partnermember", partnermember)

    print("Partner models created")
    # import FieldOffice data from excel
    import_field_offices()
    print("Field Offices created")

    field_offices = FieldOffice.objects.all()
    field_roles = RoleField.objects.all()
    for field_office in field_offices:
        for role in field_roles:
            fieldmember = FieldMemberFactory(field_office=field_office, role=role)
            update_user_role("fieldmember", fieldmember)

    for _ in range(n_unhcr_roles):
        unhcr_member = UNHCRMemberFactory()
        update_user_role("unhcrmember", unhcr_member)
    print("UNHCR models created")

    import_final_list()
    print("Final list created")
    import_audit_group()
    print("Audit Group created")


def update_user_role(resource_model, resource_data):
    resource_model_data = UserTypeRole.objects.filter(
        user_id=resource_data.user_id, is_default=True
    )
    if resource_model_data.exists() == False:
        resource_model_qs = UserTypeRole.objects.filter(
            user_id=resource_data.user_id,
            resource_model=resource_model,
            resource_id=resource_data.id,
        )
        if resource_model_qs.exists():
            resource_model_qs.update(is_default=True)


def update_partner_agreement_member(_clean_data, item, partners, partner_focal_role):
    errors = []
    formated_contacts = item["With Formatted Contacts"]
    email = _clean_data[1]
    phone = ""
    name = email
    if _clean_data[0]:
        name = _clean_data[0]
    if _clean_data[2]:
        phone = _clean_data[2]
    if REAL_EMAIL == False or REAL_EMAIL == "False":
        email = "{0}@{1}".format(email.split("@")[0], "example.com")
    user, created = User.objects.get_or_create(email=email)
    user.fullname = name
    user.phone = phone
    password = "Password@123"
    user.set_password(password)
    user.save()
    partner = partners.first()
    partner_member_check = PartnerMember.objects.filter(
        partner_id=partner.id, role_id=partner_focal_role.id, user_id=user.id
    )
    if partner_member_check.exists() == False:
        partner_member = PartnerMember(
            user=user,
            role=partner_focal_role,
            partner=partner,
            date_send_invitation=timezone.now().date(),
        )
        partner_member.save()
        update_user_role("partnermember", partner_member)
    else:
        logger.error(str(partners))
        errors.append(partner.legal_name)
    return user


def update_unchr_agreement_member(_clean_data, item, unhcrRole):
    errors = []
    formated_contacts = item["With Formatted Contacts"]
    email = _clean_data[1]
    phone = ""
    name = email
    if _clean_data[0]:
        name = _clean_data[0]
    if _clean_data[2]:
        phone = _clean_data[2]
    if REAL_EMAIL == False or REAL_EMAIL == "False":
        email = "{0}@{1}".format(email.split("@")[0], "example.com")
    user, created = User.objects.get_or_create(email=email)
    user.fullname = name
    user.phone = phone
    password = "Password@123"
    user.set_password(password)
    user.save()
    unchr_member_check = UNHCRMember.objects.filter(
        role_id=unhcrRole.id, user_id=user.id
    )
    if unchr_member_check.exists() == False:
        unhcr_member = UNHCRMember(
            user=user, role=unhcrRole, date_send_invitation=timezone.now().date()
        )
        unhcr_member.save()
        update_user_role("unhcrmember", unhcr_member)
    else:
        logger.error(str(unchr_member_check))
        errors.append(unchr_member_check)
    return user


def import_final_list(max_items=None):
    cwd = os.getcwd()
    regex = "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
    xls = os.path.join(cwd, "MSRP", "2019+-+Final+List.xlsx")
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)
    sense_check = SenseCheck.objects.get(code="SC13")
    status = Status.objects.get(code="FIN03")
    unhcrRole = UNHCRRole.objects.get(role="REVIEWER")
    partner_focal_role = PartnerRole.objects.get(role="ADMIN")
    keys = []
    focal_user = ""
    alternate_focal_user = ""
    unhcr_alternate_focal_user = ""
    unhcr_focal_user = ""
    errors = []
    for idx_j in range(sheet.ncols):
        keys.append(sheet.cell(0, idx_j).value)
    for idx_i in range(1, sheet.nrows):
        item = {}
        partner_detail = ""
        for idx_j in range(sheet.ncols):
            item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value

        formated_contacts = item["With Formatted Contacts"]
        if formated_contacts == 1:
            bu = item["Business Unit"]
            business_unit = BusinessUnit.objects.get(code=bu)
            number = int(item["Agreement Number"])
            partner_name = item["Partner Name"].strip()
            partners = Partner.objects.filter(Q(legal_name__icontains=partner_name))
            country_code = item["Operation (Country or HQ)"].strip()
            country = Country.objects.filter(code=country_code)
            country_id = None
            if country.exists():
                country_id = country.first().id
            agreement = Agreement.objects.filter(
                business_unit_id=business_unit.id, number=number
            )
            if agreement.exists():
                if partners.exists():

                    """
                    Get partner focal and alternet focal points and update to project agreement member

                    """

                    partner_focal_person = item[
                        "3 - PILOT E-mail & Telephone Number of Partner's Audit Focal Person"
                    ].split(",")
                    clean_data = ""

                    if len(partner_focal_person) == 3:
                        _partner_detail = (
                            str(partner_focal_person).replace("[", "").replace("]", "")
                        )
                        clean_data = [
                            i.replace("'", "").strip()
                            for i in _partner_detail.split(",")
                        ]
                        if clean_data[1]:
                            if re.search(regex, clean_data[1]):
                                focal_user = update_partner_agreement_member(
                                    clean_data, item, partners, partner_focal_role
                                )

                    elif len(partner_detail) > 5:
                        _partner_detail = (
                            str(partner_focal_person)
                            .replace("]',", "']  ||")
                            .replace("['", "")
                            .replace("]'", "")
                            .replace("|| '", "||")
                            .split("||")
                        )
                        for i in _partner_detail:
                            clean_data = (
                                str(i).replace("[", "").replace("]", "").split(",")
                            )
                            if clean_data[1]:
                                if re.search(regex, clean_data[1]):
                                    focal_user = update_partner_agreement_member(
                                        clean_data, item, partners, partner_focal_role
                                    )
                    else:
                        logger.error(str(partner_detail))
                        errors.append(partner_detail)

                    partner_focal_person_alternate = item[
                        "4 - PILOT E-mail &Telephone Number of Partner's Alternate Audit Focal Person"
                    ].split(",")

                    if len(partner_focal_person_alternate) == 3:
                        _partner_detail = (
                            str(partner_focal_person_alternate)
                            .replace("[", "")
                            .replace("]", "")
                        )
                        clean_data = [
                            i.replace("'", "").strip()
                            for i in _partner_detail.split(",")
                        ]
                        if clean_data[1]:
                            if re.search(regex, clean_data[1]):
                                alternate_focal_user = update_partner_agreement_member(
                                    clean_data, item, partners, partner_focal_role
                                )

                    elif len(partner_detail) > 5:
                        _partner_detail = (
                            str(partner_focal_person_alternate)
                            .replace("]',", "']  ||")
                            .replace("['", "")
                            .replace("]'", "")
                            .replace("|| '", "||")
                            .split("||")
                        )
                        for i in _partner_detail:
                            clean_data = (
                                str(i).replace("[", "").replace("]", "").split(",")
                            )
                            if clean_data[1]:
                                if re.search(regex, clean_data[1]):
                                    alternate_focal_user = (
                                        update_partner_agreement_member(
                                            clean_data,
                                            item,
                                            partners,
                                            partner_focal_role,
                                        )
                                    )
                    else:
                        logger.error(str(partner_detail))
                        errors.append(partner_detail)

                """
                Get UNHCR focal and alternet focal points and update to project agreement member
                
                """
                unhcr_focal_person = item[
                    "1 - PILOT E-mail of UNHCR Audit Focal Person Complete"
                ].split(",")
                if len(unhcr_focal_person) == 3:
                    _unhcr_focal_detail = (
                        str(unhcr_focal_person).replace("[", "").replace("]", "")
                    )
                    clean_data = [
                        i.replace("'", "").strip()
                        for i in _unhcr_focal_detail.split(",")
                    ]
                    unhcr_focal_user = update_unchr_agreement_member(
                        clean_data, item, unhcrRole
                    )

                elif len(unhcr_focal_person) > 5:
                    _unhcr_focal_detail = (
                        str(partner_focal_person)
                        .replace("]',", "']  ||")
                        .replace("['", "")
                        .replace("]'", "")
                        .replace("|| '", "||")
                        .split("||")
                    )
                    for i in _unhcr_focal_detail:
                        clean_data = str(i).replace("[", "").replace("]", "").split(",")
                        if clean_data[1]:
                            if re.search(regex, clean_data[1]):
                                unhcr_focal_user = update_unchr_agreement_member(
                                    clean_data, item, unhcrRole
                                )
                else:
                    logger.error(str(partner_detail))
                    errors.append(partner_detail)

                unhcr_focal_person_alternate = item[
                    "2 - PILOT  E-mail of UNHCR Alternate Audit Focal Person"
                ].split(",")
                if len(unhcr_focal_person_alternate) == 3:
                    _unhcr_focal_alternate_detail = (
                        str(unhcr_focal_person_alternate)
                        .replace("[", "")
                        .replace("]", "")
                    )
                    clean_data = [
                        i.replace("'", "").strip()
                        for i in _unhcr_focal_alternate_detail.split(",")
                    ]
                    if clean_data[1]:
                        if re.search(regex, clean_data[1]):
                            unhcr_alternate_focal_user = update_unchr_agreement_member(
                                clean_data, item, unhcrRole
                            )

                elif len(unhcr_focal_person_alternate) > 5:
                    _unhcr_focal_alternate_detail = (
                        str(unhcr_focal_person_alternate)
                        .replace("]',", "']  ||")
                        .replace("['", "")
                        .replace("]'", "")
                        .replace("|| '", "||")
                        .split("||")
                    )
                    for i in _unhcr_focal_alternate_detail:
                        clean_data = str(i).replace("[", "").replace("]", "").split(",")
                        if clean_data[1]:
                            if re.search(regex, clean_data[1]):
                                unhcr_alternate_focal_user = (
                                    update_unchr_agreement_member(
                                        clean_data, item, unhcrRole
                                    )
                                )
                else:
                    logger.error(str(partner_detail))
                    errors.append(partner_detail)

                auditor_name = item["Assigned Auditors "].strip()
                auditor, created = AuditAgency.objects.get_or_create(
                    legal_name=auditor_name
                )
                validated_data_partner = {}
                if focal_user:
                    validated_data_partner["request_type"] = "partner"
                    validated_data_partner["focal"] = focal_user.id
                    validated_data_partner["focal_alternate"] = alternate_focal_user.id
                    validated_data_partner["firm"] = auditor.id
                    assign_agreement_member(
                        validated_data_partner, [agreement.last().id]
                    )

                validated_data_unhcr = {}
                if unhcr_focal_user:
                    validated_data_unhcr["request_type"] = "unhcr"
                    validated_data_unhcr["focal"] = unhcr_focal_user.id
                    validated_data_unhcr[
                        "focal_alternate"
                    ] = unhcr_alternate_focal_user.id
                    validated_data_unhcr["firm"] = auditor.id
                    assign_agreement_member(validated_data_unhcr, [agreement.last().id])

                agreement.update(
                    fin_sense_check_id=sense_check.id,
                    audit_firm_id=auditor.id,
                    country_id=country_id,
                    status=status.id,
                )

            else:
                logger.error(str(agreement))
                errors.append(agreement)

    return errors


def import_user_accounts():

    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "TEMPLATE_USER_ACCOUNT.xlsx")
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)
    keys = []
    errors = []

    for idx_j in range(sheet.ncols):
        keys.append(sheet.cell(0, idx_j).value)
    for idx_i in range(1, sheet.nrows):
        item = {}
        for idx_j in range(sheet.ncols):
            item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value

        organization = item["Organization"].strip()
        Fullname = item["Fullname"].strip()
        email = item["Email"].strip()
        UserType = item["UserType"].strip()
        UserRole = item["UserRole"].strip()
        if REAL_EMAIL == False or REAL_EMAIL == "False":
            email = "{0}@{1}".format(email.split("@")[0], "example.com")

        if UserType == "HQ":
            role = UNHCRRole.objects.get(role=UserRole)
            user, created = User.objects.get_or_create(email=email)
            user.fullname = Fullname
            password = "Passw0rd!"
            user.set_password(password)
            user.save()
            unhcr_member_check = UNHCRMember.objects.filter(
                role__role=UserRole, user_id=user.id
            )
            if unhcr_member_check.exists() == False:
                unhcr_member = UNHCRMember(
                    user=user, role=role, date_send_invitation=timezone.now().date()
                )
                unhcr_member.save()
                update_user_role("unhcrmember", unhcr_member)
            else:
                logger.error(str(unhcr_member))
                errors.append(unhcr_member)

        elif UserType == "AUDITOR":
            auditor, auditor_created = AuditAgency.objects.get_or_create(
                legal_name=organization
            )
            user, created = User.objects.get_or_create(email=email)
            user.fullname = Fullname
            password = "Passw0rd!"
            user.set_password(password)
            user.save()
            auditor_member_check = AuditMember.objects.filter(
                audit_agency_id=auditor.id, role__role=UserRole, user_id=user.id
            )
            if auditor_member_check.exists() == False:
                role = AuditRole.objects.get(role=UserRole)
                auditor_member = AuditMember(
                    user=user,
                    role=role,
                    audit_agency=auditor,
                    date_send_invitation=timezone.now().date(),
                )
                auditor_member.save()
                update_user_role("auditmember", auditor_member)
            else:
                logger.error(str(auditor_member))
                errors.append(auditor_member)

        elif UserType == "FO":
            field_office, field_office_created = FieldOffice.objects.get_or_create(
                name=organization
            )
            user, created = User.objects.get_or_create(email=email)
            user.fullname = Fullname
            password = "Passw0rd!"
            user.set_password(password)
            user.save()
            field_member_check = FieldMember.objects.filter(
                field_office_id=field_office.id, role__role=UserRole, user_id=user.id
            )
            if field_member_check.exists() == False:
                role = FieldRole.objects.get(role=UserRole)
                field_member = FieldMember(
                    user=user,
                    role=role,
                    field_office=field_office,
                    date_send_invitation=timezone.now().date(),
                )
                field_member.save()
                update_user_role("fieldmember", field_member)
            else:
                logger.error(str(field_member))
                errors.append(field_member)

        elif UserType == "PARTNER":
            partner, partner_created = Partner.objects.get_or_create(
                legal_name=organization
            )
            user, created = User.objects.get_or_create(email=email)
            user.fullname = Fullname
            password = "Passw0rd!"
            user.set_password(password)
            user.save()
            partner_member_check = PartnerMember.objects.filter(
                partner_id=partner.id, role__role=UserRole, user_id=user.id
            )
            if partner_member_check.exists() == False:
                role = PartnerRole.objects.get(role=UserRole)
                partner_member = PartnerMember(
                    user=user,
                    role=role,
                    partner=partner,
                    date_send_invitation=timezone.now().date(),
                )
                partner_member.save()
                update_user_role("partnermember", partner_member)
            else:
                logger.error(str(partner_member))
                errors.append(partner_member)

    return errors


def import_audit_group():
    cwd = os.getcwd()
    xls = os.path.join(cwd, "MSRP", "Audit Worksplit Groups.xlsx")
    wb = xlrd.open_workbook(xls)
    sheet = wb.sheet_by_index(0)
    item = {}
    for idx_i in range(0, sheet.ncols):
        keys = []
        i = 1
        for i in range(sheet.nrows):
            if (
                sheet.cell_value(i, idx_i)
                and sheet.cell_value(i, idx_i) != sheet.cell(0, idx_i).value
            ):
                row_value = sheet.cell_value(i, idx_i)
                business_unit = BusinessUnit.objects.filter(code=row_value)
                if business_unit.exists():
                    bu_id = business_unit.last().id
                    keys.append(bu_id)
        item[sheet.cell(0, idx_i).value] = keys

        for i in item:
            WorkSplitGroupSerializer.create
            data = {"name": i, "business_units": item[i]}
            serializer = WorkSplitGroupSerializer(data=data)
            if serializer.is_valid():
                serializer.save()


def import_oios_bu_report():
    print("Importing OIOS BU Report")
    cwd = os.getcwd()
    files = ["2019 - Selection of Projects for Audit.xlsx"]
    for file in files:
        xls = os.path.join(cwd, "MSRP", file)
        wb = xlrd.open_workbook(xls)
        oios_sheet = (
            "OIOS Score - Sept 2019" if "2019" in file else "OIOS Score - Sept 2018"
        )
        sheet = wb.sheet_by_name(oios_sheet)
        for i in range(1, sheet.nrows):
            try:
                if "2019" in file:
                    business_unit = sheet.cell_value(i, 1)
                    country = sheet.cell_value(i, 2)
                    overall_rating = sheet.cell_value(i, 4)
                    report_date = "2019-09-01"
                elif "2018" in file:
                    business_unit = sheet.cell_value(i + 3, 8)
                    overall_rating = sheet.cell_value(i + 3, 11)
                    report_date = "2018-09-01"

                if len(business_unit) > 0:
                    business_unit_qs = BusinessUnit.objects.filter(code=business_unit)
                    if business_unit_qs.exists() == False:
                        field_office, _created = FieldOffice.objects.get_or_create(
                            name=country
                        )
                        business_unit, _ = BusinessUnit.objects.get_or_create(
                            code=business_unit,
                            name=country,
                            field_office=field_office,
                        )
                    else:
                        business_unit = business_unit_qs.first()
                    oios_report = OIOSBusinessUnitReport.objects.create(
                        business_unit=business_unit,
                        overall_rating=overall_rating,
                        report_date=report_date,
                    )
            except Exception as e:
                print("Error", e, business_unit)
                print(business_unit, "business_unit")


def import_icq_report():
    print("Importing ICQ report")
    cwd = os.getcwd()
    file = "icq_consolidated.xlsx"
    xls = os.path.join(cwd, "audit", "applications", "icq", "excel", file)
    wb = xlrd.open_workbook(xls)
    icq_sheet = "ICQ"
    sheet = wb.sheet_by_name(icq_sheet)
    for i in range(2, sheet.nrows):
        try:
            partner_number = sheet.cell_value(i, 0)
            partner_number = (
                str(int(partner_number))
                if str(partner_number).endswith(".0")
                else str(partner_number)
            )
            bu = sheet.cell_value(i, 2)
            implementer = partner_number.replace(str(bu), "")
            score = sheet.cell_value(i, 3)
            year = int(sheet.cell_value(i, 4))
            partner = Partner.objects.filter(number=implementer)
            bu = BusinessUnit.objects.filter(code=bu)
            bu = bu.first() if bu.exists() else None

            if partner.exists():
                icq = ICQReport.objects.create(
                    partner=partner.first(),
                    overall_score_perc=score,
                    user_id=1,
                    year=year,
                    business_unit=bu,
                    report_date="{0}-01-01".format(year),
                )
        except Exception as e:
            print(e)


def pqp_status():
    print("Importing PQP status")
    cwd = os.getcwd()
    files = ["2019 - Selection of Projects for Audit.xlsx"]
    for file in files:
        xls = os.path.join(cwd, "MSRP", file)
        wb = xlrd.open_workbook(xls)
        pqp_sheet = "PQP"
        sheet = wb.sheet_by_name(pqp_sheet)
        for i in range(7, sheet.nrows):
            try:
                implementer = sheet.cell_value(i, 2)
                implementer = (
                    str(int(implementer))
                    if str(implementer).endswith(".0")
                    else str(implementer)
                )

                bu_implementer = sheet.cell_value(i, 3)
                # implementer = str(int(implementer)) if str(implementer).endswith(".0") else str(implementer)
                bus = BusinessUnit.objects.all().values_list("code", flat=True)
                matchind_bu = [i for i in bus if i in bu_implementer]
                if len(matchind_bu) == 1:
                    bu = matchind_bu[0]
                    nat_implementer = bu_implementer.replace(bu, "")
                else:
                    nat_implementer = ""
                    print(bu_implementer)

                if implementer != "":
                    partner = Partner.objects.filter(number=implementer)
                    if partner.exists():
                        # Wordwide
                        pqp_status, created = PQPStatus.objects.get_or_create(
                            # granted_date=granted_data,
                            # expiry_date=date_of_expiry,
                            partner=partner.first(),
                            national_worldwide="W",
                        )
                if nat_implementer != "":
                    # Nation wide
                    partner = Partner.objects.filter(number=nat_implementer)
                    if partner.exists():
                        pqp_status, created = PQPStatus.objects.get_or_create(
                            # granted_date=granted_data,
                            # expiry_date=date_of_expiry,
                            partner=partner.first(),
                            national_worldwide="N",
                        )
                # Nationwide
            except Exception as e:
                print(e)


def import_partner_modified():
    print("Importing Partner Modified")
    cwd = os.getcwd()
    files = ["2019 - Selection of Projects for Audit.xlsx"]
    for file in files:
        xls = os.path.join(cwd, "MSRP", file)
        wb = xlrd.open_workbook(xls)
        pqp_sheet = "Modified 2015 to 2018"
        sheet = wb.sheet_by_name(pqp_sheet)
        for i in range(1, sheet.nrows):
            try:
                bu_partner_id = sheet.cell_value(i, 0)
                if bu_partner_id == "":
                    continue

                year = int(sheet.cell_value(i, 2))
                modified = int(sheet.cell_value(i, 5))
                bu = sheet.cell_value(i, 3)
                partner_number = bu_partner_id.replace(bu, "")

                agreement_qs = Agreement.objects.filter(
                    partner__number=partner_number, business_unit__code=bu
                )

                partner_qs = Partner.objects.filter(number=partner_number)
                if agreement_qs.exists():
                    modifed, created = Modified.objects.get_or_create(
                        year=year,
                        modified=modified,
                        partner=agreement_qs.first().partner,
                        business_unit=agreement_qs.first().business_unit,
                        commissioned=False,
                    )
            except Exception as e:
                print("Error in import_partner_modified", e)


def import_field_assessments():
    agreements = Agreement.objects.all()
    for agreement in agreements:
        field_assessment = FieldAssessment.objects.create()
        agreement.field_assessment = field_assessment
        agreement.save()


def import_project_cycle():
    years = [2016, 2017, 2018, 2019]
    for year in years:
        Cycle.objects.create(year=year, budget_ref=year, status_id=1)


def update_agreement_budget():
    years = [2018, 2019]
    for year in years:
        agreements = Agreement.objects.filter(cycle__year=year)
        for agreement in agreements:
            budget_qs = AgreementBudget.objects.filter(agreement=agreement, year=year)
            report_qs = AgreementReport.objects.filter(agreement=agreement)
            budget = budget_qs.aggregate(Sum("budget"))["budget__sum"]
            installments = report_qs.aggregate(Sum("installments"))["installments__sum"]
            agreement.budget = budget
            agreement.installments_paid = installments
            agreement.save()


def create_test_users():

    user_types = ["partner", "unhcr", "audit", "fo"]
    default_email = config("DEFAULT_EMAIL_TO_SEND")

    def create_user(name, email):
        user, created = User.objects.get_or_create(email=email)
        user.fullname = name
        user.set_password("Password@123")
        user.save()
        return user

    data = default_email.split("@")

    for user_type in user_types:
        if user_type == "partner":
            for role in PartnerRole.objects.all().values_list("role", flat=True):
                email = "{0}-{1}-{2}@{3}".format(
                    data[0], user_type, role.lower(), "example.com"
                )
                user = create_user(data[0], email)

                role = PartnerRole.objects.get(role=role)
                partner_member = PartnerMember(
                    user=user,
                    role=role,
                    partner_id=1,
                    date_send_invitation=timezone.now().date(),
                )
                partner_member.save()
                update_user_role("partnermember", partner_member)
        elif user_type == "unhcr":
            for role in UNRole.objects.all().values_list("role", flat=True):
                email = "{0}-{1}-{2}@{3}".format(
                    data[0], user_type, role.lower(), "example.com"
                )
                user = create_user(data[0], email)

                role = UNRole.objects.get(role=role)
                un_member = UNHCRMember(
                    user=user, role=role, date_send_invitation=timezone.now().date()
                )
                un_member.save()
                update_user_role("unhcrmember", un_member)
        elif user_type == "audit":
            for role in AuditRole.objects.all().values_list("role", flat=True):
                email = "{0}-{1}-{2}@{3}".format(
                    data[0], user_type, role.lower(), "example.com"
                )
                user = create_user(data[0], email)

                role = AuditRole.objects.get(role=role)
                auditor_member = AuditMember(
                    user=user,
                    role=role,
                    audit_agency_id=1,
                    date_send_invitation=timezone.now().date(),
                )
                auditor_member.save()
                update_user_role("auditmember", auditor_member)
        elif user_type == "fo":
            for role in RoleField.objects.all().values_list("role", flat=True):
                email = "{0}-{1}-{2}@{3}".format(
                    data[0], user_type, role.lower(), "example.com"
                )
                user = create_user(data[0], email)

                role = RoleField.objects.get(role=role)
                fo_member = FieldMember(
                    user=user,
                    role=role,
                    field_office_id=44,
                    date_send_invitation=timezone.now().date(),
                )
                fo_member.save()
                update_user_role("fieldmember", fo_member)

    print("Test users created sucessfully for {0}".format(data[0]))


def goal_fixes():
    file_path = os.path.join(
        os.getcwd(), "audit", "applications", "project", "excel", "rating.xlsx"
    )
    wb = xlrd.open_workbook(file_path)
    sheet = wb.sheet_by_name("rating")
    for idx_i in range(3, sheet.nrows):
        business_unit = sheet.cell(idx_i, 2).value
        number = sheet.cell(idx_i, 11).value

        goal = sheet.cell(idx_i, 21).value
        goal_list = goal.split()
        if number != "" or business_unit != "":
            if business_unit == "JORMN" or business_unit == "UNHCR":
                agreement = Agreement.objects.filter(
                    business_unit__code__icontains=business_unit, number=number
                ).first()
            else:
                agreement = Agreement.objects.filter(
                    business_unit__code=business_unit, number=number
                ).first()

            if agreement is not None:
                for g in goal_list:
                    g = g.replace(",", "") if "," in g else g
                    agreement_goal = agreement.goals.filter(code=g)
                    if agreement_goal.exists() == False:
                        goal_obj = Goal.objects.filter(code=g).first()
                        if goal_obj is not None:
                            agreement.goals.add(goal_obj)
                agreement.save()


def icq_report_patch():
    """ Importing ICQ data from 2015 - 2019 """

    # file_path = os.path.join(os.getcwd(), 'MSRP', 'production', 'icq_consolidated.xlsx')
    file_path = os.path.join(
        os.getcwd(), "audit", "applications", "icq", "excel", "icq_consolidated.xlsx"
    )
    wb = xlrd.open_workbook(file_path)
    icq_sheet = "ICQ"
    sheet = wb.sheet_by_name(icq_sheet)
    for i in range(2, sheet.nrows):
        try:
            bu = sheet.cell_value(i, 2)
            implementer = sheet.cell_value(i, 0)
            implementer = (
                str(int(implementer))
                if str(implementer).endswith(".0")
                else str(implementer)
            )
            partner_implementer = implementer.replace(bu, "")
            parner_legal_name = sheet.cell_value(i, 1)
            score = sheet.cell_value(i, 3)
            year = int(sheet.cell_value(i, 4))

            business_unit = BusinessUnit.objects.filter(code=bu.upper()).first()
            partner = Partner.objects.filter(
                number__icontains=partner_implementer.strip()
            ).first()
            if partner is None:
                partner, created = Partner.objects.get_or_create(
                    number=partner_implementer, legal_name=parner_legal_name
                )

            if business_unit is not None:
                report, created = ICQReport.objects.get_or_create(
                    business_unit=business_unit, partner=partner, year=year
                )
                report.overall_score_perc = score
                report.save()
        except Exception as e:
            print(e)


def partner_modified_patch():
    file_path = os.path.join(
        os.getcwd(),
        "audit",
        "applications",
        "partner",
        "excel",
        "partner_modified_2016_to_2019.xlsx",
    )
    wb = xlrd.open_workbook(file_path)
    icq_sheet = "Modified"
    sheet = wb.sheet_by_name(icq_sheet)
    for i in range(2, sheet.nrows):
        try:
            bu_pa = sheet.cell_value(i, 0)
            bu = sheet.cell_value(i, 3)
            year = sheet.cell_value(i, 2)
            is_modified = sheet.cell_value(i, 5)
            implementer_num = bu_pa.replace(bu, "")
            modified = Modified.objects.filter(
                business_unit__code=bu, partner__number=implementer_num
            ).first()
            if modified is None:
                business_unit = BusinessUnit.objects.filter(code=bu).first()
                partner = Partner.objects.filter(number=implementer_num).first()
                if business_unit and partner:
                    modified = Modified.objects.create(
                        year=year,
                        modified=True,
                        commissioned=True,
                        business_unit=business_unit,
                        partner=partner,
                    )
            else:
                modified.modified = True
                modified.save()
        except Exception as e:
            print(e)


def oios_patch():
    file_path = os.path.join(
        os.getcwd(), "audit", "applications", "project", "excel", "oios_2020.xlsx"
    )
    wb = xlrd.open_workbook(file_path)
    icq_sheet = "oios_2020"
    sheet = wb.sheet_by_name(icq_sheet)
    for i in range(2, sheet.nrows):
        try:
            bu = sheet.cell_value(i, 1)
            overall_rating = round(sheet.cell_value(i, 4), 2)
            business_unit = BusinessUnit.objects.filter(code=bu).first()

            if business_unit is not None:
                oios = OIOSBusinessUnitReport.objects.filter(
                    business_unit=business_unit
                ).first()
                if oios is not None:
                    oios.overall_rating = overall_rating
                    oios.save()
                else:
                    OIOSBusinessUnitReport.objects.create(
                        business_unit=business_unit,
                        overall_rating=overall_rating,
                        report_date="2020-11-01",
                    )
        except Exception as e:
            print(e)


def pqp_fixes():
    file_path = os.path.join(
        os.getcwd(), "audit", "applications", "project", "excel", "pqp.xlsx"
    )
    wb = xlrd.open_workbook(file_path)
    icq_sheet = "pqp"
    sheet = wb.sheet_by_name(icq_sheet)
    for i in range(1, sheet.nrows):
        try:
            bu = sheet.cell_value(i, 2)
            number = int(sheet.cell_value(i, 4))
            pqp = sheet.cell_value(i, 5)

            if number != "" or bu != "":
                if bu == "JORMN" or bu == "UNHCR":
                    agreement = Agreement.objects.filter(
                        business_unit__code__icontains=bu, number=number
                    ).first()
                else:
                    agreement = Agreement.objects.filter(
                        business_unit__code=bu, number=number
                    ).first()

            if agreement is not None:
                pqp_obj, created = PQPStatusNew.objects.get_or_create(
                    agreement=agreement,
                    business_unit=agreement.business_unit,
                    partner=agreement.partner,
                    year=2020,
                )
                pqp_obj.status = pqp
                pqp_obj.save()
            else:
                continue
        except Exception as e:
            print(e)
