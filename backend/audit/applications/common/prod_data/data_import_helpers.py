import os
import json
import logging
import requests
from decouple import config
from datetime import datetime
from django.db.models import Q, Sum
from requests.auth import HTTPBasicAuth
from common.models import CostCenter, Country, BusinessUnit, Area, Region, Comment
from partner.models import (
    ImplementerType,
    Partner,
    Modified,
    PartnerMember,
    PQPStatus,
    Role as RolePartner,
)
from project.models import (
    Pillar,
    Vendor,
    Operation,
    Goal,
    PopulationPlanningGroup,
    Situation,
    AgreementType,
    Cycle,
    Agreement,
    AgreementReport,
    Output,
    AgreementBudget,
    Account,
    AccountCategory,
    AccountType,
    Currency,
    SenseCheck,
    Status,
    OIOSBusinessUnitReport,
    SelectionSettings,
)
from icq.models import ICQReport
from field.models import FieldOffice, FieldAssessment
from common.consts import AGREEMENT_MEMBER_CHOICES, ACCOUNT_STATUSES, YES_NO, PRE01

logger = logging.getLogger("console")


# def fetch_antirio_data(data_set, year):
#     base_url = "https://aiapi.unhcr.org/Antirrio/dbtorest/api/AUDITMODULE/v1/params/flat?"
#     username = config('ANTIRRIO_USERNAME')
#     password = config('ANTIRRIO_PASSWORD')
#     querystr = "query={0}&REF_YEAR={1}".format(data_set, year)
#     url = base_url + querystr
#     auth = HTTPBasicAuth(username, password)
#     session = requests.Session()
#     session.auth = auth
#     r = session.get(url, timeout=1000)

#     if r.status_code == 200:
#         return r.json()
#     else:
#         if data_set == "HNIP998":
#             with open(os.path.join(os.getcwd(), 'MSRP', 'production', 'HNIP998.json')) as fh:
#                 data = fh.read()
#                 data = json.loads(data)
#                 return data
#         return []


def fetch_antirio_data(data_set, year):

    with open(
        os.path.join(os.getcwd(), "MSRP", "production", "{0}.json".format(data_set))
    ) as fh:
        data = fh.read()
        data = json.loads(data)
        return data


def normalize_date(date):
    formatted_date = datetime.strptime(date, "%Y-%m-%d 00:00:00.0")
    formatted_date = datetime.strftime(formatted_date, "%Y-%m-%d")
    return formatted_date


def util_decimal(item):
    if type(item) is str:
        _ = item.strip()
        if _ in [""]:
            return 0
        else:
            return float(_)
    return item


def is_None(value):
    if isinstance(value, int):
        return False
    elif isinstance(value, float):
        return False
    elif value.strip() not in [
        "",
    ]:
        return False
    return True


def parse_HNIP999(items):
    agreements = []
    for item in items:
        agreements.append([item["BUSINESS_UNIT"], int(item["HCR_SA_ID"])])
    return agreements


def filter_HNIP998(items, agreements=None):
    if agreements is None:
        res = items
    else:
        res = []
        for item in items:
            if [item["BUSINESS_UNIT"], int(item["HCR_SA_ID"])] in agreements:
                res.append(item)
    return res


def import_implementer_types(items):
    implementer_types = []
    for item in items:
        try:
            if item["PARENT_NODE_NAME"] not in implementer_types:
                implementer_types.append(item["PARENT_NODE_NAME"])
                imp_type, created = ImplementerType.objects.get_or_create(
                    implementer_type=item["PARENT_NODE_NAME"],
                )
        except Exception as e:
            print(e)


def import_partners(items):
    partners = []
    for item in items:
        try:
            if item["OPERATING_UNIT"] not in partners:
                partners.append(item["OPERATING_UNIT"])
                implementer_type = ImplementerType.objects.get(
                    implementer_type=item["PARENT_NODE_NAME"],
                )
                partner, created = Partner.objects.get_or_create(
                    legal_name=item["HCR_DESCR"],
                    number=item["OPERATING_UNIT"],
                    implementer_type=implementer_type,
                )
        except Exception as e:
            print(e)
            logger.error(str(e))


def import_pillars(items):
    pillars = []
    for item in items:
        if item["HCR_PILLAR"] not in pillars:
            pillars.append(item["HCR_PILLAR"])
            Pillar.objects.get_or_create(
                code=item["HCR_PILLAR"],
                description=item["HCR_PILLAR_DESC"],
            )


def import_vendors(items):
    vendors = []
    for item in items:
        if item["VENDOR_ID"] not in vendors:
            vendors.append(item["VENDOR_ID"])
            Vendor.objects.get_or_create(
                code=item["VENDOR_ID"],
            )


def import_operations(items):
    operations = []
    for item in items:
        if item["HCR_OPERATION_CODE"] not in operations:
            operations.append(item["HCR_OPERATION_CODE"])
            Operation.objects.get_or_create(
                code=item["HCR_OPERATION_CODE"],
            )


def import_goals(items):
    goals = []
    for item in items:
        goal_list = item["HCR_GOALS_LIST"].split(", ")
        for goal in goal_list:
            if goal not in goals:
                goals.append(goal)
                Goal.objects.get_or_create(
                    code=goal,
                )


def import_cc_s(items):
    cc_s = []
    for item in items:
        cc_list = str(item["HCR_CC_LIST"]).split(", ")
        for cc in cc_list:
            cc = str(int(float(cc.replace(",", ""))))
            if cc not in cc_s:
                cc_s.append(cc)
                CostCenter.objects.get_or_create(code=cc)


def import_ppg_s(items):
    ppg_s = []
    for item in items:
        ppg_list = item["HCR_PPG_LIST"].split(", ")
        for ppg in ppg_list:
            if ppg not in ppg_s:
                ppg_s.append(ppg)
                PopulationPlanningGroup.objects.get_or_create(
                    code=ppg,
                )


def import_situations(items):
    situations = []
    for item in items:
        situation_list = item["HCR_SIT_LIST"].split(", ")
        for situation in situation_list:
            if situation not in situations:
                situations.append(situation)
                Situation.objects.get_or_create(
                    code=situation,
                )


def import_agreement_types(items):
    agreement_types = []
    for item in items:
        if item["DESCR"] not in agreement_types:
            agreement_types.append(item["DESCR"])
            AgreementType.objects.get_or_create(
                description=item["DESCR"],
            )


def import_budget_refs(items):
    budget_refs = []
    for item in items:
        if item["HCR_BUDGET_REF"] not in budget_refs:
            budget_refs.append(item["HCR_BUDGET_REF"])
            Cycle.objects.get_or_create(
                budget_ref=item["HCR_BUDGET_REF"],
                year=item["HCR_BUDGET_REF"],
                status_id=1,
            )


def import_selection_settings(items):
    budget_refs = []
    for item in items:
        if item["HCR_BUDGET_REF"] not in budget_refs:
            budget_refs.append(item["HCR_BUDGET_REF"])
            cycle = Cycle.objects.get(
                budget_ref=item["HCR_BUDGET_REF"],
                year=item["HCR_BUDGET_REF"],
            )

            data = {
                "cycle_id": cycle.id,
                "procurement_perc": "0.100",
                "cash_perc": "0.100",
                "staff_cost_perc": "0.100",
                "emergency_perc": "0.050",
                "audit_perc": "0.200",
                "oios_perc": "0.200",
                "field_assessment_perc": "0.250",
                "risk_threshold": "1.000",
            }
            SelectionSettings.objects.create(**data)


def import_agreements(items):
    errors = []
    for item in items:
        try:
            title = item["DESCR100"]
            agreement_type = AgreementType.objects.get(description=item["DESCR"])
            hq_bu_created = None
            if item["BUSINESS_UNIT"] in ["UNHCR", "JORMN"]:
                bu_name = item["BUSINESS_UNIT"] + " HQ - " + item["TREE_NODE_CHILD"]
                business_unit, hq_bu_created = BusinessUnit.objects.get_or_create(
                    code=bu_name, name=bu_name
                )

                if hq_bu_created is not None:
                    field_office, created = FieldOffice.objects.get_or_create(
                        name=bu_name
                    )
                    business_unit.field_office = field_office
                    business_unit.save()

            business_unit, created = BusinessUnit.objects.get_or_create(
                code=item["BUSINESS_UNIT"], name=item["BUSINESS_DESCR"]
            )
            if created:
                field_office = FieldOffice.objects.create(name=item["BUSINESS_DESCR"])
                business_unit.field_office = field_office
                business_unit.save()

            partner = Partner.objects.get(number=item["OPERATING_UNIT"])
            number = int(item["HCR_SA_ID"])
            operation = Operation.objects.get(code=item["HCR_OPERATION_CODE"])
            pillar = Pillar.objects.get(code=item["HCR_PILLAR"])
            start_date = normalize_date(item["HCR_SA_START_DT"])
            end_date = normalize_date(item["HCR_SA_COMPL_DT"])
            cycle = Cycle.objects.get(budget_ref=item["HCR_BUDGET_REF"])
            vendor = Vendor.objects.get(code=item["VENDOR_ID"])

            agreement, _created = Agreement.objects.get_or_create(
                title=title,
                agreement_type=agreement_type,
                business_unit=business_unit,
                partner=partner,
                number=number,
                operation=operation,
                pillar=pillar,
                start_date=start_date,
                end_date=end_date,
                cycle=cycle,
                vendor=vendor,
                status_id=PRE01,
            )

            goals_list = item["HCR_GOALS_LIST"].split(", ")
            for goal in goals_list:
                agreement.goals.add(Goal.objects.get(code=goal))
            cc_list = str(item["HCR_CC_LIST"]).split(", ")
            for cc in cc_list:
                cc = str(int(float(cc.replace(",", ""))))
                agreement.cost_centers.add(CostCenter.objects.get(code=cc))
            ppg_list = item["HCR_PPG_LIST"].split(", ")
            for ppg in ppg_list:
                agreement.ppg_s.add(PopulationPlanningGroup.objects.get(code=ppg))
            situations = item["HCR_SIT_LIST"].split(", ")
            for situation in situations:
                agreement.situations.add(Situation.objects.get(code=situation))

            agreement.save()
        except Exception as e:
            print(e)
            logger.error(str(e))
            errors.append(item)
    return errors


def import_currency(items):
    currencies = []
    for item in items:
        if item["BASE_CURRENCY"] not in currencies:
            currencies.append(item["BASE_CURRENCY"])
            if item["BASE_CURRENCY"].strip() not in [
                "",
            ]:
                Currency.objects.get_or_create(code=item["BASE_CURRENCY"])


def import_agreement_report(items):
    errors = []
    for item in items:
        try:
            business_unit, created = BusinessUnit.objects.get_or_create(
                code=item["BUSINESS_UNIT"], name=item["BUSINESS_DESCR"]
            )
            if created:
                field_office = FieldOffice.objects.create(name=item["BUSINESS_DESCR"])
                business_unit.field_office = field_office
                business_unit.save()

            number = int(item["HCR_SA_ID"])
            agreement = Agreement.objects.get(
                business_unit=business_unit, number=number
            )
            currency = None
            if item["BASE_CURRENCY"].strip() not in [
                "",
            ]:
                currency = Currency.objects.get(code=item["BASE_CURRENCY"])
            report_date = item["HCR_SA_START_DT"]
            base = util_decimal(item["HCR_BASE_AMT"])
            installments = util_decimal(item["HCR_IP_INSTALL_AMT"])
            adjustments = util_decimal(item["HCR_IP_ADJUST_AMT"])
            cancellations = util_decimal(item["HCR_IP_CANCEL_AMT"])
            ipfr = util_decimal(item["HCR_IP_IPFR_AMT"])
            refund = util_decimal(item["HCR_IP_REFUND_AMT"])
            receivables = util_decimal(item["HCR_IP_RECV_AMT"])
            write_off = util_decimal(item["HCR_IP_WRITOFF_AMT"])

            AgreementReport.objects.get_or_create(
                agreement=agreement,
                currency=currency,
                base=base,
                installments=installments,
                adjustments=adjustments,
                cancellations=cancellations,
                ipfr=ipfr,
                refund=refund,
                receivables=receivables,
                write_off=write_off,
            )
        except Exception as e:
            logger.error(str(e))
            errors.append(item)
    return errors


def import_outputs(items):
    outputs = []
    errors = []
    for item in items:
        try:
            if item["HCR_CLASS_FLD"] not in outputs:
                outputs.append(item["HCR_CLASS_FLD"])
                if item["HCR_CLASS_FLD"].strip() not in [
                    "",
                ]:
                    Output.objects.get_or_create(code=item["HCR_CLASS_FLD"])
        except Exception as e:
            logger.error(str(e))
            errors.append(item)
    return errors


def import_agreement_budgets(items):
    errors = []
    counter = 0
    error_counter = 0
    for item in items:
        try:
            business_unit, created = BusinessUnit.objects.get_or_create(
                code=item["BUSINESS_UNIT"], name=item["BUSINESS_DESCR"]
            )
            if created:
                field_office = FieldOffice.objects.create(name=item["BUSINESS_DESCR"])
                business_unit.field_office = field_office
                business_unit.save()

            number = int(item["HCR_SA_ID"])
            agreement_qs = Agreement.objects.filter(
                business_unit=business_unit, number=number
            )
            if agreement_qs.exists():
                agreement = agreement_qs.first()
                account = None
                account_row = (
                    None if is_None(item["HCR_ACCOUNT"]) else int(item["HCR_ACCOUNT"])
                )
                if not is_None(item["HCR_ACCOUNT"]):
                    account, _created = Account.objects.get_or_create(
                        code=item["HCR_ACCOUNT"]
                    )
                output = (
                    None
                    if is_None(item["HCR_CLASS_FLD"])
                    else Output.objects.get(code=item["HCR_CLASS_FLD"])
                )
                ppg = (
                    None
                    if is_None(item["HCR_PRODUCT"])
                    else PopulationPlanningGroup.objects.get(code=item["HCR_PRODUCT"])
                )
                situation = (
                    None
                    if is_None(item["HCR_CHARTFIELD1"])
                    else Situation.objects.get(code=int(item["HCR_CHARTFIELD1"]))
                )
                cost_center = (
                    None
                    if is_None(item["DEPTID"])
                    else CostCenter.objects.get(code=int(item["DEPTID"]))
                )
                pillar = (
                    None
                    if is_None(item["HCR_PILLAR"])
                    else Pillar.objects.get(code=item["HCR_PILLAR"])
                )
                goal = (
                    None
                    if is_None(item["HCR_GOAL"])
                    else Goal.objects.get(code=item["HCR_GOAL"])
                )
                budget = util_decimal(item["HCR_BASE_AMT"])
                report_date = normalize_date(item["DATE_TO"])
                AgreementBudget.objects.create(
                    agreement=agreement,
                    account=account,
                    output=output,
                    ppg=ppg,
                    situation=situation,
                    cost_center=cost_center,
                    goal=goal,
                    pillar=pillar,
                    budget=budget,
                    report_date=report_date,
                    year=int(item["HCR_BUDGET_REF"]),
                )
        except Exception as e:
            print("Error in import_agreement_budgets", e)
            logger.error(str(e))
            errors.append(item)
    return errors


def import_field_assessments():
    agreements = Agreement.objects.all()
    for agreement in agreements:
        if agreement.field_assessment is None:
            field_assessment = FieldAssessment.objects.create()
            agreement.field_assessment = field_assessment
            agreement.save()


def update_agreement_budget(year):
    agreements = Agreement.objects.filter(cycle__year=year)
    for agreement in agreements:
        budget_qs = AgreementBudget.objects.filter(agreement=agreement, year=year)
        report_qs = AgreementReport.objects.filter(agreement=agreement)
        budget = budget_qs.aggregate(Sum("budget"))["budget__sum"]
        installments = report_qs.aggregate(Sum("installments"))["installments__sum"]
        agreement.budget = budget
        agreement.installments_paid = installments
        agreement.save()
