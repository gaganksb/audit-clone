from rest_framework import serializers
from django.urls import reverse
from django.http import HttpRequest
from notify.models import Notification
from account.models import User
from common.models import (
    Point,
    Region,
    Country,
    Area,
    BusinessUnit,
    Permission,
    CommonFile,
    Comment,
)
from background_task.models import Task
from background_task.models_completed import CompletedTask


class PointSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.ReadOnlyField()
    url = serializers.HyperlinkedIdentityField(view_name="common:point-detail")

    class Meta:
        model = Point
        fields = "__all__"
        extra_fields = [
            "id",
            "url",
        ]


class AreaSerializer(serializers.ModelSerializer):
    # url = serializers.HyperlinkedIdentityField(view_name="common:area-detail")

    class Meta:
        model = Area
        fields = ("code", "name")
        # extra_fields = [
        #     "url",
        # ]


class RegionSerializer(serializers.ModelSerializer):
    area = AreaSerializer(read_only=True)

    class Meta:
        model = Region
        fields = ("code", "name", "area")


class CountrySerializer(serializers.ModelSerializer):
    region = RegionSerializer()

    class Meta:
        model = Country
        fields = ("id", "code", "name", "region")


class BusinessUnitSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name="common:businessunit-detail")
    countries = CountrySerializer(read_only=True, many=True)
    field_office = serializers.SerializerMethodField()

    class Meta:
        model = BusinessUnit
        fields = "__all__"
        extra_fields = [
            "url",
        ]

    def get_field_office(self, instance):
        field_office = instance.field_office.__dict__
        return {
            "id": field_office['id'],
            "name": field_office['name'],
        }


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = "__all__"


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = "__all__"


class CommonUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "email", "fullname"]


class CommonFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommonFile

    def to_representation(self, instance):
        response = {
            "common_file_id": instance.id,
            "filepath": instance.file_field.url,
            "filename": instance.file_field.name,
            "created_date": instance.created,
            "user": None
            if instance.user is None
            else {
                "id": instance.user.id,
                "email": instance.user.email,
                "fullname": instance.user.fullname,
            },
        }
        return response


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class CommonCommentSerializer(serializers.ModelSerializer):
    user = CommonUserSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"


class BackgroundTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = "__all__"


class BackgroundCompletedTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompletedTask
        fields = "__all__"
