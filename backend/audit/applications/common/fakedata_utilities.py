import random
from random import randint
from django.conf import settings

from account.models import User, UserTypeRole
from field.models import FieldOffice, FieldMember, Role as RoleField
from unhcr.models import *

from .factories import *

from partner.data import import_icq, import_pqp, import_modified, create_partner_roles
from project.data import (
    import_oios,
    import_situation,
    generate_preliminary_list,
    import_preliminary_list,
)

from common.data import import_bu, create_permissions
from auditing.data import create_audit_roles
from field.enums import FieldRoleEnum
from field.data import import_field_offices, create_field_roles
from unhcr.data import create_unhcr_roles
from unhcr.enums import UNHCRRoleEnum

n_audit_agencies = 4
n_audit_roles = 3
n_work_groups = 3

n_partners = 4
n_partner_roles = 3

n_unhcr_roles = 3

n = 50

HQ_TEST_ACCOUNTS = [
    {
        "fullname": "Fake HQ Preparer",
        "email": "hq-preparer@unhcr.org",
        "role": UNHCRRoleEnum.PREPARER.name,
    },
    {
        "fullname": "Fake HQ Reviewer",
        "email": "hq-reviewer@unhcr.org",
        "role": UNHCRRoleEnum.REVIEWER.name,
    },
    {
        "fullname": "Fake HQ Approver",
        "email": "hq-approver@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Deogratias Habimana",
        "email": "habimade@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Robert Berenyi",
        "email": "berenyi@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Gianluca Nuzzo",
        "email": "nuzzo@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Anjali Upreti",
        "email": "upreti@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Francesco Sasso",
        "email": "sasso@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Reddy Vishal",
        "email": "reddy@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
]

FO_TEST_ACCOUNTS = [
    {
        "fullname": "Fake FO Reviewer",
        "email": "fo-reviewer@unhcr.org",
        "role": FieldRoleEnum.REVIEWER.name,
    },
    {
        "fullname": "Fake FO Approver",
        "email": "fo-approver@unhcr.org",
        "role": FieldRoleEnum.APPROVER.name,
    },
]


def generate_fake_data():
    create_permissions()
    create_field_roles()
    create_partner_roles()
    create_unhcr_roles()
    create_audit_roles()

    admin, created = User.objects.get_or_create(
        fullname="Admin",
        defaults={
            "email": "admin@unhcr.org",
            "is_superuser": True,
            "is_staff": True,
        },
    )
    password = "Passw0rd!"
    admin.set_password(password)
    admin.save()
    print("Superuser {}: {}/{}".format("created", admin.email, password))

    for tester in HQ_TEST_ACCOUNTS:
        user, created = User.objects.get_or_create(
            fullname=tester["fullname"], email=tester["email"]
        )
        password = "Password@123"
        user.set_password(password)
        user.save()
        role = Role.objects.get(role=tester["role"])
        UNHCRMember.objects.get_or_create(role=role, user=user)

    # for _ in range(n_audit_agencies):
    #     audit_agency = AuditAgencyFactory()
    #     for _ in range(n_audit_roles):
    #         AuditMemberFactory(audit_agency=audit_agency)
    # print('Audit models created')

    # import FieldOffice data from excel
    import_field_offices()
    print("Field Offices created")

    field_offices = FieldOffice.objects.all()
    field_roles = RoleField.objects.all()
    for field_office in field_offices:
        for role in field_roles:
            FieldMemberFactory(field_office=field_office, role=role)

    field_office = field_offices[0]
    for tester in FO_TEST_ACCOUNTS:
        user, created = User.objects.get_or_create(
            fullname=tester["fullname"], email=tester["email"]
        )
        password = "Password@123"
        user.set_password(password)
        user.save()
        role = RoleField.objects.get(role=tester["role"])
        FieldMember.objects.get_or_create(
            role=role, user=user, field_office=field_office
        )
    print("Office members created")

    for _ in range(n_unhcr_roles):
        UNHCRMemberFactory()
    print("UNHCR models created")

    qs = UserTypeRole.objects.all()
    qs.update(is_default=True)

    # import ICQ data from excel
    years = [2014, 2015, 2016, 2017]
    for year in years:
        import_icq(year)
        print("ICQ Imported. Year " + str(year))

    # import BU from excel
    import_bu()
    print("BU Imported")


def generate_projects():
    # import OIOS data from excel
    import_oios()
    print("OIOS Imported")

    # import PQP data from excel
    import_pqp()
    print("PQP Imported")

    # import Modified from excel
    import_modified()
    print("Modified Imported")

    # import Situation from excel
    import_situation()
    print("Situation Imported")


def generate_preliminarylist():
    if settings.ENV == "local":
        years = [2014, 2015, 2016, 2017, 2018, 2019]
    else:
        years = [2014, 2015, 2016, 2017, 2018, 2019]
    # import PreliminaryList from excel
    for year in years:
        generate_preliminary_list(year)
        import_preliminary_list(year)
        print("PreliminaryList Imported. Year " + str(year))
