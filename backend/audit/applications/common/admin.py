from django.contrib import admin
from .models import (
    Point,
    Country,
    Region,
    Area,
    CommonFile,
    ExceptionLog,
    BusinessUnit,
    Permission,
    CostCenter,
    Currency,
    Comment,
)


class PointAdmin(admin.ModelAdmin):
    pass


class CountryAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("code", "name", "region")
    ordering = ("code",)


class RegionAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("code", "name")
    ordering = ("code",)


class AreaAdmin(admin.ModelAdmin):
    search_fields = ("code", "name")
    list_display = ("code", "name")
    ordering = ("code",)


class CommonFileAdmin(admin.ModelAdmin):
    search_fields = ("file_field",)


class BusinessUnitAdmin(admin.ModelAdmin):
    search_fields = ("code", "code", "name", "field_office__name")
    list_display = ("id", "code", "name", "field_office")


class ExceptionLogAdmin(admin.ModelAdmin):
    search_fields = ("url",)
    list_display = ("url", "counter", "solved")
    list_filter = ("solved",)
    ordering = ("url", "counter", "solved")


admin.site.register(Point, PointAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(CommonFile, CommonFileAdmin)
admin.site.register(ExceptionLog, ExceptionLogAdmin)
admin.site.register(BusinessUnit, BusinessUnitAdmin)
admin.site.register(Permission)
admin.site.register(CostCenter)
admin.site.register(Currency)
admin.site.register(Comment)
