USERS = [
    {"fullname": "Agnes CHERONO", "email": "cherono@unhcr.org", "role": "Approver"},
    {"fullname": "Sandra HU", "email": "hus@unhcr.org", "role": "Approver"},
    {"fullname": "Abera Mergia Heye ", "email": "heyea@unhcr.org", "role": "Approver"},
    {"fullname": "Andrea Chavez", "email": "chavez@unhcr.org", "role": "Approver"},
    {
        "fullname": "Kateryna Kuritsyna",
        "email": "kuritsyn@unhcr.org",
        "role": "Approver",
    },
    {"fullname": "Gabriel Munene ", "email": "muneneg@unhcr.org", "role": "Approver"},
    {"fullname": "Ali Penalver ", "email": "PEALVER@unhcr.org", "role": "Approver"},
    {"fullname": "Mohamad Maarouf", "email": "maaroufm@unhcr.org", "role": "Approver"},
    {
        "fullname": "Alireza Bakhshayesh",
        "email": "bakhshaa@unhcr.org",
        "role": "Approver",
    },
    {"fullname": "Bokar Keita", "email": "keitab@unhcr.org", "role": "Approver"},
    {
        "fullname": "Kasonga Aime Muanza",
        "email": "muanza@unhcr.org",
        "role": "Approver",
    },
]
