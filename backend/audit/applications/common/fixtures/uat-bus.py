BUS = [
    {
        "name": "Kenya",
        "code": "KEN01",
    },
    {
        "name": "DRC",
        "code": "COD01",
    },
    {"name": "Bangladesh", "code": "BGD01"},
    {"name": "Ecuador", "code": "ECU01"},
    {"name": "Ukraine", "code": "UKR01"},
    {"name": "Costa Rica", "code": "CRI01"},
    {"name": "MCO Panama", "code": "PAN01"},
    {"name": "Lybia", "code": "LBY01"},
    {"name": "Washington", "code": "USA01"},
    {"name": "Chad", "code": "TCD01"},
    {"name": "Senegal", "code": "SEN01"},
]
