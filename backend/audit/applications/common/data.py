import csv
import datetime
from django.conf import settings
from common.models import BusinessUnit, Country, Permission
from common.enums import PermissionListEnum


def create_permissions():
    for permission in PermissionListEnum:
        Permission.objects.get_or_create(permission=permission.name)


def import_bu():
    file_path = settings.PROJECT_ROOT + "/../../excel_data/HNIP999/HNIP999.csv"
    country_iso = settings.PROJECT_ROOT + "/../../excel_data/country-iso.csv"

    with open(country_iso, "r") as f:
        next(f)
        reader = csv.reader(f, delimiter=",")
        countries = []
        alpha_3 = []
        for data in reader:
            countries.append(data[0])
            alpha_3.append(data[2])

    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f, delimiter=",")
        bu = list(BusinessUnit.objects.all().values_list("code", flat=True))
        not_in_bu = []
        for data in reader:
            if data[0] not in bu and data[0] != "":
                bu.append(data[0])
                not_in_bu.append(data[0])
        for _ in not_in_bu:
            try:
                country = Country.objects.get(code=_[0:3])
                business_unit = BusinessUnit.objects.create(code=_, name=country.name)
            except Exception as error:
                try:
                    name = countries[alpha_3.index(_[0:3])]
                    code = alpha_3[alpha_3.index(_[0:3])]
                    country = Country.objects.create(code=code, name=name)
                    business_unit = BusinessUnit.objects.create(code=_, name=name)
                    business_unit.countries.add(country)
                except Exception as error:
                    country = Country.objects.create(code=_[0:3])
                    business_unit = BusinessUnit.objects.create(code=_)
                    business_unit.countries.add(country)
