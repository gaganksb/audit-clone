from django.conf import settings
from django.core.mail import send_mail

subject = "Lorem Ipsum"
message = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut 
enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit 
in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur 
sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
mollit anim id est laborum."""


def test_ses(receiver):
    send_mail(
        subject,
        message,
        settings.DEFAULT_FROM_EMAIL,
        [
            receiver,
        ],
    )
