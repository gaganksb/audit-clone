from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.exceptions import NotAuthenticated, PermissionDenied
from common.models import Permission
from unhcr.models import UNHCRMember
from field.models import FieldMember
from partner.models import Partner
from project.models import Agreement, AgreementMember
from auditing.models import AuditAgency
from account.models import UserTypeRole


class IsUNHCR(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model
        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="unhcr",
            user_members__is_default=True,
        )
        return request.user.is_unhcr_user and user_model_qs.exists()


class IsFO(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model
        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="field",
            user_members__is_default=True,
        )
        return request.user.is_field_user and user_model_qs.exists()


class IsAUDIT(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model
        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="auditing",
            user_members__is_default=True,
        )
        return request.user.is_audit_user and user_model_qs.exists()


class IsPARTNER(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model
        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="partner",
            user_members__is_default=True,
        )
        return request.user.is_partner_user and user_model_qs.exists()


class IsSuperUser(BasePermission):
    ADMIN_ONLY_AUTH_CLASSES = [BasicAuthentication, SessionAuthentication]

    def has_permission(self, request, view):
        user = request.user
        if user and user.is_authenticated:
            return user.is_superuser or not any(
                isinstance(request._authenticator, x)
                for x in self.ADMIN_ONLY_AUTH_CLASSES
            )
        return False


class CustomizablePermission(IsAuthenticated):
    def __call__(self, *args, **kwargs):
        # Need this to get around calling __init__ when setting up permission
        return self


class IsPARTNER(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model
        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="partner",
            user_members__is_default=True,
        )
        return request.user.is_partner_user and user_model_qs.exists()


class IsPartnerAdmin(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model

        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="partner",
            user_members__is_default=True,
            partner_members__role__role="ADMIN",
        )
        return request.user.is_partner_user and user_model_qs.exists()


class IsAuditAdmin(BasePermission):
    def has_permission(self, request, view):
        user_model = getattr(request.user, "_meta").model

        user_model_qs = user_model.objects.filter(
            id=request.user.id,
            user_members__resource_app_label="auditing",
            user_members__is_default=True,
            audit_members__role__role="ADMIN",
        )
        return request.user.is_audit_user and user_model_qs.exists()

class IsAuditAdminReadOnly(BasePermission):
    def has_permission(self, request, view):
        if request.method == "GET":
            user_model = getattr(request.user, "_meta").model

            user_model_qs = user_model.objects.filter(
                id=request.user.id,
                user_members__resource_app_label="auditing",
                user_members__is_default=True,
                audit_members__role__role="ADMIN",
            )
            return request.user.is_audit_user and user_model_qs.exists()
        else:
            return False


class IsOwnerPartnerAdminPA(BasePermission):
    def has_permission(self, request, view):
        partner = Partner.objects.filter(
            partnermember__user_id=request.user.id,
            partnermember__role__role="ADMIN",
        ).values_list("partnermember__user_id", flat=True)

        user_type_role = UserTypeRole.objects.filter(
            user_id__in=partner, is_default=True, resource_app_label="partner"
        )
        if user_type_role.exists():
            agreement = Agreement.objects.filter(partner_id__in=partner)
            return agreement.exists()
        return False


class IsOwnerAuditAdminPA(BasePermission):
    def has_permission(self, request, view):
        audit = AuditAgency.objects.filter(
            auditmember__user_id=request.user.id, auditmember__role__role="ADMIN"
        ).values_list("auditmember__user_id", flat=True)
        user_type_role = UserTypeRole.objects.filter(
            user_id__in=audit, is_default=True, resource_app_label="auditing"
        )
        if user_type_role.exists():
            agreement = Agreement.objects.filter(audit_firm_id__in=audit)
            return agreement.exists()
        return False


class IsAgreementMember(BasePermission):
    def has_permission(self, request, view):
        agreement_id = request.resolver_match.kwargs.get("pk", None)

        if hasattr(view, "name") and view.name == "create_repo_file":
            agreement_id = request.data.get("agreement", None)

        if agreement_id is not None:
            agreement = Agreement.objects.filter(id=agreement_id)
            if agreement.exists():
                if request.user.id in agreement.first().focal_points.all().values_list(
                    "user__id", flat=True
                ):
                    return True

            return False
        return False
