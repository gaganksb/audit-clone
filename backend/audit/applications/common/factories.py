import random, string, decimal
import factory
from factory import lazy_attribute
from faker import Faker

from account.models import User, UserTypeRole
from auditing.models import AuditAgency, AuditMember, Role as AuditRole
from common.models import BusinessUnit, Point, Region, Country, Area
from field.models import FieldOffice, FieldMember, Role as FieldRole
from partner.models import (
    PartnerType,
    Partner,
    PartnerMember,
    Role as PartnerRole,
    PQPStatus,
)
from icq.models import ICQReport
from project.models import (
    Goal,
    AgreementType,
    Agreement,
    AgreementReport,
    OIOSBusinessUnitReport,
)
from unhcr.models import UNHCRMember, Role as UNHCRRole
from decouple import config

IMPORT_ALL = config("IMPORT_ALL")
REAL_EMAIL = config("REAL_EMAIL")


fake = Faker()


def get_code(n):
    return "".join(random.choice(string.ascii_uppercase) for _ in range(n))


def get_email(fullname):
    return (
        fullname.lower().replace(" ", "-") + "@example.com"
        if REAL_EMAIL in ["False", False]
        else "@unhcr.org"
    )


def get_lat():
    return random.randint(-90, 90)


def get_lon():
    return random.randint(-180, 180)


def get_choice(choices):
    return random.choice(choices)


def get_rating(min_value, max_value):
    return round(random.uniform(min_value, max_value), 2)


""" account """


class UserFactory(factory.django.DjangoModelFactory):
    fullname = factory.LazyAttribute(lambda o: fake.name())
    password = factory.PostGenerationMethodCall("set_password", "Password@123")

    @lazy_attribute
    def email(self):
        return (
            self.fullname.lower().replace(" ", "-") + "@example.com"
            if REAL_EMAIL in ["False", False]
            else "@unhcr.org"
        )

    class Meta:
        model = User
        django_get_or_create = ("email",)


""" auditing """


class AuditAgencyFactory(factory.django.DjangoModelFactory):
    legal_name = factory.Sequence(lambda n: "Agency {}".format(n))

    class Meta:
        model = AuditAgency
        django_get_or_create = ("legal_name",)


class AuditMemberFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    audit_agency = factory.SubFactory(AuditAgencyFactory)
    role = factory.Iterator(AuditRole.objects.all())

    class Meta:
        model = AuditMember


""" common """


class BusinessUnitFactory(factory.django.DjangoModelFactory):
    code = factory.LazyFunction(lambda: get_code(3))

    class Meta:
        model = BusinessUnit
        django_get_or_create = ("code",)


class PointFactory(factory.django.DjangoModelFactory):
    lat = factory.LazyFunction(get_lat)
    lon = factory.LazyFunction(get_lon)

    class Meta:
        model = Point
        django_get_or_create = ("lat",)


""" field """


class FieldOfficeFactory(factory.django.DjangoModelFactory):
    country = factory.Iterator(Country.objects.all())

    class Meta:
        model = FieldOffice
        django_get_or_create = ("country",)


class FieldMemberFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    field_office = factory.SubFactory(FieldOfficeFactory)
    role = factory.Iterator(FieldRole.objects.all())

    # @factory.post_generation
    # def field_office(self, create, extracted, **kwargs):
    #     self.field_office.add(extracted)
    class Meta:
        model = FieldMember


""" partner """


class PartnerFactory(factory.django.DjangoModelFactory):
    legal_name = factory.Sequence(lambda n: "Partner {}".format(n))
    partner_type = factory.Iterator(PartnerType.objects.all())

    @factory.post_generation
    def business_unit(self, create, extracted, **kwargs):
        business_unit_count = random.randint(0, 1) * random.randint(1, 4)
        if business_unit_count:
            self.business_unit.add(
                *BusinessUnitFactory.create_batch(business_unit_count)
            )

    class Meta:
        model = Partner
        django_get_or_create = ("legal_name",)


class PartnerMemberFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    partner = factory.SubFactory(PartnerFactory)
    role = factory.Iterator(PartnerRole.objects.all())

    class Meta:
        model = PartnerMember


class ICQReportFactory(factory.django.DjangoModelFactory):
    report_date = factory.LazyFunction(
        lambda: fake.date_between(start_date="today", end_date="+30d")
    )
    partner = factory.Iterator(Partner.objects.all())
    overall_score_perc = factory.LazyFunction(lambda: get_rating(0, 1))

    class Meta:
        model = ICQReport


class OIOSBusinessUnitReportFactory(factory.django.DjangoModelFactory):
    report_date = factory.LazyFunction(
        lambda: fake.date_between(start_date="today", end_date="+30d")
    )
    business_unit = factory.Iterator(BusinessUnit.objects.all())
    overall_rating = factory.LazyFunction(lambda: get_rating(0, 100))

    class Meta:
        model = OIOSBusinessUnitReport


class PQPStatusFactory(factory.django.DjangoModelFactory):
    granted_date = factory.LazyFunction(
        lambda: fake.date_between(start_date="today", end_date="+30d")
    )
    expiry_date = factory.LazyFunction(
        lambda: fake.date_between(start_date="today", end_date="+30d")
    )
    partner = factory.Iterator(Partner.objects.all())
    national_worldwide = factory.LazyFunction(lambda: get_choice(["N", "W"]))

    class Meta:
        model = PQPStatus


""" project """


class GoalFactory(factory.django.DjangoModelFactory):
    code = factory.LazyFunction(lambda: get_code(2))
    description = factory.LazyAttribute(lambda o: fake.text(max_nb_chars=50))

    class Meta:
        model = Goal
        django_get_or_create = ("code",)


class AgreementFactory(factory.django.DjangoModelFactory):
    description = factory.LazyAttribute(lambda o: fake.text(max_nb_chars=200))
    agreement_type = factory.Iterator(AgreementType.objects.all())
    agreement_date = factory.LazyFunction(
        lambda: fake.date_between(start_date="today", end_date="+30d")
    )
    number = factory.LazyFunction(lambda: get_code(8))
    partner = factory.SubFactory(PartnerFactory)

    @factory.post_generation
    def goals(self, create, extracted, **kwargs):
        self.goals.add(*GoalFactory.create_batch(random.randint(1, 4)))

    @factory.post_generation
    def business_unit(self, create, extracted, **kwargs):
        business_unit_count = random.randint(0, 1) * random.randint(1, 4)
        if business_unit_count:
            self.business_unit.add(
                *BusinessUnitFactory.create_batch(business_unit_count)
            )

    @factory.post_generation
    def agreement_report(self, create, extracted, **kwargs):
        AgreementReport.objects.create(
            report_date=fake.date_between(start_date="today", end_date="+30d"),
            agreement=self,
            budget=decimal.Decimal(random.randrange(0, 1000000000)) / 100,
            installments=0,
            staff_cost=0,
            procurement=0,
            cash=0,
            other=0,
        )

    class Meta:
        model = Agreement


""" unhcr """


class UNHCRMemberFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    role = factory.Iterator(UNHCRRole.objects.all())

    class Meta:
        model = UNHCRMember
