import os
import xlrd
from django.conf import settings
from common.models import Permission
from account.models import User, UserTypeRole
from common.enums import PermissionListEnum
from partner.data import create_partner_roles
from common.data import create_permissions
from auditing.data import create_audit_roles
from field.data import create_field_roles
from unhcr.data import create_unhcr_roles
from unhcr.enums import UNHCRRoleEnum
from common.prod_data.data_import_helpers import *
from decouple import config
from unhcr.models import Role, UNHCRMember
from field.models import FieldOffice, Score, FieldMember, Role as FieldRole
from django.db.models import Value, FloatField, CharField
from django.db.models.functions import Concat
from common.antirrio_data_import import fetch_antirio_data

REAL_EMAIL = config("REAL_EMAIL")

HQ_TEST_ACCOUNTS = [
    {
        "fullname": "Fake HQ Preparer",
        "email": "hq-preparer@unhcr.org",
        "role": UNHCRRoleEnum.PREPARER.name,
    },
    {
        "fullname": "Fake HQ Reviewer",
        "email": "hq-reviewer@unhcr.org",
        "role": UNHCRRoleEnum.REVIEWER.name,
    },
    {
        "fullname": "Fake HQ Approver",
        "email": "hq-approver@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Deogratias Habimana",
        "email": "habimade@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Robert Berenyi",
        "email": "berenyi@unhcr.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Gianluca Nuzzo",
        "email": "nuzzo@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Anjali Upreti",
        "email": "upreti@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Francesco Sasso",
        "email": "sasso@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Reddy Vishal",
        "email": "reddy@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
    {
        "fullname": "Annalisa Diana",
        "email": "diana@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
]

HQ_TEST_ACCOUNTS = [
    {
        "fullname": "Francesco Sasso",
        "email": "sasso@unicc.org",
        "role": UNHCRRoleEnum.APPROVER.name,
    },
]


def create_permissions():
    for permission in PermissionListEnum:
        Permission.objects.get_or_create(permission=permission.name)


def update_user_role(resource_model, resource_data):
    resource_model_data = UserTypeRole.objects.filter(
        user_id=resource_data.user_id, is_default=True
    )
    if resource_model_data.exists() == False:
        resource_model_qs = UserTypeRole.objects.filter(
            user_id=resource_data.user_id,
            resource_model=resource_model,
            resource_id=resource_data.id,
        )
        if resource_model_qs.exists():
            resource_model_qs.update(is_default=True)


def generate_fake_data():
    create_permissions()
    create_field_roles()
    create_partner_roles()
    create_unhcr_roles()
    create_audit_roles()

    admin, created = User.objects.get_or_create(
        fullname="Admin",
        defaults={
            "email": "admin@unhcr.org",
            "is_superuser": True,
            "is_staff": True,
        },
    )
    password = "Passw0rd!"
    admin.set_password(password)
    admin.save()
    print("Superuser {}: {}/{}".format("created", admin.email, password))

    for tester in HQ_TEST_ACCOUNTS:
        email = tester["email"]
        if REAL_EMAIL == False or REAL_EMAIL == "False":
            email = "{0}@{1}".format(email.split("@")[0], "example.com")
        user, created = User.objects.get_or_create(
            fullname=tester["fullname"], email=email
        )

        password = "Password@123"
        user.set_password(password)
        user.save()
        role = Role.objects.get(role=tester["role"])
        unhcr_member, unhcr_created = UNHCRMember.objects.get_or_create(
            role=role, user=user
        )
        update_user_role("unhcrmember", unhcr_member)

    # Generate field office member
    # file_path = os.path.join(
    #     os.getcwd(), "MSRP", "production", "Audit focal points SEP-2020.xlsx"
    # )
    # wb = xlrd.open_workbook(file_path)
    # sheet = wb.sheet_by_name("Sheet1")

    # for idx_i in range(1, sheet.nrows):
    #     bu = sheet.cell_value(idx_i, 0)
    #     country = sheet.cell_value(idx_i, 1)
    #     main_focal = sheet.cell_value(idx_i, 3)
    #     alt_focal = sheet.cell_value(idx_i, 6)

    #     business_unit, created = BusinessUnit.objects.get_or_create(
    #         code=bu, name=country
    #     )
    #     if created:
    #         field_office = FieldOffice.objects.create(name=country)
    #         business_unit.field_office = field_office
    #         business_unit.save()

    #     if "@" in main_focal:
    #         user, created = User.objects.get_or_create(email=main_focal)
    #         if created == True:
    #             role = FieldRole.objects.get(role="APPROVER")
    #             fieldmember = FieldMember.objects.create(
    #                 field_office=business_unit.field_office, role=role, user=user
    #             )
    #             update_user_role("fieldmember", fieldmember)

    #     if "@" in alt_focal:
    #         user, created = User.objects.get_or_create(email=alt_focal)
    #         if created == True:
    #             role = FieldRole.objects.get(role="APPROVER")
    #             FieldMember.objects.create(
    #                 field_office=business_unit.field_office, role=role, user=user
    #             )
    #             update_user_role("fieldmember", fieldmember)


def import_project_data(year):
    datasets = ["HNIP998", "HNIP999"]

    items_999 = []
    items_998 = []
    for dataset in datasets:
        if dataset == "HNIP998":
            items_998 = fetch_antirio_data(dataset, year)
        elif dataset == "HNIP999":
            items_999 = fetch_antirio_data(dataset, year)

    if len(items_998) > 0 and len(items_999) > 0:
        print("Importing Implementer Types")
        import_implementer_types(items_999)
        print("Importing Partners")
        import_partners(items_999)
        print("Importing Pillars")
        import_pillars(items_999)
        print("Importing Vendors")
        import_vendors(items_999)
        print("Importing Operations")
        import_operations(items_999)
        print("Importing Goals")
        import_goals(items_999)
        print("Importing Cost Centers")
        import_cc_s(items_999)
        print("Importing Population Planning Groups")
        import_ppg_s(items_999)
        print("Importing Situations")
        import_situations(items_999)
        print("Importing Agreement Types")
        import_agreement_types(items_999)
        print("Importing Budget Refs")
        import_budget_refs(items_999)
        print("Importing Selection setting")
        import_selection_settings(items_999)
        print("Importing Agreements")
        import_agreements(items_999)
        print("Importing Currencies")
        import_currency(items_999)
        print("Importing Agreement Reports")
        import_agreement_report(items_999)
        print("Importing Field Assessment")
        import_field_assessments()

        agreements = parse_HNIP999(items_999)
        items = filter_HNIP998(items=items_998, agreements=agreements)

        if True:
            print("Importing Outputs")
            import_outputs(items)
            print("Importing Agreement Budgets")
            import_agreement_budgets(items)
            print("Update Agreement Budgets")
            update_agreement_budget(year)
            print("Finished")


def find_delta_agreement():

    file_name = "2020 Project Audit - Preliminary List - 11 Nov 2020.xlsx"
    file = os.path.join(
        os.getcwd(), "MSRP", "2020-docs", "project-selection", file_name
    )

    xls = os.path.join(os.getcwd(), "MSRP", file)
    wb = xlrd.open_workbook(xls)
    icq_sheet = "Preliminary List - 11 Nov "
    sheet = wb.sheet_by_name(icq_sheet)

    delta_agreements = []
    for i in range(6, sheet.nrows):
        try:
            pa_code = sheet.cell_value(i, 0)
            agreement = Agreement.objects.annotate(
                search_name=Concat(
                    "business_unit__code", "number", output_field=CharField()
                )
            ).filter(search_name=pa_code)
            if agreement.exists() == False:
                delta_agreements.append(pa_code)
        except Exception as e:
            print(e)
    return delta_agreements


def import_partial_data(year):
    datasets = ["HNIP998", "HNIP999"]

    items_999 = []
    items_998 = []
    for dataset in datasets:
        if dataset == "HNIP998":
            items_998 = fetch_antirio_data(dataset, year)
        elif dataset == "HNIP999":
            items_999 = fetch_antirio_data(dataset, year)

    if len(items_998) > 0 and len(items_999) > 0:

        delta_agreements = find_delta_agreement()
        items_999 = [
            i
            for i in items_999
            if "{0}{1}".format(i["BUSINESS_UNIT"], int(i["HCR_SA_ID"]))
            in delta_agreements
        ]
        items_998 = [
            i
            for i in items_998
            if "{0}{1}".format(i["BUSINESS_UNIT"], int(i["HCR_SA_ID"]))
            in delta_agreements
        ]

        print("Importing Agreements")
        import_agreements(items_999)
        print("Importing Currencies")
        import_currency(items_999)
        print("Importing Agreement Reports")
        import_agreement_report(items_999)
        print("Importing Field Assessment")
        import_field_assessments()

        agreements = parse_HNIP999(items_999)
        items = filter_HNIP998(items=items_998, agreements=agreements)

        if True:
            print("Importing Outputs")
            import_outputs(items)
            print("Importing Agreement Budgets")
            import_agreement_budgets(items)
            print("Update Agreement Budgets")
            update_agreement_budget(year)
            print("Finished")
