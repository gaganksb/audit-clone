from __future__ import unicode_literals

from model_utils import Choices

AUDIT_CANCELATION_REASON_CHOICES = Choices(
    ("covid", "Postponed and cancelled audits not audited so far due to the COVID"),
    ("other", "Other reason"),
)

PARTNER_TYPES = Choices(
    ("CBO", "cbo", "Community Based Organization (CBO)"),
    ("NGO", "national", "National NGO"),
    ("Int", "international", "International Civil Society Organization"),
    ("Aca", "academic", "Academic Institution"),
    ("RCC", "red_cross", "Red Cross/Red Crescent National Society"),
)


AGREEMENT_TYPES = Choices(
    ("BIP", "bipartite", "Bipartite Agency"),
    ("TRI", "tripartite", "Tripartite"),
    ("SMS", "small", "Small Scale"),
)

SERVER_ERROR = {"errors": "Internal server error"}

ACTIVITY_TYPES = [
    "PRE01 to PRE02",
    "PRE02 to PRE03",
    "PRE03 to INT01",
    "INT01 to INT02",
    "INT02 to FIN01",
    "FIN01 to FIN02",
    "FIN02 to FIN03",
    "PRE02 to PRE01",
    "PRE03 to PRE02",
    "INT02 to INT01",
    "FIN02 to FIN01",
    "INT01 to PRE01",
    "FIN01 to INT01",
    "INT01 to INT01",
]

AGREEMENT_MEMBER_CHOICES = (
    ("auditor", "Auditor"),
    ("partner", "Partner"),
    ("unhcr", "UNHCR"),
)

NOTIFICATION_FREQUENCY_CHOICES = Choices(
    ("", "disabled", "Disabled"),
    ("1_daily", "daily", "Daily"),
    ("2_weekly", "weekly", "Weekly"),
    ("3_biweekly", "biweekly", "Every Two Weeks"),
    ("immediate", "immediate", "Immediate"),
)

ACCOUNT_STATUSES = (
    ("active", "Active"),
    ("not-active", "Not Active"),
)

YES_NO = (
    ("y", "Y"),
    ("n", "N"),
)

COMMON_COMMENTS_TYPE = (
    ("PARTNER_RESPONSE", "Partner Response"),
    ("UNHCR_COMMENTS", "UNHCR Comments"),
)

PA_MANUALLY_INCLUDED = "SC13"
PA_MANUALLY_EXCLUDED = "SC12"

PRE01 = 1
PRE02 = 2
PRE03 = 3
INT01 = 4
INT02 = 5
FIN01 = 6
FIN02 = 7
FIN03 = 8

SURVEY_TYPES = Choices(
    ("field", "FIELD", "Field"),
    ("auditor", "AUDITOR", "Auditor"),
    ("partner", "PARTNER", "Partner"),
)

ICQ_COLUMNS_EN = [
    "Process #",
    "Process",
    "General risk",
    "Key control #",
    "Key control title",
    "Key control attribute #",
    "Key control attribute",
    "Applicability of the control at the Partner?",
    "Existence of the control",
    "Applicability of the control at the Partner?",
    "Existence of the control",
    "Prior year recommendation status",
    "Comments",
    "Management letter finding",
    "Risk",
    "Risk Criticality",
    "Auditor's recommendation",
    "Management comments and proposed actions",
    "Deadlines for actions and responsible person",
]

ICQ_COLUMNS_FR = [
    "Processus #",
    "Processus",
    "Risque général",
    "Contrôle clé #",
    "Nom du contrôle clé",
    "Elément du contrôle clé # ",
    "Elément du contrôle clé",
    "Applicabilité du contrôle chez le partenaire?",
    "Existence du contrôle",
    "Applicabilité du contrôle chez le partenaire?",
    "Existence du contrôle",
    "Status de la recommendation de l'audit précédent",
    "Commentaires",
    "Constations management letter",
    "Risque",
    "Criticité du risque",
    "Recommandation de l’auditeur",
    "Commentaires et actions envisagées par la Direction",
    "Délais d’intervention et personnes responsables",
]

ICQ_COLUMNS_ES = [
    "Proceso núm.",
    "Proceso",
    "Riesgo general",
    "Control clave núm.",
    "Título del control clave",
    "Atributo del control clave núm.",
    "Atributo del control clave",
    "¿Aplicabilidad del control por el Socio?",
    "Existencia del control",
    "¿Aplicabilidad del control por el Socio?",
    "Existencia del control",
    "Estado recomendaciones del año anterior",
    "Comentarios",
    "Conclusión de la carta de auditoría",
    "Riesgo",
    "Prioridad del riesgo",
    "Recomendación del auditor",
    "Comentarios del equipo directivo y acciones propuestas",
    "Plazos de implementación y persona responsable",
]
