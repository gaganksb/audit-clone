import json
from decouple import config
from .models import ExceptionLog
from django.http import HttpResponse
from notify.helpers import send_error_notifications
from django.db.models import Q, F
import requests
import urllib.parse
from rest_framework import status
import traceback


class ExceptionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        error_check = False
        current_url = request.path_info
        try:
            stacktrace = traceback.format_exc()
        except Exception as e:
            stacktrace = None
        exception_log = ExceptionLog.objects.filter(
            Q(stacktrace__icontains=exception), url=current_url
        )
        if exception_log:
            exception_log.update(counter=F("counter") + 1)
        else:
            intitle = u"{}: {}".format(exception.__class__.__name__, exception)
            stack_answer = []
            params = urllib.parse.urlencode(
                {
                    "intitle": intitle,
                    "page": 1,
                    "pagesize": 5,
                    "order": "desc",
                    "sort": "votes",
                    "accepted": True,
                    "tagged": "python;django",
                    "wiki": False,
                    "site": "stackoverflow",
                }
            )
            stack_url = "https://api.stackexchange.com/2.2/search/advanced?" + params
            try:
                api_response = requests.get(stack_url, timeout=10)
                if api_response:
                    api_response = api_response.json()
                    api_response = api_response["items"]
                    for item in api_response:
                        stack_answer.append(
                            {"link": item["link"], "title": item["title"]}
                        )

            except Exception:
                error_check = True

        try:
            ExceptionLog.objects.create(
                url=current_url,
                stacktrace=stacktrace,
                exception=exception,
                stackoverflow=stack_answer,
                counter=0,
            )
        except Exception:
            error_check = True
        try:
            send_error_notifications(exception, stack_answer)
        except Exception:
            error_check = True

        if error_check:
            return HttpResponse(
                json.dumps({"message": "internal server error"}),
                content_type="application/json",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
