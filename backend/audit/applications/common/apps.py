from django.apps import AppConfig
from django.db import connection
from django.conf import settings

import subprocess


class CommonConfig(AppConfig):
    name = "common"

    def ready(self):
        if settings.ENV == "local":
            proc = subprocess.Popen(
                ["ps", "auwx", "|", "grep", "process_tasks"],
                stdout=subprocess.PIPE,
                shell=True,
            )
            out, err = proc.communicate()
            all_tables = connection.introspection.table_names()

            tasks = out.decode().split("\n")
            bg_job_running = False
            for taks in tasks:
                if "python manage.py process_tasks" in taks:
                    bg_job_running = True

            if bg_job_running is False:
                if "background_task" in all_tables:
                    subprocess.Popen(["python", "manage.py", "process_tasks"])
                    print("Started background job ")
            else:
                print("Background job is already running")
            connection.close()