from django.urls import path
from common import views

urlpatterns = [
    path("points/", views.PointList.as_view(), name="point-list"),
    path("points/<int:pk>/", views.PointDetail.as_view(), name="point-detail"),
    path("regions/", views.RegionList.as_view(), name="region-list"),
    path("regions/<int:pk>/", views.RegionDetail.as_view(), name="region-detail"),
    path("countries/", views.CountryList.as_view(), name="country-list"),
    path("countries/<int:pk>/", views.CountryDetail.as_view(), name="country-detail"),
    path("areas/", views.AreaList.as_view(), name="area-list"),
    path("areas/<int:pk>/", views.AreaDetail.as_view(), name="area-detail"),
    path("bu/", views.BusinessUnitList.as_view(), name="businessunit-list"),
    path(
        "bu/<int:pk>/", views.BusinessUnitDetail.as_view(), name="businessunit-detail"
    ),
    path("permission/", views.PermissionList.as_view()),
    path("general/", views.GeneralConfigAPIView.as_view(), name="general-config"),
    # path('comments/', views.CommentsAPIView.as_view(), name='comments-list'),
    path("comments/<int:pk>/", views.CommentAPIView.as_view(), name="comments-detail"),
    # dashboard
    path("dashboard/", views.DashboadViewSet.as_view(), name="dashboard-view"),
    path("bg-tasks/", views.BackgroundTaskAPIView.as_view(), name="bg-tasks"),
    path(
        "data-import-template/",
        views.FileDownloadListAPIView.as_view(),
        name="download-file",
    ),
]
