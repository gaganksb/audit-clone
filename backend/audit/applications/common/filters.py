from common.models import Country
from django_filters.filters import CharFilter
from django_filters import FilterSet

from background_task.models import Task
from background_task.models_completed import CompletedTask


class CountryFilter(FilterSet):
    code = CharFilter(lookup_expr="icontains")
    name = CharFilter(lookup_expr="icontains")
    region = CharFilter(method="filter_region")

    def filter_region(self, queryset, name, value):
        return queryset.filter(region__name__icontains=value)

    class Meta:
        model = Country
        fields = ["code", "name", "region"]


class TaskFilter(FilterSet):
    task_name = CharFilter(lookup_expr="icontains")

    class Meta:
        model = Task
        fields = [
            "task_name",
        ]


class CompletedTaskFilter(FilterSet):

    task_name = CharFilter(lookup_expr="icontains")

    class Meta:
        model = CompletedTask
        fields = [
            "task_name",
        ]
