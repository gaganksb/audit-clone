from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from django.apps import apps
from django.db.models import Q, Sum
from rest_framework import generics, status, exceptions
import os
from django.http import HttpResponse

from common.models import (
    Point,
    Region,
    Country,
    BusinessUnit,
    Area,
    Permission,
    Comment,
)
from common.serializers import (
    PointSerializer,
    RegionSerializer,
    CountrySerializer,
    BusinessUnitSerializer,
    AreaSerializer,
    PermissionSerializer,
    CommentSerializer,
)
from common.permissions import IsSuperUser, IsUNHCR
from common.enums import PermissionListEnum
from utils.rest.code import code
from collections import OrderedDict

from account.authentication import CustomAzureADBBCOAuth2
from django.urls import reverse
from rest_framework.views import APIView

from django.conf import settings
from urllib.parse import quote

from django_filters.rest_framework import DjangoFilterBackend
from common.filters import CountryFilter, TaskFilter, CompletedTaskFilter
from django.contrib.contenttypes.models import ContentType

from background_task.models import Task
from background_task.models_completed import CompletedTask

from common.serializers import (
    BackgroundTaskSerializer,
    BackgroundCompletedTaskSerializer,
)


class PointList(generics.ListAPIView):
    queryset = Point.objects.all()
    serializer_class = PointSerializer
    permission_classes = (IsAuthenticated,)


class PointDetail(generics.RetrieveAPIView):
    queryset = Point.objects.all()
    serializer_class = PointSerializer
    permission_classes = (IsAuthenticated,)


class RegionList(generics.ListCreateAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsAuthenticated,)


class RegionDetail(generics.RetrieveAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (IsAuthenticated,)


class CountryList(generics.ListCreateAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    # permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)
    filter_class = CountryFilter


class CountryDetail(generics.RetrieveAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    # permission_classes = (IsAuthenticated,)


class BusinessUnitList(generics.ListAPIView):
    serializer_class = BusinessUnitSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        code = self.request.query_params.get("code")
        name = self.request.query_params.get("name")
        queryset = BusinessUnit.objects.all()
        if code:
            queryset = queryset.filter(code__icontains=code).order_by("code")
        if name:
            queryset = queryset.filter(name__icontains=name).order_by("name")
        return queryset


class BusinessUnitDetail(generics.RetrieveAPIView):
    queryset = BusinessUnit.objects.all()
    serializer_class = BusinessUnitSerializer
    permission_classes = (IsAuthenticated,)


class AreaList(generics.ListAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer
    permission_classes = (IsAuthenticated,)


class AreaDetail(generics.RetrieveAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer
    permission_classes = (IsAuthenticated,)


class PermissionList(generics.ListAPIView):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    permission_classes = (IsAuthenticated, IsSuperUser)
    pagination_class = None


class DashboadViewSet(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):

        return Response(
            OrderedDict(
                [
                    (
                        "result",
                        "data",
                    ),
                    ("status", status.HTTP_200_OK),
                    ("code", "OK_GET"),
                    ("message", code["OK_GET"]),
                ]
            )
        )


class GeneralConfigAPIView(APIView):
    @property
    def logout_url(self):
        AUTH_DOMAIN = "https://uniccsharedb2c.b2clogin.com/uniccsharedb2c.onmicrosoft.com/oauth2/v2.0/"
        policy = settings.SOCIAL_AUTH_AZUREAD_B2C_OAUTH2_POLICY
        protocol = "http" if settings.DEBUG else "https"
        frontend_url = f"{protocol}://{settings.FRONTEND_HOST}"
        post_logout_redirect_uri = quote(frontend_url)
        return (
            AUTH_DOMAIN
            + f"logout?p={policy}&post_logout_redirect_uri={post_logout_redirect_uri}"
        )

    def get(self, request, *args, **kwargs):
        data = {
            "active-directory-login-url": reverse(
                "accounts:social-login", kwargs={"backend": "azuread-b2c-oauth2"}
            ),
            "active-directory-logout-url": self.logout_url,
        }
        return Response(data, status=status.HTTP_200_OK)


class CommentsAPIView(generics.ListCreateAPIView):

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def post(self, request, *args, **kwargs):
        content_type = request.data.get("content_type", None)
        object_id = request.data.get("object_id", None)

        content_type_qs = ContentType.objects.filter(model=content_type)

        if content_type_qs.exists() == False or content_type == None:
            error = "Invalid content type"
            raise exceptions.APIException(error)
        else:
            request.data["content_type"] = content_type_qs.first().id
            return self.create(request, *args, **kwargs)


class CommentAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class BackgroundTaskAPIView(generics.ListAPIView):
    def get(self, request, *args, **kwargs):

        task_hash = request.query_params.get("task_hash", None)
        task_id = request.query_params.get("task_id", None)
        pending_task = request.query_params.get("pending_task", None)
        pending_tasks = None
        completed_tasks = None

        if task_id is not None and task_hash is not None:
            completed_tasks = CompletedTask.objects.filter(
                verbose_name=task_id, task_hash=task_hash
            )

        if pending_task is not None:
            pending_tasks = Task.objects.filter(task_name__icontains=pending_task)
            pending_tasks = (
                pending_tasks.values()[0] if pending_tasks.exists() else None
            )

        response = {
            "pending_tasks": pending_tasks,
            "completed": {
                "status": None if completed_tasks is None else completed_tasks.exists(),
                "details": None
                if completed_tasks is None
                else completed_tasks.values(),
            },
        }

        return Response(response, status=status.HTTP_200_OK)


class FileDownloadListAPIView(generics.ListAPIView):
    def get(self, request):
        file_name = "data_import_template.xlsx"
        file_handle = os.path.join(
            os.getcwd(),
            "audit",
            "applications",
            "project",
            "excel",
            "data_import_template.xlsx",
        )
        document = open(file_handle, "rb")
        response = HttpResponse(document, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = 'attachment; filename="%s"' % file_name
        return response
