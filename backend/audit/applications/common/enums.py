from enum import Enum, unique, auto


class AutoNameEnum(Enum):
    def _generate_next_value_(name, *args):
        return name


@unique
class PermissionListEnum(AutoNameEnum):
    VIEW_DASHBOARD = auto()
    CAN_MANAGE_USERS = auto()


@unique
class UserTypeEnum(AutoNameEnum):
    HQ = auto()
    FO = auto()
    AUDITOR = auto()
    PARTNER = auto()
    OTHER = auto()
    HQ_FO = auto()
