from django.db import models
from django.core.validators import (
    MinValueValidator,
    MaxValueValidator,
    FileExtensionValidator,
)
from decimal import Decimal
from model_utils.models import TimeStampedModel
from django.conf import settings
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill
from audit.core.abstract_model import AbstractModel
from django.contrib.postgres.fields import ArrayField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField, JSONField
from background_task.models import Task
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.
class Point(AbstractModel):
    lat = models.DecimalField(
        verbose_name="Latitude",
        null=True,
        blank=True,
        max_digits=8,
        decimal_places=5,
        validators=[MinValueValidator(Decimal(-90)), MaxValueValidator(Decimal(90))],
    )
    lon = models.DecimalField(
        verbose_name="Longitude",
        null=True,
        blank=True,
        max_digits=8,
        decimal_places=5,
        validators=[MinValueValidator(Decimal(-180)), MaxValueValidator(Decimal(180))],
    )

    class Meta:
        unique_together = ["lat", "lon"]

    def __str__(self):
        return "[{}, {}]".format(self.lat, self.lon)


class Area(AbstractModel):
    code = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Region(AbstractModel):
    code = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=255)
    area = models.ForeignKey(Area, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Country(AbstractModel):
    code = models.CharField(max_length=50, unique=True, null=False)
    name = models.CharField(max_length=255, null=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name_plural = "Countries"

    def __str__(self):
        return self.name


class BusinessUnit(AbstractModel):
    code = models.CharField(max_length=50, unique=True, null=False)
    name = models.CharField(max_length=255, null=True)
    countries = models.ManyToManyField(Country)
    field_office = models.ForeignKey(
        "field.FieldOffice",
        on_delete=models.CASCADE,
        null=True,
        related_name="fo_business_units",
    )

    def __str__(self):
        return self.code


class Permission(AbstractModel):
    permission = models.CharField(max_length=255, unique=True, null=False)
    description = models.TextField(max_length=2000)

    def __str__(self):
        return self.permission


class CostCenter(AbstractModel):
    code = models.CharField(max_length=25, unique=True, null=False)
    description = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.code


class Currency(AbstractModel):
    code = models.CharField(max_length=3, null=False, unique=True)

    def __str__(self):
        return self.code


class CommonFile(TimeStampedModel):
    if settings.ENV in ["s3", "dev", "uat", "prod"]:
        from audit.settings.storage_backends import PrivateMediaStorage

        file_field = models.FileField(
            validators=(FileExtensionValidator(settings.ALLOWED_EXTENSIONS),),
            storage=PrivateMediaStorage(),
        )
    else:
        file_field = models.FileField(
            validators=(FileExtensionValidator(settings.ALLOWED_EXTENSIONS),),
        )
    user = models.ForeignKey(
        "account.User", on_delete=models.CASCADE, null=True, default=None
    )
    # Only applicable for image files
    __thumbnail = ImageSpecField(
        source="file_field",
        processors=[ResizeToFill(150, 75)],
        format="JPEG",
        options={"quality": 80},
    )

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return f"CommonFile [{self.pk}] {self.file_field}"

    @property
    def thumbnail_url(self):
        """
        Done this way to fail gracefully when trying to get thumbnail for non-image file
        """
        try:
            return self.__thumbnail.url
        except OSError:
            return None

    @property
    def has_existing_reference(self):
        """
        Returns True if this file is referenced from at least one other object
        """
        for attr_name in dir(self):
            if (
                attr_name == CommonFile.has_existing_reference.fget.__name__
                or not hasattr(self, attr_name)
            ):
                continue
            attribute = getattr(self, attr_name)
            if callable(getattr(attribute, "exists", None)) and attribute.exists():
                return True

        return False


class ExceptionLog(AbstractModel):

    url = models.CharField(max_length=200, null=True, default=None)
    stacktrace = models.TextField(default=None)
    stackoverflow = ArrayField(models.TextField(default=None), blank=True, null=True)
    counter = models.IntegerField(default=0)
    solved = models.BooleanField(default=False, null=True)
    exception = models.TextField(default=None, null=True)


class Comment(AbstractModel):
    message = models.TextField()
    user = models.ForeignKey(
        "account.User", on_delete=models.CASCADE, null=True, default=None
    )
    content_type = models.ForeignKey(
        ContentType, null=True, blank=True, on_delete=models.CASCADE
    )

    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey("content_type", "object_id")
    comment_type = JSONField(default=list, blank=True)


@receiver(post_save, sender=Task)
def update_risk_rating(sender, instance, created, **kwargs):

    if created and instance.verbose_name == None:
        instance.verbose_name = instance.id
        instance.save()

    def __str__(self):
        return self.user.fullname
