from auditing.models import AuditAgency, AuditWorksplitGroup
from django_filters.filters import CharFilter, NumberFilter
from django_filters import FilterSet


class AuditAgencyFilter(FilterSet):
    legal_name = CharFilter(lookup_expr="icontains")
    city = CharFilter(lookup_expr="icontains")
    country = CharFilter(method="filter_country")

    def filter_country(self, queryset, name, value):
        return queryset.filter(country__name__icontains=value)

    class Meta:
        model = AuditAgency
        fields = ["legal_name", "city", "country"]


class WorksplitGroupFilter(FilterSet):
    name = CharFilter(lookup_expr="icontains")
    business_units = CharFilter(method="filter_bu")

    def filter_bu(self, queryset, name, value):

        return queryset.filter(business_units__in=value.split(",")).distinct()

    class Meta:
        model = AuditWorksplitGroup
        fields = ["name", "business_units"]
