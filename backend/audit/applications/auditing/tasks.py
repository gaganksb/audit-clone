from django.db import transaction
from notify.models import NotifiedUser
from project.models import Agreement
from account.models import User
from auditing.models import AuditMember
from background_task import background
from notify.helpers import send_notification, send_notification_to_users
from django.db.models import Q
from common.helpers import filter_project_selection


@background(schedule=3)
def notify_end_of_audit_assignment(year, user_type, agency_id):

    queryset = Agreement.objects.all()
    queryset = filter_project_selection(queryset, year, "fin", "included")

    if user_type == "HQ":
        # Notify Audit admins (requested by HQ user)
        finalized_audit_firm = queryset.distinct("audit_firm")
        for firm in finalized_audit_firm:
            audit_members = AuditMember.objects.filter(
                audit_agency_id=firm.audit_firm.id, role__role="ADMIN"
            )
            users = User.objects.filter(
                id__in=audit_members.values_list("user_id", flat=True)
            )
            if users.exists():
                context = {
                    "legal_name": firm.audit_firm.legal_name,
                    "subject": "End of Audit Firm Assignment",
                }
                notification = send_notification(
                    notification_type="end_of_audit_firm_assignment",
                    obj=users.first(),
                    users=users,
                    context=context,
                )
                try:
                    with transaction.atomic():
                        user = NotifiedUser.objects.select_for_update().filter(
                            notification__name="End of Audit Firm Assignment",
                            sent_as_email=False,
                        )
                        user_notified = send_notification_to_users(user)
                except Exception as e:
                    print("Error while sending sent_project_selection_notification ", e)

    # Notify Audit reviewers (requested by AUDIT admin user)
    if user_type == "AUDITOR":

        for project in queryset.filter(audit_firm_id=agency_id):

            auditing_users = project.focal_points.filter(
                user_type="auditing", reviewer=True
            ).distinct("user_id")

            ## Notification for audit reviewers
            auditing_user_list = auditing_users.values_list("user__id", flat=True)
            users = User.objects.filter(id__in=auditing_user_list)
            for user in users:

                context = {
                    "auditor_reviewer": user.fullname,
                    "project_title": project.title,
                    "subject": "Notification of Assignment of Auditor Reviewer to a PPA",
                }
                notification = send_notification(
                    notification_type="notification_of_assignment_of_auditor_reviewer",
                    obj=user,
                    users=[user],
                    context=context,
                )

            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Notification of Assignment of Auditor Reviewer to a PPA",
                        sent_as_email=False,
                    )
                    user_notified = send_notification_to_users(user)
            except Exception as e:
                print("Error while sending sent_project_selection_notification ", e)

            auditing_reviewer_list = auditing_users.values_list(
                "user__fullname", flat=True
            )
            context = {
                "project_id": project.id,
                "project_title": project.title,
                "subject": "Notification Of Assignment Of Auditor Reviewer To PPA Workgroup",
                "audit_reviewers": ", ".join(list(auditing_reviewer_list)),
            }
            # Notify Field/BU user
            field_qs = project.focal_points.filter(
                Q(user_type="unhcr") & (Q(focal=True) | Q(alternate_focal=True))
            ).distinct("user_id")

            if field_qs.exists():
                field_users = field_qs.values_list("user_id", flat=True)
                users = User.objects.filter(id__in=field_users)

                if users.exists():
                    notification = send_notification(
                        notification_type="notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup",
                        obj=users.first(),
                        users=users,
                        context=context,
                    )
                    try:
                        with transaction.atomic():
                            user = NotifiedUser.objects.select_for_update().filter(
                                notification__name="Notification Of Assignment Of Auditor Reviewer To PPA Workgroup",
                                sent_as_email=False,
                            )
                            user_notified = send_notification_to_users(user)
                    except Exception as e:
                        print(
                            "Error while sending notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup ",
                            e,
                        )

            # Notify Partner user
            partner_qs = project.focal_points.filter(
                Q(user_type="partner") & (Q(focal=True) | Q(alternate_focal=True))
            ).distinct("user_id")

            if partner_qs.exists():
                partner_users = partner_qs.values_list("user_id", flat=True)
                users = User.objects.filter(id__in=partner_users)

                if users.exists():
                    notification = send_notification(
                        notification_type="notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup",
                        obj=users.first(),
                        users=users,
                        context=context,
                    )
                    try:
                        with transaction.atomic():
                            user = NotifiedUser.objects.select_for_update().filter(
                                notification__name="Notification Of Assignment Of Auditor Reviewer To PPA Workgroup",
                                sent_as_email=False,
                            )
                            user_notified = send_notification_to_users(user)
                    except Exception as e:
                        print(
                            "Error while sending notification_of_assignment_of_auditor_reviewer_to_ppa_workgroup ",
                            e,
                        )
