from auditing.enums import AuditRoleEnum, AUDIT_ROLE_PERMISSIONS
from auditing.models import Role
from common.models import Permission


def create_audit_roles():
    for role in AuditRoleEnum:
        _ = Role.objects.get_or_create(role=role.name)
        permissions = list(
            Permission.objects.filter(
                permission__in=AUDIT_ROLE_PERMISSIONS[role]
            ).values_list("id", flat=True)
        )
        _[0].permissions.remove(*_[0].permissions.all())
        for permission in permissions:
            _[0].permissions.add(permission)
