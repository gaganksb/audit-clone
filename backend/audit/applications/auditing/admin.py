from django.contrib import admin
from .models import AuditAgency, AuditMember, Role, AuditWorksplitGroup, AuditReport


class AuditAgencyAdmin(admin.ModelAdmin):
    search_fields = ("legal_name",)
    list_display = ("legal_name",)
    ordering = ("legal_name",)


class AuditMemberAdmin(admin.ModelAdmin):
    search_fields = ("user__email", "audit_agency__legal_name", "role__name")
    list_display = (
        "user",
        "audit_agency",
        "role",
    )
    ordering = ("audit_agency__legal_name",)
    raw_id_fields = (
        "user",
        "audit_agency",
    )


class RoleAdmin(admin.ModelAdmin):
    search_fields = ("role__role",)
    filter_horizontal = ("permissions",)

    readonly_fields = ("role",)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(AuditAgency, AuditAgencyAdmin)
admin.site.register(AuditMember, AuditMemberAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(AuditWorksplitGroup)
admin.site.register(AuditReport)
