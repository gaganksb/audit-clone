from django.urls import path
from auditing import views

urlpatterns = [
    path("", views.AuditAgencyList.as_view(), name="auditagency-list"),
    path("<int:pk>/", views.AuditAgencyDetail.as_view(), name="auditagency-detail"),
    path("<int:pk>/members/", views.AuditMemberList.as_view(), name="auditmember-list"),
    path(
        "member/<int:pk>/", views.AuditMemberDetail.as_view(), name="auditmember-list"
    ),
    path("roles/", views.AuditRoleList.as_view(), name="role-list"),
    path("roles/<int:pk>/", views.AuditRoleDetail.as_view(), name="role-detail"),
    path("groups/", views.WorkSplitGroupList.as_view(), name="group-list"),
    path("group/", views.WorkSplitGroupCreate.as_view(), name="group-create"),
    path("group/<int:pk>/", views.WorkSplitGroupDetail.as_view(), name="group-detail"),
    path(
        "agreement/<int:pk>/audit-report/",
        views.AuditReportsApiView.as_view(),
        name="audit-report",
    ),
    path(
        "audit-report/<int:pk>/",
        views.AuditReportApiView.as_view(),
        name="audit-report-detail",
    ),
    path(
        "agreements/<int:agreement_id>/report-status/",
        views.ReportStatusAPIView.as_view(),
        name="report-status",
    ),
    path(
        "end-of-assignment-notify/<int:year>/",
        views.EndOfAuditAssignmentNotification.as_view(),
        name="end-of-assignment-notify",
    ),
]
