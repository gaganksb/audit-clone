# Generated by Django 2.2.1 on 2020-09-30 09:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("common", "0001_initial"),
        ("auditing", "0002_auto_20200930_0923"),
        ("project", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="mgmtletter",
            name="agreement",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="mgmt_letter",
                to="project.Agreement",
            ),
        ),
        migrations.AddField(
            model_name="mgmtletter",
            name="user",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="auditing_mgmtletter_related",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="majorfindings",
            name="mgmt_letter",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="mgmt_letter_major_findings",
                to="auditing.MgmtLetter",
            ),
        ),
        migrations.AddField(
            model_name="majorfindings",
            name="process",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="auditing.Process"
            ),
        ),
        migrations.AddField(
            model_name="general",
            name="mgmt_letter",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="mgmt_letter_general",
                to="auditing.MgmtLetter",
            ),
        ),
        migrations.AddField(
            model_name="financialfindings",
            name="mgmt_letter",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="mgmt_letter_fin_findings",
                to="auditing.MgmtLetter",
            ),
        ),
        migrations.AddField(
            model_name="auditworksplitgroup",
            name="business_units",
            field=models.ManyToManyField(to="common.BusinessUnit"),
        ),
        migrations.AddField(
            model_name="auditworksplitgroup",
            name="user",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="auditing_auditworksplitgroup_related",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="auditreport",
            name="agreement",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="audit_report",
                to="project.Agreement",
            ),
        ),
        migrations.AddField(
            model_name="auditreport",
            name="attachments",
            field=models.ManyToManyField(to="common.CommonFile"),
        ),
        migrations.AddField(
            model_name="auditreport",
            name="user",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="auditing_auditreport_related",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="auditmember",
            name="audit_agency",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="audit_agency_members",
                to="auditing.AuditAgency",
            ),
        ),
        migrations.AddField(
            model_name="auditmember",
            name="role",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="auditing.Role"
            ),
        ),
        migrations.AddField(
            model_name="auditmember",
            name="user",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="audit_members",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="auditagency",
            name="country",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="common.Country",
            ),
        ),
        migrations.AddField(
            model_name="auditagency",
            name="user",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="auditing_auditagency_related",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="auditmember",
            unique_together={("user", "audit_agency", "role")},
        ),
    ]
