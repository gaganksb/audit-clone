from enum import unique, auto

from common.enums import AutoNameEnum, PermissionListEnum


@unique
class AuditRoleEnum(AutoNameEnum):
    REVIEWER = auto()
    ADMIN = auto()


AUDIT_ROLE_PERMISSIONS = {
    AuditRoleEnum.REVIEWER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
    AuditRoleEnum.ADMIN: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
}
