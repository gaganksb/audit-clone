from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.http import Http404
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from project.models import Agreement, SenseCheck, Cycle, SelectionSettings
from project.serializers import AgreementSerializer

from auditing.models import (
    AuditAgency,
    AuditMember,
    Role as AuditRole,
    AuditWorksplitGroup,
    AuditReport,
)
from auditing.serializers import (
    AuditAgencySerializer,
    AuditAgencyListSerializer,
    AuditMemberSerializer,
    AuditRoleSerializer,
    AuditMemberDetailSerializer,
    WorkSplitGroupSerializer,
    WorkSplitGroupListSerializer,
    AuditReportSerializer,
    AuditReportsSerializer,
)
from common.permissions import IsAUDIT, IsUNHCR
from utils.rest.code import code
from django_filters.rest_framework import DjangoFilterBackend
from auditing.filters import AuditAgencyFilter, WorksplitGroupFilter
from common.models import CommonFile
from django.http.request import QueryDict, MultiValueDict
from rest_framework import exceptions
from .permissions import AuditReportPermission, AuditReportGetPermission
from common.views import CommentsAPIView, CommentAPIView
from common.consts import COMMON_COMMENTS_TYPE
from django.contrib.contenttypes.models import ContentType
from .tasks import notify_end_of_audit_assignment
from common.helpers import filter_project_selection
from icq.models import MgmtLetter
from common.enums import UserTypeEnum

PA_MANUALLY_EXCLUDED = "SC12"
# class based view with generic views
class AuditAgencyDetail(generics.RetrieveUpdateAPIView):
    queryset = AuditAgency.objects.all()
    serializer_class = AuditAgencySerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):
        self.serializer_class = AuditAgencyListSerializer
        data = self.retrieve(request, *args, **kwargs)
        return data


class AuditAgencyList(generics.ListCreateAPIView):
    queryset = AuditAgency.objects.all()
    serializer_class = AuditAgencySerializer
    permission_classes = (IsAuthenticated, IsUNHCR)
    filter_backends = (DjangoFilterBackend,)
    filter_class = AuditAgencyFilter

    def get(self, request, *args, **kwargs):
        self.serializer_class = AuditAgencyListSerializer
        data = self.list(request, *args, **kwargs)
        return data


class AuditMemberList(generics.ListAPIView):
    queryset = AuditMember.objects.all()
    serializer_class = AuditMemberSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, pk):
        members = AuditMember.objects.filter(audit_agency__id=pk)
        serializer = AuditMemberSerializer(
            members, many=True, context={"request": request}
        )
        return Response(serializer.data)


class AuditMemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AuditMember.objects.all()
    serializer_class = AuditMemberDetailSerializer
    permission_classes = (IsAuthenticated, IsAUDIT | IsUNHCR)
    resource_model = AuditMember

    def get(self, request, pk=None, *args, **kwargs):
        audit_member_qs = self.resource_model.objects.filter(pk=pk)
        if audit_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        audit_member = audit_member_qs.last()
        res = self.serializer_class(audit_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def patch(self, request, pk=None, *args, **kwargs):
        audit_member_qs = self.resource_model.objects.filter(pk=pk)
        if audit_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        audit_member = audit_member_qs.last()
        serializer = self.serializer_class(audit_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        audit_member = serializer.update(audit_member, validated_data=request.data)
        res = self.serializer_class(audit_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def put(self, request, pk=None, *args, **kwargs):
        audit_member_qs = self.resource_model.objects.filter(pk=pk)
        if audit_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        audit_member = audit_member_qs.last()
        serializer = self.serializer_class(audit_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        audit_member = serializer.update(audit_member, validated_data=request.data)
        res = self.serializer_class(audit_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)


class AuditRoleList(generics.ListCreateAPIView):
    queryset = AuditRole.objects.all()
    serializer_class = AuditRoleSerializer
    permission_classes = (IsAuthenticated,)


class AuditRoleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AuditRole.objects.all()
    serializer_class = AuditRoleSerializer
    permission_classes = (IsAuthenticated,)


class WorkSplitGroupList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AuditWorksplitGroup.objects.all()
    serializer_class = WorkSplitGroupListSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = WorksplitGroupFilter


class WorkSplitGroupCreate(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AuditWorksplitGroup.objects.all()
    serializer_class = WorkSplitGroupSerializer


class WorkSplitGroupDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = AuditWorksplitGroup.objects.all()
    serializer_class = WorkSplitGroupSerializer

    def patch(self, request, *args, **kwargs):
        data = self.partial_update(request, *args, **kwargs)
        return Response({"result": "success"}, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        data = self.destroy(request, *args, **kwargs)
        return Response({"result": "success"}, status=status.HTTP_200_OK)


class AuditReportsApiView(generics.ListAPIView):

    queryset = AuditReport.objects.all()
    serializer_class = AuditReportsSerializer
    permission_classes = (
        IsAuthenticated,
        AuditReportGetPermission,
    )

    def get(self, request, *args, **kwargs):
        agreement = Agreement.objects.filter(id=kwargs["pk"])
        if agreement.exists() == False:
            error = "Agreement id does not exists"
            raise exceptions.APIException(error)

        audit_report, created = AuditReport.objects.get_or_create(
            agreement=agreement.first()
        )
        serializer = self.serializer_class(audit_report)
        data = serializer.data
        data["can_be_published"] = audit_report.can_be_published()
        return Response(data, status=status.HTTP_200_OK)


class AuditReportApiView(generics.UpdateAPIView):

    queryset = AuditReport.objects.all()
    serializer_class = AuditReportSerializer
    permission_classes = (
        IsAuthenticated,
        AuditReportPermission,
    )

    def patch(self, request, *args, **kwargs):

        if type(request.data) == dict:
            return self.partial_update(request, *args, **kwargs)
        else:
            files_list = request.data.getlist("common_file")
            attachments = []
            if len(files_list) > 0:
                for file_obj in files_list:
                    common_file = CommonFile.objects.create(
                        file_field=file_obj, user=request.user
                    )
                    attachments.append(common_file.id)
                request.data.update(MultiValueDict({"attachments": attachments}))
        return self.partial_update(request, *args, **kwargs)


class ReportStatusAPIView(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        data = {"audit_report": "not_published", "mgmt_letter": "not_published"}
        audit_report = AuditReport.objects.filter(agreement_id=kwargs["agreement_id"])
        mgmt_letter = MgmtLetter.objects.filter(agreement_id=kwargs["agreement_id"])
        if audit_report.exists():
            data["audit_report"] = (
                "published" if audit_report.first().published else "not_published"
            )
        if mgmt_letter.exists():
            data["mgmt_letter"] = (
                "published" if mgmt_letter.first().published else "not_published"
            )
        return Response(data, status=status.HTTP_200_OK)


class EndOfAuditAssignmentNotification(generics.CreateAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer
    permission_classes = (
        IsAuthenticated,
        IsUNHCR | IsAUDIT,
    )

    def post(self, request, *args, **kwargs):
        year = kwargs["year"]
        membership = request.user.membership

        agreements = filter_project_selection(self.queryset, year, "fin", "included")

        audit_assignment_pending = agreements.filter(audit_firm__isnull=True)
        if audit_assignment_pending.exists():
            error = "Audit firm is not assigned to all the projects"
            raise exceptions.APIException(error)
        else:
            if membership["type"] == UserTypeEnum.HQ.name:
                notify_end_of_audit_assignment(year, membership["type"], None)
            elif membership["type"] == UserTypeEnum.AUDITOR.name:
                notify_end_of_audit_assignment(
                    year, membership["type"], membership["audit_agency"]["id"]
                )
            return Response({}, status=status.HTTP_200_OK)
