from django.apps import apps
from django.db.models import fields
from rest_framework import serializers
from django.urls import reverse
from auditing.models import (
    AuditAgency,
    AuditMember,
    Role as AuditRole,
    AuditWorksplitGroup,
    AuditReport,
)
from common.models import BusinessUnit, Country
from common.models import CommonFile
from rest_framework import exceptions
from django.db import transaction
from common.models import Comment
from common.models import CommonFile
from common.serializers import CommonFileSerializer
from django.utils import timezone


class AuditMemberSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    user = serializers.ReadOnlyField(source="user.fullname")
    audit_agency = serializers.ReadOnlyField(source="audit_agency.legal_name")
    role = serializers.ReadOnlyField(source="role.role")

    class Meta:
        model = AuditMember
        fields = ("user", "audit_agency", "role", "url")


class CountrySerialzier(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ["id", "code", "name"]


class AuditAgencyListSerializer(serializers.ModelSerializer):
    audit_members = serializers.SerializerMethodField()
    country = CountrySerialzier(read_only=True)

    def get_audit_members(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("audits:auditmember-list", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = AuditAgency
        fields = "__all__"
        extra_fields = [
            "audit_members",
        ]


class AuditMemberShortSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.fullname")
    audit_agency = serializers.ReadOnlyField(source="audit_agency.legal_name")
    office = serializers.ReadOnlyField(source="audit_agency.legal_name")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = AuditMember
        fields = (
            "id",
            "user",
            "audit_agency",
            "role",
            "office",
        )

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default,
                "user_type_role_id": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .id,
            }
        )
        return serializer


class AuditMemberDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuditMember
        fields = [
            "id",
        ]

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            if key == "role":
                value = AuditRole.objects.filter(role=value).last()
            if key == "agency":
                value = AuditAgency.objects.filter(legal_name=value).last()
                key = "audit_agency"
            if key == "name":
                instance.update_user_info(key, value)
                continue
            setattr(instance, key, value)
        instance.update()
        return instance

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        user = instance.user
        role = instance.role
        agency = instance.audit_agency
        serializer.update(
            {
                "name": user.fullname,
                "email": user.email,
                "role": {"role": role.role, "id": role.id},
                "agency": {"id": agency.id, "legal_name": agency.legal_name},
            }
        )
        return serializer


class CountrySerialzier(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ["id", "code", "name"]


class AuditAgencyListSerializer(serializers.ModelSerializer):
    audit_members = serializers.SerializerMethodField()
    country = CountrySerialzier(read_only=True)

    def get_audit_members(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("audits:auditmember-list", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = AuditAgency
        fields = "__all__"
        extra_fields = [
            "audit_members",
        ]


class AuditAgencySerializerDetails(serializers.ModelSerializer):
    # audit_members = serializers.SerializerMethodField()
    country = CountrySerialzier(read_only=True)

    class Meta:
        model = AuditAgency
        fields = "__all__"
        # extra_fields = ['audit_members', ]


class AuditAgencySerializer(serializers.ModelSerializer):
    audit_members = serializers.SerializerMethodField()

    def get_audit_members(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("audits:auditmember-list", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = AuditAgency
        fields = "__all__"
        extra_fields = [
            "audit_members",
        ]


class AuditAgencyNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuditAgency
        fields = ("id", "legal_name")


class AuditRoleSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("audits:role-detail", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = AuditRole
        fields = "__all__"
        extra_fields = [
            "url",
        ]
        read_only_fields = [
            "role",
        ]


class BusinessUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessUnit
        fields = ["id", "name", "code"]


class WorkSplitGroupListSerializer(serializers.ModelSerializer):
    business_units = BusinessUnitSerializer(read_only=True, many=True)

    class Meta:
        model = AuditWorksplitGroup
        fields = ["id", "name", "business_units"]


class WorkSplitGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuditWorksplitGroup
        fields = ["id", "name", "business_units"]


class AuditReportsSerializer(serializers.ModelSerializer):
    attachments = CommonFileSerializer(many=True)

    class Meta:
        model = AuditReport
        exclude = [
            "last_modified_date",
        ]


class AuditReportSerializer(serializers.ModelSerializer):
    def validate(self, data):
        published = data.get("published", None)
        request = self.context["request"]
        if published == True:
            if request.method == "PATCH" and self.instance.can_be_published() == False:
                error = "Audit report can't be published, please check if all the required information has been update"
                raise serializers.ValidationError(error)
            else:
                data["publish_date"] = timezone.now()
        return data

    class Meta:
        model = AuditReport
        exclude = [
            "last_modified_date",
        ]
