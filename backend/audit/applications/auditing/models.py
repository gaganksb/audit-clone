from django.apps import apps
from django.db import models
from django.conf import settings
from common.models import Permission, Country, BusinessUnit
from audit.core.abstract_model import AbstractModel
from common.models import CommonFile

OPINION_CHOICES = (
    ("unmodified", "Unmodified"),
    ("qualified", "Qualified"),
    ("adverse", "Adverse"),
    ("disclaimer", "Disclaimer"),
)

# Create your models here.
class AuditAgency(AbstractModel):
    legal_name = models.CharField(max_length=255, unique=True)
    address_1 = models.CharField(max_length=255, null=True)
    address_2 = models.CharField(max_length=255, null=True)
    city = models.CharField(max_length=255, null=True)
    telephone = models.CharField(max_length=255, null=True, default=None)
    fullname = models.CharField(max_length=255, null=True, default=None)
    email = models.CharField(max_length=255, null=True, default=None)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name_plural = "Audit agencies"

    def __str__(self):
        return self.legal_name


class Role(AbstractModel):
    role = models.CharField(max_length=255, unique=True, null=False)
    permissions = models.ManyToManyField(Permission, related_name="role_permission")

    def __str__(self):
        return self.role


class AuditMember(AbstractModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="audit_members"
    )
    audit_agency = models.ForeignKey(
        AuditAgency,
        on_delete=models.CASCADE,
        null=False,
        related_name="audit_agency_members",
    )
    role = models.ForeignKey(Role, on_delete=models.CASCADE, null=False)
    temporary_password = models.CharField(max_length=255, null=True, default=None)
    date_send_invitation = models.DateField(null=True, default=None)
    is_password_change = models.BooleanField(null=False, default=False)

    class Meta:
        unique_together = ("user", "audit_agency", "role")

    def __str__(self):
        return self.user.fullname

    def __init__(self, *args, **kwargs):
        self.user_key_mapping = {
            "name": "fullname",
            "email": "email",
        }
        self.user_type_role_model = apps.get_model("account.UserTypeRole")
        super().__init__(*args, **kwargs)

    def update_user_info(self, key, value):
        user = self.user
        setattr(user, self.user_key_mapping.get(key), value)
        user.save()

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        self.trigger_create()

    def trigger_create(self):
        utr = self.user_type_role_model(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
            user_id=self.user_id,
        )
        utr.save()

    def trigger_delete(self):
        utr_qs = self.user_type_role_model.objects.filter(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
        )
        if utr_qs.exists() is True:
            utr_qs.delete()
            self.delete()


class AuditWorksplitGroup(AbstractModel):
    name = models.CharField(max_length=255, null=False, unique=True)
    business_units = models.ManyToManyField(BusinessUnit)

    def __str__(self):
        return self.name


class AuditReport(AbstractModel):
    agreement = models.OneToOneField(
        "project.Agreement", related_name="audit_report", on_delete=models.CASCADE
    )
    published = models.BooleanField(default=False)
    publish_date = models.DateTimeField(null=True, default=None)
    overall_opinion = models.CharField(choices=OPINION_CHOICES, max_length=255)
    audit_opinion = models.TextField()
    basis_for_opinion = models.TextField()
    emphasis_of_matter = models.TextField()
    mgt_responsibilities = models.TextField()
    auditor_responsibilities = models.TextField()
    additional_comment = models.TextField(null=True, default=None)
    attachments = models.ManyToManyField(CommonFile)

    def can_be_published(self):
        return all(
            [
                self.overall_opinion != "",
                self.audit_opinion != "",
                self.basis_for_opinion != "",
                self.emphasis_of_matter != "",
                self.mgt_responsibilities != "",
                self.auditor_responsibilities != "",
                self.additional_comment != "",
            ]
        )

    class Meta:
        ordering = ["-id"]

    def __str__(self):
        return "[{}{}]".format(self.agreement.business_unit.code, self.agreement.number)
