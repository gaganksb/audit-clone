from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import BasePermission, IsAuthenticated
from auditing.models import AuditReport
from project.models import Agreement


class AuditReportPermission(BasePermission):
    def has_permission(self, request, view):
        report_id = request.resolver_match.kwargs.get("pk", None)
        audit_report = AuditReport.objects.filter(id=report_id).first()
        if audit_report is None:
            return False

        agreements = Agreement.objects.filter(id=audit_report.agreement_id)
        if agreements.exists():
            agreement = agreements.first()
            focal_points = agreement.focal_points.all()
            auditing_agreement_member = focal_points.filter(
                user_type="auditing"
            ).values_list("user_id", flat=True)
            # Agreement members not assigned for the project
            if focal_points.exists() == False:
                return False

            # Allow focal_points auditors to create audit_report if it doesn't exists
            if hasattr(agreement, "audit_report") == False and request.user.id in set(
                auditing_agreement_member
            ):
                return True

            # auditor agreement members can read and update the report
            if hasattr(agreement, "audit_report") == True and request.user.id in set(
                auditing_agreement_member
            ):

                return True

            # all agreement member and hq can view if the audit report is poblished
            if (
                hasattr(agreement, "audit_report") == True
                and request.method == "GET"
                and agreement.audit_report.published == True
                and (
                    request.user.id
                    in set(focal_points.values_list("user_id", flat=True))
                    or request.user.membership["type"] == "HQ"
                )
            ):

                return True
        return False


class AuditReportGetPermission(BasePermission):
    def has_permission(self, request, view):
        agreement_id = request.resolver_match.kwargs.get("pk", None)
        agreements = Agreement.objects.filter(id=agreement_id)
        if agreements.exists():
            agreement = agreements.first()
            focal_points = agreement.focal_points.all()
            auditing_agreement_member = focal_points.filter(
                user_type="auditing"
            ).values_list("user_id", flat=True)
            # Agreement members not assigned for the project
            if focal_points.exists() == False:
                return False

            # Allow focal_points auditors to create audit_report if it doesn't exists
            if hasattr(agreement, "audit_report") == False and request.user.id in set(
                auditing_agreement_member
            ):
                return True

            # auditor agreement members can read and update the report
            if hasattr(agreement, "audit_report") == True and request.user.id in set(
                auditing_agreement_member
            ):

                return True

            # all agreement member and hq can view if the audit report is poblished
            if (
                hasattr(agreement, "audit_report") == True
                and request.method == "GET"
                and agreement.audit_report.published == True
                and (
                    request.user.id
                    in set(focal_points.values_list("user_id", flat=True))
                    or request.user.membership["type"] == "HQ"
                )
            ):

                return True
        return False
