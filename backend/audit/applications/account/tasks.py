from account.models import UserTypeRole, mapping_user_type
from notify.models import NotifiedUser
from notify.helpers import send_notification_to_users
from django.db import transaction
from background_task import background
import logging

logger = logging.getLogger("console")


@background(schedule=1)
def send_invitation_to_user():
    try:
        with transaction.atomic():
            user = NotifiedUser.objects.select_for_update().filter(
                sent_as_email=False, notification__name="Invitation"
            )
            send_notification_to_users(user)
    except Exception as e:
        logger.info(str(e))
