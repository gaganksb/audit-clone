from __future__ import unicode_literals
from django.apps import apps
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from random import choice

from auditing.models import AuditMember
from field.models import FieldMember
from partner.models import PartnerMember
from unhcr.models import UNHCRMember
from audit.core.abstract_model import AbstractModel
from common.enums import UserTypeEnum
from django.utils import timezone


mapping_user_type = {
    "unhcrmember": UNHCRMember,
    "fieldmember": FieldMember,
    "auditmember": AuditMember,
    "partnermember": PartnerMember,
    "HQ": UNHCRMember,
    "FO": FieldMember,
    "AUDITOR": AuditMember,
    "PARTNER": PartnerMember,
    "unhcr": "HQ Member",
    "field": "Field Member",
    "auditing": "Auditor",
    "partner": "Partner Member",
}


class UserManager(BaseUserManager):
    def _create_user(self, fullname, email, password, is_staff, is_superuser, **kwargs):
        email = self.normalize_email(email)
        user = self.model(
            fullname=fullname,
            email=email,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            **kwargs
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, **kwargs):
        return self._create_user(
            kwargs.pop("fullname", None),
            kwargs.pop("email", None),
            kwargs.pop("password", None),
            False,
            False,
            **kwargs
        )

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(None, email, password, **extra_fields)


class LowercaseEmailField(models.EmailField):
    """
    Override EmailField to convert emails to lowercase before saving.
    """

    def to_python(self, value):
        """
        Convert email to lowercase.
        """
        value = super(LowercaseEmailField, self).to_python(value)
        # Value can be None so check that it's a string before lowercasing.
        if isinstance(value, str):
            return value.lower()
        return value


class User(AbstractBaseUser, PermissionsMixin):

    fullname = models.CharField(
        "fullname",
        null=True,
        blank=True,
        max_length=512,
        help_text="Your fullname like first and last name, 512 characters.",
    )

    email = LowercaseEmailField("email address", max_length=254, unique=True)

    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        "active",
        default=True,
        help_text=(
            "Designates whether this user should be treated as active. "
            "Deselect this instead of deleting accounts."
        ),
    )
    phone = models.CharField("phone", max_length=254, null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)

    @property
    def is_unhcr_user(self):
        return self.unhcr_members.exists()

    @property
    def is_audit_user(self):
        return self.audit_members.exists()

    @property
    def is_field_user(self):
        return self.field_members.exists()

    @property
    def is_partner_user(self):
        return self.partner_members.exists()

    @property
    def user_type(self):
        if self.is_unhcr_user:
            return "unhcr"
        elif self.is_audit_user:
            return "audit"
        elif self.is_field_user:
            return "field"
        elif self.is_partner_user:
            return "partner"
        else:
            return "other"

    @property
    def user_role(self):
        if self.is_unhcr_user:
            return UNHCRMember.objects.get(user__id=self.id).role.role
        elif self.is_audit_user:
            return AuditMember.objects.get(user__id=self.id).role.role
        elif self.is_field_user:
            return FieldMember.objects.get(user__id=self.id).role.role
        elif self.is_partner_user:
            return PartnerMember.objects.get(user__id=self.id).role.role
        else:
            return "other"

    @property
    def role(self):
        if self.is_unhcr_user:
            return UNHCRMember.objects.get(user__id=self.id).role
        elif self.is_audit_user:
            return AuditMember.objects.get(user__id=self.id).role
        elif self.is_field_user:
            return FieldMember.objects.get(user__id=self.id).role
        elif self.is_partner_user:
            return PartnerMember.objects.get(user__id=self.id).role
        else:
            return None

    @property
    def membership(self):
        user_type_role_qs = UserTypeRole.objects.filter(user=self, is_default=True)

        user_type_role = user_type_role_qs.last()
        resource_model = mapping_user_type.get(user_type_role.resource_model)

        if resource_model == UNHCRMember:
            membership = {}
            member = UNHCRMember.objects.filter(
                user__id=self.id, id=user_type_role.resource_id
            ).last()
            membership["id"] = member.id
            membership["type"] = UserTypeEnum.HQ.name
            membership["role"] = member.role.role
            return membership
        elif resource_model == AuditMember:
            membership = {}
            member = AuditMember.objects.filter(
                user__id=self.id, id=user_type_role.resource_id
            ).last()
            membership["id"] = member.id
            membership["type"] = UserTypeEnum.AUDITOR.name
            membership["role"] = member.role.role
            membership["audit_agency"] = {
                "id": member.audit_agency.id,
                "name": member.audit_agency.legal_name,
            }
            return membership
        elif resource_model == FieldMember:
            membership = {}
            member = FieldMember.objects.filter(
                user__id=self.id, id=user_type_role.resource_id
            ).last()
            membership["id"] = member.id
            membership["type"] = UserTypeEnum.FO.name
            membership["role"] = member.role.role
            membership["field_office"] = {
                "id": member.field_office.id,
                "name": member.field_office.name,
            }
            return membership
        elif resource_model == PartnerMember:
            membership = {}
            member = PartnerMember.objects.filter(
                user__id=self.id, id=user_type_role.resource_id
            ).last()
            membership["id"] = member.id
            membership["type"] = UserTypeEnum.PARTNER.name
            membership["role"] = member.role.role
            membership["partner"] = {
                "id": member.partner.id,
                "name": member.partner.legal_name,
            }
            return membership
        else:
            membership = {}
            membership["id"] = self.id
            membership["type"] = UserTypeEnum.OTHER.name
            membership["role"] = None
            return membership

    @property
    def gen_pass(self):
        chars = "abcdefghijklmnopqrstuvwxyz123456789"
        upper_char = "ABCDEFGHJKLMNPQRSTUVWXYZ"
        str_special = ".,?!:;-_"
        raw_pass = "".join([choice(chars) for i in range(8)])
        lower_pass = raw_pass.replace(choice(raw_pass), choice(str_special))
        gen_password = lower_pass.replace(choice(lower_pass), choice(upper_char))
        return gen_password

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = UserManager()

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"
        ordering = ["id"]

    def get_short_name(self):
        return self.fullname

    def get_fullname(self):
        return self.fullname

    def __str__(self):
        return self.email


class UserProfile(AbstractModel):
    """
    User Profile model related with user as profile.

    related models:
        account.User (OneToOne): "user"
    """

    user = models.OneToOneField(User, related_name="profile", on_delete=models.CASCADE)

    def __str__(self):
        return "[{}] {}".format(self.user.email, self.user.get_fullname())

    @classmethod
    def create_user_profile(cls, sender, instance, created, **kwargs):
        """
        Signal handler to create user profiles automatically
        """
        if created:
            cls.objects.create(user=instance)


class UserTypeRole(models.Model):
    resource_app_label = models.CharField(
        max_length=64, null=True, default="", blank=True
    )
    resource_model = models.CharField(max_length=64, null=True, default="", blank=True)
    resource_id = models.IntegerField(null=True, default=None)
    user = models.ForeignKey(
        User,
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name="user_members",
    )
    is_default = models.BooleanField(null=False, default=False)

    def create_user_type(self, **kwargs):
        unhcr_role_model = apps.get_model("unhcr.Role")
        audit_role_model = apps.get_model("auditing.Role")
        audit_auditagency_model = apps.get_model("auditing.AuditAgency")
        field_role_model = apps.get_model("field.Role")
        partner_role_model = apps.get_model("partner.Role")
        partner_model = apps.get_model("partner.Partner")
        field_office_model = apps.get_model("field.FieldOffice")

        membership = kwargs.get("membership") or {}
        request_type = membership.get("type")
        request_role = membership.get("role")
        request_office_id = membership.get("office")
        request_agency_id = membership.get("agency")
        request_partner_id = membership.get("partner")
        user = kwargs.get("user")

        if request_type == UserTypeEnum.HQ.name:
            role = unhcr_role_model.objects.get(role=request_role)
            resource_instance = UNHCRMember(
                user=user, role=role, date_send_invitation=timezone.now().date()
            )
            resource_instance.save()
            return resource_instance
        elif request_type == UserTypeEnum.FO.name:
            role = field_role_model.objects.get(role=request_role)
            field_office = field_office_model.objects.get(id=request_office_id)
            resource_instance = FieldMember(
                user=user,
                role=role,
                field_office=field_office,
                date_send_invitation=timezone.now().date(),
            )
            resource_instance.save()
            return resource_instance

        elif request_type == UserTypeEnum.AUDITOR.name:
            role = audit_role_model.objects.get(role=request_role)
            audit_agency = audit_auditagency_model.objects.get(id=request_agency_id)
            resource_instance = AuditMember(
                user=user,
                role=role,
                audit_agency=audit_agency,
                date_send_invitation=timezone.now().date(),
            )
            resource_instance.save()
            return resource_instance
        elif request_type == UserTypeEnum.PARTNER.name:
            role = partner_role_model.objects.get(role=request_role)
            partner = partner_model.objects.get(id=request_partner_id)
            resource_instance = PartnerMember(
                user=user,
                role=role,
                partner=partner,
                date_send_invitation=timezone.now().date(),
            )
            resource_instance.save()
            return resource_instance

    def get_member_info(self, **kwargs):
        resource_model = kwargs.get("resource_model")
        resource_id = kwargs.get("resource_id")
        resource_instance_qs = mapping_user_type.get(resource_model).objects.filter(
            id=resource_id
        )
        if resource_instance_qs.exists() is False:
            return "", ""

        resource_instance = resource_instance_qs.last()
        role = resource_instance.role.role if resource_instance.role else ""
        office = self.get_office(resource_instance=resource_instance)
        return role, office

    def get_office(self, **kwargs):
        resource_instance = kwargs.get("resource_instance")
        resource_model = resource_instance._meta.model

        if resource_model == UNHCRMember:
            office = ""
        elif resource_model == AuditMember:
            office = getattr(resource_instance, "audit_agency").legal_name
        elif resource_model == FieldMember:
            office = getattr(resource_instance, "field_office").name
        elif resource_model == PartnerMember:
            office = getattr(resource_instance, "partner").legal_name
        else:
            office = ""
        return office

    @classmethod
    def get_field_office(cls, **kwargs):
        user = kwargs.get("user")
        resource_model = kwargs.get("resource_model")

        utr_qs = UserTypeRole.objects.filter(
            user=user,
            is_default=True,
            resource_model=resource_model,
            resource_app_label="field",
        )

        if utr_qs.exists() is False:
            fo = None
        else:
            utr = utr_qs.last()
            resource_instance_qs = mapping_user_type.get(
                utr.resource_model
            ).objects.filter(id=utr.resource_id)
            resource_instance = resource_instance_qs.last()
            fo = resource_instance.field_office
        return fo


post_save.connect(UserProfile.create_user_profile, sender=User)
