from .models import User
from django.db.models import Q, F
from django_filters import FilterSet
from django_filters.filters import CharFilter, NumberFilter, BooleanFilter
from partner.models import PartnerMember
from field.models import FieldMember
from auditing.models import AuditMember


class AccountFilter(FilterSet):
    fullname = CharFilter(field_name="fullname", lookup_expr="icontains")
    email = CharFilter(field_name="email", lookup_expr="icontains")
    has_account = CharFilter(method="filter_has_account")
    user_type = CharFilter(method="filter_user_type")
    user_role = CharFilter(method="filter_user_role")
    office = CharFilter(method="filter_office")
    partner = NumberFilter(method="filter_partner")
    office_id = NumberFilter(method="filter_office_id")
    audit_agency_id = NumberFilter(method="filter_audit_agency_id")

    def filter_office_id(self, queryset, name, value):
        field_members = FieldMember.objects.filter(field_office_id=value).values_list(
            "user_id", flat=True
        )
        return queryset.filter(id__in=field_members)

    def filter_audit_agency_id(self, queryset, name, value):
        audit_members = AuditMember.objects.filter(audit_agency_id=value).values_list(
            "user_id", flat=True
        )
        return queryset.filter(id__in=audit_members)

    def filter_partner(self, queryset, name, value):
        partner_members = PartnerMember.objects.filter(partner_id=value).values_list(
            "user_id", flat=True
        )
        return queryset.filter(id__in=partner_members)

    def filter_has_account(self, queryset, name, value):
        if value == "auditing":
            return queryset.filter(~Q(audit_members=None))
        elif value == "partner":
            return queryset.filter(~Q(partner_members=None))
        elif value == "field":
            return queryset.filter(~Q(field_members=None))
        elif value == "unhcr":
            return queryset.filter(~Q(unhcr_members=None))
        else:
            return queryset

    def filter_user_type(self, queryset, name, value, *args, **kwargs):
        inter_method_call = (
            True if len(args) == 1 and args[0] == "internal_method" else False
        )
        if value == "Auditor":
            audit_member_qs = queryset.annotate(
                is_audit_members=F("audit_members"),
                role__role=F("audit_members__role__role"),
                office_legal_name=F("audit_members__audit_agency__legal_name"),
            ).filter(~Q(is_audit_members=None))
            return audit_member_qs.distinct("id")
        elif value == "Partner":
            partner_member_qs = queryset.annotate(
                is_partner_members=F("partner_members"),
                role__role=F("partner_members__role__role"),
                office_legal_name=F("partner_members__partner__legal_name"),
            ).filter(~Q(is_partner_members=None))
            return partner_member_qs.distinct("id")
        elif value == "Field":
            field_member_qs = queryset.annotate(
                is_field_members=F("field_members"),
                role__role=F("field_members__role__role"),
                office_legal_name=F("field_members__field_office__name"),
            ).filter(~Q(is_field_members=None))
            return field_member_qs.distinct("id")
        elif value == "HQ":
            unhcr_member_qs = queryset.annotate(
                is_unhcr_members=F("unhcr_members"),
                role__role=F("unhcr_members__role__role"),
            ).filter(~Q(is_unhcr_members=None))
            return unhcr_member_qs.distinct("id")
        return queryset

    def filter_user_role(self, queryset, name, value):
        user_type, role = value.split("__")
        filter_data = self.filter_user_type(queryset, name, user_type)
        return filter_data.filter(role__role=role)

    def filter_office(self, queryset, name, value):
        user_type, legal_name = value.split("__")
        if user_type != "HQ":
            filter_data = self.filter_user_type(queryset, name, user_type)
            return filter_data.filter(office_legal_name__icontains=legal_name)
        return queryset.none()

    class Meta:
        model = User
        fields = [
            "fullname",
            "email",
            "has_account",
            "user_type",
            "user_role",
            "office",
        ]
