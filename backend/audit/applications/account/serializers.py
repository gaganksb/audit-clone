from django.apps import apps
from rest_framework import serializers
from account.models import User, UserTypeRole


class BasicUserSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    name = serializers.CharField(source="fullname", read_only=False)
    password = serializers.CharField(required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "url",
            "user_type",
            "user_role",
            "is_active",
            "membership",
            "password",
        )

    def create(self, validated_data):
        user = User(**validated_data)
        user.save()
        return user

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        del serializer["password"]
        return serializer


class UserTypeRoleSerializer(serializers.ModelSerializer):
    field_member_model = apps.get_model("field.FieldMember")
    partner_member_model = apps.get_model("partner.PartnerMember")
    audit_member_model = apps.get_model("auditing.AuditMember")
    unhcr_member_model = apps.get_model("unhcr.UNHCRMember")
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    name = serializers.CharField(source="fullname", read_only=False)

    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "is_active",
            "url",
            "membership",
        )

    def to_representation(self, instance):
        from field.serializers import FieldMemberShortSerializer
        from partner.serializers import PartnerMemberShortSerializer
        from auditing.serializers import AuditMemberShortSerializer
        from unhcr.serializers import UNHCRMemberShortSerializer

        serializer = super().to_representation(instance)
        field_members = FieldMemberShortSerializer(
            self.field_member_model.objects.filter(
                id__in=UserTypeRole.objects.filter(
                    user=instance,
                    resource_model=getattr(self.field_member_model, "_meta").model_name,
                    resource_app_label=getattr(
                        self.field_member_model, "_meta"
                    ).app_label,
                ).values_list("resource_id", flat=True)
            ),
            many=True,
        ).data
        partner_members = PartnerMemberShortSerializer(
            self.partner_member_model.objects.filter(
                id__in=UserTypeRole.objects.filter(
                    user=instance,
                    resource_model=getattr(
                        self.partner_member_model, "_meta"
                    ).model_name,
                    resource_app_label=getattr(
                        self.partner_member_model, "_meta"
                    ).app_label,
                ).values_list("resource_id", flat=True)
            ),
            many=True,
        ).data
        audit_members = AuditMemberShortSerializer(
            self.audit_member_model.objects.filter(
                id__in=UserTypeRole.objects.filter(
                    user=instance,
                    resource_model=getattr(self.audit_member_model, "_meta").model_name,
                    resource_app_label=getattr(
                        self.audit_member_model, "_meta"
                    ).app_label,
                ).values_list("resource_id", flat=True)
            ),
            many=True,
        ).data
        unhcr_members = UNHCRMemberShortSerializer(
            self.unhcr_member_model.objects.filter(
                id__in=UserTypeRole.objects.filter(
                    user=instance,
                    resource_model=getattr(self.unhcr_member_model, "_meta").model_name,
                    resource_app_label=getattr(
                        self.unhcr_member_model, "_meta"
                    ).app_label,
                ).values_list("resource_id", flat=True)
            ),
            many=True,
        ).data
        serializer.update(
            {
                "accounts": {
                    "field_members": field_members,
                    "partner_members": partner_members,
                    "audit_members": audit_members,
                    "unhcr_members": unhcr_members,
                }
            }
        )
        return serializer
