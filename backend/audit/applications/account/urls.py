from django.conf.urls import url
from django.urls import path

from account.views import (
    AccountCurrentUserRetrieveAPIView,
    AccountList,
    AccountDetail,
    AccountUserTypeView,
    AccountUserTypeDetail,
    AccountUserTypeDetailSetDefault,
    SocialAuthLoggedInUserView,
    SocialAuthLoginView,
    SetDefaultAPIView,
    UserExportAPIView,
)


urlpatterns = [
    path("", AccountList.as_view(), name="user-list"),
    path("<int:pk>/", AccountDetail.as_view(), name="user-detail"),
    path("me/", AccountCurrentUserRetrieveAPIView.as_view(), name="my-account"),
    path(
        "social-login/<str:backend>/",
        SocialAuthLoginView.as_view(),
        name="social-login",
    ),
    path(
        "social-logged-in/",
        SocialAuthLoggedInUserView.as_view(),
        name="social-logged-in",
    ),
    path(
        "user-type/<int:user_id>/", AccountUserTypeView.as_view(), name="user-type-add"
    ),
    path(
        "user-type/<int:membership_id>/<str:membership_type>/",
        AccountUserTypeDetail.as_view(),
        name="user-type-detail",
    ),
    path(
        "user-type/<int:membership_id>/<str:membership_type>/set-default/",
        AccountUserTypeDetailSetDefault.as_view(),
        name="user-type-detail-default",
    ),
    path(
        "set-default/<int:pk>/<int:user_id>/",
        SetDefaultAPIView.as_view(),
        name="set-default-view",
    ),
    path(
        "user-export/",
        UserExportAPIView.as_view(),
        name="user-export-view",
    ),
]
