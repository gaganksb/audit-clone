from auditing.serializers import AuditMemberSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, status, exceptions
from rest_framework.response import Response
from django.contrib.auth.hashers import (
    make_password,
)
from django.db import transaction
from django.http import HttpResponse
from django.utils import timezone
from account.serializers import BasicUserSerializer, UserTypeRoleSerializer
from account.models import User, UserTypeRole, mapping_user_type
from account.tasks import send_invitation_to_user
from common.enums import UserTypeEnum

from auditing.models import (
    Role as AuditRole,
    AuditMember,
    AuditAgency,
)
from field.models import (
    Role as FieldRole,
    FieldMember,
    FieldOffice,
)
from partner.models import (
    Role as PartnerRole,
    PartnerMember,
    Partner,
)

from common.permissions import IsUNHCR, IsSuperUser
from common.enums import PermissionListEnum
from unhcr.models import UNHCRMember, Role as UNHCRRole
from utils.rest.code import code
import csv

from django.db.models import Q
from django.conf import settings
from rest_auth.models import TokenModel
from rest_auth.utils import default_create_token
from django.contrib.auth import logout
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import RedirectView
from notify.helpers import send_notification
from .helpers import save_user, verify_user_member
from django_filters.rest_framework import DjangoFilterBackend
from .filters import AccountFilter


class AccountCurrentUserRetrieveAPIView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserTypeRoleSerializer

    def get_object(self):
        return self.request.user


class AccountList(generics.ListCreateAPIView):
    user_type_role_model = UserTypeRole
    serializer_class = UserTypeRoleSerializer
    queryset = User.objects.all().order_by("fullname")
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)
    filter_class = AccountFilter

    def post_existing_user(self, request, *args, **kwargs):
        membership = request.data.get("membership") or {}
        user_id = kwargs.get("user_id")
        resource_model_qs = User.objects.filter(id=user_id)
        user = resource_model_qs.last()

        resource_model_qs = UserTypeRole.objects.filter(user_id=user_id)
        self.user_type_role_model().create_user_type(membership=membership, user=user)
        if resource_model_qs.count() == 1:
            resource_model_qs.update(is_default=False)
        res = {"details": "Ok"}
        return Response(res, status=status.HTTP_201_CREATED)

    def get_queryset(self):
        field = self.request.query_params.get("field")
        order = self.request.query_params.get("order")
        my_own = self.request.query_params.get("my_own")

        queryset = User.objects.all()

        if my_own is not None:
            membership = self.request.user.membership
            request_type = membership.get("type")
            if request_type == UserTypeEnum.HQ.name:
                queryset = queryset.filter(~Q(unhcr_members=None)).distinct("id")
            elif request_type == UserTypeEnum.FO.name:
                my_org_id = list(
                    FieldMember.objects.filter(
                        user_id=self.request.user.id,
                        field_office_id=membership["field_office"]["id"],
                    ).values_list("field_office_id", flat=True)
                )
                queryset = queryset.filter(field_members__field_office_id__in=set(my_org_id)).distinct("id")
            elif request_type == UserTypeEnum.AUDITOR.name:
                my_org_id = list(
                    AuditMember.objects.filter(
                        user_id=self.request.user.id,
                        audit_agency_id=membership["audit_agency"]["id"],
                    ).values_list("audit_agency_id", flat=True)
                )
                queryset = queryset.filter(
                    audit_members__audit_agency_id__in=my_org_id
                ).distinct("id")
            elif request_type == UserTypeEnum.PARTNER.name:
                my_org_id = list(
                    PartnerMember.objects.filter(
                        user_id=self.request.user.id,
                        partner_id=membership["partner"]["id"],
                    ).values_list("partner_id", flat=True)
                )
                queryset = queryset.filter(
                    partner_members__partner_id__in=my_org_id
                ).distinct("id")
        if field:
            if order == "asc":
                queryset = queryset.order_by(field)
            else:
                queryset = queryset.order_by("-" + field)
        return queryset

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        from account.models import User

        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            # TODO: implement backend permissions (probably some logic bugs are already present)
            if serializer.errors["email"][0].code == "unique":
                user_id = User.objects.filter(email=request.data.get("email"))[0].id
                return self.post_existing_user(request, *args, user_id=user_id)
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )
        my_membership = self.request.user.membership
        my_membership_type = my_membership.get("type")
        my_membership_role = my_membership.get("role")
        membership = request.data.get("membership") or {}
        request_type = membership.get("type")
        error_msg = False

        if my_membership_type == UserTypeEnum.HQ.name:
            user = save_user(request.data)
        elif my_membership_type == UserTypeEnum.FO.name and (
            my_membership_role == "APPROVER" or my_membership_role == "REVIEWER"
        ):
            field_office = my_membership.get("field_office")
            request_office_id = membership.get("office")
            if request_type not in ["PARTNER", "FO"]:
                error_msg = True
            if request_type == "FO" and my_membership_type == "REVIEWER":
                error_msg = True
            if request_type == "PARTNER":
                user = save_user(request.data)
            elif request_type == "FO":
                if request_office_id != field_office.get("id"):
                    error_msg = True
                else:
                    user = save_user(request.data)
            else:
                error_msg = True
        elif (
            my_membership_type == UserTypeEnum.AUDITOR.name
            and my_membership_role == "ADMIN"
        ):
            audit_agency = my_membership.get("audit_agency")
            request_agency_id = membership.get("agency")
            if request_type != "AUDITOR":
                error_msg = True
            elif request_agency_id != audit_agency.get("id"):
                error_msg = True
            else:
                user = save_user(request.data)
        elif (
            my_membership_type == UserTypeEnum.PARTNER.name
            and my_membership_role == "ADMIN"
        ):
            partner = my_membership.get("partner")
            request_partner_id = membership.get("partner")
            if request_type != "PARTNER":
                error_msg = True
            elif request_partner_id != partner.get("id"):
                error_msg = True
            else:
                user = save_user(request.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if error_msg == True:
            error = "Permission denied"
            raise exceptions.APIException(error)

        protocol = "http" if settings.DEBUG else "https"
        invitation_link = f"{protocol}://{settings.FRONTEND_HOST}"

        # Do not send invitation to the partner if the partner is created by field office in field assessment phase
        if request_type == "PARTNER":
            res = self.serializer_class(user, many=False, context={"request": request})
            return Response(res.data, status=status.HTTP_201_CREATED)

        send_notification(
            notification_type="user_invitation",
            obj=user,
            users=[user],
            context={
                "email": user.email,
                "subject": "Invitation",
                "invitation_link": invitation_link,
            },
        )
        send_invitation_to_user()
        res = self.serializer_class(user, many=False, context={"request": request})
        return Response(res.data, status=status.HTTP_201_CREATED)


class AccountDetail(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = BasicUserSerializer
    permission_classes = (IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        user = User.objects.filter(id=kwargs["pk"])
        my_membership = self.request.user.membership
        if user.exists():
            if verify_user_member(my_membership, user.last()) == True:
                return self.partial_update(request, *args, **kwargs)
            else:
                error = "The requested resource is not found"
                raise exceptions.APIException(error)


class AccountUserTypeView(generics.ListCreateAPIView):
    user_type_role_model = UserTypeRole
    serializer_class = UserTypeRoleSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        my_membership = self.request.user.membership
        membership = request.data.get("membership") or {}
        user_id = kwargs.get("user_id")
        error = "The requested resource is not found"
        request_role = membership.get("role")
        resource_model_qs = User.objects.filter(id=user_id)
        if resource_model_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        user = resource_model_qs.last()
        my_membership_type = my_membership.get("type")
        my_membership_role = my_membership.get("role")
        verify_user_check = False
        if my_membership_type == UserTypeEnum.HQ.name:
            verify_user_check = True

        elif (
            my_membership_type == UserTypeEnum.FO.name
            and my_membership_role == "APPROVER"
        ):
            field_office = my_membership.get("field_office")
            my_own = user.membership.get("field_office", None)
            if membership.get("type") == UserTypeEnum.PARTNER.name:
                verify_user_check = True
            if my_own is not None and membership.get("type") == UserTypeEnum.FO.name:
                if my_own.get("id") == field_office.get("id"):
                    request_office_id = membership.get("office")
                    field = FieldMember.objects.filter(
                        field_office_id=request_office_id,
                        role__role=request_role,
                        user_id=user.id,
                    )
                    if field.exists() == False:
                        verify_user_check = True
                    else:
                        error = "Role already exists"

        elif (
            my_membership_type == UserTypeEnum.FO.name
            and my_membership_role == "REVIEWER"
        ):
            field_office = my_membership.get("field_office")
            my_own = user.membership.get("field_office", None)
            if membership.get("type") == UserTypeEnum.PARTNER.name:
                verify_user_check = True

        elif (
            my_membership_type == UserTypeEnum.AUDITOR.name
            and my_membership_role == "ADMIN"
        ):
            audit_agency = my_membership.get("audit_agency")
            my_own = user.membership.get("audit_agency", None)
            if (
                my_own is not None
                and membership.get("type") == UserTypeEnum.AUDITOR.name
            ):
                if my_own.get("id") == audit_agency.get("id"):
                    request_agency_id = membership.get("agency")
                    agency = AuditMember.objects.filter(
                        audit_agency_id=request_agency_id,
                        role__role=request_role,
                        user_id=user.id,
                    )
                    if agency.exists() == False:
                        verify_user_check = True
                    else:
                        error = "Role already exists"

        elif (
            my_membership_type == UserTypeEnum.PARTNER.name
            and my_membership_role == "ADMIN"
        ):
            partner = my_membership.get("partner")
            my_own = user.membership.get("partner", None)
            if (
                my_own is not None
                and membership.get("type") == UserTypeEnum.PARTNER.name
            ):
                if my_own.get("id") == partner.get("id"):
                    request_partner_id = membership.get("partner")
                    partner = PartnerMember.objects.filter(
                        partner_id=request_partner_id,
                        role__role=request_role,
                        user_id=user.id,
                    )
                    if partner.exists() == False:
                        verify_user_check = True
                    else:
                        error = "Role already exists"

        if verify_user_check == True:
            resource_model_qs = UserTypeRole.objects.filter(user_id=user_id)
            self.user_type_role_model().create_user_type(
                membership=membership, user=user
            )
            if resource_model_qs.count() == 1:
                resource_model_qs.update(is_default=True)
            res = self.serializer_class(user, many=False, context={"request": request})
            return Response(res.data, status=status.HTTP_201_CREATED)
        else:
            raise exceptions.APIException(error)

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get("user_id")
        resource_model_qs = UserTypeRole.objects.filter(user_id=user_id)

        if resource_model_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        user = resource_model_qs.last().user
        res = self.serializer_class(user, many=False, context={"request": request})
        return Response(res.data, status=status.HTTP_200_OK)


class AccountUserTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    user_type_role_model = UserTypeRole
    serializer_class = UserTypeRoleSerializer
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        my_membership = self.request.user.membership
        membership_id = kwargs.get("membership_id")
        membership_type = kwargs.get("membership_type")
        membership = request.data.get("membership") or {}
        resource_model = mapping_user_type.get(membership_type)

        resource_model_qs = resource_model.objects.filter(id=membership_id)
        if resource_model_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        resource_model_last = resource_model_qs.last()
        user = resource_model_qs.last().user
        my_membership_type = my_membership.get("type")
        my_membership_role = my_membership.get("role")

        verify_user_check = False
        if my_membership_type == UserTypeEnum.HQ.name:
            verify_user_check = True

        elif (
            my_membership_type == UserTypeEnum.FO.name
            and my_membership_role == "APPROVER"
        ):
            field_office = my_membership.get("field_office")
            my_own = user.membership.get("field_office", None)
            if my_own is not None and membership.get("type") == UserTypeEnum.FO.name:
                if my_own.get("id") == field_office.get("id"):
                    verify_user_check = True

        elif (
            my_membership_type == UserTypeEnum.AUDITOR.name
            and my_membership_role == "ADMIN"
        ):
            audit_agency = my_membership.get("audit_agency")
            my_own = user.membership.get("audit_agency", None)
            if (
                my_own is not None
                and membership.get("type") == UserTypeEnum.AUDITOR.name
            ):
                if my_own.get("id") == audit_agency.get("id"):
                    verify_user_check = True

        elif (
            my_membership_type == UserTypeEnum.PARTNER.name
            and my_membership_role == "ADMIN"
        ):
            partner = my_membership.get("partner")
            my_own = user.membership.get("partner", None)
            if (
                my_own is not None
                and membership.get("type") == UserTypeEnum.PARTNER.name
            ):
                if my_own.get("id") == partner.get("id"):
                    verify_user_check = True

        if verify_user_check == True:
            # delete current #
            resource_instance = resource_model_last
            resource_instance.trigger_delete()
            ##################
            self.user_type_role_model().create_user_type(
                membership=membership, user=user
            )
            res = self.serializer_class(user, many=False, context={"request": request})
            return Response(res.data, status=status.HTTP_201_CREATED)
        else:
            error = "The requested resource is not found"
            raise exceptions.APIException(error)

    def delete(self, request, *args, **kwargs):
        membership_id = kwargs.get("membership_id")
        membership_type = kwargs.get("membership_type")
        my_membership = self.request.user.membership
        resource_model = mapping_user_type.get(membership_type)
        resource_model_qs = resource_model.objects.filter(id=membership_id)
        if resource_model_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        resource_model_last = resource_model_qs.last()
        user = resource_model_last.user
        if verify_user_member(my_membership, user) == True:
            user_type_role_model = UserTypeRole
            user_type_role_qs = self.user_type_role_model.objects.filter(
                resource_model=membership_type, resource_id=membership_id
            )
            if user_type_role_qs.exists() is False:
                return Response(
                    {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                    status=status.HTTP_404_NOT_FOUND,
                )

            resource_instance = resource_model_last
            user_type_role = user_type_role_qs.last()
            if user_type_role.is_default == True:
                if user_type_role.user_id == self.request.user.id:
                    error = "You can not remove your own active account"
                    raise exceptions.APIException(error)

                other_accounts = self.user_type_role_model.objects.exclude(
                    id=membership_id
                ).filter(user_id=user_type_role.user.id, is_default=False)
                if other_accounts.exists():
                    new_default_account = other_accounts.first()
                    new_default_account.is_default = True
                    new_default_account.save()
            resource_instance.trigger_delete()
            return Response(
                {"code": "OK_DELETE", "message": code["OK_DELETE"]},
                status=status.HTTP_200_OK,
            )
        else:
            error = "The requested resource is not found"
            raise exceptions.APIException(error)


class AccountUserTypeDetailSetDefault(generics.RetrieveUpdateDestroyAPIView):
    user_type_role_model = UserTypeRole
    serializer_class = UserTypeRoleSerializer
    permission_classes = (IsAuthenticated,)

    def patch(self, request, *args, **kwargs):
        membership_id = kwargs.get("membership_id")
        membership_type = kwargs.get("membership_type")
        is_default = request.data.get("is_default") or None
        # get resouce model
        user_type_role_qs = self.user_type_role_model.objects.filter(
            resource_model=membership_type, resource_id=membership_id
        )
        if user_type_role_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        user_type_role = user_type_role_qs.last()
        if request.user != user_type_role.user:
            return Response(
                {"code": "E_PERMISSION_DENY", "message": code["E_PERMISSION_DENY"]},
                status=status.HTTP_403_FORBIDDEN,
            )
        user_type_role.is_default = is_default
        user_type_role.save(update_fields=["is_default"])
        user = user_type_role.user

        # set all another by default is False, exclude current
        self.user_type_role_model.objects.exclude(id=user_type_role.id).filter(
            user_id=user.id
        ).update(is_default=False)

        res = self.serializer_class(user, many=False, context={"request": request})
        return Response(res.data, status=status.HTTP_201_CREATED)


class SocialAuthLoggedInUserView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        protocol = "http" if settings.DEBUG else "https"
        token = default_create_token(TokenModel, self.request.user, None)

        return f"{protocol}://{settings.FRONTEND_HOST}/login/{token}"


class SocialAuthLoginView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        # Make sure session is properly cleared, in case frontend fails to do so
        logout(self.request)

        return reverse("social:begin", kwargs={"backend": self.kwargs["backend"]})


class SetDefaultAPIView(generics.RetrieveUpdateAPIView):

    queryset = UserTypeRole.objects.all()
    serializer_class = UserTypeRoleSerializer

    def patch(self, request, *args, **kwargs):
        response = {"message": "Error while updating the role"}
        try:
            user_type_role_qs = self.queryset.filter(
                user_id=kwargs["user_id"], id=kwargs["pk"]
            )

            if user_type_role_qs.exists():
                self.queryset.filter(user_id=kwargs["user_id"]).update(is_default=False)
                user_type_role_obj = user_type_role_qs.first()
                user_type_role_obj.is_default = True
                user_type_role_obj.save()
                return Response({}, status=status.HTTP_201_CREATED)
            else:
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class UserExportAPIView(generics.ListCreateAPIView):

    queryset = AuditMember.objects.all()
    serializer = AuditMemberSerializer

    def get(self, request, *args, **kwargs):

        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="users.csv"'

        column = [
            "Full Name",
            "Email",
            "User Role",
            "Is_Active",
            "Office/Agency/Partner ID",
            "Office/Agency/Partner Name",
            "User Type",
            
        ]
        members = [AuditMember, FieldMember, PartnerMember, UNHCRMember]
        writer = csv.writer(response)
        writer.writerow(column)

        for member in members:
            fields = ["user__fullname", "user__email", "role__role", "user__is_active"]
            if member.__name__ == "AuditMember":
                fields.extend(["audit_agency_id", "audit_agency__legal_name"])
                member_type = UserTypeEnum.AUDITOR.name
            elif member.__name__ == "FieldMember":
                fields.extend(
                    [
                        "field_office__fo_business_units",
                        "field_office__fo_business_units__code",
                    ]
                )
                member_type = UserTypeEnum.FO.name
            elif member.__name__ == "PartnerMember":
                fields.extend(["partner__number", "partner__legal_name"])
                member_type = UserTypeEnum.PARTNER.name
            else:
                member_type = UserTypeEnum.HQ.name

            for _item in member.objects.all().values_list(*fields):
                item = list(_item)
                if member_type == UserTypeEnum.HQ.name:
                    item.extend(["", ""])
                item.append(member_type)
                writer.writerow(item)
        return response
