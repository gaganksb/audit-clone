from auditing.models import (
    Role as AuditRole,
    AuditMember,
    AuditAgency,
)
from field.models import (
    Role as FieldRole,
    FieldMember,
    FieldOffice,
)
from partner.models import (
    Role as PartnerRole,
    PartnerMember,
    Partner,
)
from unhcr.models import UNHCRMember, Role as UNHCRRole
from account.models import User, UserTypeRole, mapping_user_type
from common.enums import UserTypeEnum
from django.utils import timezone
from rest_framework import generics, status, exceptions
from rest_framework.response import Response


def save_user(request):
    membership = request.get("membership") or {}
    request_type = membership.get("type")
    request_role = membership.get("role")
    error = "The requested resource is not found"

    if request_type == UserTypeEnum.HQ.name:

        role = UNHCRRole.objects.get(role=request_role)
        user = User.objects.create(
            fullname=request.get("name"), email=request.get("email")
        )
        unhcr_member = UNHCRMember(
            user=user, role=role, date_send_invitation=timezone.now().date()
        )
        unhcr_member.save()
    elif request_type == UserTypeEnum.FO.name:

        request_office_id = membership.get("office")
        role = FieldRole.objects.get(role=request_role)
        field_office = FieldOffice.objects.filter(id=request_office_id)
        if field_office.exists() == False:
            raise exceptions.APIException(error)
        user = User.objects.create(
            fullname=request.get("name"), email=request.get("email")
        )
        field_member = FieldMember(
            user=user,
            role=role,
            field_office=field_office.first(),
            date_send_invitation=timezone.now().date(),
        )
        field_member.save()
    elif request_type == UserTypeEnum.AUDITOR.name:

        role = AuditRole.objects.get(role=request_role)
        request_agency_id = membership.get("agency")
        audit_agency = AuditAgency.objects.filter(id=request_agency_id)
        if audit_agency.exists() == False:
            raise exceptions.APIException(error)
        user = User.objects.create(
            fullname=request.get("name"), email=request.get("email")
        )
        audit_member = AuditMember(
            user=user,
            role=role,
            audit_agency=audit_agency.first(),
            date_send_invitation=timezone.now().date(),
        )
        audit_member.save()
    elif request_type == UserTypeEnum.PARTNER.name:

        request_partner_id = membership.get("partner")
        role = PartnerRole.objects.get(role=request_role)
        partner = Partner.objects.filter(id=request_partner_id)
        if partner.exists() == False:
            raise exceptions.APIException(error)
        user = User.objects.create(
            fullname=request.get("name"), email=request.get("email")
        )
        partner_member = PartnerMember(
            user=user,
            role=role,
            partner=partner.first(),
            date_send_invitation=timezone.now().date(),
        )
        partner_member.save()

    user_type_account = UserTypeRole.objects.filter(user=user).last()
    user_type_account.is_default = True
    user_type_account.save()
    return user


def verify_user_member(my_membership, user):
    my_membership_type = my_membership.get("type")
    my_membership_role = my_membership.get("role")
    verify_user_check = False
    if my_membership_type == UserTypeEnum.HQ.name:
        verify_user_check = True

    elif (
        my_membership_type == UserTypeEnum.FO.name and my_membership_role == "APPROVER"
    ):
        field_office = my_membership.get("field_office")
        membership = user.membership.get("field_office", None)
        if membership is not None:
            if membership.get("id") == field_office.get("id"):
                verify_user_check = True
        elif user.membership["type"] == UserTypeEnum.PARTNER.name:
            verify_user_check = True
    elif (
        my_membership_type == UserTypeEnum.AUDITOR.name
        and my_membership_role == "ADMIN"
    ):
        audit_agency = my_membership.get("audit_agency")
        membership = user.membership.get("audit_agency", None)
        if membership is not None:
            if membership.get("id") == audit_agency.get("id"):
                verify_user_check = True

    elif (
        my_membership_type == UserTypeEnum.PARTNER.name
        and my_membership_role == "ADMIN"
    ):
        partner = my_membership.get("partner")
        membership = user.membership.get("partner", None)
        if membership is not None:
            if membership.get("id") == partner.get("id"):
                verify_user_check = True

    return verify_user_check
