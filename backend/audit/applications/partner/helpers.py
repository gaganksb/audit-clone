import os
import xlrd
from partner.models import Partner, AdverseDisclaimer
from icq.models import ICQReport
from common.models import BusinessUnit
from background_task import background
from project.models import Agreement


@background(schedule=5)
def import_icq_report(year, items):
    bu_list = []
    for i in items:
        try:
            partner, created = Partner.objects.get_or_create(
                number=i["implementer"], legal_name=i["partner_name"]
            )
            bu = BusinessUnit.objects.filter(code__icontains=i["bu"])
            if bu.exists() == False:
                bu_list.append(i["bu"])

            bu = bu.first() if bu.exists() else None
            icq, created = ICQReport.objects.get_or_create(
                partner=partner,
                overall_score_perc=i["score"],
                user_id=i["user_id"],
                year=i["year"],
                business_unit=bu,
                report_date="{0}-01-01".format(i["year"]),
            )
        except Exception as e:
            print("Error in import_icq_report", e)


# def import_adverse_disclaimer(year,items):
#     for i in items:
#         try:
#             agreement, created = Agreement.objects.get_or_create(number=i['implementer'], legal_name=i['partner_name'
#             advdscr, created = AdverseDisclaimer.objects.get_or_create(
#                 partner_code = i['partner_code'],
#                 ppa_code  i['ppa_code'],
#                 year = i['year'],
#                 business_unit = i['business_unit'],
#                 partner_code = i['partner_code2'],
#                 implementer_name = i['implementer_nam'],
#                 project_agreement = i['project_agreement'],
#                 modification_type = i['modification_type'],
#                 status = i['status']

#             )
#         except Exception as e:
#             print("Error in import_adverse_disclaimer", e)
