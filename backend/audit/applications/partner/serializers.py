from rest_framework import serializers
from django.urls import reverse
from django.apps import apps
from partner.models import (
    Partner,
    PartnerType,
    Role as PartnerRole,
    PartnerMember,
    PQPStatus,
    ImplementerType,
    AdverseDisclaimer,
)
from icq.models import *


class ImplementerTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImplementerType
        fields = "__all__"


class PartnerSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="partners:partner-detail")
    implementer_type = ImplementerTypeSerializer(read_only=True)

    class Meta:
        model = Partner
        fields = "__all__"
        extra_fields = [
            "url",
        ]


class PartnerTypeSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="partners:partnertype-detail")

    class Meta:
        model = PartnerType
        fields = "__all__"
        extra_fields = [
            "url",
        ]


class PartnerRoleSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("partners:role-detail", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = PartnerRole
        fields = "__all__"
        extra_fields = [
            "url",
        ]
        read_only_fields = [
            "role",
        ]


class PartnerMemberSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    user = serializers.ReadOnlyField(source="user.fullname")
    office = serializers.ReadOnlyField(source="partner.legal_name")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = PartnerMember
        fields = ("user", "partner", "role", "url")

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default,
                "user_type_role_id": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .id,
            }
        )
        return serializer


class PartnerMemberShortSerializer(serializers.ModelSerializer):
    partner = serializers.ReadOnlyField(source="partner.legal_name")
    office = serializers.ReadOnlyField(source="partner.legal_name")
    user = serializers.ReadOnlyField(source="user.fullname")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = PartnerMember
        fields = (
            "id",
            "user",
            "partner",
            "role",
            "office",
        )

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default,
                "user_type_role_id": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .id,
            }
        )
        return serializer


class PartnerMemberDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerMember
        fields = [
            "id",
        ]

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            if key == "role":
                value = PartnerRole.objects.filter(role=value).last()
            if key == "partner":
                value = Partner.objects.filter(legal_name=value).last()
            if key == "name":
                instance.update_user_info(key, value)
                continue
            setattr(instance, key, value)
        instance.update()
        return instance

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        user = instance.user
        role = instance.role
        partner = instance.partner
        serializer.update(
            {
                "name": user.fullname,
                "email": user.email,
                "role": {"role": role.role, "id": role.id},
                "partner": {"id": partner.id, "name": partner.legal_name},
            }
        )
        return serializer


class PQPStatusSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="partners:pqpstatus-detail")
    partner = PartnerSerializer(read_only=True)
    partner_id = serializers.PrimaryKeyRelatedField(
        required=True, queryset=Partner.objects.filter()
    )

    class Meta:
        model = PQPStatus
        fields = "__all__"
        extra_fields = [
            "url",
        ]

    def create(self, validated_data):
        pqp = self.Meta.model(**self.initial_data)
        pqp.save()
        return pqp


class AdverseDisclaimerSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdverseDisclaimer
        fields = "__all__"