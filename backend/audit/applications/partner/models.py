from django.apps import apps
from django.db import models
from django.db.models import Q
from django.conf import settings
from common.models import BusinessUnit, CommonFile, Permission

# from icq.models import ICQ
from decimal import Decimal
from audit.core.abstract_model import AbstractModel
import datetime
from django.core.validators import MinValueValidator, MaxValueValidator
from icq.models import ICQReport


SCOPE_OF_STATUS_CHOICES = (
    ("N", "National"),
    ("W", "WorldWide"),
    ("M", "Multi-Country"),
)

MODIFICATION_TYPE_OPTIONS = (
    ("adverse_opinion", "Adverse opinion"),
    ("disclaimer_of_opinion", "Disclaimer of opinion"),
)

MIN_YEAR = 1980
MAX_YEAR = 2050


class PartnerType(AbstractModel):
    partner_type = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.partner_type


class ImplementerType(AbstractModel):
    implementer_type = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=250, null=True)

    def __str__(self):
        return self.implementer_type


class Partner(AbstractModel):
    legal_name = models.CharField(max_length=255)
    number = models.CharField(max_length=255, unique=True)
    partner_type = models.ForeignKey(PartnerType, on_delete=models.CASCADE, null=True)
    implementer_type = models.ForeignKey(
        ImplementerType, on_delete=models.CASCADE, null=True
    )
    engagement_date = models.DateField(null=True, default=None)

    def latest_icq(self):
        year = datetime.datetime.now().year - 4
        report_date = str(year) + "-12-31"
        try:
            res = (
                ICQReport.objects.values_list("overall_score_perc", flat=True)
                .filter(Q(partner=self) & Q(report_date__gte=report_date))
                .order_by("-report_date")[0]
            )
            return res
        except Exception:
            return Decimal(0)

    @property
    def modified_in_last_4years(self):
        year = datetime.datetime.now().year - 4
        return (
            Modified.objects.filter(
                Q(partner=self) & Q(year__gte=year) & Q(modified=True)
            ).count()
            > 0
        )

    @property
    def partner_performance_test(self):
        if self.field_assessment is not None:
            return self.field_assessment.score.value
        return None

    def audit_test(self):
        outputs = [5, 4, 3, 2]
        if self.modified_in_last_4years():
            return outputs[0]
        elif self.latest_icq() <= 0:
            return outputs[0]
        elif 0.4 <= self.latest_icq() < 0.6:
            return outputs[1]
        elif 0.6 <= self.latest_icq() < 0.8:
            return outputs[2]
        elif 0.8 <= self.latest_icq() < 1:
            return outputs[3]
        else:
            return outputs[0]

    def __str__(self):
        return self.legal_name


class Modified(AbstractModel):
    year = models.IntegerField()
    partner = models.ForeignKey(Partner, on_delete=models.PROTECT)
    modified = models.BooleanField(default=False)
    commissioned = models.BooleanField()
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.PROTECT)

    def __str__(self):
        return "{} - {} - {}".format(self.year, self.partner.number, self.modified)


class PQPStatus(AbstractModel):
    granted_date = models.DateField(null=True, default=None)
    expiry_date = models.DateField(null=True, default=None)
    partner = models.ForeignKey(Partner, on_delete=models.PROTECT)
    national_worldwide = models.CharField(
        max_length=1, choices=SCOPE_OF_STATUS_CHOICES, null=True, default=None
    )

    def __str__(self):
        return "{} - {}".format(self.partner.number, self.national_worldwide)


class PQPStatusNew(AbstractModel):
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.CASCADE)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    agreement = models.ForeignKey("project.Agreement", on_delete=models.CASCADE)
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    status = models.BooleanField(null=False, default=False)

    def __str__(self):
        return "{}{}".format(self.business_unit.code, self.partner.number)


class Role(AbstractModel):
    role = models.CharField(max_length=255, unique=True, null=False, default="Reader")
    permissions = models.ManyToManyField(Permission, related_name="partner_permission")

    def __str__(self):
        return self.role


class PartnerMember(AbstractModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="partner_members",
    )
    partner = models.ForeignKey(
        Partner, on_delete=models.CASCADE, related_name="partner_firm_members"
    )
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    temporary_password = models.CharField(max_length=255, null=True, default=None)
    date_send_invitation = models.DateField(null=True, default=None)
    is_password_change = models.BooleanField(null=False, default=False)

    class Meta:
        unique_together = ("user", "partner", "role")

    def __init__(self, *args, **kwargs):
        self.user_key_mapping = {
            "name": "fullname",
            "email": "email",
        }
        self.user_type_role_model = apps.get_model("account.UserTypeRole")
        super().__init__(*args, **kwargs)

    def update_user_info(self, key, value):
        user = self.user
        setattr(user, self.user_key_mapping.get(key), value)
        user.save()

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        self.trigger_create()

    def trigger_create(self):
        utr = self.user_type_role_model(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
            user_id=self.user_id,
        )
        utr.save()

    def trigger_delete(self):
        utr_qs = self.user_type_role_model.objects.filter(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
        )
        if utr_qs.exists() is True:
            utr_qs.delete()
            self.delete()

    def __str__(self):
        return self.user.fullname


class AdverseDisclaimer(AbstractModel):
    agreement_number = models.IntegerField()
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.PROTECT)
    partner = models.ForeignKey(Partner, on_delete=models.PROTECT)
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    modification_type = models.CharField(
        max_length=30, choices=MODIFICATION_TYPE_OPTIONS, null=True, default=None
    )
    status = models.BooleanField(null=False, default=False)

    def __str__(self):
        return "{}{}".format(self.business_unit.code, self.agreement_number)
