from django.core.management.base import BaseCommand
from partner.models import Partner

# importing datetime module
import datetime
import os
import xlrd
import xlsxwriter


class Command(BaseCommand):
    help = "Store Partners Date based on Partner Id"

    def handle(self, *args, **kwargs):
        cwd = os.getcwd()
        file_path = os.path.join(
            cwd, "audit", "applications", "partner", "excel", "2020 - New Partners.xlsx"
        )
        wb = xlrd.open_workbook(file_path)
        sheet_names = wb.sheet_by_name("2020 NEW PARTNERS")

        # Create a workbook and add a worksheet.
        workbook_path = os.path.join(
            cwd,
            "audit",
            "applications",
            "partner",
            "excel",
            "partner_not_in_excel.xlsx",
        )
        workbook = xlsxwriter.Workbook(workbook_path)
        worksheet = workbook.add_worksheet("2020 NEW PARTNERS")

        # iterating rows
        for idx_i in range(1, sheet_names.nrows):
            year = sheet_names.cell(idx_i, 0).value
            partner_id = sheet_names.cell(idx_i, 5).value

            date_instance = datetime.date(int(year), 1, 1)
            partner_obj = Partner.objects.filter(number=int(partner_id)).exists()
            if partner_obj:
                Partner.objects.filter(number=int(partner_id)).update(
                    engagement_date=date_instance
                )
            else:
                # writing xlsx file to iterate columns
                for idx_j in range(sheet_names.ncols):
                    worksheet.write(0, idx_j, sheet_names.cell(idx_i, idx_j).value)

                    worksheet.write(idx_i, idx_j, sheet_names.cell(idx_i, idx_j).value)
        workbook.close()
