import os
import xlrd
from django.db.models import Q
from django.db.models.functions import Lower

from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.filters import OrderingFilter
from rest_framework import filters
from project.models import Agreement
from partner.filters import (
    PartnerAgreementFilter,
    PQPStatusFilter,
    PartnerListFilter,
    AdverseDisclaimerFilter,
)
from project.serializers import AgreementAssignmentListSerializer
from django.http.request import QueryDict, MultiValueDict
from .helpers import import_icq_report
from rest_framework import exceptions
from partner.models import (
    Partner,
    PartnerType,
    Role as PartnerRole,
    PartnerMember,
    PQPStatus,
    AdverseDisclaimer,
)

from icq.models import ICQReport
from icq.serializers import ICQReportSerializer
from partner.serializers import (
    PartnerSerializer,
    PartnerTypeSerializer,
    PartnerRoleSerializer,
    PartnerMemberDetailSerializer,
    PartnerMemberSerializer,
    PQPStatusSerializer,
    AdverseDisclaimerSerializer,
)

from utils.rest.code import code
from common.permissions import IsPARTNER, IsUNHCR, IsFO
from common.models import CommonFile
from rest_framework.request import Request
from django.http import HttpRequest
from icq.models import ICQ
from icq.filters import ICQFilter


class PartnerList(generics.ListCreateAPIView):
    serializer_class = PartnerSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Partner.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_class = PartnerListFilter

    # def get_queryset(self):
    #     legal_name = self.request.query_params.get('legal_name')
    #     number = self.request.query_params.get('number')
    #     implementer_type = self.request.query_params.get('implementer_type')

    #     try:
    #         if legal_name is not None:
    #             self.queryset = self.queryset.filter(Q(legal_name__icontains=legal_name))
    #         if number is not None:
    #             self.queryset = self.queryset.filter(number=number)
    #         if implementer_type is not None:
    #             self.queryset = self.queryset.filter(implementer_type__implementer_type = implementer_type)
    #     except Exception:
    #         self.queryset = Partner.objects.none()
    #     return  self.queryset


class PartnerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Partner.objects.all()
    serializer_class = PartnerSerializer
    permission_classes = (IsAuthenticated,)


class PartnerTypeList(generics.ListCreateAPIView):
    queryset = PartnerType.objects.all()
    serializer_class = PartnerTypeSerializer
    permission_classes = (IsAuthenticated,)


class PartnerTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PartnerType.objects.all()
    serializer_class = PartnerTypeSerializer
    permission_classes = (IsAuthenticated,)


class PartnerRoleList(generics.ListCreateAPIView):
    queryset = PartnerRole.objects.all()
    serializer_class = PartnerRoleSerializer
    permission_classes = (IsAuthenticated,)


class PartnerRoleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PartnerRole.objects.all()
    serializer_class = PartnerRoleSerializer
    permission_classes = (IsAuthenticated,)


class PartnerMemberList(generics.ListAPIView):
    queryset = PartnerMember.objects.all()
    serializer_class = PartnerMemberSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, pk):
        members = PartnerMember.objects.filter(partner__id=pk)
        serializer = PartnerMemberSerializer(
            members, many=True, context={"request": request}
        )
        return Response(serializer.data)


class PartnerMemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PartnerMember.objects.all()
    serializer_class = PartnerMemberDetailSerializer
    permission_classes = (IsAuthenticated, IsPARTNER | IsUNHCR)
    resource_model = PartnerMember

    def get(self, request, pk=None, *args, **kwargs):
        partner_member_qs = self.resource_model.objects.filter(pk=pk)
        if partner_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        partner_member = partner_member_qs.last()

        res = self.serializer_class(
            partner_member, context={"request": request}, many=False
        )
        return Response(res.data, status=status.HTTP_200_OK)

    def patch(self, request, pk=None, *args, **kwargs):
        partner_member_qs = self.resource_model.objects.filter(pk=pk)
        if partner_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        partner_member = partner_member_qs.last()
        serializer = self.serializer_class(partner_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        partner_member = serializer.update(partner_member, validated_data=request.data)
        res = self.serializer_class(
            partner_member, context={"request": request}, many=False
        )
        return Response(res.data, status=status.HTTP_200_OK)

    def put(self, request, pk=None, *args, **kwargs):
        partner_member_qs = self.resource_model.objects.filter(pk=pk)
        if partner_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        partner_member = partner_member_qs.last()
        serializer = self.serializer_class(partner_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        partner_member = serializer.update(partner_member, validated_data=request.data)
        res = self.serializer_class(
            partner_member, context={"request": request}, many=False
        )
        return Response(res.data, status=status.HTTP_200_OK)


class ICQReportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ICQReport.objects.all()
    serializer_class = ICQReportSerializer
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        instance = self.queryset.get(pk=kwargs["pk"])
        instance.report_doc = None
        instance.save()
        icq = ICQ.objects.filter(icq_report_id=kwargs["pk"])
        icq.delete()
        return Response({"message": "success"})


# class ICQReportList(generics.ListCreateAPIView):
#     queryset = ICQReport.objects.all().order_by('business_unit__code')
#     serializer_class = ICQReportSerializer
#     permission_classes = (IsAuthenticated, )

#     filter_backends = (filters.OrderingFilter, DjangoFilterBackend,)
#     filter_class = ICQFilter

#     ordering_fields = [
#         'business_unit__code', 'year', 'partner__legal_name', 'partner__number',
#         'report_date', 'user__email', 'overall_score_perc'
#     ]

#     def get_queryset(self):
#         membership = self.request.user.membership
#         year = self.request.query_params.get('year', None)
#         if year is None:
#             raise exceptions.APIException("year is a required field")

#         if membership['type'] == "AUDITOR":
#             agreements = Agreement.objects.filter(audit_firm_id=membership['audit_agency']['id'])
#             return self.queryset.filter(partner_id__in=agreements.values_list('partner_id', flat=True))
#         return self.queryset

#     def post(self, request, *args, **kwargs):
#         files_list = request.data.getlist('report_file', [])
#         year = request.data.get('year', None)

#         if year is None:
#             raise exceptions.APIException("Year is a required field")

#         if len(files_list) == 0:
#             raise exceptions.APIException("Please select a file")

#         if len(files_list) > 1:
#             raise exceptions.APIException("Only single file upload is supported")

#         if str(files_list[0]).endswith(".xlsx") == False:
#             raise exceptions.APIException("Invalid file type")

#         items = []
#         xls = files_list[0]
#         wb = xlrd.open_workbook(file_contents=xls.read())
#         try:
#             icq_sheet = "ICQ"
#             sheet = wb.sheet_by_name(icq_sheet)
#         except Exception as e:
#             raise exceptions.APIException("No sheet name ICQ in the file")

#         for i in range(2, sheet.nrows):
#             try:
#                 implementer = sheet.cell_value(i, 0)
#                 implementer = str(int(implementer)) if ".0" in str(implementer) else str(implementer)
#                 # partner_number = sheet.cell_value(i, 1)
#                 partner_name = sheet.cell_value(i, 2)
#                 bu = sheet.cell_value(i, 3)

#                 # bu = partner_number.replace(str(implementer), "")
#                 score = sheet.cell_value(i, 4)
#                 year = int(sheet.cell_value(i, 5))
#                 items.append({
#                     "implementer": implementer,
#                     "partner_name": partner_name,
#                     "bu": bu,
#                     "score": score,
#                     "user_id": request.user.id,
#                     "year": year
#                 })
#             except Exception as e:
#                 print("Error ", e)
#                 error = "Error while uploading ICQ, Please check row number ".format(i)
#                 raise exceptions.APIException(error)
#         import_icq_report(year, items)
#         return Response({"message": "done"}, status=status.HTTP_200_OK)


class PQPStatusDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PQPStatus.objects.all()
    serializer_class = PQPStatusSerializer
    permission_classes = (IsAuthenticated,)


class PQPStatusList(generics.ListCreateAPIView):
    queryset = PQPStatus.objects.all()
    serializer_class = PQPStatusSerializer
    permission_classes = (IsAuthenticated,)

    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = PQPStatusFilter

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(
                {"message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )
        pqp = serializer.save()
        res = self.serializer_class(pqp, many=False, context={"request": request})
        return Response(res.data, status=status.HTTP_201_CREATED)


class PartnerAgreeementList(generics.ListCreateAPIView):
    queryset = Agreement.objects.all()
    permission_classes = (IsAuthenticated, IsPARTNER | IsUNHCR)
    serializer_class = AgreementAssignmentListSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = PartnerAgreementFilter

    def get(self, request, *args, **kwargs):
        partner_id = kwargs["pk"]
        self.queryset = Agreement.objects.filter(partner_id=partner_id)
        return self.list(request, *args, **kwargs)


class AdverseDisclaimerList(generics.ListCreateAPIView):
    queryset = AdverseDisclaimer.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    serializer_class = AdverseDisclaimerSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_class = AdverseDisclaimerFilter
    ordering_fields = [
        "status",
        "year",
    ]


class AdverseDisclaimerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AdverseDisclaimer.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    serializer_class = AdverseDisclaimerSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_class = AdverseDisclaimerFilter
    ordering_fields = [
        "status",
        "year",
    ]
