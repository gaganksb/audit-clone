from django.urls import path
from partner import views

urlpatterns = [
    path("", views.PartnerList.as_view(), name="partner-list"),
    path("<int:pk>/", views.PartnerDetail.as_view(), name="partner-detail"),
    path(
        "<int:pk>/members/",
        views.PartnerMemberList.as_view(),
        name="partnermember-list",
    ),
    path(
        "member/<int:pk>/",
        views.PartnerMemberDetail.as_view(),
        name="partner-member-detail",
    ),
    path("types/", views.PartnerTypeList.as_view(), name="partnertype-list"),
    path(
        "types/<int:pk>/", views.PartnerTypeDetail.as_view(), name="partnertype-detail"
    ),
    path("roles/", views.PartnerRoleList.as_view(), name="role-list"),
    path("roles/<int:pk>/", views.PartnerRoleDetail.as_view(), name="role-detail"),
    # path('icq/', views.ICQReportList.as_view(), name='icqreport-list'),
    path("icq/<int:pk>/", views.ICQReportDetail.as_view(), name="icqreport-detail"),
    path("pqp/", views.PQPStatusList.as_view(), name="pqpstatus-list"),
    path("pqp/<int:pk>/", views.PQPStatusDetail.as_view(), name="pqpstatus-detail"),
    path(
        "agreements/<int:pk>/",
        views.PartnerAgreeementList.as_view(),
        name="partner-agreeement-list",
    ),
    path(
        "adverse-disclaimer/",
        views.AdverseDisclaimerList.as_view(),
        name="adverse-disclaimer-list",
    ),
    path(
        "adverse-disclaimer/<int:pk>/",
        views.AdverseDisclaimerDetail.as_view(),
        name="adverse-disclaimer-list",
    ),
]
