from django.contrib import admin
from .models import (
    Partner,
    PartnerMember,
    Role,
    PartnerType,
    AdverseDisclaimer,
    ImplementerType,
    Modified,
    PQPStatus,
    PQPStatusNew,
)


class PartnerTypeAdmin(admin.ModelAdmin):
    search_fields = ("partner_type",)
    list_display = ("partner_type",)
    ordering = ("partner_type",)


class PartnerAdmin(admin.ModelAdmin):
    search_fields = ("legal_name",)
    list_display = ("legal_name",)
    ordering = ("legal_name",)


class PartnerMemberAdmin(admin.ModelAdmin):
    search_fields = ("user__email", "partner__legal_name", "role__name")
    list_display = (
        "user",
        "partner",
        "role",
    )
    ordering = ("user__email",)
    raw_id_fields = (
        "user",
        "partner",
    )


class RoleAdmin(admin.ModelAdmin):
    search_fields = ("role__role",)
    filter_horizontal = ("permissions",)

    readonly_fields = ("role",)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


# class AdverseDisclaimerAdmin(admin.ModelAdmin):
#     list_display=('agreement', 'year', 'modification_type', 'status')

admin.site.register(Partner, PartnerAdmin)
admin.site.register(PartnerMember, PartnerMemberAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(PartnerType, PartnerTypeAdmin)
admin.site.register(AdverseDisclaimer)
admin.site.register(ImplementerType)
admin.site.register(Modified)
admin.site.register(PQPStatus)
admin.site.register(PQPStatusNew)
