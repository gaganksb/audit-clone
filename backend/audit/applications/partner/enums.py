from enum import unique, auto

from common.enums import AutoNameEnum, PermissionListEnum


@unique
class PartnerRoleEnum(AutoNameEnum):
    REVIEWER = auto()
    ADMIN = auto()


PARTNER_ROLE_PERMISSIONS = {
    PartnerRoleEnum.REVIEWER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
    PartnerRoleEnum.ADMIN: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
}
