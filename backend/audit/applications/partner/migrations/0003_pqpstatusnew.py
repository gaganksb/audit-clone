# Generated by Django 2.2.1 on 2020-10-22 03:47

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0002_auto_20201014_0756"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("common", "0002_auto_20200930_0923"),
        ("partner", "0002_auto_20201019_1204"),
    ]

    operations = [
        migrations.CreateModel(
            name="PQPStatusNew",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_date",
                    models.DateTimeField(default=django.utils.timezone.now, null=True),
                ),
                (
                    "last_modified_date",
                    models.DateTimeField(default=django.utils.timezone.now, null=True),
                ),
                (
                    "year",
                    models.IntegerField(
                        validators=[
                            django.core.validators.MinValueValidator(1980),
                            django.core.validators.MaxValueValidator(2050),
                        ]
                    ),
                ),
                ("status", models.BooleanField(default=False)),
                (
                    "agreement",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="project.Agreement",
                    ),
                ),
                (
                    "business_unit",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="common.BusinessUnit",
                    ),
                ),
                (
                    "partner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="partner.Partner",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="partner_pqpstatusnew_related",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
