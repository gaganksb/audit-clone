import csv
import datetime
from django.conf import settings

# from partner.models import (Partner, ICQReport, PQPStatus, Modified)
from partner.models import Partner, PQPStatus, Modified
from partner.enums import PartnerRoleEnum, PARTNER_ROLE_PERMISSIONS
from partner.models import Role
from common.models import Permission


def create_partner_roles():
    for role in PartnerRoleEnum:
        _ = Role.objects.get_or_create(role=role.name)
        permissions = list(
            Permission.objects.filter(
                permission__in=PARTNER_ROLE_PERMISSIONS[role]
            ).values_list("id", flat=True)
        )
        _[0].permissions.remove(*_[0].permissions.all())
        for permission in permissions:
            _[0].permissions.add(permission)


def import_icq(year):
    pass
    # file_path = settings.PROJECT_ROOT + '/../' + 'applications/partner/excel/icq-' + str(year) + '.csv'
    # with open(file_path,'r') as f:
    #     next(f) # skip headings
    #     reader=csv.reader(f,delimiter='\t')
    #     for partner_number, overall_score_perc in reader:
    #         partner = Partner.objects.filter(number=partner_number[5:])
    #         overall_score_perc = overall_score_perc.replace(',', '.').strip()
    #         report_date = str(year) + '-12-31'
    #         if len(partner) > 0 and overall_score_perc != '-':
    #             partner = partner[0]
    #             # ICQReport(report_date=report_date, partner=partner, overall_score_perc=float(overall_score_perc)).save()


def import_pqp():
    file_path = settings.PROJECT_ROOT + "/../" + "applications/partner/excel/pqp.csv"
    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f, delimiter="\t")
        for partner_number, national_worldwide, granted_date, expiry_date in reader:
            granted_date = datetime.datetime.strptime(
                granted_date.strip(), "%d/%m/%Y"
            ).strftime("%Y-%m-%d")
            expiry_date = datetime.datetime.strptime(
                expiry_date.strip(), "%d/%m/%Y"
            ).strftime("%Y-%m-%d")
            national_worldwide = national_worldwide[0].upper()
            if len(partner_number) > 0:
                partner = Partner.objects.filter(number=partner_number)
                if len(partner) > 0:
                    partner = partner[0]
                    PQPStatus(
                        partner=partner,
                        national_worldwide=national_worldwide,
                        granted_date=granted_date,
                        expiry_date=expiry_date,
                    ).save()


def import_modified():
    file_path = (
        settings.PROJECT_ROOT + "/../" + "applications/partner/excel/modified.csv"
    )
    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f, delimiter="\t")
        for partner_number, year, commissioned, modified in reader:
            partner = Partner.objects.filter(number=partner_number[5:])
            year = int(year)
            if int(commissioned) == 1:
                commissioned = True
            else:
                commissioned = False
            if int(modified) == 1:
                modified = True
            else:
                modified = False
            if len(partner) > 0:
                partner = partner[0]
                Modified(
                    year=year,
                    partner=partner,
                    commissioned=commissioned,
                    modified=modified,
                ).save()
