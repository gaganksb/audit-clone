from django_filters import FilterSet
from django_filters.filters import CharFilter, NumberFilter
from project.models import Agreement, Cycle, SelectionSettings, SenseCheck
from partner.models import Partner, AdverseDisclaimer, PQPStatus
from django.db.models import Q

PA_MANUALLY_EXCLUDED = "SC12"
from common.helpers import filter_project_selection


class PartnerListFilter(FilterSet):

    legal_name = CharFilter(field_name="legal_name", lookup_expr="icontains")
    number = NumberFilter(field_name="number", lookup_expr="exact")
    implementer_type = CharFilter(
        field_name="implementer_type__implementer_type", lookup_expr="icontains"
    )
    finalized_in_year = NumberFilter(method="filter_finalized")

    def filter_finalized(self, queryset, name, value):

        cycle = Cycle.objects.filter(year=value).first()
        if cycle is None:
            return queryset.none()
        else:
            agreements = Agreement.objects.all()
            fin_agreements = filter_project_selection(
                agreements, cycle.year, "fin", "included"
            )
            partner_ids = fin_agreements.distinct("partner").values_list("partner_id", flat=True)
            return queryset.filter(id__in=partner_ids)

    class Meta:
        model = Partner
        fields = [
            "legal_name",
            "number",
            "implementer_type",
            "finalized_in_year",
        ]


class PartnerAgreementFilter(FilterSet):
    number = NumberFilter(field_name="number", lookup_expr="icontains")
    budget_ref = NumberFilter(method="filter_has_budget_ref")
    business_unit = CharFilter(method="filter_has_business_unit")
    agreement_type = CharFilter(method="filter_has_agreement_type")

    def filter_has_budget_ref(self, queryset, name, value):
        return queryset.filter(cycle__year=value)

    def filter_has_business_unit(self, queryset, name, value):
        return queryset.filter(business_unit__code=value)

    def filter_has_agreement_type(self, queryset, name, value):
        return queryset.filter(agreement_type__description__icontains=value)

    class Meta:
        model = Agreement
        fields = ["number", "budget_ref", "agreement_type", "business_unit"]


class PQPStatusFilter(FilterSet):

    partner = CharFilter(field_name="partner__legal_name", lookup_expr="icontains")

    class Meta:
        model = PQPStatus
        fields = [
            "partner",
        ]


class AdverseDisclaimerFilter(FilterSet):
    status = CharFilter(field_name="status", lookup_expr="icontains")
    year = NumberFilter(field_name="year", lookup_expr="icontains")

    class Meta:
        model = AdverseDisclaimer
        fields = [
            "year",
            "status",
        ]
