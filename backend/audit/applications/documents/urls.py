from django.urls import path
from django.http import JsonResponse
from . import views


def check(request):
    return JsonResponse({"res": "success"})


urlpatterns = [
    path("", views.RepositoryList.as_view(), name="documents-create"),
    path("<int:pk>/", views.RepositoryDetail.as_view(), name="documents-detail"),
    path(
        "project_id/<int:pk>/file/<int:file_id>/",
        views.RepositoryDetail.as_view(),
        name="documents-detail",
    ),
]
