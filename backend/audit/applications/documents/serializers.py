from rest_framework import serializers
from .models import Repository
from project.models import Agreement
from common.models import CommonFile
from common.serializers import CommonFileSerializer
from rest_framework import exceptions


class RepositorySerializer(serializers.ModelSerializer):
    # common_file = serializers.FileField(write_only=True)
    common_file = CommonFileSerializer(many=True, required=False)

    class Meta:
        model = Repository
        fields = ["id", "agreement", "common_file"]

    def create(self, validated_data):
        request = self.context["request"]
        agreement_id = request.data.get("agreement")
        files_list = request.data.getlist("common_file")
        list_repo_id = []
        agreement = Agreement.objects.filter(id=agreement_id)
        if agreement.exists() == False:
            error = "The agreement doesn't exists"
            raise exceptions.APIException(error)

        repo = Repository.objects.create(agreement=agreement.first())
        for file_obj in files_list:
            common_file = CommonFile.objects.create(
                file_field=file_obj, user=request.user
            )
            repo.common_file.add(common_file)
        repo.save()
        return validated_data
