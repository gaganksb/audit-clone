from .models import Repository
from django_filters.filters import CharFilter, NumberFilter
from django_filters import FilterSet


class RepositoryFilter(FilterSet):
    repository = NumberFilter(field_name="id", lookup_expr="exact")
    agreement = NumberFilter(field_name="agreement_id", lookup_expr="exact")

    class Meta:
        model = Repository
        fields = ["id", "agreement"]
