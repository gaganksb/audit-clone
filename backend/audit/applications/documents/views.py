from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .models import Repository
from common.models import CommonFile
from .serializers import RepositorySerializer
from django_filters.rest_framework import DjangoFilterBackend
from .filters import RepositoryFilter
from rest_framework import exceptions
from common.permissions import IsUNHCR
from project.permissions import IsAgreementMember


class RepositoryList(generics.CreateAPIView):
    permission_classes = (
        IsAuthenticated,
        IsUNHCR | IsAgreementMember,
    )
    serializer_class = RepositorySerializer
    queryset = Repository.objects.filter()
    name = "create_repo_file"


class RepositoryDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (
        IsAuthenticated,
        IsUNHCR | IsAgreementMember,
    )
    serializer_class = RepositorySerializer
    queryset = Repository.objects.filter()

    def get(self, request, *args, **kwargs):
        agreement_id = kwargs["pk"]
        self.queryset = Repository.objects.filter(agreement_id=agreement_id)
        serializer = RepositorySerializer(self.queryset, many=True)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        files_list = request.data.getlist("common_file")
        repo = Repository.objects.get(id=kwargs["file_id"])
        for file_obj in files_list:
            common_file = CommonFile.objects.create(
                file_field=file_obj, user=request.user
            )
            repo.common_file.add(common_file)
        repo.save()
        return Response({"result": "success"})

    def delete(self, request, *args, **kwargs):
        common_file_id = request.query_params.get("common_file_id", None)
        if common_file_id is None:
            error = "Please provide the common_file_id"
            raise exceptions.APIException(error)

        repo = Repository.objects.get(id=kwargs["file_id"])
        repo.common_file.remove(common_file_id)
        repo.save()
        return Response({"result": "success"})
