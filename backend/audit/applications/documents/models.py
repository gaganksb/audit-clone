from django.db import models
from audit.core.abstract_model import AbstractModel
from common.models import CommonFile
from project.models import Agreement
from django.db.models.signals import post_save


class Repository(AbstractModel):
    common_file = models.ManyToManyField(CommonFile)
    agreement = models.ForeignKey(
        Agreement,
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name="agreement_repo",
    )
