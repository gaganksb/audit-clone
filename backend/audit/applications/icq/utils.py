import xlrd
import os
from common.models import CommonFile, BusinessUnit
from project.models import Agreement
from partner.models import Partner
from icq.models import ICQReport
from icq.calc import CalcICQScore
from rest_framework import serializers
from django.core.files import File
from common.consts import ICQ_COLUMNS_EN, ICQ_COLUMNS_ES, ICQ_COLUMNS_FR


class ImportICQ:

    file_path = None

    def __init__(self, file_path, file_name, year, user_id):
        self.file_path = file_path
        self.file_name = file_name
        self.year = year
        self.user_id = user_id

    def get_cell_range(self, xlr, sheet, start_col, start_row, end_col, end_row):
        try:
            workbook = xlrd.open_workbook(xlr)
            sheet = workbook.sheet_by_name(sheet)
            return [
                sheet.row_slice(row, start_colx=start_col, end_colx=end_col + 1)
                for row in range(start_row, end_row + 1)
            ]
        except:
            return []

    def is_float(self, data):
        return str(data).split(".")[0].isdigit()

    def _vlidate_and_get_bu_partner(self):
        business_unit = self.get_cell_range(self.file_path, "ICQ questions", 3, 3, 3, 3)
        partner = self.get_cell_range(self.file_path, "ICQ questions", 3, 2, 3, 2)
        if len(business_unit) == 0 or len(business_unit[0]) == 0:
            error = {"message": "Invalid Business Unit"}
            return None, False, error
        if len(partner) == 0 or len(partner[0]) == 0:
            error = {"message": "Invalid Partner"}
            return None, False, error
        if " " in str(partner[0][0].value):
            error = {"message": "Invalid Partner"}
            return None, False, error
        partner = (
            int(partner[0][0].value)
            if self.is_float(partner[0][0].value)
            else partner[0][0].value
        )
        partner = "{}-{}".format(business_unit[0][0].value, partner)
        return partner, True, None

    def validate_icq_file(self):
        try:
            columns = self.get_cell_range(self.file_path, "ICQ questions", 1, 6, 19, 6)
            data = self.get_cell_range(self.file_path, "ICQ questions", 0, 0, 10, 117)

            lang = self.get_cell_range(self.file_path, "ICQ questions", 0, 0, 0, 0)

            try:
                lang = lang[0][0].value
                if lang and lang == "FR":
                    ICQ_COLUMNS = ICQ_COLUMNS_FR
                elif lang and lang == "ES":
                    ICQ_COLUMNS = ICQ_COLUMNS_ES
                else:
                    ICQ_COLUMNS = ICQ_COLUMNS_EN
            except:
                ICQ_COLUMNS = ICQ_COLUMNS_EN

            partner, valid, error = self._vlidate_and_get_bu_partner()
            if not valid:
                return None, False, error
            if len(columns) == 0 or len(data) == 0:
                error = {"message": "Unable to Read Data from ICQ File"}
                return None, False, error
            if len(lang) == 0:
                error = {"message": "Unable to Read Language from ICQ File"}
                return None, False, error

            if str(columns[0]) != str(ICQ_COLUMNS):
                for index, i in enumerate(columns[0]):
                    if i.value == ICQ_COLUMNS[index]:
                        continue
                    else:
                        error = {"message": "Unable to Read All Columns from ICQ File"}
                        return None, False, error

            for index, i in enumerate(data):
                if (str(i) == str(data[index])) == True:
                    continue
                else:
                    error = {"message": "Unable to Read All Rows from ICQ File"}
                    return None, False, error
            return partner, True, None
        except Exception as e:
            error = {"message": str(e)}
            return None, False, error

    error_summary = """
        <h2 id="icq-upload-error-summary">ICQ Upload Error Summary</h2>
            <p>The Below Error Occured While Trying To Upload ICQ</p>
            <ul>
                <li><strong>File</strong>: {0}</li>
                <li><strong>Partner</strong>: {1}</li>
                <li><strong>Business Unit</strong>: {2}</li>
                <li><strong>Year</strong>: {3}</li>
            </ul>
            <br/>
        <h3>{4}</h3>
    """

    def import_icq_data(self):
        try:
            data, valid, error = self.validate_icq_file()
            if valid:
                bu, partner = data.split("-")
                business_unit = BusinessUnit.objects.filter(code=bu).first()
                partner = Partner.objects.filter(number=partner).first()
                if partner is None:
                    error = "Invalid Partner Name"
                    upload_error = self.error_summary.format(
                        str(self.file_name), partner, bu, self.year, error
                    )
                    return upload_error, False
                if business_unit is None:
                    error = "Invalid Business Unit Name"
                    upload_error = self.error_summary.format(
                        str(self.file_name), partner.id, bu, self.year, error
                    )
                    return upload_error, False
                try:
                    file_content = open(self.file_path, "rb")
                    django_file = File(file_content)
                    common_file = CommonFile()
                    common_file.file_field.save(self.file_name, django_file, save=True)
                except Exception as e:
                    error = "Error while storing the data on disk"
                    upload_error = self.error_summary.format(
                        str(self.file_name), partner.id, bu, self.year, error
                    )
                    return upload_error, False

                try:
                    icq_report, created = ICQReport.objects.get_or_create(
                        business_unit=business_unit,
                        partner=partner,
                        year=self.year,
                    )
                    if not created:
                        error = "Duplicate file {}, Data for BU: {} and Partner: {} already exists".format(
                            str(self.file_name), business_unit.code, partner.number
                        )
                        upload_error = self.error_summary.format(
                            str(self.file_name), partner.id, bu, self.year, error
                        )
                        return upload_error, False
                    icq_calc_instance = CalcICQScore(common_file.id)
                    icq_calc_instance = icq_calc_instance.generate_score()
                    if icq_calc_instance is not None:
                        overall_score_perc = icq_calc_instance["overall_score_perc"]
                        process_wise_score = icq_calc_instance["process_wise_score"]
                        key_control_attrs = icq_calc_instance["key_control_attrs"]
                        icq_report.overall_score_perc = overall_score_perc
                        icq_report.process_wise_score = process_wise_score
                        icq_report.key_control_attrs = key_control_attrs
                        icq_report.report_doc = common_file
                        icq_report.user_id = self.user_id
                        icq_report.save()
                        agreements = Agreement.objects.filter(
                            business_unit=business_unit,
                            partner=partner,
                        )
                        agreements.update(icq_report=icq_report)
                        return True, True
                    else:
                        link = """<p>Click <a href="/api/icq/download-icq-template/">here</a> to download the template</p>"""
                        error = "Something wrong with the ICQ file, Please compare your file with the ICQ template. {}".format(
                            link
                        )
                        upload_error = self.error_summary.format(
                            str(self.file_name), partner.id, bu, self.year, error
                        )
                        return upload_error, False
                except Exception as e:
                    link = """<p>Click <a href="/api/icq/download-icq-template/">here</a> to download the template</p>"""
                    error = "Something wrong with the ICQ file, Please compare your file with the ICQ template. {}".format(
                        link
                    )
                    upload_error = self.error_summary.format(
                        str(self.file_name), partner.id, bu, self.year, error
                    )
                    return upload_error, False
            else:
                upload_error = self.error_summary.format(
                    str(self.file_name), "", "", self.year, str(error)
                )
                return upload_error, False
        except Exception as e:
            print(e, "Error")
            upload_error = self.error_summary.format(
                str(self.file_name), "", "", self.year, str(e)
            )
            return upload_error, False
