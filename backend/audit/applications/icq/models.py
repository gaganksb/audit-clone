# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from collections import defaultdict
from typing import Text

from django.conf import settings
from django.db import models
from django.db.models.fields import TextField
from audit.core.abstract_model import AbstractModel
from project.models import Cycle, Agreement
from django.contrib.postgres.fields import ArrayField, JSONField
from common.models import Comment, BusinessUnit, CommonFile
from django.core.validators import MinValueValidator, MaxValueValidator

MIN_YEAR = 2014
MAX_YEAR = 5050
CONTROL_CHOICES = (("adequate", "Adequate"), ("inadequate", "Inadequate"), ("na", "NA"))

PRIOR_CHOICES = (
    ("na", "No prior year finding"),
    ("fully_remediated", "Fully remediated"),
    ("partially_remediated", "Partially remediated"),
    ("not_remediated", "Not remediated"),
)

FINANCIAL_FINDINGS_TYPES = [
    ("EXP_NOT_USED", "Expenditure not used for the purpose/objectives of PA"),
    ("NO_SUPP_DOC", "No supporting documentation"),
    (
        "INSUFF_DOC",
        "Insufficient supporting documentation (may include insufficient access to information, people or sites)",
    ),
    (
        "COST_EXC",
        "Personnel costs exceed the agreed budget and/or amounts paid to ineligible persons",
    ),
    ("MISS_CALC", "Partner Integrity Capacity and Support Costs (PICSC) Miscalculated"),
    (
        "CUTT_OFF_ERROR",
        "Cut-off errors: commitments recorded as expenditures and/or expenditure committed after the end of the project implementation period and/or when payments were issued after the liquidation period but the deliverables were not received by the end of the implementation period.",
    ),
    ("INAPPROP_EXC_RATE", "Inappropriate exchange rate used"),
    (
        "EXCESS_TRANSFER",
        "Budgetary transfers made in excess of 30% limit at the output level without UNHCR approval",
    ),
    (
        "FIN_INFO_NON_RECON",
        "Financial information not reconciled to GL (when PFR items are not traceable into the GL)",
    ),
    ("NO_PROOF", "No proof of goods/services  received or activities undertaken"),
    ("INCORRECT_VAT", "VAT incorrectly claimed or a VAT valid exemption not utilized"),
    ("OTH_INCOME", "Other Income (e.g. bank interest) not reported or miscalculated"),
    (
        "DOUBLE_CHARGE",
        "Expenditure double-funded or double charged or double recorded /reported",
    ),
    (
        "OTHER_FIN_FINDING",
        "Other financial findings that did not fall within any of the other headings (please clarify).",
    ),
]

CRITICALITY = (
    ("high", "High"),
    ("low", "Low"),
    ("medium", "Medium"),
)


class Process(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        return self.title


class ProcessTitle(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        return self.title


class GeneralRisk(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        return self.title


class KeyControl(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        return self.title


class KeyControlTitle(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)

    def __str__(self):
        return self.title


class KeyControlAttr(models.Model):
    title = models.CharField(max_length=200, null=True, default=None)
    detail = models.TextField()

    def __str__(self):
        return self.title


class ICQReport(models.Model):
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.PROTECT)
    partner = models.ForeignKey("partner.Partner", on_delete=models.PROTECT)
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    report_date = models.DateField(
        verbose_name="Report Date", null=False, blank=True, auto_now=True
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, default=None
    )
    report_doc = models.OneToOneField(
        CommonFile, on_delete=models.CASCADE, null=True, default=None
    )
    overall_score_perc = models.DecimalField(
        max_digits=11, decimal_places=8, null=True, default=None
    )
    process_wise_score = JSONField(default=dict, blank=True)
    key_control_attrs = JSONField(default=dict, blank=True)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return "{}-{}-{}".format(
            self.business_unit.code, self.partner.legal_name, self.year
        )


class ICQ(AbstractModel):

    icq_report = models.ForeignKey(ICQReport, on_delete=models.CASCADE)
    process = models.ForeignKey(Process, on_delete=models.CASCADE)
    process_title = models.ForeignKey(
        ProcessTitle, on_delete=models.CASCADE, null=True, default=None
    )
    general_risk = models.ForeignKey(
        GeneralRisk, on_delete=models.CASCADE, null=True, default=None
    )
    key_control = models.ForeignKey(
        KeyControl, on_delete=models.CASCADE, null=True, default=None
    )
    key_control_title = models.ForeignKey(
        KeyControlTitle, on_delete=models.CASCADE, null=True, default=None
    )
    key_control_attr = models.ForeignKey(
        KeyControlAttr, on_delete=models.CASCADE, null=True, default=None
    )

    control_at_partner = models.CharField(max_length=50, null=True, default=None)
    existance_at_control = models.CharField(
        max_length=200, choices=CONTROL_CHOICES, null=True, default=None
    )
    prior_year_recommendation = models.CharField(
        max_length=200, choices=PRIOR_CHOICES, null=True, default=None
    )
    comment = models.TextField(null=True, default=None)
    mgmt_letter_finding = models.TextField(null=True, default=None)
    risk = models.TextField(null=True, default=None)
    risk_criticality = models.TextField(null=True, default=None)
    auditor_recommendation = models.TextField(null=True, default=None)
    management_proposed_actions = models.TextField(null=True, default=None)
    actions_deadlines = models.TextField(null=True, default=None)
    responsible_person = models.TextField(null=True, default=None)

    additional_info = models.CharField(max_length=200, null=True, default=None)
    additional_info_result = models.CharField(max_length=200, null=True, default=None)
    additional_info_comment = models.CharField(max_length=200, null=True, default=None)
    partner_response = models.TextField(null=True, default=None)
    unhcr_comments = models.TextField(null=True, default=None)
    auditors_disagreement_comment = models.TextField(null=True, default=None)

    def __str__(self):
        return "{}-{}".format(self.icq_report.business_unit.code, self.process.title)


class MgmtLetter(AbstractModel):
    agreement = models.OneToOneField(
        "project.Agreement", related_name="mgmt_letter", on_delete=models.CASCADE
    )
    published = models.BooleanField(default=False)
    publish_date = models.DateTimeField(null=True, default=None)

    def can_be_published(self):
        icq = self.agreement.icq_report
        general = General.objects.filter(mgmt_letter_id=self.id).first()
        fin_findings = FinancialFindings.objects.filter(mgmt_letter_id=self.id).first()

        return all([icq, general, fin_findings])

    def __str__(self):
        return (
            "{}{}".format(self.agreement.business_unit.code, self.agreement.number)
            if self.agreement.business_unit is not None
            else self.agreement.number
        )


class General(models.Model):
    mgmt_letter = models.OneToOneField(
        MgmtLetter, related_name="mgmt_letter_general", on_delete=models.CASCADE
    )
    introduction = models.TextField(null=True, default=None)
    scope_of_work = models.TextField(null=True, default=None)
    executive_summary = models.TextField(null=True, default=None)
    audit_objectives = models.TextField(null=True, default=None)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class FinancialFindings(models.Model):
    mgmt_letter = models.ForeignKey(
        MgmtLetter, related_name="mgmt_letter_fin_findings", on_delete=models.CASCADE
    )
    type = models.CharField(choices=FINANCIAL_FINDINGS_TYPES, max_length=255)
    required_recovery = JSONField(default=list, blank=True)
    further_review = JSONField(default=list, blank=True)
    no_recovery = JSONField(default=list, blank=True)
    description = models.TextField(null=True, default=None)
    partner_response = models.TextField(null=True, default=None)
    unhcr_comments = models.TextField(null=True, default=None)
    auditors_disagreement_comment = models.TextField(null=True, default=None)
    completed = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            "mgmt_letter",
            "type",
        )

    def __str__(self):
        return (
            "{}{}".format(
                self.mgmt_letter.agreement.business_unit.code,
                self.mgmt_letter.agreement.number,
            )
            if self.mgmt_letter.agreement.business_unit is not None
            else self.mgmt_letter.agreement.number
        )


class OverallAssessment(AbstractModel):
    icq_report = models.OneToOneField(ICQReport, on_delete=models.CASCADE)
    overall_rating = JSONField(default=list, blank=True)

    @property
    def overall_assessment(self):

        if self.overall_rating:
            for key in self.overall_rating:
                if self.overall_rating[key] == "Requires improvements":
                    return "Requires improvements"
            return "Adequate"
        else:
            return "Requires improvements"

    def __str__(self):
        return "{}-{}-{}".format(
            self.icq_report.business_unit.code,
            self.icq_report.partnerlegal_name,
            self.icq_report.year,
        )


class FollowUp(AbstractModel):
    mgmt_letter = models.ForeignKey(MgmtLetter, on_delete=models.CASCADE)
    control_attribute = models.ForeignKey(KeyControlAttr, on_delete=models.CASCADE)
    prev_recommendation = models.TextField(null=True, default=None)
    criticality = models.CharField(choices=CRITICALITY, max_length=255)
    summary = models.TextField(null=True, default=None)
    recommendation = models.TextField(null=True, default=None)

    def __str__(self):
        return (
            "{}{}".format(
                self.mgmt_letter.agreement.business_unit.code,
                self.mgmt_letter.agreement.number,
            )
            if self.mgmt_letter.agreement.business_unit is not None
            else self.mgmt_letter.agreement.number
        )


class TrackICQUpload(AbstractModel):
    errors = TextField(null=True, default=None)
