import django_filters
from django_filters.filters import CharFilter, NumberFilter, DateFilter, DateRangeFilter
from django_filters import FilterSet
from project.models import Agreement
from icq.models import ICQReport
from django.db.models import Q


class ICQFilter(FilterSet):

    business_unit_code = CharFilter(
        field_name="business_unit__code", lookup_expr="iexact"
    )
    partner_legal_name = CharFilter(
        field_name="partner__legal_name", lookup_expr="icontains"
    )
    business_unit = CharFilter(field_name="business_unit_id", lookup_expr="exact")
    partner = CharFilter(field_name="partner_id", lookup_expr="exact")
    number = CharFilter(field_name="partner__number", lookup_expr="exact")
    year = NumberFilter(field_name="year", lookup_expr="exact")
    view_items = CharFilter(method="filter_view_type")

    def filter_view_type(self, queryset, name, value):
        if value == "all":
            return queryset
        elif value == "uploaded":
            return queryset.filter(~Q(report_doc=None))
        elif value == "not_uploaded":
            return queryset.filter(report_doc=None)
        else:
            return queryset.none()

    class Meta:
        model = ICQReport
        fields = ["partner", "business_unit", "partner", "number", "year", "view_items"]
