QUESTIONS = {
    "A": {
        "weight": 0.15,
        "assessment": "Requires improvements",
        "A1": {
            "weight": 0.10,
            "A1.1": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A1.2": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A1.3": {
                "weight": 0.10,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "A1.4": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A2": {
            "weight": 0.12,
            "A2.1": {
                "weight": 0.25,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "A2.2": {
                "weight": 0.25,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A2.3": {
                "weight": 0.15,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A2.4": {
                "weight": 0.10,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A2.5": {
                "weight": 0.10,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A2.6": {
                "weight": 0.15,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A3": {
            "weight": 0.20,
            "A3.1": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Inadequate",
            },
            "A3.2": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A4": {
            "weight": 0.02,
            "A4.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A5": {
            "weight": 0.03,
            "A5.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "A6": {
            "weight": 0.10,
            "A6.1": {
                "weight": 0.40,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "A6.2": {
                "weight": 0.40,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "A6.3": {
                "weight": 0.20,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "A7": {
            "weight": 0.05,
            "A7.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A8": {
            "weight": 0.15,
            "A8.1": {
                "weight": 0.2,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A8.2": {
                "weight": 0.5,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A8.3": {
                "weight": 0.3,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A9": {
            "weight": 0.05,
            "A9.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A10": {
            "weight": 0.10,
            "A10.1": {
                "weight": 0.5,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "A10.2": {
                "weight": 0.5,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "A11": {
            "weight": 0.08,
            "A11.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
    },
    "B": {
        "weight": 0.20,
        "assessment": "Requires improvements",
        "B1": {
            "weight": 0.15,
            "B1.1": {
                "weight": 0.20,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "B1.2": {
                "weight": 0.20,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "B1.3": {
                "weight": 0.20,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "B1.4": {
                "weight": 0.10,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "B1.5": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "B2": {
            "weight": 0.05,
            "B2.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "B3": {
            "weight": 0.15,
            "B3.1": {
                "weight": 0.40,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "B3.2": {
                "weight": 0.60,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "B4": {
            "weight": 0.20,
            "B4.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "B5": {
            "weight": 0.30,
            "B5.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "B6": {
            "weight": 0.15,
            "B6.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
    },
    "C": {
        "weight": 0.05,
        "assessment": "Requires improvements",
        "C1": {
            "weight": 0.20,
            "C1.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "C2": {
            "weight": 0.20,
            "C2.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "C3": {
            "weight": 0.30,
            "C3.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "C4": {
            "weight": 0.20,
            "C4.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
    },
    "D": {
        "weight": 0.05,
        "assessment": "Requires improvements",
        "D1": {
            "weight": 0.2,
            "D1.1": {
                "weight": 0.4,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "D1.2": {
                "weight": 0.6,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D2": {
            "weight": 0.2,
            "D2.1": {
                "weight": 0.5,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "D2.2": {
                "weight": 0.2,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "D2.3": {
                "weight": 0.3,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "D3": {
            "weight": 0.15,
            "D3.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D4": {
            "weight": 0.15,
            "D4.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D5": {
            "weight": 0.10,
            "D5.1": {
                "weight": 0.6,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "D5.2": {
                "weight": 0.4,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D6": {
            "weight": 0.10,
            "D6.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D7": {
            "weight": 0.05,
            "D7.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "D8": {
            "weight": 0.05,
            "D8.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
    },
    "E": {
        "weight": 0.10,
        "assessment": "Requires improvements",
        "E1": {
            "weight": 0.60,
            "E1.1": {
                "weight": 0.40,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "E1.2": {
                "weight": 0.60,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "E2": {
            "weight": 0.40,
            "E2.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
    },
    "F": {
        "weight": 0.10,
        "assessment": "Requires improvements",
        "F1": {
            "weight": 0.40,
            "F1.1": {
                "weight": 1.0,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
        "F2": {
            "weight": 0.60,
            "F2.1": {
                "weight": 0.25,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "F2.2": {
                "weight": 0.25,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
            "F2.3": {
                "weight": 0.50,
                "applicability": "No",
                "existence_of_control": "N/A",
            },
        },
    },
    "G": {
        "weight": 0.15,
        "assessment": "Requires improvements",
        "G1": {
            "weight": 0.45,
            "G1.1": {
                "weight": 0.60,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "G1.2": {
                "weight": 0.40,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "G2": {
            "weight": 0.45,
            "G2.1": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "G2.2": {
                "weight": 0.30,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "G2.3": {
                "weight": 0.40,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "G3": {
            "weight": 0.10,
            "G3.1": {
                "weight": 0.60,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "G3.2": {
                "weight": 0.40,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
    },
    "H": {
        "weight": 0.20,
        "assessment": "Requires improvements",
        "H1": {
            "weight": 0.25,
            "H1.1": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "H1.2": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "H2": {
            "weight": 0.25,
            "H2.1": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
            "H2.2": {
                "weight": 0.50,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
        "H3": {
            "weight": 0.5,
            "H3.1": {
                "weight": 1.0,
                "applicability": "Yes",
                "existence_of_control": "Adequate",
            },
        },
    },
}
