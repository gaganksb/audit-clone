from django.core.management.base import BaseCommand
from partner.models import Partner

# importing datetime module
import datetime
import os
import xlrd
import xlsxwriter
import zipfile
import tempfile
from icq.models import TrackICQUpload
from icq.tasks import import_bulk_icq_report


class Command(BaseCommand):
    help = "Store Partners Date based on Partner Id"

    def add_arguments(self, parser):
        parser.add_argument(
            "-y",
            "--year",
            type=int,
            help="Year for which you want to import ICQ",
        )
        parser.add_argument(
            "-f",
            "--file_path",
            type=str,
            help="file path of the zip file",
        )

    def handle(self, *args, **kwargs):
        icq_zip_file = kwargs.get("file_path", None)
        year = kwargs.get("year", None)

        if str(icq_zip_file).endswith(".zip") == False or icq_zip_file == None:
            print({"message": "Bad Request"})
        else:
            with tempfile.TemporaryDirectory() as tmpdir:
                file_dir = tmpdir

            with zipfile.ZipFile(icq_zip_file, "r") as zip_ref:
                zip_ref.extractall(tmpdir)
            tracking = TrackICQUpload.objects.create()
            user_id = 1  # Super Admin User Id
            import_bulk_icq_report(file_dir, year, user_id, tracking.id)
