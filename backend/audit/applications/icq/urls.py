from django.urls import path
from icq import views

urlpatterns = [
    path("", views.ICQReportList.as_view(), name="icqreport-list"),
    path("<int:pk>/", views.ICQAPIView.as_view(), name="icq-detail"),
    path(
        "overallrating/<int:agreement_id>/",
        views.OverallRatingAPIView.as_view(),
        name="icqreport-list",
    ),
    # path('overallrating-exp/<int:agreement_id>/', views.OverallRatingExpAPIView.as_view(), name='icqreport-list-exp'),
    path("<int:agreement_id>/", views.ICQsAPIView.as_view(), name="icqreport"),
    path(
        "major_findings/<int:agreement_id>/",
        views.ICQsAPIView.as_view(),
        name="icqreport-major_findings",
    ),
    path("upload/", views.ICQReportList.as_view(), name="icq-upload"),
    path("download/", views.ICQDownloadView.as_view(), name="icq-download"),
    path(
        "agreements/<int:agreement_id>/mgmts/",
        views.MgmtLettersAPIView.as_view(),
        name="mgmt-letter",
    ),
    # path('agreements/<int:agreement_id>/mgmts-exp/', views.MgmtLettersExportAPIView.as_view(), name='mgmt-letter-exp'),
    path(
        "agreements/<int:agreement_id>/mgmts/<int:pk>/",
        views.MgmtLetterAPIView.as_view(),
        name="mgmt-letter-detail",
    ),
    path(
        "mgmts/general/<int:pk>/",
        views.MgmtLetterGeneralAPIView.as_view(),
        name="mgmt-letter-general-detail",
    ),
    path(
        "mgmts/fin_findings/<int:pk>/",
        views.FinancialFindingAPIView.as_view(),
        name="fin_findings-detail",
    ),
    path(
        "mgmts/follow_ups/<int:pk>/",
        views.FollowUpAPIView.as_view(),
        name="follow_ups-detail",
    ),
    path(
        "download-icq-template/",
        views.FileDownloadListAPIView.as_view(),
        name="download-file",
    ),
    # path('test-icq/', views.MGMTLetterPDFExport.as_view(), name='test_icq'),
    path(
        "export-icqsummary/<int:year>/",
        views.ExportIcqSummary.as_view(),
        name="exporticqsummary",
    ),
    path(
        "export-mgmt-letter/<int:agreement_id>/",
        views.MGMTLetterPDFExport.as_view(),
        name="export-mgmt-letter",
    ),
    path(
        "icq-bulk-upload/<int:year>/",
        views.ICQBulkUpload.as_view(),
        name="icq-bulk-upload",
    ),
    path(
        "icq-bulk-upload-error/",
        views.ICQBulkUploadErrors.as_view(),
        name="icq-bulk-upload-error",
    ),
]
