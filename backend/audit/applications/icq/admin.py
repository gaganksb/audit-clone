from django.contrib import admin
from .models import (
    Process,
    ProcessTitle,
    GeneralRisk,
    KeyControl,
    KeyControlTitle,
    KeyControlAttr,
    ICQReport,
    ICQ,
    MgmtLetter,
    General,
    FinancialFindings,
    OverallAssessment,
    FollowUp,
)

admin.site.register(Process)
admin.site.register(ProcessTitle)
admin.site.register(GeneralRisk)
admin.site.register(KeyControl)
admin.site.register(KeyControlTitle)
admin.site.register(KeyControlAttr)
admin.site.register(ICQReport)
admin.site.register(ICQ)
admin.site.register(MgmtLetter)
admin.site.register(General)
admin.site.register(FinancialFindings)
admin.site.register(OverallAssessment)
admin.site.register(FollowUp)
