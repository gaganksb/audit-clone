import os
import xlrd
import json
from django.contrib.postgres import fields
from django.db import models
from rest_framework import serializers
from icq.models import (
    ICQ,
    General,
    Process,
    ProcessTitle,
    GeneralRisk,
    KeyControl,
    KeyControlTitle,
    KeyControlAttr,
    ICQReport,
    FinancialFindings,
    MgmtLetter,
    FollowUp,
    OverallAssessment,
    TrackICQUpload,
)
from django.db.models import Q
from rest_framework import exceptions
from django.db import transaction
from partner.serializers import PartnerSerializer  # PartnerSerializer
from common.models import CommonFile
from common.enums import UserTypeEnum
from project.models import Agreement
from common.serializers import (
    CommonFileSerializer,
    CommonUserSerializer,
    BusinessUnitSerializer,
)
from django.utils import timezone
from icq.calc import CalcICQScore

from icq.tasks import notify_mgmt_letter_enabled, notify_mgmt_letter_publish


class ProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = "__all__"


class ProcessTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcessTitle
        fields = "__all__"


class GeneralRiskSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneralRisk
        fields = "__all__"


class KeyControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = KeyControl
        fields = "__all__"


class KeyControlTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = KeyControlTitle
        fields = "__all__"


class KeyControlAttrSerializer(serializers.ModelSerializer):
    class Meta:
        model = KeyControlAttr
        fields = "__all__"


class ICQSerialzier(serializers.ModelSerializer):

    process = ProcessSerializer()
    process_title = ProcessTitleSerializer()
    general_risk = GeneralRiskSerializer()
    key_control = KeyControlSerializer()
    key_control_title = KeyControlTitleSerializer()
    key_control_attr = KeyControlAttrSerializer()

    class Meta:
        model = ICQ
        fields = "__all__"


class GeneralSerializer(serializers.ModelSerializer):
    class Meta:
        model = General
        fields = "__all__"
        read_only_fields = ("mgmt_letter",)

    def update(self, instance, validated_data):
        data = super(GeneralSerializer, self).update(instance, validated_data)
        MgmtLetter.objects.filter(id=instance.mgmt_letter_id).update(published=False)
        return data


class FinancialFindingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinancialFindings
        fields = "__all__"
        read_only_fields = ("mgmt_letter",)

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        return serializer


class FollowUpSerializer(serializers.ModelSerializer):
    control_attribute_code = serializers.ReadOnlyField(source="control_attribute.title")

    class Meta:
        model = FollowUp
        fields = [
            "control_attribute",
            "control_attribute_code",
            "criticality",
            "created_date",
            "id",
            "prev_recommendation",
            "recommendation",
            "summary",
        ]
        read_only_fields = ("mgmt_letter",)

    def update(self, instance, validated_data):

        data = super(FollowUpSerializer, self).update(instance, validated_data)
        MgmtLetter.objects.filter(id=instance.mgmt_letter_id).update(published=False)
        return data


class OverallAssessmentSerialzier(serializers.ModelSerializer):
    class Meta:
        model = OverallAssessment
        fields = "__all__"
        depth = 1

    def to_representation(self, instance):

        serializer = super().to_representation(instance)
        serializer.update(
            {
                "overall_assessment": instance.overall_assessment,
                # "icq_report": None if instance.icq_report is None else instance.icq_report,
            }
        )
        return serializer


class MgmtLetterSerializer(serializers.ModelSerializer):
    class Meta:
        model = MgmtLetter
        exclude = [
            "last_modified_date",
        ]

    def fin_finding_calc(self, financial_findings_qs):

        required_recovery_total_usd = 0
        further_review_total_usd = 0
        no_recovery_total_usd = 0
        required_recovery_total_local = 0
        further_review_total_local = 0
        no_recovery_total_local = 0

        for i in financial_findings_qs.values(
            "required_recovery", "further_review", "no_recovery"
        ):
            required_recovery_total_usd += int(i["required_recovery"]["usd"])
            required_recovery_total_local += int(
                i["required_recovery"]["local_currency"]
            )

            further_review_total_usd += int(i["further_review"]["usd"])
            further_review_total_local += int(i["further_review"]["local_currency"])

            no_recovery_total_usd += int(i["no_recovery"]["usd"])
            no_recovery_total_local += int(i["no_recovery"]["local_currency"])

        final_data = {
            "required_recovery": {
                "total_usd": required_recovery_total_usd,
                "total_local": required_recovery_total_local,
            },
            "further_review": {
                "total_usd": further_review_total_usd,
                "total_local": further_review_total_local,
            },
            "no_recovery": {
                "total_usd": no_recovery_total_usd,
                "total_local": no_recovery_total_local,
            },
            "total": required_recovery_total_usd
            + further_review_total_usd
            + no_recovery_total_usd,
        }
        return final_data

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        related_mgmt_letter = {"can_be_published": instance.can_be_published()}
        sections = [
            "general",
            "fin_findings",
            "major_findings",
            "overall_rating",
            "follow_up",
        ]
        for section in sections:
            if section == "general":
                general_qs = General.objects.filter(mgmt_letter=instance)
                general_qs = GeneralSerializer(general_qs, many=True)
                related_mgmt_letter.update({"general": general_qs.data})
            elif section == "fin_findings":
                fin_findings_qs = FinancialFindings.objects.filter(mgmt_letter=instance)
                fin_findings = FinancialFindingsSerializer(fin_findings_qs, many=True)
                fin_findings_calc = self.fin_finding_calc(fin_findings_qs)
                related_mgmt_letter.update(
                    {
                        "fin_findings": fin_findings.data,
                        "fin_findings_calc": fin_findings_calc,
                    }
                )
            elif section == "follow_up":
                follow_ups_qs = FollowUp.objects.filter(mgmt_letter=instance)
                follow_ups = FollowUpSerializer(follow_ups_qs, many=True)
                related_mgmt_letter.update({"follow_up": follow_ups.data})
        serializer.update(related_mgmt_letter)
        return serializer

    def validate(self, data):
        published = data.get("published", None)
        request = self.context["request"]
        if published == True:
            if request.method == "PATCH" and self.instance.can_be_published() == False:
                error = "Management letter can't be published, please check if all the required information has been update"
                raise serializers.ValidationError(error)
            else:
                data["publish_date"] = timezone.now()
        return data

    def data_validation(self, data):
        for item in data:
            validation_col = ["required_recovery", "further_review", "no_recovery"]
            if item in validation_col:
                obj = data[item]
                try:
                    usd = int(obj["usd"])
                    local_currency = int(obj["local_currency"])
                except:
                    validation_cols = ", ".join(validation_col)
                    raise serializers.ValidationError(
                        {
                            "error": "Only integer values are allowed in {0}".format(
                                validation_cols
                            )
                        }
                    )

    @transaction.atomic
    def update(self, instance, validated_data):
        request = self.context["request"]

        agreement = validated_data.pop("agreement", None)
        general = request.data.pop("general", None)
        fin_findings = request.data.pop("fin_findings", None)
        major_findings = request.data.pop("major_findings", None)
        overall_assessments = request.data.pop("overall_assessments", None)
        follow_up = request.data.pop("follow_up", None)

        if general is not None:
            general_obj = General(mgmt_letter=instance, **general)
            general_obj.save()

        if fin_findings is not None:
            self.data_validation(fin_findings)
            fin_finding_obj = FinancialFindings(mgmt_letter=instance, **fin_findings)
            fin_finding_obj.save()

        if follow_up is not None:
            control_attribute = follow_up.pop("control_attribute")
            followup_obj = FollowUp(
                mgmt_letter=instance,
                control_attribute_id=control_attribute,
                **follow_up
            )
            followup_obj.save()

        for (key, value) in validated_data.items():
            setattr(instance, key, value)
        instance.published = False

        agreement_id = self.context["view"].kwargs.get("agreement_id", None)
        if "published" in request.data and request.data["published"] == True:
            instance.published = True
            notify_mgmt_letter_publish(agreement_id)
        instance.save()
        return instance


class ICQTestReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICQ
        fields = "__all__"


class ICQReportSerializer(serializers.ModelSerializer):
    report_doc = CommonFileSerializer(read_only=True)
    user = CommonUserSerializer(read_only=True)
    related_projects = serializers.ListField(write_only=True)

    uploaded_file = None
    ICQ_COLUMNS = [
        "Process #",
        "Process",
        "General risk",
        "Key control #",
        "Key control title",
        "Key control attribute #",
        "Key control attribute",
        "Applicability of the control at the Partner?",
        "Existence of the control",
        "Applicability of the control at the Partner?",
        "Existence of the control",
        "Prior year recommendation status",
        "Comments",
        "Management letter finding",
        "Risk",
        "Risk Criticality",
        "Auditor's recommendation",
        "Management comments and proposed actions",
        "Deadlines for actions and responsible person",
    ]

    class Meta:
        model = ICQReport
        fields = "__all__"
        extra_fields = [
            "url",
        ]

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        business_unit_serializer = BusinessUnitSerializer(
            instance.business_unit, context={"request": self.context["request"]}
        )
        partner_serializer = PartnerSerializer(
            instance.partner, context={"request": self.context["request"]}
        )
        serializer.update(
            {
                "business_unit": business_unit_serializer.data,
                "partner": partner_serializer.data,
            }
        )
        return serializer

    def get_cell_range(self, xlr, sheet, start_col, start_row, end_col, end_row):
        try:
            workbook = xlrd.open_workbook(file_contents=xlr)
            sheet = workbook.sheet_by_name(sheet)
            return [
                sheet.row_slice(row, start_colx=start_col, end_colx=end_col + 1)
                for row in range(start_row, end_row + 1)
            ]
        except Exception as e:
            print("Error while reading the icq file", e)
            return []

    def validate_icq_file(self, request):
        try:
            # Todo: Find a better way to compare list
            self.uploaded_file = request.FILES[
                "report_doc"
            ].read()  # files_list[0].read()
            columns = self.get_cell_range(
                self.uploaded_file, "ICQ questions", 1, 6, 19, 6
            )
            data = self.get_cell_range(
                self.uploaded_file, "ICQ questions", 0, 0, 10, 117
            )
            if len(columns) == 0 or len(data) == 0:
                return False
            if str(columns[0]) != str(self.ICQ_COLUMNS):
                for index, i in enumerate(columns[0]):
                    if i.value == self.ICQ_COLUMNS[index]:
                        continue
                    else:
                        return False

            for index, i in enumerate(data):
                if (str(i) == str(data[index])) == True:
                    continue
                else:
                    return False
            return True
        except Exception as e:
            print("Error : ", e)
            return False

    @transaction.atomic
    def create(self, validated_data):
        related_projects = validated_data.pop("related_projects", [])
        if len(related_projects) == 0 or type(related_projects) != list:
            raise serializers.ValidationError("related_projects is a required field")
        else:
            related_projects = related_projects[0]
            related_projects = related_projects.split(",")
        request = self.context["request"]
        if request.user.membership["type"] != UserTypeEnum.AUDITOR.name:
            raise exceptions.PermissionDenied("You dont have permission to upload ICQ")

        valid = self.validate_icq_file(request)
        file = request.FILES["report_doc"]
        if valid:
            common_file = CommonFile.objects.create(file_field=file, user=request.user)
            for project in related_projects:
                agreement = Agreement.objects.filter(id=project).first()
                icq_report, _ = ICQReport.objects.get_or_create(
                    business_unit_id=agreement.business_unit_id,
                    partner_id=agreement.partner_id,
                    year=agreement.cycle.year,
                )
                icq_calc_instance = CalcICQScore(common_file.id)
                icq_calc_instance = icq_calc_instance.generate_score()
                if icq_calc_instance is None:
                    raise serializers.ValidationError("Error while uploading ICQ File")
                overall_score_perc = icq_calc_instance["overall_score_perc"]
                process_wise_score = icq_calc_instance["process_wise_score"]
                key_control_attrs = icq_calc_instance["key_control_attrs"]
                icq_report.overall_score_perc = overall_score_perc
                icq_report.process_wise_score = process_wise_score
                icq_report.key_control_attrs = key_control_attrs
                icq_report.report_doc = common_file
                icq_report.user = request.user
                icq_report.save()

                if agreement is not None:
                    agreement.icq_report = icq_report
                    agreement.save()
            ## Updating the file data in database
            self.update_icq_data(icq_report.id)
            notify_mgmt_letter_enabled(related_projects)
            return icq_report
        else:
            raise serializers.ValidationError("Invalid ICQ file.")

    def update_icq_data(self, icq_report_id):
        data = self.get_cell_range(self.uploaded_file, "ICQ questions", 1, 8, 19, 117)
        workbook = xlrd.open_workbook(file_contents=self.uploaded_file)
        sheet = workbook.sheet_by_name("ICQ questions")

        fina_data = []
        for row in data:
            row_data = {}
            for index, column in enumerate(self.ICQ_COLUMNS):
                cell_data = row[index].value
                row_data[column] = cell_data
            fina_data.append(row_data)

        overall_rating = {
            "A": sheet.cell_value(34, 10),
            "B": sheet.cell_value(48, 10),
            "C": sheet.cell_value(54, 10),
            "D": sheet.cell_value(69, 10),
            "E": sheet.cell_value(74, 10),
            "F": sheet.cell_value(81, 10),
            "G": sheet.cell_value(93, 10),
            "H": sheet.cell_value(100, 10),
        }

        stored_processes = Process.objects.all().values_list("title", flat=True)

        # Delete old data if new file is uploaded
        old_icq_qs = ICQ.objects.filter(icq_report_id=icq_report_id)
        if old_icq_qs.exists():
            old_icq_qs.delete()

        for row in fina_data:
            try:

                if "Process #" in row and row["Process #"] in stored_processes:

                    process = Process.objects.filter(
                        title__icontains=row["Process #"].strip()
                    )
                    process_title = ProcessTitle.objects.filter(
                        title__icontains=row["Process"].strip()
                    )

                    if row["Process #"] in [
                        "I1",
                        "I2",
                        "I3",
                        "I4",
                        "I5",
                        "J1",
                        "J2",
                        "J3",
                        "J4",
                        "J5",
                        "J6",
                    ]:
                        additional_info = row["Process"]
                        additional_info_result = row["Key control #"]
                        additional_info_comment = row["Key control title"]
                        icq = ICQ.objects.create(
                            icq_report_id=icq_report_id,
                            process=process.first(),
                            additional_info=additional_info,
                            additional_info_result=additional_info_result,
                            additional_info_comment=additional_info_comment,
                        )
                        continue

                    if (
                        "\n" in row["General risk"]
                    ):  # A workaround to deal with multiline text in the excel file
                        text = row["General risk"].split("\n")
                        line_one = text[0]
                        line_tow = text[1]
                        general_risk = GeneralRisk.objects.filter(
                            Q(title__icontains=line_one) & Q(title__icontains=line_tow)
                        )
                    else:
                        general_risk = GeneralRisk.objects.filter(
                            title__icontains=row["General risk"].strip()
                        )

                    if (
                        "\n" in row["Key control #"]
                    ):  # A workaround to deal with multiline text in the excel file
                        text = row["Key control #"].split("\n")
                        line_one = text[0]
                        line_tow = text[1]
                        key_control = KeyControl.objects.filter(
                            Q(title__icontains=line_one) & Q(title__icontains=line_tow)
                        )
                    else:
                        key_control = KeyControl.objects.filter(
                            title__icontains=row["Key control #"].strip()
                        )

                    if (
                        "\n" in row["Key control title"]
                    ):  # A workaround to deal with multiline text in the excel file
                        text = row["Key control title"].split("\n")
                        line_one = text[0]
                        line_tow = text[1]
                        key_control_title = KeyControlTitle.objects.filter(
                            Q(title__icontains=line_one) & Q(title__icontains=line_tow)
                        )
                    else:
                        key_control_title = KeyControlTitle.objects.filter(
                            title__icontains=row["Key control title"].strip()
                        )

                    if (
                        "\n" in row["Key control attribute #"]
                    ):  # A workaround to deal with multiline text in the excel file
                        text = row["Key control attribute #"].split("\n")
                        line_one = text[0]
                        line_tow = text[1]
                        key_control_attr = KeyControlAttr.objects.filter(
                            Q(title__icontains=line_one) & Q(title__icontains=line_tow)
                        )
                    else:
                        key_control_attr = KeyControlAttr.objects.filter(
                            title__icontains=row["Key control attribute #"].strip()
                        )

                    if all(
                        [
                            process,
                            process_title,
                            general_risk,
                            key_control,
                            key_control_title,
                            key_control_attr,
                        ]
                    ):

                        control_at_partner = row.get(
                            "Applicability of the control at the Partner?", ""
                        )
                        control_at_partner = (
                            None
                            if control_at_partner.strip() == ""
                            else control_at_partner
                        )

                        existance_at_control = row.get("Existence of the control", "")
                        existance_at_control = (
                            None
                            if existance_at_control.strip() == ""
                            else existance_at_control
                        )

                        prior_year_recommendation = row.get(
                            "Prior year recommendation status", ""
                        )
                        prior_year_recommendation = (
                            None
                            if prior_year_recommendation.strip() == ""
                            else prior_year_recommendation
                        )

                        comment = row.get("Comments", "")
                        comment = None if comment.strip() == "" else comment

                        mgmt_letter_finding = row.get("Management letter finding", "")
                        mgmt_letter_finding = (
                            None
                            if mgmt_letter_finding.strip() == ""
                            else mgmt_letter_finding
                        )

                        risk = row.get("Risk", "")
                        risk = None if risk.strip() == "" else risk

                        risk_criticality = row.get("Risk Criticality", "")
                        risk_criticality = (
                            None if risk_criticality.strip() == "" else risk_criticality
                        )

                        auditor_recommendation = row.get("Auditor's recommendation", "")
                        auditor_recommendation = (
                            None
                            if auditor_recommendation.strip() == ""
                            else auditor_recommendation
                        )

                        management_proposed_actions = row.get(
                            "Management comments and proposed actions", ""
                        )
                        management_proposed_actions = (
                            None
                            if management_proposed_actions.strip() == ""
                            else management_proposed_actions
                        )

                        actions_deadlines = row.get(
                            "Deadlines for actions and responsible person", ""
                        )
                        actions_deadlines = (
                            None
                            if actions_deadlines.strip() == ""
                            else actions_deadlines
                        )

                        additional_info = row.get("additional_info", "")
                        additional_info = (
                            None if additional_info.strip() == "" else additional_info
                        )

                        additional_info_result = row.get("additional_info_result", "")
                        additional_info_result = (
                            None
                            if additional_info_result.strip() == ""
                            else additional_info_result
                        )

                        additional_info_comment = row.get("additional_info_comment", "")
                        additional_info_comment = (
                            None
                            if additional_info_comment.strip() == ""
                            else additional_info_comment
                        )
                        icq = ICQ.objects.create(
                            icq_report_id=icq_report_id,
                            process=process.first(),
                            process_title=process_title.first(),
                            general_risk=general_risk.first(),
                            key_control=key_control.first(),
                            key_control_title=key_control_title.first(),
                            key_control_attr=key_control_attr.first(),
                            control_at_partner=control_at_partner,
                            existance_at_control=existance_at_control,
                            prior_year_recommendation=prior_year_recommendation,
                            comment=comment,
                            mgmt_letter_finding=mgmt_letter_finding,
                            risk=risk,
                            risk_criticality=risk_criticality,
                            auditor_recommendation=auditor_recommendation,
                            management_proposed_actions=management_proposed_actions,
                            actions_deadlines=actions_deadlines,
                            additional_info=additional_info,
                            additional_info_result=additional_info_result,
                            additional_info_comment=additional_info_comment,
                        )
                    else:
                        raise exceptions.APIException(
                            "Error in row {}".format(str(row))
                        )
            except Exception as e:
                raise exceptions.APIException("Error in row {}".format(str(row)))
        overall_assessment, created = OverallAssessment.objects.get_or_create(
            icq_report_id=icq_report_id
        )
        overall_assessment.overall_rating = overall_rating
        overall_assessment.save()

    @transaction.atomic
    def update(self, instance, validated_data):
        request = self.context["request"]
        files_list = request.data.getlist("report_file", [])

        if request.method == "PUT":
            raise exceptions.APIException("Method not allowed")

        icq_report = ICQReport.objects.filter(id=instance.id)
        icq = ICQ.objects.filter(icq_report_id=instance.id)
        icq.delete()
        if "report_file" in request.data:
            files_list = request.data.getlist("report_file")
            if len(files_list) == 1:
                # validate if the uploaded file is correct
                validated = self.validate_icq_file(request)

                if validated == False:
                    raise exceptions.APIException("Invalid File format")
                else:
                    self.update_icq_data(instance.id)
                    common_file = CommonFile.objects.create(
                        file_field=files_list[0], user=request.user
                    )
                    validated_data.update({"report_doc": common_file})
                    icq_report.update(**validated_data)
                    icq_report = ICQReport.objects.get(id=instance.id)
                    return icq_report
                raise exceptions.APIException("Invalid or multiple files")
            else:
                raise exceptions.APIException("Invalid or multiple files")
        else:
            updated = icq.update(**validated_data)
            icq = ICQReport.objects.get(id=instance.id)
            return icq


class ICQDownloadSerializer(serializers.ModelSerializer):
    report_doc = CommonFileSerializer()

    class Meta:
        model = ICQReport
        fields = "__all__"


class ICQBulkUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICQReport
        fields = ("report_doc",)


class TrackICQUploadSerializer(serializers.ModelSerializer):

    user = CommonUserSerializer()
    errors = serializers.SerializerMethodField()

    class Meta:
        model = TrackICQUpload
        fields = "__all__"

    def get_errors(self, instance):
        print(instance.errors)
        try:
            return eval(instance.errors)
        except Exception as e:
            return []
