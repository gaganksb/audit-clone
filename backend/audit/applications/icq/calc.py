import os
import xlrd
from django.db.models import Q
from icq.questions import QUESTIONS
from common.models import CommonFile


class CalcICQScore:
    def __init__(self, common_file_id):
        self.final_data = {}
        self.process_wise_score = {}
        self.questionnaire = self.get_questions(common_file_id, QUESTIONS)

    def icq_overall_score(self):  # <- from Rates!D6
        processes = (
            self.questionnaire.keys()
        )  # get processes from questionnaire, i.e., [A, B, ..., H]
        section_weight = [self.questionnaire[i]["weight"] for i in self.questionnaire]
        calc_section_weight = []
        for process in processes:
            weighted_process_score = self.weighted_process_score(
                self.questionnaire[process]
            )
            calc_section_weight.append(weighted_process_score["process_score_sum"])

        res = round(self.final_normalization(section_weight, calc_section_weight), 2)
        if res >= 1:
            return 1.00
        else:
            return res

    def weighted_process_score(self, process):  # <- from 'Weight per process'!G2
        weight = process["weight"]
        processes = [process[i] for i in process if i not in ["weight", "assessment"]]
        weights = [
            process[i]["weight"] for i in process if i not in ["weight", "assessment"]
        ]
        values = []
        for item in processes:
            values.append(self.process_score(item))
        ## normalize is returning incorrect value for some processes
        normalized_score = self.normalize(weight, weights, values)

        return {"normalized_score": normalized_score, "process_score_sum": sum(values)}

    def process_score(self, process):  # <- from 'Weighting sheet'!Y27
        attributes = [
            i
            for i in process
            if i not in ["weight", "assessment", "existence_of_control", "attr"]
        ]
        res = 0
        for attribute in attributes:
            attr = process[attribute]
            attr.update({"attr": attribute})
            existence_of_control = self.existence_of_control_icq(attr)
            res += existence_of_control
        return res

    def existence_of_control_icq(self, attribute):  # <- from 'Weighting sheet'!Y2
        if "." in attribute["attr"]:
            process, control, attr = self.parse_attribute_data(attribute["attr"])
            if self.questionnaire[process][control][attr]["existence_of_control"] in [
                "Adequate",
                "Adecuado",
                "Adéquat",
            ]:
                attr_weight = self.attribute_weight(attribute, process, control, attr)
                control_weight = self.control_weight(process, control, attr)
                res = attr_weight * control_weight
                return res
        return 0

    def attribute_weight(
        self, attribute, process, control, attr
    ):  # <- from 'Weighting sheet'!U2
        weight = attribute["weight"]
        attribute_details = self.get_siblings("attribute_weight", attribute["attr"])
        attributes = attribute_details["attributes"]
        weights = attribute_details["weights"]
        values = []
        for item in attributes:
            if item != "attr":
                applicability = self.parse_applicability(
                    self.questionnaire[process][control][item]["applicability"]
                )  # TODO: parse = 1 if input "Yes" else 0
                values.append(applicability)
        attr_weight = self.normalize(weight, weights, values)
        return attr_weight

    def control_weight(self, process, control, attr):  # <- from 'Weighting sheet'!W2
        weight = QUESTIONS[process][control]["weight"]
        control_details = self.get_siblings("control_weight", control)
        controls = control_details["controls"]
        weights = control_details["weights"]
        values = []
        for item in controls:
            applicability = self.parse_applicability(self.applicable_weight(item))
            values.append(applicability)
        response = self.normalize(weight, weights, values)
        return response

    def applicable_weight(self, control):  # <- from 'Weighting sheet'!V2
        response = self.get_siblings("attribute_weight", control)
        attributes = response["attributes"]
        res = 0
        for attr in attributes:
            if attr != "attr":
                process = attr[0]
                control = attr.split(".")[0]
                attribute = QUESTIONS[process][control][attr]
                attribute.update({"attr": attr})
                attr_weight = self.attribute_weight(attribute, process, control, attr)
                res = res + attr_weight
        if res >= 0.999:
            return "Yes"
        return "No"

    def normalize(self, weight, weights, values):
        res = 0
        for i in range(len(values)):
            if values[i] != 0:
                res = res + weights[i]
        try:
            return weight / res
        except:
            return 0

    def final_normalization(self, section_weight, calc_section_weight):
        score = []
        re_weights = []
        for idx, val in enumerate(calc_section_weight):
            if calc_section_weight[idx] == 0:
                section_weight[idx] = 0

        for idx, val in enumerate(calc_section_weight):
            try:
                re_weights.append(round(section_weight[idx] / sum(section_weight), 2))
            except:
                re_weights.append(0)

        results = []
        for idx, val in enumerate(re_weights):
            results.append(re_weights[idx] * calc_section_weight[idx])

        return sum(results)

    def parse_attribute_data(self, attr):
        control = attr[0]
        process = attr.split(".")[0]
        return control, process, attr

    def get_siblings(self, req_function, attribute):
        if req_function == "attribute_weight":
            parent = attribute[0]
            child = attribute.split(".")[0]
            attributes = []
            weights = []
            for i in QUESTIONS[parent][child]:
                if i not in ["weight", "existence_of_control", "attr"]:
                    attributes.append(i)
                    weights.append(QUESTIONS[parent][child][i]["weight"])
            response = {"attributes": attributes, "weights": weights}
            return response
        elif req_function == "control_weight":
            parent = attribute[0]
            controls = []
            weights = []
            for i in QUESTIONS[parent]:
                if i not in ["weight", "existence_of_control", "assessment"]:
                    controls.append(i)
                    weights.append(QUESTIONS[parent][i]["weight"])
            response = {"controls": controls, "weights": weights}
            return response

    def parse_applicability(self, applicability):
        return 1 if applicability == "Yes" else 0

    def find_node(self, obj):
        if type(obj) == dict and len(obj) > 0:
            for k in obj:
                if type(obj) == dict and "existence_of_control" in obj:
                    self.final_data[obj["attr"]] = obj["existence_of_control"]
                elif type(obj) == dict:
                    self.find_node(obj[k])
        return self.final_data

    def get_cell_range(self, sheet, start_col, start_row, end_col, end_row):
        return [
            sheet.row_slice(row, start_colx=start_col, end_colx=end_col + 1)
            for row in range(start_row, end_row + 1)
        ]

    def get_questions(self, common_file_id, QUESTIONS):
        common_file_obj = CommonFile.objects.filter(id=common_file_id).first()
        if common_file_obj is not None:
            file = common_file_obj.file_field
            try:
                workbook = xlrd.open_workbook(file_contents=file.read())
                self.sheet = workbook.sheet_by_index(1)
                data = self.get_cell_range(self.sheet, 1, 8, 26, 117)
                col = self.get_cell_range(self.sheet, 1, 6, 26, 6)
            except Exception as e:
                return None
            column = [i.value for i in col[0]]
            column[7] = "applicability_prev_year"
            column[8] = "existence_of_control_prev_year"
            column[9] = "applicability"
            column[10] = "existence_of_control"
            processes = ["A", "B", "C", "D", "E", "F", "G", "H"]
            for row in data:
                try:
                    values = [i.value for i in row]
                    data_dict = dict(zip(column, values))
                    process = data_dict["Process #"]
                    Key_control = data_dict["Key control #"]
                    Key_control_attr = data_dict["Key control attribute #"]
                    applicability = data_dict["applicability"]
                    existence_of_control = data_dict["existence_of_control"]
                    if process in processes:
                        QUESTIONS[process][Key_control][Key_control_attr][
                            "applicability"
                        ] = applicability
                        QUESTIONS[process][Key_control][Key_control_attr][
                            "existence_of_control"
                        ] = existence_of_control
                except Exception as e:
                    continue
            return QUESTIONS
        else:
            return None

    def get_process_score(self):
        data = {}
        for process in self.questionnaire.keys():
            res = self.weighted_process_score(self.questionnaire[process])
            data[process] = res["process_score_sum"]
        return data

    def generate_score(self):
        try:
            if self.is_valid:
                overall_icq_score = self.icq_overall_score()
                process_wise_score = self.get_process_score()
                key_control_attrs = self.find_node(self.questionnaire)
                return {
                    "overall_score_perc": overall_icq_score,
                    "process_wise_score": process_wise_score,
                    "key_control_attrs": key_control_attrs,
                }
            else:
                return None
        except Exception as e:
            return None

    @property
    def is_valid(self):

        if self.questionnaire is None:
            return False
        else:
            return True
