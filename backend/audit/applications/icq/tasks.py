import os
import json
from django.db import transaction
from django.template.defaulttags import cycle
from notify.models import NotifiedUser
from django.utils import timezone
from project.models import Agreement
from background_task import background
from notify.helpers import send_notification, send_notification_to_users
from django.db.models import Q
from common.models import BusinessUnit
from partner.models import Partner
from account.models import User
from icq.models import ICQReport, TrackICQUpload
from icq.utils import ImportICQ
from django.core.mail import send_mail


@background(schedule=3)
def notify_mgmt_letter_publish(project):
    agreement = Agreement.objects.filter(id=project).first()
    if agreement is not None:
        # auditing_users = agreement.focal_points.filter(user_type="auditing").distinct('user_id')
        partner_and_field_users = agreement.focal_points.filter(
            Q(user_type="partner") | Q(user_type="unhcr")
        ).distinct("user_id")

        partner_and_field_users_list = partner_and_field_users.values_list(
            "user__id", flat=True
        )
        users = User.objects.filter(id__in=partner_and_field_users_list)
        for user in users:

            context = {
                "fullname": user.fullname,
                "project_title": agreement.title,
                "subject": "Management Letter is publish notification",
            }
            notification = send_notification(
                notification_type="mgmt_letter_publish",
                obj=user,
                users=[user],
                context=context,
            )

            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Management Letter is publish notification",
                        sent_as_email=False,
                    )
                    user_notified = send_notification_to_users(user)
            except Exception as e:
                print("Error while sending sent_project_selection_notification ", e)


@background(schedule=3)
def notify_mgmt_letter_enabled(projects):
    for project in projects:
        agreement = Agreement.objects.filter(id=project).first()
        if agreement is not None:
            auditing_users = agreement.focal_points.filter(
                user_type="auditing"
            ).distinct("user_id")
            auditing_user_list = auditing_users.values_list("user__id", flat=True)
            users = User.objects.filter(id__in=auditing_user_list)
            for user in users:
                context = {
                    "auditor_reviewer": user.fullname,
                    "project_title": agreement.title,
                    "number": agreement.number,
                    "subject": "Management Letter is enabled notification",
                }
                notification = send_notification(
                    notification_type="mgmt_letter_enabled",
                    obj=user,
                    users=[user],
                    context=context,
                )

                try:
                    with transaction.atomic():
                        user = NotifiedUser.objects.select_for_update().filter(
                            notification__name="Management Letter is enabled notification",
                            sent_as_email=False,
                        )
                        user_notified = send_notification_to_users(user)
                except Exception as e:
                    print("Error while sending sent_project_selection_notification ", e)


@background(schedule=3)
def create_icq_report(agreements):

    for agreement in agreements:

        try:
            partner = Partner.objects.get(number=agreement["partner__number"])
            bu = BusinessUnit.objects.get(code=agreement["business_unit__code"])
            year = agreement["cycle__year"]

            icq = ICQReport.objects.get_or_create(
                report_date=timezone.now(),
                partner=partner,
                year=year,
                business_unit=bu,
                overall_score_perc=0.00,
                is_default=True,
            )
        except Exception as e:
            print("Error", e)


def import_bulk_icq_report(file_dir, year, user_id, tracking_id):
    res = []
    for file in os.listdir(file_dir):
        file_path = os.path.join(file_dir, file)
        import_icq = ImportICQ(file_path, file, year, user_id)
        response, uploaded = import_icq.import_icq_data()
        if not uploaded:
            res.append(response)
    try:
        TrackICQUpload.objects.filter(id=tracking_id).update(
            errors=res, user_id=user_id
        )
    except:
        return res
