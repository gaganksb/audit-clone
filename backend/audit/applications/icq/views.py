import os
from tempfile import tempdir
import xlrd
import zipfile
from django.db import transaction
from django.db.models.query import QuerySet
from django.shortcuts import render
from rest_framework import generics, status, exceptions
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from icq.models import (
    ICQ,
    MgmtLetter,
    General,
    FinancialFindings,
    ICQReport,
    FollowUp,
    OverallAssessment,
    TrackICQUpload,
)

from icq.serializers import (
    ICQSerialzier,
    MgmtLetterSerializer,
    GeneralSerializer,
    FinancialFindingsSerializer,
    ICQReportSerializer,
    FollowUpSerializer,
    OverallAssessmentSerialzier,
    ICQDownloadSerializer,
    ICQBulkUploadSerializer,
    TrackICQUploadSerializer,
)
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from project.models import Agreement
from project.serializers import AgreementSerializer
from common.models import CommonFile
from icq.calc import CalcICQScore
from icq.filters import ICQFilter
from django.http import FileResponse

from django.http import HttpResponse
from wsgiref.util import FileWrapper
from icq.serializers import ICQTestReportSerializer
from common.permissions import IsAUDIT
import pdfkit
import csv
from icq.models import ICQReport
from django.template.defaulttags import register
from django.template.loader import render_to_string
from icq.tasks import import_bulk_icq_report


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


class ICQsAPIView(generics.ListAPIView):

    queryset = ICQ.objects.all()
    serializer_class = ICQSerialzier

    def get_queryset(self):
        agreement_id = self.kwargs.get("agreement_id", None)
        agreement = Agreement.objects.filter(id=agreement_id).first()
        if agreement is not None:
            return self.queryset.filter(
                icq_report=agreement.icq_report, existance_at_control="Inadequate"
            ).order_by("id")
        return self.queryset.none()

    def get(self, request, *args, **kwargs):

        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True)
        response = {
            "count": queryset.count(),
            "next": None,
            "previous": None,
            "results": serializer.data,
        }
        return Response(response)


class ICQAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = ICQ.objects.all()
    serializer_class = ICQTestReportSerializer


class MgmtLettersAPIView(generics.ListAPIView):

    queryset = MgmtLetter.objects.all()
    serializer_class = MgmtLetterSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        agreement = Agreement.objects.filter(id=kwargs["agreement_id"])
        if agreement.exists() == False:
            error = "Agreement id does not exists"
            raise exceptions.APIException(error)

        mgmt_letter, created = MgmtLetter.objects.get_or_create(
            agreement=agreement.first()
        )
        serializer = self.serializer_class(mgmt_letter)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MgmtLettersExportAPIView(generics.ListAPIView):

    queryset = MgmtLetter.objects.all()
    serializer_class = MgmtLetterSerializer

    def get(self, request, *args, **kwargs):
        agreement = Agreement.objects.filter(id=kwargs["agreement_id"])
        if agreement.exists() == False:
            error = "Agreement id does not exists"
            raise exceptions.APIException(error)

        mgmt_letter, created = MgmtLetter.objects.get_or_create(
            agreement=agreement.first()
        )
        serializer = self.serializer_class(mgmt_letter)
        return Response(serializer.data, status=status.HTTP_200_OK)


class MgmtLetterAPIView(generics.UpdateAPIView):

    queryset = MgmtLetter.objects.all()
    serializer_class = MgmtLetterSerializer
    permission_classes = (IsAuthenticated,)


class MgmtLetterGeneralAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = General.objects.all()
    serializer_class = GeneralSerializer
    permission_classes = (IsAuthenticated,)

    def __str__(self):
        return "general"


class FinancialFindingAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = FinancialFindings.objects.all()
    serializer_class = FinancialFindingsSerializer
    permission_classes = (IsAuthenticated,)

    def __str__(self):
        return "financialfindings"

    def patch(self, request, *args, **kwargs):

        user = request.user
        partner_response = request.data.get("partner_response", None)
        unhcr_comments = request.data.get("unhcr_comments", None)
        auditors_disagreement_comment = request.data.get(
            "auditors_disagreement_comment", None
        )
        error = "you are not authorized to perform this action"

        if partner_response and user.membership["type"] != "PARTNER":
            raise exceptions.PermissionDenied(error)
        if unhcr_comments and user.membership["type"] != "FO":
            raise exceptions.PermissionDenied(error)
        if auditors_disagreement_comment and user.membership["type"] != "AUDITOR":
            raise exceptions.PermissionDenied(error)

        instance = self.queryset.filter(id=kwargs["pk"]).first()
        if instance is not None:
            MgmtLetter.objects.filter(id=instance.mgmt_letter_id).update(
                published=False
            )

        return self.partial_update(request, *args, **kwargs)


class FollowUpAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = FollowUp.objects.all()
    serializer_class = FollowUpSerializer
    permission_classes = (IsAuthenticated,)


class ICQReportList(generics.ListCreateAPIView):
    queryset = ICQReport.objects.all()
    serializer_class = ICQReportSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = ICQFilter

    def get_queryset(self):
        membership = self.request.user.membership
        year = self.request.query_params.get("year", None)
        if year is None:
            raise exceptions.APIException("year is a required field")

        if membership["type"] == "AUDITOR":
            agreements = Agreement.objects.filter(
                cycle__year=year, audit_firm_id=membership["audit_agency"]["id"]
            )
            return self.queryset.filter(
                partner_id__in=agreements.values_list("partner_id", flat=True)
            )
        return self.queryset


class ICQDownloadView(generics.ListAPIView):
    queryset = ICQReport.objects.all()
    serializer_class = ICQDownloadSerializer
    permission_classes = (IsAuthenticated,)


class OverallRatingAPIView(generics.ListCreateAPIView):
    queryset = OverallAssessment.objects.all()
    serializer_class = OverallAssessmentSerialzier
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):

        agreement_id = self.kwargs["agreement_id"]
        agreement = Agreement.objects.filter(id=agreement_id).first()
        if agreement is None or agreement.icq_report is None:
            return self.queryset.none()
        else:
            queryset = self.queryset.filter(icq_report=agreement.icq_report)
            return queryset


class OverallRatingExpAPIView(generics.ListCreateAPIView):
    queryset = OverallAssessment.objects.all()
    serializer_class = OverallAssessmentSerialzier

    def get_queryset(self):

        agreement_id = self.kwargs["agreement_id"]
        agreement = Agreement.objects.filter(id=agreement_id).first()
        if agreement is None or agreement.icq_report is None:
            return self.queryset.none()
        else:
            queryset = self.queryset.filter(icq_report=agreement.icq_report)
            return queryset


class FileDownloadListAPIView(generics.ListAPIView):
    def get(self, request):
        file_name = "sample.xlsx"
        file_handle = os.path.join(
            os.getcwd(),
            "audit",
            "applications",
            "icq",
            "templates",
            "icq_template.xlsx",
        )
        document = open(file_handle, "rb")
        response = HttpResponse(document, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = 'attachment; filename="%s"' % file_name
        return response


class ICQTest(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        icq_file = request.FILES.get("icq_file")

        common_file = CommonFile.objects.create(file_field=icq_file, user=request.user)
        icq_calc_instance = CalcICQScore(common_file)
        icq_calc_instance = icq_calc_instance.generate_score()
        return Response({"msg": icq_calc_instance})


class ExportIcqSummary(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        year = self.kwargs["year"]
        fields = [
            "id",
            "business_unit__code",
            "partner__number",
            "partner__legal_name",
            "icq_agreements",
            "year",
            "overall_score_perc",
            "is_default",
            "key_control_attrs",
            "process_wise_score",
        ]

        columns = [
            "Id",
            "Business Unit",
            "Partner Number",
            "Partner Name",
            "PPA",
            "Year",
            "Overall_Score_Perc",
            "Is_Default",
        ]

        key_control_attrs = [
            "A1.1",
            "A1.2",
            "A1.3",
            "A1.4",
            "A2.1",
            "A2.2",
            "A2.3",
            "A2.4",
            "A2.5",
            "A2.6",
            "A3.1",
            "A3.2",
            "A4.1",
            "A5.1",
            "A6.1",
            "A6.2",
            "A6.3",
            "A7.1",
            "A8.1",
            "A8.2",
            "A8.3",
            "A9.1",
            "A10.1",
            "A10.2",
            "A11.1",
            "B1.1",
            "B1.2",
            "B1.3",
            "B1.4",
            "B1.5",
            "B2.1",
            "B3.1",
            "B3.2",
            "B4.1",
            "B5.1",
            "B6.1",
            "C1.1",
            "C2.1",
            "C3.1",
            "C4.1",
            "D1.1",
            "D1.2",
            "D2.1",
            "D2.2",
            "D2.3",
            "D3.1",
            "D4.1",
            "D5.1",
            "D5.2",
            "D6.1",
            "D7.1",
            "D8.1",
            "E1.1",
            "E1.2",
            "E2.1",
            "F1.1",
            "F2.1",
            "F2.2",
            "F2.3",
            "G1.1",
            "G1.2",
            "G2.1",
            "G2.2",
            "G2.3",
            "G3.1",
            "G3.2",
            "H1.1",
            "H1.2",
            "H2.1",
            "H2.2",
            "H3.1",
        ]

        process_wise_score = ["A", "B", "C", "D", "E", "F", "G", "H"]

        columns.extend(key_control_attrs)
        columns.extend(process_wise_score)
        items = ICQReport.objects.filter(year=year).values(*fields)
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachment; filename=icqsummary.csv"
        writer = csv.writer(response, delimiter=",")
        writer.writerow(columns)
        for obj in items:
            row_data = []
            key_control_attr_list = []
            for i in key_control_attrs:
                _ = obj["key_control_attrs"].get(i, "")
                key_control_attr_list.append(_)

            process_wise_score_list = []
            for i in process_wise_score:
                _ = obj["process_wise_score"].get(i, "")
                process_wise_score_list.append(_)

            del obj["key_control_attrs"]
            del obj["process_wise_score"]
            obj.update(
                {
                    "icq_agreements": "{}{}".format(
                        obj["business_unit__code"], obj["icq_agreements"]
                    )
                }
            )
            row_data.extend(obj.values())
            row_data.extend(key_control_attr_list)
            row_data.extend(process_wise_score_list)

            writer.writerow(row_data)
        return response


class MGMTLetterPDFExport(generics.ListAPIView):

    queryset = MgmtLetter.objects.all()
    serializer_class = MgmtLetterSerializer

    FINANCIAL_FINDINGS_TYPES = {
        "EXP_NOT_USED": "Expenditure not used for the purpose/objectives of PA",
        "NO_SUPP_DOC": "No supporting documentation",
        "INSUFF_DOC": "Insufficient supporting documentation (may include insufficient access to information, people or sites)",
        "COST_EXC": "Personnel costs exceed the agreed budget and/or amounts paid to ineligible persons",
        "MISS_CALC": "Partner Integrity Capacity and Support Costs (PICSC) Miscalculated",
        "CUTT_OFF_ERROR": "Cut-off errors: commitments recorded as expenditures and/or expenditure committed after the end of the project implementation period and/or when payments were issued after the liquidation period but the deliverables were not received by the end of the implementation period. ",
        "INAPPROP_EXC_RATE": "Inappropriate exchange rate used",
        "EXCESS_TRANSFER": "Budgetary transfers made in excess of 30% limit at the output level without UNHCR approval",
        "FIN_INFO_NON_RECON": "Financial information not reconciled to GL (when PFR items are not traceable into the GL)",
        "NO_PROOF": "No proof of goods/services  received or activities undertaken",
        "INCORRECT_VAT": "VAT incorrectly claimed or a VAT valid exemption not utilized",
        "OTH_INCOME": "Other Income (e.g. bank interest) not reported or miscalculated",
        "DOUBLE_CHARGE": "Expenditure double-funded or double charged or double recorded /reported",
        "OTHER_FIN_FINDING": "Other financial findings that did not fall within any of the other headings (please clarify).",
    }

    PROCESSES = {
        "A": "Payment and Bank Management	Requires improvements",
        "B": "Procurement	Adequate",
        "C": "Management of Activities Subcontracted to Sub-Partners	N/A",
        "D": "Personnel Engagement and Payment	Adequate",
        "E": "Budget	Adequate",
        "F": "Goods and Property	N/A",
        "G": "General Control Environment	Adequate",
        "H": "Compliance with other terms of the Partnership Agreement",
    }

    def get(self, request, *args, **kwargs):

        agreement = Agreement.objects.filter(id=kwargs["agreement_id"]).first()
        mgmt_letter = self.queryset.filter(agreement_id=kwargs["agreement_id"]).first()

        if agreement is None or mgmt_letter is None:
            error = "<h4>Bad request, Agreement or management letter related to agreement doesn't exists</h4>"
            pdf = pdfkit.from_string(error, False, options)
            response = HttpResponse(pdf, content_type="application/pdf")
            response["Content-Disposition"] = "attachment;"
            filename = "mgmt_letter.pdf"
            return response
        else:
            mgmt_serializer = self.serializer_class(
                mgmt_letter, context={"request": request}
            )
            agreement_serializer = AgreementSerializer(
                agreement, context={"request": request}
            )
            major_findings_qs = ICQ.objects.filter(
                icq_report=agreement.icq_report, existance_at_control="Inadequate"
            ).order_by("id")
            overall_assessment = OverallAssessment.objects.filter(
                icq_report=agreement.icq_report
            )
            icq_serialzier = ICQSerialzier(major_findings_qs, many=True)
            overall_assessment_serializer = OverallAssessmentSerialzier(
                overall_assessment, many=True
            )

            html = render_to_string(
                "mgmt_export.html",
                {
                    "mgmt_letter": mgmt_serializer.data,
                    "agreement": agreement_serializer.data,
                    "FINANCIAL_FINDINGS_TYPES": self.FINANCIAL_FINDINGS_TYPES,
                    "major_findings": icq_serialzier.data,
                    "overall_assessment": overall_assessment_serializer.data,
                    "PROCESSES": self.PROCESSES,
                },
            )
            options = {
                "page-size": "Letter",
                "encoding": "UTF-8",
            }
            pdf = pdfkit.from_string(html, False, options)
            response = HttpResponse(pdf, content_type="application/pdf")
            response["Content-Disposition"] = "attachment;"
            filename = "mgmt_letter.pdf"
            return response

            # return render(
            #     request, 'mgmt_export.html', {
            #         "mgmt_letter": mgmt_serializer.data,
            #         "agreement": agreement_serializer.data,
            #         "FINANCIAL_FINDINGS_TYPES": self.FINANCIAL_FINDINGS_TYPES,
            #         "major_findings": icq_serialzier.data,
            #         "overall_assessment": overall_assessment_serializer.data,
            #         "PROCESSES": self.PROCESSES
            #     }
            # )


class ICQBulkUpload(generics.CreateAPIView):

    queryset = ICQReport.objects.all()
    serializer_class = ICQBulkUploadSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        icq_zip_file = request.FILES.get("icq_zip_file", None)
        year = kwargs["year"]
        if str(icq_zip_file).endswith(".zip") == False or icq_zip_file == None:
            return Response({"message": "Bad Request"})
        else:
            import tempfile

            with tempfile.TemporaryDirectory() as tmpdir:
                file_dir = tmpdir

            with zipfile.ZipFile(icq_zip_file, "r") as zip_ref:
                zip_ref.extractall(tmpdir)
            tracking = TrackICQUpload.objects.create()
            import_bulk_icq_report(file_dir, year, request.user.id, tracking.id)
        return Response(
            {
                "message": "Files are uploading you will receive a notification once the upload is done"
            }
        )


class ICQBulkUploadErrors(generics.ListAPIView):

    queryset = TrackICQUpload.objects.all()
    serializer_class = TrackICQUploadSerializer
    permission_classes = (IsAuthenticated,)
