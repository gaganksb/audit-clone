# Generated by Django 2.2.1 on 2021-10-14 11:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('icq', '0010_auto_20210730_0255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='icqreport',
            name='overall_score_perc',
            field=models.DecimalField(decimal_places=8, default=None, max_digits=11, null=True),
        ),
    ]
