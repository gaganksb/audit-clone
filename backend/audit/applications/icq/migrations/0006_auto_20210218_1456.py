# Generated by Django 2.2.1 on 2021-02-18 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("icq", "0005_auto_20210218_1439"),
    ]

    operations = [
        migrations.AlterField(
            model_name="general",
            name="audit_objectives",
            field=models.TextField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name="general",
            name="executive_summary",
            field=models.TextField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name="general",
            name="introduction",
            field=models.TextField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name="general",
            name="scope_of_work",
            field=models.TextField(default=None, null=True),
        ),
    ]
