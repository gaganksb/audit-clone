# Generated by Django 2.2.1 on 2021-04-15 05:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("icq", "0007_auto_20210319_0308"),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name="icqreport",
            unique_together=set(),
        ),
    ]
