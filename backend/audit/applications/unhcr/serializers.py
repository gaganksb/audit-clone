from django.apps import apps
from rest_framework import serializers
from django.urls import reverse
from django.http import HttpRequest
from unhcr.models import (
    Role as UNHCRRole,
    UNHCRMember,
)


class UNHCRRoleSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("unhcr:role-detail", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = UNHCRRole
        fields = "__all__"
        extra_fields = [
            "url",
        ]
        read_only_fields = [
            "role",
        ]


class UNHCRMemberSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    user = serializers.ReadOnlyField(source="user.fullname")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = UNHCRMember
        fields = ("user", "role", "url")

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default
            }
        )
        return serializer


class UNHCRMemberShortSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.fullname")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = UNHCRMember
        fields = (
            "id",
            "user",
            "role",
        )

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default,
                "user_type_role_id": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .id,
            }
        )
        return serializer


class UNHCRMemberDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = UNHCRMember
        fields = [
            "id",
        ]

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            if key == "role":
                value = UNHCRRole.objects.filter(role=value).last()
            if key == "name" or key == "email":
                instance.update_user_info(key, value)
                continue
            setattr(instance, key, value)
        instance.update()
        return instance

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        user = instance.user
        role = instance.role

        serializer.update(
            {
                "name": user.fullname,
                "email": user.email,
                "role": {"role": role.role, "id": role.id},
            }
        )
        return serializer
