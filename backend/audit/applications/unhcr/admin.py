from django.contrib import admin
from django.contrib.admin.forms import forms
from .models import Role, UNHCRMember


class UNHCRMemberForm(forms.ModelForm):
    temporary_password = forms.CharField(required=False)
    date_send_invitation = forms.DateField(required=False)


class UNHCRMemberAdmin(admin.ModelAdmin):
    search_fields = ("user__email", "role__role")
    list_display = (
        "user",
        "role",
    )
    ordering = ("user__email",)
    raw_id_fields = ("user",)
    form = UNHCRMemberForm


class RoleAdmin(admin.ModelAdmin):
    search_fields = ("role__role",)
    filter_horizontal = ("permissions",)

    readonly_fields = ("role",)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(UNHCRMember, UNHCRMemberAdmin)
admin.site.register(Role, RoleAdmin)
