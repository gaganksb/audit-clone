from common.models import Permission
from unhcr.enums import UNHCRRoleEnum, UNHCR_ROLE_PERMISSIONS
from unhcr.models import Role


def create_unhcr_roles():
    for role in UNHCRRoleEnum:
        _ = Role.objects.get_or_create(role=role.name)
        permissions = list(
            Permission.objects.filter(
                permission__in=UNHCR_ROLE_PERMISSIONS[role]
            ).values_list("id", flat=True)
        )
        _[0].permissions.remove(*_[0].permissions.all())
        for permission in permissions:
            _[0].permissions.add(permission)
