from django.urls import path
from unhcr import views

urlpatterns = [
    path("members/", views.UNHCRMemberList.as_view(), name="unhcrmember-list"),
    # member detail
    path(
        "member/<int:pk>/", views.UNHCRMemberDetail.as_view(), name="unhcrmember-detail"
    ),
    path("roles/", views.UNHCRRoleList.as_view(), name="role-list"),
    path("roles/<int:pk>/", views.UNHCRRoleDetail.as_view(), name="role-detail"),
]
