from django.db import models
from django.apps import apps
from django.conf import settings
from common.models import Permission, Country
from audit.core.abstract_model import AbstractModel


class Role(AbstractModel):
    role = models.CharField(max_length=255, unique=True, null=False)
    permissions = models.ManyToManyField(Permission, related_name="unhcr_permission")

    def __str__(self):
        return self.role


class UNHCRMember(AbstractModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="unhcr_members"
    )
    role = models.ForeignKey(Role, on_delete=models.CASCADE, null=False)
    temporary_password = models.CharField(max_length=255, null=True, default=None)
    date_send_invitation = models.DateField(null=True, default=None)
    is_password_change = models.BooleanField(null=False, default=False)

    def __init__(self, *args, **kwargs):
        self.user_key_mapping = {
            "name": "fullname",
            "email": "email",
        }
        self.user_type_role_model = apps.get_model("account.UserTypeRole")
        super().__init__(*args, **kwargs)

    def update_user_info(self, key, value):
        user = self.user
        setattr(user, self.user_key_mapping.get(key), value)
        user.save()

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        self.trigger_create()

    def trigger_create(self):
        utr = self.user_type_role_model(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
            user_id=self.user_id,
        )
        utr.save()

    def trigger_delete(self):
        utr_qs = self.user_type_role_model.objects.filter(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
        )
        if utr_qs.exists() is True:
            utr_qs.delete()
            self.delete()


class UNHCROffice(AbstractModel):
    name = models.CharField(max_length=255, unique=True, null=False)


class CountryOffice(AbstractModel):
    office = models.ForeignKey(UNHCROffice, on_delete=models.CASCADE, null=False)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=False)
