from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from unhcr.models import (
    Role as UNHCRRole,
    UNHCRMember,
)
from unhcr.serializers import (
    UNHCRRoleSerializer,
    UNHCRMemberSerializer,
    UNHCRMemberDetailSerializer,
)
from common.permissions import IsUNHCR
from utils.rest.code import code


class UNHCRRoleList(generics.ListCreateAPIView):
    queryset = UNHCRRole.objects.all()
    serializer_class = UNHCRRoleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class UNHCRRoleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UNHCRRole.objects.all()
    serializer_class = UNHCRRoleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class UNHCRMemberList(generics.ListAPIView):
    queryset = UNHCRMember.objects.all()
    serializer_class = UNHCRMemberSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get_queryset(self):
        name = self.request.query_params.get("legal_name")
        try:
            if name is not None:
                return FieldOffice.objects.all().filter(Q(name__icontains=name))
            return FieldOffice.objects.all()
        except Exception:
            return FieldOffice.objects.all()


class UNHCRMemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UNHCRMember.objects.all()
    serializer_class = UNHCRMemberDetailSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)
    resource_model = UNHCRMember

    def get(self, request, pk=None, *args, **kwargs):
        unhcr_member_qs = self.resource_model.objects.filter(pk=pk)
        if unhcr_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        unhcr_member = unhcr_member_qs.last()
        res = self.serializer_class(unhcr_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def patch(self, request, pk=None, *args, **kwargs):
        unhcr_member_qs = self.resource_model.objects.filter(pk=pk)
        if unhcr_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        unhcr_member = unhcr_member_qs.last()
        serializer = self.serializer_class(unhcr_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        unhcr_member = serializer.update(unhcr_member, validated_data=request.data)
        res = self.serializer_class(unhcr_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def put(self, request, pk=None, *args, **kwargs):
        unhcr_member_qs = self.resource_model.objects.filter(pk=pk)
        if unhcr_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        unhcr_member = unhcr_member_qs.last()
        serializer = self.serializer_class(unhcr_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        unhcr_member = serializer.update(unhcr_member, validated_data=request.data)
        res = self.serializer_class(unhcr_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)
