from django.apps import AppConfig


class UnhcrConfig(AppConfig):
    name = "unhcr"
