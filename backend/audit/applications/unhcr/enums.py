from enum import unique, auto

from common.enums import AutoNameEnum, PermissionListEnum


@unique
class UNHCRRoleEnum(AutoNameEnum):
    PREPARER = auto()
    REVIEWER = auto()
    APPROVER = auto()


UNHCR_ROLE_PERMISSIONS = {
    UNHCRRoleEnum.PREPARER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
        PermissionListEnum.CAN_MANAGE_USERS.name,
    ],
    UNHCRRoleEnum.REVIEWER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
        PermissionListEnum.CAN_MANAGE_USERS.name,
    ],
    UNHCRRoleEnum.APPROVER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
        PermissionListEnum.CAN_MANAGE_USERS.name,
    ],
}
