import csv
import datetime
from django.conf import settings
from common.models import BusinessUnit, Country, CostCenter, Currency
from project.models import (
    OIOSBusinessUnitReport,
    Situation,
    AgreementType,
    Pillar,
    Agreement,
    AgreementBudget,
    AgreementReport,
    Account,
    Goal,
    PopulationPlanningGroup,
    Cycle,
    SelectionSettings,
    SenseCheck,
    Status,
)
from partner.models import (
    Partner,
)
from django.db.models import Q

from collections import defaultdict
from django.apps import apps

ROOT_DATA = "/code/excel_data/preliminary-list/"


def import_oios():
    file_path = (
        settings.PROJECT_ROOT + "/../" + "applications/project/excel/oios-sep-18.csv"
    )
    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f, delimiter="\t")
        for business_unit, overall_rating in reader:
            business_unit = BusinessUnit.objects.filter(code=business_unit)
            overall_rating = overall_rating.replace(",", ".").strip()
            report_date = "2018-09-30"
            if len(business_unit) > 0:
                business_unit = business_unit[0]
                OIOSBusinessUnitReport(
                    report_date=report_date,
                    business_unit=business_unit,
                    overall_rating=overall_rating,
                ).save()


def import_situation():
    file_path = (
        settings.PROJECT_ROOT + "/../" + "applications/project/excel/situation.csv"
    )
    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f, delimiter="\t")
        codes = []
        for code in reader:
            if len(code) > 0 and code[0] not in codes:
                codes.append(code[0])
                Situation(code=code[0]).save()


def generate_preliminary_list(year):
    csv_path = (
        settings.PROJECT_ROOT
        + "/../../excel_data/preliminary-list/preliminary-list-"
        + str(year)
        + ".csv"
    )
    csv_file = open(csv_path, "w+")
    writer = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
    header = [
        "AgreementID",
        "Budget (USD)",
        "Procurement",
        "Staff Costs",
        "Cash",
        "Other accounts",
    ]
    writer.writerow(header)

    agreement_budgets = AgreementBudget.objects.filter(year=year)

    agreement_ids = list(
        agreement_budgets.distinct().values_list("agreement_id", flat=True)
    )
    counter = 0
    error_ids = []
    for agreement_id in agreement_ids:
        try:
            # prendi l'agreement_id (infatti esistono più project_agreement con lo stesso number)
            budget = agreement_budgets.filter(agreement__id=agreement_id)
            budget_usd = sum(list(budget.values_list("budget", flat=True)))
            procurement_accounts = budget.filter(
                account__account_type__description="Procurement"
            )
            procurement = sum(
                list(procurement_accounts.values_list("budget", flat=True))
            )
            other_accounts = budget.filter(
                account__account_type__description="Other accounts"
            )
            other = sum(list(other_accounts.values_list("budget", flat=True)))
            staff_cost_accounts = budget.filter(
                account__account_type__description="Cash"
            )
            staff_cost = sum(list(staff_cost_accounts.values_list("budget", flat=True)))
            cash_accounts = budget.filter(
                account__account_type__description="Staff Costs"
            )
            cash = sum(list(cash_accounts.values_list("budget", flat=True)))
            row = [agreement_id, budget_usd, procurement, staff_cost, cash, other]
            writer.writerow(row)
        except Exception as error:
            error_ids.append(agreement_id)
            print(error)
    csv_file.close()


def import_preliminary_list(year):
    file_path = (
        settings.PROJECT_ROOT
        + "/../../excel_data/preliminary-list/preliminary-list-"
        + str(year)
        + ".csv"
    )
    statuses = Status.objects.all()
    for status in statuses:
        Cycle.objects.get_or_create(year=int(year), status=status)

    SelectionSettings.objects.get_or_create(
        cycle=Cycle.objects.filter(year=int(year))[0],
        procurement_perc=0.2,
        cash_perc=0.2,
        staff_cost_perc=0.2,
        emergency_perc=0.1,
        audit_perc=0,
        oios_perc=0,
        risk_threshold=2.0,
    )

    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f)
        for data in reader:
            try:
                pass
            except Exception:
                print("[ERROR] Cannot import Preliminary List item")
