from django.urls import path
from project import views


urlpatterns = [
    path("goals/", views.GoalList.as_view(), name="goal-list"),
    path("goals/<int:pk>/", views.GoalDetail.as_view(), name="goal-detail"),
    path("agreements/", views.AgreementList.as_view(), name="agreement-list"),
    path("agreement/", views.AgreementCreate.as_view(), name="agreement-create"),
    path(
        "agreement/<int:pk>/",
        views.AgreementRetrieveUpdateDestroy.as_view(),
        name="agreement-retrieveupdatedestroy",
    ),
    path(
        "agreement-types/", views.AgreementTypeList.as_view(), name="agreementtype-list"
    ),
    path(
        "agreement-types/<int:pk>/",
        views.AgreementTypeDetail.as_view(),
        name="agreementtype-detail",
    ),
    path(
        "agreement-reports/",
        views.AgreementReportList.as_view(),
        name="agreementreport-list",
    ),
    path(
        "agreement-reports/<int:pk>/",
        views.AgreementReportDetail.as_view(),
        name="agreementreport-detail",
    ),
    path(
        "locations/<int:agreement_id>/",
        views.LocationAPIView.as_view(),
        name="locations-detail",
    ),
    path("locations/", views.LocationsAPIView.as_view(), name="locations-list"),
    path("<int:pk>/", views.AgreementDetail.as_view(), name="agreement-detail"),
    path("exp-/<int:pk>/", views.AgreementDetailExp.as_view(), name="agreement-detail"),
    path(
        "agreement/<int:pk>/<str:messages>/",
        views.AgreementMessageList.as_view(),
        name="agreement-message-list",
    ),
    path(
        "agreement/<int:pk>/message/<int:msg_id>/",
        views.AgreementMessageDetail.as_view(),
        name="agreement-message-detail",
    ),
    path(
        "budgets/<int:pk>/",
        views.AgreementBudgetDetail.as_view(),
        name="agreement_budget_detail",
    ),
    path("cycles/", views.CycleList.as_view(), name="cycle-list"),
    path("cycles/<int:year>/", views.CycleDetail.as_view(), name="cycle-detail"),
    path(
        "assignments/<int:budget_ref>/",
        views.AgreementAssignmentList.as_view(),
        name="assignments-list",
    ),
    path(
        "assignments/members/",
        views.AgreementMemberList.as_view(),
        name="agreementmember-list",
    ),
    path("modify/<int:year>/", views.ModifyAPIView.as_view(), name="modify-project"),
    path("latest-cycle/<int:year>/", views.CycleAPIView.as_view(), name="latest-cycle"),
    path(
        "promote/<int:year>/",
        views.PromoteProjectAPIView.as_view(),
        name="initialize-project",
    ),
    path("activity/<int:year>/", views.ActivityAPIView.as_view(), name="activity-list"),
    path("oios/", views.OIOSBusinessUnitReportsAPIView.as_view(), name="oios-list"),
    path(
        "oios/<int:pk>/",
        views.OIOSBusinessUnitReportAPIView.as_view(),
        name="oios-detail",
    ),
    path(
        "selection-settings/",
        views.SelectionSettingsAPIView.as_view(),
        name="selection-settings-list",
    ),
    path(
        "selection-settings/<int:year>/",
        views.SelectionSettingAPIView.as_view(),
        name="selection-settings-detail",
    ),
    path(
        "fo_progress/<int:year>/", views.FOProgressAPIView.as_view(), name="fo-progress"
    ),
    path(
        "selection_progress/<int:year>/",
        views.SelectionProgress.as_view(),
        name="selection-progress",
    ),
    path("hq-promote/<int:year>/", views.PromoteToFinal.as_view(), name="hq-promote"),
    path(
        "hq-dashboard/<int:year>/", views.HQDashboardView.as_view(), name="hq-dashboard"
    ),
    path(
        "modify-member/<int:pk>/",
        views.AgreementMemberAPIView.as_view(),
        name="modify-member",
    ),
    path(
        "project-selection-stats/<int:year>/",
        views.ProjectSelectionStats.as_view(),
        name="project-selection-stats",
    ),
    path("export/<int:year>/", views.AgreementListExport.as_view(), name="export"),
    path(
        "audit-assignment-status/<int:year>/",
        views.AuditAssignmentStatus.as_view(),
        name="audit-assignment-status",
    ),
    path(
        "export-audit-assignment/<int:year>/",
        views.AuditAssignmentExport.as_view(),
        name="export-audit-assignment",
    ),
    path(
        "audit-unassignment/<int:year>/",
        views.AuditorUnassignAPIView.as_view(),
        name="audit-unassignment",
    ),
    path(
        "delete-locations/<int:pk>/",
        views.LocationDeleteAPIView.as_view(),
        name="delete-locations",
    ),
    path("cancel-audit/", views.CancelledAuditList.as_view(), name="cancel-audit-list"),
    path(
        "cancel-audit/<int:pk>/",
        views.CancelledAuditDetail.as_view(),
        name="cancel-audit-detail",
    ),
    path(
        "cancel-audit-import/",
        views.CancelledAuditImportAPI.as_view(),
        name="cancel-audit-import",
    ),
    path(
        "cancel-audit-export/<int:year>/",
        views.ExportCancelledAudit.as_view(),
        name="cancel-audit-export",
    ),
    path(
        "modify-status/<int:year>/",
        views.ModifyStatusAPIView.as_view(),
        name="modify-status",
    ),
]
