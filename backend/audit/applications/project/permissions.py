from rest_framework.permissions import BasePermission
from project.models import Agreement
from field.models import FieldMember


class AgreementPermission(BasePermission):
    SAFE_METHODS = ["GET"]
    SAFE_USERS = ["HQ"]

    def has_permission(self, request, view):
        user = request.user
        agreement_id = request.resolver_match.kwargs.get("pk", None)
        if agreement_id == None:
            agreement_id = request.data.get("agreement", None)
            if agreement_id == None:
                return False

        # Allow any type of HQ or FO user to perform any action
        membership = user.membership
        if membership["type"] in self.SAFE_USERS:
            return True

        if request.method not in self.SAFE_METHODS:
            return False

        agreements = Agreement.objects.filter(id=agreement_id)
        # Check if the request user is a part of the reqested agreement
        if agreements.exists():

            # User is an agreement member / agreemnt focal point
            if user.id in agreements.first().focal_points.all().values_list(
                "user__id", flat=True
            ):
                return True

            # User if an admin of the partner firm related to the aggreement
            if membership["type"] == "PARTNER":
                partner_admins = (
                    agreements.first()
                    .partner.partner_firm_members.filter(role__role="ADMIN")
                    .values_list("user_id", flat=True)
                )
                if user.id in partner_admins:
                    return True

            # User if an admin of the audit agency related to the aggreement
            if membership["type"] == "AUDITOR":
                if agreements.first().audit_firm == None:
                    return False
                audit_admins = (
                    agreements.first()
                    .audit_firm.audit_agency_members.filter(role__role="ADMIN")
                    .values_list("user_id", flat=True)
                )
                if user.id in audit_admins:
                    return True

            if membership["type"] == "FO":
                if (
                    agreements.first().business_unit is not None
                    and agreements.first().business_unit.field_office is not None
                ):
                    field_office_id = agreements.first().business_unit.field_office_id
                    field_office_users = FieldMember.objects.filter(
                        field_office_id=field_office_id
                    ).values_list("user_id", flat=True)
                    if request.user.id in field_office_users:
                        return True
        return False


class IsAgreementMember(AgreementPermission):

    SAFE_METHODS = ["GET", "POST", "PATCH", "PUT", "DELETE"]
    def has_permission(self, request, view):
        return super().has_permission(request, view)