from django_filters import FilterSet
from django.db.models import Q, Sum
from .models import (
    Agreement,
    AgreementMember,
    SenseCheck,
    OIOSBusinessUnitReport,
    SelectionSettings,
    PreviousAudit,
    CancelledAudit,
)
from auditing.models import AuditWorksplitGroup
from django_filters.filters import (
    CharFilter,
    NumberFilter,
    BooleanFilter,
)
from django.db.models import (
    F,
    Subquery,
    Q,
    Case,
    When,
    Value,
    Count,
    OuterRef,
    IntegerField,
    DecimalField,
)
from icq.models import ICQReport
from rest_framework import exceptions
from common.helpers import filter_project_selection
from datetime import datetime
from partner.models import AdverseDisclaimer, Modified
import xlrd
import os

PA_MANUALLY_EXCLUDED = "SC12"


class SenseCheckFilter:

    queryset = Agreement.objects.all()
    settings = SelectionSettings.objects.all()

    def __init__(self, queryset, year):
        self.year = int(year)
        self.queryset = queryset.annotate(
            business_unit_code=F("business_unit__code"),
            engagement_date=F("partner__engagement_date"),
            oios=Case(
                When(
                    Q(business_unit__code__contains="UNHCR"),
                    then=Value(
                        OIOSBusinessUnitReport.objects.filter(
                            business_unit__code__contains="UNHCR", report_date__year=self.year
                        )
                        .first()
                        .overall_rating
                    ),
                ),
                default=F("business_unit__oios_report__overall_rating"),
                output_field=DecimalField(),
            ),
        )
        self.settings = self.settings.filter(cycle__year=self.year).first()

        if self.settings.oios_cutoff is not None:
            self.oios_top_20 = self.settings.oios_cutoff
        else:
            self.oios_top_20 = (
                OIOSBusinessUnitReport.objects.filter(report_date__year=self.year)
                .order_by("-overall_rating")[:20]
                .values_list("overall_rating", flat=True)
            )

    def check_included_in_pre_int(self):
        """
        Check if each project is included in preliminary and interim list
        """
        self.queryset = self.queryset.annotate(
            check_included_in_pre_int=Case(
                When(
                    (Q(int_sense_check__isnull=True) | Q(int_sense_check=False)),
                    then=Value(0),
                ),
                default=Value(1),
                output_field=IntegerField(),
            )
        )

    def check_risk_and_cost_benefit(self):
        """
        Check if the risk rating of each project is >= threshold
        Check if the cost benefit of each project is >= threshold
        Check if the value of prev filter check_in_int is false
        """
        self.check_included_in_pre_int()

        self.queryset = self.queryset.annotate(
            check_risk_and_cost_benefit=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(risk_rating__gte=self.settings.risk_threshold)
                    & Q(budget__gte=self.settings.cost_benefit),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_adverse_disclaimer(self):
        """
        Check if Partner Have Adverse or Disclaimer of Opinion in last 4 year
        Check if the cost benefit of each project is >= threshold
        Check if the value of all prev filters is 0
        """
        self.check_risk_and_cost_benefit()

        year = int(datetime.today().year)
        last_4_year = list(range(year - 4, year, 1))
        settings = SelectionSettings.objects.get(cycle__year=self.year)
        self.queryset = self.queryset.annotate(
            check_adverse_disclaimer=Count(
                Subquery(
                    AdverseDisclaimer.objects.filter(
                        business_unit=OuterRef("business_unit"),
                        partner=OuterRef("partner"),
                        year__in=last_4_year,
                    ).only("pk")[:1]
                )
            )
        ).annotate(
            check_adverse_disclaimer=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer__gte=1)
                    & Q(budget__gte=settings.cost_benefit),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_if_new_partner(self):
        """
        check if projects implemented by all new partner to UNHCR
        Check if the cost benefit of each project is >= threshold
        Check if the value of all prev filters is 0
        """
        self.check_adverse_disclaimer()

        self.queryset = self.queryset.annotate(
            check_if_new_partner=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(budget__gte=self.settings.cost_benefit)
                    & Q(engagement_date__year=self.settings.cycle.year),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_if_not_audited_in_last_4_year(self):
        """
        check if partners not audited in last_4_years
        Check if the cost benefit of each project is >= threshold
        Check if the value of all prev filters is 0
        """
        self.check_if_new_partner()
        year = self.year
        last_4_year = list(range(year - 4, year, 1))

        self.queryset = self.queryset.annotate(
            check_if_not_audited_in_last_4_year=Case(
                When(
                    Q(business_unit__code__contains="UNHCR"),
                    then=Count(
                        Subquery(
                            PreviousAudit.objects.filter(
                                business_unit__code__contains="UNHCR",
                                partner=OuterRef("partner"),
                                year__in=last_4_year,
                            ).only("pk")[:1]
                        )
                    ),
                ),
                default=Count(
                    Subquery(
                        PreviousAudit.objects.filter(
                            business_unit=OuterRef("business_unit"),
                            partner=OuterRef("partner"),
                            year__in=last_4_year,
                        ).only("pk")[:1]
                    )
                ),
                output_field=IntegerField(),
            )
        ).annotate(
            check_if_not_audited_in_last_4_year=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(budget__gte=self.settings.cost_benefit),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_if_qualified_opinion_last_year(self):
        """
        check if partners with qualified opinions in last year
        Check if the cost benefit of each project is >= threshold
        Check if the value of all prev filters is 0
        """
        self.check_if_not_audited_in_last_4_year()
        last_year = self.year - 1
        self.queryset = self.queryset.annotate(
            check_if_qualified_opinion_last_year=Count(
                Subquery(
                    Modified.objects.filter(
                        business_unit=OuterRef("business_unit"),
                        partner=OuterRef("partner"),
                        year=last_year,
                    ).only("pk")[:1]
                )
            )
        ).annotate(
            check_if_qualified_opinion_last_year=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year__gte=1)
                    & Q(budget__gte=self.settings.cost_benefit),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_high_values_threshold(self):
        """
        Check if high value of each project is greater or equal to threshold
        Check if the value of all prev filters is 0
        """
        self.check_if_qualified_opinion_last_year()
        self.queryset = self.queryset.annotate(
            check_high_values_threshold=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year=0)
                    & Q(budget__gte=self.settings.high_values),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_materiality_and_oios(self):
        """
        Check if materiality greater or equal to threshold
        Check if OIOS greater or equal to OIOS cut off for top 20 High Risk
        Check if the value of all prev filters is 0
        """
        try:
            ## check budget of the auditable projects
            self.check_high_values_threshold()
            agreements = Agreement.objects.filter(
                is_auditable=True, cycle__year=self.year
            )
            MARGIN_OF_ERROR = 0.05
            total_budget = agreements.aggregate(total_budget=Sum("budget"))
            one_percent_of_value = float(total_budget["total_budget"]) * 0.01
            five_percent_margin_of_error = one_percent_of_value * MARGIN_OF_ERROR
            materiality = round(five_percent_margin_of_error, -3)
        except Exception as e:
            print(e, "e")
            self.check_high_values_threshold()
            materiality = self.settings.materiality

        self.queryset = self.queryset.annotate(
            check_materiality_and_oios=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year=0)
                    & Q(check_high_values_threshold=0)
                    & Q(budget__gte=materiality)
                    & Q(oios__gte=self.oios_top_20),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_materiality_oios_and_budget(self):
        """
        Check if materiality greater or equal to threshold
        Check if OIOS greater or equal to OIOS cut off for top 20 High Risk
        Sort the result by high to low budget and filter 1st 10 records
        Check if the value of all prev filters is 0
        """
        self.check_materiality_and_oios()
        top_materiality_projects = (
            self.queryset.annotate(
                new_val=Case(
                    When(check_materiality_and_oios=1, then=F("budget")),
                    default=Value(0),
                    output_field=IntegerField(),
                ),
            )
            .order_by("-new_val")
            .values_list("id", flat=True)[:10]
        )

        self.queryset = self.queryset.annotate(
            check_materiality_oios_and_budget=Case(
                When(
                    Q(id__in=top_materiality_projects),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            ),
        ).order_by("-budget")

    def check_materiality_oios_budget_and_icq(self):
        """
        Check if materiality greater or equal to threshold
        Check if OIOS greater or equal to OIOS cut off for top 20 High Risk
        Sort the result by high to low budget and filter 1st 10 records
        Check if icq_score is >= sense_check_icq in selection settings
        Check if the value of all prev filters is 0
        """
        self.check_materiality_oios_and_budget()

        self.queryset = self.queryset.annotate(
            icq_score=Subquery(
                ICQReport.objects.filter(
                    business_unit=OuterRef("business_unit"),
                    partner=OuterRef("partner"),
                )
                .order_by("-year")
                .values("overall_score_perc")[:1]
            )
        ).annotate(
            check_materiality_oios_budget_and_icq=Case(
                When(
                    Q(check_materiality_and_oios=1)
                    & Q(check_materiality_oios_and_budget=0)
                    & Q(icq_score__lt=self.settings.sense_check_icq),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_not_audited_projects(self):
        """
        Partner Not Audited for 2016/17 agreements in 2018-2019
        previous filters should be 0
        budget greater than equal to 150000
        """
        self.check_materiality_oios_budget_and_icq()

        year = self.year
        interval_one = [year - 4, year - 3]
        interval_two = [year - 2, year - 1]
        self.queryset = self.queryset.annotate(
            check_interval_one=Case(
                When(
                    Q(business_unit__code__contains="UNHCR"),
                    then=Count(
                        Subquery(
                            PreviousAudit.objects.filter(
                                business_unit__code__contains="UNHCR",
                                partner=OuterRef("partner"),
                                year__in=interval_one,
                            ).only("pk")[:1]
                        )
                    ),
                ),
                default=Count(
                    Subquery(
                        PreviousAudit.objects.filter(
                            business_unit=OuterRef("business_unit"),
                            partner=OuterRef("partner"),
                            year__in=interval_one,
                        ).only("pk")[:1]
                    )
                ),
                output_field=IntegerField(),
            ),
            check_interval_two=Case(
                When(
                    Q(business_unit__code__contains="UNHCR"),
                    then=Count(
                        Subquery(
                            PreviousAudit.objects.filter(
                                business_unit__code__contains="UNHCR",
                                partner=OuterRef("partner"),
                                year__in=interval_two,
                            ).only("pk")[:1]
                        )
                    ),
                ),
                default=Count(
                    Subquery(
                        PreviousAudit.objects.filter(
                            business_unit=OuterRef("business_unit"),
                            partner=OuterRef("partner"),
                            year__in=interval_two,
                        ).only("pk")[:1]
                    )
                ),
                output_field=IntegerField(),
            ),
        ).annotate(
            check_not_audited_projects=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year=0)
                    & Q(check_high_values_threshold=0)
                    & Q(check_materiality_oios_and_budget=0)
                    & Q(check_materiality_oios_budget_and_icq=0)
                    & Q(Q(check_interval_one__gte=1) & Q(check_interval_two=0))
                    & Q(budget__gte=self.settings.cost_benefit),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_materiality_and_not_in_oios(self):
        """
        previous filters should be False
        budget greater than cost benefit but smaller than materiality
        Projects not in Top 20 OIOS Countries so OIOS smaller than oios_top_20
        """
        self.check_not_audited_projects()
        self.queryset = self.queryset.annotate(
            check_materiality_and_not_in_oios=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year=0)
                    & Q(check_high_values_threshold=0)
                    & Q(check_materiality_oios_and_budget=0)
                    & Q(check_materiality_oios_budget_and_icq=0)
                    & Q(check_not_audited_projects=0)
                    & Q(budget__gte=self.settings.cost_benefit)
                    & Q(budget__lt=self.settings.materiality)
                    & Q(oios__lt=self.oios_top_20),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def check_postponed_and_cancelled(self):
        """
        Check if the audit was cancelled in previous cycle
        Check if all prev filter values are False
        """
        year = self.year
        self.check_materiality_and_not_in_oios()
        self.queryset = self.queryset.annotate(
            CancelledAudits=Count(
                Subquery(
                    CancelledAudit.objects.filter(
                        business_unit=OuterRef("business_unit"),
                        partner=OuterRef("partner"),
                        year=year - 1,
                    ).only("pk")[:1]
                )
            )
        ).annotate(
            check_postponed_and_cancelled=Case(
                When(
                    Q(check_included_in_pre_int=0)
                    & Q(check_risk_and_cost_benefit=0)
                    & Q(check_adverse_disclaimer=0)
                    & Q(check_if_new_partner=0)
                    & Q(check_if_not_audited_in_last_4_year=0)
                    & Q(check_if_qualified_opinion_last_year=0)
                    & Q(check_high_values_threshold=0)
                    & Q(check_materiality_oios_and_budget=0)
                    & Q(check_materiality_oios_budget_and_icq=0)
                    & Q(check_not_audited_projects=0)
                    & Q(budget__gte=self.settings.cost_benefit)
                    & Q(CancelledAudits__gte=1),
                    then=Value(1),
                ),
                default=Value(0),
                output_field=IntegerField(),
            )
        )

    def load_test_data(self):
        filename = (
            "2020_Modulation-Similation_Jan 2021_ Final List - Final - 26 Jan 2021.xlsx"
        )
        file_path = os.path.join(
            os.getcwd(), "audit", "applications", "project", "excel", filename
        )

        wb = xlrd.open_workbook(file_path)
        sheet = wb.sheet_by_name("All Test - at 2.6 and $>150K ")
        test_data = {}
        for idx in range(3, sheet.nrows):
            try:

                business_unit = sheet.cell_value(idx, 2)
                number = sheet.cell_value(idx, 11)
                partner_number = sheet.cell_value(idx, 8)
                if number != "":
                    number = int(number)

                    test_data["{}{}".format(business_unit, number)] = dict(
                        business_unit=business_unit,
                        number=number,
                        partner_number=partner_number,
                        budget=sheet.cell_value(idx, 17),
                        risk_rating=sheet.cell_value(idx, 41),
                        icq_score=sheet.cell_value(idx, 35),
                        modified=sheet.cell_value(idx, 36),
                        oios=sheet.cell_value(idx, 39),
                        partner_score=sheet.cell_value(idx, 40),
                        goal=sheet.cell_value(idx, 22),
                        pqp=sheet.cell_value(idx, 23),
                        check_included_in_pre_int=int(sheet.cell_value(idx, 56)),
                        check_risk_and_cost_benefit=int(sheet.cell_value(idx, 57)),
                        check_adverse_disclaimer=int(sheet.cell_value(idx, 59)),
                        check_if_new_partner=int(sheet.cell_value(idx, 61)),
                        check_if_not_audited_in_last_4_year=int(
                            sheet.cell_value(idx, 63)
                        ),
                        check_if_qualified_opinion_last_year=int(
                            sheet.cell_value(idx, 65)
                        ),
                        check_high_values_threshold=int(sheet.cell_value(idx, 67)),
                        check_materiality_and_oios=int(sheet.cell_value(idx, 69)),
                        check_materiality_oios_and_budget=sheet.cell_value(idx, 71),
                        check_materiality_oios_budget_and_icq=int(
                            sheet.cell_value(idx, 73)
                        ),
                        check_not_audited_projects=int(sheet.cell_value(idx, 75)),
                        check_materiality_and_not_in_oios=int(
                            sheet.cell_value(idx, 77)
                        ),
                        check_postponed_and_cancelled=int(sheet.cell_value(idx, 79)),
                    )
            except Exception as e:
                print("Error while reading row {}{}".format(business_unit, number), e)
        return test_data

    def test_sensecheck(self, filter_methods=None):

        test_data = self.load_test_data()
        if filter_methods == None:
            filter_methods = "filter_all_sense_check"

        function = getattr(self, filter_methods)
        function()

        test_result = {}
        all_methods = [i for i in dir(self) if str(i).startswith("check_")]
        for method in all_methods:
            try:
                test_pass = 0
                test_fail = []
                for agreement in self.queryset.values():
                    ppa = "{}{}".format(
                        agreement["business_unit_code"], agreement["number"]
                    )
                    test = test_data.get(ppa)
                    if test is not None:
                        if agreement[method] == test[method]:
                            test_pass += 1
                        else:
                            test_fail.append({"agreement": agreement, "test": test})
                test_result[method] = {"test_pass": test_pass, "test_fail": test_fail}
            except:
                print("error")
        return test_result


class CommentsCycleFilter(FilterSet):
    def filter_year(self, queryset, value):
        return queryset

    def filter_membership_type(self, queryset, value):
        user = value
        user_offices = list(
            user.field_members.values_list("field_office__name", flat=True)
        )
        queryset = queryset.filter(
            user__field_members__field_office__name__in=user_offices
        )
        return queryset

    ## Filtering based on the request
    ## https://django-filter.readthedocs.io/en/master/guide/usage.html#filtering-the-primary-qs
    @property
    def qs(self):
        queryset = super().qs
        user = getattr(self.request, "user", None)
        year = self.request.parser_context["kwargs"].get("year", None)
        type = self.request.query_params.get("type", None)
        if user is not None and type == "FO":
            queryset = self.filter_membership_type(queryset, user)
        if year is not None:
            queryset = self.filter_year(queryset, year)
        return queryset


class AgreementAssignmentFilter(FilterSet):
    agreement_id = NumberFilter(field_name="id", lookup_expr="exact")
    partner = CharFilter(field_name="partner__legal_name", lookup_expr="icontains")
    business_unit = CharFilter(
        field_name="business_unit__code", lookup_expr="icontains"
    )
    budget_ref = NumberFilter(field_name="cycle__year", lookup_expr="exact")
    reasons = CharFilter(method="filter_reason")
    firm = CharFilter(field_name="audit_firm__legal_name", lookup_expr="icontains")
    group = CharFilter(method="filter_wordsplit_bu")
    view_items = CharFilter(method="filter_view_items")
    number = NumberFilter(field_name="number", lookup_expr="exact")
    # default = BooleanFilter(method="filter_default")

    # def __init__(self, *args, **kwargs):
    #     try:
    #         kwargs["data"]._mutable = True
    #         if "default" not in kwargs["data"]:
    #             kwargs["data"]["default"] = True
    #         kwargs["data"]._mutable = False
    #         super(AgreementAssignmentFilter, self).__init__(*args, **kwargs)
    #     except Exception as e:
    #         print("kwargs not available in filter")
    #         super(AgreementAssignmentFilter, self).__init__(*args, **kwargs)

    class Meta:
        models = Agreement
        fields = [
            "partner",
            "budget_ref",
            "reasons",
            "firm",
            "group",
            "agreement_id",
            "number",
            "business_unit",
        ]

    # def filter_default(self, queryset, name, value):
    #     if (
    #         name == "internal_default"
    #     ):  # A workaround to call the filter internally for batch project assignment
    #         budget_ref = value.get("budget_ref", None)
    #         my_own = None
    #         user = value.get("user", None)
    #     else:
    #         budget_ref = self.request.query_params.get("budget_ref", None)
    #         my_own = self.request.query_params.get("my_own", None)
    #         user = self.request.user

    #     membership = user.membership
    #     user_type_mapping = {
    #         "HQ": "unhcr",
    #         "AUDITOR": "auditing",
    #         "PARTNER": "partner",
    #         "FO": "field",
    #     }
        # if budget_ref is None:
        #     error = "budget_ref is a required field"
        #     raise exceptions.APIException(error)

        # if membership["type"] not in ["AUDITOR", "PARTNER", "FO", "HQ"]:
        #     error = "You are not authorized to perform this action"
        #     raise exceptions.APIException(error)

        ## Filter finalized projects
        # inc = SenseCheck.objects.exclude(code="SC12").values_list("code", flat=True)
        # if queryset.exists() == False:
        #     return queryset
        # cycle = queryset.first().cycle
        # risk_threshold = (
        #     SelectionSettings.objects.filter(cycle=cycle).first().risk_threshold
        # )
        # queryset = queryset.filter(cycle__year=budget_ref, status__code="FIN03")
        # queryset = queryset.filter(Q(fin_sense_check=True))

        ## Filter projects based on the role and user type
        # if (
        #     membership["type"] == "AUDITOR"
        #     and membership["role"] == "ADMIN"
        #     and my_own == None
        # ):
        #     audit_firm_id = membership["audit_agency"]["id"]
        #     return queryset.filter(audit_firm_id=audit_firm_id)
        # elif (
        #     membership["type"] == "PARTNER"
        #     and membership["role"] == "ADMIN"
        #     and my_own == None
        # ):
        #     partner_id = membership["partner"]["id"]
        #     return queryset.filter(partner_id=partner_id)
        # elif (
        #     membership["type"] == "FO"
        #     and membership["role"] == "APPROVER"
        #     and my_own == None
        # ):
        #     field_office = membership["field_office"]["id"]
        #     return queryset.filter(business_unit__field_office__id=field_office)
        # elif membership["type"] == "HQ":
        #     return queryset
        # elif membership["type"] == "AUDITOR" or membership["type"] == "PARTNER":
        #     return queryset.filter(
        #         focal_points__user_id=user.id,
        #         focal_points__user_type=user_type_mapping[membership["type"]],
        #     ).distinct("id")
        # else:
        #     return queryset.filter(focal_points__user_id=user.id)

    # def _filter_default(self, queryset, values):
    #     data = self.filter_default(queryset, "internal_default", values)
    #     return queryset

    def filter_wordsplit_bu(self, queryset, name, value):
        business_units = AuditWorksplitGroup.objects.filter(
            name__icontains=value
        ).values_list("business_units", flat=True)
        return queryset.filter(business_unit_id__in=business_units)

    def filter_reason(self, queryset, name, value):
        sensecheck = SenseCheck.objects.get(code=value)
        sensecheck_function = sensecheck.settings.get("function", None)
        year = queryset.first().cycle.year
        sensecheck_filter = SenseCheckFilter(queryset, year)
        function = getattr(sensecheck_filter, sensecheck_function)
        function()
        new_queryset = sensecheck_filter.queryset
        return new_queryset.filter(**{"{}".format(sensecheck_function): 1})

    def filter_view_items(self, queryset, name, value):
        try:
            user_type = value.split("__")[0]
            filter_type = value.split("__")[1]

            if user_type == "unhcr" and filter_type == "assigned":
                return queryset.filter(audit_firm__isnull=False)
            if user_type == "unhcr" and filter_type == "not_assigned":
                return queryset.filter(audit_firm__isnull=True)

            if user_type == "auditing" and filter_type == "assigned":
                return queryset.filter(
                    focal_points__user_type=user_type, focal_points__reviewer=True
                ).distinct("id")
            if user_type == "auditing" and filter_type == "not_assigned":
                agreement_with_reviewers = (
                    Agreement.objects.filter(focal_points__reviewer=True)
                    .distinct("id")
                    .values_list("id")
                )
                return queryset.filter(~Q(id__in=agreement_with_reviewers))

            if filter_type == "assigned":
                return queryset.filter(
                    focal_points__user_type=user_type, focal_points__focal=True
                ).distinct("id")
            elif filter_type == "not_assigned":
                return queryset.filter(~Q(focal_points__user_type=user_type)).distinct(
                    "id"
                )
            else:
                return queryset
        except:
            return queryset.none()


class AgreementMemberFilter(FilterSet):

    name = CharFilter(field_name="user__fullname", lookup_expr="icontains")
    business_unit_ids = CharFilter(method="filter_business_unit_id")
    worksplit_group_ids = CharFilter(method="filter_worksplit_group_ids")

    class Meta:
        models = AgreementMember
        model = AgreementMember
        fields = [
            "name",
            "business_unit_ids",
            "worksplit_group_ids",
        ]

    def filter_business_unit_id(self, queryset, name, value):
        bu_related_agency_members = (
            Agreement.objects.filter(business_unit_id__in=value.split(","))
            .distinct()
            .values_list("focal_points", flat=True)
        )
        return queryset.filter(id__in=bu_related_agency_members)

    def filter_worksplit_group_ids(self, queryset, name, value):
        worksplit_goup_related_agency_members = (
            Agreement.objects.filter(audit_worksplit_group_id__in=value.split(","))
            .distinct()
            .values_list("focal_points", flat=True)
        )
        return queryset.filter(id__in=worksplit_goup_related_agency_members)


class AgreementFilter(FilterSet):

    budget_ref = NumberFilter(field_name="cycle__year", lookup_expr="exact")
    year = NumberFilter(field_name="cycle__year", lookup_expr="exact")
    number = NumberFilter(field_name="number", lookup_expr="exact")
    business_unit__code = CharFilter(
        field_name="business_unit__code", lookup_expr="iexact"
    )
    business_unit = NumberFilter(field_name="business_unit_id", lookup_expr="exact")
    risk_rating = CharFilter(method="filter_risk_rating")
    view_items = CharFilter(method="filter_view_items")
    partner = CharFilter(field_name="partner__legal_name", lookup_expr="icontains")
    reason = CharFilter(method="filter_reason")
    status = CharFilter(method="filter_status")
    delta = BooleanFilter(field_name="delta", lookup_expr="exact")
    firm = CharFilter(field_name="audit_firm__legal_name", lookup_expr="iexact")

    class Meta:
        model = Agreement
        fields = ["budget_ref", "year", "delta"]

    def filter_status(self, queryset, name, value):
        status = queryset.values_list("cycle__status", flat=True)
        if value == "pre":
            if len(status) > 0 and status[0] > 3:
                return queryset
            return queryset.filter(cycle__status__id__in=[1, 2, 3])
        elif value == "int":
            if len(status) > 0 and status[0] > 5:
                return queryset
            return queryset.filter(cycle__status__id__in=[4, 5])
        elif value == "fin":
            return queryset.filter(cycle__status__id__in=[6, 7, 8])
        else:
            return queryset.none

    def filter_risk_rating(self, queryset, name, value):
        try:
            filter_type, value = value.split("__")
            value = float(value)
            if filter_type == "gte":
                return queryset.filter(risk_rating__gte=value)
            elif filter_type == "lte":
                return queryset.filter(risk_rating__lte=value)
            else:
                return queryset.none()
        except Exception as e:
            print("Error", e)
            return queryset.none()

    def filter_reason(self, queryset, name, value):

        sensecheck = SenseCheck.objects.get(code=value)
        if queryset.exists():
            return queryset.filter(reason=sensecheck)
        else:
            return queryset

    def filter_view_items(self, queryset, name, value):
        try:
            agreement_type, value = value.split("__")
            if queryset.count() == 0:
                return queryset.none()
            if queryset.exists():
                year = queryset.first().cycle.year
                queryset = filter_project_selection(
                    queryset, year, agreement_type, value
                )
                return queryset
            else:
                return queryset
        except:
            return queryset.none()


class OIOSBusinessUnitFilter(FilterSet):

    business_unit = CharFilter(field_name="business_unit__code", lookup_expr="exact")
    year = CharFilter(field_name="report_date", lookup_expr="contains")

    class Meta:
        models = OIOSBusinessUnitReport
        fields = [
            "business_unit",
            "year",
        ]


class SelectionSettingsFilter(FilterSet):

    year = CharFilter(field_name="cycle__year", lookup_expr="contains")

    class Meta:
        models = SelectionSettings
        fields = [
            "year",
        ]
