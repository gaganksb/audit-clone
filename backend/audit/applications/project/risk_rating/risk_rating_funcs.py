get_cash_test = """--cash_test
create or replace function get_cash_test(_agreement_id int)
	returns table(
		bu_number TEXT,
		agreement_id int,
		year int,
		procurement numeric,
		staff_cost numeric,
		cash numeric,
		other_accounts numeric 
)
as $$
begin
return query select concat(cbu.code, pa.number) as bu_number,
			   pa.id as agreement_id,
			   pc.year,
			   sum(case when pab.account_type = 'procurement' then pab.budget  else 0 end) as procurement,
			   sum(case when pab.account_type = 'staff_cost' then pab.budget  else 0 end) as staff_cost,
			   sum(case when pab.account_type = 'cash' then pab.budget  else 0 end) as cash,
			   sum(case when pab.account_type = 'other_accounts' then pab.budget  else 0 end) as other_accounts
from project_agreementbudget pab
inner join project_agreement as pa on pa.id = pab.agreement_id
inner join common_businessunit as cbu on cbu.id = pa.business_unit_id
inner join project_cycle as pc on pc.id = pa.cycle_id
where pa.id = _agreement_id
group by bu_number, pa.id, pc.year;
end;$$
LANGUAGE 'plpgsql';
"""


emergency_goal_test = """--emergency_goal_test
create or replace function emergency_goal_test(_agreement_id int)
	returns table(
		agreement_id INT,
		goal INT
)
as $$
begin
return query select p_project_goal.agreement_id,
	case when p_project_goal.goal = 1 then 1 when p_project_goal.goal = 2 then 2 else 0 end as goal 
from (
	select distinct(id) as agreement_id,
		   case
		   	   when 'PB' in (
			   	   select pg.code
				   from project_agreement_goals pag
				   left join project_goal as pg on pag.goal_id = pg.id
				   where pag.agreement_id = pa.id
				) and (
				   select count(pg.code)
				   from project_agreement_goals pag
				   left join project_goal as pg on pag.goal_id = pg.id
				   where pag.agreement_id = pa.id) >= 2
			   then 2
			   when exists(
			   	   select pg.code
				   from project_agreement_goals pag
				   left join project_goal as pg on pag.goal_id = pg.id
				   where pag.agreement_id = pa.id and pg.code = 'PB'
			   ) then 1
			   else 0
		   end as goal
	from project_agreement pa
	where pa.id = _agreement_id
) as p_project_goal;
end;$$
LANGUAGE 'plpgsql';
"""


modified_in_last_4_years = """
--modified_in_last_4_years
create or replace function modified_in_last_4_years(_agreement_id int)
	returns table(
		agreement_id INT,
		modified bool
)
as $$
begin
return query select
	pa.id,
	case
		when pa_cbu.code like '%UNHCR%' or pa_cbu.code = '%JORMN%' then exists(
		select
			pm.id
		from
			partner_modified pm
		inner join common_businessunit as cbu on
			cbu.id = pa.business_unit_id
		where
			partner_id = pa.partner_id
			and (
			select
				position(cbu.code in pa_cbu.code)) != 0
			and pm.modified = true
			and pm.year >= (
			select
				to_number(to_char(current_date - interval '6 year', 'YYYY'), '9999') ) )
		else exists(
		select
			pm.id
		from
			partner_modified pm
		inner join common_businessunit as cbu on
			cbu.id = pa.business_unit_id
		where
			partner_id = pa.partner_id
			and business_unit_id = pa.business_unit_id
			and pm.modified = true
			and pm.year >= (
			select
				to_number(to_char(current_date - interval '6 year', 'YYYY'), '9999') ) ) end as modified
	from
		project_agreement pa
	inner join common_businessunit as pa_cbu on
		pa_cbu.id = pa.business_unit_id
	where
		pa.id=_agreement_id;
end;$$
LANGUAGE 'plpgsql';
"""


latest_icq = """
--latest_icq
create or replace function latest_icq(_agreement_id int, _year int)
	returns table(
		agreement_id INT,
		overall_score_perc numeric
)
as $$
begin
return query  select
	_agreement_id as agreement_id,
		icq.overall_score_perc
	from
		icq_icqreport as icq
	inner join common_businessunit as cbu on
		cbu.id = icq.business_unit_id
	where
		case 
			when cbu.code like '%UNHCR%' or cbu.code like '%JORMN%' then 
				(select position(cbu.code in (select code from project_agreement pa inner join common_businessunit as cbu on cbu.id = pa.business_unit_id where pa.id = _agreement_id)) != 0)
			else 
				(icq.business_unit_id = ( select business_unit_id from project_agreement pa where id = _agreement_id))
		end
		and icq.partner_id = (select partner_id from project_agreement pa where id = _agreement_id)
		and icq.year >= (select 2020 - 4)
	order by
		icq.year desc
	limit 1;

IF NOT FOUND then
   return query select
		_agreement_id as agreement_id,
		0.0 as overall_score_perc
	from
		icq_icqreport as icq
	limit 1;
END IF;
end;$$
LANGUAGE 'plpgsql';
"""


get_partner_score = """
--get_partner_score
CREATE OR REPLACE function get_partner_score(_agreement_id INT) 
	RETURNS table(
		agreement_id INT,
		value INT
)
AS $$
begin
return QUERY select
	_agreement_id as project_agreement,
	(select COALESCE((select
		fs.value
	from
		project_agreement pa
	left join field_fieldassessment as fa on
		fa.id = pa.field_assessment_id
	inner join field_score as fs on
		fs.id = fa.score_id
	where
		pa.id = _agreement_id), 5))
from
	project_agreement pa limit 1;
end;$$
LANGUAGE 'plpgsql';
"""


get_oios_bu_report = """
--oios_business_unit_report
create or replace function get_oios_bu_report(_start_date varchar, _end_date varchar, _agreement_id int)
	returns table(
		agreement_id int,
		oios_rating numeric
)
as $$
begin
return query select _agreement_id AS agreement_id, 
   (select Coalesce(
   	(select
		oios.overall_rating AS oios_rating 
		from
			project_oiosbusinessunitreport as oios
		where
			oios.report_date BETWEEN _start_date::date AND _end_date::date
			and oios.business_unit_id = (
		select
		case
			when code like '%JORMN%' then (
			select
				id
			from
				common_businessunit cb
			where
				cb.code = 'JORMN')
			when code like '%UNHCR%' then (
			select
				id
			from
				common_businessunit cb
			where
				cb.code = 'UNHCR')
			else business_unit_id end
		from
			project_agreement pa
		left join common_businessunit cbu on
			cbu.id = pa.business_unit_id
		where
			pa.id = _agreement_id limit 1)), 0));
end;$$
LANGUAGE 'plpgsql';
"""


pqp_status = """
--pqp_status
create or replace function pqp_status(_agreement_id int)
	returns table(
		agreement_id INT,
		pqp_worldwide Bool
)
as $$
begin
return query select _agreement_id, (select coalesce((select pqp.status as pqp_worldwide from partner_pqpstatusnew as pqp where pqp.agreement_id = _agreement_id limit 1), false)) from project_agreement pa limit 1;
end;$$
LANGUAGE 'plpgsql';
"""


get_settings = """
create or replace function settings(_year INT)
	returns table(
		procurement_perc numeric,
		cash_perc numeric,
	    staff_cost_perc numeric,
	    emergency_perc numeric,
	    audit_perc numeric,
	    field_assessment_perc numeric,
	    oios_perc numeric
)
as $$
begin
return query select ps.procurement_perc,
	   ps.cash_perc,
	   ps.staff_cost_perc,
	   ps.emergency_perc,
	   ps.audit_perc,
	   ps.field_assessment_perc,
	   ps.oios_perc
from project_selectionsettings ps
inner join project_cycle as pc on ps.cycle_id = pc.id
where pc.budget_ref = _year;
end;$$
LANGUAGE 'plpgsql';
"""


risk_rating_query = """
select (project_profile + oios_rating * 0.2 + partner_score * 0.25) as risk_rating from (
	select oios_rating,
		   partner_score,
		   (
				procurement_test * (select procurement_perc from settings(%(__year__)s)) +
				cash_test * (select cash_perc from settings(%(__year__)s)) +
				staff_cost_test * (select staff_cost_perc from settings(%(__year__)s)) +
				emergency_test * (select emergency_perc from settings(%(__year__)s)) + 
				audit_test * (select audit_perc from settings(%(__year__)s)) 
			) as project_profile
	from (
		select pa.id,
			   cash_test.procurement,
			   cash_test.cash,
			   cash_test.staff_cost,
			   cash_test.other_accounts,
			   goal.goal,
			   modified.modified,
			   icq.overall_score_perc,
			   partner_score.value as partner_score,
			   pqp.pqp_worldwide,
			   bu_report.oios_rating,
			   case when pqp_worldwide = true then
			   		case
				   		when cash_test.procurement > 150000 then 4
				   		when cash_test.procurement between 100000 and 150000 then 3
				   		when cash_test.procurement between 50000 and 100000 then 2
				   		when cash_test.procurement between 20000 and 50000 then 1
				   		when cash_test.procurement < 20000 then 0
				   	end
			   else
				   	case
				   		when cash_test.procurement > 150000 then 5
				   		when cash_test.procurement between 100000 and 150000 then 4
				   		when cash_test.procurement between 50000 and 100000 then 3
				   		when cash_test.procurement between 20000 and 50000 then 2
				   		when cash_test.procurement < 20000 then 1
					end
		       end as procurement_test,
		       case
		   	   		when cash_test.cash > 150000 then 5
			   		when cash_test.cash between 100000 and 150000 then 4
			   		when cash_test.cash between 50000 and 100000 then 3
			   		when cash_test.cash between 20000 and 50000 then 2
			   		when cash_test.cash < 20000 then 1
		       end as cash_test,
		   	   case
		   	   		when cash_test.staff_cost > 150000 then 5
			   		when cash_test.staff_cost between 100000 and 150000 then 4
			   		when cash_test.staff_cost between 50000 and 100000 then 3
			   		when cash_test.staff_cost between 20000 and 50000 then 2
			   		when cash_test.staff_cost < 20000 then 1
		       end as staff_cost_test,
		       case
		   	   		when goal = 1 then 5
			   		when goal = 2 then 3
			   		else 0
		       end as emergency_test,
		       case
		   	   		when modified = true then 5
		   	   		when modified = false then
		   	   		case
		   	   			when icq.overall_score_perc = null or icq.overall_score_perc < 0 then 5
		   	   			when icq.overall_score_perc between 0.4 and 0.6 then 4
		   	   			when icq.overall_score_perc between 0.6 and 0.8 then 3
		   	   			when icq.overall_score_perc between 0.8 and 1 then 2
		   	   			else 5
		   	   		end
		       end as audit_test
		from project_agreement pa
		left join (select * from get_cash_test(%(__agreement_id__)s)) as cash_test on cash_test.agreement_id = pa.id
		left join (select * from emergency_goal_test(%(__agreement_id__)s)) as goal on goal.agreement_id = pa.id
		left join (select * from modified_in_last_4_years(%(__agreement_id__)s)) as modified on modified.agreement_id = pa.id
		left join (select * from latest_icq(%(__agreement_id__)s, %(__year__)s)) as icq on icq.agreement_id = pa.id
		left join (select * from get_partner_score(%(__agreement_id__)s)) as partner_score on partner_score.agreement_id = pa.id
		left join (select * from pqp_status(%(__agreement_id__)s)) as pqp on pqp.agreement_id = pa.id
		left join (select * from get_oios_bu_report('%(__year__)s-01-01', '%(__year__)s-12-31', %(__agreement_id__)s)) as bu_report on bu_report.agreement_id = pa.id
		where pa.id = %(__agreement_id__)s
	) as rr_table
) as risk_rating_table;
"""


RISK_RATING_FUNCS = [
    get_cash_test,
    emergency_goal_test,
    modified_in_last_4_years,
    latest_icq,
    get_partner_score,
    get_oios_bu_report,
    pqp_status,
    get_settings,
]
