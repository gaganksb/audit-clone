from __future__ import absolute_import
from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction
from notify.models import NotifiedUser
from dateutil.relativedelta import relativedelta
from notify.helpers import send_notification_to_users
from notify.helpers import send_notification
from django.utils import timezone
from project.models import Agreement
from account.models import User
from auditing.models import AuditMember


class Command(BaseCommand):
    help = "Send notifications to users with daily email preference"

    def add_arguments(self, parser):
        parser.add_argument("-ids", "--agreement_ids", type=str, help="agreement_ids")

    def handle(self, *args, **kwargs):
        pass
        # agreement_ids = kwargs.get('agreement_ids', None)

        # date = timezone.datetime.today() - relativedelta(days=1)
        # protocol = 'http' if settings.DEBUG else 'https'
        # link  = f'{protocol}://{settings.FRONTEND_HOST}'
        # subject = "Audit Firm Selection"

        # if agreement_ids is not None:
        #     agreement_ids = [int(i) for i in agreement_ids.split(" ") if i.isdigit() == True]
        #     agreement_qs = Agreement.objects.filter(id__in=agreement_ids)
        # else:
        #     agreement_qs = Agreement.objects.filter(last_modified_date__day=date.day,
        #                                             last_modified_date__year=date.year,
        #                                             last_modified_date__month=date.month)
        # audit_firm_ids = set(agreement_qs.values_list("audit_firm_id", flat=True))

        # for firm in audit_firm_ids:

        #     if agreement_ids is not None:
        #         agreeemnt_qs = Agreement.objects.filter(audit_firm_id=firm, id__in=agreement_ids).distinct("id")
        #     else:
        #         agreeemnt_qs = Agreement.objects.filter(audit_firm_id=firm,
        #                                 last_modified_date__day=date.day,
        #                                 last_modified_date__year=date.year,
        #                                 last_modified_date__month=date.month).distinct("id")

        #     audit_member_qs = AuditMember.objects.filter(
        #         audit_agency_id=firm,
        #         role__role="ADMIN"
        #     )
        #     for member in audit_member_qs:
        #         audit_firm = member.audit_agency.legal_name
        #         users = member.user
        #         projects = agreeemnt_qs
        #         send_notification(
        #             notification_type='audit_firm_selection',
        #             obj=agreeemnt_qs.first(),
        #             users=[users],
        #             context={
        #                 "audit_firm": audit_firm,
        #                 "projects": projects.values_list("title", "business_unit__name", "number"),
        #                 "subject": subject,
        #                 "link":link
        #             }
        #         )

        # if settings.ENV not in ['local',]:
        #     try:
        #         with transaction.atomic():
        #             user = NotifiedUser.objects.select_for_update().filter(
        #                 sent_as_email=False, notification__name=subject
        #             )
        #             send_notification_to_users(user)
        #     except Exception as e:
        #         print(str(e))
