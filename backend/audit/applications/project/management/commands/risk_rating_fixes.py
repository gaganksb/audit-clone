import os
import xlrd
from django.core.management.base import BaseCommand
from project.models import AgreementBudget
from common.risk_rating_fixes_import import (
    import_project_agreement_report,
    import_agreement_budget,
    goal_fixes,
    icq_report_patch,
    partner_modified_patch,
    oios_patch,
    pqp_fixes,
)


class Command(BaseCommand):
    help = "Import interim list from the excel file for 2020"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        AgreementBudget.objects.all().delete()
        import_project_agreement_report()
        import_agreement_budget()
        goal_fixes()
        icq_report_patch()
        partner_modified_patch()
        oios_patch()
        pqp_fixes()
