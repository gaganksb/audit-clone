from common.new_fakedata_util import (
    import_project_agreement_report,
    import_agreement_budget,
)
import os
import xlrd


class Command(BaseCommand):
    help = "Import interim list from the excel file for 2020"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        AgreementBudget.objects.all().delete()
        import_project_agreement_report()
        import_agreement_budget()
