from __future__ import absolute_import
from django.conf import settings
from django.core.management.base import BaseCommand
from common.consts import NOTIFICATION_FREQUENCY_CHOICES
from django.db import transaction
from notify.models import NotifiedUser
from dateutil.relativedelta import relativedelta
from notify.helpers import send_notification_to_users
from notify.helpers import send_notification
from django.utils import timezone
from project.models import Agreement, AgreementMember
from account.models import User


class Command(BaseCommand):
    help = "Send notifications to users with daily email preference"

    def add_arguments(self, parser):
        parser.add_argument(
            "-ids", "--agreement_member_ids", type=str, help="agreement_member_ids"
        )
        parser.add_argument("-id", "--agreement_ids", type=str, help="agreement_ids")

    def handle(self, *args, **kwargs):
        pass
        # agreement_member_ids = kwargs.get('agreement_member_ids', None)
        # agreement_ids = kwargs.get('agreement_ids', None)
        # agreement_ids = [int(i) for i in agreement_ids.split(" ") if i.isdigit() == True]

        # date = timezone.datetime.today() - relativedelta(days=1) # remove delta and do python manage.py {command}
        # protocol = 'http' if settings.DEBUG else 'https'
        # link = f'{protocol}://{settings.FRONTEND_HOST}'
        # subject = "Agreement Member Selection"

        # if agreement_member_ids is not None:
        #     agreement_member_ids = [int(i) for i in agreement_member_ids.split(" ") if i.isdigit() == True]
        #     agreement_member_qs = AgreementMember.objects.filter(id__in=agreement_member_ids)
        # else:
        #     agreement_member_qs = AgreementMember.objects.filter(created_date__day=date.day,
        #                                                         created_date__year=date.year,
        #                                                         created_date__month=date.month)

        # user_qs = set(agreement_member_qs.values_list('user__email', flat=True))
        # for user in user_qs:
        #     users = User.objects.filter(email=user)
        #     agreement_member = AgreementMember.objects.filter(user__email=user)
        #     if agreement_member.exists():
        #         if agreement_member_ids is not None:
        #             agreement = agreement_member[0].focal_points.filter(id__in=agreement_ids)
        #         else:
        #             agreement = agreement_member[0].focal_points.filter(last_modified_date__day=date.day,
        #                                                                 last_modified_date__year=date.year,
        #                                                                 last_modified_date__month=date.month)
        #         if agreement.exists():
        #             focals = agreement.filter(focal_points__focal=True, focal_points__user__email=user)
        #             alt_focals = agreement.filter(focal_points__alternate_focal=True, focal_points__user__email=user)
        #             reviewers = agreement.filter(focal_points__reviewer=True, focal_points__user__email=user)
        #             if focals.exists():
        #                 send_notification(
        #                     notification_type='agreement_member_selection',
        #                     obj=agreement.first(),
        #                     users=users,
        #                     context={
        #                         "agreement_member_role": "Focal Person",
        #                         "projects": focals.values_list("title", "business_unit__name", "number"),
        #                         "fullname": users.first().fullname,
        #                         "subject": subject,
        #                         "link":link
        #                     }
        #                 )
        #             elif alt_focals.exists():
        #                 send_notification(
        #                     notification_type='agreement_member_selection',
        #                     obj=agreement.first(),
        #                     users=users,
        #                     context={
        #                         "agreement_member_role": "Alternate Focal Person",
        #                         "projects": alt_focals.values_list("title", "business_unit__name", "number"),
        #                         "fullname": users.first().fullname,
        #                         "subject": subject,
        #                         "link":link
        #                     }
        #                 )
        #             elif reviewers.exists():
        #                 send_notification(
        #                     notification_type='agreement_member_selection',
        #                     obj=agreement.first(),
        #                     users=users,
        #                     context={
        #                         "agreement_member_role": "Reviewer",
        #                         "projects": reviewers.values_list("title", "business_unit__name", "number"),
        #                         "fullname": users.first().fullname,
        #                         "subject": subject,
        #                         "link":link
        #                     }
        #                 )

        # if settings.ENV not in ['local',]:
        #     try:
        #         with transaction.atomic():
        #             user = NotifiedUser.objects.select_for_update().filter(
        #                 sent_as_email=False, notification__name=subject
        #             )
        #             send_notification_to_users(user)
        #     except Exception as e:
        #         print(str(e))
