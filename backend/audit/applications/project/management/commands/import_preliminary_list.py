from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.management import call_command

import os
import xlrd
from django.db.models import F, Q
from common.consts import INT01, PA_MANUALLY_EXCLUDED
from partner.models import *
from project.models import *
from icq.models import *
from field.models import *


update_agreements = []


class ProjectSelectionImport:
    def __init__(self):

        self.file_name = (
            "2020_Modulation-Similation_October 2020_Final - 5 Nov - for UNICC.xlsx"
        )
        self.file = os.path.join(
            "/code", "audit", "applications", "project", "excel", self.file_name
        )
        self.pqp_sheet = "All Test - at 2.6 and $>150K "
        self.pre_list = "Preliminary List 26 Oct 2020"
        self.risk_rating_sheet = "All Test - at 2.6 and $>150K "
        self.new_partner_sheet = "New partner in 2020"
        self.field_assessment_sheet = "Field IP Performance Assessment"
        self.partner_modified_sheet = "Modified 2016 to 2019"
        self.oios_sheet = "OIOS Score - Oct 2020"
        self.icq_sheet = "All Test - at 2.6 and $>150K "
        self.pre_list_sheet = "Preliminary List - 11 Nov "
        self.pre_file_name = "2020 Project Audit - Preliminary List - 11 Nov 2020.xlsx"
        self.pre_file_path = os.path.join(
            "/code", "audit", "applications", "project", "excel", self.pre_file_name
        )
        self.unhcr_code = {
            "Div Int Protection Glo": "DIP-GLO",
            "DIP Resettlement": "DIP RE",
            "PSP Global Ops": "PSP GO",
            "Div External Relations Glo": "DERS-GLO",
            "Executive Office Global OPs": "EXOFF-GO",
            "DRS TS": "DRS TS",
            "Div Human Resources GLO": "DHRM-GLO",
            "Financial Resources Service": "FRS-GO",
            "DRS GLO": "DRSGO",
            "Other Operations in Europe": "OOE",
            "CGSC DPSM": "CGSC DPSM",
            "Regional Activities Europe": "REU",
            "Other Operations In MENA": "OOM",
        }

        self.workbook = xlrd.open_workbook(self.file)
        self.pre_workbook = xlrd.open_workbook(self.pre_file_path)

    def import_data(self):
        # self.import_field_assessment()
        self.import_pre_list()

    def import_pre_list(self):
        print("Importing Pre List")
        sheet = self.pre_workbook.sheet_by_name(self.pre_list_sheet)
        update_agreements = []
        for i in range(6, sheet.nrows):
            try:
                business_unit = sheet.cell_value(i, 2)
                number = int(sheet.cell_value(i, 9))
                sense_check = sheet.cell_value(i, 14)
                document_location = sheet.cell_value(i, 15)
                site_location = sheet.cell_value(i, 16)
                audited_by_gov = sheet.cell_value(i, 17)
                tree_node = sheet.cell_value(i, 18)

                if business_unit == "UNHCR":
                    tree_node = self.unhcr_code[tree_node]
                    business_unit_node = "{0} HQ - {1}".format(business_unit, tree_node)
                    agreement = Agreement.objects.filter(
                        business_unit__code__contains=business_unit,
                        number=number,
                    ).first()
                elif business_unit == "JORMN":
                    tree_node = self.unhcr_code[tree_node]
                    agreement = Agreement.objects.filter(
                        business_unit__code__contains=business_unit,
                        number=number,
                    ).first()
                else:
                    agreement = Agreement.objects.filter(
                        business_unit__code=business_unit, number=number
                    ).first()

                if agreement is not None:
                    code = "SC{}".format(SenseCheck.objects.all().count() + 1)
                    sense_check_obj, created = SenseCheck.objects.get_or_create(
                        description=sense_check
                    )
                    if created:
                        sense_check_obj.code = code
                        sense_check_obj.save()

                    agreement.pre_sense_check_id = sense_check_obj.id
                    update_agreements.append(agreement.id)
                    document_location_obj, created = Location.objects.get_or_create(
                        location_type="document", city=document_location
                    )
                    site_location_obj, created = Location.objects.get_or_create(
                        location_type="site", city=site_location
                    )
                    agreement.locations.add(document_location_obj)
                    agreement.locations.add(site_location_obj)
                    agreement.audited_by_gov = False if audited_by_gov == "No" else True
                    agreement.save()
                else:
                    print(
                        dict(
                            business_unit=business_unit,
                            number=number,
                            sense_check=sense_check,
                            document_location=document_location,
                            site_location=site_location,
                            audited_by_gov=audited_by_gov,
                            tree_node=tree_node,
                        )
                    )
                    print("Fetch Data from Antirio")
            except Exception as e:
                print("Error in import_pre_list", e)

        cycle = Cycle.objects.filter(year=2020).first()
        cycle.status_id = INT01
        cycle.save()
        aggr_updated = Agreement.objects.filter(cycle__year=2020).update(
            status_id=INT01, int_sense_check_id=F("pre_sense_check_id")
        )
        progress_updated = FieldAssessmentProgress.objects.all().update(
            pending_with="COMPLETED"
        )
        manualy_excluded_sense_check = SenseCheck.objects.filter(
            code=PA_MANUALLY_EXCLUDED
        ).first()
        Agreement.objects.filter(~Q(id__in=update_agreements)).update(
            int_sense_check_id=manualy_excluded_sense_check,
            pre_sense_check_id=manualy_excluded_sense_check,
        )

    def import_risk_rating(self):
        assessments = self.import_field_assessment()
        sheet = self.workbook.sheet_by_name(self.risk_rating_sheet)

        for i in range(3, sheet.nrows):
            try:
                business_unit = sheet.cell_value(i, 2)
                tree_node = sheet.cell_value(i, 6)
                aggr_number = sheet.cell_value(i, 11)
                aggr_number = (
                    int(aggr_number) if type(aggr_number) == float else aggr_number
                )
                risk_rating = round(sheet.cell_value(i, 41), 2)

                if business_unit == "UNHCR":
                    tree_node = self.unhcr_code[tree_node]
                    business_unit_node = "{0} HQ - {1}".format(business_unit, tree_node)
                    agreement = Agreement.objects.filter(
                        business_unit__code=business_unit_node,
                        number=aggr_number,
                    ).first()
                elif business_unit == "JORMN":
                    tree_node = self.unhcr_code[tree_node]
                    agreement = Agreement.objects.filter(
                        business_unit__code=business_unit,
                        business_unit__name=tree_node,
                        number=number,
                    ).first()
                else:
                    agreement = Agreement.objects.filter(
                        business_unit__code=business_unit, number=aggr_number
                    ).first()

                if agreement is not None:
                    field_assessent = FieldAssessment.objects.get(
                        id=agreement.field_assessment.id
                    )
                    score = assessments.get("{0}{1}".format(business_unit, aggr_number))
                    score_obj = Score.objects.filter(value=score).first()
                    field_assessent.score = score_obj
                    field_assessent.save()
                    agreement.risk_rating = risk_rating
                    agreement.save()
                else:
                    print("Fetch Data from Antirio")
            except Exception as e:
                print("Error ", e)

    def import_field_assessment(self):
        print("Importing OIOS")

        sheet = self.workbook.sheet_by_name(self.field_assessment_sheet)
        assessments = {}
        for i in range(2, sheet.nrows):
            try:
                bu = sheet.cell_value(i, 0)
                assessmet_score = int(sheet.cell_value(i, 1))
                assessments[bu] = assessmet_score
            except Exception as e:
                print(e)
        return assessments


class Command(BaseCommand):
    help = "Import Preliminary List."

    def handle(self, *args, **options):
        import_project_selection = ProjectSelectionImport()
        import_project_selection.import_data()
        self.stdout.write("Done.")
