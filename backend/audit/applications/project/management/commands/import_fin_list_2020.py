import os
import xlrd
from account.models import User
from django.db.models import F
from project.models import Agreement, Cycle, Location, SenseCheck
from django.core.management.base import BaseCommand, CommandError
from common.models import Country
from common.helpers import parse_location, get_or_create_aggr_member
from common.consts import FIN01, FIN02, FIN03

YEAR = 2020


def add_focal_point(agreement, data):
    unhcr_audit_focal_point = data.get("unhcr_audit_focal_point", None)
    unhcr_audit_alt_focal_point = data.get("unhcr_audit_alt_focal_point", None)
    unhcr_partner_focal_point = data.get("unhcr_partner_focal_point", None)
    unhcr_partner_alt_focal_point = data.get("unhcr_partner_alt_focal_point", None)

    aggr_member = None
    if unhcr_audit_focal_point is not None:
        user, _ = User.objects.get_or_create(email=unhcr_audit_focal_point)
        if user.fullname is None:
            user.fullname = unhcr_audit_focal_point
            user.save()
        aggr_member = get_or_create_aggr_member(user, "unhcr", "focal")
    elif unhcr_audit_alt_focal_point is not None:
        user, _ = User.objects.get_or_create(
            email=unhcr_audit_alt_focal_point, fullname=unhcr_audit_alt_focal_point
        )
        if user.fullname is None:
            user.fullname = unhcr_audit_alt_focal_point
            user.save()
        aggr_member = get_or_create_aggr_member(user, "unhcr", "alternate_focal")
    elif unhcr_partner_focal_point is not None:
        user, _ = User.objects.get_or_create(
            email=unhcr_partner_focal_point, fullname=unhcr_partner_focal_point
        )
        if user.fullname is None:
            user.fullname = unhcr_partner_focal_point
            user.save()
        aggr_member = get_or_create_aggr_member(user, "partner", "focal")
    elif unhcr_partner_alt_focal_point is not None:
        user, _ = User.objects.get_or_create(
            email=unhcr_partner_alt_focal_point, fullname=unhcr_partner_alt_focal_point
        )
        if user.fullname is None:
            user.fullname = unhcr_partner_alt_focal_point
            user.save()
        aggr_member = get_or_create_aggr_member(user, "partner", "alternate_focal")

    if aggr_member is not None:
        agreement.focal_points.add(aggr_member)
        agreement.save()
    return agreement


class Command(BaseCommand):
    help = "Import interim list from the excel file for 2020"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        file_name = "2020 Project Audit - Final List of Projects Selected for Audit - PAs as of 15 Jan 2021.xlsx"
        file_path = os.path.join(
            os.getcwd(), "audit", "applications", "project", "excel", file_name
        )
        wb = xlrd.open_workbook(file_path)
        sheet = wb.sheet_by_name("Audit Final List - 15 Jan 2021")

        agreements = Agreement.objects.filter(cycle__year=2020)
        PA_MANUALLY_EXCLUDED, PA_MANUALLY_INCLUDED = 12, 13
        final_list = {}

        for i in range(5, sheet.nrows):
            ppa = sheet.cell_value(i, 0)
            document = sheet.cell_value(i, 17)
            site = sheet.cell_value(i, 18)
            unhcr_audit_focal_point = sheet.cell_value(i, 19)
            unhcr_audit_alt_focal_point = sheet.cell_value(i, 20)
            unhcr_partner_focal_point = sheet.cell_value(i, 21)
            unhcr_partner_alt_focal_point = sheet.cell_value(i, 22)
            sense_check = sheet.cell_value(i, 24)

            docs = document.split(",")
            sites = site.split(",")
            if len(docs) % 2 == 0:
                doc_locations = parse_location(docs)
            elif len(docs) == 1:
                doc_locations = [{"city": None, "country": docs[0]}]
            else:
                doc_locations = []

            if len(sites) % 2 == 0:
                site_locations = parse_location(sites)
            elif len(sites) == 1:
                site_locations = [{"city": None, "country": sites[0]}]
            else:
                site_locations = []

            final_list[ppa] = {
                "document": doc_locations,
                "site": site_locations,
                "status": False,
                "sense_check": sense_check,
                "unhcr_audit_focal_point": unhcr_audit_focal_point,
                "unhcr_audit_alt_focal_point": unhcr_audit_alt_focal_point,
                "unhcr_partner_focal_point": unhcr_partner_focal_point,
                "unhcr_partner_alt_focal_point": unhcr_partner_alt_focal_point,
            }

        for agreement in agreements:
            if "JORMN" in agreement.business_unit.code:
                ppa = "{}{}".format("JORMN", agreement.number)
            elif "UNHCR" in agreement.business_unit.code:
                ppa = "{}{}".format("UNHCR", agreement.number)
            else:
                ppa = "{}{}".format(agreement.business_unit.code, agreement.number)
            res = final_list.get(ppa, None)

            if res is not None:
                sense_check = SenseCheck.objects.filter(
                    description=res["sense_check"]
                ).first()
                if sense_check is None:
                    code = "SC{}".format(SenseCheck.objects.all().count() + 1)
                    sense_check, created = SenseCheck.objects.get_or_create(
                        description=res["sense_check"]
                    )
                    if created:
                        sense_check.code = code
                        sense_check.save()

                agreement.fin_sense_check = sense_check

                for doc in res["document"]:
                    try:
                        doc_country = doc.get("country", "")
                        if doc_country != "":
                            country_obj = Country.objects.filter(
                                name=doc_country.strip()
                            ).first()
                            doc_location = Location.objects.filter(
                                location_type="document",
                                is_valid=True,
                                city=doc.get("city"),
                                country=country_obj,
                            ).first()
                            if doc_location is not None:
                                agreement.locations.add(doc_location)
                            else:
                                doc_location_obj = Location.objects.create(
                                    location_type="document",
                                    is_valid=True,
                                    city=doc.get("city"),
                                    country=country_obj,
                                )
                                agreement.locations.add(doc_location_obj)
                    except Exception as e:
                        print("Error", e, doc)

                for site in res["site"]:
                    try:
                        site_country = site.get("country", "")
                        if site_country != "":
                            country_obj = Country.objects.filter(
                                name=site_country.strip()
                            ).first()
                            site_location = Location.objects.filter(
                                location_type="site",
                                is_valid=True,
                                city=site.get("city"),
                                country=country_obj,
                            ).first()
                            if site_location is not None:
                                agreement.locations.add(site_location)
                            else:
                                site_location_obj = Location.objects.create(
                                    location_type="site",
                                    is_valid=True,
                                    city=site.get("city"),
                                    country=country_obj,
                                )
                                agreement.locations.add(site_location_obj)
                    except Exception as e:
                        print("Error", e, site)
                agreement = add_focal_point(agreement, res)
                agreement.save()

        Agreement.objects.filter(cycle__year=2020).update(status_id=FIN03)
        cycle = Cycle.objects.filter(year=2020).first()
        cycle.status_id = FIN03
        cycle.save()

        print("Final list 2020 imported")
