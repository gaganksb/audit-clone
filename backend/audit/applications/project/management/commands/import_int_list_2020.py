import os
import xlrd
from django.db.models import F
from project.models import Agreement, Cycle, Location
from django.core.management.base import BaseCommand, CommandError


FIN01 = 6
YEAR = 2020


class Command(BaseCommand):
    help = "Import interim list from the excel file for 2020"

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        file_name = "2020 Project Audit Interim List - 30 November 2020.xlsx"
        file_path = os.path.join(
            os.getcwd(), "audit", "applications", "project", "excel", file_name
        )
        wb = xlrd.open_workbook(file_path)
        sheet = wb.sheet_by_name("Interim List - 26 Nov")

        agreements = Agreement.objects.filter(cycle__year=2020)
        PA_MANUALLY_EXCLUDED, PA_MANUALLY_INCLUDED = 12, 13
        interim_list = {}

        for i in range(6, sheet.nrows):
            ppa = sheet.cell_value(i, 0)
            document = sheet.cell_value(i, 14)
            site = sheet.cell_value(i, 15)
            is_gov = sheet.cell_value(i, 16)
            is_gov = True if is_gov == "Yes" else False
            interim_list[ppa] = {
                "document": document,
                "site": site,
                "is_gov": is_gov,
                "status": False,
            }

        for agreement in agreements:
            if "JORMN" in agreement.business_unit.code:
                ppa = "{}{}".format("JORMN", agreement.number)
            elif "UNHCR" in agreement.business_unit.code:
                ppa = "{}{}".format("UNHCR", agreement.number)
            else:
                ppa = "{}{}".format(agreement.business_unit.code, agreement.number)
            res = interim_list.get(ppa, None)
            if res is None:
                int_inc = False
            else:
                interim_list[ppa]["status"] = True
                int_inc = True

            if agreement.pre_sense_check_id == PA_MANUALLY_EXCLUDED:
                pre_inc = False
            else:
                pre_inc = True

            if pre_inc == True and int_inc == True:
                agreement.int_sense_check_id = agreement.pre_sense_check_id
            elif pre_inc == False and int_inc == False:
                agreement.int_sense_check_id = agreement.pre_sense_check_id
            elif pre_inc == False and int_inc == True:
                agreement.int_sense_check_id = PA_MANUALLY_INCLUDED
            elif pre_inc == True and int_inc == False:
                agreement.int_sense_check_id = PA_MANUALLY_EXCLUDED

            # Adding Location
            if res is not None:
                site_city = res["site"]
                document_city = res["document"]

                if len(site_city) >= 2:
                    site_location = Location.objects.filter(
                        location_type="site", city=site_city, address__isnull=True
                    ).first()
                    if site_location is not None:
                        agreement.locations.add(site_location)
                    else:
                        Location.objects.create(location_type="site", city=site_city)

                if len(site_city) >= 2:
                    doc_location = Location.objects.filter(
                        location_type="document",
                        city=document_city,
                        address__isnull=True,
                    ).first()
                    if doc_location is not None:
                        agreement.locations.add(doc_location)
                    else:
                        Location.objects.create(
                            location_type="document", city=document_city
                        )
            agreement.save()

        agreements.update(status_id=FIN01, fin_sense_check_id=F("int_sense_check_id"))
        cycle = Cycle.objects.filter(year=2020).first()
        cycle.status_id = FIN01
        cycle.save()

        print("Interim list 2020 imported")
