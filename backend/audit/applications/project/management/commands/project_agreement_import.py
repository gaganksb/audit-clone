import os
import re
import xlrd
import logging
from django.db.models import Sum
from field.models import (
    FieldOffice,
    Score,
    FieldAssessment,
    FieldAssessmentProgress,
)

from common.models import CostCenter, BusinessUnit, Comment, Region, Country, CommonFile
from common.consts import PRE03, INT02, FIN03
from partner.models import (
    ImplementerType,
    Partner,
    Modified,
    PQPStatusNew,
    AdverseDisclaimer,
)
from icq.models import ICQReport
from project.models import (
    Pillar,
    Vendor,
    Operation,
    Goal,
    PopulationPlanningGroup,
    Situation,
    AgreementType,
    Cycle,
    Agreement,
    AgreementReport,
    Output,
    AgreementBudget,
    Account,
    Currency,
    OIOSBusinessUnitReport,
    SelectionSettings,
    PreviousAudit,
    CancelledAudit,
    SenseCheck,
)
from auditing.models import AuditWorksplitGroup, AuditAgency
from account.models import User
from unhcr.models import *
from field.tasks import update_risk_rating
from common.helpers import get_or_create_aggr_member

logger = logging.getLogger("console")
from datetime import datetime

from unhcr.models import UNHCRMember
from django.core.management.base import BaseCommand
from django.core.files import File
from project.helpers import update_project_selection_reason as update_reason

ACCOUNT_TYPE_CHOICES = {
    "other_accounts": "Other accounts",
    "procurement": "Procurement",
    "staff_cost": "Staff Costs",
    "cash": "cash",
    "no_budget": "No budget yet in HNIP",
}


def convert_to_string(data):
    try:
        if type(data) == float:
            return str(int(data))
        elif type(data) == int:
            return str(data)
        elif type(data) == str and "." in data:
            return str(data.split(".")[0])
        elif type(data) == str and "," in data and data.rstrip().endswith(","):
            return str(data.split(",")[0])
        else:
            return str(data)
    except Exception as e:
        print("Error in convert_to_string ", e, data)


def validate_data_type(data):
    data_type_mapping = {
        "Situation": str,
        "Site": str,
        "Budget(USD)": float,
        "To": str,
        "Output": str,
        "Cost Centre": str,
        "Goal": str,
        "PPG": str,
        "Account": str,
        "Parent Node Name": str,
        "Account Type": str,
        "PPA CODE": str,
        "Partner CODE": str,
        "Partner Type": str,
        "Count of PPAs (Duplicate & Closable LOMI Removed)": int,
        "Business Unit": str,
        "Description": str,
        "IP Agreement Number": str,
        "Agreement Date": str,
        "Agreement Type": str,
        "DESCR100": str,
        "Start Node": str,
        "Descr": str,
        "Node": str,
        "Country / HQ Node": str,
        "Description1": str,
        "Operation": str,
        "Pillar": str,
        "HCR_PILLAR_DESC": str,
        "Budget Year": int,
        "Bgt Yr": int,
        "Cost Centre(s)": str,
        "Goal(s)": str,
        "Planning Grp(s)": str,
        "Situation(s)": str,
        "Implementer": str,
        "HCR_DESCR": str,
        "Implementer Type": str,
        "Start Date": str,
        "Completion Date": str,
        "Budget (Agreement)": float,
        "Currency": str,
        "Installments Paid to IP": float,
        "Adjustments for IP": float,
        "VENDOR_ID": str,
        "HCR_IP_CANCEL_AMT": str,
        "HCR_IP_IPFR_AMT": str,
        "HCR_IP_REFUND_AMT": str,
        "HCR_IP_RECV_AMT": str,
        "HCR_IP_WRITOFF_AMT": str,
    }
    str_float_fields = [
        "IP Agreement Number",
        "Implementer",
        "Situation(s)",
        "Account",
        "Cost Centre",
        "Situation",
        "Pillar",
        "Cost Centre(s)",
    ]
    for i in data_type_mapping:
        if i in data:
            if data_type_mapping[i] != type(data[i]):
                if "date" in i.lower():
                    new_date = parse_excel_date(data[i])
                    data.update({"{}".format(i): new_date})
                elif i in str_float_fields:
                    new_str = convert_to_string(data[i])
                    data.update({"{}".format(i): new_str})
                elif i == "Budget Year" or i == "Bgt Yr":
                    data.update({"{}".format(i): int(data[i])})
    return data


def parse_excel_date(xl_date):
    try:
        xl_date = int(xl_date)
        if isinstance(xl_date, (int, float)):
            datetime_date = xlrd.xldate_as_datetime(xl_date, 0)
            date_object = datetime_date.date()
            string_date = date_object.isoformat()
            return string_date
        else:
            return xl_date
    except:
        return xl_date


def check_if_number(data):
    try:
        return str(data).isdigit()
    except:
        try:
            d = str(data).replace(".", "0")
            return d.isdigit()
        except:
            return False
            print("Error in check_if_number ")


class ImportAgreements:

    HNIP999_sheet = "HNIP999"
    HNIP998_sheet = "HNIP998"
    main_sheet = "main"
    import_delta = None
    delta_projects = []

    def __init__(self, file_id):
        self.file_id = file_id
        file_obj = CommonFile.objects.filter(id=file_id).first()
        self.wb = xlrd.open_workbook(file_contents=file_obj.file_field.read())
        print("Wordbook loaded")

    def util_decimal(self, item):
        if type(item) is str:
            _ = item.strip()
            if _ in [""]:
                return 0
            else:
                return float(_)
        return item

    def create_country(self, item):
        try:
            region, created = Region.objects.get_or_create(code=item["Node"])
            if created:
                region.name = item["Description"]
                region.save()

            country, created = Country.objects.get_or_create(
                code=item["Country / HQ Node"]
            )
            if created:
                country.name = item["Description1"]
                country.region = region
                country.save()
            return country
        except:
            return None

    def parse_HNIP999(self, items):

        agreements = []
        for item in items:
            if item["Business Unit"] != "" and item["IP Agreement Number"] != "":
                agreements.append(
                    [item["Business Unit"], int(item["IP Agreement Number"])]
                )
        return agreements

    def filter_HNIP998(self, items, agreements=None):
        if agreements is None:
            res = items
        else:
            res = []
            for item in items:
                if [
                    item["Business Unit"],
                    int(item["IP Agreement Number"]),
                ] in agreements:
                    res.append(item)
        return res

    def is_None(self, value):
        if isinstance(value, int):
            return False
        elif isinstance(value, float):
            return False
        elif value.strip() not in [
            "",
        ]:
            return False
        return True

    def import_outputs(self, items):
        outputs = []
        errors = []
        for item in items:
            try:
                if item["Output"] not in outputs:
                    outputs.append(item["Output"])
                    if item["Output"].strip() not in [
                        "",
                    ]:
                        Output.objects.get_or_create(code=item["Output"])
            except Exception as e:
                logger.error(str(e))
                errors.append(item)
        return errors

    def import_agreement_budgets(self, items):
        errors = []
        for item in items:
            try:
                business_unit = BusinessUnit.objects.get(code=item["Business Unit"])
                number = int(item["IP Agreement Number"])
                agreement = Agreement.objects.filter(
                    business_unit=business_unit,
                    number=number,
                    cycle__year=item["Bgt Yr"],
                ).first()
                account = None
                if not self.is_None(item["Account"]):
                    account, _ = Account.objects.get_or_create(code=item["Account"])
                output = (
                    None
                    if self.is_None(item["Output"])
                    else Output.objects.get(code=item["Output"])
                )
                ppg = (
                    None
                    if self.is_None(item["PPG"])
                    else PopulationPlanningGroup.objects.get(code=item["PPG"])
                )
                situation = (
                    None
                    if self.is_None(item["Situation"])
                    else Situation.objects.filter(
                        code=int(item["Situation"])
                        if type(item["Situation"]) == float
                        else item["Situation"]
                    ).first()
                )
                cost_center = (
                    None
                    if self.is_None(item["Cost Centre"])
                    else CostCenter.objects.get(code=int(item["Cost Centre"]))
                )
                pillar = (
                    None
                    if self.is_None(item["Pillar"])
                    else Pillar.objects.filter(
                        code=int(item["Pillar"])
                        if type(item["Pillar"]) == float
                        else item["Pillar"]
                    ).first()
                )

                goal = (
                    None
                    if self.is_None(item["Goal"])
                    else Goal.objects.get(code=item["Goal"])
                )
                budget = self.util_decimal(item["Budget(USD)"])
                report_date = int(item["To"])
                report_date = datetime.fromordinal(
                    datetime(1900, 1, 1).toordinal() + report_date - 2
                )

                _type = item["Account Type"]
                _type in ACCOUNT_TYPE_CHOICES.values()
                value = [
                    i
                    for i, v in ACCOUNT_TYPE_CHOICES.items()
                    if _type is not None and v.lower() == _type.lower()
                ]
                account_type = value[0] if len(value) > 0 else None
                AgreementBudget.objects.create(
                    agreement=agreement,
                    year=int(item["Bgt Yr"]),
                    account=account,
                    account_type=account_type,
                    output=output,
                    ppg=ppg,
                    situation=situation,
                    cost_center=cost_center,
                    goal=goal,
                    pillar=pillar,
                    report_date=report_date,
                    budget=round(budget, 4),
                )
            except Exception as e:
                print("Error in import_agreement_budgets", e, item)
                logger.error(str(e))
                errors.append(item)
        return errors

    def import_implementer_types(self, items):
        implementer_types = []
        for item in items:
            try:
                if item["Implementer Type"] not in implementer_types:
                    implementer_types.append(item["Implementer Type"])
                    ImplementerType.objects.get_or_create(
                        implementer_type=item["Implementer Type"],
                    )
            except Exception as e:
                print("Error in import_implementer_types -> {}".format(str(e)), item)

    def import_partners(self, items):
        partners = []
        for item in items:
            try:
                if item["Implementer"] not in partners:
                    partners.append(item["Implementer"])
                    implementer_type = ImplementerType.objects.get(
                        implementer_type=item["Implementer Type"],
                    )
                    partner, created = Partner.objects.get_or_create(
                        number=str(item["Implementer"])
                        if str(item["Implementer"]).isdigit()
                        else item["Implementer"],
                    )
                    if created:
                        partner.legal_name = item["HCR_DESCR"]
                        partner.save()
                    partner.implementer_type = implementer_type
                    partner.save()
            except Exception as e:
                logger.error(str(e))

    def import_pillars(self, items):
        pillars = []
        for item in items:
            if item["Pillar"] not in pillars:
                pillars.append(item["Pillar"])
                Pillar.objects.get_or_create(
                    code=int(item["Pillar"])
                    if type(item["Pillar"]) == float
                    else item["Pillar"],
                    description=item["HCR_PILLAR_DESC"],
                )

    def import_vendors(self, items):
        vendors = []
        for item in items:
            if item["VENDOR_ID"] not in vendors:
                vendors.append(item["VENDOR_ID"])
                Vendor.objects.get_or_create(
                    code=item["VENDOR_ID"],
                )

    def import_operations(self, items):
        operations = []
        for item in items:
            if item["Operation"] not in operations:
                operations.append(item["Operation"])
                Operation.objects.get_or_create(
                    code=item["Operation"],
                )

    def import_goals(self, items):
        goals = []
        for item in items:
            goal_list = item["Goal(s)"].split(", ")
            for goal in goal_list:
                if goal not in goals:
                    goals.append(goal)
                    Goal.objects.get_or_create(
                        code=goal,
                    )

    def import_cc_s(self, items):
        cc_s = []
        for item in items:
            try:
                if item["Cost Centre(s)"] is not "":
                    cc_list = str(item["Cost Centre(s)"]).split(", ")
                    for cc in cc_list:
                        try:
                            cc = str(int(float(cc.replace(",", ""))))
                        except:
                            cc = cc.replace(",", "")
                        if cc not in cc_s:
                            cc_s.append(cc)
                            CostCenter.objects.get_or_create(code=cc)
            except Exception as e:
                print(e)

    def import_ppg_s(self, items):
        ppg_s = []
        for item in items:
            try:
                if item["Planning Grp(s)"] is not "":
                    ppg_list = item["Planning Grp(s)"].split(", ")
                    for ppg in ppg_list:
                        if ppg not in ppg_s:
                            ppg_s.append(ppg)
                            PopulationPlanningGroup.objects.get_or_create(
                                code=ppg,
                            )
            except Exception as e:
                print("Error import_ppg_s: ", e, item)

    def import_situations(self, items):
        for item in items:
            try:
                if str(item["Situation(s)"]) is not "":
                    if "," in str(item["Situation(s)"]):
                        situation_list = item["Situation(s)"].split(", ")
                        for situation in situation_list:
                            Situation.objects.get_or_create(
                                code=situation,
                            )
                    else:
                        Situation.objects.get_or_create(
                            code=item["Situation(s)"],
                        )
            except Exception as e:
                print("Error import_situations: ", e, item)

    def import_agreement_types(self, items):
        agreement_types = []
        for item in items:
            if item["Agreement Type"] not in agreement_types:
                agreement_types.append(item["Agreement Type"])
                AgreementType.objects.get_or_create(
                    description=item["Agreement Type"],
                )

    def import_budget_refs(self, items):
        budget_refs = []
        for item in items:
            if item["Budget Year"] is not "":
                if item["Budget Year"] not in budget_refs:
                    budget_refs.append(item["Budget Year"])
                    cycle, created = Cycle.objects.get_or_create(
                        budget_ref=item["Budget Year"],
                        year=item["Budget Year"],
                    )
                    if created:
                        cycle.status_id = 1
                        cycle.save()

    def import_agreements(self, items):
        errors = []

        for item in items:
            try:
                if item["Business Unit"] != "":
                    title = item["DESCR100"]
                    agreement_type = AgreementType.objects.get(
                        description=item["Agreement Type"]
                    )
                    business_unit = BusinessUnit.objects.filter(
                        code=item["Business Unit"]
                    ).first()
                    if business_unit is None:
                        fo, _ = FieldOffice.objects.get_or_create(
                            name=item["Description"]
                        )
                        business_unit = BusinessUnit.objects.create(
                            code=item["Business Unit"],
                            name=item["Description"],
                            field_office=fo,
                        )
                    partner = Partner.objects.filter(
                        number=str(item["Implementer"])
                    ).first()
                    number = int(item["IP Agreement Number"])
                    operation = Operation.objects.get(code=item["Operation"])
                    pillar = Pillar.objects.filter(
                        code=int(item["Pillar"])
                        if type(item["Pillar"]) == float
                        else item["Pillar"]
                    ).first()
                    start_date = item["Start Date"]
                    end_date = item["Completion Date"]
                    cycle = Cycle.objects.get(year=item["Budget Year"])
                    vendor = Vendor.objects.filter(code=item["VENDOR_ID"]).first()
                    agreement = Agreement.objects.filter(
                        business_unit=business_unit, number=number, cycle=cycle
                    )

                    country = self.create_country(item)
                    audit_worksplit_group = AuditWorksplitGroup.objects.filter(
                        business_units=business_unit
                    ).first()
                    if agreement.exists():
                        continue
                    else:
                        agreement = Agreement.objects.create(
                            business_unit=business_unit,
                            number=number,
                            cycle=cycle,
                            partner=partner,
                            status_id=1,
                        )
                        if vendor is not None:
                            agreement.vendor = vendor
                        agreement.title = title
                        agreement.start_date = start_date
                        agreement.end_date = end_date
                        agreement.agreement_type = agreement_type
                        agreement.operation = operation
                        agreement.pillar = pillar
                        agreement.country = country
                        agreement.audit_worksplit_group = audit_worksplit_group
                        if self.delta_projects and "{}{}".format(agreement.business_unit.code, agreement.number) in self.delta_projects:
                            agreement.delta = True
                        agreement.save()
                        goals_list = item["Goal(s)"].split(", ")
                        for goal in goals_list:
                            agreement.goals.add(Goal.objects.get(code=goal))
                        cc_list = str(item["Cost Centre(s)"]).split(", ")
                        for cc in cc_list:
                            try:
                                cc = str(int(float(cc.replace(",", ""))))
                            except:
                                cc = cc.replace(",", "")
                            agreement.cost_centers.add(CostCenter.objects.get(code=cc))
                        ppg_list = item["Planning Grp(s)"].split(", ")
                        for ppg in ppg_list:
                            agreement.ppg_s.add(
                                PopulationPlanningGroup.objects.get(code=ppg)
                            )
                        situations = str(item["Situation(s)"]).split(", ")
                        for situation in situations:
                            try:
                                agreement.situations.add(
                                    Situation.objects.filter(
                                        code=int(situation)
                                    ).first()
                                )
                            except Exception as e:
                                print("Error while importing Situation", e)
            except Exception as e:
                print("check", e, item)
                logger.error(str(e))
                errors.append(item)
        return errors

    def import_currency(self, items):
        currencies = []
        for item in items:
            if item["Currency"] not in currencies:
                currencies.append(item["Currency"])
                if item["Currency"].strip() not in [
                    "",
                ]:
                    Currency.objects.get_or_create(code=item["Currency"])

    def util_decimal(self, item):
        if type(item) is str:
            _ = item.strip()
            if _ in [""]:
                return 0
            else:
                return float(_)
        return item

    def import_agreement_report(self, items):
        errors = []
        for item in items:
            try:
                if item["Business Unit"] is not "":
                    business_unit = BusinessUnit.objects.get(code=item["Business Unit"])
                    number = int(item["IP Agreement Number"])
                    agreement = Agreement.objects.filter(
                        business_unit=business_unit, number=number
                    ).first()
                    currency = None
                    if item["Currency"].strip() not in [
                        "",
                    ]:
                        currency = Currency.objects.get(code=item["Currency"])
                    report_date = item["Agreement Date"]
                    base = self.util_decimal(item["Budget (Agreement)"])
                    installments = self.util_decimal(item["Installments Paid to IP"])
                    adjustments = self.util_decimal(item["Adjustments for IP"])
                    cancellations = self.util_decimal(item["HCR_IP_CANCEL_AMT"])
                    ipfr = self.util_decimal(item["HCR_IP_IPFR_AMT"])
                    refund = self.util_decimal(item["HCR_IP_REFUND_AMT"])
                    receivables = self.util_decimal(item["HCR_IP_RECV_AMT"])
                    write_off = self.util_decimal(item["HCR_IP_WRITOFF_AMT"])

                    AgreementReport.objects.get_or_create(
                        agreement=agreement,
                        currency=currency,
                        base=base,
                        installments=installments,
                        adjustments=adjustments,
                        cancellations=cancellations,
                        ipfr=ipfr,
                        refund=refund,
                        receivables=receivables,
                        write_off=write_off,
                    )
            except Exception as e:
                logger.error(str(e))
                errors.append(item)
        return errors

    def handle_duplicates(self, items):
        updated_items = {}
        for item in items:
            if item["PPA CODE"] not in updated_items:
                updated_items[item["PPA CODE"]] = item
            else:
                old_item = updated_items[item["PPA CODE"]]
                old_item.update(
                    {
                        "Budget (Agreement)": old_item["Budget (Agreement)"]
                        + item["Budget (Agreement)"],
                        "Installments Paid to IP": old_item["Installments Paid to IP"]
                        + item["Installments Paid to IP"],
                    }
                )
                updated_items[item["PPA CODE"]] = old_item
        return list(updated_items.values())

    def import_HNIP999(self, sheet):
        sheet = self.wb.sheet_by_name(sheet)
        main_sheet = self.wb.sheet_by_name(self.main_sheet)

        all_projects = []
        for i in range(1, main_sheet.nrows):
            try:
                ppa = sheet.cell_value(i, 0)
                all_projects.append(ppa)
            except:
                pass

        keys = []
        for idx_j in range(sheet.ncols):
            keys.append(sheet.cell(0, idx_j).value)

        items = []
        nrows = sheet.nrows

        for idx_i in range(1, nrows):
            item = {}
            for idx_j in range(sheet.ncols):
                if keys[idx_j] == "Implementer":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v) if ".0" in str(v) else v
                if keys[idx_j] == "Situation(s)":
                    try:
                        item[keys[idx_j]] = int(sheet.cell(idx_i, idx_j).value)
                    except:
                        item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
                elif keys[idx_j] == "IP Agreement Number":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v)
                else:
                    item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
            if self.import_delta:
                if item["PPA CODE"] in self.delta_projects:
                    item = validate_data_type(item)
                    items.append(item)
            else:
                item = validate_data_type(item)
                items.append(item)
        return self.handle_duplicates(items)

    def import_HNIP998(self, sheet):
        sheet = self.wb.sheet_by_name(sheet)

        keys = []
        for idx_j in range(sheet.ncols):
            keys.append(sheet.cell(0, idx_j).value)

        items = []
        for idx_i in range(1, sheet.nrows):
            item = {}
            for idx_j in range(sheet.ncols):
                if keys[idx_j] == "Implementer":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v) if ".0" in str(v) else v
                elif keys[idx_j] == "IP Agreement Number":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v)
                else:
                    item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value

            if self.import_delta == True:
                if (
                    "{}{}".format(
                        item["Business Unit"], int(item["IP Agreement Number"])
                    )
                    in self.delta_projects
                ):
                    item = validate_data_type(item)
                    items.append(item)
                    continue
            else:
                item = validate_data_type(item)
                items.append(item)
        return items

    def create_field_assessment(self, year, is_delta):
        queryset = Agreement.objects.filter(cycle__year=year)
        if is_delta:
            agreements = (
                queryset
                .filter(field_assessment__isnull=True,)
                .exclude(fin_sense_check=True)
            )
            queryset = queryset.exclude(fin_sense_check=True)
        else:
            agreements = queryset.filter(
                cycle__year=year,
                field_assessment__isnull=True
            )

        for agreement in agreements:
            field_assessment = FieldAssessment.objects.create(score_id=4)
            agreement.field_assessment = field_assessment
            agreement.save()

        if is_delta:
            assessment = Agreement.objects.filter(cycle__year=year).values_list(
                "field_assessment_id", flat=True
            )
            FieldAssessment.objects.filter(id__in=assessment).update(deadline_date=None)
            field_offices = (
                queryset
                .distinct("business_unit__field_office_id")
                .values_list("business_unit__field_office_id", flat=True)
            )
            fo_progress = FieldAssessmentProgress.objects.filter(field_office_id__in=field_offices)
            fo_progress.update(pending_with="REVIEWER")

    def import_selection_settings(self, year):

        data = {
            "procurement_perc": "0.100",
            "cash_perc": "0.100",
            "staff_cost_perc": "0.100",
            "emergency_perc": "0.050",
            "audit_perc": "0.200",
            "oios_perc": "0.200",
            "field_assessment_perc": "0.250",
            "risk_threshold": "2.6",
            "cost_benefit": 150000,
            "high_values": 5000000,
            "materiality": 645000,
            "sense_check_icq": 0.71,
        }
        cycle = Cycle.objects.filter(year=year).first()
        data.update({"cycle_id": cycle.id})
        SelectionSettings.objects.get_or_create(**data)

    def update_agreement_with_budget(self, year):

        agreements = Agreement.objects.filter(cycle__year=year)
        for agreement in agreements:
            try:
                data = AgreementBudget.objects.filter(
                    agreement=agreement, year=agreement.cycle.year
                )
                budget_sum = data.aggregate(Sum("budget"))["budget__sum"]
                agreement.budget = budget_sum
                agreement.save()
            except Exception as e:
                print("Error in update_agreement_with_budget ", e, str(agreement))

    def import_pre_list(self, year):
        sheet = self.wb.sheet_by_name("main")
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 2)
            number = sheet.cell_value(i, 11)
            risk_rating = round(sheet.cell_value(i, 41), 3)
            pre_reason = sheet.cell_value(i, 82)
            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            sense_check = SenseCheck.objects.filter(description=pre_reason).first()
            if agreement is not None and sense_check is not None:
                agreement.reason = sense_check
                agreement.risk_rating = risk_rating
                agreement.pre_sense_check = True
            elif agreement is not None:
                agreement.risk_rating = risk_rating
            agreement.status_id = PRE03
            agreement.save()
        Cycle.objects.filter(year=year).update(status_id=PRE03)

    def import_int_list(self, year):
        sheet = self.wb.sheet_by_name("main")
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 2)
            number = sheet.cell_value(i, 11)
            int_reason = sheet.cell_value(i, 83)
            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            sense_check = SenseCheck.objects.filter(description=int_reason).first()
            if agreement is not None and sense_check is not None:
                agreement.int_reason = sense_check
                agreement.int_sense_check = True
            agreement.status_id = INT02
            agreement.save()
        Cycle.objects.filter(year=year).update(status_id=INT02)

    def import_fin_list(self, year):
        sheet = self.wb.sheet_by_name("main")
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 2)
            number = sheet.cell_value(i, 11)
            fin_reason = sheet.cell_value(i, 88)
            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            sense_check = SenseCheck.objects.filter(description=fin_reason).first()
            if agreement is not None and sense_check is not None:
                agreement.fin_reason = sense_check
                agreement.fin_sense_check = True
            agreement.status_id = FIN03
            agreement.save()
        Cycle.objects.filter(year=year).update(status_id=FIN03)

    def import_audit_assignments(self):
        sheet = self.wb.sheet_by_name("audit_selection")
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 4)
            number = sheet.cell_value(i, 7)
            audit_agency = sheet.cell_value(i, 10)
            audit_agency = audit_agency.rstrip()
            agency = AuditAgency.objects.filter(legal_name__iexact=audit_agency).first()
            if not agency:
                agency = AuditAgency.objects.create(legal_name=audit_agency)
            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            if agreement is not None:
                agreement.audit_firm = agency
                agreement.save()

    def import_focal_points(self):
        sheet = self.wb.sheet_by_name("audit_selection")
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 4)
            number = sheet.cell_value(i, 7)

            unhcr_focal = sheet.cell_value(i, 15)
            unhcr_focal = re.findall(
                "[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+", str(unhcr_focal)
            )
            if len(unhcr_focal) > 0:
                unhcr_focal = unhcr_focal[0]
            else:
                unhcr_focal = None

            unhcr_alt_focal = sheet.cell_value(i, 16)
            unhcr_alt_focal = re.findall(
                "[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+", str(unhcr_alt_focal)
            )
            if len(unhcr_alt_focal) > 0:
                unhcr_alt_focal = unhcr_alt_focal[0]
            else:
                unhcr_alt_focal = None

            partner_focal = sheet.cell_value(i, 17)
            partner_focal = re.findall(
                "[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+", str(partner_focal)
            )
            if len(partner_focal) > 0:
                partner_focal = partner_focal[0]
            else:
                partner_focal = None

            partner_alt_focal = sheet.cell_value(i, 18)
            partner_alt_focal = re.findall(
                "[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+", str(partner_alt_focal)
            )
            if len(partner_alt_focal) > 0:
                partner_alt_focal = partner_alt_focal[0]
            else:
                partner_alt_focal = None

            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            if agreement is not None:
                agreement.focal_points.clear()
                if unhcr_focal is not None:
                    unhcr_focal_member = get_or_create_aggr_member(
                        User.objects.get_or_create(email=unhcr_focal)[0],
                        "unhcr",
                        "focal",
                    )
                    agreement.focal_points.add(unhcr_focal_member)
                if unhcr_alt_focal is not None:
                    unhcr_alt_focal_member = get_or_create_aggr_member(
                        User.objects.get_or_create(email=unhcr_alt_focal)[0],
                        "unhcr",
                        "alternate_focal",
                    )
                    agreement.focal_points.add(unhcr_alt_focal_member)
                if partner_focal is not None:
                    partner_focal_member = get_or_create_aggr_member(
                        User.objects.get_or_create(email=partner_focal)[0],
                        "partner",
                        "focal",
                    )
                    agreement.focal_points.add(partner_focal_member)
                if partner_alt_focal is not None:
                    partner_alt_focal_member = get_or_create_aggr_member(
                        User.objects.get_or_create(email=partner_alt_focal)[0],
                        "partner",
                        "alternate_focal",
                    )
                    agreement.focal_points.add(partner_alt_focal_member)
                agreement.save()

    def import_data(self, year, import_type, is_delta):
        items = self.import_HNIP999(self.HNIP999_sheet)
        agreements = self.parse_HNIP999(items)
        items_998 = self.import_HNIP998(self.HNIP998_sheet)
        filtered_items_998 = self.filter_HNIP998(items=items_998, agreements=agreements)

        if import_type == "all" or import_type == None:
            print("Importing Implementer Types")
            self.import_implementer_types(items)
            print("Importing Partners")
            self.import_partners(items)
            print("Importing Pillars")
            self.import_pillars(items)
            print("Importing Vendors")
            self.import_vendors(items)
            print("Importing Operations")
            self.import_operations(items)
            print("Importing Goals")
            self.import_goals(items)
            print("Importing Cost Centers")
            self.import_cc_s(items)
            print("Importing Population Planning Groups")
            self.import_ppg_s(items)
            print("Importing Situations")
            self.import_situations(items)
            print("Importing Agreement Types")
            self.import_agreement_types(items)
            print("Importing Budget Refs")
            self.import_budget_refs(items)
            print("Importing Agreements")
            self.import_agreements(items)
            print("Importing Currencies")
            self.import_currency(items)
            print("Importing Agreement Reports")
            self.import_agreement_report(items)
            print("Importing Outputs")
            self.import_outputs(filtered_items_998)
            print("Importing Agreement Budgets")
            self.import_agreement_budgets(filtered_items_998)
            print("Updating project wise agreement budget")
            self.update_agreement_with_budget(year)
            print("Import default project selection settings")
            self.import_selection_settings(year)
            print("Creating field assessment for each agreemnt")
            self.create_field_assessment(year, is_delta)
            if is_delta:
                cycle = Cycle.objects.filter(year=year).first()
                if cycle is not None:
                    cycle.status_id=1
                    cycle.save()
            print("Finished")
        else:
            callable_function = getattr(self, import_type)
            if import_type in ["import_agreement_budgets", "import_outputs"]:
                callable_function(filtered_items_998)
            else:
                callable_function(items)

    def test(self):
        items_998 = self.import_HNIP998(self.HNIP998_sheet)
        items_999 = self.import_HNIP999(self.HNIP999_sheet)

        print(len(items_998), len(items_999), self.import_delta, self.delta_projects)


class RiskRatingData:

    icq_sheet = "consolidated_icq"
    pqp_sheet = "consolidated_pqp"
    oios_sheet = "oios"
    partner_modified_sheet = "partner_modified"
    field_assessent_sheet = "field_assessment"
    partner_adverse_disclaimer_sheet = "adverse_disclaimer"
    previous_audits_sheet = "previous_audits"
    partner_engagement = "partner_engagement"
    cancelled_audit_sheet = "cancelled_audits"

    def __init__(self, file_id):
        file_obj = CommonFile.objects.filter(id=file_id).first()
        self.wb = xlrd.open_workbook(file_contents=file_obj.file_field.read())
        print("Wordbook loaded")

    def import_icq(self):
        sheet = self.wb.sheet_by_name(self.icq_sheet)
        for i in range(1, sheet.nrows):
            try:
                implementer = sheet.cell_value(i, 0)
                bu = sheet.cell_value(i, 1)
                implementer = implementer.replace(bu, "")
                implementer = implementer.lower()
                _bu = bu.lower()
                implementer = implementer.replace(_bu, "")
                score = round(sheet.cell_value(i, 2), 8)
                year = int(sheet.cell_value(i, 3))
                partner = Partner.objects.filter(
                    number__iexact=str(implementer)
                ).first()
                business_unit = BusinessUnit.objects.filter(code__iexact=bu).first()
                if business_unit is None:
                    print("import_icq, Find related country for {}".format(bu))
                    continue
                if business_unit is not None and partner is not None:
                    icq_report = ICQReport.objects.filter(
                        partner=partner, business_unit=business_unit, year=year
                    ).first()
                    if icq_report is None:
                        icq_report = ICQReport.objects.create(
                            partner=partner,
                            business_unit=business_unit,
                            year=year,
                            overall_score_perc=score,
                        )
                    else:
                        if float(icq_report.overall_score_perc) - score != 0:
                            icq_report.overall_score_perc = score
                            icq_report.save()
                else:
                    print("Could not import ICQ coz bu or partner is None")
            except Exception as e:
                print(dict(implementer=implementer, bu=bu, year=year))
                print("Error: ", e)

    def import_pqp(self):
        sheet = self.wb.sheet_by_name(self.pqp_sheet)
        for i in range(1, sheet.nrows):
            try:
                year = int(sheet.cell_value(i, 0))
                bu = sheet.cell_value(i, 3)
                number = sheet.cell_value(i, 4)
                status = sheet.cell_value(i, 7)
                status = True if "Yes" in status else False
                if number != "" or bu != "":
                    if bu == "JORMN" or bu == "UNHCR":
                        agreement = Agreement.objects.filter(
                            business_unit__code__icontains=bu, number=number
                        ).first()
                    else:
                        agreement = Agreement.objects.filter(
                            business_unit__code=bu, number=number
                        ).first()

                if agreement is not None:
                    pqp_obj, created = PQPStatusNew.objects.get_or_create(
                        agreement=agreement,
                        business_unit=agreement.business_unit,
                        partner=agreement.partner,
                        year=year,
                    )
                    pqp_obj.status = status
                    pqp_obj.save()
                else:
                    continue
            except Exception as e:
                print("Error: ", e)

    def import_oios(self, year):
        sheet = self.wb.sheet_by_name(self.oios_sheet)
        for i in range(1, sheet.nrows):
            try:
                bu = sheet.cell_value(i, 0)
                report_date = sheet.cell_value(i, 1)
                report_date = parse_excel_date(report_date)
                overall_rating = sheet.cell_value(i, 2)
                business_unit = BusinessUnit.objects.filter(code=bu).first()
                if business_unit is None:
                    print("import_oios Find related country for {}".format(bu))
                    continue
                if business_unit is not None:
                    oios = OIOSBusinessUnitReport.objects.filter(
                        business_unit=business_unit, report_date__year=year
                    ).first()
                    if oios is not None:
                        oios.overall_rating = overall_rating
                        oios.save()
                    else:
                        OIOSBusinessUnitReport.objects.create(
                            business_unit=business_unit,
                            overall_rating=overall_rating,
                            report_date=report_date,
                        )
            except Exception as e:
                print("Error: ", e)

    def import_partner_modified(self):
        sheet = self.wb.sheet_by_name(self.partner_modified_sheet)
        for i in range(1, sheet.nrows):
            try:
                try:
                    implementer = int(sheet.cell_value(i, 0))
                except:
                    implementer = sheet.cell_value(i, 0)

                legal_name = sheet.cell_value(i, 1)
                bu = sheet.cell_value(i, 2)
                year = int(sheet.cell_value(i, 3))
                modified = int(sheet.cell_value(i, 4))

                partner, created = Partner.objects.get_or_create(
                    number=str(implementer)
                )
                if created:
                    partner.legal_name = legal_name
                    partner.save()

                business_unit = BusinessUnit.objects.filter(code=bu).first()
                if business_unit is None:
                    print(
                        "import_partner_modified Find related country for {}".format(bu)
                    )
                    continue
                modifed, created = Modified.objects.get_or_create(
                    year=year,
                    modified=modified,
                    partner=partner,
                    business_unit=business_unit,
                    commissioned=False,
                )
            except Exception as e:
                print("Error: ", e)

    def import_field_assessment(self, year):
        sheet = self.wb.sheet_by_name(self.field_assessent_sheet)
        for i in range(1, sheet.nrows):
            bu = sheet.cell_value(i, 1)
            number = sheet.cell_value(i, 2)
            if number is not "":
                score = int(sheet.cell_value(i, 4))

                agreement = Agreement.objects.filter(
                    business_unit__code=bu, number=number, cycle__year=year
                ).first()
                if agreement:
                    assessment = FieldAssessment.objects.filter(
                        id=agreement.field_assessment_id
                    ).first()
                    score_obj = Score.objects.filter(value=score).first()
                    assessment.score = score_obj
                    assessment.save()

    def update_field_assessment_progress(self, year):

        field_offices = (
            Agreement.objects.filter(cycle__year__contains=year)
            .distinct()
            .values_list("business_unit__field_office_id", flat=True)
        )
        default_comment = Comment.objects.create(
            user_id=1, message="Field assessment completed by admin"
        )
        for office in field_offices:
            progress, created = FieldAssessmentProgress.objects.get_or_create(
                budget_ref=year, field_office_id=office
            )
            if created == True:
                progress.comments.add(default_comment)
            else:
                progress.comments.add(default_comment)
            progress.pending_with = "COMPLETED"
            progress.save()

    def import_partner_adverse_disclaimer(self):
        sheet = self.wb.sheet_by_name(self.partner_adverse_disclaimer_sheet)
        for i in range(1, sheet.nrows):
            try:
                year = sheet.cell_value(i, 2)
                bu_code = sheet.cell_value(i, 3)
                try:
                    partner_number = int(sheet.cell_value(i, 4))
                except:
                    partner_number = sheet.cell_value(i, 4)

                agreement_number = sheet.cell_value(i, 6)
                status = sheet.cell_value(i, 8)
                modification_type = sheet.cell_value(i, 7)

                partner, _ = Partner.objects.get_or_create(number=str(partner_number))
                business_unit = BusinessUnit.objects.filter(code=bu_code).first()
                if business_unit is None:
                    print(
                        "import_partner_adverse_disclaimer Find related country for {}".format(
                            bu_code
                        )
                    )
                    continue
                AdverseDisclaimer.objects.create(
                    agreement_number=agreement_number,
                    business_unit=business_unit,
                    partner=partner,
                    year=year,
                    modification_type=modification_type,
                    status=status,
                )
            except Exception as e:
                print("Error in import_partner_adverse_disclaimer", e)

    def import_previous_audits(self):
        sheet = self.wb.sheet_by_name(self.previous_audits_sheet)
        for i in range(1, sheet.nrows):
            try:
                bu_code = sheet.cell_value(i, 0)
                agreement_number = sheet.cell_value(i, 1)
                try:
                    partner_number = int(sheet.cell_value(i, 2))
                except:
                    partner_number = sheet.cell_value(i, 2)

                year = sheet.cell_value(i, 3)
                audited = sheet.cell_value(i, 4)

                partner, _ = Partner.objects.get_or_create(number=str(partner_number))
                business_unit = BusinessUnit.objects.filter(code=bu_code).first()
                if business_unit is None:
                    print(
                        "import_previous_audits Find related country for {}".format(
                            bu_code
                        )
                    )
                    continue
                PreviousAudit.objects.get_or_create(
                    business_unit=business_unit,
                    partner=partner,
                    agreement_number=agreement_number,
                    year=year,
                    audited=audited,
                )
            except Exception as e:
                print("Error in import_partner_adverse_disclaimer", e)

    def update_partner_engagement_date(self):
        Partner.objects.all().update(engagement_date=None)
        sheet = self.wb.sheet_by_name(self.partner_engagement)
        for i in range(1, sheet.nrows):
            try:
                engagement_date = sheet.cell_value(i, 3)
                engagement_date = parse_excel_date(engagement_date)
                try:
                    partner_number = int(sheet.cell_value(i, 0))
                except:
                    partner_number = sheet.cell_value(i, 0)
                Partner.objects.filter(number=str(partner_number)).update(
                    engagement_date=engagement_date
                )
            except Exception as e:
                print("Error in update_partner_engagement_date", e)

    def import_cancelled_audit(self):
        sheet = self.wb.sheet_by_name(self.cancelled_audit_sheet)
        for i in range(1, sheet.nrows):
            try:
                year = sheet.cell_value(i, 9)
                bu_code = sheet.cell_value(i, 2)
                try:
                    partner_number = int(sheet.cell_value(i, 5))
                except:
                    partner_number = sheet.cell_value(i, 5)

                agreement_number = sheet.cell_value(i, 7)
                status = sheet.cell_value(i, 8)
                reason = sheet.cell_value(i, 10)

                partner, _ = Partner.objects.get_or_create(number=str(partner_number))
                business_unit = BusinessUnit.objects.filter(code=bu_code).first()
                if business_unit is None:
                    print(
                        "import_cancelled_audit Find related country for {}".format(
                            bu_code
                        )
                    )
                    continue
                CancelledAudit.objects.get_or_create(
                    business_unit=business_unit,
                    partner=partner,
                    agreement_number=int(agreement_number),
                    year=int(year),
                    reason=reason,
                    is_cancelled=int(status),
                )
            except Exception as e:
                print("Error in import_cancelled_audits", e)

    def update_project_selection_reason(self, year):
        update_reason(year)

    def import_risk_rating_data(self, year):
        from field.tasks import update_risk_rating

        print("Importing ICQ")
        self.import_icq()
        print("Importing PQP")
        self.import_pqp()
        print("Importing OIOS")
        self.import_oios(year)
        print("Importing Partner Modified")
        self.import_partner_modified()
        # print("Importing Field Assessment", year)
        # self.import_field_assessment(year)
        # print("Updating Risk rating")
        # update_risk_rating(year)
        # print("Updating field assessment prgress", year)
        # self.update_field_assessment_progress(year)
        print("Importing Partner Adverse Disclaimer")
        self.import_partner_adverse_disclaimer()
        print("Importing Previous Audits")
        self.import_previous_audits()
        # print("Updating Partner Engagement Date")
        # self.update_partner_engagement_date()
        print("Importing Cancelled Audit")
        self.import_cancelled_audit()
        # print("Updating Project Selection Reasons")
        # self.update_project_selection_reason(year)
        print("Finished")


class Command(BaseCommand):
    help = "Import interim list from the excel file for 2020"

    def add_arguments(self, parser):
        parser.add_argument(
            "-t",
            "--type",
            type=str,
            help="project type (delta or all)",
        )
        parser.add_argument(
            "-y",
            "--year",
            type=int,
            help="year",
        )

    def handle(self, *args, **kwargs):
        project_type = kwargs.get("type", None)
        year = kwargs.get("year", None)
        if project_type not in ["delta", "risk_rating", "all"]:
            print("Please provide a valid argument --type delta or --type all")
            return None
        elif year is None or year not in [2020, 2021]:
            print("Please provide a valid argument for --year")
            return None

        file_name = "data_import_template_2021 small.xlsx"
        file_path = os.path.join(
            os.getcwd(), "audit", "applications", "project", "excel", file_name
        )
        with open(file_path, "rb") as fi:
            file_obj = CommonFile.objects.create(
                file_field=File(fi, name=os.path.basename(fi.name))
            )
        if project_type == "delta":
            wb = xlrd.open_workbook(file_path)
            sheet = wb.sheet_by_name("main")
            delta_projects = []
            for i in range(1, sheet.nrows):
                try:
                    ppa = sheet.cell_value(i, 0)
                    bu = sheet.cell_value(i, 2)
                    number = int(sheet.cell_value(i, 11))
                    delta_projects.append(ppa)
                except:
                    pass
            import_agreements = ImportAgreements(file_path)
            import_agreements.import_delta = True
            import_agreements.delta_projects = delta_projects
            import_agreements.import_data(year, "all", None)
        elif project_type == "risk_rating_data":
            risk_rating_data = RiskRatingData(file_path)
            risk_rating_data.import_risk_rating_data(year)

        elif project_type == "all":
            import_agreements = ImportAgreements(file_obj.id)
            import_agreements.import_data(year, "all", None)
            risk_rating_data = RiskRatingData(file_obj.id)
            risk_rating_data.import_risk_rating_data(year)
            update_risk_rating(year)
        elif project_type == "risk_rating":
            risk_rating_data = RiskRatingData(file_obj.id)
            risk_rating_data.import_risk_rating_data(year)
            update_risk_rating(year)
        else:
            print("Please provide a valid argument --type delta or --type all")
