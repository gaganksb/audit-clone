# import os
# import xlrd
# from django.db import connection
# from project.models import Agreement
# from common.models import BusinessUnit
# from partner.models import ICQReport
# from django.core.management.base import BaseCommand
# from project.fixtures.risk_rating_data import test_data
# from project.risk_rating.risk_rating_funcs import risk_rating_query_2019


# class Command(BaseCommand):
#     help = 'Send notifications to users with daily email preference'

#     def handle(self, *args, **kwargs):
#         print("Risk Rating Test")
#         cwd = os.getcwd()
#         files = ["2019 - Selection of Projects for Audit.xlsx"]
#         cursor = connection.cursor()
#         for file in files:
#             xls = os.path.join(cwd, "MSRP", file)
#             wb = xlrd.open_workbook(xls)
#             pqp_sheet = " All Test - Initial"
#             sheet = wb.sheet_by_name(pqp_sheet)
#             test_pass = []
#             test_fail = []
#             for i in range(1, sheet.nrows):
#                 try:
#                     business_unit = sheet.cell_value(i, 2)
#                     aggr_num = int(sheet.cell_value(i, 11))
#                     old_rating = round(sheet.cell_value(i, 41), 2)
#                     partner_num = sheet.cell_value(i, 8)
#                     partner_num = int(partner_num) if (isinstance(partner_num, int) or isinstance(partner_num, float)) else str(partner_num)

#                     # icq_qs = ICQReport.objects.filter(partner__number=partner_num)
#                     # bu_qs = BusinessUnit.objects.filter(code=business_unit)

#                     agreement = Agreement.objects.filter(cycle__year=2019, number=aggr_num, business_unit__code=business_unit)
#                     if agreement.exists():
#                         risk_rating_q = risk_rating_query_2019.replace("__agreement_id__", str(agreement.first().id))
#                         cursor.execute(risk_rating_q)
#                         data = cursor.fetchall()
#                         rating = round(float(data[0][0]), 2)

#                         if abs(old_rating-rating) < 0.25 :
#                             test_pass.append(1)
#                         else:
#                             print("Test Failed for Agreement id: {} and Agreement Number: {} and BU: {}".format(agreement.first().id, aggr_num, business_unit))
#                             print("Pre saved score: {} Calculated score: {}".format(old_rating, rating))
#                             test_fail.append(1)
#                 except Exception as e:
#                     print(e)

#             print({
#                 "Total Cases ": len(test_pass) + len(test_fail),
#                 "Test Pass ": len(test_pass),
#                 "Test Fail ": len(test_fail)
#             })
