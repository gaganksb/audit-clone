import os
import xlrd
from project.models import Agreement
from project.helpers import check_risk_rating_breakdown
from project.management.commands.project_agreement_import import validate_data_type


class RiskRatingTest:
    def __init__(self):

        file_name = "data_import_template.xlsx"
        file_path = os.path.join(
            os.getcwd(), "audit", "applications", "project", "excel", file_name
        )
        self.wb = xlrd.open_workbook(file_path)

    def read_data(self, sheet):
        sheet = self.wb.sheet_by_name(sheet)

        keys = []
        for idx_j in range(sheet.ncols):
            keys.append(sheet.cell(0, idx_j).value)

        items = []
        nrows = sheet.nrows

        for idx_i in range(1, nrows):
            item = {}
            for idx_j in range(sheet.ncols):
                if keys[idx_j] == "Implementer":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v) if ".0" in str(v) else v
                if keys[idx_j] == "Situation(s)":
                    try:
                        item[keys[idx_j]] = int(sheet.cell(idx_i, idx_j).value)
                    except:
                        item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value
                elif keys[idx_j] == "IP Agreement Number":
                    v = sheet.cell(idx_i, idx_j).value
                    item[keys[idx_j]] = int(v)
                else:
                    item[keys[idx_j]] = sheet.cell(idx_i, idx_j).value

            item = validate_data_type(item)
            items.append(item)
        return items

    def run_test(self):
        items = self.read_data("main")

        for i in items:
            agreement = (
                Agreement.objects.filter(
                    business_unit__code=i["Business Unit"],
                    number=i["IP Agreement Number"],
                )
            ).first()
            risk_rating = check_risk_rating_breakdown(agreement)

            if abs(int(risk_rating["procurement"]) - int(i["Procurement "])) > 2:
                print(
                    "procurement",
                    agreement.id,
                    risk_rating["procurement"],
                    i["Procurement "],
                )
            if abs(int(risk_rating["staff_cost"]) - int(i["Staff Costs"])) > 2:
                print("staff_cost", risk_rating["staff_cost"], i["Staff Costs"])
            if abs(int(risk_rating["cash"]) - int(i["Cash"])) > 2:
                print("cash", risk_rating["cash"], i["Cash"])
            if not (int(risk_rating["other_accounts"]) == int(i["Other Accounts"])):
                print(
                    "other_accounts", risk_rating["other_accounts"], i["Other Accounts"]
                )
            if not (
                risk_rating["goal"]
                == i[
                    "Emergency (Goal = PB)\nOnly PB - 1\nPB and Other - 2\nRest - Blank "
                ]
            ):
                print(
                    "goal",
                    risk_rating["goal"],
                    i[
                        "Emergency (Goal = PB)\nOnly PB - 1\nPB and Other - 2\nRest - Blank "
                    ],
                )
            if not (
                int(risk_rating["modified"])
                == int(i["Modified in Last 4 Years (2016 to 2019)"])
            ):
                print(
                    "modified",
                    risk_rating["modified"],
                    i["Modified in Last 4 Years (2016 to 2019)"],
                )
            if not (
                int(risk_rating["pqp_worldwide"])
                == int(i["PQP Status (file as of 30 Sept 2020)"])
            ):
                print(
                    "pqp_worldwide",
                    risk_rating["pqp_worldwide"],
                    i["PQP Status (file as of 30 Sept 2020)"],
                )
            if not (int(risk_rating["oios_rating"]) == int(i["OIOS"])):
                print("oios_rating", risk_rating["oios_rating"], i["OIOS"])
            if not (
                int(risk_rating["risk_rating"])
                == int(
                    i[
                        "Project Risk Rating (Project Profile + OIOS+Field Partner Performance Assessment)"
                    ]
                )
            ):
                print(
                    "risk_rating",
                    risk_rating["risk_rating"],
                    i[
                        "Project Risk Rating (Project Profile + OIOS+Field Partner Performance Assessment)"
                    ],
                )
