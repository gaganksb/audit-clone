from django.apps import apps
from django.db import models
from django.db.models import Q, Sum
from django.core.validators import MinValueValidator, MaxValueValidator
from auditing.models import AuditWorksplitGroup, AuditAgency
from common.models import Country, BusinessUnit, CostCenter, Currency
from common.consts import (
    AGREEMENT_MEMBER_CHOICES,
    ACCOUNT_STATUSES,
    YES_NO,
    AUDIT_CANCELATION_REASON_CHOICES,
)

from django.conf import settings

from audit.core.abstract_model import AbstractModel
from django.utils.translation import ugettext_lazy as _
from utils.rest.code import code
from django.contrib.postgres.fields import JSONField
import datetime

MIN_YEAR = 1980
MAX_YEAR = 2050

SENSE_CHECK = (
    ("default", "Default"),
    ("risk_above", "Projects above Risk Threshold"),
    (
        "adverse_opinion",
        "Remaining Projects having Adverse or Disclaimer of Opinion in last 4 years",
    ),
    (
        "budget_50k",
        "Remaining Projects implemented by New Partner to UNHCR with budget >= USD 50k",
    ),
    (
        "not_audited_before_1",
        "Remaining Partners not Audited Before for their PAs created in 2013 and 2014 and with budget >=50k",
    ),
    (
        "not_audited_before_2",
        "Remaining Partners not Audited Before for their PAs created in 2015 and 2016 and with budget >=50k",
    ),
    (
        "not_audited_before_3",
        "Remaining Partners not Audited for their PAs created in 2017 with budget >=50k and Risk rating >2.7",
    ),
    (
        "qualified_opinions",
        "Remaining Partners with qualified opinions in 2017 audit cycle and budget >=50k",
    ),
    ("budget_5M", "Remaining Projects with Budget >=5M"),
    (
        "budget_656k",
        "Remaining Projects having Budget >=656k (In OIOS Top 20 Countries) - 10 Highest Budget",
    ),
    (
        "budget_656k_icq",
        "Remaining Projects having Budget >=656k (In OIOS Top 20 Countries) - out of 10 Highest Budget but with recent ICQ <0.70",
    ),
    (
        "interim",
        "From/Included in the Interim list as field/HQ requests or category changed",
    ),
    ("excluded", "PA Manually Excluded"),
)

PENDING = "pending"
REJECTED = "rejected"
DONE = "done"
PROJECT_LIST_STATUS = ((PENDING, "pending"), (REJECTED, "rejected"), (DONE, "done"))

ACCOUNT_TYPE_CHOICES = (
    ("cash", "Cash"),
    ("other_accounts", "Other accounts"),
    ("procurement", "Procurement"),
    ("staff_cost", "Staff Costs"),
    ("no_budget", "No budget yet in HNIP"),
)

LOCATION_CHOICES = (
    ("site", "Site"),
    ("document", "Document"),
)


class Goal(AbstractModel):
    code = models.CharField(max_length=3, unique=True, null=False)
    description = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.code


class Situation(AbstractModel):
    code = models.CharField(max_length=25, unique=True, null=False)

    def __str__(self):
        return self.code


class Status(AbstractModel):
    code = models.CharField(max_length=25, unique=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.description


class Operation(AbstractModel):
    code = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.description


class Vendor(AbstractModel):
    code = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.description


class Output(AbstractModel):
    code = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.description


class AccountType(AbstractModel):
    description = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.description


class AccountCategory(AbstractModel):
    description = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.description


class Account(AbstractModel):
    code = models.CharField(max_length=25, unique=True, null=False)
    description = models.CharField(max_length=255, null=True)
    account_type = models.ForeignKey(AccountType, on_delete=models.CASCADE, null=True)
    account_category = models.ForeignKey(
        AccountCategory, on_delete=models.CASCADE, null=True
    )
    status = models.CharField(max_length=255, choices=ACCOUNT_STATUSES)
    open_item = models.CharField(max_length=255, choices=YES_NO)
    budgetary_only = models.CharField(max_length=255, choices=YES_NO)

    def __str__(self):
        return self.code


class Pillar(AbstractModel):
    code = models.CharField(max_length=1, unique=True, null=False)
    description = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.code


class AgreementType(AbstractModel):
    description = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.description


class OIOSBusinessUnitReport(AbstractModel):
    report_date = models.DateField(verbose_name="Report Date", null=True, default=None)
    business_unit = models.ForeignKey(
        BusinessUnit, on_delete=models.PROTECT, related_name="oios_report"
    )
    overall_rating = models.DecimalField(max_digits=5, decimal_places=4)

    def __str__(self):
        return "{} - {}".format(self.report_date, self.business_unit.code)


class PopulationPlanningGroup(AbstractModel):
    code = models.CharField(max_length=25, unique=True, null=False)
    description = models.CharField(max_length=255, unique=True, null=True)

    def __str__(self):
        return self.code


class AgreementMember(AbstractModel):

    TEAM_TYPE_CHOICES = (("field_work", "Field Work"), ("other", "Other"))

    user_type = models.CharField(max_length=255, choices=AGREEMENT_MEMBER_CHOICES)
    focal = models.BooleanField(null=True, default=False)
    alternate_focal = models.BooleanField(null=True, default=False)
    reviewer = models.BooleanField(null=True, default=False)

    team_type = models.CharField(
        choices=TEAM_TYPE_CHOICES, max_length=50, null=True, blank=True, default=None
    )
    role = models.CharField(max_length=255, null=True, blank=True, default=None)
    duration_spent_in_days = models.IntegerField(null=True, blank=True, default=None)
    tasks_completed = JSONField(default=list)
    profile_summary = models.TextField(null=True, blank=True, default=None)
    qualifications = models.TextField(null=True, blank=True, default=None)
    years_of_experience = models.IntegerField(null=True, blank=True, default=None)
    language = JSONField(default=list)
    clients_audited_example = JSONField(default=list)
    ISA_experience = models.TextField(null=True, blank=True, default=None)
    comments = models.TextField(null=True, blank=True, default=None)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="agreement_members",
    )

    def __str__(self):
        return self.user.fullname


class SenseCheck(AbstractModel):
    code = models.CharField(max_length=5)
    description = models.CharField(max_length=255)
    settings = JSONField(default=dict)

    def __str__(self):
        return self.code


class Comments(AbstractModel):
    user = models.ForeignKey(
        "account.User", null=True, default=None, on_delete=models.CASCADE
    )
    year = models.CharField(max_length=4, null=True, default=None, blank=True)
    description = models.TextField(null=True, default=None, blank=True)

    def set_comment(self, **kwargs):
        comment = self.__class__(**kwargs)
        comment.save()
        self.assign_comment(comment=comment)
        return comment

    @staticmethod
    def assign_comment(**kwargs):
        comment = kwargs.get("comment")
        aca = AccountCommentAssignment(assignee_group="preparer", comment=comment)
        aca.save()

    def __str__(self):
        return self.user.fullname


class Cycle(AbstractModel):
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    status = models.ForeignKey(Status, on_delete=models.PROTECT, null=True)
    changed_date = models.DateField(null=True, default=None)
    budget_ref = models.IntegerField(null=True, unique=True)
    budget_ref = models.IntegerField(null=True, unique=True)
    delta_enabled = models.BooleanField(default=False)
    field_assessment_deadline = models.DateField(null=True, blank=True, default=None)
    comments = models.ManyToManyField("common.Comment")

    def __str__(self):
        return "{}".format(self.year)


class Location(AbstractModel):
    location_type = models.CharField(
        max_length=200, choices=LOCATION_CHOICES, null=True, default=None
    )
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, null=True, default=None
    )
    city = models.CharField(max_length=750, null=True, default=None)
    address = models.TextField(null=True, default=None)
    is_valid = models.BooleanField(default=False, null=True)

    def __str__(self):
        return "{}-{}-{}".format(self.country, self.city, self.location_type)


class Agreement(AbstractModel):
    title = models.CharField(max_length=750, null=True)
    description = models.TextField(max_length=2000, null=True, blank=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)
    locations = models.ManyToManyField(Location, blank=True)
    agreement_type = models.ForeignKey(
        AgreementType, on_delete=models.CASCADE, null=True
    )
    partner = models.ForeignKey("partner.Partner", on_delete=models.CASCADE)
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.CASCADE, null=True)
    field_assessment = models.ForeignKey(
        "field.FieldAssessment",
        related_name="field_assessments",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    agreement_date = models.DateField(
        verbose_name="Agreement Date", null=True, blank=True
    )
    number = models.IntegerField()
    operation = models.ForeignKey(
        Operation, on_delete=models.CASCADE, max_length=50, null=True
    )
    pillar = models.ForeignKey(Pillar, on_delete=models.CASCADE, null=True)
    start_date = models.DateField(verbose_name="Start Date", null=True)
    end_date = models.DateField(verbose_name="Completion Date", null=True)
    budget = models.DecimalField(max_digits=10, decimal_places=2, default=0, null=True)
    cycle = models.ForeignKey(
        Cycle, on_delete=models.PROTECT, related_name="agreement_cycle", null=False
    )
    goals = models.ManyToManyField(Goal)
    cost_centers = models.ManyToManyField(CostCenter)
    ppg_s = models.ManyToManyField(PopulationPlanningGroup)
    situations = models.ManyToManyField(Situation)
    budget_ref = models.IntegerField(null=True, default=None, blank=True)
    installments_paid = models.DecimalField(
        max_digits=10, decimal_places=2, default=0, null=True
    )
    audit_firm = models.ForeignKey(
        AuditAgency, on_delete=models.PROTECT, null=True, blank=True
    )
    audit_worksplit_group = models.ForeignKey(
        AuditWorksplitGroup, on_delete=models.PROTECT, null=True, blank=True
    )
    focal_points = models.ManyToManyField(
        AgreementMember, related_name="focal_points", blank=True
    )
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, null=True, blank=True)
    pre_sense_check = models.BooleanField(null=True, blank=True, default=None)
    int_sense_check = models.BooleanField(null=True, blank=True, default=None)
    fin_sense_check = models.BooleanField(null=True, blank=True, default=None)
    pre_comment = models.ForeignKey(
        "common.Comment",
        on_delete=models.CASCADE,
        related_name="pre_comment",
        null=True,
        blank=True,
    )
    int_comment = models.ForeignKey(
        "common.Comment",
        on_delete=models.CASCADE,
        related_name="int_comment",
        null=True,
        blank=True,
    )
    fin_comment = models.ForeignKey(
        "common.Comment",
        on_delete=models.CASCADE,
        related_name="fin_comment",
        null=True,
        blank=True,
    )
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=True)
    risk_rating = models.DecimalField(
        max_digits=10, decimal_places=4, default=None, null=True, blank=True
    )
    audited_by_gov = models.BooleanField(default=False, null=True)
    delta = models.BooleanField(default=False, null=True)
    icq_report = models.ForeignKey(
        "icq.ICQReport",
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        related_name="icq_agreements",
        blank=True,
    )
    reason = models.ForeignKey(
        SenseCheck, on_delete=models.SET_NULL, null=True, default=None, blank=True
    )
    int_reason = models.ForeignKey(
        SenseCheck,
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        blank=True,
        related_name="int_reason",
    )
    fin_reason = models.ForeignKey(
        SenseCheck,
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        blank=True,
        related_name="fin_reason",
    )
    risk_rating_calc = JSONField(default=dict, blank=True)
    kpi2_sampled = models.BooleanField(default=False)
    is_auditable = models.BooleanField(default=True)

    @property
    def emergency(self):
        goals = self.goals.all().values_list("code", flat=True)
        if "PB" in goals and len(goals) == 1:
            return 1
        if "PB" in goals and len(goals) > 1:
            return 2
        else:
            return 0

    @property
    def get_emergency_test(self):
        emergency = self.emergency
        if emergency == 1:
            return 5
        elif emergency == 2:
            return 3
        else:
            return 0

    @property
    def has_pqp(self):
        from partner.models import PQPStatusNew

        if "UNHCR" in self.business_unit.code:
            pqp = PQPStatusNew.objects.filter(
                business_unit__code__contains="UNHCR",
                partner=self.partner,
                year=self.cycle.year,
            ).first()
        else:
            pqp = PQPStatusNew.objects.filter(
                business_unit=self.business_unit,
                partner=self.partner,
                year=self.cycle.year,
            ).first()
        print(pqp)
        if pqp is not None:
            return 1 if pqp.status else 0
        else:
            return 0

    def overall_pqp_status(self):
        from partner.models import PQPStatus

        qs = PQPStatus.objects.values_list("national_worldwide", flat=True).filter(
            Q(partner=self.partner)
            & (Q(national_worldwide="N") | Q(national_worldwide="W"))
        )
        result = 0
        if qs.exists():
            result = 1
        return result

    @property
    def procurement(self):
        ab = AgreementBudget.objects.filter(
            agreement_id=self.id, account_type="procurement"
        )
        res = ab.aggregate(budget=Sum("budget"))
        res = res.get("budget", 0)
        return 0 if not res else res

    @property
    def cash(self):
        ab = AgreementBudget.objects.filter(agreement_id=self.id, account_type="cash")
        res = ab.aggregate(budget=Sum("budget"))
        res = res.get("budget", 0)
        return 0 if not res else res

    @property
    def staff_costs(self):
        ab = AgreementBudget.objects.filter(
            agreement_id=self.id, account_type="staff_cost"
        )
        res = ab.aggregate(budget=Sum("budget"))
        res = res.get("budget", 0)
        return 0 if not res else res

    def get_oios_test(self, year):
        if "UNHCR" in self.business_unit.code:
            oios = (
                OIOSBusinessUnitReport.objects.filter(
                    business_unit__code__icontains="UNHCR", report_date__year=year
                )
            ).first()
        else:
            oios = (
                OIOSBusinessUnitReport.objects.filter(
                    business_unit=self.business_unit, report_date__year=year
                )
            ).first()
        if oios:
            return oios.overall_rating
        else:
            return 0

    @property
    def get_field_assessment_test(self):
        if self.field_assessment is not None:
            return self.field_assessment.score.value
        return 5

    @property
    def partner_modified_in_4_years_test(self):
        from partner.models import Modified

        year = datetime.datetime.now().year - 4
        if "UNHCR" in self.business_unit.code:
            return (
                Modified.objects.filter(
                    Q(partner=self.partner)
                    & Q(business_unit__code__contains="UNHCR")
                    & Q(year__gte=year)
                    & Q(modified=True)
                ).count()
                > 0
            )
        else:
            return (
                Modified.objects.filter(
                    Q(partner=self.partner)
                    & Q(business_unit=self.business_unit)
                    & Q(year__gte=year)
                    & Q(modified=True)
                ).count()
                > 0
            )
        # modified = ["qualified", "adverse", "disclaimer"]
        # if self.audit_report.overall_opinion in modified:
        #     return True
        # else:
        #     return False

    @property
    def get_icq_test(self):
        from icq.models import ICQReport

        year = datetime.datetime.now().year - 4
        if "UNHCR" in self.business_unit.code:
            icq_report = (
                ICQReport.objects.filter(
                    year__gte=year,
                    business_unit__code__icontains="UNHCR",
                    partner=self.partner,
                )
                .order_by("-year")
                .first()
            )
        else:
            icq_report = (
                ICQReport.objects.filter(
                    year__gte=year,
                    business_unit=self.business_unit,
                    partner=self.partner,
                )
                .order_by("-year")
                .first()
            )
        if icq_report is not None:
            return icq_report.overall_score_perc
        else:
            return 0

    @property
    def audit_test(self):
        partner_modified = self.partner_modified_in_4_years_test
        icq_score = self.get_icq_test

        if partner_modified == 1:
            return 5
        else:
            if partner_modified == 0 and icq_score <= 0:
                return 5
            elif partner_modified == 0 and (icq_score >= 0.4 and icq_score < 0.6):
                return 4
            elif partner_modified == 0 and (icq_score >= 0.6 and icq_score < 0.8):
                return 3
            elif partner_modified == 0 and (icq_score >= 0.8 and icq_score <= 1):
                return 2
            else:
                return 5

    @property
    def get_procurement_weight(self):
        if self.has_pqp:
            if self.procurement < 20000:
                return 1 - 1
            elif self.procurement >= 20000 and self.procurement < 50000:
                return 2 - 1
            elif self.procurement >= 50000 and self.procurement < 100000:
                return 3 - 1
            elif self.procurement >= 100000 and self.procurement < 150000:
                return 4 - 1
            else:
                return 5 - 1
        else:
            if self.procurement < 20000:
                return 1
            elif self.procurement >= 20000 and self.procurement < 50000:
                return 2
            elif self.procurement >= 50000 and self.procurement < 100000:
                return 3
            elif self.procurement >= 100000 and self.procurement < 150000:
                return 4
            else:
                return 5

    @property
    def procurement_test(self):
        return 0 if self.get_procurement_weight is None else self.get_procurement_weight

    @property
    def get_cash_test(self):
        cash = self.cash
        if cash < 20000:
            return 1
        elif cash >= 20000 and cash < 50000:
            return 2
        elif cash >= 50000 and cash < 100000:
            return 3
        elif cash >= 100000 and cash < 150000:
            return 4
        else:
            return 5

    @property
    def get_staffcost_test(self):
        staff_costs = self.staff_costs
        if staff_costs < 20000:
            return 1
        elif staff_costs >= 20000 and staff_costs < 50000:
            return 2
        elif staff_costs >= 50000 and staff_costs < 100000:
            return 3
        elif staff_costs >= 100000 and staff_costs < 150000:
            return 4
        else:
            return 5

    @property
    def get_staffcost_test(self):
        staff_costs = self.staff_costs
        if staff_costs < 20000:
            return 1
        elif staff_costs >= 20000 and staff_costs < 50000:
            return 2
        elif staff_costs >= 50000 and staff_costs < 100000:
            return 3
        elif staff_costs >= 100000 and staff_costs < 150000:
            return 4
        else:
            return 5

    @property
    def get_project_profile(self):
        settings = SelectionSettings.objects.filter(cycle=self.cycle).first()
        if settings is not None:
            return float(
                self.procurement_test * settings.procurement_perc
                + self.get_cash_test * settings.cash_perc
                + self.get_staffcost_test * settings.staff_cost_perc
                + self.get_emergency_test * settings.emergency_perc
            ) + (float(self.audit_test) * float(settings.audit_perc))
        else:
            return 0.00

    @property
    def get_risk_rating(self):
        project_profile = self.get_project_profile
        oios_test = float(self.get_oios_test(self.cycle.year))
        field_assessment_test = float(self.get_field_assessment_test)
        risk_rating = (project_profile + oios_test * 0.2) + (
            field_assessment_test * 0.25
        )
        response = {
            "risk_rating": risk_rating,
            "cash": float(self.get_cash_test),
            "goal": float(self.get_emergency_test),
            "staff_cost": float(self.get_staffcost_test),
            "oios_rating": float(self.get_oios_test(self.cycle.year)),
            "procurement": float(self.get_procurement_weight),
            "modified": float(self.partner_modified_in_4_years_test),
            "other_accounts": None,
            "pqp_worldwide": float(self.has_pqp),
            "overall_score_perc": float(self.get_icq_test),
            "value": float(self.get_field_assessment_test),
            "field_assessment_test": float(self.get_field_assessment_test),
        }
        return response

    def get_raw_query(self, year=None, **kwargs):
        return super().get_raw_query(year=year, **kwargs)

    def __str__(self):
        return "{}{} - {}-{}".format(
            "" if self.business_unit is None else self.business_unit.code,
            self.number,
            self.partner.number,
            self.partner.legal_name,
        )

    @property
    def filter_const_benefit():
        return 1

    class Meta:
        unique_together = (
            "business_unit",
            "number",
        )


class AgreementReport(AbstractModel):
    report_date = models.DateField(verbose_name="Report Date", null=True)
    agreement = models.ForeignKey(Agreement, on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True)
    base = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    installments = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    adjustments = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    cancellations = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    ipfr = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    refund = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    receivables = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    write_off = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    reported_by = models.DecimalField(max_digits=10, decimal_places=2, null=True)

    def __str__(self):
        return (
            "{}{}".format(self.agreement.business_unit.code, self.agreement.number)
            if self.agreement.business_unit is not None
            else self.agreement.number
        )


class AgreementBudget(AbstractModel):
    agreement = models.ForeignKey(
        Agreement,
        on_delete=models.CASCADE,
        null=False,
        related_name="related_agreement",
    )
    account_type = models.CharField(
        max_length=200, choices=ACCOUNT_TYPE_CHOICES, null=True, default=None
    )
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True)
    output = models.ForeignKey(Output, on_delete=models.CASCADE, null=True)
    pillar = models.ForeignKey(Pillar, on_delete=models.CASCADE, null=True)
    cost_center = models.ForeignKey(CostCenter, on_delete=models.CASCADE, null=True)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE, null=True)
    ppg = models.ForeignKey(
        PopulationPlanningGroup, on_delete=models.CASCADE, null=True
    )
    situation = models.ForeignKey(Situation, on_delete=models.CASCADE, null=True)
    budget = models.DecimalField(max_digits=20, decimal_places=2, null=True)
    report_date = models.DateField(null=True)

    def __str__(self):
        return (
            "{}{}-{}".format(
                self.agreement.business_unit.code, self.agreement.number, self.year
            )
            if self.agreement.business_unit is not None
            else "{}-{}".format(self.agreement.number, self.year)
        )


class CycleTimeRemindTemplate(AbstractModel):
    duration = models.IntegerField(null=True, default=0)
    name = models.CharField(max_length=255, null=True, default=None)
    description = models.CharField(max_length=255, null=True, default=None)

    def __str__(self):
        return "{} {} ".format(self.duration)


class SelectionSettings(AbstractModel):
    cycle = models.ForeignKey(Cycle, on_delete=models.PROTECT, null=False)
    risk_threshold = models.DecimalField(max_digits=10, decimal_places=3, default=0)

    procurement_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    cash_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    staff_cost_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    emergency_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    audit_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    oios_perc = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    field_assessment_perc = models.DecimalField(
        max_digits=10, decimal_places=3, default=0
    )

    oios_cutoff = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    cost_benefit = models.IntegerField(default=0, null=True)
    high_values = models.IntegerField(default=0, null=True)
    materiality = models.IntegerField(default=0, null=True)
    sense_check_icq = models.DecimalField(
        max_digits=6,
        decimal_places=4,
        default=0.0000,
        null=True,
        help_text="Recent icq score  be compared for the projects",
    )

    def __str__(self):
        return "{0}".format(self.cycle.year)


class AccountCommentAssignment(AbstractModel):
    assignee_group = models.CharField(max_length=16, null=True, default="", blank=True)
    assignee = models.ForeignKey(
        "account.User",
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name="assignee_account",
    )
    comment = models.ForeignKey(
        Comments, null=True, default=None, on_delete=models.CASCADE
    )

    def __str__(self):
        return self.assignee.user.fullname


"""
Message Model
 @message string
 @agreement int (foreign key of Aggrement model)
 @user int (foreign key of User model)

"""


class Message(AbstractModel):
    message = models.TextField(max_length=2000, null=True)
    agreement = models.ForeignKey(Agreement, on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.user.fullname


class PreviousAudit(AbstractModel):
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.CASCADE)
    partner = models.ForeignKey("partner.Partner", on_delete=models.CASCADE)
    agreement_number = models.IntegerField(null=True, default=None)
    year = models.IntegerField()
    audited = models.BooleanField(null=True, default=None)

    class Meta:
        unique_together = ("business_unit", "partner", "agreement_number", "year")

    def __str__(self):
        return "{}-{}-{}-{}".format(
            self.business_unit.code,
            self.partner.number,
            self.agreement_number,
            self.year,
        )


class CancelledAudit(AbstractModel):
    business_unit = models.ForeignKey(BusinessUnit, on_delete=models.CASCADE)
    partner = models.ForeignKey("partner.Partner", on_delete=models.CASCADE)
    agreement_number = models.IntegerField(null=True, default=None)
    year = models.IntegerField()
    reason = models.CharField(max_length=20, choices=AUDIT_CANCELATION_REASON_CHOICES)
    is_cancelled = models.BooleanField(null=True, default=None)

    class Meta:
        unique_together = ("business_unit", "partner", "agreement_number", "year")

    def __str__(self):
        return "{}{}".format(self.business_unit.code, self.partner.number)
