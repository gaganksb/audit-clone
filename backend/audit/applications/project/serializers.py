from rest_framework import serializers
from urllib import parse
from django.http import QueryDict
from rest_framework import exceptions
from project.models import (
    Goal,
    Agreement,
    AgreementType,
    AgreementReport,
    OIOSBusinessUnitReport,
    SelectionSettings,
    Cycle,
    SenseCheck,
    Comments,
    AgreementMember,
    Message,
    AgreementBudget,
    Operation,
    Status,
    Location,
    PreviousAudit,
    CancelledAudit,
)
from common.models import Comment, Country
from partner.serializers import (
    PartnerSerializer,
)
from auditing.serializers import AuditAgencySerializerDetails
from django.db.models import F
from project.models import Comments
from .helpers import fetch_focal_points
from project.filters import AgreementAssignmentFilter
from common.serializers import CommonUserSerializer
from .tasks import (
    assign_agreement_member_bgtask,
    assign_agreement_member,
    send_agreement_msg_notification,
)
from auditing.models import AuditWorksplitGroup
from common.helpers import filter_project_selection
from icq.serializers import ICQDownloadSerializer
from common.serializers import CommonUserSerializer


class GoalSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="projects:goal-detail")

    class Meta:
        model = Goal
        fields = "__all__"
        extra_fields = [
            "url",
        ]


class AgreementReportSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name="projects:agreementreport-detail"
    )

    class Meta:
        model = AgreementReport
        fields = "__all__"
        extra_fields = [
            "url",
        ]


class SenseCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = SenseCheck
        fields = "__all__"


class CommonCommentSerializer(serializers.ModelSerializer):
    user = CommonUserSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"


class OperationCode(serializers.ModelSerializer):
    class Meta:
        model = Operation
        fields = ["id", "code"]


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        if instance.country is not None:
            country = Country.objects.filter(id=instance.country.id).values(
                "id", "name"
            )
            serializer.update({"country": country[0]})
        else:
            serializer.update({"country": None})
        return serializer


class AgreementSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="projects:agreement-detail")
    partner = PartnerSerializer(read_only=True)
    country = serializers.ReadOnlyField(source="country.name")
    region = serializers.ReadOnlyField(source="country.region.name")
    pillar = serializers.ReadOnlyField(source="pillar.code")
    agreement_type = serializers.ReadOnlyField(source="agreement_type.description")
    business_unit = serializers.ReadOnlyField(source="business_unit.code")
    bu = serializers.ReadOnlyField(source="business_unit.id")
    field_office = serializers.ReadOnlyField(source="business_unit.field_office.id")
    audit_firm = AuditAgencySerializerDetails()
    agreement_member = serializers.SerializerMethodField()

    pre_comment = CommonCommentSerializer(read_only=True)
    int_comment = CommonCommentSerializer(read_only=True)
    fin_comment = CommonCommentSerializer(read_only=True)
    operation = OperationCode()
    icq_report = ICQDownloadSerializer()
    reason = SenseCheckSerializer(read_only=True)
    agreement_survey = serializers.SerializerMethodField()
    working_papers = serializers.SerializerMethodField()

    class Meta:
        model = Agreement
        fields = "__all__"
        extra_fields = [
            "url",
        ]

    def get_working_papers(self, instance):
        from quality_assurance.serializers import WorkingPaperSerializer
        serializer = WorkingPaperSerializer(instance.agreement_working_papers.all(), many=True)
        return serializer.data

    def get_agreement_survey(self, instance):
        from quality_assurance.serializers import AgreementSurveySmallSerializer

        serializer = AgreementSurveySmallSerializer(
            instance.agreement_surveys.all(), many=True
        )
        return serializer.data

    def get_agreement_member(self, obj):
        agreement_assignment = fetch_focal_points(obj)
        return agreement_assignment

    def to_representation(self, instance):
        agreement_report = {}
        project_icqs = []
        serializer = super().to_representation(instance)
        cycle = CycleSerializer(instance.cycle)
        overall_risk_rating = instance.risk_rating
        serializer.update({"overall_risk_rating": overall_risk_rating})
        agreement_report_qs = AgreementReport.objects.filter(agreement=instance)
        if agreement_report_qs.exists():
            fields = [
                "id",
                "receivables",
                "cancellations",
                "adjustments",
                "ipfr",
                "write_off",
                "refund",
            ]
            agreement_report = {
                k: v for k, v in agreement_report_qs.values()[0].items() if k in fields
            }
        serializer.update({"agreement_report": agreement_report, "cycle": cycle.data})

        locations = instance.locations.filter(is_valid=True)
        locations_serializer = LocationSerializer(locations, many=True)

        if instance.audit_firm is not None:
            project_icqs = (
                Agreement.objects.filter(
                    audit_firm_id=instance.audit_firm.id,
                    cycle__year=instance.cycle.year,
                    partner=instance.partner,
                    business_unit=instance.business_unit,
                )
                .annotate(agreement_id=F("id"))
                .values("agreement_id", "number", "business_unit__code")
            )
        serializer.update(
            {
                "locations": locations_serializer.data,
                "audited_by_gov": instance.audited_by_gov,
                "project_icqs": project_icqs,
            }
        )
        return serializer


class AgreementSerializerCRUD(serializers.ModelSerializer):
    class Meta:
        model = Agreement
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        cycle = CycleSerializer(instance.cycle)

        locations = instance.locations.filter(is_valid=True)
        locations_serializer = LocationSerializer(locations, many=True)

        serializer.update({"cycle": cycle.data, "locations": locations_serializer.data})
        return serializer


class AgreementTypeSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name="projects:agreementtype-detail"
    )

    class Meta:
        model = AgreementType
        fields = "__all__"
        extra_fields = [
            "url",
        ]


class OIOSBusinessUnitReportSerializer(serializers.ModelSerializer):
    business_unit_code = serializers.SerializerMethodField()

    def get_business_unit_code(self, obj):
        return obj.business_unit.code

    class Meta:
        model = OIOSBusinessUnitReport
        fields = "__all__"
        extra_fields = ["business_unit_code"]


class OIOSBusinessUnitReportCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OIOSBusinessUnitReport
        fields = "__all__"


class CycleStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = "__all__"


class CycleSerializer(serializers.ModelSerializer):
    status = CycleStatusSerializer(read_only=True)

    class Meta:
        model = Cycle
        exclude = (
            "id",
            "created_date",
            "last_modified_date",
            "user",
            "changed_date",
            "comments",
        )


class SelectionSettingsSerializer(serializers.ModelSerializer):
    cycle = CycleSerializer(read_only=True)

    class Meta:
        model = SelectionSettings
        fields = "__all__"

    def create(self, validated_data):
        request = self.context["request"]
        year = request.data.get("year", None)
        if year is None:
            raise exceptions.APIException(
                "year is a required field to create project selection settings"
            )
        else:
            cycle, created = Cycle.objects.get_or_create(year=year, budget_ref=year)
            selection_settings, created = SelectionSettings.objects.get_or_create(
                cycle=cycle, **validated_data
            )
        return selection_settings


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Cycle
        fields = "__all__"


class ErrorSerializer(serializers.Serializer):
    message = serializers.CharField(required=False, allow_blank=True, max_length=100)


class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        cycle_qs = Cycle.objects.filter(finallist__comment=instance).distinct("id")
        serializer.update({"cycle_lists": CycleSerializer(cycle_qs, many=True).data})
        return serializer


class CommentCycleStreamingSerializer(serializers.ModelSerializer):
    user = CommonUserSerializer()
    action = serializers.SerializerMethodField()

    class Meta:
        model = Comments
        fields = ["id", "action", "description", "created_date", "user"]


class AgreementAssignmentListSerializer(serializers.ModelSerializer):
    partner = PartnerSerializer()
    audit_firm = AuditAgencySerializerDetails()
    agreement_member = serializers.SerializerMethodField()
    url = serializers.HyperlinkedIdentityField(view_name="projects:agreement-detail")
    country = serializers.ReadOnlyField(source="country.name")
    pillar = serializers.ReadOnlyField(source="pillar.code")
    agreement_type = serializers.ReadOnlyField(source="agreement_type.description")
    business_unit = serializers.ReadOnlyField(source="business_unit.code")
    operation = OperationCode()

    class Meta:
        model = Agreement
        fields = "__all__"
        extra_fields = [
            "url",
        ]

    def get_agreement_member(self, obj):
        agreement_assignment = fetch_focal_points(obj)
        return agreement_assignment

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        business_unit = instance.business_unit
        ws_group = AuditWorksplitGroup.objects.filter(
            business_units=business_unit
        ).values_list("name", flat=True)
        ws_group_str = ", ".join(list(ws_group))
        serializer.update({"ws_group_list": ws_group_str})
        return serializer


class AssignmentMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgreementMember
        fields = "__all__"

    def to_representation(self, instance):
        response = {"id": None, "email": None, "fullname": None}
        if instance.user_type_role is not None:
            response["id"] = instance.user_type_role.id
            response["email"] = instance.user_type_role.user.email
            response["fullname"] = instance.user_type_role.user.fullname
            return response
        return response


class AgreementMemberListSerializer(serializers.ModelSerializer):
    user = CommonUserSerializer()

    class Meta:
        model = AgreementMember
        fields = "__all__"


class AgreementMemberUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgreementMember
        fields = "__all__"


class AgreementMemberSerializer(serializers.Serializer):
    agreements = serializers.ListField(child=serializers.IntegerField(), required=True)
    request_type = serializers.CharField(required=True)
    firm = serializers.IntegerField(required=False)
    focal = serializers.IntegerField(required=False)
    focal_alternate = serializers.IntegerField(required=False)
    filter_url = serializers.CharField(required=False)
    reviewers = serializers.ListField(child=serializers.IntegerField(), required=False)

    def create(self, validated_data):
        selected_agreement = None
        filter_url = validated_data.get("filter_url", None)
        firm = validated_data.get("firm", None)
        request_type = validated_data.get("request_type", None)
        budget_ref = self.context.get("view").kwargs.get("budget_ref")
        reviewers = validated_data.get("reviewers", [])
        request = self.context["request"]
        membership = request.user.membership
        year = self.context.get("view").kwargs.get("budget_ref")

        if filter_url is None:
            selected_agreement = validated_data["agreements"]
        else:
            # Filter based on the queryparams
            query_params = dict(parse.parse_qsl(parse.urlsplit(filter_url).query))
            query_dict = QueryDict("", mutable=True)
            query_dict.update(query_params)
            ## if request user is auditor admin then filter agreements from his own agency only
            if membership["type"] == "AUDITOR":
                queryset = Agreement.objects.filter(
                    cycle__year=budget_ref,
                    audit_firm_id=membership["audit_agency"]["id"],
                )
            else:
                queryset = Agreement.objects.filter(cycle__year=budget_ref)

            queryset = filter_project_selection(
                queryset, budget_ref, "fin", "included"
            )
            # A workaround to call the filter internally for batch project assignment
            data = AgreementAssignmentFilter(
                query_dict,
                queryset=queryset,
            )
            selected_agreement = data.qs.values_list("id", flat=True)
        if selected_agreement is not None and len(selected_agreement) > 0:
            if firm == 0 and membership["type"] == "HQ" and request_type == "unhcr":
                agreements_to_update = Agreement.objects.filter(
                    id__in=selected_agreement
                )
                agreements_to_update.update(audit_firm=None)
                return validated_data
            if len(selected_agreement) > 20:
                assign_agreement_member_bgtask(
                    validated_data,
                    list(selected_agreement),
                    filter_url,
                    membership,
                    year,
                )
            else:
                assign_agreement_member(
                    validated_data,
                    list(selected_agreement),
                    filter_url,
                    membership,
                    year,
                )
            return validated_data
        else:
            error = "No project aggrement found based on the given criteria"
            raise exceptions.APIException(error)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ["id", "message", "user", "created_date"]

    def create(self, validated_data):
        user = (
            validated_data.pop("user")
            if "user" in validated_data
            else self.context["request"].user
        )
        agreement_id = self.context.get("view").kwargs.get("pk", None)
        message = validated_data.get("message", None)

        agreement = Agreement.objects.filter(id=agreement_id)

        if agreement.exists() == False:
            error = "user or agreement is invalid please check"
            raise exceptions.APIException(error)
        message = Message(message=message, agreement=agreement.first(), user=user)
        message.save()

        # Send notification to agreement members
        agreement_id = agreement.first().id
        msg = message.message
        fullname = message.user.fullname
        send_agreement_msg_notification(agreement_id, msg, fullname)
        return message

    def to_representation(self, instance):
        user = {
            "id": instance.user.id,
            "email": instance.user.email,
            "fullname": instance.user.fullname,
        }
        data = {
            "id": instance.id,
            "message": instance.message,
            "created_date": instance.created_date,
            "user": user,
        }
        return data


class AgreementBudgetSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgreementBudget
        depth = 1
        exclude = ["user"]


class FOProgressSerializer(serializers.ModelSerializer):

    business_unit = serializers.ReadOnlyField(source="business_unit.code")
    progress = serializers.ReadOnlyField(source="status.code")
    year = serializers.ReadOnlyField(source="cycle.year")

    class Meta:
        model = Agreement
        fields = ["id", "business_unit", "progress", "year"]

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        return serializer


class PreliminaryListExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agreement
        fields = ["id"]

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        agreement = instance.agreement
        agreement_business_unit = agreement.business_unit.code
        agreement_country = agreement.country.name
        agreement_budget = agreement.budget
        agreement_installments_paid = agreement.installments_paid
        agreement_description = agreement.description
        agreement_number = agreement.number
        agreement_start_date = agreement.start_date
        agreement_end_date = agreement.end_date
        agreement_operation = agreement.operation

        if hasattr(instance.sense_check, "description") is True:
            sense_check = instance.sense_check
        else:
            sense_check = None

        partner = agreement.partner
        partner_legal_name = partner.legal_name
        partner_number = partner.number

        if hasattr(partner, "implementer_type") is True:
            partner_implementer_type = {
                "implementer_type": partner.implementer_type.implementer_type
            }
        else:
            partner_implementer_type = None

        serializer.update(
            {
                "agreement": {
                    "id": agreement.id,
                    "business_unit": agreement_business_unit,
                    "country": agreement_country,
                    "budget": agreement_budget,
                    "installments_paid": agreement_installments_paid,
                    "description": agreement_description,
                    "number": agreement_number,
                    "start_date": agreement_start_date,
                    "end_date": agreement_end_date,
                    "operation": agreement_operation,
                    "implementer_type": {"implementer_type": partner_implementer_type},
                    "partner": {
                        "id": partner.id,
                        "partner_legal_name": partner_legal_name,
                        "partner_number": partner_number,
                    },
                },
                "sense_check": sense_check,
            }
        )
        return serializer


class AuditorUnassignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agreement
        fields = ["id"]


class PreviousAuditSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreviousAudit
        fields = ["business_unit", "partner", "agreement_number", "year", "audited"]


class CancelledAuditSerializer(serializers.ModelSerializer):
    class Meta:
        model = CancelledAudit
        fields = [
            "business_unit",
            "partner",
            "agreement_number",
            "year",
            "reason",
            "is_cancelled",
        ]
