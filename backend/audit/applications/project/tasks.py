from django.db import transaction
from notify.models import NotifiedUser
from django.utils import timezone
from project.models import (
    Agreement,
    AgreementMember,
    SenseCheck,
    Cycle,
    SelectionSettings,
)
from account.models import User
from auditing.models import AuditAgency
from background_task import background
from django.core.management import call_command
from notify.helpers import send_notification, send_notification_to_users
from field.models import FieldMember
from unhcr.models import UNHCRMember
from django.db.models import Q
from .models import SenseCheck, SelectionSettings, Cycle

from common.consts import (
    PRE02,
    PRE03,
    FIN02,
    PRE01,
    INT01,
    FIN03,
    PRE02,
    FIN01,
    INT02,
    FIN01,
    INT01,
)
from django.conf import settings
from common.helpers import filter_project_selection
import tempfile
import csv
import os
from datetime import datetime
from field.models import FieldAssessmentProgress, FieldMember
from notify.helpers import send_email_with_attachment
from project.management.commands.project_agreement_import import RiskRatingData
from quality_assurance.models import TeamComposition, TeamCompositionUser
from project.helpers import sql_risk_rating

PA_MANUALLY_EXCLUDED = "SC12"

CMDS = [
    "agreement_assignment_notification",
    "audit_firm_selection_notification",
    "partner_audit_selection_notification",
]


@background(schedule=2)
def send_real_time_notification(agreement_member_ids, selected_agreement):
    for cmd in CMDS:
        selected_agreement = " ".join([str(i) for i in selected_agreement])
        if cmd == "agreement_assignment_notification":
            agreement_member_ids = " ".join([str(i) for i in agreement_member_ids])
            call_command(
                cmd,
                "--agreement_member_ids",
                agreement_member_ids,
                "--agreement_ids",
                selected_agreement,
            )
        else:
            call_command(cmd, "-ids", selected_agreement)


def create_team_composition(year):
    print("Creating team composition")
    cycle = Cycle.objects.filter(year=year).first()
    queryset = Agreement.objects.all()
    agreement_qs = filter_project_selection(queryset, year, "fin", "included")
    agencies = (
        agreement_qs.exclude(audit_firm__isnull=True)
        .distinct("audit_firm")
        .values_list("audit_firm", flat=True)
    )

    for agency in agencies:
        team_composition, created = TeamComposition.objects.get_or_create(
            audit_agency_id=agency, cycle=cycle
        )
        qs = Agreement.objects.filter(audit_firm_id=agency)
        focal_points = (
            qs.exclude(focal_points__isnull=True)
            .distinct("focal_points")
            .values_list("focal_points__id", flat=True)
        )
        for focal_point in focal_points:
            TeamCompositionUser.objects.create(
                team_composition=team_composition, agreement_member_id=focal_point
            )


def assign_agreement_member(
    validated_data, selected_agreement, filter_url, membership, year
):
    agreement_members = []

    firm = validated_data.get("firm", None)
    reviewers = validated_data.get("reviewers", [])
    focal_user = validated_data.get("focal", None)
    alternate_focal_user = validated_data.get("focal_alternate", None)
    if focal_user is not None:
        focal_user = User.objects.filter(is_active=True, id=focal_user)
        focal_agree, created = AgreementMember.objects.get_or_create(
            user_type=validated_data["request_type"],
            focal=True,
            user=focal_user.first(),
        )
        agreement_members.append(focal_agree)

    if alternate_focal_user is not None:
        alternate_focal_user = User.objects.filter(
            is_active=True, id=alternate_focal_user
        )

        alt_focal_agree, created = AgreementMember.objects.get_or_create(
            user_type=validated_data["request_type"],
            alternate_focal=True,
            user=alternate_focal_user.first(),
        )
        agreement_members.append(alt_focal_agree)

    for agreement in selected_agreement:
        agreement_obj = Agreement.objects.get(id=agreement)

        # Add audit firm
        if firm is not None:
            audit_firm = AuditAgency.objects.filter(id=firm)
            if audit_firm.exists():
                agreement_obj.audit_firm = audit_firm.first()

        # Check if focal exists
        if focal_user is not None:
            existing_focal = agreement_obj.focal_points.filter(
                user_type=validated_data["request_type"], focal=True
            )
            if existing_focal.exists() == True:
                agreement_obj.focal_points.remove(existing_focal.first())
                agreement_obj.focal_points.add(focal_agree)
            else:
                agreement_obj.focal_points.add(focal_agree)

        if alternate_focal_user is not None:
            # Check if alternate focal exists
            existing_focal_alt = agreement_obj.focal_points.filter(
                user_type=validated_data["request_type"], alternate_focal=True
            )
            if existing_focal_alt.exists() == True:
                agreement_obj.focal_points.remove(existing_focal_alt.first())
                agreement_obj.focal_points.add(alt_focal_agree)
            else:
                agreement_obj.focal_points.add(alt_focal_agree)

        if len(reviewers) > 0:
            old_reviewers = agreement_obj.focal_points.filter(
                user_type=validated_data["request_type"], reviewer=True
            )
            for reviewer in old_reviewers:
                agreement_obj.focal_points.remove(reviewer)

            for reviewer in reviewers:
                reviewer_user = User.objects.filter(is_active=True, id=reviewer)
                if reviewer_user.exists():
                    reviewer_agree, created = AgreementMember.objects.get_or_create(
                        user_type=validated_data["request_type"],
                        reviewer=True,
                        user=reviewer_user.first(),
                    )
                    agreement_obj.focal_points.add(reviewer_agree)
        agreement_obj.last_modified_date = timezone.now()
        agreement_obj.save()


@background(schedule=10)
def assign_agreement_member_bgtask(
    validated_data, selected_agreement, filter_url, membership, year
):
    assign_agreement_member(
        validated_data, selected_agreement, filter_url, membership, year
    )


@background(schedule=10)
def notify_project_modification(validated_data, selected_agreement):
    pass


@background
def send_project_selection_notification(year, request_status):
    cycle = Cycle.objects.filter(year=year).first()
    if request_status == INT01:
        """
        Send notification to field office approver, reviewer and hq users that an action is required
        from their end in Interim phase
        """
        # Field office Notification
        queryset = Agreement.objects.filter(cycle__year=year)
        if cycle.delta_enabled:
            queryset = queryset.exclude(fin_sense_check=True)
        field_office = (
            queryset
            .distinct("business_unit__field_office")
            .values("business_unit__field_office")
        )
        user_id = FieldMember.objects.filter(field_office_id__in=field_office).values(
            "user_id"
        )
        users = User.objects.filter(id__in=user_id)

        if users.exists():

            send_notification(
                notification_type="finalization_of_preliminary_list",
                obj=users.first(),
                users=users,
                context={"year": year, "subject": "Finalization of Preliminary List"},
            )

            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Finalization of Preliminary List",
                        sent_as_email=False,
                    )
                    send_notification_to_users(user)
            except Exception as e:
                print("Error while sending sent_project_selection_notification ", e)

    elif request_status == FIN01:
        notification_names = ["End of Interim HQ", "End of Interim FO"]

        # send notification to hq
        user_id = UNHCRMember.objects.all().values_list("user_id", flat=True)
        unhcr_users = User.objects.filter(id__in=user_id)
        if unhcr_users.exists():
            send_notification(
                notification_type="end_of_interim_hq",
                obj=unhcr_users.first(),
                users=unhcr_users,
                context={"year": year, "subject": "End of Interim HQ"},
            )

        # send notification to field_office
        queryset = Agreement.objects.filter(cycle__year=year)
        if cycle.delta_enabled:
            queryset = queryset.exclude(fin_sense_check=True)

        field_office = (
            queryset
            .distinct("business_unit__field_office")
            .values("business_unit__field_office")
        )
        user_id = FieldMember.objects.filter(field_office_id__in=field_office).values(
            "user_id"
        )
        field_users = User.objects.filter(id__in=user_id)
        if field_users.exists():
            send_notification(
                notification_type="end_of_interim_fo",
                obj=field_users.first(),
                users=field_users,
                context={"year": year, "subject": "End of Interim FO"},
            )

        try:
            user = NotifiedUser.objects.filter(
                notification__name__in=notification_names, sent_as_email=False
            )
            send_notification_to_users(user)
        except Exception as e:
            print("Error while sending sent_project_selection_notification ", e)

        agreements = Agreement.objects.filter(cycle__year=year)
        if cycle.delta_enabled:
            agreements = agreements.exclude(fin_sense_check=True)
        int_selected_agreement = filter_project_selection(
            agreements, year, "int", "included"
        )
        for agreement in int_selected_agreement:
            users = agreement.focal_points.filter(user_type="partner").values_list(
                "user_id", flat=True
            )
            if users.exists():
                partner_users = User.objects.filter(id__in=users)

                context = {
                    "title": agreement.title,
                    "business_unit__name": agreement.business_unit.name,
                    "number": agreement.number,
                    "subject": "End of Interim Partner",
                }

                send_notification(
                    notification_type="end_of_interim_partner",
                    obj=partner_users.first(),
                    users=partner_users,
                    context=context,
                )

                try:
                    with transaction.atomic():
                        user = NotifiedUser.objects.select_for_update().filter(
                            notification__name="End of Interim Partner",
                            sent_as_email=False,
                        )
                        send_notification_to_users(user)
                except Exception as e:
                    print("Error while sending sent_project_selection_notification ", e)

    elif request_status == FIN03:
        # send notification to hq
        user_id = UNHCRMember.objects.all().values_list("user_id", flat=True)
        unhcr_users = User.objects.filter(id__in=user_id)
        if unhcr_users.exists():
            send_notification(
                notification_type="end_of_final_list_hq",
                obj=unhcr_users.first(),
                users=unhcr_users,
                context={"year": year, "subject": "End of Final List HQ"},
            )

            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="End of Final List HQ", sent_as_email=False
                    )
                    send_notification_to_users(user)
            except Exception as e:
                print("Error while sending sent_project_selection_notification ", e)

        agreements = Agreement.objects.filter(cycle__year=year)
        if cycle.delta_enabled:
            agreements = agreements.exclude(fin_sense_check=True)
        fin_selected_agreement = filter_project_selection(
            agreements, year, "fin", "included"
        )

        for agreement in fin_selected_agreement:
            users = agreement.focal_points.filter(user_type="partner").values_list(
                "user_id", flat=True
            )
            if users.exists():
                partner_users = User.objects.filter(id__in=users)

                protocol = "http" if settings.DEBUG else "https"
                invitation_link = f"{protocol}://{settings.FRONTEND_HOST}"

                context = {
                    "title": agreement.title,
                    "business_unit__name": agreement.business_unit.name,
                    "number": agreement.number,
                    "subject": "End of Final Partner",
                    "invitation_link": invitation_link,
                }

                send_notification(
                    notification_type="end_of_final_list_partner",
                    obj=partner_users.first(),
                    users=partner_users,
                    context=context,
                )

                try:
                    with transaction.atomic():
                        user = NotifiedUser.objects.select_for_update().filter(
                            notification__name="End of Final Partner",
                            sent_as_email=False,
                        )
                        send_notification_to_users(user)
                except Exception as e:
                    print("Error while sending sent_project_selection_notification ", e)


@background(schedule=1)
def risk_rating_update(year):
    agreements = Agreement.objects.filter(cycle__year=year)
    for agreement in agreements:
        risk_rating = sql_risk_rating(agreement)
        agreement.risk_rating = risk_rating
        agreement.save()


@background(schedule=1)
def notify_interim_completed_reviewer(field_office_id):
    """
    this notification is sent when the Field Office Reviewer of a business unit completes the
    Interim assessment as Field Office reviewer and send it for approval to Field Office Approver
    """
    field_members = FieldMember.objects.filter(
        field_office_id=field_office_id, role__role="APPROVER"
    )
    field_users = User.objects.filter(
        id__in=field_members.values_list("user_id", flat=True)
    )
    if field_users.exists():
        send_notification(
            notification_type="interim_completed_reviewer",
            obj=field_users.first(),
            users=field_users,
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Interim List Completed From FO Reviewer",
                    sent_as_email=False,
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)


@background(schedule=1)
def send_agreement_msg_notification(agreement_id, message, user_fullname):
    email_body = """
    Message from: {0} on project agreement: {1}

    {2}
    """.format(
        user_fullname, agreement_id, message
    )

    agreement = Agreement.objects.get(id=agreement_id)
    agreement_members = agreement.focal_points.all().values_list("user_id", flat=True)
    users = User.objects.filter(id__in=agreement_members)
    if users.exists():
        send_notification(
            notification_type="custom_msg",
            obj=agreement,
            users=users,
            context={
                "custom_msg": email_body,
                "subject": "Agreement Message",
            },
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Agreement Message", sent_as_email=False
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)


@background(schedule=3)
def agreement_list_export(year, report_type, user_id):

    fields = [
        "business_unit__code",
        "number",
        "partner__number",
        "country__name",
        "country__region__name",
        "country__code",
        "partner__number",
        "partner__legal_name",
        "partner__implementer_type__implementer_type",
        "start_date",
        "end_date",
        "agreement_type",
        "audit_worksplit_group__name",
        "budget",
        "installments_paid",
        "audit_firm__legal_name",
        "field_assessment__score__value",
        "risk_rating",
        "pre_sense_check",
        "int_sense_check",
        "fin_sense_check",
        "pre_comment__message",
        "int_comment__message",
        "fin_comment__message",
        "reason__description",
        "risk_rating_calc__cash",
        "risk_rating_calc__goal",
        "risk_rating_calc__staff_cost",
        "risk_rating_calc__oios_rating",
        "risk_rating_calc__procurement",
        "risk_rating_calc__modified",
        "risk_rating_calc__other_accounts",
        "risk_rating_calc__pqp_worldwide",
        "risk_rating_calc__overall_score_perc",
        "risk_rating_calc__value",
        "status__code",
        "audited_by_gov",
    ]

    excel_fields = [
        "bu_number",
        "BU_partner_id",
        "business_unit__code",
        "country__name",
        "country__region__name",
        "partner__number",
        "partner__legal_name",
        "partner__implementer_type__implementer_type",
        "number",
        "start_date",
        "end_date",
        "agreement_type",
        "audit_worksplit_group__name",
        "budget",
        "installments_paid",
        "audit_firm__legal_name",
        "doc_location",
        "site_location",
        "unhcr_focal",
        "unhcr_alternate_focal",
        "partner_focal",
        "partner_alternate_focal",
        "field_assessment__score__value",
        "risk_rating",
        "pre_sense_check",
        "int_sense_check",
        "fin_sense_check",
        "pre_comment__message",
        "int_comment__message",
        "fin_comment__message",
        "reason__description",
        "risk_rating_calc__cash",
        "risk_rating_calc__goal",
        "risk_rating_calc__staff_cost",
        "risk_rating_calc__oios_rating",
        "risk_rating_calc__procurement",
        "risk_rating_calc__modified",
        "risk_rating_calc__other_accounts",
        "risk_rating_calc__pqp_worldwide",
        "risk_rating_calc__overall_score_perc",
        "risk_rating_calc__value",
        "status__code",
        "audited_by_gov",
    ]

    excel_col_labels = [
        "BU+PPA ID",
        "BU+Partner ID",
        "Business Unit",
        "Country Name",
        "Region Name",
        "Implementer Number",
        "Partner Name",
        "Implementer Type",
        "Agreement Number",
        "Project Start Date",
        "Project Implementation/ Completion Date",
        "Agreement Type",
        "Audit Worksplit Group",
        "Total Budget - (Agreement) - USD",
        "Total Installments Paid to Partners - USD",
        "Assigned Auditors",
        "Location of Project Documents for Auditor's Field Visit  (Name of City and Country)",
        "Location of Project Implementation Sites (Name of City and Country)",
        "E-mail of UNHCR Audit Focal Person",
        "E-mail of UNHCR Alternate Audit Focal Person",
        "E-mail & Telephone Number of Partner's Audit Focal Person",
        "E-mail &Telephone Number of Partner's Alternate Audit Focal Person",
        "Field Assessment",
        "Risk Rating",
        "Pre Check",
        "Int Check",
        "Fin Check",
        "Pre Comment",
        "Int Comment",
        "Fin Comment",
        "Reason for Selection",
        "Cash",
        "Emergency Goal",
        "Staff Costs",
        "OIOS",
        "Procurement",
        "Modified in Last 4 Yrs",
        "Other Accounts",
        "PQP Status",
        "Latest ICQ (Last 4yrs)",
        "Partner's Performance Assessment by Field Operations",
        "Status",
        "Audit By Gov",
    ]

    temp_dir = tempfile.gettempdir()
    file_name = datetime.strftime(datetime.now(), "%d-%m-%Y_%H:%M:%S.csv")
    file_path = os.path.join(temp_dir, file_name)

    fo_ids = FieldAssessmentProgress.objects.filter(
        pending_with="COMPLETED"
    ).values_list("field_office_id", flat=True)
    queryset = Agreement.objects.filter(
        ~Q(field_assessment__score__id=4),
        business_unit__field_office_id__in=fo_ids
    )
    cycle_qs = Cycle.objects.filter(year=year)
    cycle_status = cycle_qs.first().status.id
    agreements = queryset.filter(
        cycle__status_id__in=range(1, cycle_status + 1), cycle__year=year
    )

    if cycle_status in [PRE01, PRE02, PRE03]:
        fields.remove("int_comment__message")
        fields.remove("fin_comment__message")
        fields.remove("int_sense_check")
        fields.remove("fin_sense_check")
        excel_fields.remove("int_comment__message")
        excel_fields.remove("fin_comment__message")
        excel_fields.remove("int_sense_check")
        excel_fields.remove("fin_sense_check")
        excel_col_labels.remove("Int Comment")
        excel_col_labels.remove("Int Check")
        excel_col_labels.remove("Fin Comment")
        excel_col_labels.remove("Fin Check")

    elif cycle_status in [INT01, INT02]:
        fields.remove("fin_comment__message")
        fields.remove("fin_sense_check")
        excel_fields.remove("fin_comment__message")
        excel_fields.remove("fin_sense_check")
        excel_col_labels.remove("Fin Comment")
        excel_col_labels.remove("Fin Check")

    with open(file_path, "w", newline="", encoding="utf-8") as fp:
        writer = csv.writer(fp)
        writer.writerow(excel_col_labels)

        for row in agreements.values(*fields):
            bu_number = "{0}{1}".format(row["business_unit__code"], row["number"])
            BU_partner_id = "{0}{1}".format(
                row["business_unit__code"], row["partner__number"]
            )
            agrr = Agreement.objects.get(
                business_unit__code=row["business_unit__code"], number=row["number"]
            )

            documents = agrr.locations.filter(location_type="document").values("city", "country__name")
            document = ", ".join(["{0} - {1}".format(i['city'], i['country__name']) for i in documents])

            sites = agrr.locations.filter(location_type="site").values("city", "country__name")
            site = ", ".join(["{0} - {1}".format(i['city'], i['country__name']) for i in sites])

            unhcr_focal = agrr.focal_points.filter(
                user_type="unhcr", focal=True
            ).values_list("user__email", flat=True)
            unhcr_focal = ";".join(unhcr_focal)
            unhcr_alternate_focal = agrr.focal_points.filter(
                user_type="unhcr", alternate_focal=True
            ).values_list("user__email", flat=True)
            unhcr_alternate_focal = ";".join(unhcr_alternate_focal)
            partner_focal = agrr.focal_points.filter(
                user_type="partner", focal=True
            ).values_list("user__email", flat=True)
            partner_focal = ";".join(partner_focal)
            partner_alternate_focal = agrr.focal_points.filter(
                user_type="partner", alternate_focal=True
            ).values_list("user__email", flat=True)
            partner_alternate_focal = ";".join(partner_alternate_focal)

            row.update(
                {
                    "doc_location": document,
                    "site_location": site,
                    "unhcr_focal": unhcr_focal,
                    "unhcr_alternate_focal": unhcr_alternate_focal,
                    "partner_focal": partner_focal,
                    "partner_alternate_focal": partner_alternate_focal,
                    "bu_number": bu_number,
                    "BU_partner_id": BU_partner_id,
                }
            )

            row_data = [row[i] for i in excel_fields]
            writer.writerow(row_data)

    user_qs = User.objects.filter(id=user_id)
    if user_qs.exists():
        attachment = {}
        attachment["file_path"] = file_path

        send_notification(
            notification_type="agreement_list_export",
            obj=user_qs.first(),
            users=user_qs,
            context={
                "year": year,
                "subject": "Agreement List Export",
            },
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Agreement List Export", sent_as_email=False
                )
                send_email_with_attachment(user, [attachment])
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)
    os.remove(file_path)


@background(schedule=2)
def import_canceled_audits(file_path):
    cancel_audits = RiskRatingData(file_path)
    cancel_audits.import_cancelled_audit()
    os.remove(file_path)
