pre_stats_budget = """
select risk_status, sum(project_budget) from 
	(
		select pab.agreement_id,
			pc.year,
			sum(pab.budget) as project_budget,
			pa.pre_sense_check,
			pa.risk_rating,
			case when pa.risk_rating is null or pa.risk_rating >= %(risk_rating)d or pa.pre_sense_check in (select id from project_sensecheck ps where ps.code != 'SC12')
				then 
					'included'
				else 
					'excluded'
			end as risk_status
		from
			project_agreementbudget pab
		left join project_agreement as pa on pa.id = pab.agreement_id 
		left join project_cycle as pc on pa.cycle_id = pc.id
		where pc.year = %(year)d
		group by pab.agreement_id, pa.risk_rating, pc.year, pa.pre_sense_check 
	) as project_aggregation
group by risk_status;
"""

pre_stats_summary = """
select sum(excluded) as excluded ,
	   sum(included) as included 
from (
	select pa.id,
		   count(case when pa.risk_rating is null or risk_rating >= %(risk_rating)d or pa.pre_sense_check in (select id from project_sensecheck ps where ps.code != 'SC12') then 1 else null end) as included,
		   count(case when pa.risk_rating is null or risk_rating >= %(risk_rating)d or pa.pre_sense_check in (select id from project_sensecheck ps where ps.code != 'SC12') then 1 else null end) as included,
		   count(case when risk_rating < %(risk_rating)d then 1 else null end) as excluded
	from project_agreement pa
	inner join project_cycle as pc on pc.id = pa.cycle_id
	where pc.year=%(year)d
	group by pa.id
) as stats;
"""
