import os
import json
from django.db import models
from django.apps import apps
from django.http import HttpResponseRedirect, Http404, request
from django.urls import reverse_lazy
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework import status
from django.db.models import Q, F, Sum
from .filters import *
from rest_framework import filters
from django.contrib.contenttypes.models import ContentType
import csv
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db import transaction
from project.tasks import (
    send_project_selection_notification,
    notify_interim_completed_reviewer,
    agreement_list_export,
    import_canceled_audits,
)
from icq.tasks import create_icq_report

from project.models import (
    Goal,
    Agreement,
    AgreementType,
    AgreementReport,
    OIOSBusinessUnitReport,
    SelectionSettings,
    Cycle,
    SenseCheck,
    AgreementMember,
    Message,
    AgreementBudget,
    CancelledAudit,
)
from project.serializers import *
from field.tasks import update_risk_rating_bg
from field.models import FieldAssessmentProgress, FieldMember
from common.permissions import IsUNHCR, IsFO, IsAUDIT, IsPartnerAdmin, IsAuditAdmin
from .permissions import AgreementPermission, IsAgreementMember
from account.models import User
from utils.rest.code import code
from django_filters.rest_framework import DjangoFilterBackend
from common.models import Comment
from project.serializers import FOProgressSerializer
from common.helpers import filter_role_based_agreements, filter_project_selection

from common.consts import PRE01, PRE02, PRE03, INT01, INT02, FIN01, FIN02, FIN03
import tempfile

PA_MANUALLY_EXCLUDED = "SC12"


class GoalList(generics.ListCreateAPIView):
    queryset = Goal.objects.all()
    serializer_class = GoalSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class GoalDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Goal.objects.all()
    serializer_class = GoalSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class AgreementList(generics.ListAPIView):
    serializer_class = AgreementSerializer
    queryset = Agreement.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = AgreementFilter

    ordering_fields = [
        "risk_rating",
        "reason",
        "business_unit__code",
        "number",
        "country",
        "partner__legal_name",
        "partner__number",
        "budget",
        "installments_paid",
        "description",
        "partner__implementer_type__implementer_type",
        "start_date",
        "end_date",
        "agreement_type",
        "agreement_date",
        "operation",
    ]

    def get_queryset(self):
        membership = self.request.user.membership
        year = self.request.query_params.get("year", None)
        cycle = Cycle.objects.filter(year=year).first()
        if cycle is not None:
            fo_ids = FieldAssessmentProgress.objects.filter(
                pending_with="COMPLETED", budget_ref=year
            ).values_list("field_office_id", flat=True)
            if cycle.delta_enabled:
                queryset = Agreement.objects.filter(
                    ~Q(field_assessment__score__id=4)
                ).order_by("id")
            else:
                queryset = Agreement.objects.filter(
                    ~Q(field_assessment__score__id=4), business_unit__field_office_id__in=fo_ids
                ).order_by("id")

            if membership.get("type") == "FO":
                return queryset.filter(
                    business_unit__field_office_id=membership["field_office"]["id"]
                )
            else:
                return queryset
        else:
            return self.queryset.none()


class LocationsAPIView(generics.ListCreateAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    permission_classes = (IsAuthenticated,)


class LocationAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        try:
            agreement_id = kwargs["agreement_id"]
            agreement = self.queryset.get(id=agreement_id)
            locations = request.data.get("locations")
            for location in locations:
                location_obj = Location.objects.get(id=location)
                location_obj.is_valid = True
                location_obj.save()
                agreement.locations.add(location_obj)
                agreement.save()
            serializer = self.serializer_class(agreement, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception as e:
            response = {"message": "Error while updating location"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class LocationDeleteAPIView(generics.DestroyAPIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    permission_classes = (IsAuthenticated,)


class ModifyAPIView(generics.ListAPIView, generics.UpdateAPIView):
    queryset = Agreement.objects.all()
    serializer_class = CycleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    def _get_or_create_comments(self, **kwargs):
        content_type = ContentType.objects.filter(
            app_label="project", model="agreement"
        )
        try:
            comment_qs = Comment.objects.filter(
                content_type=content_type.first(),
                comment_type=kwargs["list_type"],
                object_id=kwargs["agreement_id"],
            )
            if comment_qs.exists():
                return (False, comment_qs.first())
            else:
                comment = Comment.objects.create(
                    message=kwargs["comment"],
                    user=self.request.user,
                    object_id=kwargs["agreement_id"],
                    content_type=content_type.first(),
                    comment_type=kwargs["list_type"],
                )
                return (True, comment)
        except Exception as e:
            print(e)
            return (False, False)

    def patch(self, request, *args, **kwargs):
        sense_check = request.data.get("sense_check")
        membership = request.user.membership
        request_comment = request.data.get("comment")
        list_type = request.data.get("list_type")
        agreement_id = request.data.get("agreement_id")
        action_type = request.data.get("action_type")
        year = kwargs["year"]

        if list_type in ["PRE", "FIN"] and membership["type"] != "HQ":
            raise exceptions.PermissionDenied(_(code["E_PERMISSION_DENY"]))
        if list_type == "INT" and membership["type"] != "FO":
            raise exceptions.PermissionDenied(_(code["E_PERMISSION_DENY"]))
        if (action_type == "include" and sense_check == 12) or (
            action_type == "exclude" and sense_check != 12
        ):
            raise exceptions.PermissionDenied(
                "Invalid sense check for the given action type"
            )

        agreement_qs = self.queryset.filter(id=agreement_id)
        if agreement_qs.exists():
            created, comment = self._get_or_create_comments(**request.data)
            if created == False and comment == False:
                return Response(
                    {"results": "Error while creating comment"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if created == False:
                comment.message = request_comment
                comment.save()

            if list_type == "PRE":
                agreement_qs.update(pre_comment_id=comment, pre_sense_check=sense_check)
            elif list_type == "INT":
                agreement_qs.update(int_comment_id=comment, int_sense_check=sense_check)
            elif list_type == "FIN":
                agreement_qs.update(fin_comment_id=comment, fin_sense_check=sense_check)
            else:
                return Response({"res": "error"}, status=status.HTTP_400_BAD_REQUEST)
            return Response({"res": "success"}, status=status.HTTP_200_OK)
        return Response(
            {"res": "Invalid project agreement id"}, status=status.HTTP_400_BAD_REQUEST
        )

    def put(self, request, *args, **kwargs):
        error = "Put method now allowed"
        raise exceptions.APIException(error)


class PromoteProjectAPIView(generics.UpdateAPIView):

    queryset = Cycle.objects.all()
    serializer_class = CycleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        request_status = request.data.get("status")
        membership = request.user.membership
        request_comment = request.data.get("comment")
        request_type = request.data.get("request_type")
        list_type = request.data.get("list_type")
        year = kwargs["year"]

        permissions = {
            "HQ_PREPARER": [
                PRE02,
            ],
            "HQ_REVIEWER": [
                PRE03,
                FIN02,
                PRE01,
            ],
            "HQ_APPROVER": [INT01, FIN03, PRE02, FIN01],
            "FO_REVIEWER": [INT02],
            "FO_APPROVER": [FIN01, INT01],
        }

        user_role = "{0}_{1}".format(membership.get("type"), membership.get("role"))
        permission_check = permissions.get(user_role)

        # Check if the right user is making right call
        if permission_check is None or request_status not in permission_check:
            raise exceptions.PermissionDenied(_(code["E_PERMISSION_DENY"]))

        cycle = Cycle.objects.filter(year=year)
        status = Status.objects.filter(id=request_status)
        if cycle.exists() and status.exists():
            content_type = ContentType.objects.filter(
                app_label="project", model="cycle"
            )
            cycle = cycle.first()
            status = status.first()

            agreement = cycle.agreement_cycle.all()
            if cycle.delta_enabled:
                agreement = agreement.exclude(fin_sense_check=True)
            if membership.get("type") == "FO":
                agreement = agreement.filter(
                    business_unit__field_office_id=membership["field_office"]["id"]
                )
                json_data = {
                    "change_type": "{0} to {1}".format(
                        agreement.first().status.code, status.code
                    ),
                    "list_type": list_type,
                }
            else:
                prev_status = int(request_status) - 1
                if (
                    request_status == 6
                    and membership.get("type") == "HQ"
                    and membership.get("role") == "APPROVER"
                ):
                    agreement = agreement.filter(cycle__status_id=7)
                    cycle.status = status
                else:
                    agreement = agreement.filter(cycle__status_id=prev_status)
                json_data = {
                    "change_type": "{0} to {1}".format(cycle.status.code, status.code),
                    "list_type": list_type,
                }

            comment = Comment.objects.create(
                message=request_comment,
                user=self.request.user,
                content_type=content_type.first(),
                comment_type=json_data,
            )
            cycle.comments.add(comment)
            if request_status not in [5, 6]:
                cycle.status = status
            if request_status == FIN03:
                _agreement = agreement.values(
                    "partner__number", "business_unit__code", "cycle__year"
                ).distinct()
                agreements = list(_agreement)
                create_icq_report(
                    agreements
                )  # Generate icq report for finalized projects
            if status.id == 4:
                agreement.update(
                    status_id=status,
                    int_sense_check=F("pre_sense_check"),
                    int_comment_id=F("pre_comment_id"),
                )
            elif status.id == 6:
                agreement.update(
                    status_id=status,
                    fin_sense_check=F("fin_sense_check"),
                    fin_comment_id=F("int_comment_id"),
                )
            else:
                agreement.update(status_id=status)
            cycle.save()

            if request_status == INT01 and request_type == "promote":
                send_project_selection_notification(year, request_status)
            if (
                request_status == INT02
                and request_type == "promote"
                and membership["type"] == "FO"
            ):
                notify_interim_completed_reviewer(membership["field_office"]["id"])
            elif (
                request_status == FIN01
                and request_type == "promote"
                and membership["type"] == "HQ"
            ):
                send_project_selection_notification(year, request_status)
            elif request_status == FIN03 and request_type == "promote":
                ## Enable Delta workflow and send notification
                cycle.delta_enabled = True
                cycle.save()
                send_project_selection_notification(year, request_status)

            return Response({"res": "success"}, status=200)
        return Response({"res": "Invalid Cycle"}, status=400)

    def patch(self, request, *args, **kwargs):
        error = "Put method now allowed"
        raise exceptions.APIException(error)

    def put(self, request, *args, **kwargs):
        error = "Put method now allowed"
        raise exceptions.APIException(error)


class PromoteToFinal(generics.CreateAPIView):
    queryset = Cycle.objects.all()
    serializer_class = CycleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def post(self, request, *args, **kwargs):
        request_comment = request.data.get("comment")
        list_type = "FIN01"
        year = kwargs["year"]

        cycle = Cycle.objects.filter(year=year)
        status = Status.objects.filter(id=6)
        if cycle.exists() and status.exists():
            content_type = ContentType.objects.filter(
                app_label="project", model="cycle"
            )
            cycle = cycle.first()
            status = status.first()
            agreements = Agreement.objects.filter(
                cycle__status__code__in=["INT01", "INT02"]
            )
            if cycle.delta_enabled:
                agreements = agreements.exclude(fin_sense_check=True)
            json_data = {"change_type": "HQ Promoted to FIN01", "list_type": list_type}
            comment = Comment.objects.create(
                message=request_comment,
                user=self.request.user,
                content_type=content_type.first(),
                comment_type=json_data,
            )
            cycle.comments.add(comment)
            cycle.status = status
            agreements.update(
                status_id=status.id,
                fin_sense_check=F("int_sense_check"),
                fin_comment_id=F("int_comment_id"),
            )
            cycle.save()

            # send notification for fin1
            send_project_selection_notification(year, 6)
            return Response({"res": "success"}, status=200)
        return Response({"res": "Invalid Cycle"}, status=400)


class SelectionProgress(generics.ListAPIView):
    queryset = Agreement.objects.all()
    serializer_class = CycleSerializer

    def get(self, request, *args, **kwargs):
        queryset = (
            self.queryset.filter(cycle__year=kwargs["year"])
            .values("status")
            .annotate(Count("status"))
        )
        total_count = self.queryset.filter(cycle__year=kwargs["year"]).count()

        return Response(
            {
                "count": total_count,
                "FIN01": self.queryset.filter(
                    status=6, cycle__year=kwargs["year"]
                ).count(),
                "results": queryset,
            },
            status=200,
        )


class AgreementDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAgreementMember)

    def get_queryset(self):
        membership = self.request.user.membership
        return filter_role_based_agreements(self.request.user, membership, self.queryset)


class AgreementDetailExp(generics.RetrieveUpdateDestroyAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer


class AgreementCreate(generics.CreateAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializerCRUD
    permission_classes = (IsAuthenticated, IsUNHCR)


class AgreementRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializerCRUD
    success_url = reverse_lazy("projects:agreement-list")
    error_url = reverse_lazy("projects:agreement-error")
    permission_classes = (
        IsAuthenticated,
        AgreementPermission,
    )

    def get_success_url(self):
        if self.success_url:
            return self.success_url.format(**self.object.__dict__)
        else:
            raise ImproperlyConfigured(
                "No success URL to redirect to. Provide a success_url."
            )

    def get_error_url(self):
        if self.error_url:
            return self.error_url.format(**self.object.__dict__)
        else:
            raise ImproperlyConfigured(
                "No error URL to redirect to. Provide a error_url."
            )

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
            return HttpResponseRedirect(success_url)
        except models.ProtectedError:
            return Response(
                {"error": "Protected Resources"}, status=status.HTTP_409_CONFLICT
            )


class AgreementTypeList(generics.ListCreateAPIView):
    queryset = AgreementType.objects.all()
    serializer_class = AgreementTypeSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class AgreementTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AgreementType.objects.all()
    serializer_class = AgreementTypeSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class AgreementReportList(generics.ListCreateAPIView):
    queryset = AgreementReport.objects.all()
    serializer_class = AgreementReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class AgreementReportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AgreementReport.objects.all()
    serializer_class = AgreementReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class PADetail(generics.RetrieveUpdateAPIView):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def handle_comments(self, request, *args, **kwargs):
        body = json.loads(request.body.decode("utf-8"))
        user = User.objects.get(id=self.request.user.id)
        description = body["comment"]
        comment = Comments.objects.create(user=user, description=description)
        request.data["comment"] = comment.id
        return request.data

    def patch(self, request, pk=None, *args, **kwargs):
        qs = self.queryset.filter(pk=pk)

        if qs.exists() is False:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        qs = qs.filter(Q(cycle__status__code__in=["PRE01", "PRE02", "PRE03"]))
        if qs.exists() is False:
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        pl = qs.last()
        serializer = self.serializer_class(
            pl, data=self.handle_comments(request), context={"request": request}
        )
        if serializer.is_valid():
            serializer.save()
        else:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data)

    def put(self, request, pk=None, *args, **kwargs):
        qs = self.queryset.filter(pk=pk)

        if qs.exists() is False:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        qs = qs.filter(Q(cycle__status__code__in=["PRE01", "PRE02", "PRE03"]))
        if qs.exists() is False:
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        pl = qs.last()

        serializer = self.serializer_class(
            pl, data=self.handle_comments(request), context={"request": request}
        )
        if serializer.is_valid():
            serializer.save()
        else:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data)


class CycleList(generics.ListAPIView):
    serializer_class = CycleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    def get_queryset(self):
        try:
            return Cycle.objects.all().order_by("-year")
        except Exception:
            raise Http404("Cycle does not exist")


class CycleDetail(generics.RetrieveUpdateAPIView):
    queryset = Cycle.objects.all()
    serializer_class = CycleSerializer
    lookup_field = "year"
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    resource_model = Cycle
    project_status_instance = apps.get_model("project.Status")
    user_type_role_model = apps.get_model("account.UserTypeRole")
    field_office_model = apps.get_model("field.FieldOffice")

    def get(self, request, *args, **kwargs):
        year = kwargs["year"]
        fo = self.user_type_role_model.get_field_office(
            user=request.user, resource_model="fieldmember"
        )
        if fo:
            res_all, data, total_projects = self.resource_model().get_latest_cycles(
                ffo_ids=(fo.id,), year=year
            )
            res_list = [
                item for item in res_all if item.get("office").get("id") == fo.id
            ]
            res = res_list[0].get("cycle") if res_list else {}
        else:
            cycle = self.resource_model().get_cycle_based_on_year(year=year)
            if not cycle:
                return Response({}, status=status.HTTP_404_NOT_FOUND)
            res = self.serializer_class(cycle).data
        return Response(res, status=status.HTTP_200_OK)


class CycleAPIView(generics.RetrieveAPIView):
    queryset = Cycle.objects.all()
    serializer_class = CycleSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO | IsAUDIT)

    def get(self, request, *args, **kwargs):
        data = self.queryset.filter(year=kwargs["year"])
        field_office = request.data.get("field_office")
        membership = request.user.membership
        if data.exists():
            serializer = self.serializer_class(data.first())

            if membership["type"] == "FO":
                fo_status = (
                    data.first()
                    .agreement_cycle.filter(
                        business_unit__field_office_id=membership["field_office"]["id"]
                    )
                    .values("status__code", "status__id")
                    .first()
                )
                if fo_status is not None:
                    status_code = fo_status["status__code"]
                    status_id = fo_status["status__id"]
                    response = serializer.data
                    if "status" in response:
                        response["status"]["code"] = status_code
                        response["status"]["id"] = status_id
                        return Response(response, status=status.HTTP_200_OK)
                else:
                    return Response([], status=status.HTTP_200_OK)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response([], status=status.HTTP_200_OK)


class AgreementAssignmentList(generics.ListCreateAPIView):
    queryset = Agreement.objects.all().order_by("id")
    serializer_class = AgreementAssignmentListSerializer
    permission_classes = (
        IsAuthenticated,
        IsUNHCR | IsPartnerAdmin | IsAuditAdmin | IsFO,
    )
    permission_classes = (IsAuthenticated,)
    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = AgreementAssignmentFilter
    ordering_fields = [
        "partner__legal_name",
        "country__name",
        "business_unit",
        "number",
        "partner__number",
        "budget",
        "installments_paid",
        "partner__implementer_type__implementer_type",
        "start_date",
        "end_date",
        "agreement_type",
        "agreement_date",
        "operation__code",
    ]

    def get_queryset(self):
        membership = self.request.user.membership
        year = self.kwargs["budget_ref"]
        queryset = super().get_queryset().filter(cycle__year=year)
        fin_list_qs = filter_project_selection(queryset, year, "fin", "included")

        if membership["type"] == "HQ":
            return fin_list_qs
        elif membership["type"] == "AUDITOR" and membership["role"] == "ADMIN":
            return fin_list_qs.filter(audit_firm_id=membership["audit_agency"]["id"])
        elif membership["type"] == "PARTNER" and membership["role"] == "ADMIN":
            return fin_list_qs.filter(partner_id=membership["partner"]["id"])
        elif membership["type"] == "FO" and membership["role"] == "APPROVER":
            field_office = membership["field_office"]["id"]
            return fin_list_qs.filter(business_unit__field_office__id=field_office)
        elif membership["type"] == "AUDITOR":
            return fin_list_qs.filter(
                focal_points__user=self.request.user,
                focal_points__user_type="auditing",
            ).distinct("id")
        elif membership["type"] == "PARTNER":
            return fin_list_qs.filter(
                focal_points__user=self.request.user,
                focal_points__user_type="auditing",
            ).distinct("id")
        else:
            return queryset.filter(focal_points__user=self.request.user)

    def post(self, request, *args, **kwargs):
        self.serializer_class = AgreementMemberSerializer
        data = self.create(request, *args, **kwargs)
        return data


class AgreementMemberList(generics.ListCreateAPIView):

    queryset = AgreementMember.objects.all()
    serializer_class = AssignmentMemberSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = AgreementMemberFilter


"""
 AgreementMessage Class Start
 @List Message
 @Post Message
"""


class AgreementMessageList(generics.ListCreateAPIView):

    permission_classes = (IsAuthenticated, IsUNHCR | IsAgreementMember)
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def get(self, request, *args, **kwargs):
        agreement_id = kwargs.get("pk")
        self.queryset = self.queryset.filter(agreement_id=agreement_id)
        return self.list(request, *args, **kwargs)


"""
 AgreementMessageDetail Class Start
 @Update Message
 @Delete Message
"""


class AgreementMessageDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsUNHCR | IsAgreementMember)
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def delete(self, request, *args, **kwargs):
        message = self.queryset.filter(id=kwargs["msg_id"]).delete()
        return Response(
            {"code": "OK_DELETE", "message": code["OK_DELETE"]},
            status=status.HTTP_200_OK,
        )

    def patch(self, request, *args, **kwargs):

        message = self.queryset.filter(id=kwargs["msg_id"]).first()
        if message is not None:
            serializer = self.serializer_class(message, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(
                    {"code": "OK_DELETE", "message": code["OK_DELETE"]},
                    status=status.HTTP_200_OK,
                )
        return Response(
            {"code": "E_BAD_REQUEST", "message": code["E_BAD_REQUEST"]},
            status=status.HTTP_400_BAD_REQUEST,
        )


class AgreementBudgetDetail(generics.ListCreateAPIView):
    queryset = AgreementBudget.objects.all()
    serializer_class = AgreementBudgetSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAgreementMember)

    def get(self, request, *args, **kwargs):
        agreement_id = kwargs.get("pk")
        self.queryset = self.queryset.filter(agreement_id=agreement_id)
        return self.list(request, *args, **kwargs)


class ActivityAPIView(generics.ListAPIView):
    queryset = Cycle.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (IsAuthenticated, IsFO)

    def get(self, request, *args, **kwargs):
        cycle_qs = self.queryset.filter(year=kwargs["year"])
        membership = request.user.membership
        field_office = membership["field_office"]["id"]

        if cycle_qs.exists():
            members = FieldMember.objects.filter(
                field_office_id=field_office, user=request.user
            ).values_list("user_id", flat=True)
            comments_qs = cycle_qs.first().comments.filter(user_id__in=members)
            serializer = CommonCommentSerializer(comments_qs, many=True)
            return Response({"results": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"results": []}, status=status.HTTP_200_OK)


class OIOSBusinessUnitReportsAPIView(generics.ListCreateAPIView):
    queryset = OIOSBusinessUnitReport.objects.all()
    serializer_class = OIOSBusinessUnitReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = OIOSBusinessUnitFilter


class OIOSBusinessUnitReportAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = OIOSBusinessUnitReport.objects.all()
    serializer_class = OIOSBusinessUnitReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class SelectionSettingsAPIView(generics.ListAPIView):
    queryset = SelectionSettings.objects.all()
    serializer_class = SelectionSettingsSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )
    filter_class = SelectionSettingsFilter


class SelectionSettingAPIView(generics.RetrieveUpdateAPIView):
    queryset = SelectionSettings.objects.all()
    serializer_class = SelectionSettingsSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def patch(self, request, *args, **kwargs):

        queryset = self.queryset.filter(cycle__year=kwargs["year"])
        if queryset.exists():
            instance = queryset.first()
            serializer = self.serializer_class(instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                if len(request.data) == 1 and "risk_threshold" in request.data:
                    return Response(serializer.data, status=status.HTTP_200_OK)
                bg_task = update_risk_rating_bg(instance.cycle.year)
                data = serializer.data
                data["bg_task_id"] = bg_task.pk
                data["bg_task_hash"] = bg_task.task_hash
                return Response(data, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            cycle, created = Cycle.objects.get_or_create(
                year=kwargs["year"], budget_ref=kwargs["year"]
            )
            instance = SelectionSettings.objects.create(cycle=cycle, **request.data)
            bg_task = update_risk_rating_bg(instance.cycle.year)
            serializer = self.serializer_class(instance)
            data = serializer.data
            data["bg_task_id"] = bg_task.pk
            data["bg_task_hash"] = bg_task.task_hash
            return Response(data, status=status.HTTP_200_OK)


class FOProgressAPIView(generics.ListCreateAPIView):

    queryset = Agreement.objects.all()
    serializer_class = FOProgressSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get_queryset(self):
        year = self.kwargs["year"]
        if year is not None:
            queryset = self.queryset.filter(cycle__year=year).distinct(
                "business_unit__field_office_id", "status__code"
            )
            return queryset
        return self.queryset.none


class HQDashboardView(generics.ListAPIView):

    queryset = Agreement.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):

        year = kwargs["year"]
        aggreements = Agreement.objects.filter(status__isnull=False)
        cycle = Cycle.objects.filter(year=year).values("status__code")
        if len(cycle) == 0:
            return Response({"cycle": None}, status=status.HTTP_200_OK)
        cycle_status = {"status__code": None}

        total_fa = (
            Agreement.objects.filter(cycle__budget_ref=year)
            .distinct()
            .values("business_unit__field_office_id")
        )
        field_assessment_progress = FieldAssessmentProgress.objects.filter(
            budget_ref=year
        )
        data = field_assessment_progress.values("pending_with").annotate(
            Count("pending_with")
        )

        cycle_status["status__code"] = (
            cycle[0]["status__code"] if cycle.count() > 0 else None
        )
        pre_list_check = filter_project_selection(
            self.queryset.filter(pre_sense_check=True), year, "pre", "included"
        )
        if cycle[0]["status__code"] == "PRE01" and pre_list_check.count() == 0:
            cycle_status["status__code"] = "FIELD_ASSESSMENT"

        agreements_sm = Agreement.objects.filter(cycle__year=year)
        cycle = Cycle.objects.filter(year=year).first()

        pre_stats, int_stats, fin_stats = ("NA", "NA", "NA")
        for i in range(1, cycle.status.id + 1):
            if i in [PRE01, PRE02, PRE03] and pre_stats is "NA":
                pre_stats = filter_project_selection(
                    agreements_sm, year, "pre", "included"
                ).count()
            elif i in [INT01, INT02] and int_stats is "NA":
                int_stats = filter_project_selection(
                    agreements_sm, year, "int", "included"
                ).count()
            elif i in [FIN01, FIN02, FIN03] and fin_stats is "NA":
                fin_stats = filter_project_selection(
                    agreements_sm, year, "fin", "included"
                ).count()

        response = {
            "line": aggreements.values("cycle__year").annotate(
                status_count=Count("status"), status_type=F("status__code")
            ),
            "small_stats": {
                "pre": pre_stats,
                "int": int_stats,
                "fin": fin_stats,
            },
            "cycle": cycle_status,
        }

        return Response(response, status=status.HTTP_200_OK)


class AgreementMemberAPIView(generics.UpdateAPIView):

    queryset = Agreement.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = AgreementSerializer

    def parse_focal_points(self, request):
        data = dict(
            unhcr__focal=request.data.get("unhcr_focal", None),
            unhcr__alternate_focal=request.data.get("unhcr_focal_alternate", None),
            partner__focal=request.data.get("partner_focal", None),
            partner__alternate_focal=request.data.get("partner_focal_alternate", None),
            auditing__reviewer=request.data.get("auditor_reviewers", None),
        )
        if not any(([data[i] for i in data])):
            return None
        return data

    def create_focal_point(self, user_id, user_type, role):
        user = User.objects.filter(is_active=True, id=user_id).first()
        if user is not None:
            data = {"user_type": user_type, "{}".format(role): True, "user": user}
            unhcr_focal, _ = AgreementMember.objects.get_or_create(**data)
            return unhcr_focal

    def patch(self, request, *args, **kwargs):
        membership = request.user.membership
        agreement_id = kwargs["pk"]
        agreement_obj = filter_role_based_agreements(
            request.user, membership, self.queryset.filter(id=agreement_id)
        ).first()
        audited_by_gov = request.data.pop("audited_by_gov", None)
        data = self.parse_focal_points(request)
        if data is not None:
            for member in data:
                user_id = data[member]
                user_type, role = member.split("__")
                filter_params = {"user_type": user_type, "{}".format(role): True}
                agreement_obj.focal_points.remove(*agreement_obj.focal_points.filter(**filter_params))
                if user_id is not None and user_type != "auditing":
                    agreement_member = self.create_focal_point(user_id, user_type, role)
                    agreement_obj.focal_points.add(agreement_member)
                else:
                    user_ids = data[member]
                    if user_ids is not None:
                        for user_id in user_ids:
                            agreement_member = self.create_focal_point(user_id, "auditing", "reviewer")
                            agreement_obj.focal_points.add(agreement_member)

        if audited_by_gov is not None:
            agreement_obj.audited_by_gov = audited_by_gov

        agreement_obj.save()
        serializer = self.serializer_class(
            agreement_obj, context={"request": request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

class ProjectSelectionStats(generics.ListAPIView):

    queryset = Agreement.objects.all()

    def get(self, request, *args, **kwargs):

        year = kwargs.get("year")
        agreements = Agreement.objects.filter(is_auditable=True)
        included = filter_project_selection(agreements, year, "pre", "included")
        excluded = filter_project_selection(agreements, year, "pre", "excluded")

        response = {
            "summary_stats": {
                "included": included.count(),
                "excluded": excluded.count(),
            },
            "budget_stats": {
                "included": included.aggregate(Sum("budget"))["budget__sum"],
                "excluded": excluded.aggregate(Sum("budget"))["budget__sum"],
            },
        }

        return Response(response, status=status.HTTP_200_OK)


class AgreementListExport(generics.ListCreateAPIView):
    serializer_class = PreliminaryListExportSerializer
    permission_classes = (IsAuthenticated,)
    resource_model = Agreement

    def get(self, request, *args, **kwargs):

        report_type = request.query_params.get("report_type", None)
        year = kwargs["year"]

        try:
            report_type = int(report_type)
        except Exception as e:
            error = "a valid report_type is a required"
            raise exceptions.APIException(error)

        if report_type is None or report_type not in range(1, 9):
            error = "a valid report_type is a required"
            raise exceptions.APIException(error)

        fo_ids = FieldAssessmentProgress.objects.filter(
            pending_with="COMPLETED"
        ).values_list("field_office_id", flat=True)
        queryset = Agreement.objects.filter(
            ~Q(field_assessment__score__id=4), business_unit__field_office_id__in=fo_ids
        )
        cycle_qs = Cycle.objects.filter(year=year)
        if cycle_qs.exists() == False:
            error = "No data available based on the given criteria"
            raise exceptions.APIException(error)
        inc = SenseCheck.objects.exclude(code="SC12").values_list("code", flat=True)
        cycle_status = cycle_qs.first().status.id
        agreements = queryset.filter(
            cycle__status_id__in=range(1, cycle_status + 1), cycle__year=year
        )

        if agreements.exists():
            agreement_list_export(year, report_type, request.user.id)
            return Response({"message": "An email will be send with the attachment"})
        else:
            error = "No data available based on the given criteria"
            raise exceptions.APIException(error)


class AuditAssignmentStatus(generics.ListCreateAPIView):
    def get(self, request, *args, **kwargs):

        year = kwargs["year"]
        queryset = Agreement.objects.filter(cycle__year=year)
        agreements = filter_project_selection(queryset, year, "fin", "included")
        audit_assignment_pending = agreements.filter(audit_firm__isnull=True)
        focal_assignment_pending = agreements.filter(focal_points__isnull=True)

        response = {
            "audit_assignment_completed": True
            if audit_assignment_pending.count() == 0
            else False,
            "focal_assignment_completed": True
            if focal_assignment_pending.count() == 0
            else False,
        }

        return Response(response, status=status.HTTP_200_OK)


class AuditAssignmentExport(generics.ListCreateAPIView):

    queryset = Agreement.objects.all()
    serializer_class = PreliminaryListExportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    def get(self, request, *args, **kwargs):

        year = kwargs["year"]
        membership = request.user.membership

        queryset = filter_project_selection(self.queryset, year, "fin", "included")

        if membership["type"] == "AUDITOR":
            queryset = queryset.filter(audit_firm_id=membership["audit_agency"]["id"])

        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = 'attachment; filename="audit-assignment.csv"'

        fields = [
            "Reason",
            "Business Unit",
            "Agreement Number",
            "Country",
            "Partner",
            "Implementer Number",
            "Budget(in USD)",
            "Installments Paid",
            "Implementer Type",
            "ProjectStartDate",
            "ProjectEndDate",
            "Agreement Type",
            "Agreement Date",
            "Operation",
            "Audit Firm Assigned",
        ]
        writer = csv.writer(response)
        writer.writerow(fields)

        if queryset.exists():

            data = queryset.values_list(
                "reason__description",
                "business_unit__code",
                "number",
                "country__name",
                "partner__legal_name",
                "partner__number",
                "budget",
                "installments_paid",
                "partner__implementer_type__implementer_type",
                "start_date",
                "end_date",
                "agreement_type",
                "agreement_date",
                "operation__code",
                "audit_firm_id",
            )

            for row in data:
                row = list(row)
                if row[-1] == None:
                    row[-1] = "No"
                else:
                    row[-1] = "Yes"
                writer.writerow(row)
            return response
        raise exceptions.NotFound("No data available")


class AuditorUnassignAPIView(generics.CreateAPIView):

    queryset = Agreement.objects.all()
    serializer_class = AuditorUnassignSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        filter_url = request.data.get("filter_url", None)
        agreements = request.data.get("agreements", [])

        if len(agreements) == 0 and filter_url == None:
            error = "Either a list of agreement ids or a filter_url is required"
            raise exceptions.APIException(error)

        if type(agreements) != list:
            error = "Agreement ids must be a list"
            raise exceptions.APIException(error)

        if filter_url is None:
            selected_agreement = agreements
        else:
            # Filter based on the queryparams
            from urllib import parse
            from django.http import QueryDict

            query_params = dict(parse.parse_qsl(parse.urlsplit(filter_url).query))
            query_dict = QueryDict("", mutable=True)
            query_dict.update(query_params)

            queryset = filter_project_selection(
                self.queryset, kwargs["year"], "fin", "included"
            )
            # A workaround to call the filter internally for batch project assignment
            data = AgreementAssignmentFilter(
                query_dict,
                queryset=queryset,
            )
            values = {"cycle__year": kwargs["year"], "user": request.user}
            data = data.qs.filter(**values)
            selected_agreement = data.values_list("id", flat=True)

        self.queryset.filter(id__in=selected_agreement).update(audit_firm=None)

        return Response({"message": "Successfully removed audit firms"}, status=200)


class CancelledAuditList(generics.ListCreateAPIView):
    queryset = CancelledAudit.objects.all()
    serializer_class = CancelledAuditSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class CancelledAuditDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CancelledAudit.objects.all()
    serializer_class = CancelledAuditSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class CancelledAuditImportAPI(generics.ListCreateAPIView):
    queryset = CancelledAudit.objects.all()
    serializer_class = CancelledAuditSerializer

    def post(self, request, *args, **kwargs):

        file = request.FILES.get("file")
        temp_dir = tempfile.gettempdir()
        file_name = "tmp_data_import.xlsx"
        file_path = os.path.join(temp_dir, file_name)

        with open(file_path, "wb") as fp:
            fp.write(file.read())
        import_canceled_audits(file_path)
        return Response({"msg": "Importing data is sucessfully done"})


class ExportCancelledAudit(generics.ListCreateAPIView):
    queryset = CancelledAudit.objects.all()
    serializer_class = CancelledAuditSerializer

    def get(self, request, *args, **kwargs):
        year = self.kwargs["year"]
        fields = [
            "id",
            "business_unit_id",
            "partner_id",
            "agreement_number",
            "year",
            "reason",
            "is_cancelled",
        ]

        columns = [
            "ID",
            "Business_Unit_Id",
            "Partner_Id",
            "Agreement_Number",
            "Year",
            "Reason",
            "Is_Cancelled",
        ]
        items = CancelledAudit.objects.filter(year=year).values(*fields)
        response = HttpResponse(content_type="text/csv")
        response["Content-Disposition"] = "attachement; filename=cancelledaudit.csv"
        writer = csv.writer(response, delimiter=",")
        writer.writerow(columns)
        for obj in items:
            writer.writerow(obj.values())
        return response


class ModifyStatusAPIView(
    generics.CreateAPIView,
):
    queryset = Agreement.objects.all()
    serializer_class = AgreementSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    def post(self, request, *args, **kwargs):
        comment = request.data.get("comment")
        list_type = request.data.get("list_type")
        agreements = request.data.get("agreements", None)
        filter_params = request.data.get("filter_params", None)
        action_type = request.data.get("action_type")
        if action_type not in ["include", "exclude"] or list_type not in [
            "PRE",
            "INT",
            "FIN",
        ]:
            return Response({"res": "Bad request"}, status=status.HTTP_400_BAD_REQUEST)

        if filter_params is None:
            agreement_qs = self.queryset.filter(id__in=agreements)
        else:
            filter_data = AgreementFilter(filter_params, self.queryset)
            agreement_qs = Agreement.objects.filter(
                id__in=filter_data.qs.values_list("id", flat=True)
            )
        if agreement_qs.exists():
            cycle = Cycle.objects.filter(id=agreement_qs.first().cycle.id).first()
            if cycle and cycle.delta_enabled:
                agreement_qs = agreement_qs.exclude(fin_sense_check=True)
            if agreement_qs.exists():
                comment = Comment.objects.create(message=comment)
                new_status = True if action_type == "include" else False
                data = {
                    "{}_sense_check".format(list_type.lower()): new_status,
                    "{}_comment".format(list_type.lower()): comment,
                }
                agreement_qs.update(**data)
                return Response({"res": "success"}, status=status.HTTP_200_OK)
            else:
                return Response(
                    {"res": "Cant change already selected projects"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        return Response(
            {"res": "No project agreement found based on the given criteria"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    def put(self, request, *args, **kwargs):
        error = "Put method now allowed"
        raise exceptions.APIException(error)
