from django.db import connection
from project.risk_rating.risk_rating_funcs import risk_rating_query
from common.helpers import common_raw_sql
from common.models import Comment
from project.filters import SenseCheckFilter
from project.models import Agreement, SenseCheck, Cycle
from django.db.models import F


def sql_risk_rating(instance):
    try:
        risk_rating_q = risk_rating_query % {
            "__year__": instance.cycle.year,
            "__agreement_id__": str(instance.id),
        }
        cursor = connection.cursor()
        cursor.execute(risk_rating_q)
        data = cursor.fetchall()

        if data[0][0] is None:
            pass
        connection.close()
        try:
            rating = round(data[0][0], 4)
            return rating
        except Exception as e:
            print(e, instance.__dict__)
            return 0
    except:
        return 0


def check_risk_rating_breakdown(instance):
    try:
        data = dict(
            cash_test="select * from get_cash_test(%(__agreement_id__)s) as cash_test"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            emergency_goal_test="select * from emergency_goal_test(%(__agreement_id__)s) as goal"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            modified_in_last_4_years="select * from modified_in_last_4_years(%(__agreement_id__)s) as modified"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            latest_icq="select * from latest_icq(%(__agreement_id__)s, %(__year__)s) as icq"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            get_partner_score="select * from get_partner_score(%(__agreement_id__)s) as partner_score"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            pqp_status="select * from pqp_status(%(__agreement_id__)s) as pqp"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
            get_oios_bu_report="select * from get_oios_bu_report('%(__year__)s-01-01', '%(__year__)s-12-31', %(__agreement_id__)s) as bu_report"
            % {"__year__": instance.cycle.year, "__agreement_id__": str(instance.id)},
        )
        response = {}
        for q in data:
            try:
                d = float(common_raw_sql(data[q]))
            except:
                d = common_raw_sql(data[q])
            if len(d) > 0:
                response.update(d[0])

        response.update(
            {
                "procurement": float(response["procurement"]),
                "staff_cost": float(response["staff_cost"]),
                "cash": float(response["cash"]),
                "other_accounts": float(response["other_accounts"]),
                "overall_score_perc": float(response["overall_score_perc"]),
                "oios_rating": float(response["oios_rating"]),
                "risk_rating": None
                if sql_risk_rating(instance) is None
                else float(sql_risk_rating(instance)),
                "partner": instance.partner.number,
                "bu": instance.business_unit.code,
                "number": instance.number,
            }
        )
        return response
    except Exception as e:
        return {"Error": str(e)}


def fetch_focal_points(instance):
    response = {}
    firm = {"id": None, "legal_name": None}
    for user_type in ["partner", "unhcr", "auditing"]:
        focal_points = (
            instance.focal_points.filter(user_type=user_type, focal=True)
            .annotate(
                _id=F("user__id"), email=F("user__email"), fullname=F("user__fullname")
            )
            .values("_id", "email", "fullname")
            .annotate(id=F("_id"))
            .values("id", "email", "fullname")
        )
        focal_points = None if len(focal_points) == 0 else focal_points[0]
        alt_focal_point = (
            instance.focal_points.filter(user_type=user_type, alternate_focal=True)
            .annotate(
                _id=F("user__id"), email=F("user__email"), fullname=F("user__fullname")
            )
            .values("_id", "email", "fullname")
            .annotate(id=F("_id"))
            .values("id", "email", "fullname")
        )
        alt_focal_point = None if len(alt_focal_point) == 0 else alt_focal_point[0]
        reviewers = (
            instance.focal_points.filter(user_type=user_type, reviewer=True)
            .annotate(
                _id=F("user__id"), email=F("user__email"), fullname=F("user__fullname")
            )
            .values("_id", "email", "fullname")
            .annotate(id=F("_id"))
            .values("id", "email", "fullname")
        )
        data = {
            "focal": focal_points,
            "alternate_focal": alt_focal_point,
            "reviewers": list(reviewers),
        }
        if user_type == "partner" and instance.partner is not None:
            firm["id"] = instance.partner.id
            firm["legal_name"] = instance.partner.legal_name
            data["firm"] = firm
        elif user_type == "auditor" and instance.audit_firm is not None:
            firm["id"] = instance.audit_firm.id
            firm["legal_name"] = instance.audit_firm.legal_name
            data["firm"] = firm
        response[user_type] = data
    return response


def update_project_selection_reason(year):
    cycle = Cycle.objects.filter(year=year).first()
    if cycle is not None:
        queryset = Agreement.objects.filter(cycle=cycle, is_auditable=True)
        sensecheck_filter = SenseCheckFilter(queryset, year)
        sensecheck_filter.check_postponed_and_cancelled()

        sensechecks = SenseCheck.objects.filter(
            settings__function__isnull=False
        ).order_by("id")
        for sensecheck in sensechecks:
            if sensecheck.settings["function"] != "check_materiality_and_oios":
                filtered_reasons = sensecheck_filter.queryset.filter(
                    **{sensecheck.settings["function"]: 1}
                )
                filtered_reasons = filtered_reasons.values_list("id", flat=True)
                if cycle.delta_enabled:
                    queryset = queryset.exclude(fin_sense_check=True)

                queryset.filter(id__in=filtered_reasons).update(
                    reason=sensecheck,
                    pre_sense_check=True,
                )
