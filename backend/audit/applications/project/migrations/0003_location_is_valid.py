# Generated by Django 2.2.1 on 2020-10-27 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0002_auto_20201014_0756"),
    ]

    operations = [
        migrations.AddField(
            model_name="location",
            name="is_valid",
            field=models.BooleanField(default=False, null=True),
        ),
    ]
