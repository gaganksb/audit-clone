# Generated by Django 2.2.1 on 2021-09-05 13:12

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0015_auto_20210831_1351"),
    ]

    operations = [
        migrations.AddField(
            model_name="agreement",
            name="risk_rating_calc",
            field=django.contrib.postgres.fields.jsonb.JSONField(default=dict),
        ),
    ]
