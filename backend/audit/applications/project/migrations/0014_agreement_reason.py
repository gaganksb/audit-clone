# Generated by Django 2.2.1 on 2021-08-12 04:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("project", "0013_auto_20210803_1106"),
    ]

    operations = [
        migrations.AddField(
            model_name="agreement",
            name="reason",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="project.SenseCheck",
            ),
        ),
    ]
