# Generated by Django 2.2.1 on 2021-10-19 03:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0018_auto_20211006_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='agreement',
            name='kpi2_sampled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='selectionsettings',
            name='oios_cutoff',
            field=models.DecimalField(decimal_places=3, default=0, max_digits=10),
        ),
    ]
