# Generated by Django 2.2.1 on 2021-06-10 07:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("partner", "0005_adversedisclaimer"),
        ("common", "0002_auto_20200930_0923"),
        ("project", "0005_auto_20210121_1357"),
    ]

    operations = [
        migrations.AddField(
            model_name="selectionsettings",
            name="cost_benefit",
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name="selectionsettings",
            name="high_values",
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name="selectionsettings",
            name="materiality",
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name="selectionsettings",
            name="sense_check_icq",
            field=models.IntegerField(
                default=0,
                help_text="Recent icq score  be compared for the projects",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="PreviousAudit",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_date",
                    models.DateTimeField(default=django.utils.timezone.now, null=True),
                ),
                (
                    "last_modified_date",
                    models.DateTimeField(default=django.utils.timezone.now, null=True),
                ),
                ("agreement_number", models.IntegerField(default=None, null=True)),
                ("year", models.IntegerField()),
                ("audited", models.BooleanField(default=None, null=True)),
                (
                    "business_unit",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="common.BusinessUnit",
                    ),
                ),
                (
                    "partner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="partner.Partner",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="project_previousaudit_related",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "unique_together": {
                    ("business_unit", "partner", "agreement_number", "year")
                },
            },
        ),
    ]
