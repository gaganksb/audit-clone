from django.contrib import admin
from .models import (
    Goal,
    Situation,
    AgreementType,
    Agreement,
    AgreementReport,
    Cycle,
    CycleTimeRemindTemplate,
    Status,
    Operation,
    Vendor,
    Output,
    AccountType,
    AccountCategory,
    Account,
    Pillar,
    OIOSBusinessUnitReport,
    PopulationPlanningGroup,
    AgreementMember,
    SenseCheck,
    Comments,
)


class GoalAdmin(admin.ModelAdmin):
    search_fields = (
        "code",
        "description",
    )
    list_display = (
        "code",
        "description",
    )
    ordering = ("code",)


class AgreementTypeAdmin(admin.ModelAdmin):
    search_fields = ("description",)
    list_display = ("description",)
    ordering = ("description",)


class AgreementAdmin(admin.ModelAdmin):
    search_fields = ("business_unit__code", "number")
    list_display = (
        "business_unit",
        "number",
        "partner",
    )
    list_filter = ("cycle",)


class AgreementReportAdmin(admin.ModelAdmin):
    search_fields = ("agreement__number",)
    list_display = ("agreement",)
    ordering = ("agreement__number",)
    raw_id_fields = ("agreement",)


class CycleAdmin(admin.ModelAdmin):
    list_display = ("id", "year", "changed_date")


class CycleTimeRemindTemplateAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "duration")


admin.site.register(Goal, GoalAdmin)
admin.site.register(AgreementType, AgreementTypeAdmin)
admin.site.register(Agreement, AgreementAdmin)
admin.site.register(AgreementReport, AgreementReportAdmin)
admin.site.register(Cycle, CycleAdmin)
admin.site.register(CycleTimeRemindTemplate, CycleTimeRemindTemplateAdmin)
admin.site.register(Situation)
admin.site.register(Status)
admin.site.register(Operation)
admin.site.register(Vendor)
admin.site.register(Output)
admin.site.register(AccountType)
admin.site.register(AccountCategory)
admin.site.register(Account)
admin.site.register(Pillar)
admin.site.register(OIOSBusinessUnitReport)
admin.site.register(PopulationPlanningGroup)
admin.site.register(AgreementMember)
admin.site.register(SenseCheck)
admin.site.register(Comments)
