from enum import Enum, unique, auto


class AutoNameEnum(Enum):
    def _generate_next_value_(name, *args):
        return name

@unique
class KPIEnum(AutoNameEnum):
    KPI1 = auto()
    KPI2 = auto()
    KPI3 = auto()
    KPI4 = auto()
    KPI5 = auto()

@unique
class SurveyTypeEnum(AutoNameEnum):
    FIELD = "field"
    PARTNER = "partner"
    AUDITOR = "auditor"
    KPI1 = "audit_timeline_survey"
    KPI2 = "audit_quality_survey"