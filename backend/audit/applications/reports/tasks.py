import csv
import tempfile
from account.models import User
from background_task import background
from notify.helpers import send_notification
from datetime import datetime
from field.models import FieldAssessmentProgress
from project.models import Agreement
from notify.helpers import send_email_with_attachment
from common.consts import (
    FIN02,
    FIN03,
    FIN01
)
@background(schedule=3)
def agreement_list_export(year, report_type, user_id):

    fields = [
        "business_unit__code",
        "number",
        "partner__number",
        "country__name",
        "country__region__name",
        "country__code",
        "partner__number",
        "partner__legal_name",
        "partner__implementer_type__implementer_type",
        "start_date",
        "end_date",
        "agreement_type",
        "audit_worksplit_group__name",
        "budget",
        "installments_paid",
        "audit_firm__legal_name",
        "field_assessment__score__value",
        "risk_rating",
        "pre_sense_check",
        "int_sense_check",
        "fin_sense_check",
        "pre_comment__message",
        "int_comment__message",
        "fin_comment__message",
        "reason__description",
        "risk_rating_calc__cash",
        "risk_rating_calc__goal",
        "risk_rating_calc__staff_cost",
        "risk_rating_calc__oios_rating",
        "risk_rating_calc__procurement",
        "risk_rating_calc__modified",
        "risk_rating_calc__other_accounts",
        "risk_rating_calc__pqp_worldwide",
        "risk_rating_calc__overall_score_perc",
        "risk_rating_calc__value",
        "status__code",
        "audited_by_gov",
    ]

    excel_fields = [
        "bu_number",
        "BU_partner_id",
        "business_unit__code",
        "country__name",
        "country__region__name",
        "partner__number",
        "partner__legal_name",
        "partner__implementer_type__implementer_type",
        "number",
        "start_date",
        "end_date",
        "agreement_type",
        "audit_worksplit_group__name",
        "budget",
        "installments_paid",
        "audit_firm__legal_name",
        "doc_location",
        "site_location",
        "unhcr_focal",
        "unhcr_alternate_focal",
        "partner_focal",
        "partner_alternate_focal",
        "field_assessment__score__value",
        "risk_rating",
        "pre_sense_check",
        "int_sense_check",
        "fin_sense_check",
        "pre_comment__message",
        "int_comment__message",
        "fin_comment__message",
        "reason__description",
        "risk_rating_calc__cash",
        "risk_rating_calc__goal",
        "risk_rating_calc__staff_cost",
        "risk_rating_calc__oios_rating",
        "risk_rating_calc__procurement",
        "risk_rating_calc__modified",
        "risk_rating_calc__other_accounts",
        "risk_rating_calc__pqp_worldwide",
        "risk_rating_calc__overall_score_perc",
        "risk_rating_calc__value",
        "status__code",
        "audited_by_gov",
    ]

    excel_col_labels = [
        "BU+PPA ID",
        "BU+Partner ID",
        "Business Unit",
        "Country Name",
        "Region Name",
        "Implementer Number",
        "Partner Name",
        "Implementer Type",
        "Agreement Number",
        "Project Start Date",
        "Project Implementation/ Completion Date",
        "Agreement Type",
        "Audit Worksplit Group",
        "Total Budget - (Agreement) - USD",
        "Total Installments Paid to Partners - USD",
        "Assigned Auditors",
        "Location of Project Documents for Auditor's Field Visit  (Name of City and Country)",
        "Location of Project Implementation Sites (Name of City and Country)",
        "E-mail of UNHCR Audit Focal Person",
        "E-mail of UNHCR Alternate Audit Focal Person",
        "E-mail & Telephone Number of Partner's Audit Focal Person",
        "E-mail &Telephone Number of Partner's Alternate Audit Focal Person",
        "Field Assessment",
        "Risk Rating",
        "Pre Check",
        "Int Check",
        "Fin Check",
        "Pre Comment",
        "Int Comment",
        "Fin Comment",
        "Reason for Selection",
        "Cash",
        "Emergency Goal",
        "Staff Costs",
        "OIOS",
        "Procurement",
        "Modified in Last 4 Yrs",
        "Other Accounts",
        "PQP Status",
        "Latest ICQ (Last 4yrs)",
        "Partner's Performance Assessment by Field Operations",
        "Status",
        "Audit By Gov",
    ]

    temp_dir = tempfile.gettempdir()
    file_name = datetime.strftime(datetime.now(), "%d-%m-%Y_%H:%M:%S.csv")
    file_path = os.path.join(temp_dir, file_name)

    fo_ids = FieldAssessmentProgress.objects.filter(
        pending_with="COMPLETED"
    ).values_list("field_office_id", flat=True)
    queryset = Agreement.objects.filter(
        ~Q(field_assessment__score__id=4), business_unit__field_office_id__in=fo_ids
    )
    cycle_qs = Cycle.objects.filter(year=year)
    cycle_status = cycle_qs.first().status.id
    agreements = queryset.filter(
        cycle__status_id__in=range(1, cycle_status + 1), cycle__year=year
    )

    if cycle_status in [PRE01, PRE02, PRE03]:
        fields.remove("int_comment__message")
        fields.remove("fin_comment__message")
        fields.remove("int_sense_check")
        fields.remove("fin_sense_check")
        excel_fields.remove("int_comment__message")
        excel_fields.remove("fin_comment__message")
        excel_fields.remove("int_sense_check")
        excel_fields.remove("fin_sense_check")
        excel_col_labels.remove("Int Comment")
        excel_col_labels.remove("Int Check")
        excel_col_labels.remove("Fin Comment")
        excel_col_labels.remove("Fin Check")

    elif cycle_status in [INT01, INT02]:
        fields.remove("fin_comment__message")
        fields.remove("fin_sense_check")
        excel_fields.remove("fin_comment__message")
        excel_fields.remove("fin_sense_check")
        excel_col_labels.remove("Fin Comment")
        excel_col_labels.remove("Fin Check")

    with open(file_path, "w", newline="", encoding="utf-8") as fp:
        writer = csv.writer(fp)
        writer.writerow(excel_col_labels)

        for row in agreements.values(*fields):
            bu_number = "{0}{1}".format(row["business_unit__code"], row["number"])
            BU_partner_id = "{0}{1}".format(
                row["business_unit__code"], row["partner__number"]
            )
            agrr = Agreement.objects.get(
                business_unit__code=row["business_unit__code"], number=row["number"]
            )

            documents = agrr.locations.filter(location_type="document").values(
                "city", "country__name"
            )
            document = ", ".join(
                ["{0} - {1}".format(i["city"], i["country__name"]) for i in documents]
            )

            sites = agrr.locations.filter(location_type="site").values(
                "city", "country__name"
            )
            site = ", ".join(
                ["{0} - {1}".format(i["city"], i["country__name"]) for i in sites]
            )

            unhcr_focal = agrr.focal_points.filter(
                user_type="unhcr", focal=True
            ).values_list("user__email", flat=True)
            unhcr_focal = ";".join(unhcr_focal)
            unhcr_alternate_focal = agrr.focal_points.filter(
                user_type="unhcr", alternate_focal=True
            ).values_list("user__email", flat=True)
            unhcr_alternate_focal = ";".join(unhcr_alternate_focal)
            partner_focal = agrr.focal_points.filter(
                user_type="partner", focal=True
            ).values_list("user__email", flat=True)
            partner_focal = ";".join(partner_focal)
            partner_alternate_focal = agrr.focal_points.filter(
                user_type="partner", alternate_focal=True
            ).values_list("user__email", flat=True)
            partner_alternate_focal = ";".join(partner_alternate_focal)

            row.update(
                {
                    "doc_location": document,
                    "site_location": site,
                    "unhcr_focal": unhcr_focal,
                    "unhcr_alternate_focal": unhcr_alternate_focal,
                    "partner_focal": partner_focal,
                    "partner_alternate_focal": partner_alternate_focal,
                    "bu_number": bu_number,
                    "BU_partner_id": BU_partner_id,
                }
            )

            row_data = [row[i] for i in excel_fields]
            writer.writerow(row_data)

    user_qs = User.objects.filter(id=user_id)
    if user_qs.exists():
        attachment = {}
        attachment["file_path"] = file_path

        send_notification(
            notification_type="agreement_list_export",
            obj=user_qs.first(),
            users=user_qs,
            context={
                "year": year,
                "subject": "Agreement List Export",
            },
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Agreement List Export", sent_as_email=False
                )
                send_email_with_attachment(user, [attachment])
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)
    os.remove(file_path)
