from rest_framework import generics, exceptions
from django.db.models import Q
from rest_framework.permissions import IsAuthenticated
from project.models import Agreement, SenseCheck, Cycle
from field.models import FieldAssessmentProgress
from project.serializers import PreliminaryListExportSerializer


class AgreementListExport(generics.ListCreateAPIView):
    serializer_class = PreliminaryListExportSerializer
    permission_classes = (IsAuthenticated,)
    resource_model = Agreement

    def get(self, request, *args, **kwargs):

        report_type = request.query_params.get("report_type", None)
        year = kwargs["year"]

        try:
            report_type = int(report_type)
        except Exception as e:
            error = "a valid report_type is a required"
            raise exceptions.APIException(error)

        if report_type is None or report_type not in range(1, 9):
            error = "a valid report_type is a required"
            raise exceptions.APIException(error)

        fo_ids = FieldAssessmentProgress.objects.filter(
            pending_with="COMPLETED"
        ).values_list("field_office_id", flat=True)
        queryset = Agreement.objects.filter(
            ~Q(field_assessment__score__id=4), business_unit__field_office_id__in=fo_ids
        )
        cycle_qs = Cycle.objects.filter(year=year)
        if cycle_qs.exists() == False:
            error = "No data available based on the given criteria"
            raise exceptions.APIException(error)
        inc = SenseCheck.objects.exclude(code="SC12").values_list("code", flat=True)
        cycle_status = cycle_qs.first().status.id
        agreements = queryset.filter(
            cycle__status_id__in=range(1, cycle_status + 1), cycle__year=year
        )

        if agreements.exists():
            agreement_list_export(year, report_type, request.user.id)
            return Response({"message": "An email will be send with the attachment"})
        else:
            error = "No data available based on the given criteria"
            raise exceptions.APIException(error)
