from django.apps import AppConfig


class QualityAssuranceConfig(AppConfig):
    name = "reports"
