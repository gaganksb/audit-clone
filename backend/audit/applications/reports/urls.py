from django.urls import path
from quality_assurance import views

urlpatterns = [
    path(
        "projects/export/<int:year>/",
        views.AgreementListExport.as_view(),
        name="export",
    ),
]
