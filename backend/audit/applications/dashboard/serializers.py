import os
import xlrd
from project.models import Cycle
from common.models import CommonFile
from rest_framework import serializers
from dashboard.models import DataImport
from dashboard.tasks import import_msrp_data, import_users, import_risk_rating_data
from quality_assurance.tasks import survey_import_command
from django.core.management import call_command


class DataImportSerialzier(serializers.ModelSerializer):

    import_file = serializers.FileField(write_only=True, required=False)
    is_delta = serializers.BooleanField(write_only=True, required=True)
    import_type = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = DataImport
        fields = "__all__"

    def validate(self, data):

        supported_imports = [
            "import_agreement_budgets",
            "import_agreement_report",
            "import_agreement_types",
            "import_agreements",
            "import_budget_refs",
            "import_cc_s",
            "import_currency",
            "import_delta",
            "import_goals",
            "import_implementer_types",
            "import_operations",
            "import_outputs",
            "import_partners",
            "import_pillars",
            "import_ppg_s",
            "import_selection_settings",
            "import_situations",
            "import_vendors",
            "import_survey",
        ]

        import_file = data.get("import_file", None)
        import_link = data.get("import_link", None)
        import_type = data.get("import_type", None)

        if import_link is None and import_file is None:
            error = "Import Link or Import File is Required"
            raise serializers.ValidationError({"message": error})
        if data.get("year", None) == None:
            error = "Year is a required field"
            raise serializers.ValidationError({"message": error})
        if import_type is not None and import_type not in supported_imports:
            error = "This type of MSRP data cant be imported"
            raise serializers.ValidationError({"message": error})
        return data

    def create_data_import(self, validated_data, import_file, data_types):

        data_import = DataImport.objects.create(
            import_file=import_file,
            import_link=validated_data.get("import_link", None),
            year=validated_data.get("year", None),
            data_types=data_types,
        )
        return data_import

    def create(self, validated_data):

        import_file = validated_data.pop("import_file", None)
        import_link = validated_data.pop("import_link", None)
        import_type = validated_data.pop("import_type", None)
        year = validated_data.get("year")
        is_delta = validated_data.get("is_delta", False)
        data_types = validated_data.get("data_types", [])

        if import_file is not None:
            file_obj = CommonFile.objects.create(file_field=import_file)

        for data_type in data_types:
            if import_file is not None and data_type == "msrp":
                cycle, created = Cycle.objects.get_or_create(year=year, budget_ref=year)
                if created:
                    cycle.status_id = 1
                    cycle.save()
                if is_delta and cycle.delta_enabled == False:
                    message = "Delta is not enabled yet"
                    raise serializers.ValidationError(message)
                import_msrp_data(file_obj.id, cycle.year, is_delta, import_type)
                data_import = self.create_data_import(
                    validated_data, file_obj, data_types
                )
                return data_import
            elif import_file is not None and data_type == "users":
                import_users(file_obj.id)
            elif import_file is not None and data_type == "survey":
                survey_import_command(year, file_obj.id)
            elif import_file is not None and data_type == "risk_rating":
                import_risk_rating_data(file_obj.id, year, "all")
        return validated_data
