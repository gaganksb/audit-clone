from django.db import models
from common.models import CommonFile
from django.db.models.fields import TextField
from audit.core.abstract_model import AbstractModel

from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.postgres.fields import ArrayField

MIN_YEAR = 1980
MAX_YEAR = 2050


class DataImport(AbstractModel):

    DATA_TYPES_CHOICES = (
        ("msrp", "MSRP"),
        ("cancelled_audits", "Cancelled Audits"),
        ("field_assessment", "Field Assessments"),
        ("consolidated_icq", "ICQ Scores"),
        ("consolidated_pqp", "PQP"),
        ("oios", "OIOS"),
        ("partner_modified", "Partner Modified"),
        ("adverse_disclaimer", "Adverse Disclaimer"),
        ("previous_audits", "Previous Audits"),
        ("partner_engagement", "Partner Engagement"),
        ("cancelled_audits", "Cancel Audits"),
        ("users", "Users With Role"),
        ("survey", "Survey"),
    )

    import_file = models.ForeignKey(
        CommonFile, on_delete=models.CASCADE, null=True, blank=True, default=None
    )
    import_link = models.CharField(max_length=500, null=True, blank=True, default=None)
    year = models.IntegerField(
        validators=[MinValueValidator(MIN_YEAR), MaxValueValidator(MAX_YEAR)]
    )
    data_types = ArrayField(models.TextField(default=None), blank=True, null=True)
    # is_delta = models.BooleanField(default=False)
    errors = TextField(null=True, default=None)

    def __str__(self):
        return f"CommonFile [{self.import_file.pk}, {self.import_file.file_field}]"
