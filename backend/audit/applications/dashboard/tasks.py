from common.models import CommonFile
import os
import xlrd
from background_task import background
from project.models import Agreement
from project.management.commands.project_agreement_import import (
    ImportAgreements,
    RiskRatingData,
    convert_to_string,
)

from background_task import background
from common.helpers import update_user_role
from auditing.models import Role as AuditRole, AuditAgency
from partner.models import Role as PartnerRole, Partner
from field.models import Role as FieldRole
from unhcr.models import Role as UNHCRRole
from django.db.models import Q

from account.models import User
from common.models import BusinessUnit
from common.factories import (
    AuditMemberFactory,
    PartnerMemberFactory,
    FieldMemberFactory,
    UNHCRMemberFactory,
)
from notify.helpers import send_notification, send_notification_to_users
from django.db import transaction
from notify.models import NotifiedUser


def get_delta_projects(file_id, year):
    file_obj = CommonFile.objects.filter(id=file_id).first()
    wb = xlrd.open_workbook(file_contents=file_obj.file_field.read())
    sheet = wb.sheet_by_name("HNIP999")

    delta_projects = []
    for i in range(1, sheet.nrows):
        try:
            bu = sheet.cell_value(i, 4)
            number = int(sheet.cell_value(i, 6))
            agreement = Agreement.objects.filter(
                business_unit__code=bu, number=number
            ).first()
            if agreement is None:
                delta_projects.append("{}{}".format(bu, number))
        except Exception as e:
            print(e)
    return delta_projects


@background
def import_msrp_data(file_id, year, is_delta, import_type):
    import_agreements = ImportAgreements(file_id)
    import_type = "all" if import_type is None else import_type
    if is_delta:
        delta_projects = get_delta_projects(file_id, year)
        import_agreements.import_delta = True
        import_agreements.delta_projects = delta_projects
        import_agreements.import_data(year, import_type, is_delta)
    else:
        import_agreements.import_data(year, import_type, is_delta)
        import_agreements.create_field_assessment(year, None)


@background
def import_users(file_id):
    file_obj = CommonFile.objects.filter(id=file_id).first()
    wb = xlrd.open_workbook(file_contents=file_obj.file_field.read())
    sheet = wb.sheet_by_name("users")
    errors = []
    for i in range(1, sheet.nrows):
        try:
            fullname = sheet.cell_value(i, 0)
            email = sheet.cell_value(i, 1)
            user_role = sheet.cell_value(i, 2)
            identifier = convert_to_string(sheet.cell_value(i, 3))
            name = sheet.cell_value(i, 4)
            user_type = sheet.cell_value(i, 5)
            user, created = User.objects.get_or_create(email=email)
            user.fullname = fullname
            user.save()
            if user_type == "AUDITOR":
                try:
                    agency = AuditAgency.objects.filter(
                        legal_name__icontains=name
                    ).first()
                    role = AuditRole.objects.filter(role=user_role).first()
                    auditmember = AuditMemberFactory(
                        user=user, audit_agency=agency, role=role
                    )
                    update_user_role("auditmember", auditmember)
                except Exception as e:
                    errors.append(
                        {
                            "identifier": identifier,
                            "name": name,
                            "email": email,
                            "user_type": user_type,
                            "error": str(e),
                        }
                    )
            if user_type == "PARTNER":
                try:
                    partner = Partner.objects.filter(Q(number=identifier)).first()
                    role = PartnerRole.objects.filter(role=user_role).first()
                    partnermember = PartnerMemberFactory(
                        user=user, partner=partner, role=role
                    )
                    update_user_role("partnermember", partnermember)
                except Exception as e:
                    errors.append(
                        {
                            "identifier": identifier,
                            "name": name,
                            "email": email,
                            "user_type": user_type,
                            "error": str(e),
                        }
                    )
            if user_type == "FO":
                try:
                    bu = BusinessUnit.objects.filter(code__iexact=name).first()
                    field_office = bu.field_office
                    role = FieldRole.objects.filter(role=user_role).first()
                    fieldmember = FieldMemberFactory(
                        user=user, field_office=field_office, role=role
                    )
                    update_user_role("fieldmember", fieldmember)
                except Exception as e:
                    errors.append(
                        {
                            "identifier": identifier,
                            "name": name,
                            "email": email,
                            "user_type": user_type,
                            "error": str(e),
                        }
                    )
            if user_type == "HQ":
                try:
                    role = UNHCRRole.objects.filter(role=user_role).first()
                    unhcrmember = UNHCRMemberFactory(user=user, role=role)
                    update_user_role("unhcrmember", unhcrmember)
                except Exception as e:
                    errors.append(
                        {
                            "identifier": identifier,
                            "name": name,
                            "email": email,
                            "user_type": user_type,
                            "error": str(e),
                        }
                    )
        except Exception as e:
            pass

    print(errors)
    if len(errors) > 0:
        context = {
            "custom_msg": "",
            "subject": "User Import Errors",
        }

        message = "<h3>Please check bellow the list of errors</h3><br/>"
        for i in errors:
            message += """
            <p>Identifier: {0}</p>
            <p>Name: {1}</p>
            <p>Email: {2}</p>
            <p>User Type: {3}</p>
            <p>Error Message: {4}</p>
            ---------------------------
            """.format(
                i["identifier"], i["name"], i["email"], i["user_type"], i["error"]
            )

        context.update(
            {
                "custom_msg": message,
            }
        )

        user, _ = User.objects.get_or_create(email="singhn@unicc.org")
        notification = send_notification(
            notification_type="custom_msg",
            obj=user,
            users=[user],
            context=context,
        )
        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="User Import Errors",
                    sent_as_email=False,
                )
                user_notified = send_notification_to_users(user)
        except Exception as e:
            print("Error while sending sent_project_selection_notification ", e)

@background
def import_risk_rating_data(file_id, year, import_type):
    import_type = "all" if import_type is None else import_type
    risk_rating_data = RiskRatingData(file_id)
    risk_rating_data.import_risk_rating_data(year)
