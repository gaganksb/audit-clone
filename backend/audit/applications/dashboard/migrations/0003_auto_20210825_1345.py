# Generated by Django 2.2.1 on 2021-08-25 13:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("dashboard", "0002_remove_dataimport_is_delta"),
    ]

    operations = [
        migrations.RenameField(
            model_name="dataimport",
            old_name="data_type",
            new_name="data_types",
        ),
    ]
