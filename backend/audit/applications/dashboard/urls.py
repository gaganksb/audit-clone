from django.urls import path
from dashboard import views


urlpatterns = [
    path("import-data/", views.DataImportAPI.as_view(), name="import-data"),
]
