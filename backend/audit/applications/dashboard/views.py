from rest_framework import generics
from dashboard.models import DataImport
from dashboard.serializers import DataImportSerialzier


class DataImportAPI(generics.ListCreateAPIView):

    queryset = DataImport.objects.all()
    serializer_class = DataImportSerialzier
