import csv
import datetime
from django.conf import settings
from common.models import BusinessUnit, Country, CostCenter, Currency
from field.models import FieldOffice, FieldMember, Role
from django.db.models import Q

from collections import defaultdict
from django.apps import apps
from field.enums import FieldRoleEnum, FIELD_ROLE_PERMISSIONS
from common.models import Permission


def create_field_roles():
    for role in FieldRoleEnum:
        _ = Role.objects.get_or_create(role=role.name)
        permissions = list(
            Permission.objects.filter(
                permission__in=FIELD_ROLE_PERMISSIONS[role]
            ).values_list("id", flat=True)
        )
        _[0].permissions.remove(*_[0].permissions.all())
        for permission in permissions:
            _[0].permissions.add(permission)


def alpha2_to_alpha3(code):
    file_path = settings.PROJECT_ROOT + "/../applications/field/excel/country-iso.csv"

    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f)
        for data in reader:
            name = data[0]
            alpha2 = data[1]
            alpha3 = data[2]
            if code == alpha2:
                return [name, alpha3]
        raise Exception("[ERROR] Invalid country code")


def import_field_offices():
    file_path = settings.PROJECT_ROOT + "/../applications/field/excel/field_office.csv"

    with open(file_path, "r") as f:
        next(f)  # skip headings
        reader = csv.reader(f)
        for data in reader:
            code = data[0]
            try:
                [name, alpha3] = alpha2_to_alpha3(code)
                country_qs = Country.objects.filter(code=alpha3)
                if country_qs.exists() is False:
                    continue
                country = country_qs.last()
                fo_qs = FieldOffice.objects.filter(name__icontains=country.name)
                if fo_qs.exists() is True:
                    continue
                field_office = FieldOffice.objects.create(name=country.name)
                BusinessUnit.objects.filter(countries__code=country.code).update(
                    field_office=field_office
                )
            except Exception as error:
                print(error)
        # assign a default field office to business units that still do not have a field office
        BusinessUnit.objects.filter(field_office=None).update(
            field_office=FieldOffice.objects.get(pk=1)
        )
