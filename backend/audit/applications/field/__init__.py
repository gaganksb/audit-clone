from model_utils.choices import Choices


ALTERNATIVE = "alternative"
ORIGINAL = "original"
FOCAL_POINT_TYPE = Choices(
    (ALTERNATIVE, "ALTERNATIVE", "alternative"), (ORIGINAL, "ORIGINAL", "original")
)
