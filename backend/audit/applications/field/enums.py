from enum import unique, auto

from common.enums import AutoNameEnum, PermissionListEnum


@unique
class FieldRoleEnum(AutoNameEnum):
    REVIEWER = auto()
    APPROVER = auto()


FIELD_ROLE_PERMISSIONS = {
    FieldRoleEnum.REVIEWER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
    FieldRoleEnum.APPROVER: [
        PermissionListEnum.VIEW_DASHBOARD.name,
    ],
}
