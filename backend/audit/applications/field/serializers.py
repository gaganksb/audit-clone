from django.apps import apps
from django.db.models import Q
from rest_framework import serializers, exceptions
from django.urls import reverse
from field.models import (
    FieldOffice,
    Role as FieldRole,
    FieldMember,
    FieldAssessment,
    Score,
    FieldAssessmentProgress,
)
from common.models import BusinessUnit, Comment
from common.serializers import BusinessUnitSerializer
from project.models import Agreement
from common.serializers import CommonCommentSerializer, CommonUserSerializer
from django.db.models import Count, Q
from project.models import AgreementMember
from field.tasks import (
    notify_start_project_selection,
    assessment_completed_from_fo_reviewer_notify,
    update_risk_rating_bg,
)


class FieldOfficeSerializer(serializers.ModelSerializer):
    business_unit = serializers.SerializerMethodField()

    def get_business_unit(self, instance):
        bu = BusinessUnit.objects.filter(field_office__name=instance.name)
        serializer = BusinessUnitSerializer(bu.first(), context=self.context)
        return serializer.data

    class Meta:
        model = FieldOffice
        fields = "__all__"


class FieldOfficeProgressSerializer(serializers.ModelSerializer):
    cycle_instance = apps.get_model("project.Cycle")

    class Meta:
        model = FieldOffice
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        fo_ins = FieldOffice.objects.filter(id=instance.id).last()
        return serializer


class FieldRoleSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        return self._context["request"].build_absolute_uri(
            reverse("fields:role-detail", kwargs={"pk": obj.pk})
        )

    class Meta:
        model = FieldRole
        fields = "__all__"
        extra_fields = [
            "url",
        ]
        read_only_fields = [
            "role",
        ]


class FieldMemberSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name="accounts:user-detail")
    user_id = serializers.ReadOnlyField(source="user.id")
    user = serializers.ReadOnlyField(source="user.fullname")
    email = serializers.ReadOnlyField(source="user.email")
    field_office = serializers.ReadOnlyField(source="field_office.name")
    role = serializers.ReadOnlyField(source="role.role")

    class Meta:
        model = FieldMember
        fields = ("user_id", "user", "email", "field_office", "role", "url")


class FieldMemberShortSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source="user.fullname")
    office = serializers.ReadOnlyField(source="field_office.name")
    role = serializers.ReadOnlyField(source="role.role")
    UserTypeRole = apps.get_model("account.UserTypeRole")

    class Meta:
        model = FieldMember
        fields = (
            "id",
            "user",
            "office",
            "role",
        )

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        serializer.update(
            {
                "is_default": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .is_default,
                "user_type_role_id": self.UserTypeRole.objects.filter(
                    resource_model=getattr(instance, "_meta").model_name,
                    resource_app_label=getattr(instance, "_meta").app_label,
                    resource_id=instance.id,
                )
                .last()
                .id,
            }
        )
        return serializer


class FieldMemberDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldMember
        fields = [
            "id",
        ]

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            if key == "role":
                value = FieldRole.objects.filter(role=value).last()
            if key == "office":
                value = FieldOffice.objects.filter(name=value).last()
                key = "field_office"
            if key == "name":
                instance.update_user_info(key, value)
                continue
            setattr(instance, key, value)
        instance.update()
        return instance

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        user = instance.user
        role = instance.role
        office = instance.field_office
        serializer.update(
            {
                "name": user.fullname,
                "email": user.email,
                "role": {"role": role.role, "id": role.id},
                "office": {"id": office.id, "name": office.name},
            }
        )
        return serializer


class ScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Score
        fields = "__all__"


class AgreementMemberSerializer(serializers.ModelSerializer):
    user = CommonUserSerializer()

    class Meta:
        model = AgreementMember
        fields = "__all__"


class FieldAssessmentSerializer(serializers.ModelSerializer):
    comment = CommonCommentSerializer(read_only=True)

    class Meta:
        model = FieldAssessment
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        agreement = instance.field_assessments.all()

        aggrement_members = AgreementMemberSerializer(
            agreement.first().focal_points.all(), many=True
        )

        response = {}
        if len(aggrement_members.data) > 0:
            for i in aggrement_members.data:
                if i["user_type"] == "unhcr" and i["focal"] == True:
                    response["unhcr_focal_point"] = i["user"]
                if i["user_type"] == "unhcr" and i["alternate_focal"] == True:
                    response["unhcr_alt_focal_point"] = i["user"]
                if i["user_type"] == "partner" and i["focal"] == True:
                    response["partner_focal_point"] = i["user"]
                if i["user_type"] == "partner" and i["alternate_focal"] == True:
                    response["partner_alt_focal_point"] = i["user"]

        if agreement.exists():
            data = {
                "business_unit": {
                    "code": agreement.first().business_unit.code,
                },
                "partner": {
                    "id": agreement.first().partner.id,
                    "legal_name": agreement.first().partner.legal_name,
                    "budget_ref": agreement.first().cycle.budget_ref,
                },
                "score": {
                    "id": instance.score.id,
                    "value": instance.score.value,
                    "description": instance.score.description,
                },
                "agreement": {
                    "id": agreement.first().id,
                    "number": agreement.first().number,
                    "delta": agreement.first().delta,
                    "fin_sense_check": agreement.first().fin_sense_check,
                },
                "cycle": {
                    "id": agreement.first().cycle.id,
                    "year": agreement.first().cycle.year,
                    "delta_enabled": agreement.first().cycle.delta_enabled,
                    "delta_field_assessment_deadline": agreement.first().cycle.field_assessment_deadline,
                },
                "focal_points": response,
            }
            serializer.update(**data)
        return serializer


class FieldAssessmentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldAssessment
        fields = "__all__"
        read_only_fields = ("partner", "business_unit")


class BusinessUnitStatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessUnit
        fields = [
            "id",
            "code",
        ]

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        fas_finished_count = FieldAssessment.objects.filter(
            Q(business_unit=instance) & ~Q(score_id=4)
        ).count()
        fas_total_count = FieldAssessment.objects.filter(
            Q(business_unit=instance)
        ).count()

        ser = {
            "business_unit": serializer,
            "total": fas_total_count,
            "completed": fas_finished_count,
            "finished": False if fas_finished_count < fas_total_count else True,
        }
        return ser


class FieldMemberAssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldOffice
        fields = "__all__"

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        field_member_qs = FieldMember.objects.filter(
            fieldmemberassignment__field_office=instance
        )
        field_member_res = FieldMemberShortInfoSerializer(
            field_member_qs, many=True
        ).data

        serializer.update({"field_members": field_member_res})
        return serializer


class FieldMemberShortInfoSerializer(serializers.ModelSerializer):
    fullname = serializers.ReadOnlyField(source="user.fullname")
    email = serializers.ReadOnlyField(source="user.email")
    role = serializers.ReadOnlyField(source="role.role")

    class Meta:
        model = FieldMember
        fields = ("id", "fullname", "email", "role")

    def to_representation(self, instance):
        serializer = super().to_representation(instance)
        return serializer


class FieldAssessmentProgressListSerializer(serializers.ModelSerializer):
    comments = CommonCommentSerializer(many=True)

    class Meta:
        model = FieldAssessmentProgress
        fields = [
            "budget_ref",
            "pending_with",
            "field_office",
            "comments",
        ]
        extra_kwargs = {"field_office": {"required": False}}

    def create(self, validated_data):

        comment_data = validated_data.pop("comments")[0]
        request = self.context.get("request")
        membership = request.user.membership

        if membership["type"] == "HQ" and validated_data["pending_with"] != "COMPLETED":
            raise exceptions.APIException(
                "You are not authorized to perform this action"
            )
        if (
            membership["type"] == "FO"
            and membership["role"] == "APPROVER"
            and validated_data["pending_with"] != "HQ"
        ):
            raise exceptions.APIException(
                "You are not authorized to perform this action"
            )
        if (
            membership["type"] == "FO"
            and membership["role"] == "REVIEWER"
            and validated_data["pending_with"] != "APPROVER"
        ):
            raise exceptions.APIException(
                "You are not authorized to perform this action"
            )

        comment = Comment.objects.create(user=request.user, **comment_data)
        if request.user.membership["type"] == "HQ":
            fa_progress = FieldAssessmentProgress.objects.filter(
                budget_ref=validated_data["budget_ref"],
            )
        else:
            fa_progress = FieldAssessmentProgress.objects.filter(
                budget_ref=validated_data["budget_ref"],
                field_office_id=validated_data["field_office"],
            )

        if fa_progress.exists():
            fa_progress.update(pending_with=validated_data["pending_with"])
            if (
                validated_data["pending_with"] == "COMPLETED"
                and request.user.membership["type"] == "HQ"
            ):
                # Notify HQ and Field office
                notify_start_project_selection(validated_data["budget_ref"], "both")
                # Run risk_rating calculation and update agreement table with the new_risk rating
                update_risk_rating_bg(validated_data["budget_ref"])
            if (
                validated_data["pending_with"] == "APPROVER"
                and request.user.membership["type"] == "FO"
            ):
                # Notify field office approver that reviewer has completed field assessment
                assessment_completed_from_fo_reviewer_notify(
                    validated_data["field_office"].id
                )
            return fa_progress.first()
        else:
            field_assignment_progress = FieldAssessmentProgress.objects.create(
                **validated_data
            )
            field_assignment_progress.comments.add(comment)
            field_assignment_progress.save()
            if (
                validated_data["pending_with"] == "APPROVER"
                and request.user.membership["type"] == "FO"
            ):
                # Notify field office approver that reviewer has completed field assessment
                assessment_completed_from_fo_reviewer_notify(
                    validated_data["field_office"].id
                )
            return field_assignment_progress

    def update(self, instance, validated_data):
        comment_data = validated_data.pop("comment", None)

        if comment_data is not None:
            comment = Comment.objects.filter(id=instance.comment.id).update(
                description=comment_data["message"]
            )

        instance.budget_ref = validated_data.get("budget_ref", instance.budget_ref)
        instance.pending_with = validated_data.get(
            "pending_with", instance.pending_with
        )
        instance.field_office = validated_data.get(
            "field_office", instance.field_office
        )
        instance.save()
        return instance


class FieldAssessmentProgressSerializer(serializers.ModelSerializer):
    comments = CommonCommentSerializer(read_only=True, many=True)

    class Meta:
        model = FieldAssessmentProgress
        fields = ["pending_with", "comments"]


class FieldOfficeStatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldOffice
        fields = ["id", "name"]

    def to_representation(self, instance):
        request = self.context.get("request")
        budget_ref = request.query_params.get("budget_ref", None)
        field_assessments = Agreement.objects.filter(
            cycle__budget_ref=budget_ref, business_unit__field_office=instance
        ).values_list("field_assessment_id", flat=True)
        small_stats = FieldAssessment.objects.filter(
            id__in=field_assessments
        ).aggregate(
            total=Count("id"), completed=Count("score_id", filter=~Q(score_id=4))
        )
        progress = FieldAssessmentProgress.objects.filter(
            field_office_id=instance.id, budget_ref=budget_ref
        )
        data = FieldAssessmentProgressSerializer(progress, many=True).data
        return {
            "field_office": {"id": instance.id, "name": instance.name},
            **small_stats,
            "pending_with": None if len(data) == 0 else data[0],
        }


class FieldAssessmentCanProceedSerializer(serializers.Serializer):
    canproceed = serializers.BooleanField(required=False)


class SetDeltaDeadlineSerializer(serializers.Serializer):
    field_assessment_deadline = serializers.DateField(required=True)

    def update(self, instance, validated_data):
        instance.field_assessment_deadline = validated_data["field_assessment_deadline"]
        instance.save()
        return instance
