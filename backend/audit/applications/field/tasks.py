import os
from django.db import transaction
from notify.models import NotifiedUser
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from notify.helpers import (
    send_notification,
    send_notification_to_users,
    send_email_with_attachment,
)
from .models import (
    FieldAssessment,
    NO_FIELD_RESPONSE,
    FieldAssessment,
    FieldMember,
    FieldAssessmentProgress,
)
from background_task import background
from project.models import Agreement, AgreementMember, SenseCheck, Cycle
from unhcr.models import UNHCRMember
from account.models import User
from common.models import Comment
import csv
import tempfile
from datetime import datetime
from project.helpers import update_project_selection_reason
from django.db.models import Case, When, Value, IntegerField, Q

NO_FIELD_RESPONSE_RECEIVED = 4
PRE_01 = 1
HIGH_RISK = 3


@background(schedule=10)
def check_field_assessment_remind_date():
    """
    check at least 1 day late after remind_date
    :return:
    """
    dt = timezone.now().date()
    fa_qs = FieldAssessment.objects.filter(
        remind_date__lte=dt - relativedelta(days=1),
        score_id=NO_FIELD_RESPONSE,
        is_remind=True,
    )
    if fa_qs.exists() is True:
        fa_qs.update(is_remind=False)

    notified_users = NotifiedUser.objects.filter(
        notification__name="Remind Field Office",
        last_modified_date=dt - relativedelta(days=1),
        sent_as_email=True,
    )
    if notified_users.exists() is True:
        notified_users.update(sent_as_email=False)


@background(schedule=5)
def send_reminder():
    try:
        with transaction.atomic():
            user = NotifiedUser.objects.select_for_update().filter(
                notification__name="Remind Field Office", sent_as_email=False
            )
            send_notification_to_users(user)
    except Exception as e:
        print("Error while sending notification ", e)


@background
def notify_start_project_selection(year, user_type):
    """
    Notify HQ user and field office User that project selection as been started
    based on the user_type -> "HQ", "FO", "BOTH"
    """

    def notify_hq():
        # Notify HQ user
        hq_members = UNHCRMember.objects.all().values_list("user_id", flat=True)
        users = User.objects.filter(id__in=hq_members)
        if users.exists():
            context = {"year": year, "subject": "Project Selection Started"}
            send_notification(
                notification_type="project_selection_started",
                obj=users.first(),
                users=users,
                context=context,
            )
            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Project Selection Started",
                        sent_as_email=False,
                    )
                    send_notification_to_users(user)
            except Exception as e:
                print("Error while sending notification ", e)

    def notify_fo():
        # Notify FO user
        field_office = Agreement.objects.filter(cycle__year=year).values_list(
            "business_unit__field_office_id", flat=True
        )
        field_members = FieldMember.objects.filter(
            field_office_id__in=field_office
        ).values_list("user_id", flat=True)
        users = User.objects.filter(id__in=field_members)
        context = {"year": year, "subject": "Field Assessment Ended"}
        if users.exists():
            send_notification(
                notification_type="field_assessment_ended",
                obj=users.first(),
                users=users,
                context=context,
            )

            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Field Assessment Ended", sent_as_email=False
                    )
                    send_notification_to_users(user)
            except Exception as e:
                print("Error while sending notification ", e)

    if user_type == "HQ":
        notify_hq()
    elif user_type == "FO":
        notify_fo()
    else:
        notify_hq()
        notify_fo()


def update_risk_rating(year):
    cycle = Cycle.objects.filter(year=year).first()
    if cycle is not None:
        agreements = Agreement.objects.filter(cycle__year=year)
        if cycle.delta_enabled:
            agreements = agreements.exclude(fin_sense_check=True)
        agreements.update(pre_sense_check=False, risk_rating=None)
        for agreement in agreements:
            risk_ratings = agreement.get_risk_rating
            agreement.risk_rating = risk_ratings.get("risk_rating", None)
            agreement.risk_rating_calc = risk_ratings
            agreement.save()

        ## Update not auditable projects
        agreements.update(reason=None)
        not_auditable_ids = (
            agreements.annotate(
                auditable=Case(
                    When(
                        Q(
                            partner__implementer_type__implementer_type__in=[
                                "LOCALNGO",
                                "INTLNGO",
                                "GOVT",
                            ]
                        )
                        & ~Q(
                            agreement_type__description__in=[
                                "NFP PFA Agreement",
                                "UN Agreement",
                            ]
                        ),
                        then=Value(1),
                    ),
                    default=Value(0),
                    output_field=IntegerField(),
                ),
            )
            .filter(auditable=False)
            .values("id")
        )

        not_auditable_comment = (
            "Not Auditable (UN and National Fundraising Partners agreements)"
        )
        reason = SenseCheck.objects.filter(code="SC13").first()
        comment = Comment.objects.create(message=not_auditable_comment)
        agreements.filter(id__in=not_auditable_ids).update(
            pre_sense_check=False,
            int_sense_check=False,
            fin_sense_check=False,
            reason=reason,
            pre_comment=comment,
            is_auditable=False
        )
        update_project_selection_reason(year)


@background
def update_risk_rating_bg(year):
    update_risk_rating(year)


@background
def field_assessment_hq(year, comment, user_id):
    cycle = Cycle.objects.filter(year=year).first()
    if cycle is not None:
        comment = Comment.objects.create(user_id=user_id, **comment)
        default_comment = Comment.objects.create(
            user_id=user_id, message="Field response not provided before deadline"
        )
        field_offices = (
            Agreement.objects.filter(cycle__year=year)
            .distinct()
            .values_list("business_unit__field_office_id", flat=True)
        )
        agreements = Agreement.objects.filter(
            cycle__year=year,
            field_assessment__score=NO_FIELD_RESPONSE_RECEIVED
        )

        if cycle.delta_enabled:
            agreements = agreements.exclude(fin_sense_check=True)

        # Add a default focal point to all the agreements
        for agreement in agreements:
            if (
                agreement.field_assessment.score_id == NO_FIELD_RESPONSE_RECEIVED
                and agreement.focal_points.all().exists() == False
            ):
                fo_member = FieldMember.objects.filter(
                    field_office_id=agreement.business_unit.field_office_id,
                    role__role="APPROVER",
                )
                if fo_member.exists():
                    agreement_member, created = AgreementMember.objects.get_or_create(
                        user_type="unhcr", focal=True, user=fo_member.first().user
                    )
                    agreement.focal_points.add(agreement_member)
                    agreement.status_id = PRE_01
                    agreement.save()

        # update field assessment with highest score, score_id 3 value is 5
        agreements = agreements.values_list("field_assessment_id", flat=True)
        field_assessment = FieldAssessment.objects.filter(id__in=agreements)
        field_assessment.update(score_id=HIGH_RISK)

        # update field assessment progress set it to completed
        for office in field_offices:
            progress, created = FieldAssessmentProgress.objects.get_or_create(
                budget_ref=year, field_office_id=office
            )
            if created == True:
                progress.comments.add(default_comment)
            else:
                progress.comments.add(comment)
            progress.pending_with = "COMPLETED"
            progress.save()

        # send notification to hq users
        notify_start_project_selection(year, "HQ")

        # Run risk_rating calculation and update agreement table with the new_risk rating
        update_risk_rating(year)


@background(schedule=5)
def remind_interim_list(year, field_office):
    # send notification to FO to complete the pending action in interim list
    user_id = FieldMember.objects.filter(field_office_id=field_office).values("user_id")
    field_users = User.objects.filter(id__in=user_id)
    if field_users.exists():
        send_notification(
            notification_type="remind_interim_list",
            obj=field_users.first(),
            users=field_users,
            context={"year": year, "subject": "Remind Interim List"},
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Remind Interim List", sent_as_email=False
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)


@background(schedule=1)
def assessment_completed_from_fo_reviewer_notify(field_office_id):

    field_members = FieldMember.objects.filter(
        field_office_id=field_office_id, role__role="APPROVER"
    )
    field_users = User.objects.filter(
        id__in=field_members.values_list("user_id", flat=True)
    )
    if field_users.exists():
        send_notification(
            notification_type="field_assessment_completed_reviewer",
            obj=field_users.first(),
            users=field_users,
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Field Assessment Completed From Reviewer",
                    sent_as_email=False,
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)


def export_field_assessment_progress(year):
    fields = [
        "business_unit__code",
        "business_unit__field_office_id",
        "business_unit__field_office__name",
    ]

    fos = (
        Agreement.objects.filter(cycle__year=year)
        .distinct("business_unit__field_office")
        .values(*fields)
    )

    columns = [
        "Business Unit",
        "Field Office",
        "Total Projects",
        "Peding With",
        "Completed From FO",
    ]
    temp_dir = tempfile.gettempdir()
    file_name = "Field-Assessment-Progress-{}".format(
        datetime.strftime(datetime.now(), "%d-%m-%Y_%H:%M:%S.csv")
    )
    file_path = os.path.join(temp_dir, file_name)

    with open(file_path, "w") as fp:
        writer = csv.writer(fp)
        writer.writerow(columns)
        for fo in fos:
            fo_id = fo["business_unit__field_office_id"]
            agreements = Agreement.objects.filter(
                cycle__year=year, business_unit__field_office_id=fo_id
            )
            progress = FieldAssessmentProgress.objects.filter(
                field_office_id=fo_id, budget_ref=year
            ).first()

            pendint_with = "FO REVIEWER"
            completed = "No"
            if progress is not None:
                pendint_with = progress.pending_with
                if pendint_with == "COMPLETED" or pendint_with == "HQ":
                    completed = "Yes"
                    pendint_with = "HQ"
                else:
                    pendint_with = "FO {}".format(pendint_with)

            row_data = (
                fo["business_unit__code"],
                fo["business_unit__field_office__name"],
                agreements.count(),
                pendint_with,
                completed,
            )
            writer.writerow(row_data)

    return file_path


@background(schedule=1)
def export_field_assessment(year, user_id):

    fields = [
        "business_unit__code",
        "partner__number",
        "partner__legal_name",
        "number",
        "field_assessment__score__value",
        "field_assessment__score__description",
    ]
    columns = [
        "BU+PPA ID",
        "Business Unit",
        "Partner Implement Number",
        "Partner Name",
        "Agreement Number",
        "Field Assessment Value",
        "Field Assessment Desc",
        "E-mail of UNHCR Audit Focal Person",
        "E-mail of UNHCR Alternate Audit Focal Person",
        "E-mail of Partner's Audit Focal Person",
        "E-mail of Partner's Alternate Audit Focal Person",
    ]

    items = Agreement.objects.filter(
        cycle__year=year, field_assessment__isnull=False
    ).values(*fields)

    temp_dir = tempfile.gettempdir()
    file_name = datetime.strftime(datetime.now(), "%d-%m-%Y_%H:%M:%S.csv")
    file_path = os.path.join(temp_dir, file_name)

    with open(file_path, "w") as fp:
        writer = csv.writer(fp)
        writer.writerow(columns)
        for obj in items:
            row_data = []
            bu_ppa = "{}{}".format(obj["business_unit__code"], obj["number"])
            row_data.append(bu_ppa)
            row_data.extend(obj.values())

            focal_points = (
                Agreement.objects.get(
                    business_unit__code=obj["business_unit__code"], number=obj["number"]
                )
                .focal_points.all()
                .values("user_type", "focal", "alternate_focal", "user__email")
            )

            unhcr_focal = [
                i["user__email"]
                for i in focal_points
                if i["focal"] == True and i["user_type"] == "unhcr"
            ]
            unhcr_alternate_focal = [
                i["user__email"]
                for i in focal_points
                if i["alternate_focal"] == True and i["user_type"] == "unhcr"
            ]
            partner_focal = [
                i["user__email"]
                for i in focal_points
                if i["focal"] == True and i["user_type"] == "partner"
            ]
            partner_alternate_focal = [
                i["user__email"]
                for i in focal_points
                if i["alternate_focal"] == True and i["user_type"] == "partner"
            ]

            row_data.extend(unhcr_focal)
            row_data.extend(unhcr_alternate_focal)
            row_data.extend(partner_focal)
            row_data.extend(partner_alternate_focal)
            writer.writerow(row_data)

    progress_file_path = export_field_assessment_progress(year)
    user_qs = User.objects.filter(id=user_id)
    if user_qs.exists():
        attachments = []
        for a in [file_path, progress_file_path]:
            attachments.append({"file_path": a})

        send_notification(
            notification_type="fieldassessment_export",
            obj=user_qs.first(),
            users=user_qs,
            context={
                "year": year,
                "subject": "Field Assessment Export",
            },
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Field Assessment Export", sent_as_email=False
                )
                send_email_with_attachment(user, attachments)
        except Exception as e:
            print("Error while sending remind_interim_list notification ", e)
    os.remove(file_path)
    os.remove(progress_file_path)
