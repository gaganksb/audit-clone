from django.contrib import admin
from .models import (
    FieldOffice,
    FieldMember,
    Role,
    FieldMemberAssignment,
    FieldOfficeRoleNotification,
    Score,
    FieldAssessment,
    FieldAssessmentProgress,
)


class FieldOfficeAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    list_display = ("name",)
    ordering = ("name",)


class FieldMemberAdmin(admin.ModelAdmin):
    search_fields = ("user__email", "field_office__name", "role__name")
    list_display = (
        "user",
        "role",
    )
    ordering = ("user__email",)
    raw_id_fields = (
        "user",
        "field_office",
    )


class RoleAdmin(admin.ModelAdmin):
    search_fields = ("role__role",)
    filter_horizontal = ("permissions",)
    readonly_fields = ("role",)

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class FieldMemberAssignmentAdmin(admin.ModelAdmin):
    list_display = (
        "field_office_id",
        "field_office__name",
        "field_member_id",
        "field_member__user__email",
        "field_member__role",
    )
    search_fields = (
        "field_office__name",
        "field_member__user__email",
    )

    @staticmethod
    def field_office__name(obj):
        return obj.field_office.name

    @staticmethod
    def field_member__user__email(obj):
        return obj.field_member.user.email

    @staticmethod
    def field_member__role(obj):
        return obj.field_member.role


admin.site.register(FieldOffice, FieldOfficeAdmin)
admin.site.register(FieldMember, FieldMemberAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(FieldMemberAssignment, FieldMemberAssignmentAdmin)
admin.site.register(FieldOfficeRoleNotification)
admin.site.register(Score)
admin.site.register(FieldAssessment)
admin.site.register(FieldAssessmentProgress)
