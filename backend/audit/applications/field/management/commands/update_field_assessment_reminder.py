from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from notify.models import NotifiedUser
from django.utils import timezone
from dateutil.relativedelta import relativedelta
from .models import FieldAssessment, NO_FIELD_RESPONSE
from background_task import background


class Command(BaseCommand):
    help = "Creates a set of FieldMemberAssignment."

    def handle(self, *args, **options):

        """
        check at least 1 day late after remind_date
        :return:
        """
        dt = timezone.now().date()
        fa_qs = FieldAssessment.objects.filter(
            remind_date__lte=dt - relativedelta(days=1),
            score_id=NO_FIELD_RESPONSE,
            is_remind=True,
        )
        if fa_qs.exists() is True:
            fa_qs.update(is_remind=False)

        notified_users = NotifiedUser.objects.filter(
            notification__name="Remind Field Office",
            last_modified_date=dt - relativedelta(days=1),
            sent_as_email=True,
        )
        if notified_users.exists() is True:
            notified_users.update(sent_as_email=False)

        self.stdout.write("Updated field assessment reminder")
