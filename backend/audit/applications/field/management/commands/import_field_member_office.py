from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from field.models import FieldMemberAssignment
import os
import csv


class Command(BaseCommand):
    help = "Creates a set of FieldMemberAssignment."

    def handle(self, *args, **options):
        fp = os.path.join(
            settings.BASE_DIR,
            "audit",
            "applications",
            "field/excel/field_member_office.csv",
        )

        with open(fp, "r") as f:
            next(f)
            reader = csv.reader(f, delimiter=",")
            for r in reader:
                try:
                    fma = FieldMemberAssignment(
                        field_member_id=r[0], field_office_id=r[1]
                    )
                    fma.save()
                except:
                    self.stdout.write(
                        "field_member_id: {} --- field_office_id: {}".format(r[0], r[1])
                    )
        self.stdout.write("Create FieldMemberAssignment.")
