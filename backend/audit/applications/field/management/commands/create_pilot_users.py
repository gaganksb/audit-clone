from __future__ import absolute_import

from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.hashers import (
    make_password,
)
from account.models import User, UserTypeRole, mapping_user_type
from field.models import FieldOffice, FieldMember, Role
import os
import csv


class Command(BaseCommand):
    help = "Creates pilot users"

    def handle(self, *args, **options):
        fp = os.path.join(
            settings.BASE_DIR,
            "audit",
            "applications",
            "field/excel/list_pilot_users.csv",
        )
        with open(fp, "r") as f:
            next(f)
            reader = csv.reader(f, delimiter=",")
            for r in reader:
                fullname = r[0]
                email = r[1].lower()
                office = r[2]
                temp_password = User().gen_pass
                user = User(
                    fullname=fullname,
                    email=email,
                    password=make_password(temp_password),
                )
                user.save()
                field_office_qs = FieldOffice.objects.filter(name__icontains=office)
                if field_office_qs.exists() is False:
                    continue

                role_qs = Role.objects.filter(role="REVIEWER")

                field_member = FieldMember(
                    user=user,
                    role=role_qs.last(),
                    field_office=field_office_qs.last(),
                    temporary_password=temp_password,
                )
                field_member.save()
                user_type_account = UserTypeRole.objects.filter(
                    user=user, resource_model="fieldmember", resource_id=field_member.id
                ).last()
                user_type_account.is_default = True
                user_type_account.save()
