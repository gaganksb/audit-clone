from rest_framework.response import Response
from rest_framework import generics, status, exceptions
from rest_framework.permissions import IsAuthenticated
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from common.permissions import IsUNHCR, IsFO
from django.db.models import Count, Q
from common.serializers import NotificationSerializer
from project.models import Agreement, SelectionSettings, Cycle
from field.models import (
    FieldOffice,
    Role as FieldRole,
    FieldMember,
    FieldAssessment,
    NO_FIELD_RESPONSE,
    FieldMemberAssignment,
    FieldAssessmentProgress,
)
from field.serializers import *
from rest_framework import filters
from .tasks import send_reminder
from utils.rest.code import code
from dateutil.parser import parse
import datetime
from common.pagination import SmallPagination

from .filters import *
from django_filters.rest_framework import DjangoFilterBackend

from notify.helpers import send_notification
from notify.models import Notification

from common.models import Comment
from project.models import AgreementMember
from field.tasks import (
    field_assessment_hq,
    remind_interim_list,
    export_field_assessment,
)
from account.models import User
from django.db import transaction
from partner.models import PartnerMember


class FieldOfficeList(generics.ListCreateAPIView):
    serializer_class = FieldOfficeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        name = self.request.query_params.get("legal_name")
        try:
            if name is not None:
                return FieldOffice.objects.all().filter(Q(name__icontains=name))
            return FieldOffice.objects.all()
        except Exception:
            return FieldOffice.objects.none()


class FieldOfficeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FieldOffice.objects.all()
    serializer_class = FieldOfficeSerializer
    permission_classes = (IsAuthenticated,)


class FieldRoleList(generics.ListCreateAPIView):
    queryset = FieldRole.objects.all()
    serializer_class = FieldRoleSerializer
    permission_classes = (IsAuthenticated,)


class FieldRoleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FieldRole.objects.all()
    serializer_class = FieldRoleSerializer
    permission_classes = (IsAuthenticated,)


class FieldMemberList(generics.ListAPIView):
    queryset = FieldMember.objects.all()
    serializer_class = FieldMemberSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend,)
    filter_class = FieldMemberFilter


class FieldMemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FieldMember.objects.all()
    serializer_class = FieldMemberDetailSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    resource_model = FieldMember

    def get(self, request, pk=None, *args, **kwargs):
        field_member_qs = self.resource_model.objects.filter(pk=pk)
        if field_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )

        field_member = field_member_qs.last()
        res = self.serializer_class(field_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def patch(self, request, pk=None, *args, **kwargs):
        field_member_qs = self.resource_model.objects.filter(pk=pk)
        if field_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        field_member = field_member_qs.last()
        serializer = self.serializer_class(field_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        field_member = serializer.update(field_member, validated_data=request.data)
        res = self.serializer_class(field_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)

    def put(self, request, pk=None, *args, **kwargs):
        field_member_qs = self.resource_model.objects.filter(pk=pk)
        if field_member_qs.exists() is False:
            return Response(
                {
                    "status": status.HTTP_404_NOT_FOUND,
                    "code": "E_NOT_FOUND",
                    "message": code["E_NOT_FOUND"],
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        field_member = field_member_qs.last()
        serializer = self.serializer_class(field_member, data=request.data)

        if serializer.is_valid() is False:
            return Response(
                {"errors": serializer.errors}, status=status.HTTP_400_BAD_REQUEST
            )

        field_member = serializer.update(field_member, validated_data=request.data)
        res = self.serializer_class(field_member, many=False)
        return Response(res.data, status=status.HTTP_200_OK)


class FieldAssessmentsAPIView(generics.ListAPIView):
    serializer_class = FieldAssessmentSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    queryset = FieldAssessment.objects.all().order_by("-created_date")
    filter_backends = (
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = FieldAssessmentListFilter
    pagination_class = SmallPagination

    ordering_fields = [
        "score__description",
        "score__value",
        "field_assessments__partner__legal_name",
        "field_assessments__business_unit__code",
        "field_assessments__number",
        "field_assessments__delta",
    ]

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == "FO":
            agreements = Agreement.objects.filter(
                business_unit__field_office_id=membership["field_office"]["id"]
            )
            return self.queryset.filter(
                id__in=agreements.values_list("field_assessment", flat=True)
            )
        return self.queryset


class FieldAssessmentAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = FieldAssessmentSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    queryset = FieldAssessment.objects.all()

    @transaction.atomic
    def patch(self, request, *args, **kwargs):
        assessment = self.queryset.get(id=kwargs["pk"])
        project = assessment.field_assessments.first()

        comment = request.data.get("comment", None)
        unhcr_focal_point = request.data.pop("unhcr_focal_point", None)
        unhcr_alt_focal_point = request.data.pop("unhcr_alt_focal_point", None)
        partner_focal_point = request.data.pop("partner_focal_point", None)
        partner_alt_focal_point = request.data.pop("partner_alt_focal_point", None)

        fa_qs = FieldAssessment.objects.filter(id=kwargs["pk"])
        if fa_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        if not all([unhcr_focal_point, partner_focal_point]):
            error = "Please provide valid focal and alternate focal points"
            return Response({"messages": error}, status=status.HTTP_400_BAD_REQUEST)

        if comment is not None:
            comment = Comment.objects.create(message=comment, user=request.user)
            request.data.update({"comment": comment.id})
            fa_obj = fa_qs.first()
            fa_obj.comment = comment
            fa_obj.save()

        serializer = self.serializer_class(fa_qs.first(), request.data)
        if serializer.is_valid():
            serializer.save()

            # Add project agreement members
            agreement_qs = Agreement.objects.filter(field_assessment=kwargs["pk"])
            agreement_obj = agreement_qs.first()

            if unhcr_focal_point is not None:
                is_field_member = FieldMember.objects.filter(
                    field_office=project.business_unit.field_office,
                    user_id=unhcr_focal_point,
                ).exists()
                if is_field_member:
                    existing_focal_points = agreement_obj.focal_points.filter(
                        user_type="unhcr", focal=True
                    )
                    agreement_obj.focal_points.remove(*existing_focal_points)
                    unhcr_fp, _ = AgreementMember.objects.get_or_create(
                        user_type="unhcr", focal=True, user_id=unhcr_focal_point
                    )
                    agreement_obj.focal_points.add(unhcr_fp)
                else:
                    msg = "Invalid unhcr auditor focal point for the field office - {}".format(
                        project.business_unit.field_office.name
                    )
                    return Response({"messages": msg}, status=status.HTTP_400_BAD_REQUEST)
            if unhcr_alt_focal_point is not None:
                is_field_member = FieldMember.objects.filter(
                    field_office=project.business_unit.field_office,
                    user_id=unhcr_alt_focal_point,
                ).exists()
                if is_field_member:
                    existing_focal_points = agreement_obj.focal_points.filter(
                        user_type="unhcr", alternate_focal=True
                    )
                    agreement_obj.focal_points.remove(*existing_focal_points)
                    unhcr_alt_fp, _ = AgreementMember.objects.get_or_create(
                        user_type="unhcr",
                        alternate_focal=True,
                        user_id=unhcr_alt_focal_point,
                    )
                    agreement_obj.focal_points.add(unhcr_alt_fp)
                else:
                    msg = "Invalid alt unhcr auditor focal point for the field office - {}".format(
                        project.business_unit.field_office.name
                    )
                    return Response({"messages": msg}, status=status.HTTP_400_BAD_REQUEST)
            if partner_focal_point is not None:
                is_partner_member = PartnerMember.objects.filter(
                    partner=project.partner,
                    user_id=partner_focal_point,
                ).exists()
                if is_partner_member:
                    existing_focal_points = agreement_obj.focal_points.filter(
                        user_type="partner", focal=True
                    )
                    agreement_obj.focal_points.remove(*existing_focal_points)

                    partner_fp, _ = AgreementMember.objects.get_or_create(
                        user_type="partner", focal=True, user_id=partner_focal_point
                    )
                    agreement_obj.focal_points.add(partner_fp)
                else:
                    msg = "Invalid partner focal point for the selected partner - {}".format(
                        project.partner.legal_name
                    )
                    return Response({"messages": msg}, status=status.HTTP_400_BAD_REQUEST)
            if partner_alt_focal_point is not None:
                is_partner_member = PartnerMember.objects.filter(
                    partner=project.partner,
                    user_id=partner_alt_focal_point,
                ).exists()
                if is_partner_member:
                    existing_focal_points = agreement_obj.focal_points.filter(
                        user_type="partner", alternate_focal=True
                    )
                    agreement_obj.focal_points.remove(*existing_focal_points)

                    partner_alt_fp, _ = AgreementMember.objects.get_or_create(
                        user_type="partner",
                        alternate_focal=True,
                        user_id=partner_alt_focal_point,
                    )
                    agreement_obj.focal_points.add(partner_alt_fp)
                else:
                    msg = "Invalid partner alt focal point for the selected partner - {}".format(
                        project.partner.legal_name
                    )
                    return Response({"messages": msg}, status=status.HTTP_400_BAD_REQUEST)
            agreement_obj.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        raise exceptions.APIException(str(serializer.errors))


class FieldAssessmentDeadlineList(generics.ListCreateAPIView):
    queryset = FieldAssessment.objects.all()
    serializer_class = FieldAssessmentUpdateSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)

    def get(self, request, *args, **kwargs):
        budget_ref = request.query_params.get("budget_ref", None)
        if budget_ref is not None and str(budget_ref).isdigit():
            assessments = Agreement.objects.filter(
                cycle__year=budget_ref
            ).values_list("field_assessment_id", flat=True)
            field_assessment_qs = FieldAssessment.objects.filter(
                id__in=assessments
            )
            deadline_date = None
            if field_assessment_qs.first() is not None:
                deadline_date = field_assessment_qs.first().deadline_date
            data = {
                "deadline": all(
                    field_assessment_qs.values_list("deadline_date", flat=True)
                ),
                "deadline_date": deadline_date,
            }
            return Response({"results": data})
        return Response(
            {"message": "budget_ref is a required field"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    def post(self, request, *args, **kwargs):
        deadline_date = request.data.get("deadline", None)
        budget_ref = request.data.get("budget_ref", None)
        subject = "Remind Field Office"
        notification_type = "remind_field_office"

        if deadline_date is None or budget_ref is None:
            raise exceptions.APIException(
                "Please provide the budget_ref and deadline_date"
            )

        fa_ids = Agreement.objects.filter(cycle__budget_ref=budget_ref).values_list(
            "field_assessment_id", flat=True
        )
        fa_qs = self.queryset.filter(id__in=fa_ids)
        if fa_qs.exists() is False:
            raise exceptions.APIException(
                "Deadline already set for all the projects for {0}".format(budget_ref)
            )

        fa_qs.update(deadline_date=parse(deadline_date))

        # sending reminder to field offices
        filtered_field_offices = Agreement.objects.filter(
            cycle__budget_ref=budget_ref, field_assessment__score_id=NO_FIELD_RESPONSE
        )
        to_emails = (
            FieldMember.objects.filter(
                field_office_id__in=filtered_field_offices.values_list(
                    "business_unit__field_office_id", flat=True
                ).distinct()
            )
            .distinct()
            .values_list("user__email", flat=True)
        )

        users = User.objects.filter(email__in=to_emails)
        fo = FieldOffice.objects.filter(
            id__in=filtered_field_offices.values_list(
                "business_unit__field_office_id", flat=True
            ).distinct()
        )
        try:
            context = {
                "subject": subject,
                "field_fieldassessment__deadline_date": filtered_field_offices.first().field_assessment.deadline_date,
                "remind_or_inform": "inform",
            }
        except Exception as e:
            context = {
                "subject": subject,
            }
        if fo.exists():
            send_notification(
                notification_type=notification_type,
                obj=fo.first(),
                users=users,
                context=context,
            )
            send_reminder()

            fa_qs = FieldAssessment.objects.filter(
                id__in=filtered_field_offices.values("field_assessment_id")
            )
            fa_qs.update(remind_date=datetime.datetime.now(), is_remind=True)

        return Response(
            {
                "code": "OK_UPDATE",
                "message": code["OK_UPDATE"],
                "status": status.HTTP_200_OK,
            },
            status=status.HTTP_200_OK,
        )

class SetDeltaDeadline(generics.UpdateAPIView):
    queryset = Cycle.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR)
    serializer_class = SetDeltaDeadlineSerializer



class FieldMemberOfficeView(generics.ListCreateAPIView):
    queryset = FieldOffice.objects.all()
    serializer_class = FieldMemberAssignmentSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    resource_model = FieldOffice
    field_member_model = FieldMember
    field_member_assignment = FieldMemberAssignment

    def post(self, request, *args, **kwargs):
        office_name = request.data.get("office")
        field_member_id = request.data.get("field_member_id")
        focal_point_type = request.data.get("focal_point_type")
        field_office_qs = self.resource_model.objects.filter(name=office_name)
        if field_office_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        field_member_qs = self.field_member_model.objects.filter(pk=field_member_id)
        if field_member_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        fms_qs = self.field_member_assignment.objects.filter(
            field_office=field_office_qs.last(),
            field_member=field_member_qs.last(),
            focal_point_type=focal_point_type,
        )
        if fms_qs.exists() is True:
            return Response(
                {"code": "E_DUPLICATE_REQUEST", "message": code["E_DUPLICATE_REQUEST"]},
                status=status.HTTP_400_BAD_REQUEST,
            )

        fms = FieldMemberAssignment(
            field_office=field_office_qs.last(),
            field_member=field_member_qs.last(),
            focal_point_type=focal_point_type,
        )
        fms.save()
        res = self.serializer_class(field_office_qs.last(), many=False)
        return Response(res.data, status=status.HTTP_201_CREATED)

    def get_queryset(self, *args, **kwargs):
        return self.queryset


class FieldMemberOfficeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FieldMember.objects.all()
    serializer_class = FieldMemberAssignmentSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsFO)
    resource_model = FieldMember
    field_office_model = FieldOffice
    field_member_assignment = FieldMemberAssignment

    def delete(self, request, pk=None, *args, **kwargs):
        office_name = request.data.get("office")
        field_member_qs = self.queryset.filter(pk=pk)
        if field_member_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )

        field_office_qs = self.field_office_model.objects.filter(name=office_name)
        if field_office_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        fms_qs = self.field_member_assignment.objects.filter(
            field_office=field_office_qs.last(), field_member=field_member_qs.last()
        )
        if fms_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        fms_qs.delete()
        return Response(
            {"code": "OK_DELETE", "message": code["OK_DELETE"]},
            status=status.HTTP_200_OK,
        )

    def get(self, request, pk=None, *args, **kwargs):
        field_member_qs = self.queryset.filter(pk=pk)
        if field_member_qs.exists() is False:
            return Response(
                {"code": "E_NOT_FOUND", "message": code["E_NOT_FOUND"]},
                status=status.HTTP_404_NOT_FOUND,
            )
        res = self.serializer_class(field_member_qs.last(), many=False)
        return Response(res.data, status=status.HTTP_200_OK)


class FieldAssessmentProgressList(generics.ListCreateAPIView):

    permission_classes = (IsAuthenticated,)
    queryset = FieldAssessmentProgress.objects.all()
    serializer_class = FieldAssessmentProgressListSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = FieldAssessmentProgressFilter


class AssessmentProgressHQ(generics.CreateAPIView):

    queryset = FieldAssessmentProgress.objects.all()
    serializer_class = FieldAssessmentProgressListSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def post(self, request, *args, **kwargs):
        budget_ref = request.data.get("budget_ref", None)
        comment_data = request.data.pop("comments", None)
        if (
            budget_ref is not None
            and comment_data is not None
            and len(comment_data) > 0
        ):
            selection_settings = SelectionSettings.objects.filter(
                cycle__year=budget_ref
            )
            if selection_settings.exists():
                comment_data = comment_data[0]
                # Assign focal point to each project assign default score and notify end of the field assessment
                field_assessment_hq(budget_ref, comment_data, request.user.id)
                msg = "Project selection will start soon"
                return Response({"message": msg}, status=status.HTTP_200_OK)
            else:
                msg = "Project Selection Settings are not available for this Budget Reference"
                return Response({"message": msg}, status=status.HTTP_400_BAD_REQUEST)
        else:
            msg = "Please provide budget_ref and comments"
            return Response({"message": msg}, status=status.HTTP_400_BAD_REQUEST)


class FieldAssessmentStats(generics.ListCreateAPIView):
    queryset = FieldOffice.objects.all()
    serializer_class = FieldOfficeStatsSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):
        partial = request.query_params.get("partial", None)
        budget_ref = request.query_params.get("budget_ref", None)
        field_office = request.query_params.get("field_office", None)

        if budget_ref == None:
            raise exceptions.APIException("Please provide the budget_ref")

        if field_office is not None:
            self.queryset = FieldOffice.objects.filter(id=field_office)
        else:
            field_offices = (
                Agreement.objects.filter(cycle__year=budget_ref)
                .distinct()
                .values_list("business_unit__field_office_id", flat=True)
            )
            self.queryset = FieldOffice.objects.filter(id__in=field_offices)

        field_assessments = Agreement.objects.filter(
            cycle__budget_ref=budget_ref
        ).values_list("field_assessment_id", flat=True)

        overall_stats = FieldAssessment.objects.filter(
            id__in=field_assessments
        ).aggregate(
            total=Count("id"), completed=Count("score_id", filter=~Q(score_id=4))
        )
        if field_assessments.exists() == False:
            self.queryset = []
        overall_stats.update({"budget_ref": int(budget_ref)})
        if partial == "true":
            return Response(overall_stats, status=status.HTTP_200_OK)

        data = self.list(request, *args, **kwargs)
        data.data.update(**overall_stats)
        return data


class FieldAssessmentCanProceed(generics.ListAPIView):
    queryset = FieldAssessmentProgress.objects.all()
    serializer_class = FieldAssessmentCanProceedSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        budget_ref = request.query_params.get("budget_ref", None)
        field_office_id = request.query_params.get("field_office", None)

        if request.user.membership["type"] == "HQ":
            total_fa = (
                Agreement.objects.filter(cycle__budget_ref=budget_ref)
                .distinct()
                .values("business_unit__field_office_id")
            )
            field_assessment_progress = self.queryset.filter(budget_ref=budget_ref)
            data = field_assessment_progress.values("pending_with").annotate(
                Count("pending_with")
            )
            response = {"total_assessments": total_fa.count(), "response": data}
            return Response(response, status=status.HTTP_200_OK)

        canproceed = False
        if budget_ref is not None:
            field_office = FieldOffice.objects.filter(id=field_office_id)
            if field_office.exists() == False:
                raise exceptions.APIException("Field office does not exists")

            canproceed = field_office.first().canproceed(budget_ref=budget_ref)
            progress = self.queryset.filter(
                budget_ref=budget_ref, field_office_id=field_office_id
            )
            response = progress.values()
            if len(response) == 0:
                return Response(
                    {
                        "field_office_id": field_office_id,
                        "budget_ref": budget_ref,
                        "pending_with": "REVIEWER",
                        "canproceed": canproceed,
                    }
                )
            response[0].update({"canproceed": canproceed})
            return Response(response[0], status=status.HTTP_200_OK)
        raise exceptions.APIException("Internal server error")


class FieldAssessmentRemind(generics.ListCreateAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def post(self, request, *args, **kwargs):
        dt_date = datetime.datetime.today().date()
        to_emails = request.data.get("to_emails", None)
        field_office_id = request.data.get("field_office_id", None)
        budget_ref = request.data.get("budget_ref", None)
        content = request.data.get("content", None)
        subject = "Remind Field Office"
        notification_type = "remind_field_office"

        ## send reminder to all field office members if only budger_ref is provided
        if budget_ref is not None and all([to_emails, field_office_id]) == False:
            filtered_field_offices = Agreement.objects.filter(
                cycle__budget_ref=budget_ref,
                field_assessment__score_id=NO_FIELD_RESPONSE,
            )
            to_emails = (
                FieldMember.objects.filter(
                    field_office_id__in=filtered_field_offices.values_list(
                        "business_unit__field_office_id", flat=True
                    ).distinct()
                )
                .distinct()
                .values_list("user__email", flat=True)
            )
            if len(to_emails) == 0 or filtered_field_offices.exists() == False:
                error = "No field office to notify based on the request"
                raise exceptions.APIException(error)
        else:
            notification_type = "custom_msg"
            if all([to_emails, field_office_id]) == False:
                error = "field_office_id and to_emails are required fields"
                raise exceptions.APIException(error)

            filtered_field_offices = Agreement.objects.filter(
                business_unit__field_office_id=field_office_id,
                field_assessment__score_id=NO_FIELD_RESPONSE,
            )
        if filtered_field_offices.exists() == False:
            error = "Field assessment not pending"
            raise exceptions.APIException(error)

        users = User.objects.filter(email__in=to_emails)
        fo = FieldOffice.objects.filter(
            id__in=filtered_field_offices.values_list(
                "business_unit__field_office_id", flat=True
            ).distinct()
        )
        try:
            context = {
                "custom_msg": "{0}".format(content),
                "subject": subject,
                "field_fieldassessment__deadline_date": filtered_field_offices.first().field_assessment.deadline_date,
                "remind_or_inform": "remind",
            }
        except Exception as e:
            context = {
                "custom_msg": "{0}".format(content),
                "subject": subject,
                "remind_or_inform": "remind",
            }

        if fo.exists():
            send_notification(
                notification_type=notification_type,
                obj=fo.first(),
                users=users,
                context=context,
            )
            send_reminder()

            fa_qs = FieldAssessment.objects.filter(
                id__in=filtered_field_offices.values("field_assessment_id")
            )
            fa_qs.update(remind_date=datetime.datetime.now(), is_remind=True)
        return Response({"result": "success"}, status=status.HTTP_200_OK)


class InterimRemindAPIView(generics.CreateAPIView):

    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def post(self, request, *args, **kwargs):
        year = request.data.get("year")
        field_office = request.data.get("field_office")
        if year is None or field_office is None:
            error = "year and field_office are required fields"
            raise exceptions.APIException(error)
        remind_interim_list(year, field_office)
        return Response({"result": "success"}, status=status.HTTP_200_OK)


class FieldAssessmentExport(generics.ListAPIView):

    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):
        year = kwargs.get("year", None)

        if year is None:
            error = "Year is a required field"
            return Response({"errors": error}, status=status.HTTP_400_BAD_REQUEST)
        else:
            export_field_assessment(year, request.user.id)
            return Response({"message": "An email will be sent to you"})


class DeleteAgreementAndFieldAssessment(generics.DestroyAPIView):

    queryset = FieldAssessment.objects.all()
    serializers_class = FieldAssessmentSerializer
    permission_classes = (
        IsAuthenticated,
        IsUNHCR,
    )

    def delete(self, request, *args, **kwargs):
        try:
            field_assessment = self.queryset.filter(id=kwargs["pk"])
            agreement = Agreement.objects.filter(
                field_assessment=field_assessment.first()
            )
            agreement.delete()
            field_assessment.delete()
            return Response({}, status=status.HTTP_200_OK)
        except Exception as e:
            error = "Error while deleting the agreement, please contact admin".format(
                str(e)
            )
            return Response({"message": error})
