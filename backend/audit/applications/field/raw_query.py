raw_sql_find_email_address = """
    select
	u.id,
	u.email as email
from
	field_fieldassessment ffa
left join common_businessunit businessunit on
	ffa.business_unit_id = businessunit.id
left join field_fieldoffice ff on
	businessunit.field_office_id = ff.id
left join field_fieldmemberassignment ffmas on
	ff.id = ffmas.field_office_id
inner join field_fieldmember ffm on
	ff.id = ffm.field_office_id
left join account_user au on
	ffm.user_id = au.id
left join account_user u on
	ffm.user_id = u.id
where
	ffa.score_id = 4 {}
group by
	u.email,
	u.id
"""
