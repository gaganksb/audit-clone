from django.apps import apps
from django.db import models
from django.conf import settings

from project.models import Cycle
from . import FOCAL_POINT_TYPE
from common.models import (
    Permission,
)
from notify.models import Notification
from audit.core.abstract_model import AbstractModel

# see fixtures
SCORE_CHOICES = ((0, 0), (1, 1), (3, 3), (5, 5))
NO_FIELD_RESPONSE = 4
STATUS_CHOICES = (
    ("REVIEWER", "Reviewer"),
    ("APPROVER", "Approver"),
    ("HQ", "HQ"),
    ("COMPLETED", "Completed"),
)


class FieldOffice(AbstractModel):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    def canproceed(self, **kwargs):
        budget_ref = kwargs["budget_ref"]
        cycle = Cycle.objects.filter(year=budget_ref).first()
        from project.models import Agreement

        agreements = Agreement.objects.filter(
            business_unit__field_office_id=self.id, cycle=cycle
        )
        if cycle.delta_enabled:
            agreements = agreements.exclude(fin_sense_check=True)

        if agreements.exists() == False:
            return False
        else:
            agreements = agreements.filter(field_assessment__score_id=4)
            return len(agreements) == 0

    def __str__(self):
        return self.name


class Role(AbstractModel):
    role = models.CharField(max_length=255, unique=True, null=False)
    permissions = models.ManyToManyField(Permission, related_name="field_permission")

    def __str__(self):
        return self.role


class FieldMember(AbstractModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="field_members"
    )
    field_office = models.ForeignKey(FieldOffice, on_delete=models.CASCADE, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, null=True)
    temporary_password = models.CharField(max_length=255, null=True, default=None)
    date_send_invitation = models.DateField(null=True, default=None)
    is_password_change = models.BooleanField(null=False, default=False)

    class Meta:
        ordering = ["id"]
        unique_together = ("user", "field_office", "role")

    def __init__(self, *args, **kwargs):
        self.user_key_mapping = {
            "name": "fullname",
            "email": "email",
        }
        self.user_type_role_model = apps.get_model("account.UserTypeRole")
        super().__init__(*args, **kwargs)

    def update_user_info(self, key, value):
        user = self.user
        setattr(user, self.user_key_mapping.get(key), value)
        user.save()

    def save(self, *args, **kwargs):
        super().save(self, *args, **kwargs)
        self.trigger_create()

    def trigger_create(self):
        utr = self.user_type_role_model(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
            user_id=self.user_id,
        )
        utr.save()

    def trigger_delete(self):
        utr_qs = self.user_type_role_model.objects.filter(
            resource_app_label=getattr(self, "_meta").app_label,
            resource_model=getattr(self, "_meta").model_name,
            resource_id=self.id,
        )
        if utr_qs.exists() is True:
            utr_qs.delete()
            self.delete()

    def __str__(self):
        return self.user.fullname


class FieldMemberAssignment(AbstractModel):
    field_office = models.ForeignKey(
        FieldOffice, null=True, default=None, on_delete=models.CASCADE
    )
    field_member = models.ForeignKey(
        FieldMember, null=True, default=None, on_delete=models.CASCADE
    )
    focal_point_type = models.CharField(
        max_length=32,
        null=True,
        choices=FOCAL_POINT_TYPE,
        default=FOCAL_POINT_TYPE.ORIGINAL,
        blank=True,
    )

    def __str__(self):
        return ["{}, {}"].format(
            self.field_office.name, self.field_member.user.fullname
        )


class FieldOfficeRoleNotification(AbstractModel):
    field_office_role = models.ForeignKey(
        Role, null=True, default=None, on_delete=models.CASCADE
    )
    notification = models.ForeignKey(
        Notification, null=True, default=None, on_delete=models.CASCADE
    )

    def __str__(self):
        return self.field_office_role.role


class Score(models.Model):
    description = models.CharField(max_length=255, unique=True)
    value = models.IntegerField(choices=SCORE_CHOICES)

    def __str__(self):
        return self.description


class FieldAssessment(AbstractModel):
    score = models.ForeignKey(
        Score, on_delete=models.CASCADE, default=NO_FIELD_RESPONSE
    )
    comment = models.ForeignKey(
        "common.Comment", on_delete=models.CASCADE, null=True, default=""
    )
    deadline_date = models.DateField(null=True, default=None)
    is_remind = models.BooleanField(null=False, default=False)
    remind_date = models.DateField(null=True, default=None)

    def __str__(self):
        return self.score.description


class FieldAssessmentProgress(models.Model):
    field_office = models.ForeignKey(FieldOffice, on_delete=models.CASCADE)
    budget_ref = models.IntegerField(null=True, default=None)
    comments = models.ManyToManyField("common.Comment")
    created_date = models.DateTimeField(auto_now=True)
    update_date = models.DateTimeField(auto_now=True)
    pending_with = models.CharField(
        max_length=255, choices=STATUS_CHOICES, default="REVIEWER"
    )

    def __str__(self):
        return self.field_office.name
