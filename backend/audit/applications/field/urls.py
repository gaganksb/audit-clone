from django.urls import path
from field import views

urlpatterns = [
    path("", views.FieldOfficeList.as_view(), name="fieldoffice-list"),
    path("<int:pk>/", views.FieldOfficeDetail.as_view(), name="fieldoffice-detail"),
    path("<int:pk>/members/", views.FieldMemberList.as_view(), name="fieldmember-list"),
    path(
        "member/<int:pk>/",
        views.FieldMemberDetail.as_view(),
        name="field-member-detail",
    ),
    path(
        "member/assignment/",
        views.FieldMemberOfficeView.as_view(),
        name="field-member-assignment",
    ),
    path(
        "member/assignment/<int:pk>/",
        views.FieldMemberOfficeDetail.as_view(),
        name="field-member-assignment-detail",
    ),
    path("roles/", views.FieldRoleList.as_view(), name="role-list"),
    path("roles/<int:pk>/", views.FieldRoleDetail.as_view(), name="role-detail"),
    path(
        "assessments/",
        views.FieldAssessmentsAPIView.as_view(),
        name="fieldassessment-list",
    ),
    path(
        "assessments/<int:pk>/",
        views.FieldAssessmentAPIView.as_view(),
        name="fieldassessment-detail",
    ),
    path(
        "assessments/stats/",
        views.FieldAssessmentStats.as_view(),
        name="fieldassessment-stats",
    ),
    path(
        "assessments/deadline/",
        views.FieldAssessmentDeadlineList.as_view(),
        name="fieldassessment-deadline",
    ),
    path(
        "assessments/delta-deadline/<int:pk>/",
        views.SetDeltaDeadline.as_view(),
        name="fieldassessment-delta-deadline",
    ),
    path(
        "assessments/remind/",
        views.FieldAssessmentRemind.as_view(),
        name="fieldassessment-remind",
    ),
    path(
        "assessments/progress/",
        views.FieldAssessmentProgressList.as_view(),
        name="fieldassessment-progress",
    ),
    path(
        "assessments/progress-hq/",
        views.AssessmentProgressHQ.as_view(),
        name="fieldassessment-progress-hq",
    ),
    path(
        "assessments/canproceed/",
        views.FieldAssessmentCanProceed.as_view(),
        name="fieldassessment-canproceed",
    ),
    path(
        "remind-interim/", views.InterimRemindAPIView.as_view(), name="remind-interim"
    ),
    path(
        "export-fieldassessment/<int:year>/",
        views.FieldAssessmentExport.as_view(),
        name="csv-export",
    ),
    path(
        "delete-project/<int:pk>/",
        views.DeleteAgreementAndFieldAssessment.as_view(),
        name="delete-project",
    ),
]
