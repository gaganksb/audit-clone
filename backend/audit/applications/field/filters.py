import django_filters
from django_filters.filters import (
    CharFilter,
    NumberFilter,
    DateFilter,
    DateRangeFilter,
    BooleanFilter,
)
from django_filters import FilterSet
from project.models import Agreement
from .models import FieldAssessment


class CommonFilter(FilterSet):
    start = CharFilter(method="filter_start")
    end = CharFilter(method="filter_end")
    year = NumberFilter(field_name="created_date", lookup_expr="year")
    month = NumberFilter(field_name="created_date", lookup_expr="month")

    def filter_start(self, queryset, name, value):
        return queryset.filter(created_date__gte=value)

    def filter_end(self, queryset, name, value):
        return queryset.filter(created_date__lte=value)

    class Meta:
        fields = [
            "start",
            "end",
            "year",
            "month",
        ]


class FieldAssessmentListFilter(FilterSet):

    budget_ref = CharFilter(method="filter_year")
    partner = CharFilter(method="filter_partner")
    business_unit = CharFilter(method="filter_bu")
    id = NumberFilter(method="filter_id")
    score__id = NumberFilter(method="filter_score")
    delta = BooleanFilter(field_name="field_assessments__delta", lookup_expr="exact")

    def filter_year(self, queryset, name, value):
        field_assessment_ids = Agreement.objects.filter(cycle__budget_ref=value).values(
            "field_assessment_id"
        )
        return queryset.filter(id__in=field_assessment_ids)

    def filter_partner(self, queryset, name, value):
        field_assessment_ids = Agreement.objects.filter(
            partner__legal_name__icontains=value
        ).values("field_assessment_id")
        return queryset.filter(id__in=field_assessment_ids)

    def filter_bu(self, queryset, name, value):
        field_assessment_ids = Agreement.objects.filter(
            business_unit__code=value
        ).values("field_assessment_id")
        return queryset.filter(id__in=field_assessment_ids)

    def filter_score(self, queryset, name, value):
        return queryset.filter(score__id=value)

    def filter_id(self, queryset, name, value):
        return queryset.filter(id=value)

    class Meta:
        model = FieldAssessment
        fields = ["budget_ref", "partner", "score__id", "id", "delta"]


class FieldAssessmentProgressFilter(FilterSet):
    field_office = NumberFilter(field_name="field_office_id", lookup_expr="exact")
    budget_ref = NumberFilter(field_name="budget_ref", lookup_expr="exact")

    class Meta:
        fields = ["field_office", "budget_ref"]


class FieldAssessmentStatsFilter(FilterSet):

    budget_ref = CharFilter(method="filter_year")
    field_office = CharFilter(method="filter_field_office")

    def filter_year(self, queryset, name, value):
        field_assessment_ids = Agreement.objects.filter(budget_ref=value).values(
            "field_assessment_id"
        )
        return queryset.filter(id__in=field_assessment_ids)

    def filter_field_office(self, queryset, name, value):
        return queryset.filter(business_unit__field_office_id=value)

    class Meta:
        fields = [
            "budget_ref",
            "field_office",
        ]


class FieldMemberFilter(FilterSet):

    name = CharFilter(method="filter_name")
    field_office = NumberFilter(field_name="field_office_id", lookup_expr="exact")

    def filter_name(self, queryset, name, value):
        return queryset.filter(user__fullname__icontains=value)
