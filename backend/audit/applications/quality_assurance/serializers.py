from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from common.models import Comment, CommonFile
from common.serializers import (
    CommonUserSerializer,
    CountrySerializer,
    CommonCommentSerializer,
    BusinessUnitSerializer,
)
from project.serializers import (
    CycleSerializer,
    CommonUserSerializer,
    AgreementMemberListSerializer,
)
from auditing.serializers import AuditAgencyNameSerializer
from quality_assurance.models import (
    ImprovementArea,
    TeamComposition,
    TeamCompositionUser,
    TeamCompositionCountry,
    TeamCompositionCriteria,
    TeamCompositionCountryCriteria,
    AgreementSurvey,
    Response,
    Message,
    Survey,
    Question,
    KPI3SurveyReport,
    AuditSummary,
    AuditSummaryReport,
    AuditQuality,
    FinalReport,
    SummaryReport,
    WorkingPaper,
    QuestionCategory,
    Agreement,
)
from datetime import date
from common.enums import UserTypeEnum
from quality_assurance.enums import SurveyTypeEnum
from quality_assurance.tasks import import_agreement_survey
from quality_assurance.models import Cycle
from quality_assurance.helpers import (
    check_audit_assignment_status,
    check_audit_work,
    check_survey_uploaded,
)
from django.db.models.functions import ExtractWeek
from django.db.models import Count
from partner.serializers import PartnerSerializer

class ImprovementAreaSerializer(serializers.ModelSerializer):
    auditor_comment = CommonCommentSerializer(read_only=True)

    class Meta:
        model = ImprovementArea
        fields = "__all__"

    def update(self, instance, validated_data):
        request = self.context["request"]
        if "auditor_comment" in request.data:
            comment = request.data.pop("auditor_comment", None)
            if comment is not None:
                comment = Comment.objects.create(message=comment)
                instance.auditor_comment = comment
                instance.save()
        return super().update(instance, validated_data)


class TeamCompositionUserSerializer(serializers.ModelSerializer):
    agreement_member = AgreementMemberListSerializer()
    status = serializers.SerializerMethodField()

    class Meta:
        model = TeamCompositionUser
        exclude = ("team_composition", "created_date", "last_modified_date", "user")

    def get_status(self, instance):
        status = all(
            [
                instance.agreement_member.team_type,
                instance.agreement_member.role,
                instance.agreement_member.duration_spent_in_days,
                instance.agreement_member.tasks_completed,
                instance.agreement_member.profile_summary,
                instance.agreement_member.qualifications,
                instance.agreement_member.years_of_experience,
                instance.agreement_member.language,
                instance.agreement_member.clients_audited_example,
                instance.agreement_member.ISA_experience,
                instance.agreement_member.comments,
            ]
        )
        return "done" if status == True else "todo"


class TeamCompositionCountrySerializer(serializers.ModelSerializer):

    country = CountrySerializer()
    team_composition_country_criterias = serializers.SerializerMethodField()

    class Meta:
        model = TeamCompositionCountry
        fields = "__all__"

    def get_team_composition_country_criterias(self, instance):
        qs = instance.team_composition_country_criterias.all()
        serializer = TeamCompositionCountryCriteriaListSerializer(qs, many=True)
        return serializer.data


class TeamCompositionCriteriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamCompositionCriteria
        fields = (
            "id",
            "name",
            "description",
        )


class TeamCompositionCountryCriteriaListSerializer(serializers.ModelSerializer):
    remarks = CommonCommentSerializer()
    auditor_comment = CommonCommentSerializer()
    criteria = TeamCompositionCriteriaSerializer()

    class Meta:
        model = TeamCompositionCountryCriteria
        exclude = ("team_composition_country",)


class TeamCompositionCountryCriteriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamCompositionCountryCriteria
        fields = "__all__"


class TeamCompositionSerializer(serializers.ModelSerializer):

    cycle = CycleSerializer(read_only=True)
    audit_agency = AuditAgencyNameSerializer(read_only=True)
    team_composition_users = TeamCompositionUserSerializer(many=True, read_only=True)
    team_composition_countries = TeamCompositionCountrySerializer(
        many=True, read_only=True
    )
    user = CommonUserSerializer(read_only=True)
    team_composition_country_criterias = TeamCompositionCountryCriteriaSerializer(
        many=True, read_only=True
    )
    comments = CommonCommentSerializer(read_only=True, many=True)
    status = serializers.SerializerMethodField()
    team_composition_improvement_areas = ImprovementAreaSerializer(
        many=True, read_only=True
    )

    class Meta:
        model = TeamComposition
        fields = "__all__"

    def validate(self, data):
        """
        Check if the Team composition is ready to be finalized
        """
        request = self.context["request"]
        if "finalized" in request.data:
            pk = self.context.get("view").kwargs.get("pk")
            team_composition = TeamComposition.objects.get(pk=pk)
            if all(
                [
                    team_composition.introduction,
                    team_composition.scope_and_objective,
                    team_composition.overall_conclusion,
                    team_composition.methodology,
                ]
            ):
                return data
            else:
                raise serializers.ValidationError(
                    "Team Composition cant be finalized yet"
                )
        return data

    def get_status(self, instance):
        return "done" if instance.team_composition_countries.all().exists() else "todo"


class TeamCompositionProfileSerializer(serializers.ModelSerializer):

    cycle = CycleSerializer(read_only=True)
    audit_agency = AuditAgencyNameSerializer(read_only=True)
    team_composition_users = TeamCompositionUserSerializer(many=True, read_only=True)
    team_composition_countries = TeamCompositionCountrySerializer(
        many=True, read_only=True
    )
    user = CommonUserSerializer(read_only=True)
    team_composition_country_criterias = TeamCompositionCountryCriteriaSerializer(
        many=True, read_only=True
    )
    comments = CommonCommentSerializer(read_only=True, many=True)
    team_composition_improvement_areas = ImprovementAreaSerializer(
        many=True, read_only=True
    )

    class Meta:
        model = TeamComposition
        fields = "__all__"


class DynamicLangModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `lang` argument that
    controls which languanges should be displayed.
    """

    def __init__(self, *args, **kwargs):
        super(DynamicLangModelSerializer, self).__init__(*args, **kwargs)


class MessageSerializer(DynamicLangModelSerializer, serializers.ModelSerializer):
    class Meta:
        model = Message
        exclude = ("id", "created_date", "last_modified_date", "user")


class QuestionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionCategory
        exclude = (
            "created_date",
            "last_modified_date",
            "user",
        )


class QuestionSerializer(serializers.ModelSerializer):
    title = MessageSerializer(read_only=True)
    survey_response = serializers.SerializerMethodField()
    comment_request_msg = MessageSerializer()
    category = QuestionCategorySerializer()

    class Meta:
        model = Question
        exclude = (
            "survey",
            "created_date",
            "last_modified_date",
            "user",
        )

    def get_survey_response(self, instance):
        request = self.context["request"]
        kwargs = request.parser_context["kwargs"]
        agreement_survey_id = kwargs.get("pk", None)
        response = Response.objects.filter(
            question=instance, agreement_survey_id=agreement_survey_id
        )
        return response.values()


class SurveySerializer(serializers.ModelSerializer):
    cycle = CycleSerializer(read_only=True)
    title = MessageSerializer(read_only=True)
    survey_questions = QuestionSerializer(read_only=True, many=True)

    class Meta:
        model = Survey
        fields = "__all__"


class SurveySmallSerializer(serializers.ModelSerializer):
    title = MessageSerializer(read_only=True)

    class Meta:
        model = Survey
        fields = ("id", "title", "type")


class AgreementSurveySerializer(serializers.ModelSerializer):

    user = CommonUserSerializer(read_only=True)
    cycle = CycleSerializer(read_only=True)
    title = MessageSerializer(read_only=True)
    survey = SurveySerializer(read_only=True)

    class Meta:
        model = AgreementSurvey
        fields = "__all__"


class AgreementSurveySmallSerializer(serializers.ModelSerializer):
    survey = SurveySmallSerializer(read_only=True)

    class Meta:
        model = AgreementSurvey
        fields = ("id", "survey", "status")


class ResponseSerializer(serializers.ModelSerializer):

    number = serializers.ReadOnlyField(source="agreement_survey.agreement.number")
    bu_code = serializers.ReadOnlyField(
        source="agreement_survey.agreement.business_unit.code"
    )
    partner_name = serializers.ReadOnlyField(
        source="agreement_survey.agreement.partner.legal_name"
    )
    survey_type = serializers.ReadOnlyField(source="agreement_survey.survey.type")

    class Meta:
        model = Response
        fields = "__all__"


class KPI3SurveyReportSerializer(serializers.ModelSerializer):
    cycle = CycleSerializer(read_only=True)
    comments = CommonCommentSerializer(many=True, read_only=True)

    class Meta:
        model = KPI3SurveyReport
        fields = "__all__"

    def validate(self, data):

        if "finalized" in data and data["finalized"] == True:
            if not all(
                [
                    self.instance.introduction,
                    self.instance.scope_and_objective,
                    self.instance.methodology,
                ]
            ):
                raise serializers.ValidationError(
                    "Please add required field before finalizing"
                )
        return data

    def update(self, instance, validated_data):
        request = self.context["request"]
        comments = request.data.get("comments")
        for comment in comments:
            comment = Comment.objects.create(**comment)
            instance.comments.add(comment)
            instance.save()
        return super().update(instance, validated_data)


class FinalReportSerializer(serializers.ModelSerializer):
    working_paper_progress = serializers.SerializerMethodField()

    class Meta:
        model = FinalReport
        exclude = ("created_date", "last_modified_date", "user", "audit_quality")

    def validate(self, data):

        if "finalized" in data and data["finalized"] == True:
            if not all(
                [
                    self.instance.background,
                    self.instance.scope_and_objective,
                    self.instance.methodology,
                ]
            ):
                raise serializers.ValidationError(
                    "Please add required field before finalizing"
                )
        return data

    def working_paper_stats(self, year, audit_firm_id):
        queryset = WorkingPaper.objects.filter(
            agreement__cycle__year=year, agreement__audit_firm_id=audit_firm_id
        )
        response = []
        for field in ["created_date", "date_received", "date_reviewed"]:
            params = {"{}_week".format(field): ExtractWeek(field)}
            data = queryset.values(**params).annotate(total=Count("id"))
            data = data.filter(**{"{}_week__isnull".format(field): False})
            response.extend(data)
        return response

    def get_working_paper_progress(self, instance):
        year = instance.audit_quality.cycle.year
        audit_agency_id = instance.audit_quality.audit_agency.id
        response = self.working_paper_stats(year, audit_agency_id)
        final_response = []
        weeks = sorted(set([list(i.values())[0] for i in response]))
        for idx, week in enumerate(weeks):
            idx = idx + 1
            week_data = {
                "WK{}".format(idx): {"requested": 0, "received": 0, "reviewed": 0, "total_wp_in_prog": 0, "week": week}
            }
            for item in response:
                if (
                    item.get("created_date_week") is not None
                    and item.get("created_date_week") == week
                ):
                    week_data["WK{}".format(idx)].update({"requested": item["total"]})
                elif (
                    item.get("date_received_week") is not None
                    and item.get("date_received_week") == week
                ):
                    week_data["WK{}".format(idx)].update({"received": item["total"]})
                elif (
                    item.get("date_reviewed_week") is not None
                    and item.get("date_reviewed_week") == week
                ):
                    week_data["WK{}".format(idx)].update({"reviewed": item["total"]})
            for i in week_data.values():
                d = i.get("received", 0) + i.get("reviewed", 0)
            week_data["WK{}".format(idx)].update({"total_wp_in_prog": d})
            final_response.append(week_data)
        return final_response


class SummaryReportSerializer(serializers.ModelSerializer):
    report = serializers.ReadOnlyField()

    class Meta:
        model = SummaryReport
        exclude = ("created_date", "last_modified_date", "user", "audit_quality")

    def validate(self, data):

        if "finalized" in data and data["finalized"] == True:
            if not all(
                [
                    self.instance.introduction,
                    self.instance.purpose,
                    self.instance.overall_summary,
                ]
            ):
                raise serializers.ValidationError(
                    "Please add required field before finalizing"
                )
        return data


class AuditQualitySerializer(serializers.ModelSerializer):
    cycle = CycleSerializer(read_only=True)
    final_reports = SerializerMethodField()
    summary_reports = SerializerMethodField()
    audit_agency = AuditAgencyNameSerializer()

    class Meta:
        model = AuditQuality
        fields = "__all__"

    def get_final_reports(self, instance):
        serializer = FinalReportSerializer(instance.final_reports.first())
        return serializer.data

    def get_summary_reports(self, instance):
        serializer = SummaryReportSerializer(instance.summary_reports.first())
        return serializer.data


class WorkingPaperSerializer(serializers.ModelSerializer):
    from common.serializers import CommonFileSerializer

    audit_quality = AuditQualitySerializer
    status = serializers.SerializerMethodField()
    file = CommonFileSerializer(read_only=True)

    def validate(self, data):

        if "reviewed" in data and data["reviewed"] == True:
            if not all(
                [
                    self.instance.title,
                    self.instance.description,
                    self.instance.date_received,
                    self.instance.file,
                ]
            ):
                raise serializers.ValidationError(
                    "Please add required field before finalizing"
                )

        return data

    def get_status(self, instance):
        return "requested" if instance.file is None else "received"

    def is_agreement_audit_admin(self, instance, membership):
        return all(
            [
                membership["type"] == UserTypeEnum.AUDITOR.name,
                membership["role"] == "ADMIN",
                instance.agreement.audit_firm is not None,
                instance.agreement.audit_firm.id == membership["audit_agency"]["id"],
            ]
        )

    def is_agreement_audit_reviewer(self, instance, membership):
        return all(
            [
                membership["type"] == UserTypeEnum.AUDITOR.name,
                membership["role"] == "REVIEWER",
                instance.agreement.audit_firm is not None,
                instance.agreement.audit_firm.id == membership["audit_agency"]["id"],
            ]
        )

    def update(self, instance, validated_data):
        request = self.context["request"]
        membership = request.user.membership
        file = request.FILES.get("file", None)

        audit_users = instance.agreement.focal_points.filter(
            user_type="auditing"
        ).values_list("user_id", flat=True)
        if membership["type"] == UserTypeEnum.HQ.name and file is None:
            if "reviewed" in validated_data:
                validated_data.update({"date_reviewed": date.today()})
            return super().update(instance, validated_data)

        elif membership["type"] == UserTypeEnum.HQ.name and file is not None:
            msg = "HQ users are not authorized to upload working papers"
            raise serializers.ValidationError(msg)

        elif request.user.id in audit_users and file is not None:
            file = CommonFile.objects.create(file_field=file)
            validated_data.update({"file": file, "date_received": date.today()})
            return super().update(instance, validated_data)

        else:
            msg = "You are not authorized to perform this action"
            raise serializers.ValidationError(msg)

    class Meta:
        model = WorkingPaper
        fields = "__all__"


class AuditSummarySerializer(serializers.ModelSerializer):
    audit_agency = AuditAgencyNameSerializer(read_only=True)
    cycle = CycleSerializer(read_only=True)

    class Meta:
        model = AuditSummary
        fields = "__all__"

    def to_representation(self, instance):
        if type(instance) == dict and "kpi1_created" in instance:
            return {"response": instance["kpi1_created"]}
        else:
            return super().to_representation(instance)

    def validate(self, data):
        year = self.context.get("view").kwargs.get("year")
        cycle = Cycle.objects.filter(year=year).first()

        is_auditor_assigned = check_audit_assignment_status(cycle.year, True)
        is_audit_work_done = check_audit_work(cycle.year, True)["audit_work_completed"]
        is_survey_uploaded = check_survey_uploaded(cycle.year)["all_uploaded"]

        if cycle is None:
            raise serializers.ValidationError("Invalid Cycle")
        if is_auditor_assigned == False:
            msg = "Please completed audit assignment before starting KPI1"
            raise serializers.ValidationError(msg)
        if is_audit_work_done == False:
            msg = "Please completed audit work ie Management letter, ICQ and Audit report, before starting KPI1"
            raise serializers.ValidationError(msg)
        if is_survey_uploaded == False:
            msg = "Please upload survey before starting KPI1"
            raise serializers.ValidationError(msg)
        return data

    def delete_audit_survey(self, cycle):
        audit_summary_qs = AuditSummary.objects.filter(cycle=cycle)
        AuditSummaryReport.objects.filter(
            audit_summary__in=audit_summary_qs.values_list("id", flat=True)
        ).delete()
        audit_summary_qs.delete()

    def create_agreement_survey(self, year, survey_type):
        AgreementSurvey.objects.filter(
            survey__type=SurveyTypeEnum.KPI2.value, survey__cycle__year=year
        ).delete()
        return import_agreement_survey(year, survey_type)

    def create_reports(self, year, selected_agreements):
        """
        Delete Old Audit Survey
        Delete Old Audit Survey Report
        Create New Audit Survey for the requested cycle
        Create Related Reports
        """
        cycle = Cycle.objects.get(year=year)
        self.delete_audit_survey(cycle)

        audit_firms = (
            selected_agreements.filter(audit_firm__isnull=False)
            .distinct("audit_firm")
            .values_list("audit_firm", flat=True)
        )
        audit_summary_per_firm = []
        for firm in audit_firms:
            audit_summary_per_firm.append(
                AuditSummary(audit_agency_id=firm, cycle=cycle, status="todo")
            )
        audit_summary_data = AuditSummary.objects.bulk_create(audit_summary_per_firm)
        for audit_summary in audit_summary_data:
            AuditSummaryReport.objects.get_or_create(audit_summary=audit_summary)
        serializer = AuditSummarySerializer(audit_summary_data, many=True)
        return serializer.data

    def create(self, validated_data):
        request = self.context["request"]
        survey_type = request.data.get("survey_type", "audit_timeline_survey")
        year = self.context.get("view").kwargs.get("year")
        selected_agreements = self.create_agreement_survey(year, survey_type)
        response = self.create_reports(year, selected_agreements)
        validated_data.update({"kpi1_created": response})
        return validated_data


class KPI2KeyFindingsSerializer(serializers.ModelSerializer):

    business_unit = BusinessUnitSerializer()
    audit_firm = AuditAgencyNameSerializer()
    partner = PartnerSerializer()
    key_findings = SerializerMethodField()

    class Meta:
        model = Agreement
        fields = ("number", "business_unit", "audit_firm", "partner", "key_findings")

    def get_key_findings(self, instance):

        audit_quality_survey = instance.agreement_surveys.filter(survey__type="audit_quality_survey")
        responses = Response.objects.filter(agreement_survey__in=audit_quality_survey)
        response = responses.values(
            "question__title__english",
            "question__category__title",
            "answer"
        )
        return response