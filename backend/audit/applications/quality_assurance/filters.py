from django.db.models.fields import CharField
import django_filters
from django_filters.filters import CharFilter, NumberFilter
from django_filters import FilterSet

from quality_assurance.models import TeamComposition, Response, AuditQuality, AuditSummary


class TeamCompositionFilter(FilterSet):

    year = NumberFilter(field_name="cycle__year", lookup_expr="exact")
    audit_agency = NumberFilter(field_name="audit_agency", lookup_expr="exact")
    country = NumberFilter(method="filter_by_country")

    def filter_by_country(self, queryset, name, value):
        return (
            queryset.filter()
            .prefetch_related("team_composition_countries")
            .filter(team_composition_countries=value)
        )

    class Meta:
        model = TeamComposition
        fields = ["year", "audit_agency", "country"]


class TeamCompositionProfileFilter(FilterSet):

    year = NumberFilter(method="filter_by_year")
    name = CharFilter(method="filter_by_name")
    role = CharFilter(method="filter_by_role")

    def filter_by_year(self, queryset, name, value):
        return queryset.filter(cycle__year=value)

    def filter_by_name(self, queryset, name, value):
        return queryset.filter(
            team_composition_users__agreement_member__user__fullname__icontains=value
        )

    def filter_by_role(self, queryset, name, value):
        return queryset.filter(
            team_composition_users__agreement_member__role__icontains=value
        )

    class Meta:
        model = TeamComposition
        fields = ["year", "name", "role"]


class SurveyResponseFilter(FilterSet):

    business_unit = CharFilter(
        field_name="agreement_survey__agreement__business_unit__code",
        lookup_expr="iexact",
    )
    number = CharFilter(
        field_name="agreement_survey__agreement__number", lookup_expr="iexact"
    )
    partner = CharFilter(
        field_name="agreement_survey__agreement__partner__legal_name",
        lookup_expr="icontains",
    )
    answer_option = CharFilter(field_name="answer__answer", lookup_expr="icontains")
    survey_type = CharFilter(
        field_name="agreement_survey__survey__type", lookup_expr="iexact"
    )
    audit_agency = CharFilter(
        field_name="agreement_survey__agreement__audit_firm_id", lookup_expr="exact"
    )

    class Meta:
        model = Response
        fields = ["business_unit", "number", "partner", "answer_option", "audit_agency"]


class AuditQualityFilter(FilterSet):

    audit_agency = NumberFilter(field_name="audit_agency", lookup_expr="exact")
    firm = CharFilter(field_name="audit_agency__legal_name", lookup_expr="icontains")

    class Meta:
        model = AuditQuality
        fields = ["audit_agency", "firm"]

class AuditSummaryFilter(FilterSet):

    audit_agency = NumberFilter(field_name="audit_agency", lookup_expr="exact")
    firm = CharFilter(field_name="audit_agency__legal_name", lookup_expr="icontains")

    class Meta:
        model = AuditSummary
        fields = ["audit_agency", "firm"]