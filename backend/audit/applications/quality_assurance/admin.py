from django.contrib import admin
from quality_assurance.models import (
    TeamComposition,
    ImprovementArea,
    TeamCompositionUser,
    TeamCompositionCountry,
    TeamCompositionCriteria,
    TeamCompositionCountryCriteria,
    Message,
    Survey,
    Question,
    AgreementSurvey,
    Response,
)


admin.site.register(TeamComposition)
admin.site.register(ImprovementArea)
admin.site.register(TeamCompositionUser)
admin.site.register(TeamCompositionCountry)
admin.site.register(TeamCompositionCriteria)
admin.site.register(TeamCompositionCountryCriteria)
admin.site.register(Message)
admin.site.register(Survey)
admin.site.register(Question)
admin.site.register(AgreementSurvey)
admin.site.register(Response)
