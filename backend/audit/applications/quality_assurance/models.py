from common.models import CommonFile
from django.db import models
from django.contrib.postgres.fields import JSONField
from audit.core.abstract_model import AbstractModel
from project.models import Cycle, AgreementMember
from auditing.models import AuditAgency
from common.models import Comment, Country
from project.models import Cycle, Agreement
from account.models import User
from django.db.models import Count, F


class TeamComposition(AbstractModel):
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    audit_agency = models.ForeignKey(AuditAgency, on_delete=models.CASCADE)
    introduction = models.TextField()
    scope_and_objective = models.TextField()
    overall_conclusion = models.TextField()
    methodology = models.TextField()
    summary = models.TextField()
    comments = models.ManyToManyField(Comment)
    finalized = models.BooleanField(default=False)

    def __str__(self):
        return "{}-{}".format(self.audit_agency.legal_name, self.cycle.year)


class ImprovementArea(AbstractModel):
    team_composition = models.ForeignKey(
        TeamComposition,
        on_delete=models.CASCADE,
        related_name="team_composition_improvement_areas",
    )
    improvement_area = models.TextField(null=True, blank=True, default=None)
    auditor_comment = models.ForeignKey(
        Comment, on_delete=models.SET_NULL, null=True, blank=True, default=None
    )

    def __str__(self):
        return "{}".format(self.auditor_comment.message)


class TeamCompositionUser(AbstractModel):
    team_composition = models.ForeignKey(
        TeamComposition, on_delete=models.CASCADE, related_name="team_composition_users"
    )
    agreement_member = models.ForeignKey(AgreementMember, on_delete=models.CASCADE)

    @property
    def is_profile_completed(self):

        return all(
            [
                self.agreement_member.reviewer,
                self.agreement_member.team_type,
                self.agreement_member.role,
                self.agreement_member.duration_spent_in_days,
                self.agreement_member.tasks_completed,
                self.agreement_member.profile_summary,
                self.agreement_member.qualifications,
                self.agreement_member.years_of_experience,
                self.agreement_member.language,
                self.agreement_member.clients_audited_example,
                self.agreement_member.ISA_experience,
            ]
        )

    def __str__(self):
        return "{}".format(self.agreement_member.user.email)


class TeamCompositionCountry(AbstractModel):
    team_composition = models.ForeignKey(
        TeamComposition,
        on_delete=models.CASCADE,
        related_name="team_composition_countries",
    )
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.country.name)


class TeamCompositionCriteria(AbstractModel):
    description = models.TextField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return "{}".format(self.name)


class TeamCompositionCountryCriteria(AbstractModel):
    team_composition_country = models.ForeignKey(
        TeamCompositionCountry,
        null=True,
        on_delete=models.CASCADE,
        related_name="team_composition_country_criterias",
    )
    criteria = models.ForeignKey(
        TeamCompositionCriteria,
        on_delete=models.CASCADE,
        related_name="team_composition_criteria",
    )
    remarks = models.ForeignKey(
        Comment,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
        related_name="team_composition_remarks",
    )
    auditor_comment = models.ForeignKey(
        Comment,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
        related_name="team_composition_comments",
    )

    def __str__(self):
        return "{}".format(self.criteria.name)


class Message(AbstractModel):
    english = models.TextField()
    french = models.TextField()
    spanish = models.TextField()

    def __str__(self):
        return "{}".format(self.english)


class Survey(AbstractModel):
    SURVEY_TYPE_ENUM = (
        ("field", "Field"),
        ("partner", "Partner"),
        ("audit", "Audit"),
        ("audit_quality_survey", "Audit Quality Survey - KPI2"),
    )

    title = models.ForeignKey(Message, on_delete=models.CASCADE)
    type = models.CharField(max_length=50, choices=SURVEY_TYPE_ENUM)
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.title)


class QuestionCategory(AbstractModel):
    title = models.CharField(max_length=255)


class Question(AbstractModel):
    QUESTION_TYPE_ENUM = (
        ("input", "Input"),
        ("textarea", "Textarea"),
        ("radio", "Radio"),
        ("select", "Select"),
        ("multi_select", "Multi Select"),
        ("checkbox", "Checkbox"),
    )

    title = models.ForeignKey(Message, on_delete=models.CASCADE)
    type = models.CharField(max_length=50, choices=QUESTION_TYPE_ENUM)
    required = models.BooleanField(default=True)
    survey = models.ForeignKey(
        Survey, on_delete=models.CASCADE, related_name="survey_questions"
    )
    category = models.ForeignKey(
        QuestionCategory, models.SET_NULL, null=True, blank=True, default=None
    )
    option = JSONField(default=dict)
    comment_request_msg = models.ForeignKey(
        Message, on_delete=models.CASCADE, related_name="comment_msg"
    )
    comments = models.TextField(default=None, blank=True, null=True)

    def __str__(self):
        return "{}".format(self.title)

    def __str__(self):
        return "{}".format(self.title)


class AgreementSurvey(AbstractModel):
    SURVEY_STATUS_ENUM = (
        ("todo", "Todo"),
        ("in_progress", "In Progress"),
        ("done", "Done"),
    )
    agreement = models.ForeignKey(
        Agreement, on_delete=models.CASCADE, related_name="agreement_surveys"
    )
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    placeholders = JSONField(default=dict)
    status = models.CharField(max_length=50, choices=SURVEY_STATUS_ENUM, default="todo")

    def __str__(self):
        return "{}".format(self.survey.title)


class Response(AbstractModel):
    agreement_survey = models.ForeignKey(AgreementSurvey, on_delete=models.CASCADE, related_name="agreement_survey_responses")
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = JSONField(default=dict)
    placeholders = JSONField(default=dict)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.agreement_survey.survey.title)


class KPI3SurveyReport(AbstractModel):
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    audit_agency = models.ForeignKey(AuditAgency, on_delete=models.CASCADE)
    introduction = models.TextField(blank=True)
    scope_and_objective = models.TextField(blank=True)
    methodology = models.TextField(blank=True)
    conclusion = models.TextField(blank=True)
    comments = models.ManyToManyField(Comment)
    finalized = models.BooleanField(default=False)

    class Meta:
        unique_together = ("cycle", "audit_agency")


class AuditQuality(AbstractModel):
    STATUS_CHOICES = (
        ("todo", "Todo"),
        ("done", "Done"),
    )
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    audit_agency = models.ForeignKey(AuditAgency, on_delete=models.CASCADE, blank=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, blank=True)

    def __str__(self):
        return "{} - {}".format(self.audit_agency.legal_name, self.cycle.year)


class WorkingPaper(AbstractModel):
    agreement = models.ForeignKey(
        Agreement, on_delete=models.CASCADE, related_name="agreement_working_papers"
    )
    title = models.CharField(max_length=255)
    description = models.TextField()
    reviewed = models.BooleanField(default=False)
    date_received = models.DateField(null=True, blank=True, default=None)
    date_reviewed = models.DateField(null=True, blank=True, default=None)
    file = models.ForeignKey(
        CommonFile, null=True, blank=True, default=None, on_delete=models.SET_NULL
    )

    def __str__(self):
        return "{}".format(self.title)


class FinalReport(AbstractModel):
    audit_quality = models.ForeignKey(
        AuditQuality, on_delete=models.CASCADE, related_name="final_reports"
    )
    background = models.TextField(null=True, blank=True, default=None)
    scope_and_objective = models.TextField(null=True, blank=True, default=None)
    methodology = models.TextField(null=True, blank=True, default=None)
    finalized = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.audit_quality.cycle)


class SummaryReport(AbstractModel):
    audit_quality = models.ForeignKey(
        AuditQuality, on_delete=models.CASCADE, related_name="summary_reports"
    )
    introduction = models.TextField(null=True, blank=True, default=None)
    purpose = models.TextField(null=True, blank=True, default=None)
    overall_summary = models.TextField(null=True, blank=True, default=None)
    finalized = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.audit_quality.cycle)

    @property
    def report(self):

        cycle = self.audit_quality.cycle
        qs = Agreement.objects.filter(kpi2_sampled=True, cycle=cycle)

        data = (
            qs.values("country__region__name")
            .annotate(total=Count("country__region__name"))
            .values("total", region=F("country__region__name"))
            .order_by("total")
        )
        return data


class AuditSummary(AbstractModel):
    SUMMARY_STATUS_ENUM = (
        ("todo", "Todo"),
        ("in_progress", "In Progress"),
        ("done", "Done"),
    )
    audit_agency = models.ForeignKey(AuditAgency, on_delete=models.CASCADE)
    cycle = models.ForeignKey(Cycle, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=50, choices=SUMMARY_STATUS_ENUM, default="todo"
    )

    class Meta:
        unique_together = ("cycle", "audit_agency")


class AuditSummaryReport(AbstractModel):
    audit_summary = models.ForeignKey(
        AuditSummary, on_delete=models.CASCADE, related_name="audit_summary_reports"
    )
    introduction = models.TextField(blank=True)
    scope_and_objective = models.TextField(blank=True)
    methodology = models.TextField(blank=True)
    conclusion = models.TextField(blank=True)
    finalized = models.BooleanField(default=False)
    comments = models.ManyToManyField(Comment)
