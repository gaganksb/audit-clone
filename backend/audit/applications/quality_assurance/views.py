import math
import os
import random
from django.db.models.query import QuerySet
from rest_framework import generics, status
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
from rest_framework import exceptions
from rest_framework import status
from django.db.models import F, CharField
from django.db.models.functions import Concat
from quality_assurance.enums import SurveyTypeEnum
from utils.rest.code import code
from common.models import Comment, Country
from common.serializers import CountrySerializer, CommentSerializer
from common.helpers import filter_project_selection
from quality_assurance.models import *
from quality_assurance.models import Response as UserResponse
from quality_assurance.serializers import *
from quality_assurance.permissions import SurveyResponsePermission
from quality_assurance.filters import (
    TeamCompositionFilter,
    TeamCompositionProfileFilter,
    SurveyResponseFilter,
    AuditQualityFilter,
    AuditSummaryFilter,
)
from quality_assurance.tasks import (
    notify_teamcomposition_profile_update,
    notify_auditor_survey,
    start_agreement_survey,
    notify_KPI2_auditor_final,
    notify_KPI2_auditor_summary,
    import_agreement_survey,
)

from project.models import Agreement
from project.models import AgreementMember
from project.serializers import AgreementMemberUpdateSerializer
from project.serializers import AgreementSerializer
from rest_framework.response import Response

from django.http import HttpResponse
import pdfkit
from django.template.loader import render_to_string
from django.shortcuts import render
from django.conf import settings

from django_filters.rest_framework import DjangoFilterBackend
from django.db import transaction
from common.permissions import IsAuditAdmin, IsUNHCR, IsAuditAdminReadOnly, IsAUDIT
from common.consts import SURVEY_TYPES

from common.enums import UserTypeEnum
from common.pagination import TinyResultSetPagination
from dashboard.models import DataImport
from dashboard.serializers import DataImportSerialzier
from quality_assurance.helpers import (
    check_audit_work,
    check_audit_assignment_status,
    check_survey_uploaded,
)
from django.db.models import Q
from project.filters import AgreementFilter
from weasyprint import HTML


class AuditWorkStatus(generics.ListAPIView):
    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):
        bypass = request.data.get("bypass", None)
        response = check_audit_work(kwargs["year"], bypass)
        return Response(response, status=status.HTTP_200_OK)


class SurveyUploadStatus(generics.ListAPIView):
    def get(self, request, *args, **kwargs):
        bypass = request.data.get("bypass", False)
        response = check_survey_uploaded(kwargs["year"])
        return Response(response, status=status.HTTP_200_OK)


class TeamCompositionList(generics.ListAPIView):

    queryset = TeamComposition.objects.all()
    serializer_class = TeamCompositionSerializer


class TeamCompositionAPIView(generics.ListCreateAPIView):

    queryset = TeamComposition.objects.all()
    serializer_class = TeamCompositionSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    filter_backends = (DjangoFilterBackend,)
    filter_class = TeamCompositionFilter

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return self.queryset.filter(
                cycle__year=self.kwargs["year"],
                audit_agency_id=membership["audit_agency"]["id"],
            )
        return self.queryset.filter(cycle__year=self.kwargs["year"])

    def get_pre_sampling_response(self, data):

        response = {"count": len(data), "next": None, "previous": None, "results": []}

        result = []
        for item in data:
            result.append(
                {
                    "id": None,
                    "cycle": None,
                    "audit_agency": {
                        "id": item["audit_agency_id"],
                        "legal_name": item["legal_name"],
                    },
                    "team_composition_users": [],
                    "team_composition_countries": [],
                    "user": None,
                    "comments": [],
                    "status": "todo",
                }
            )
        response.update({"results": result})
        return response

    def get(self, request, *args, **kwargs):
        qs = self.get_queryset()
        if qs.exists():
            return self.list(request, *args, **kwargs)
        else:
            response = Agreement.objects.filter(
                cycle__year=kwargs["year"], audit_firm__isnull=False
            )
            print(response)
            if response.exists():
                data = (
                    response.distinct("audit_firm")
                    .annotate(
                        audit_agency_id=F("audit_firm_id"),
                        legal_name=F("audit_firm__legal_name"),
                    )
                    .values("audit_agency_id", "legal_name")
                )
                res = self.get_pre_sampling_response(data)
                return Response(res, status=status.HTTP_200_OK)
        return Response([], status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        cycle = Cycle.objects.filter(year=kwargs["year"]).first()

        if cycle is not None:
            for data in request.data:
                countries = data.get("countries")
                p = {
                    "audit_agency_id": int(data.get("audit_agency")),
                    "cycle_id": cycle.id,
                }

                team_composition, _ = TeamComposition.objects.get_or_create(**p)
                team_composition.user = request.user
                team_composition.save()

                for country in countries:
                    TeamCompositionCountry.objects.create(
                        team_composition=team_composition, country_id=country
                    )
            return Response({"message": "success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(
                {"code": "E_BAD_REQUEST", "message": code["E_BAD_REQUEST"]},
                status=status.HTTP_400_BAD_REQUEST,
            )


class TeamCompositionDetail(generics.RetrieveUpdateAPIView):

    queryset = TeamComposition.objects.all()
    serializer_class = TeamCompositionSerializer

    @transaction.atomic
    def patch(self, request, *args, **kwargs):

        team_composition_country_criteria = request.data.get(
            "team_composition_country_criteria", None
        )
        comments = request.data.get("comments", [])

        for comment in comments:
            serializer = CommentSerializer(data=comment)
            if serializer.is_valid():
                comment = serializer.save()
                team_composition = self.get_object()
                team_composition.comments.clear()
                team_composition.comments.add(comment)
                team_composition.save()

        if team_composition_country_criteria is not None:
            for criteria in team_composition_country_criteria:
                remarks = criteria.get("remarks", None)
                auditor_comment = criteria.get("auditor_comment", None)

                remarks = Comment.objects.create(message=remarks)
                auditor_comment = Comment.objects.create(message=auditor_comment)
                criteria.update(
                    {"remarks": remarks.id, "auditor_comment": auditor_comment.id}
                )
                if type(criteria["criteria"]) != int and "criteria" in criteria:
                    criteria.update({"criteria": criteria["criteria"]["id"]})
                    criteria_id = criteria["criteria"]["id"]
                else:
                    criteria_id = criteria.get("criteria")
                criteria_data = TeamCompositionCountryCriteria.objects.filter(
                    team_composition_country_id=criteria.get(
                        "team_composition_country"
                    ),
                    criteria=criteria_id,
                ).first()
                if criteria_data is not None:
                    serializer = TeamCompositionCountryCriteriaSerializer(
                        criteria_data, data=criteria
                    )
                else:
                    serializer = TeamCompositionCountryCriteriaSerializer(data=criteria)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(
                        serializer.errors, status=status.HTTP_400_BAD_REQUEST
                    )
            return self.partial_update(request, *args, **kwargs)
        else:
            return self.partial_update(request, *args, **kwargs)


class TeamCompositionUpdate(generics.RetrieveUpdateAPIView):

    queryset = AgreementMember.objects.all()
    serializer_class = AgreementMemberUpdateSerializer
    permission_classes = (IsAuthenticated,)


class ImprovementAreasAPIView(generics.CreateAPIView):

    queryset = ImprovementArea.objects.all()
    serializer_class = ImprovementAreaSerializer


class ImprovementAreasDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = ImprovementArea.objects.all()
    serializer_class = ImprovementAreaSerializer


class KPI4SampleCountriesAPIView(generics.CreateAPIView):

    queryset = Agreement.objects.all()
    serializers = AgreementSerializer

    def post(self, request, *args, **kwargs):

        audit_firms = (
            Agreement.objects.filter(
                cycle__year=kwargs["year"], audit_firm__isnull=False
            )
            .distinct("audit_firm")
            .values("audit_firm_id", "audit_firm__legal_name")
        )

        response = []
        for firm in audit_firms:
            data = Agreement.objects.filter(
                audit_firm_id=firm["audit_firm_id"]
            ).distinct("country")
            total_countries_for_agency = data.count()
            sample_count_20_percent = total_countries_for_agency * 0.20
            sample_count_20_percent = math.ceil(sample_count_20_percent)
            countries = list(data.values_list("country", flat=True))
            country_ids = random.sample(countries, sample_count_20_percent)
            country_qs = Country.objects.filter(id__in=country_ids)
            serializer = CountrySerializer(country_qs, many=True)
            response.append(
                {
                    "audit_firm": {
                        "id": firm["audit_firm_id"],
                        "legal_name": firm["audit_firm__legal_name"],
                    },
                    "countries": serializer.data,
                }
            )
        return Response(response)


class TeamCompositionCriteriasAPIView(generics.ListAPIView):
    queryset = TeamCompositionCriteria.objects.all()
    serializer_class = TeamCompositionCriteriaSerializer


class AgreementMemberProjects(generics.ListAPIView):

    queryset = AgreementMember.objects.all()
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    def validated_data(self):
        agreement_member_id = self.request.query_params.get("agreement_member_id", None)
        year = self.request.query_params.get("year", None)
        if agreement_member_id is None or year is None:
            raise exceptions.ValidationError("year and agreement member id is required")
        else:
            return agreement_member_id, year, self.request.user.membership

    def get(self, request, *args, **kwargs):
        agreement_member_id, year, membership = self.validated_data()
        membership = self.request.user.membership

        if membership["type"] == "AUDITOR":
            agreements = Agreement.objects.filter(
                audit_firm_id=membership["audit_agency"]["id"], cycle__year=year
            )
            am = AgreementMember.objects.filter(id=agreement_member_id).first()
            agreements = (
                agreements.filter(focal_points=am)
                .annotate(
                    ppa=Concat(
                        F("business_unit__code"), F("number"), output_field=CharField()
                    )
                )
                .values_list("ppa", flat=True)
            )
            return Response(set(agreements))

        else:
            agreements = Agreement.objects.filter(cycle__year=year)
            am = AgreementMember.objects.filter(id=agreement_member_id).first()
            agreements = (
                agreements.filter(focal_points=am)
                .annotate(
                    ppa=Concat(
                        F("business_unit__code"), F("number"), output_field=CharField()
                    )
                )
                .values_list("ppa", flat=True)
            )
            return Response(set(agreements))


class TeamCompositionProfileDetail(generics.ListAPIView):

    queryset = TeamComposition.objects.all()
    serializer_class = TeamCompositionProfileSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = TeamCompositionProfileFilter


class NotifyTeamCompositionAPIView(generics.CreateAPIView):

    queryset = TeamCompositionUser.objects.all()
    serializers = TeamCompositionUserSerializer

    def post(self, request, *args, **kwargs):
        year = request.data.get("year", None)
        if year is not None:
            notify_teamcomposition_profile_update(year)
            return Response({"message": "Notification sent succefully"}, status=200)
        else:
            return Response(
                {"message": "Year is a required field to send notification"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class TeamCompositionPDFExport(generics.ListAPIView):

    queryset = TeamComposition.objects.all()
    serializer_class = TeamCompositionSerializer

    def get(self, request, *args, **kwargs):

        year = self.request.query_params.get("year", None)
        team_composition = self.queryset.filter(
            id=kwargs["pk"], cycle__year=year
        ).first()

        options = {
            "page-size": "Letter",
            "encoding": "UTF-8",
        }

        if team_composition is None or year is None:
            error = "{<h4>Bad request, Year is Required </h4>}"
            pdf = pdfkit.from_string(error, False, options)
            response = HttpResponse(pdf, content_type="application/pdf")
            response[
                "Content-Disposition"
            ] = 'attachment; filename = "teamcomposition_report.pdf"'
            return response

        else:
            teamcomposition_serializer = self.serializer_class(
                team_composition, context={"request": request}
            )

            if settings.ENV == "local":
                context = {
                    "team_composition": teamcomposition_serializer.data,
                }
                return render(request, "report.html", context=context)

            else:
                html = render_to_string(
                    "report.html",
                    {
                        "team_composition": teamcomposition_serializer.data,
                    },
                )
                options = {
                    "page-size": "Letter",
                    "encoding": "UTF-8",
                }

                pdf = pdfkit.from_string(html, False, options)
                response = HttpResponse(pdf, content_type="application/pdf")
                response["Content-Disposition"] = "attachment;"

                return response


class AgreementSurveySelectionAPIView(generics.ListAPIView):
    queryset = AgreementSurvey.objects.all()
    serializer_class = AgreementSurveySerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    def get(self, request, *args, **kwargs):

        year = self.request.query_params.get("year")
        audit_agency = self.request.query_params.get("audit_agency", None)
        membership = request.user.membership

        if membership["type"] == UserTypeEnum.AUDITOR.name:
            self.queryset = self.queryset.filter(
                agreement__audit_firm_id=membership["audit_agency"]["id"]
            )

        if audit_agency is not None and membership["type"] == UserTypeEnum.HQ.name:
            self.queryset = self.queryset.filter(agreement__audit_firm_id=audit_agency)

        data = (
            self.queryset.filter(survey__cycle__year=year)
            .distinct("agreement__audit_firm")
            .annotate(
                audit_agency_id=F("agreement__audit_firm_id"),
                legal_name=F("agreement__audit_firm__legal_name"),
                # status=F("agreement__audit_firm__legal_name")
            )
            .values("id", "audit_agency_id", "legal_name")
        )
        response_data = []
        for row in data:
            s = KPI3SurveyReport.objects.filter(
                audit_agency_id=row["audit_agency_id"], cycle__year=year
            ).first()
            if s is not None:
                row.update({"status": "done" if s.finalized == True else "todo"})
            response_data.append(row)
        return Response(response_data, status=status.HTTP_200_OK)


class AgreementSurveySelectionDetail(generics.RetrieveAPIView):

    queryset = AgreementSurvey.objects.all()
    serializer_class = AgreementSurveySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        if self.request.user.membership["type"] == UserTypeEnum.AUDITOR.name:
            return self.queryset.filter(
                id=self.kwargs["pk"],
                survey__type=SURVEY_TYPES.AUDITOR,
                agreement__focal_points__user_type="auditing",
                agreement__focal_points__user_id=self.request.user.id,
                agreement__audit_firm_id=self.request.user.membership["audit_agency"][
                    "id"
                ],
            ).distinct("id")
        elif self.request.user.membership["type"] == UserTypeEnum.FO.name:
            return self.queryset.filter(
                id=self.kwargs["pk"],
                survey__type=SURVEY_TYPES.FIELD,
                agreement__business_unit__field_office_id=self.request.user.membership[
                    "field_office"
                ]["id"],
            ).distinct("id")
        elif self.request.user.membership["type"] == UserTypeEnum.PARTNER.name:
            return self.queryset.filter(
                id=self.kwargs["pk"],
                survey__type=SURVEY_TYPES.PARTNER,
                agreement__focal_points__user_type="partner",
                agreement__focal_points__user_id=self.request.user.id,
                agreement__partner_id=self.request.user.membership["partner"]["id"],
            ).distinct("id")
        elif self.request.user.membership["type"] == UserTypeEnum.HQ.name:
            return self.queryset.filter(id=self.kwargs["pk"]).distinct("id")
        else:
            return self.queryset.none()


class AgreementSurveyResponse(generics.CreateAPIView):

    queryset = AgreementSurvey.objects.all()
    serializer_class = AgreementSurveySerializer
    permission_classes = (
        IsAuthenticated,
        SurveyResponsePermission,
    )

    def post(self, request, *args, **kwargs):

        agreement_survey = self.queryset.filter(id=kwargs["pk"]).first()
        action = request.data.get("action", None)
        valid_actions = ["save", "update", "finalize"]
        if (
            agreement_survey is not None
            and action is not None
            and action in valid_actions
        ):
            response_datas = request.data.get("responses")
            for response_data in response_datas:
                data = {
                    "question": response_data.get("question"),
                    "agreement_survey": agreement_survey.id,
                    "answer": response_data.get("answer", None),
                    "user": request.user.id,
                }
                if action == "save":
                    try:
                        user_response = UserResponse.objects.get(
                            agreement_survey_id=kwargs["pk"],
                            question_id=data["question"],
                        )
                        serializer = ResponseSerializer(user_response, data=data)
                        if serializer.is_valid():
                            serializer.save()
                        else:
                            raise exceptions.APIException(serializer.errors)

                    except UserResponse.DoesNotExist:
                        user_response = None
                        if user_response is None:
                            serializer = ResponseSerializer(data=data)
                            if serializer.is_valid():
                                serializer.save()
                        else:
                            raise exceptions.APIException(serializer.errors)

                elif action == "finalize":
                    agreement_survey.status = "done"
                    agreement_survey.save()
            return Response(
                {"message": "Response created"}, status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                {"message": "No Agreement Survey found"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class AgreementSurveyStart(generics.CreateAPIView):

    queryset = AgreementSurvey.objects.all()
    serializer_class = AgreementSurveySerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def post(self, request, *args, **kwargs):
        survey_qs = Survey.objects.filter(cycle__year=kwargs["year"])
        if survey_qs.exists() == False:
            msg = "Please update survey before starting survey for this year"
            return Response({"message": msg}, status=status.HTTP_400_BAD_REQUEST)
        start_agreement_survey(kwargs["year"])
        return Response({"message": "Started Agreement Survey Phase"})


class AgreementSurveyNotifyAPIView(generics.CreateAPIView):

    queryset = AgreementSurvey.objects.all()
    serializer_class = AgreementSurveySerializer

    def post(self, request, *args, **kwargs):
        year = request.data.get("year", None)
        audit_firm_id = request.data.get("audit_firm_id", None)

        if year is not None and audit_firm_id is not None:
            notify_auditor_survey(year, audit_firm_id)
            return Response({"message": "Notification sent successfully"}, status=200)
        else:
            return Response(
                {"message": "Year is a required field to send notification"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class SurveyPDFExport(generics.ListAPIView):

    queryset = KPI3SurveyReport.objects.all()
    serializer_class = KPI3SurveyReportSerializer

    def get(self, request, *args, **kwargs):

        year = self.request.query_params.get("year", None)
        survey_details = self.queryset.filter(cycle__year=year).first()

        options = {
            "page-size": "Letter",
            "encoding": "UTF-8",
        }

        if survey_details is None or year is None:
            error = "{<h4>Bad request, Year is Required </h4>}"
            pdf = pdfkit.from_string(error, False, options)
            response = HttpResponse(pdf, content_type="application/pdf")
            response[
                "Content-Disposition"
            ] = 'attachment; filename = "teamcomposition_report.pdf"'
            return response

        else:
            survey_details_serializer = self.serializer_class(
                survey_details, context={"request": request}
            )

            if settings.ENV == "local":
                context = {
                    "survey_details": survey_details_serializer.data,
                }
                return render(request, "report.html", context=context)

            else:
                html = render_to_string(
                    "report.html",
                    {
                        "survey_details": survey_details_serializer.data,
                    },
                )
                options = {
                    "page-size": "Letter",
                    "encoding": "UTF-8",
                }

                pdf = pdfkit.from_string(html, False, options)
                response = HttpResponse(pdf, content_type="application/pdf")
                response["Content-Disposition"] = "attachment;"

                return response


class KPI3SurveyReportAPIView(generics.RetrieveAPIView):

    queryset = KPI3SurveyReport.objects.all()
    serializer_class = KPI3SurveyReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    def get_questions(self, year):

        queryset = Survey.objects.filter(
            survey_questions__parent__isnull=True,
            cycle__year=year,
        )

        display_field = ["question_type", "question", "question_title", "type"]
        field_questions = (
            queryset.filter(type="field")
            .distinct("question")
            .prefetch_related("survey_questions")
            .distinct("survey_questions")
            .annotate(
                question_type=F("survey_questions__type"),
                question=F("survey_questions"),
                question_title=F("survey_questions__title__english"),
            )
            .values(*display_field)
        )
        partner_questions = (
            queryset.filter(type="partner")
            .prefetch_related("survey_questions")
            .distinct("survey_questions")
            .annotate(
                question_type=F("survey_questions__type"),
                question=F("survey_questions"),
                question_title=F("survey_questions__title__english"),
            )
            .values(*display_field)
        )
        auditor_questions = (
            queryset.filter(type="auditor")
            .prefetch_related("survey_questions")
            .distinct("survey_questions")
            .annotate(
                question_type=F("survey_questions__type"),
                question=F("survey_questions"),
                question_title=F("survey_questions__title__english"),
            )
            .values(*display_field)
        )
        return {
            "field_questions": field_questions,
            "partner_questions": partner_questions,
            "auditor_questions": auditor_questions,
        }

    def get(self, request, *args, **kwargs):
        cycle = Cycle.objects.filter(year=kwargs["pk"]).first()
        audit_agency = AuditAgency.objects.filter(id=kwargs["audit_agency"]).first()
        if cycle is not None and audit_agency is not None:
            membership = request.user.membership
            if membership["type"] == UserTypeEnum.AUDITOR.name:
                if membership["audit_agency"]["id"] != audit_agency.id:
                    return Response({}, status=status.HTTP_200_OK)
                KPI3_survey_report = KPI3SurveyReport.objects.filter(
                    cycle=cycle, audit_agency=membership["audit_agency"]["id"]
                ).first()
            else:
                KPI3_survey_report, _ = KPI3SurveyReport.objects.get_or_create(
                    cycle=cycle, audit_agency=audit_agency
                )
            if KPI3_survey_report is not None:
                serializer = KPI3SurveyReportSerializer(KPI3_survey_report)
                questions = self.get_questions(cycle.year)
                response = serializer.data
                response.update({"questions": questions})
                return Response(response, status=status.HTTP_200_OK)
            return Response({}, status=status.HTTP_200_OK)
        else:
            return Response(
                {"detail": "Invalid Cycle or Audit Agency"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class KPI3SurveyReportUpdateAPIView(generics.UpdateAPIView):

    queryset = KPI3SurveyReport.objects.all()
    serializer_class = KPI3SurveyReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)


class ResponseListAPIView(generics.ListAPIView):

    queryset = UserResponse.objects.all()
    serializer_class = ResponseSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = SurveyResponseFilter
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)
    pagination_class = TinyResultSetPagination

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return self.queryset.filter(
                agreement_survey__survey__cycle__year=self.kwargs["year"],
                agreement_survey__agreement__audit_firm_id=membership["audit_agency"][
                    "id"
                ],
            )
        else:
            return self.queryset.filter(
                agreement_survey__survey__cycle__year=self.kwargs["year"]
            )

    def get(self, request, *args, **kwargs):
        survey_type = request.query_params.get("survey_type", None)
        survey_types = ["field", "auditor", "partner"]
        if survey_type is not None and survey_type in survey_types:
            return self.list(request, *args, **kwargs)
        else:
            filtered_data = self.filter_class(request.query_params, self.get_queryset())
            serializer = self.serializer_class(filtered_data.qs, many=True)
            return Response(serializer.data)


class SurveyStats(generics.ListAPIView):

    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdmin)

    survey_types = [
        ["Satisfactory", "Yes"],
        ["Partially Satisfactory"],
        ["Unsatisfactory", "No"],
    ]

    def build_response(self, option, queryset):
        response_percentage = 0
        filtered_qs = queryset.filter(answer__answer=option)

        if queryset.count() > 0:
            response_percentage = round(
                (filtered_qs.count() / queryset.count()) * 100, 2
            )

        return {
            "option": option,
            "response_count": filtered_qs.count(),
            "response_percentage": response_percentage,
        }

    def get(self, request, *args, **kwargs):

        year = request.query_params.get("year", None)
        question = request.query_params.get("question", None)
        survey_type = request.query_params.get("survey_type", None)
        membership = request.user.membership

        membership = request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            queryset = UserResponse.objects.filter(
                agreement_survey__survey__cycle__year=year,
                agreement_survey__survey__type=survey_type,
                agreement_survey__agreement__audit_firm_id=membership["audit_agency"][
                    "id"
                ],
                question=question,
            )
            survey_types = Question.objects.filter(id=question).values_list(
                "question_options__title__english", flat=True
            )
        else:
            queryset = UserResponse.objects.filter(
                agreement_survey__survey__cycle__year=year,
                agreement_survey__survey__type=survey_type,
                question=question,
            )
            survey_types = Question.objects.filter(id=question).values_list(
                "question_options__title__english", flat=True
            )

        response = []
        for s_type in survey_types:
            res = self.build_response(s_type, queryset)
            response.append(res)
        return Response(response, status=status.HTTP_200_OK)


class CheckSurveyImported(generics.ListAPIView):

    permission_classes = (IsAuthenticated, IsUNHCR)

    def get(self, request, *args, **kwargs):
        year = kwargs["year"]
        survey_qs = Survey.objects.filter(cycle__year=year)
        response = {"questions_uploaded": survey_qs.exists(), "year": year}
        return Response(response, status=status.HTTP_200_OK)


class SurveyTemplateDownload(generics.ListAPIView):
    def get(self, request):
        file_name = "survey_template_sample.xlsx"
        file_handle = os.path.join(
            os.getcwd(),
            "audit",
            "applications",
            "quality_assurance",
            "excel",
            "survey_import_template.xlsx",
        )
        document = open(file_handle, "rb")
        response = HttpResponse(document, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = 'attachment; filename="%s"' % file_name
        return response


class SurveyQuestionsUpload(generics.ListCreateAPIView):

    queryset = DataImport.objects.all()
    serializer_class = DataImportSerialzier


class KPI2Sample(generics.ListCreateAPIView):

    queryset = AuditQuality.objects.all()
    serializer_class = AuditQualitySerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdminReadOnly)

    filter_backends = (DjangoFilterBackend,)
    filter_class = AuditQualityFilter

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return (
                super()
                .get_queryset()
                .filter(
                    audit_agency_id=membership["audit_agency"]["id"],
                    cycle__year=self.kwargs["year"],
                )
            )
        return super().get_queryset().filter(cycle__year=self.kwargs["year"])

    def sample_projects(self, year, sample_size):
        """
        Check if audit assignment and work has been completed
        Delete sampling if any previous sampling exists for request cycle
        Delete related Agreement Survey for the request cycle
        Sample new project for the request cycle
        """

        audit_work_completed = check_audit_work(year, True)
        audit_assignment_completed = check_audit_assignment_status(year, True)
        if not audit_work_completed or not audit_assignment_completed:
            msg = {"message": "Audit assignment/work still pending"}
            return Response(msg, status=status.HTTP_400_BAD_REQUEST)

        AgreementSurvey.objects.filter(
            survey__type=SurveyTypeEnum.KPI2.value, survey__cycle__year=year
        ).delete()
        AuditQuality.objects.filter(cycle__year=year).delete()
        agreements_qs = Agreement.objects.filter(cycle__year=year)
        agreements_qs.update(kpi2_sampled=False)
        agreements = filter_project_selection(agreements_qs, year, "fin", "included")
        total_agreements = agreements.count()
        sample_count_20_percent = total_agreements * sample_size
        sample_count_20_percent = math.ceil(sample_count_20_percent)
        sampled_agreements = list(agreements.values_list("id", flat=True))
        sampled_agreements_ids = random.sample(
            sampled_agreements, sample_count_20_percent
        )
        sampled_agreements = agreements_qs.filter(id__in=sampled_agreements_ids)
        sampled_agreements.update(kpi2_sampled=True)
        msg = {"message": "Sampling Completed"}
        return Response(msg, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        year = kwargs["year"]
        cycle = Cycle.objects.filter(year=year).first()
        if cycle is not None:
            return self.sample_projects(year, 0.20)
        else:
            response = {"message": "Invalid Cycle"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class KPI2Start(generics.CreateAPIView):

    queryset = AuditQuality.objects.all()
    serializer_class = AuditQualitySerializer
    permission_classes = (IsAuthenticated, IsUNHCR)

    def create_agreement_survey(self, year, survey_type):
        AgreementSurvey.objects.filter(
            survey__type=SurveyTypeEnum.KPI2.value, survey__cycle__year=year
        ).delete()
        return import_agreement_survey(year, survey_type)

    def delete_audit_quality(self, cycle):
        audit_quality_qs = AuditQuality.objects.filter(cycle=cycle)
        FinalReport.objects.filter(
            audit_quality__in=audit_quality_qs.values_list("id", flat=True)
        ).delete()
        SummaryReport.objects.filter(
            audit_quality__in=audit_quality_qs.values_list("id", flat=True)
        ).delete()
        audit_quality_qs.delete()

    def create_reports(self, cycle, sampled_agreements):
        """
        Delete old Audit Quality
        Delete old Audit Quality Report
        Create New Audit quality for the requested cycle
        Create related reports
        """
        self.delete_audit_quality(cycle)

        audit_firms = (
            sampled_agreements.filter(audit_firm__isnull=False)
            .distinct("audit_firm")
            .values_list("audit_firm", flat=True)
        )
        if audit_firms.exists():
            audit_quality_per_firm = []
            for firm in audit_firms:
                audit_quality_per_firm.append(
                    AuditQuality(audit_agency_id=firm, cycle=cycle, status="todo")
                )
            audit_quality_data = AuditQuality.objects.bulk_create(
                audit_quality_per_firm
            )
            for audit_quality in audit_quality_data:
                FinalReport.objects.get_or_create(audit_quality=audit_quality)
                SummaryReport.objects.get_or_create(audit_quality=audit_quality)
            qs = self.queryset.filter(cycle__year=cycle.year)
            serializer = self.serializer_class(qs, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            response = {"message": "Audit work is not completed yet"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        KPI2Sample().post(request, *args, **kwargs)
        survey_type = request.data.get("survey_type", "audit_quality_survey")
        cycle = Cycle.objects.filter(year=kwargs["year"]).first()
        if survey_type is not None and cycle is not None:
            sampled_agreements = self.create_agreement_survey(cycle.year, survey_type)
            response = self.create_reports(cycle, sampled_agreements)
            return response
        else:
            response = {"message": "Invalid cycle or survey type"}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)


class KPI2SummaryReportAPIView(generics.RetrieveUpdateAPIView):

    queryset = SummaryReport.objects.all()
    serializer_class = SummaryReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdminReadOnly)

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return (
                super()
                .get_queryset()
                .filter(audit_quality__audit_agency_id=membership["audit_agency"]["id"])
            )
        return super().get_queryset()


class KPI2FinalReportAPIView(generics.RetrieveUpdateAPIView):

    queryset = FinalReport.objects.all()
    serializer_class = FinalReportSerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdminReadOnly)

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return (
                super()
                .get_queryset()
                .filter(audit_quality__audit_agency_id=membership["audit_agency"]["id"])
            )
        return super().get_queryset()


class RequestWorkingPapers(generics.ListCreateAPIView):

    queryset = WorkingPaper.objects.all()
    serializer_class = WorkingPaperSerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class WorkingPaperDetailAPIView(generics.RetrieveUpdateDestroyAPIView):

    queryset = WorkingPaper.objects.all()
    serializer_class = WorkingPaperSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return (
                super()
                .get_queryset()
                .filter(agreement__audit_firm_id=membership["audit_agency"]["id"])
            )
        return super().get_queryset()

    def delete(self, request, *args, **kwargs):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.HQ.name:
            data = self.destroy(request, *args, **kwargs)
            return Response({"result": "success"}, status=status.HTTP_200_OK)
        else:
            return Response(
                {"message": "You are not authorized to delete working paper"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class UploadWorkingPaper(generics.UpdateAPIView):

    queryset = WorkingPaper.objects.all()
    serializer_class = WorkingPaperSerializer
    permission_classes = (IsAuthenticated, IsAUDIT)


class KPI2AuditorNotifyAPIView(generics.CreateAPIView):

    queryset = FinalReport.objects.all()
    serializer_class = FinalReportSerializer

    def post(self, request, *args, **kwargs):
        year = request.data.get("year", None)
        audit_firm_id = request.data.get("audit_firm_id", None)
        notification_type = request.data.get("notification_type", None)

        if year is not None and audit_firm_id is not None:
            if notification_type == "final_report":
                notify_KPI2_auditor_final(year, audit_firm_id)
                return Response(
                    {"message": "Notification for final report sent successfully"},
                    status=200,
                )
            elif notification_type == "summary_report":
                notify_KPI2_auditor_summary(year, audit_firm_id)
                return Response(
                    {"message": "Notification for summary report sent successfully"},
                    status=200,
                )
        else:
            return Response(
                {"message": "Year is a required field to send notification"},
                status=status.HTTP_400_BAD_REQUEST,
            )


class KPI1Start(generics.CreateAPIView):

    queryset = AuditSummary.objects.all()
    serializer_class = AuditSummarySerializer
    permission_classes = (IsAuthenticated, IsUNHCR)


class KPI1AuditSummary(generics.ListCreateAPIView):

    queryset = AuditSummary.objects.all()
    serializer_class = AuditSummarySerializer
    permission_classes = (IsAuthenticated, IsUNHCR | IsAuditAdminReadOnly)

    filter_backends = (DjangoFilterBackend,)
    filter_class = AuditSummaryFilter

    def get_queryset(self):
        membership = self.request.user.membership
        if membership["type"] == UserTypeEnum.AUDITOR.name:
            return (
                super()
                .get_queryset()
                .filter(audit_agency_id=membership["audit_agency"]["id"])
            )
        return super().get_queryset()


class KPI2KeyFindingsAPIView(generics.ListAPIView):

    queryset = Agreement.objects.all()
    serializer_class = KPI2KeyFindingsSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = AgreementFilter

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(kpi2_sampled=True, audit_firm_id=self.kwargs["audit_firm"])
        )


class KPI2FinalPDFExport(generics.ListAPIView):
    def get(self, request, *args, **kwargs):

        year = kwargs.get("year", None)
        audit_firm = kwargs.get("audit_firm", None)
        final_report = FinalReport.objects.filter(
            audit_quality__cycle__year=year, audit_quality__audit_agency_id=audit_firm
        ).first()

        if all([year, audit_firm, final_report]):
            serializer = FinalReportSerializer(final_report)
            agreement_survey_ids = AgreementSurvey.objects.filter(
                agreement__cycle__year=year, agreement__audit_firm_id=audit_firm
            ).values_list("id", flat=True)
            responses = UserResponse.objects.filter(
                ~Q(answer__answer="Yes"),
                Q(agreement_survey_id__in=agreement_survey_ids),
            ).annotate(
                business_unit=F("agreement_survey__agreement__business_unit__code"),
                number=F("agreement_survey__agreement__number"),
                questions=F("question__title__english"),
            )
            fields = [
                "business_unit",
                "number",
                "questions",
                "answer",
            ]

            key_findings = list(responses.values(*fields))

            context = {
                "final_report": serializer.data,
                "key_findings": key_findings,
                "year": year
            }

            html = render_to_string(
                "final_report.html",
                context,
            )
            htmldoc = HTML(string=html, base_url="")
            css = os.path.join(
                "audit",
                "applications",
                "quality_assurance",
                "css/soe.css"
            )
            data = htmldoc.write_pdf(stylesheets=[css])
            response = HttpResponse(content_type="application/pdf")
            # response["Content-Disposition"] = "attachment;"
            response.write(data)
            return response
        else:
            response = ({"message": "Invalid Year or Audit Firm ID"},)
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
