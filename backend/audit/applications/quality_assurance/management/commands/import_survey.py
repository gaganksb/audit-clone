from django.core.management.base import BaseCommand
from quality_assurance.tasks import survey_import_command


class Command(BaseCommand):
    help = "Import Partner, Audit and Field survey after audit assignment"

    def add_arguments(self, parser):
        parser.add_argument(
            "-y",
            "--year",
            type=int,
            help="year",
        )

    def handle(self, *args, **kwargs):
        year = kwargs.get("year", None)
        survey_import_command(year, None)
