from quality_assurance.models import AgreementSurvey
from django.core.management.base import BaseCommand
from django.db.models import query
from django.db import transaction
from notify.models import NotifiedUser
from account.models import User
from common.helpers import filter_project_selection
from notify.helpers import send_notification
from notify.helpers import send_notification_to_users
from django.conf import settings
from project.models import Agreement
from quality_assurance.models import Survey


class Command(BaseCommand):
    help = "Send notifications to Agreement members for the survey"

    def add_arguments(self, parser):
        parser.add_argument(
            "-y",
            "--year",
            type=int,
            help="year",
        )

    def handle(self, *args, **kwargs):
        year = kwargs["year"]
        agreements = Agreement.objects.all()

        queryset = filter_project_selection(agreements, year, "fin", "included")
        project_ids = queryset.distinct("id").values_list("id", flat=True)

        surveys = Survey.objects.filter(cycle__year=year)

        for survey in surveys:
            for agreement_id in project_ids:
                agreement_survey, created = AgreementSurvey.objects.get_or_create(
                    agreement_id=agreement_id,
                    survey=survey,
                    placeholders={"year": year},
                )

                # if created:
                focal_points = agreement_survey.agreement.focal_points.distinct(
                    "user_id"
                )
                users_list = focal_points.values_list("user__id", flat=True)
                users = User.objects.filter(id__in=users_list)

                for user in users:
                    context = {
                        "year": year,
                        # "project_title": project.title,
                        "subject": "Surveys are Ready!",
                    }
                    send_notification(
                        notification_type="agreement_survey",
                        obj=agreement_survey,
                        users=[user],
                        context=context,
                    )
