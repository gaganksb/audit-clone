import xlrd
from django.db import transaction
from background_task import background
from notify.models import NotifiedUser
from quality_assurance.models import (
    TeamCompositionUser,
    AgreementSurvey,
    FinalReport,
    SummaryReport,
    QuestionCategory,
)
from auditing.models import AuditMember, AuditAgency
from common.helpers import filter_project_selection
from quality_assurance.models import (
    Message,
    Survey,
    Question,
    # Option,
    AgreementSurvey,
)
from project.models import Cycle, Agreement
from account.models import User
from common.models import CommonFile
from notify.helpers import send_notification, send_notification_to_users
from django.core.management import call_command
from quality_assurance.enums import SurveyTypeEnum


class SurveyImport:
    def __init__(self, year, file_id):
        file_obj = CommonFile.objects.get(id=file_id)
        self.cycle = Cycle.objects.filter(year=year).first()
        self.wb = xlrd.open_workbook(file_contents=file_obj.file_field.read())

    def excel_to_dict(self, s):
        sheet = self.wb.sheet_by_name(s)
        columns = []
        for col in range(sheet.ncols):
            columns.append(sheet.cell(0, col).value)

        response = []
        for row in range(sheet.nrows):
            if row == 0:
                continue
            res = {}
            for idx, col in enumerate(columns):
                res.update({"{}".format(col): sheet.cell(row, idx).value})
            response.append(res)
        return response

    def get_messages(self, data, columns_with_msg):
        res = {}
        for colmn in columns_with_msg:
            eng_msgs = set([i[colmn] for i in data])
            survey_msgs = (
                Message.objects.filter(english__in=eng_msgs)
                .values_list("id", "english")
                .distinct("id")
            )
            res.update({v: k for k, v in survey_msgs})
        return res

    def import_messages(self, data, columns_with_msg):
        unique_msgs_dict = self.get_messages(data, columns_with_msg)
        data_list = []
        for row in data:
            for col in columns_with_msg:
                msgs = row[col]
                if ";;" in msgs:
                    for msg in msgs.split(";;"):
                        msg_to_check = msg
                else:
                    msg_to_check = msgs

                if msg_to_check not in unique_msgs_dict:
                    data_list.append(Message(english=msg_to_check))
        Message.objects.bulk_create(data_list)

    def import_survey(self, data):
        if self.cycle is not None:
            self.import_messages(data, ["survey"])
            unique_msgs_dict = self.get_messages(data, ["survey"])
            survey_msgs = Survey.objects.filter(cycle=self.cycle).values_list(
                "id", "title__english"
            )
            survey_msgs_dict = {v: k for k, v in survey_msgs}
            survey_list = []
            for survey in data:
                if survey["survey"] not in survey_msgs_dict:
                    survey_list.append(
                        Survey(
                            title_id=unique_msgs_dict[survey["survey"]],
                            type=survey["survey_type"],
                            cycle=self.cycle,
                        )
                    )
            Survey.objects.bulk_create(survey_list)

    def import_questions_and_options(self, data, survey_type):
        category = QuestionCategory.objects.all().values_list("id", "title")
        category_dict = {v: k for k, v in category}
        msg_fields = ["question", "comment_msg"]
        self.import_messages(data, msg_fields)
        unique_msgs_dict = self.get_messages(data, msg_fields)

        survey = Survey.objects.filter(cycle=self.cycle, type=survey_type).first()
        if survey is not None:
            question_msgs = Question.objects.filter(
                survey__cycle=self.cycle
            ).values_list("id", "title__english")
            question_msgs_dict = {v: k for k, v in question_msgs}
            question_list = []
            for question in data:
                options = []
                if question["options"] and type(question["options"]) == str:
                    options = question["options"].split(";;")

                if question["category"] not in category_dict:
                    category = QuestionCategory.objects.create(
                        title=question["category"]
                    )
                    category = category.id
                else:
                    category = category_dict[question["category"]]

                if question["question"] not in question_msgs_dict:
                    question_list.append(
                        Question(
                            title_id=unique_msgs_dict[question["question"]],
                            type=question["type"],
                            required=question["required"],
                            survey=survey,
                            category_id=category,
                            comment_request_msg_id=unique_msgs_dict[
                                question["comment_msg"]
                            ],
                            option={"english": options},
                        )
                    )
            Question.objects.bulk_create(question_list)

    def start(self):
        data = self.excel_to_dict("survey")
        self.import_survey(data)
        sheets = [
            "audit_timeline_survey",
            "audit_quality_survey",
            "field",
            "partner",
            "auditor",
        ]
        for sheet in sheets:
            data = self.excel_to_dict(sheet)
            survey_type = sheet
            self.import_questions_and_options(data, survey_type)


@background
def notify_teamcomposition_profile_update(year):

    teamcomposition_user_qs = TeamCompositionUser.objects.filter(
        team_composition__cycle__year=year, agreement_member__reviewer=True
    )

    for teamcomposition_user in teamcomposition_user_qs:
        if teamcomposition_user.is_profile_completed == False:
            send_notification(
                notification_type="remind_teamcomposition_auditor",
                obj=teamcomposition_user,
                users=[teamcomposition_user.agreement_member.user],
                context={"year": year, "subject": "Team Composition Notification"},
            )
            try:
                with transaction.atomic():
                    user = NotifiedUser.objects.select_for_update().filter(
                        notification__name="Team Composition Notification",
                        sent_as_email=False,
                    )
                    send_notification_to_users(user)
            except Exception as e:
                print("Error while sending team_composition_notification ", e)


@background
def notify_auditor_survey(year, audit_firm_id):

    users = (
        Agreement.objects.filter(
            cycle__year=year,
            audit_firm_id=audit_firm_id,
            focal_points__user_type="auditing",
        )
        .distinct("focal_points__user")
        .values_list("focal_points__user", flat=True)
    )

    audit_member = AuditMember.objects.filter(user_id__in=users, role__role="ADMIN")

    if audit_member.exists():
        context = {
            "year": year,
            "legal_name": AuditAgency.objects.get(id=audit_firm_id).legal_name,
            "subject": "Auditor Notification for Survey",
        }

        send_notification(
            notification_type="remind_auditor_for_survey",
            obj=AgreementSurvey.objects.filter(
                agreement__audit_firm_id=audit_firm_id, agreement__cycle__year=year
            ).first(),
            users=audit_member,
            context=context,
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Auditor Notification for Survey",
                    sent_as_email=False,
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_auditor_for_survey ", e)


@background
def notify_KPI2_auditor_final(year, audit_firm_id):

    users = (
        Agreement.objects.filter(
            cycle__year=year,
            audit_firm_id=audit_firm_id,
            focal_points__user_type="auditing",
        )
        .distinct("focal_points__user")
        .values_list("focal_points__user", flat=True)
    )

    audit_member_ids = AuditMember.objects.filter(
        user_id__in=users, role__role="ADMIN"
    ).values_list("user_id", flat=True)

    if audit_member_ids.exists():
        audit_member_ids = list(audit_member_ids)
        context = {
            "year": year,
            "legal_name": AuditAgency.objects.get(id=audit_firm_id).legal_name,
            "subject": "Reminder for the Audit Quality",
        }

        send_notification(
            notification_type="remind_KPI2_auditor_final",
            obj=FinalReport.objects.filter(
                audit_quality__audit_agency_id=audit_firm_id,
                audit_quality__cycle__year=year,
            ).first(),
            users=User.objects.filter(id__in=audit_member_ids),
            context=context,
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Reminder for the Audit Quality",
                    sent_as_email=False,
                )

                send_notification_to_users(user)

        except Exception as e:
            print("Error while sending remind_KPI2_auditor_final ", e)


@background
def notify_KPI2_auditor_summary(year, audit_firm_id):

    users = (
        Agreement.objects.filter(
            cycle__year=year,
            audit_firm_id=audit_firm_id,
            focal_points__user_type="auditing",
        )
        .distinct("focal_points__user")
        .values_list("focal_points__user", flat=True)
    )

    audit_member_ids = AuditMember.objects.filter(
        user_id__in=users, role__role="ADMIN"
    ).values_list("user_id", flat=True)

    if audit_member_ids.exists():
        audit_member_ids = list(audit_member_ids)
        context = {
            "year": year,
            "legal_name": AuditAgency.objects.get(id=audit_firm_id).legal_name,
            "subject": "Reminder for the Audit Quality Summary",
        }

        send_notification(
            notification_type="remind_KPI2_auditor_summary",
            obj=SummaryReport.objects.filter(
                audit_quality__audit_agency_id=audit_firm_id,
                audit_quality__cycle__year=year,
            ).first(),
            users=User.objects.filter(id__in=audit_member_ids),
            context=context,
        )

        try:
            with transaction.atomic():
                user = NotifiedUser.objects.select_for_update().filter(
                    notification__name="Reminder for the Audit Quality Summary",
                    sent_as_email=False,
                )
                send_notification_to_users(user)
        except Exception as e:
            print("Error while sending remind_KPI2_auditor_summary ", e)


def import_survey(wb, year):

    sheet = wb.sheet_by_name("survey")
    for i in range(1, sheet.nrows):

        en_survery = sheet.cell_value(i, 0)
        fr_survey = sheet.cell_value(i, 1)
        es_survery = sheet.cell_value(i, 2)
        survey_type = sheet.cell_value(i, 3)

        cycle = Cycle.objects.filter(year=year).first()
        if cycle is not None:
            message, _ = Message.objects.get_or_create(
                english=en_survery, french=fr_survey, spanish=es_survery
            )
            Survey.objects.get_or_create(title=message, type=survey_type, cycle=cycle)


def import_agreement_survey(year, survey_type):
    agreements = Agreement.objects.filter(cycle__year=year)
    queryset = filter_project_selection(agreements, year, "fin", "included")
    if survey_type == SurveyTypeEnum.KPI1.value:
        surveys = Survey.objects.filter(
            cycle__year=year, type=SurveyTypeEnum.KPI1.value
        )
    elif survey_type == SurveyTypeEnum.KPI2.value:
        queryset = queryset.filter(kpi2_sampled=True)
        surveys = Survey.objects.filter(
            cycle__year=year, type=SurveyTypeEnum.KPI2.value
        )
    else:
        KPI_3_4_SURVEY = [
            SurveyTypeEnum.FIELD.value,
            SurveyTypeEnum.PARTNER.value,
            SurveyTypeEnum.AUDITOR.value,
        ]
        surveys = Survey.objects.filter(cycle__year=year, type__in=KPI_3_4_SURVEY)

    filtered_agreements = queryset.values_list("id", flat=True)
    agreement_surveys = []
    for survey in surveys:
        for agreement_id in filtered_agreements:
            agreement_surveys.append(
                AgreementSurvey(
                    agreement_id=agreement_id,
                    survey=survey,
                    placeholders={"year": year},
                )
            )
    AgreementSurvey.objects.bulk_create(agreement_surveys)
    return filtered_agreements


def survey_import_command(year, file_id):
    survey_import = SurveyImport(year, file_id)
    survey_import.start()


@background
def start_agreement_survey(year):
    call_command("survey_notification", year=year)
