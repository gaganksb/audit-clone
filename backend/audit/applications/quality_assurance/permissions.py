from rest_framework.permissions import BasePermission
from project.models import Agreement
from quality_assurance.models import AgreementSurvey, Survey
from common.enums import UserTypeEnum


class SurveyResponsePermission(BasePermission):

    """
    To ensure users are allowed to fill their respective surveys
    """

    def has_permission(self, request, view):
        kwargs = view.kwargs
        membership = request.user.membership
        agreement_survey = view.queryset.filter(id=kwargs["pk"]).first()
        if agreement_survey is not None:
            focal_points = agreement_survey.agreement.focal_points.all()
            hq_surveys = ["audit_timeline_survey", "audit_quality_survey"]
            if membership["type"] == UserTypeEnum.HQ.name and agreement_survey.survey.type in hq_surveys:
                return True
            elif membership["type"] == UserTypeEnum.FO.name:
                is_focal = focal_points.filter(
                    user_type="unhcr", user=request.user
                ).exists()
                is_field_survey = agreement_survey.survey.type == "field"
                return all([is_focal, is_field_survey])
            elif membership["type"] == UserTypeEnum.PARTNER.name:
                is_focal = focal_points.filter(
                    user_type="partner", user=request.user
                ).exists()
                is_partner_survey = agreement_survey.survey.type == "partner"
                return all([is_focal, is_partner_survey])
            elif membership["type"] == UserTypeEnum.AUDITOR.name:
                is_focal = focal_points.filter(
                    user_type="auditing", user=request.user
                ).exists()
                is_audit_survey = agreement_survey.survey.type in ["audit", "auditor"]
                return all([is_focal, is_audit_survey])
        return False
