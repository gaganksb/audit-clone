from project.models import Agreement
from common.helpers import filter_project_selection
from quality_assurance.models import Survey
from quality_assurance.enums import SurveyTypeEnum


def check_audit_assignment_status(year, bypass=False):
    """
    Bypass can be true for HQ approver this will unlock other parts even if the audit work is not done
    """

    if bypass:
        return {"audit_assignment_completed": True}

    queryset = Agreement.objects.filter(cycle__year=year)
    agreements = filter_project_selection(queryset, year, "fin", "included")
    agreement_with_no_agency = agreements.filter(audit_firm__isnull=True)
    return agreement_with_no_agency.exists()


def check_audit_work(year, bypass=False):
    """
    Bypass can be true for HQ approver this will unlock other parts even if the audit work is not done
    """

    if bypass:
        return {"audit_work_completed": True}

    queryset = Agreement.objects.filter(cycle__year=year)
    agreements = filter_project_selection(queryset, year, "fin", "included")

    pending_icqs = agreements.filter(icq_report__isnull=True).values(
        "business_unit__code", "number"
    )
    pending_icqs_count = (
        agreements.filter(icq_report__isnull=True)
        .values("business_unit__code", "number")
        .count()
    )

    pending_mgmt_letters = agreements.filter(mgmt_letter__isnull=True).values(
        "business_unit__code", "number"
    )
    pending_mgmt_letters_count = (
        agreements.filter(mgmt_letter__isnull=True)
        .values("business_unit__code", "number")
        .count()
    )

    pending_audit_reports = agreements.filter(audit_report__isnull=True).values(
        "business_unit__code", "number"
    )
    pending_audit_reports_count = (
        agreements.filter(audit_report__isnull=True)
        .values("business_unit__code", "number")
        .count()
    )

    return {
        "audit_work_completed": not all(
            [
                pending_audit_reports_count,
                pending_mgmt_letters_count,
                pending_icqs_count,
            ]
        ),
        "icqs": {
            "pending_icqs": pending_icqs,
            "pending_icqs_count": pending_icqs_count,
        },
        "mgmt_letters": {
            "pending_icqs": pending_mgmt_letters,
            "pending_icqs_count": pending_mgmt_letters_count,
        },
        "audit_reports": {
            "pending_icqs": pending_audit_reports,
            "pending_icqs_count": pending_audit_reports_count,
        },
    }


def check_survey_uploaded(year):
    response = {"all_uploaded": True}
    for i in SurveyTypeEnum.__members__:
        survey_type = SurveyTypeEnum.__members__[i].value
        survey_type_qs = Survey.objects.filter(cycle__year=year, type=survey_type)
        if survey_type_qs.exists():
            response.update({"{}".format(survey_type): True})
        else:
            response.update({"{}".format(survey_type): False, "all_uploaded": False})
    return response
