from decouple import config

DEFAULT_FROM_EMAIL = (
    "UNHCR Integrity and Assurance Module <noreply@unpartnerportal.org>"
)

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = config("AWS_SMTP_REGION_ENDPOINT")
EMAIL_PORT = config("EMAIL_PORT", 587)
EMAIL_HOST_USER = config("AWS_SES_ACCESS_KEY_ID", None)
EMAIL_HOST_PASSWORD = config("AWS_SES_SECRET_ACCESS_KEY", None)
EMAIL_USE_TLS = True
