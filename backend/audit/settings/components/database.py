from decouple import config

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": config("DB_NAME"),
        "USER": config("DB_USERNAME"),
        "PASSWORD": config("DB_PASSWORD"),
        "HOST": config("DB_HOSTNAME"),
        "PORT": 5432,
    }
}

POSTGRES_SSL_MODE = config("POSTGRES_SSL", default="off")
if POSTGRES_SSL_MODE == "on":
    DATABASES["default"].update({"OPTIONS": {"sslmode": "require"}})
