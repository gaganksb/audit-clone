from audit.settings.components.common import *
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration


sentry_sdk.init(
    dsn=config("SENTRY_SDK"), integrations=[DjangoIntegration()], send_default_pii=True
)

# Static resources related. See documentation at: http://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/
BASE_DIR = "/opt/python/current/app/audit"
STATIC_ROOT = os.path.join(BASE_DIR, "..", "static_cdn", "static")

# Django compress settings
COMPRESS_ENABLED = True
COMPRESS_CSS_HASHING_METHOD = "content"
COMPRESS_CSS_FILTERS = [
    "compressor.filters.css_default.CssAbsoluteFilter",
    "compressor.filters.cssmin.CSSMinFilter",
]
HTML_MINIFY = True
KEEP_COMMENTS_ON_MINIFYING = True
COMPRESS_STORAGE = "audit.settings.storage_backends.CachedS3Boto3Storage"

# Static files
STATICFILES_DIRS = [os.path.join(BASE_DIR, "staticfiles")]
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "compressor.finders.CompressorFinder",
]
AWS_DEFAULT_ACL = None
AWS_ACCESS_KEY_ID = config("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = config("AWS_SECRET_ACCESS_KEY")
AWS_STORAGE_BUCKET_NAME = config("AWS_STORAGE_BUCKET_NAME")
AWS_S3_CUSTOM_DOMAIN = config("AWS_CDN")
AWS_IS_GZIPPED = True
AWS_S3_OBJECT_PARAMETERS = {
    "CacheControl": "max-age=86400",
}
AWS_STATIC_LOCATION = "static"
STATICFILES_STORAGE = "audit.settings.storage_backends.StaticStorage"
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_STATIC_LOCATION)
COMPRESS_URL = STATIC_URL

# Public media files
AWS_PUBLIC_MEDIA_LOCATION = "media/public"
DEFAULT_FILE_STORAGE = "audit.settings.storage_backends.PublicMediaStorage"

# Private media files
AWS_PRIVATE_MEDIA_LOCATION = "media/private"
PRIVATE_FILE_STORAGE = "audit.settings.storage_backends.PrivateMediaStorage"

# Templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.media",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.static",
                "django.template.context_processors.request",
            ],
        },
    },
]

# Debug tools
DEBUG = False
IS_STAGING = False

extend_list_avoid_repeats(
    INSTALLED_APPS,
    [
        "rest_framework_swagger",
    ],
)

# http to https redirection
CORS_REPLACE_HTTPS_REFERER = True
HOST_SCHEME = "https://"
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_SECONDS = 1000000
SECURE_FRAME_DENY = True

SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = "DENY"
