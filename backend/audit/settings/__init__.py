from split_settings.tools import include
from decouple import config

ENV = config("ENV")

base_settings = [
    "components/common.py",  # standard django settings
    "components/email.py",
    "components/database.py",
    # Select the right env:
    "environments/%s.py" % ENV,
]

# Include settings:
include(*base_settings)
