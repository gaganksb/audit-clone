from django.http import HttpResponse
from django.template import loader
from django.conf import settings


def index(request):
    template = loader.get_template("react.html")
    context = {
        "base_template": "base_debug.html",  # if settings.IS_STAGING is True else 'base.html',
    }
    return HttpResponse(template.render(context, request))
