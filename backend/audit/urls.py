from django.contrib import admin
from django.conf.urls import url, include
from django.conf import settings
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from decouple import config
from django.template import *
from drf_yasg import openapi
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from .views import index

schema_view = get_schema_view(
    openapi.Info(
        title="Audit API Doc",
        default_version="v1",
        description="Rest API documentation of the UAM Portal",
        contact=openapi.Contact(email="servicedeskbrindisi@unicc.org"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    patterns=[
        url(
            r"^api/accounts/",
            include(("account.urls", "account"), namespace="accounts"),
        ),
        url(
            r"^api/audits/", include(("auditing.urls", "auditing"), namespace="audits")
        ),
        url(r"^api/fields/", include(("field.urls", "field"), namespace="fields")),
        url(
            r"^api/projects/",
            include(("project.urls", "project"), namespace="projects"),
        ),
        url(r"^api/unhcr/", include(("unhcr.urls", "unhcr"), namespace="unhcr")),
        url(r"^api/common/", include(("common.urls", "common"), namespace="common")),
        url(r"^api/social/", include("social_django.urls", namespace="social")),
        url(r"^api/rest-auth/", include("rest_auth.urls")),
        url(
            r"^api/notification/",
            include(("notify.urls", "notify"), namespace="notify"),
        ),
        url(r"^api/icq/", include(("icq.urls", "icq"), namespace="icq")),
    ],
)

ENV = config("ENV")


html = """<html>{{ request.get_full_path }}</html>"""


def test(request):
    from django.http import HttpResponse
    from django.shortcuts import render

    context_instance = RequestContext(request)
    # print(request.build_absolute_uri())
    # print("------------------------------------")
    print(request.get_full_path)
    # print(request._get_full_path
    return HttpResponse(request.get_full_path())


urlpatterns = [
    url(r"^api/doc/$", schema_view.with_ui("redoc"), name="schema-redoc"),
    url(r"^api/admin/", admin.site.urls),
    url(r"^api/accounts/", include(("account.urls", "account"), namespace="accounts")),
    url(r"^api/audits/", include(("auditing.urls", "auditing"), namespace="audits")),
    url(r"^api/fields/", include(("field.urls", "field"), namespace="fields")),
    url(r"^api/partners/", include(("partner.urls", "partner"), namespace="partners")),
    url(r"^api/projects/", include(("project.urls", "project"), namespace="projects")),
    url(r"^api/unhcr/", include(("unhcr.urls", "unhcr"), namespace="unhcr")),
    url(r"^api/common/", include(("common.urls", "common"), namespace="common")),
    url(r"^api/social/", include("social_django.urls", namespace="social")),
    url(
        r"^api/documents/",
        include(("documents.urls", "documents"), namespace="documents"),
    ),
    url(r"^api/notification/", include(("notify.urls", "notify"), namespace="notify")),
    url(r"^api/test/", test, name="test"),
    url(r"^api/rest-auth/", include("rest_auth.urls")),
    url(r"^api/icq/", include(("icq.urls", "icq"), namespace="icq")),
    url(
        r"^api/quality-assurance/",
        include(
            ("quality_assurance.urls", "quality_assurance"),
            namespace="quality-assurance",
        ),
    ),
    url(
        r"^api/dashboard/",
        include(("dashboard.urls", "dashboard"), namespace="dashboard"),
    ),
]

if settings.IS_DEV or settings.IS_STAGING:
    import debug_toolbar

    urlpatterns += [
        url(r"^api/__debug__/", include(debug_toolbar.urls)),
    ]

# staticfiles
urlpatterns += staticfiles_urlpatterns()

# react
urlpatterns += [
    url("", index),
]

if ENV == "local":
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
