import os
from openpyxl import Workbook, load_workbook
from openpyxl.utils import coordinate_from_string, column_index_from_string
from openpyxl.styles import Font, Alignment, PatternFill, Side, Border
from django.conf import settings

EXCEL_EXTEND = "xlsx"
# Table header
WhiteFill = PatternFill(start_color="FFFFFF", end_color="FFFFFF", fill_type="solid")
table_font = Font(color="000000", bold=True)
table_align = Alignment(horizontal="left", vertical="center", wrap_text=True)
table_fill = PatternFill(fgColor="8a2be2", fill_type="solid")
table_border = Border(
    left=Side(style="thin"),
    right=Side(style="thin"),
    top=Side(style="thin"),
    bottom=Side(style="thin"),
)


class ManageExcelFile:
    def __init__(self, name, **kwargs):
        # if EXCEL_EXTEND not in name:
        #     self.file_name = "{}.{}".format(name, EXCEL_EXTEND)
        # else:
        self.file_name = name

        self.short_path = os.path.join("uploads", self.file_name)
        self.full_path = os.path.join(settings.DATA_VOLUME, "uploads", self.file_name)
        self.work_book = None

    def load_workbook(self):
        self.work_book = load_workbook(self.file_name)

    def get_sheet(self):
        sheet = self.work_book.active
        return sheet

    def add_new_sheet(self, name, **kwargs):
        """Add new sheet with given name to work book."""
        if kwargs.get("many"):
            new_sheet = self.work_book.create_sheet(name)
        else:
            new_sheet = self.work_book.active
            new_sheet.title = name

        # self.save_file()
        return new_sheet

    def load_work_sheet(self, name):
        try:
            sheet = self.work_book.get_sheet_by_name(name)
        except:
            sheet = None

        return sheet

    def get_file_path(self):
        self.work_book.save(self.full_path)
        return self.short_path

    def save_file(self):
        if self.work_book is None:
            return

        try:
            self.work_book.save(self.full_path)
        except Exception as e:
            pass

    @staticmethod
    def styled_cell(work_cell, **kwargs):
        """Apply styles(Font, Alignment, Fill, Border) for a cell."""
        # Get format from kwargs
        font = kwargs.get("font")
        alignment = kwargs.get("alignment")
        pattern_fill = kwargs.get("patternFill")
        border = kwargs.get("border")

        # Apply format for cell
        if font is not None:
            work_cell.font = font
        if alignment is not None:
            work_cell.alignment = alignment
        if pattern_fill is not None:
            work_cell.fill = pattern_fill
        if border is not None:
            work_cell.border = border

    @staticmethod
    def get_cells_from_range(worksheet, cell_range):
        start_cell, end_cell = cell_range.split(":")

        start_coord = coordinate_from_string(start_cell)
        start_row = start_coord[1]
        start_column = column_index_from_string(start_coord[0])

        end_coord = coordinate_from_string(end_cell)
        end_row = end_coord[1]
        end_column = column_index_from_string(end_coord[0])

        cell_list = []
        for row in range(start_row, end_row + 1):
            for column in range(start_column, end_column + 1):
                cell_list.append(worksheet.cell(row=row, column=column))
        return cell_list
