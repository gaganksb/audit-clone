from __future__ import unicode_literals
from django.db.models import Q
from django.apps import apps
import importlib
from copy import deepcopy

TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
TIME_FORMAT_FOR_MAIL = "%d/%m/%Y"


class NotificationHandler:
    def __init__(self, **kwargs):
        self.template_model = apps.get_model("notification", "Template")
        self.notification_model = apps.get_model("notification", "Notification")
        if kwargs:
            self.updated_instance = kwargs.get("instance", {})
            self.model = kwargs.get("model", None)
            self.updated_model = kwargs.get("updated_model", "")
            instance = kwargs.get("old_instance", "")
            if instance:
                self.instance = instance.__dict__
            else:
                self.instance = {key: None for key in self.updated_instance.keys()}
            self.master_mapping = self.get_master_mapping()
            if self.master_mapping:
                self.map_obj()

    # Get master_mapping from models file
    # Make sure that there is a master_mapping in each Model models.py file
    def get_master_mapping(self):
        s = str(self.model).rpartition(".")[0][8:]
        s = importlib.import_module(s)
        if hasattr(s, "master_mapping"):
            return s.master_mapping
        return {}

    def map_obj(self):
        for key, value in self.master_mapping.items():
            if isinstance(value, str):
                k = self.master_mapping[key]
                if self.updated_instance.get(key, None):
                    self.updated_instance[k] = self.updated_instance.get(key, None)
                if self.instance.get(key, None):
                    self.instance[k] = self.instance.get(key, None)

    # Filter out all templates that can be used to send email based on the
    # updated data
    def send_notification(self, stop_sending_email=False, specified_mail_template=[]):
        if stop_sending_email:
            return
        if not specified_mail_template:
            all_templates = self.template_model.objects.all()
            template_query = all_templates.filter(
                Q(apply_for_model=self.updated_model) & Q(active=True)
            )
        else:
            template_query = self.template_model.objects.filter(
                Q(master_key__in=specified_mail_template) & Q(active=True)
            )

        for template in template_query:
            is_condition = template.is_condition
            field_name = template.apply_for_field
            current_value = self.instance.get(field_name, "")
            updated_value = self.updated_instance.get(field_name, "")

            if is_condition:
                comparison = template.values_for_applying
                if not comparison:
                    continue
                if comparison == "<":
                    is_ok = updated_value < current_value
                elif comparison == "<=":
                    is_ok = updated_value <= current_value
                elif comparison == "=":
                    is_ok = updated_value == current_value
                elif comparison == ">=":
                    is_ok = updated_value >= current_value
                elif comparison == ">":
                    is_ok = updated_value > current_value
                else:
                    is_ok = updated_value != current_value
            else:
                # cast value to correct type
                check_value = (
                    apps.get_model(template.apply_for_model.replace("_", "."))
                    ._meta.get_field(template.apply_for_field)
                    .to_python(template.values_for_applying)
                )
                expected_value = check_value
                updated_value = self.updated_instance.get(field_name, "")
                if expected_value == updated_value and current_value != updated_value:
                    is_ok = True
                else:
                    is_ok = False
            if not is_ok:
                continue

            notification = self.notification_model()
            notification.from_email = template.from_email
            notification.subject = template.name
            user_model = apps.get_model("account", "User")
            notification.uid = user_model.objects.first()
            notification.status = "draft"

            notification.template = template
            notification.model_id = self.updated_instance.get("id", -1)

            notification.save()
            notification.send()

    def set_instance(self, **kwargs):
        instance = kwargs.get("instance")
        old_instance = type(instance).objects.filter(pk=instance.id).first()
        new_kwargs = deepcopy(kwargs)
        new_kwargs.update({"old_instance": old_instance})
        new_kwargs["model"] = type(instance)
        self.set_notification(**new_kwargs)

    def set_notification(self, **kwargs):
        instance = kwargs.get("instance")
        kwargs["instance"] = instance.__dict__
        kwargs["updated_model"] = "{}_{}".format(
            instance._meta.app_label, instance._meta.model_name
        )
        stop_sending = kwargs.get("stop_sending_notification", False)
        self.__class__(**kwargs).send_notification(stop_sending)
