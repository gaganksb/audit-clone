from django.db.models import Q
from django.core.exceptions import FieldError
from utils.rest.viewsets import CustomPagination
from dateutil.parser import parse
import datetime
import re


def search(params={}, **kwargs):
    # todo: [BETA] Need more testing
    params = params or kwargs.get("query_params", {})
    pagination_class = kwargs.get("pagination_class", None) or CustomPagination
    mapping = kwargs.get("mapping", {})
    model = kwargs.get("model", None)
    custom_queryset = kwargs.get("queryset", None)

    fields = [x.name for x in model._meta.get_fields()]
    s_mapping = {}
    for field in fields:
        s_mapping.update({field: field})
    s_mapping.update(**mapping)

    try:
        master_mapping = model._meta.app_config.models_module.master_mapping
    except AttributeError:
        master_mapping = {}
    for key, value in master_mapping.items():
        s_mapping.update({value: key})

    page_size = params.get("page_size", 10)
    page = params.get("page", 1)
    order_by = params.get("sort_by", "id")

    pagination_class.status_param = "status"
    pagination_class.page_size = page_size
    pagination_class.page = page

    list_or = []
    list_and = {}
    for key, value in params.items():
        if value.startswith("~"):
            value = value[1:]
            start_with = "~"
        else:
            start_with = ""

        if value.startswith("[") and value.endswith("]"):
            value = [x.strip(",").strip() for x in value[1:-1].split(",")]
        elif re.search(r"[\,]+", value):
            value = [x.strip(",").strip() for x in value.split(",")]

        if key in s_mapping.keys():  # Map key for searching
            if value == "true":
                value = True
            elif value == "false":
                value = False

            filter_string = s_mapping[key]
            if isinstance(value, str):
                if (
                    not key.endswith("id")
                    and not key.endswith("lte")
                    and not key.endswith("gte")
                ):
                    filter_string += "__icontains"
            elif isinstance(value, list):
                filter_string += "__in"

            if start_with:
                list_or.append(
                    {
                        filter_string: value,
                    }
                )
            else:
                list_and.update(
                    {
                        filter_string: value,
                    }
                )
        if key.endswith("gte") or key.endswith("lte"):
            match_date_time = re.search(r"\d+-\d+-\d+", str(value))
            if match_date_time:
                if (
                    isinstance(parse(value), datetime.date) is True
                    or isinstance(parse(value), datetime.datetime) is True
                ):
                    value = parse(value)
            else:
                match_num = re.search("^[\d\.]+$", value)
                if match_num:
                    value = float(value)

            filter_string = s_mapping[key]
            if start_with:
                list_or.append(
                    {
                        filter_string: value,
                    }
                )
            else:
                list_and.update(
                    {
                        filter_string: value,
                    }
                )
        if key == "sort_by":
            default_sort_key = "created_date"
            if value.startswith("-"):
                order_by = "{}{}".format(
                    "-", s_mapping.get(value[1:]) or default_sort_key
                )
            else:
                order_by = s_mapping.get(value) or default_sort_key

    queryset = custom_queryset if custom_queryset is not None else model.objects
    q_object = Q()
    for x in list_or:
        q_object.add(Q(**x), Q.OR)
    try:
        queryset = queryset.filter(q_object)
        queryset = queryset.filter(**list_and)
        queryset = queryset.order_by(order_by)
    except FieldError:
        pagination_class.queryset = queryset.none()
        return queryset.none()

    pagination_class.queryset = queryset

    return queryset
