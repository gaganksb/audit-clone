from django.db import connection
from collections import defaultdict
from heapq import *


def get_all_fk_in_db(db_name, use_auth_user_in_map=False):
    # get all foreign key and reversed column in specific database
    # output will be performed as {keys: values}
    #   - key: foreign key column name.
    #   - value: reversed id of foreign key.
    # cursor = connections[db_name].cursor()
    query = """
    select
      kcu.table_name || '.' || string_agg(kcu.column_name, ', '),
      rel_tco.table_name || '.' || 'id'
    
    from information_schema.table_constraints tco
    join information_schema.key_column_usage kcu
              on tco.constraint_schema = kcu.constraint_schema
              and tco.constraint_name = kcu.constraint_name
    join information_schema.referential_constraints rco
              on tco.constraint_schema = rco.constraint_schema
              and tco.constraint_name = rco.constraint_name
    join information_schema.table_constraints rel_tco
              on rco.unique_constraint_schema = rel_tco.constraint_schema
              and rco.unique_constraint_name = rel_tco.constraint_name
    where tco.constraint_type = 'FOREIGN KEY'
    group by kcu.table_schema,
             kcu.table_name,
             rel_tco.table_name,
             rel_tco.table_schema,
             kcu.constraint_name
    order by kcu.table_schema,
             kcu.table_name;
    """

    with connection.cursor() as cursor:
        cursor.execute(query)
        rows = cursor.fetchall()
    output = {}
    for row in rows:
        output.update({row[0]: row[1]})
    return output


def prepare_data_for_djikstra_search(dict_frks):
    # this function will prepare the data for search the shortest path
    # to connect 2 tables.
    # input: is the output of function get_all_fk_in_db()
    # output: standardized data and its inverse
    output = []
    inv_output = []
    for vals in dict_frks:
        model_name = vals.split(".")[0]
        field_name = vals.split(".")[1]
        iv_model_name = dict_frks.get(vals).split(".")[0]
        iv_field_name = dict_frks.get(vals).split(".")[1]
        p_edge = 1
        if "attachment" in model_name or "attachment" in iv_model_name:
            p_edge *= 2
        if "attachment_masterattachment" in [model_name, iv_model_name]:
            p_edge *= 5
        output.append((model_name, iv_model_name, p_edge, {vals: dict_frks.get(vals)}))
        inv_output.append(
            (iv_model_name, model_name, p_edge, {vals: dict_frks.get(vals)})
        )
    return output, inv_output


def get_shortest_path_by_dijkstra(frk_connections, from_table, to_table):
    # get the shortest path of joining 2 tables
    # example:
    # assume table A have FK of table B
    #        table B have FK of table C
    #        table C have FK of table A
    # output from A join with C will be :
    # [(A, B, (road to B)),
    #  (B, C, (road to C))]
    # print ("=====================\n", frk_connections, "\t\t", from_table,
    #        "\t\t", to_table)
    def yeild_path(direction, dir_path):
        if direction:
            dir_path.insert(0, direction[0])
            return yeild_path(direction[1], dir_path)
        else:
            return

    edges = defaultdict(list)
    for le, ri, c, rd in frk_connections:
        edges[le].append((c, ri, rd))  # main road
        edges[ri].append((c, le, rd))  # reverse road
    heap_edges = [(0, from_table, ())]
    seen = set()
    output = ()
    # use djikstra algo with heap to find the shortest path.
    while heap_edges:
        (cost, v1, path) = heappop(heap_edges)
        if v1 not in seen:
            seen.add(v1)
            path = (v1, path)
            if v1 == to_table:
                output = (cost, path)
                break
            for c, v2, rd in edges.get(v1, ()):
                if v2 not in seen:
                    heappush(heap_edges, (cost + c, v2, path))
    # after getting the path, parse the road that it's the join condition.
    lst_peaks = []
    if not output:
        return ()
    yeild_path(output[1], lst_peaks)
    count = 0
    road = []
    for item in lst_peaks[1:]:
        count += 1
        for it in edges.get(lst_peaks[count - 1]):
            if item == it[1]:
                key = list(it[2].keys())[0]
                val = it[2].get(list(it[2].keys())[0])
                road.append((lst_peaks[count - 1], it[1], (key, val)))
                # road.update(it[2])
                break
    # return road
    # format will be an ordered list:
    # list[tuple("tb1", "tb2", tuple("tb1.on_column", "tb2.on_column"))]
    return road


def parse_path_2_queries(selected_fields, from_table, path):
    # make query from path that it's output of searching.
    # selected fields will be used for filtering the output fields
    fields = ""
    if selected_fields and isinstance(selected_fields, list):
        fields = ", ".join(selected_fields)
    # print (selected_fields, from_table, path)
    if path and len(path) >= 1:
        query = "SELECT {0}, {1} FROM {2}\n".format(
            from_table + ".id", fields, from_table
        )
        count = 1
        join = ""
        models = [from_table]
        appending_path = path
        while appending_path:
            # print(appending_path)
            rmv_idx = []
            for idx, p in enumerate(appending_path):
                if p[0] not in models:
                    join += "LEFT JOIN {0} ON ({1}={2})\n".format(
                        p[0], p[2][0], p[2][1]
                    )
                    models.append(p[0])
                elif p[1] not in models:
                    join += "LEFT JOIN {0} ON ({1}={2})\n".format(
                        p[1], p[2][0], p[2][1]
                    )
                    models.append(p[1])
                else:
                    rmv_idx.append(idx)
                    continue
                # appending_path.pop(idx)
                rmv_idx.append(idx)
            appending_path = [
                p for idx, p in enumerate(appending_path) if idx not in rmv_idx
            ]

        if join:
            query += join
            query += "\nWHERE {} in ".format(from_table + ".id")
            query += "{idlist};"  # 10, 23, 12, 24
            return query
    return ""
