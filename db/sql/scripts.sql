-- List of all projects
select 
	bu.code as business_unit,
	prj.number as ppa_id, 
	prj.title, 
	par.number as implementer_number, 
	imp.implementer_type as implementer_type, 
	par.legal_name as implementer_legal_name,
	prj.start_date, 
	prj.end_date, 
	prj.budget, 
	prj.installments_paid, 
	agt.description as agreement_type, 
	pil.description as pillar, 
	ven.code as vendor_code
from project_agreement as prj
	join partner_partner as par on prj.partner_id=par.id
	join partner_implementertype as imp on par.implementer_type_id=imp.id
	join common_businessunit as bu on prj.business_unit_id=bu.id
	join project_agreementtype as agt on prj.agreement_type_id=agt.id
	join project_pillar as pil on prj.pillar_id=pil.id
	join project_vendor as ven on prj.vendor_id=ven.id
;

-- List of all field members
select 
	usr.email,
	ofc.name,
	rol.role
	from field_fieldmember as fld
	join field_fieldoffice as ofc on fld.field_office_id=ofc.id
	join account_user as usr on fld.user_id=usr.id
	join field_role as rol on fld.role_id=rol.id;

-- List of all HQ members
select 
	usr.email,
	rol."role"
	from unhcr_unhcrmember as hq
	join unhcr_role as rol on hq.role_id=rol.id
	join account_user as usr on hq.user_id=usr.id;