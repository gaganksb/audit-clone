
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import R from 'ramda';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import error, * as errorSelector from './reducers/errorReducer';
import success from './reducers/successReducer';
import session from './reducers/session';
import nav from './reducers/nav';
import userData from './reducers/users/userData';
import usersList from './reducers/users/usersList';
import assessmentUsers from './reducers/users/assessmentUser';
import messageList from './reducers/messages/messageList';
import icqList from './reducers/projects/icqReducer/icqList';
import icqDetailList from './reducers/projects/icqReducer/icqDetailedList';
import pqpList from './reducers/projects/pqpReducer/pqpList';
import oiosList from './reducers/projects/oiosReducer/oiosList';
import documentList from './reducers/documents/documentList';
import permissionList from './reducers/users/permissionList';
import bgJobStatus from './reducers/bgTaskReducer';
import projectList from './reducers/projects/projectsReducer/getProjectList';
// import preliminaryList from './reducers/projects/projectsReducer/getPreliminaryList';
import preliminaryStats from './reducers/projects/projectsReducer/statsReducer';
import paList from './reducers/projects/projectsReducer/getPAList';
import paExcludedList from './reducers/projects/projectsReducer/getPAExcludedList';
import ILExcludedList from './reducers/projects/projectsReducer/getILExcludedList';
import rolePermissions from './reducers/users/rolePermissions';
import settings from './reducers/projects/projectsReducer/getSettings';
import projectDetails from './reducers/projects/projectsReducer/getProjectDetails';
import cycle from './reducers/projects/projectsReducer/getCycle';
import latestCycle from './reducers/projects/projectsReducer/getLatestCycle';
import BUListReducer from './reducers/projects/oiosReducer/buList';
import partnerList from './reducers/partners/partnerList';
import partnerDetails from './reducers/partners/getPartnerDetails';
import partnerAssignmentList from './reducers/partners/partnerAssignmentList';
import interimList from './reducers/projects/projectsReducer/getInterimList';
import interimListInProgress from './reducers/projects/projectsReducer/getInterimListInProgress';
import finalList from './reducers/projects/projectsReducer/getFinalList';
import rollbackInterim from './reducers/projects/projectsReducer/rollbackReducer';
import cycleRollback from './reducers/projects/projectsReducer/rollbackCycle';
import assessmentList from './reducers/assessmentReducer/getAssessmentList';
import fieldList from './reducers/fields/fieldList';
import auditList from './reducers/audits/auditList';
import dashboardDetails from './reducers/projects/dashboardReducer';
import finalExcludedList from './reducers/projects/projectsReducer/getFinalExcludedList';
import cycleFO from './reducers/projects/projectsReducer/getCycleFO';
import fieldDashboard from './reducers/fields/fieldDashboard';
import generalConfig from './reducers/generalConfig';
import commentList from './reducers/projects/projectsReducer/getCommentList';
import modifyProject from './reducers/projects/projectsReducer/modifyProject';
import activityList from './reducers/projects/projectsReducer/getActivityList';
import assessmentProgressReducer from './reducers/assessmentReducer/assessmentProgress';
import countries from './reducers/countries';
import projectAssignment from './reducers/audits/projectAssignment/assignmentList';
import fieldMemberListReducer from './reducers/fields/fieldMemberList';
import groupList from './reducers/audits/groups/groupList';
import messages from './reducers/projects/messagesReducer/loadMessage';
import budgetList from './reducers/projects/budgetReducer/getBudgetList';
import auditReport from './reducers/reports/auditReports/getReports';
import managementReport from './reducers/reports/managementReports/getReports';
import reportStatus from './reducers/reports/getReportStatus';
import deadlineDetails from './reducers/assessmentReducer/getDeadline';
import selectionProgressReducer from './reducers/projects/projectsReducer/getSelectionProgress';
import selectionProgressReducerHQ from './reducers/projects/projectsReducer/getSelectionProgressHQ';
import modifyProjectHQ from './reducers/projects/projectsReducer/modifyProjectHQ';
import checkReminder from './reducers/audits/projectAssignment/remindAll';
import downloadCSV from './reducers/projects/projectsReducer/downloadCSVs';
import majorFinding from './reducers/reports/managementReports/getMajorFinding';
import overallFinding from './reducers/reports/managementReports/getOverallRating';
import followUps from './reducers/reports/managementReports/getFollowupRating'
import icqErrors from './reducers/reports/icqQuestionnaire/getICQErrors';
import AuditFirmCountryList from './reducers/qualityAssurance/AuditFirmCountryList';
import SampleAuditFirmCountryList from './reducers/qualityAssurance/SampleAuditFirmCountryList';
import Kpi3Reducer from './reducers/qualityAssurance/Kpi3Reducer';
import Kpi2Reducer from './reducers/qualityAssurance/kpi2Reducer';

const mainReducer = combineReducers({
  assessmentList,
  auditList,
  BUListReducer,
  overallFinding,
  followUps,
  cycle,
  cycleFO,
  downloadCSV,
  error,
  success,
  fieldList,
  finalExcludedList,
  finalList,
  form: formReducer,
  assessmentUsers,
  generalConfig,
  icqList,
  icqDetailList,
  ILExcludedList,
  interimList,
  majorFinding,
  interimListInProgress,
  latestCycle,
  documentList,
  nav,
  countries,
  oiosList,
  paExcludedList,
  paList,
  partnerDetails,
  partnerList,
  pqpList,
  messages,
  permissionList,
  projectList,
  bgJobStatus,
  preliminaryStats,
  projectDetails,
  rolePermissions,
  routing: routerReducer,
  session,
  settings,
  userData,
  usersList,
  dashboardDetails,
  fieldDashboard,
  messageList,
  commentList,
  partnerAssignmentList,
  groupList,
  activityList,
  projectAssignment,
  assessmentProgress: assessmentProgressReducer,
  fieldMemberList: fieldMemberListReducer,
  budgetList,
  auditReport,
  reportStatus,
  deadlineDetails,
  selectionProgress: selectionProgressReducer,
  selectionProgressReducerHQ,
  checkReminder,
  modifyProjectHQ,
  managementReport,
  icqErrors,
  AuditFirmCountryList,
  SampleAuditFirmCountryList,
  Kpi3Reducer,
  Kpi2Reducer,
});

const middleware = [thunk, routerMiddleware(browserHistory)];
let composeEnhancers = compose;
if (process.env.NODE_ENV !== 'production') {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || composeEnhancers;
}

export default createStore(
  mainReducer,
  composeEnhancers(
    applyMiddleware(...middleware),
  ),
);

export const selectAllErrorsMapped = state => errorSelector.selectAllErrorsMapped(state.error);