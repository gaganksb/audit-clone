export const SESSION_STATUS = {
  INITIAL: 'initial',
  CHANGING: 'changing',
  READY: 'ready',
};

export const OFFICE_LABELS = {
  audit: 'Audit Agency',
  field: 'Field Office',
  partner: 'Partner'
};

export const buttonType = {
  submit: 'Submit',
  cancel: 'Cancel',
  action: 'Action'
};

export const filterButtons = {
  search: 'Search',
  reset: 'Reset'
}

export const RadioOptions = [
  {
    value: 'unmodified',
    label: 'UNMODIFIED'
  },
  {
    value: 'qualified',
    label: 'QUALIFIED'
  },
  {
    value: 'adverse',
    label: 'ADVERSE'
  },
  {
    value: 'disclaimer',
    label: 'DISCLAIMER'
  }
];

export const ASSESSMENT_RATINGS = [
  { key: '0', value: 'Select All' },
  { key: '1', value: 'Partner Meets Expectations' },
  { key: '2', value: 'Partner Medium Level - Some risk exposure' },
  { key: '3', value: 'Partner Weak Level - High risk' },
  { key: '4', value: 'No Field Response/Assessment Received' },
  { key: '5', value: 'UN Agencies Agreement Not Implemented - National Fund Raising' }
];

export const USERS = [
  {
    KEY: 'audit',
    NAME: 'Auditor',
    ENUM: 'AUDITOR',
    MODEL: 'auditmember',
    ROLES: [
      { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
      { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
    ],
    OFFICE: {
      label: OFFICE_LABELS.audit,
      type_to_search: 'audit__legal_name'
    }
  },
  {
    KEY: 'field',
    NAME: 'Field Office Member',
    ENUM: 'FO',
    MODEL: 'fieldmember',
    ROLES: [
      { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
      { NAME: 'Approver', ID: 2, ENUM: 'APPROVER' }
    ],
    OFFICE: {
      label: OFFICE_LABELS.field,
      type_to_search: 'field__name'
    }
  },
  {
    KEY: 'partners',
    NAME: 'Partner Member',
    ENUM: 'PARTNER',
    MODEL: 'partnermember',
    ROLES: [
      { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
      { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
    ],
    OFFICE: {
      label: OFFICE_LABELS.partner,
      type_to_search: 'partner__legal_name'
    }
  },
  {
    KEY: 'unhcr',
    NAME: 'HQ Member',
    ENUM: 'HQ',
    MODEL: 'unhcrmember',
    ROLES: [
      { NAME: 'Preparer', ID: 1, ENUM: 'PREPARER' },
      { NAME: 'Reviewer', ID: 2, ENUM: 'REVIEWER' },
      { NAME: 'Approver', ID: 3, ENUM: 'APPROVER' },
    ],
    OFFICE: null
  },
  {
    KEY: 'other',
    NAME: 'Other',
    ENUM: 'OTHER',
    ROLES: null,
    OFFICE: null
  }
];

export const USERS_TYPES = {
  HQ: 'HQ',
  FO: 'FO',
  PARTNER: 'PARTNER',
  AUDITOR: 'AUDITOR'
};

export const USERS_KEY = {
  audit: 'audit',
  field: 'field',
  partners: 'partners',
  unhcr: 'unhcr',
  other: 'other',
}

export const CYCLE_STATUS = {
  PRE01: {
    id: 1,
    code: 'PRE01',
    description: 'In Preparation',
  },
  PRE02: {
    id: 2,
    code: 'PRE02',
    description: 'Under HQ Review',
  },
  PRE03: {
    id: 3,
    code: 'PRE03',
    description: 'Under HQ Approval',
  },
  INT01: {
    id: 4,
    code: 'INT01',
    description: 'Under FO Review',
  },
  INT02: {
    id: 5,
    code: 'INT02',
    description: 'Under FO Approval',
  },
  FIN01: {
    id: 6,
    code: 'FIN01',
    description: 'Waiting Auditor Selection',
  },
  FIN02: {
    id: 7,
    code: 'FIN02',
    description: 'Finalized',
  },
  FIN03: {
    id: 8,
    code: 'FIN03',
    description: 'Auditor Selection Started',
  }
}

export const MEMBERSHIPS = {
  field_office: 'field_office'
}

export const BODY_PARAMS = {
  preliminarylist: 'preliminarylist',
  interimlist: 'interimlist',
  finallist: 'finallist',
}

export const FINANCIAL_FINDINGS_TYPES = [
  { type: 'EXP_NOT_USED', value: 'Expenditure not used for the purpose/objectives of PA' },
  { type: 'NO_SUPP_DOC', value: 'No supporting documentation' },
  { type: 'INSUFF_DOC', value: 'Insufficient supporting documentation (may include insufficient access to information, people or sites)' },
  { type: 'COST_EXC', value: 'Personnel costs exceed the agreed budget and/or amounts paid to ineligible persons' },
  { type: 'MISS_CALC', value: 'Partner Integrity Capacity and Support Costs (PICSC) Miscalculated' },
  { type: 'CUTT_OFF_ERROR', value: 'Cut-off errors: commitments recorded as expenditures and/or expenditure committed after the end of the project implementation period and/or when payments were issued after the liquidation period but the deliverables were not received by the end of the implementation period. ' },
  { type: 'INAPPROP_EXC_RATE', value: 'Inappropriate exchange rate used' },
  { type: 'EXCESS_TRANSFER', value: 'Budgetary transfers made in excess of 30% limit at the output level without UNHCR approval' },
  { type: 'FIN_INFO_NON_RECON', value: 'Financial information not reconciled to GL (when PFR items are not traceable into the GL)' },
  { type: 'NO_PROOF', value: 'No proof of goods/services  received or activities undertaken' },
  { type: 'INCORRECT_VAT', value: 'VAT incorrectly claimed or a VAT valid exemption not utilized' },
  { type: 'OTH_INCOME', value: 'Other Income (e.g. bank interest) not reported or miscalculated' },
  { type: 'DOUBLE_CHARGE', value: 'Expenditure double-funded or double charged or double recorded /reported' },
  { type: 'OTHER_FIN_FINDING', value: 'Other financial findings that did not fall within any of the other headings (please clarify).' },
]

export const SENSE_CHECKS = {
  above_risk: {
    id: 1,
    code: "SC01",
    description:
      "Projects at Risk Threshold 2.6 or above (Before Sense Check) with values >=150,000",
  },
  before_sense_check: {
    id: 2,
    code: "SC02",
    description:
      "Projects Having Adverse or Disclaimer of Opinion between {current_year - 4; current_year - 1} with values >=150000",
  },
  covid: {
    id: 3,
    code: "SC03",
    description:
      "Projects implemented by All New Partner to UNHCR Worldwide with values >=150,000",
  },
  oios_1: {
    id: 4,
    code: "SC04",
    description:
      "Partners not Audited in the last 4 years with values >=150,000",
  },
  k_150: {
    id: 5,
    code: "SC05",
    description:
      "Partners with qualified opinions in {current_year - 1} audit cycle with values >=150000",
  },
  not_audit_1: {
    id: 6,
    code: "SC06",
    description: "Projects with Budget >=5M",
  },
  qualified: {
    id: 7,
    code: "SC07",
    description: "Projects having Budget >=645K (In OIOS Top 20 Countries)",
  },
  M_5: {
    id: 8,
    code: "SC08",
    description:
      "Projects having Budget >=645K (In OIOS Top 20 Countries) - 10 Highest Budget",
  },
  not_audit_2: {
    id: 9,
    code: "SC09",
    description:
      "Projects having Budget >=645K (In OIOS Top 20 Countries) - out of 10 Highest Budget but with recent ICQ <0.70",
  },
  not_audit_3: {
    id: 10,
    code: "SC10",
    description:
      "Partner Not Audited for 2016/17 agreements in 2018-2019 audit cycles with values >=150000",
  },
  covid_postpone: {
    id: 11,
    code: "SC11",
    description:
      "Projects Having Budget <645K, Not in Top 20 Risky Countries with values >=150000",
  },
  manually_excluded: {
    id: 12,
    code: "SC12",
    description:
      "Postponed and cancelled audits not audited so far due to the COVID",
  },
};

export const ACTIVITY_MSG = {
  'PRE01 to PRE02': ['hq_preparer', 'sent the Preliminary List for Review'],
  'PRE02 to PRE03': ['hq_reviewer', 'sent the Preliminary List for Approval'],
  'PRE03 to INT01': ['hq_approver', 'finalized the Preliminary List'],
  'INT01 to INT02': ['fo_reviewer', 'sent the Interim List for Approval'],
  'INT02 to FIN01': ['fo_approver', 'finalized the Interim List'],
  'FIN01 to FIN02': ['hq_reviewer', 'sent the Final List for Approval'],
  'FIN02 to FIN03': ['hq_approver', 'finalized the Final List'],

  'PRE02 to PRE01': ['hq_reviewer', 'rejected the Preliminary List'],
  'PRE03 to PRE02': ['hq_approver', 'rejected the Preliminary List'],
  'INT02 to INT01': ['fo_approver', 'rejected the Interim List'],
  'FIN02 to FIN01': ['hq_approver', 'rejected the Final List'],
  'INT01 to PRE03': ['hq_user', 'rollback Interim List'],
  'FIN01 to INT02': ['hq_user', 'rollback Final List'],
  'INT01 to INT01': ['unknown', 'unknown'],
  "HQ Promoted to FIN01": ['hq_user', 'HQ Promoted to FIN01'],
  "FIN01 to FIN01": ['unknown', 'unknown'],
}

export const LIST_TYPE = {
  preliminary: 'PRE',
  interim: 'INT',
  final: 'FIN'
}

export const icqAttribute = [
  { key: 1, value: 'A1.1' },
  { key: 2, value: 'A1.2' },
  { key: 3, value: 'A1.3' },
  { key: 4, value: 'A1.4' },
  { key: 5, value: 'A2.1' },
  { key: 6, value: 'A2.2' },
  { key: 7, value: 'A2.3' },
  { key: 8, value: 'A2.4' },
  { key: 9, value: 'A2.5' },
  { key: 10, value: 'A2.6' },
  { key: 11, value: 'A3.1' },
  { key: 12, value: 'A3.2' },
  { key: 13, value: 'A4.1' },
  { key: 14, value: 'A5.1' },
  { key: 15, value: 'A6.1' },
  { key: 16, value: 'A6.2' },
  { key: 17, value: 'A6.3' },
  { key: 18, value: 'A7.1' },
  { key: 19, value: 'A8.1' },
  { key: 20, value: 'A8.2' },
  { key: 21, value: 'A8.3' },
  { key: 22, value: 'A9.1' },
  { key: 23, value: 'A9.2' },
  { key: 24, value: 'A10.1' },
  { key: 25, value: 'A10.2' },
  { key: 26, value: 'A11.1' },
  { key: 27, value: 'B1.1' },
  { key: 28, value: 'B1.2' },
  { key: 29, value: 'B1.3' },
  { key: 30, value: 'B1.4' },
  { key: 31, value: 'B1.5' },
  { key: 32, value: 'B1.6' },
  { key: 33, value: 'B2.1' },
  { key: 34, value: 'B3.1' },
  { key: 35, value: 'B3.2' },
  { key: 36, value: 'B4.1' },
  { key: 37, value: 'B5.1' },
  { key: 38, value: 'B6.1' },
  { key: 39, value: 'C1.1' },
  { key: 40, value: 'C2.1' },
  { key: 41, value: 'C3.1' },
  { key: 42, value: 'C4.1' },
  { key: 43, value: 'D1.1' },
  { key: 44, value: 'D1.2' },
  { key: 45, value: 'D2.1' },
  { key: 46, value: 'D2.2' },
  { key: 47, value: 'D2.3' },
  { key: 48, value: 'D3.1' },
  { key: 49, value: 'D4.1' },
  { key: 50, value: 'D5.1' },
  { key: 51, value: 'D5.2' },
  { key: 52, value: 'D6.1' },
  { key: 53, value: 'D7.1' },
  { key: 54, value: 'D8.1' },
  { key: 55, value: 'D9.1' },
  { key: 56, value: 'E1.1' },
  { key: 57, value: 'E1.2' },
  { key: 58, value: 'E2.1' },
  { key: 59, value: 'F1.1' },
  { key: 60, value: 'F1.2' },
  { key: 61, value: 'F2.1' },
  { key: 62, value: 'F2.2' },
  { key: 63, value: 'F2.3' },
  { key: 64, value: 'G1.1' },
  { key: 65, value: 'G1.2' },
  { key: 66, value: 'G1.3' },
  { key: 67, value: 'G1.4' },
  { key: 68, value: 'G1.5' },
  { key: 69, value: 'G2.1' },
  { key: 70, value: 'G2.2' },
  { key: 71, value: 'G2.3' },
  { key: 72, value: 'G3.1' },
  { key: 73, value: 'G3.2' },
  { key: 74, value: 'H1.1' },
  { key: 75, value: 'H1.2' },
  { key: 76, value: 'H2.1' },
  { key: 77, value: 'H2.2' },
  { key: 78, value: 'H3.1' }]

export const Process = {
  A: "Payment and Bank Management",
  B: "Procurement",
  C: "Management of Activities Subcontracted to Sub-Partners",
  D: "Personnel Engagement and Payment",
  E: "Budget",
  F: "Goods and Property",
  G: "General Control Environment",
  H: "Compliance with other terms of the Partnership Agreement"
}
export const PARTNER = 'PARTNER';
export const UNHCR = 'HQ';
export const AUDIT = 'AUDITOR';
export const FIELD = 'FO';
export const OTHER = 'OTHER';

export const ACCOUNT_TYPES = {
  field_members: FIELD,
  partner_members: PARTNER,
  audit_members: AUDIT,
  unhcr_members: UNHCR
}

export const LOGO = 'https://d3ks7tc21a4fkt.cloudfront.net/static/media/logo-unhcr.a1f80b1c.png';
export const LOGO_LOGIN = 'https://d10wjolh60a7gg.cloudfront.net/static/media/logo-login.png';
export const BACKGROUND_IMG = 'https://d3ks7tc21a4fkt.cloudfront.net/static/media/home-bkg-2.565af0fe.jpg';

export const auditNote = 'The Audit should also assess and take into account the possible effect of a prior year modified opinion or significant risks that was raised in UNHCR monitoring/verification reports and has not been properly corrected or resolved that could have accumulative negative effect. \r\n Audit opinions must be one of the following: (a)Unmodified, (b)Qualified, (c)Adverse, or (d)Disclaimer. If the audit opinion is other than "unmodified" the audit report must describe both the nature and amount of the possible effects on the PFR(both qualitative and qualitative as applicable \r\n The Auditor retains the responsibility to issue and present the appropriate independent Audit Report in accordance with the relevant and the most up-to-date IBAs'

export const graphData = [
  {
    name: 'Total Projects', projects: 60,
  },
  {
    name: 'Projects above Risk Threshold', projects: 10,
  },
  {
    name: 'Remaining Projects having Adverse or Disclaimer of Opinion in last 4 years', projects: 6,
  },
  {
    name: 'Remaining Projects implemented by New Partner to UNHCR with budget >= USD 50k', projects: 8,
  },
  {
    name: 'Remaining Partners not Audited Before for their PAs created in 2013 and 2014 and with budget >=50k', projects: 4,
  },
  {
    name: 'Remaining Partners not Audited Before for their PAs created in 2015 and 2016 and with budget >=50k', projects: 2,
  },
  {
    name: 'Remaining Partners not Audited for their PAs created in 2017 with budget >=50,000 and Risk rating >2.7', projects: 5,
  },
  {
    name: 'Remaining Partners with qualified opinions in 2017 audit cycle and budget >=50k', projects: 5,
  },
  {
    name: 'Remaining Projects with Budget >=5M', projects: 2,
  },
  {
    name: 'Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) - 10 Highest Budget', projects: 0,
  },
  {
    name: 'Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) -  out of 10 Highest Budget but with recent ICQ <0.70', projects: 3
  },
  {
    name: 'From/Included in the Interim list as field/HQ requests or category changed', projects: 10
  },
  {
    name: 'PA Manually Included', projects: 5
  },

];

export const MemberTeamType = [
  { code: "Field Work", value: "field_work" },
  { code: "Other", value: "other" },
]

export function getLocalLanguage(local_lang) {
  const langMap = [
    { lang: "english", value: "en-US" },
    { lang: "french", value: "fr-FR" },
    { lang: "spanish", value: "es-ES" },
  ]

  for (let lang in langMap) {
    if (lang.value == local_lang) {
      return lang.lang;
    }
    return "english";
  }
}