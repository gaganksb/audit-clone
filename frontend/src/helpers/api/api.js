import axios from 'axios';
import moment from 'moment-timezone';
import store from '../../store';

const host = '/api';

// Internal help/generic functions
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (`${name}=`)) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

function buildHeaders(authorize = false, extraHeaders = {}) {
  const token = store.getState().session.token;
  const partnerId = store.getState().session.partnerId;
  const officeId = store.getState().session.officeId;
  let headers = {
    Pragma: 'no-cache',
    'Cache-Control': 'no-cache',
    'Client-Timezone-Name': moment.tz.guess(),
  };
  if (authorize && token) headers = { ...headers, Authorization: `token ${token}` };
  if (partnerId) headers = { ...headers, 'Partner-ID': partnerId };
  if (officeId) headers = { ...headers, 'Agency-Office-ID': officeId };

  return { ...headers, ...extraHeaders };
}

function get(uri, params = {}, options) {
  const opt = { method: 'GET', params, headers: buildHeaders() };
  return axios.get(`${host}${uri}`, { ...opt, ...options })
    .then(response => response.data);
}

function post(uri, body = {}) {
  const options = {
    headers: buildHeaders(false, { 'X-CSRFToken': getCookie('csrftoken') }),
  };
  return axios.post(`${host}${uri}`, body, options)
    .then(response => response.data);
}

function authorizedGet({ uri, params = {}, options }) {
  const opt = {
    params,
    headers: buildHeaders(true),
  };
  return axios.get(`${host}${uri}`, { ...options, ...opt })
    .then(response => response.data);
}

function authorizedPost({ uri, params, body = {} }) {
  const options = {
    params,
    headers: buildHeaders(true, { 'X-CSRFToken': getCookie('csrftoken') }),
  };

  return axios.post(`${host}${uri}`, body, options)
    .then(response => response.data);
}

function authorizedDelete({ uri, params }) {
  const options = {
    params,
    headers: buildHeaders(true, { 'X-CSRFToken': getCookie('csrftoken') }),
  };
  return axios.delete(`${host}${uri}`, options)
    .then(response => response.data);
}

function authorizedPatch({ uri, params, body = {} }) {
  const options = {
    params,
    headers: buildHeaders(true, { 'X-CSRFToken': getCookie('csrftoken') }),
  };
  return axios.patch(`${host}${uri}`, body, options)
    .then(response => response.data);
}

function authorizedPut({ uri, params, body = {} }) {
  const options = {
    params,
    headers: buildHeaders(true, { 'X-CSRFToken': getCookie('csrftoken') }),
  };
  return axios.put(`${host}${uri}`, body, options)
    .then(response => response.data);
}

function authorizedPostUpload({ uri, body = {}, params }) {
  const options = {
    params,
    headers: buildHeaders(true, {
      'content-type': 'multipart/form-data',
      'X-CSRFToken': getCookie('csrftoken'),
    }),
  };
  return axios.post(`${host}${uri}`, body, options)
    .then(response => response.data);
}

function authorizedPatchUpload({ uri, body = {}, params }) {
  const options = {
    params,
    headers: buildHeaders(true, {
      'content-type': 'multipart/form-data',
      'X-CSRFToken': getCookie('csrftoken'),
    }),
  };
  return axios.patch(`${host}${uri}`, body, options)
    .then(response => response.data);
}

export function authorizedFileDownload({ uri }) {
  const opt = {
    headers: buildHeaders(true),
    responseType: 'blob'
  };
  return axios.get(`${host}${uri}`, { ...opt }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    const filename_match = response.headers['content-disposition'].match(/filename="(.+)"/);
    if (filename_match) {
      link.setAttribute('download', filename_match[1]);
    }
    else {
      link.setAttribute('download', "export.pdf");
    }
    document.body.appendChild(link);
    link.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(link);
  });
}

// Accounts
export function postRegistration(body) {
  return post('/accounts/registration', body);
}

export function login(body) {
  return post('/rest-auth/login/', body);
}

export function logout() {
  return post('/rest-auth/logout/');
}

export function getUserData() {
  return authorizedGet({ uri: '/accounts/me/' });
}

export function getGlobalConfig() {
  return get('/common/general/');
}

export const passwordReset = post.bind(null, '/rest-auth/password/reset/');

export const passwordResetConfirm = post.bind(null, '/rest-auth/password/reset/confirm/');


// ID portal
export function getUsersList(params, options) {
  return authorizedGet({ uri: '/accounts/', params, options });
}

export function postNewUser(body) {
  return authorizedPost({ uri: '/accounts/', body });
}

export function deleteUser(id) {
  return authorizedDelete({ uri: `/accounts/${id}/` });
}

export function patchOtherUser(id, body) {
  return authorizedPatch({ uri: `/accounts/${id}/`, body });
}

export function patchAuditUser(id, body) {
  return authorizedPatch({ uri: `/audits/member/${id}/`, body });
}

export function patchFO(id, body) {
  return authorizedPatch({ uri: `/fields/member/${id}/`, body });
}

export function getFieldMemberList(id, params) {
  return authorizedGet({ uri: `/fields/${id}/members/`, params });
}

export function patchUNHCR(id, body) {
  return authorizedPatch({ uri: `/unhcr/member/${id}/`, body });
}

export function patchPartnerUser(id, body) {
  return authorizedPatch({ uri: `/partners/member/${id}/`, body });
}

export function getPermissionList(params, options) {
  return authorizedGet({ uri: `/common/permission/`, params, options });
}

export function getRolePermissions(user_type, user_role__id, params, options) {
  return authorizedGet({ uri: `/${user_type}/roles/${user_role__id}/`, params, options });
}

export function getBUList(params, options) {
  return authorizedGet({ uri: `/common/bu/`, params, options });
}

export function addAccount(id, body) {
  return authorizedPost({ uri: `/accounts/user-type/${id}/`, body })
}

export function deleteAccount(id, type) {
  return authorizedDelete({ uri: `/accounts/user-type/${id}/${type}/` })
}

export function downloadCsv(year, type) {
  return authorizedGet({ uri: `/projects/export/${year}/?report_type=${type}` })
}

export function downloadFieldAssessment(year) {
  return authorizedGet({ uri: `/fields/export-fieldassessment/${year}/` })
}

export function editAccount(id, body) {
  return authorizedPatch({ uri: `/accounts/${id}/`, body });
}

export function setDefaultAccount(id, type, body) {
  return authorizedPatch({ uri: `/accounts/user-type/${id}/${type}/set-default/`, body });
}

// ICQ

export function getIcqList(params, options) {
  return authorizedGet({ uri: '/icq/', params, options });
}

export function getIcqDetailedList(id, year, params, options) {
  return authorizedGet({ uri: `/icq/${id}/${year}/`, params, options });
}

export function deleteICQPartner(id) {
  return authorizedDelete({ uri: `/partners/icq/${id}/` });
}

export function patchICQPartner(id, body) {
  return authorizedPatch({ uri: `/partners/icq/${id}/`, body });
}

export function postNewICQPartner(body) {
  return authorizedPost({ uri: '/partners/icq/', body });
}

//PQP
export function getPqpList(params, options) {
  return authorizedGet({ uri: '/partners/pqp/', params, options });
}

export function deletePQPPartner(id) {
  return authorizedDelete({ uri: `/partners/pqp/${id}/` });
}

export function patchPQPPartner(id, body) {
  return authorizedPatch({ uri: `/partners/pqp/${id}/`, body });
}

export function postNewPQPPartner(body) {
  return authorizedPost({ uri: '/partners/pqp/', body });
}

//OIOS
export function getOiosList(params, options) {
  return authorizedGet({ uri: '/projects/oios/', params, options });
}

export function deleteOIOSPartner(id) {
  return authorizedDelete({ uri: `/projects/oios/${id}/` });
}

export function patchOIOSPartner(id, body) {
  return authorizedPatch({ uri: `/projects/oios/${id}/`, body });
}

export function postNewOIOSPartner(body) {
  return authorizedPost({ uri: '/projects/oios/', body });
}

//Project Selection
export function getPAList(year, params, options) {
  return authorizedGet({ uri: `/projects/list/${year}/`, params, options });
}

export function getPAExcludedList(year, params, options) {
  return authorizedGet({ uri: `/projects/list/excluded/${year}/`, params, options });
}

export function getILExcludedList(year, params, options) {
  return authorizedGet({ uri: `/projects/interim-list/excluded/${year}/`, params, options });
}

export function getFLExcludedList(year, params, options) {
  return authorizedGet({ uri: `/projects/final-list/excluded/${year}/`, params, options });
}

export function getAgreementList(params, options) {
  return authorizedGet({ uri: `/projects/agreements/`, params, options });
}

// export function getPreliminaryList(year, params, options) {
//   return authorizedGet({ uri: `/projects/preliminary-list/${year}/`, params, options });
// }

export function getPreliminaryStats(year) {
  return authorizedGet({ uri: `/projects/project-selection-stats/${year}/` })
}

export function remindFO(body) {
  return authorizedPost({ uri: `/fields/remind-interim/`, body })
}

export function getProjectDetails(id, options) {
  return authorizedGet({ uri: `/projects/${id}/`, options });
}

export function getProjectDetailsExp(id, options) {
  return authorizedGet({ uri: `/projects/exp-/${id}/`, options });
}

export function editProjectDetails(id, body) {
  return authorizedPatch({ uri: `/projects/modify-member/${id}/`, body })
}

export function patchProject(year, body) {
  return authorizedPost({ uri: `/projects/modify-status/${year}/`, body });
}

export function patchProjectGov(id, body) {
  return authorizedPatch({ uri: `/projects/${id}/`, body });
}

export function patchILProject(id, body) {
  return authorizedPatch({ uri: `/projects/interim-list/${id}/`, body });
}

export function patchFLProject(id, body) {
  return authorizedPatch({ uri: `/projects/final-list/${id}/`, body });
}

export function getPreviewExcluded(year, params, options) {
  return authorizedGet({ uri: `/projects/preliminary-list/preview/excluded/${year}/`, params, options });
}

export function getPreviewIncluded(year, params, options) {
  return authorizedGet({ uri: `/projects/preliminary-list/preview/included/${year}/`, params, options });
}

//Selection Settings
export function getSettings(params, options) {
  return authorizedGet({ uri: `/projects/selection-settings/`, params, options });
}

export function patchSettings(year, body) {
  return authorizedPatch({ uri: `/projects/selection-settings/${year}/`, body });
}

// Cycles
// export function getCycle(year, params, options) {
//   return authorizedGet({ uri: `/projects/cycles/${year}/`, params, options });
// }

export function getCycle(year, params, options) {
  return authorizedGet({ uri: `/projects/latest-cycle/${year}/`, params, options });
}

// export function getLatestCycle(params, options) {
//   return authorizedGet({ uri: '/projects/latest-cycle/', params, options });
// }

export function patchCycle(year, body) {
  return authorizedPatch({ uri: `/projects/cycles/${year}/`, body });
}

// Partners
export function getPartnerList(params, options) {
  return authorizedGet({ uri: `/partners/`, params, options });
}

export function getPartnerDetails(id, options) {
  return authorizedGet({ uri: `/partners/${id}/`, options });
}

export function partnerAssignmentList(id, params) {
  return authorizedGet({ uri: `/partners/agreements/${id}/`, params });
}

// Interim List
export function getInterimListPro(year, params, options) {
  return authorizedGet({ uri: `/projects/interim-list/all/${year}/`, params, options });
}

export function getInterimList(year, params, options) {
  return authorizedGet({ uri: `/projects/interim-list/included/${year}/`, params, options });
}

export function getInterimListInProgress(year, params, options) {
  params.year = year;
  return authorizedGet({ uri: `/projects/interim-list/progress/`, params, options });
}

export function initializeInterimList(year) {
  return authorizedPost({ uri: `/projects/interim-list/initialize/${year}/` });
}

export function rollbackInterimList(year) {
  return authorizedPost({ uri: `/projects/interim-list/rollback/${year}/` })
}

export function getInterimListCycleFO(year, foId) {
  return authorizedGet({ uri: `/projects/interim-list/${year}/${foId}/` })
}

//Final List
export function getFinalList(year, params, options) {
  return authorizedGet({ uri: `/projects/final-list/all/${year}/`, params, options });
}

export function initializeFinalList(year) {
  return authorizedPost({ uri: `/projects/final-list/initialize/${year}/` });
}

export function initializeFinalListFO(year, foId) {
  return authorizedPost({ uri: `/projects/final-list/initialize/${year}/${foId}/` });
}

export function rollbackFinalList(year) {
  return authorizedPost({ uri: `/projects/final-list/rollback/${year}/` })
}

export function rollbackCycle(year, body) {
  return authorizedPatch({ uri: `/projects/cycles/${year}/`, body })
}

export function getExcludedFinalList(year, params, options) {
  return authorizedGet({ uri: `/projects/final-list/excluded/${year}/`, params, options });
}

export function patchFinalProject(id, body) {
  return authorizedPatch({ uri: `/projects/final-list/${id}/`, body });
}

//Fields
export function getFieldList(params, options) {
  return authorizedGet({ uri: `/fields/`, params, options });
}

export function sendReminder(body) {
  return authorizedPost({ uri: `/fields/assessments/remind/`, body });
}

export function setDeadline(body) {
  return authorizedPost({ uri: `/fields/assessments/deadline/`, body });
}

export function setDeltaDeadline(cycleId, body) {
  return authorizedPatch({ uri: `/fields/assessments/delta-deadline/${cycleId}/`, body });
}

export function assessmentPostDeadline(body) {
  return authorizedPost({ uri: `/fields/assessments/progress-hq/`, body });

}

export function getDeadline(params, options) {
  return authorizedGet({ uri: `/fields/assessments/deadline/`, params, options });
}

export function fieldDashboard(params) {
  return authorizedGet({ uri: `/fields/assessments/stats/`, params })
}

//Partner Assessment
export function getFieldAssessment(params) {
  return authorizedGet({ uri: `/fields/assessments/`, params })
}

export function editRating(id, body) {
  return authorizedPatch({ uri: `/fields/assessments/${id}/`, body })
}

//Audit Firms
export function getAuditList(params, options) {
  return authorizedGet({ uri: `/audits/`, params, options });
}

export function patchAuditFirm(id, body) {
  return authorizedPatch({ uri: `/audits/${id}/`, body });
}

export function createAuditFirm(body) {
  return authorizedPost({ uri: `/audits/`, body });
}

//Audit Groups
export function getGroupList(params, options) {
  return authorizedGet({ uri: `/audits/groups/`, params, options });
}

export function patchAuditGroup(id, body) {
  return authorizedPatch({ uri: `/audits/group/${id}/`, body });
}

export function createAuditGroup(body) {
  return authorizedPost({ uri: `/audits/group/`, body });
}

export function deleteAuditGroup(id) {
  return authorizedDelete({ uri: `/audits/group/${id}/` });
}

// Audit Project Assignments
export function getProjectAssignments(year, params, options) {
  return authorizedGet({ uri: `/projects/assignments/${year}/`, params, options })
}

export function patchProjectAssignments(year, body) {
  return authorizedPost({ uri: `/projects/assignments/${year}/`, body })
}

export function remindAuditAssignment(year, params) {
  return authorizedGet({ uri: `/projects/audit-assignment-status/${year}/`, params })
}

export function unassignFirm(year, body) {
  return authorizedPost({ uri: `/projects/audit-unassignment/${year}/`, body })
}

export function sendAuditReminder(year) {
  return authorizedPost({ uri: `/audits/end-of-assignment-notify/${year}/` })
}

//Dashboard
export function getDashboardDetails(year, params, options) {
  return authorizedGet({ uri: `/projects/hq-dashboard/${year}/`, params, options });
}

// get summary data
export function getSummaryData(params, options) {
  return [
    {
      "year": 2017,
      "all": {
        "total": 1577,
        "value": 1313
      },
      "ngo": {
        "total": 1534,
        "value": 1242
      },
      "ge_risk": {
        "total": 511,
        "value": 744
      },
      "adverse": {
        "total": 11,
        "value": 4
      }
    },
    {
      "year": 2018,
      "all": {
        "total": 1789,
        "value": 1678
      },
      "ngo": {
        "total": 1534,
        "value": 1242
      },
      "ge_risk": {
        "total": 777,
        "value": 666
      },
      "adverse": {
        "total": 11,
        "value": 4
      }
    },
    {
      "year": 2019,
      "all": {
        "total": 1277,
        "value": 1213
      },
      "ngo": {
        "total": 1334,
        "value": 1142
      },
      "ge_risk": {
        "total": 711,
        "value": 444
      },
      "adverse": {
        "total": 15,
        "value": 7
      }
    },
  ];
}

// get all messages
export function getMessageList(params) {
  return authorizedGet({ uri: `/notification/`, params });
}

export function getMessageDetail(id) {
  return authorizedGet({ uri: `/notification/${id}/` });
}

export function updateMessage(id, body) {
  return authorizedPatch({ uri: `/notification/${id}/`, body });
}

// activity
export function getActivities(year, params) {
  return authorizedGet({ uri: `/projects/activity/${year}/`, params });
}

// comments
// export function editProject(year, body) {
//   return authorizedPost({ uri: `/projects/modify/${year}/`, body});
// }

export function promoteProject(year, body) {
  return authorizedPost({ uri: `/projects/promote/${year}/`, body });
}

export function getComments(year, params) {
  return authorizedGet({ uri: `/projects/comments/` });
}

// budget

export function budgetList(id, params) {
  return authorizedGet({ uri: `/projects/budgets/${id}/`, params });
}

//messages

export function getMessages(id, params) {
  return authorizedGet({ uri: `/projects/agreement/${id}/messages/`, params })
}

export function postMessages(id, body) {
  return authorizedPost({ uri: `/projects/agreement/${id}/messages/`, body })
}

export function deleteMessages(id, msgId) {
  return authorizedDelete({ uri: `/projects/agreement/${id}/message/${msgId}/` })
}

export function editMessages(rowId, id, body) {
  return authorizedPatch({ uri: `/projects/agreement/${rowId}/message/${id}/`, body })
}

// document
export function uploadDocument(body) {
  return authorizedPostUpload({ uri: `/documents/`, body });
}

export function getUploadedDocuments(id) {
  return authorizedGet({ uri: `/documents/${id}/` });
}

export function patchDocuments(projectId, id, body) {
  return authorizedPatch({ uri: `/documents/project_id/${projectId}/file/${id}/`, body });
}

export function deleteDocument(projectId, id, params) {
  return authorizedDelete({ uri: `/documents/project_id/${projectId}/file/${id}/`, params })
}

export function postFieldAssessmentProgress(body) {
  return authorizedPost({ uri: `/fields/assessments/progress/`, body });
}

export function getFieldAssessmentProgress(params) {
  return authorizedGet({ uri: `/fields/assessments/progress/`, params });
}

export function getFieldAssessmentCanProceed(params) {
  return authorizedGet({ uri: `/fields/assessments/canproceed/`, params })
}

//get all countries

export function getCountries(params) {
  return get('/common/countries/', params)
}

// reports

export function getAuditReports(id, params) {
  return authorizedGet({ uri: `/audits/agreement/${id}/audit-report/`, params });
}

export function patchAuditReports(id, body) {
  return authorizedPatchUpload({ uri: `/audits/audit-report/${id}/`, body });
}

export function editManagementReport(id, mgmtID, body) {
  return authorizedPatch({ uri: `/audits/agreements/${id}/mgmts/${mgmtID}/`, body });
}

export function getReportStatus(id, params) {
  return authorizedGet({ uri: `/audits/agreements/${id}/report-status/`, params });
}

export function getSelectionProgress(year, params) {
  return authorizedGet({ uri: `/projects/fo_progress/${year}/`, params });
}

export function getSelectionProgressHQ(year, params) {
  return authorizedGet({ uri: `/projects/selection_progress/${year}/`, params });
}

export function postProjectProgressHQ(year, body) {
  return authorizedPost({ uri: `/projects/hq-promote/${year}/`, body });
}

export function postFollowUp(id, body) {
  return authorizedPatch({ uri: `/icq/mgmts/follow_ups/${id}/`, body });
}

export function patchICQQuestionnaire(body) {
  return authorizedPost({ uri: `/icq/upload/`, body });
}

// Cron job

export function getCompletionStatus(params) {
  return authorizedGet({ uri: `/common/bg-tasks/`, params })
}

//Management Report


export function editFinancialFinding(id, body) {
  return authorizedPatch({ uri: `/icq/mgmts/fin_findings/${id}/`, body });
}

export function editMajorFinding(id, body) {
  return authorizedPatch({ uri: `/audits/mgmts/major_findings/${id}/`, body });
}

export function deleteMajorFinding(id) {
  return authorizedDelete({ uri: `/audits/mgmts/major_findings/${id}/` })
}

export function deleteOverallAssessment(id) {
  return authorizedDelete({ uri: `/audits/mgmts/overall_assessments/${id}/` })
}

export function deleteFollowup(id) {
  return authorizedDelete({ uri: `/icq/mgmts/follow_ups/${id}/` })
}

export function getManagementReport(id, params) {
  return authorizedGet({ uri: `/icq/agreements/${id}/mgmts/`, params });
}

export function getManagementReportExp(id, params) {
  return authorizedGet({ uri: `/icq/agreements/${id}/mgmts-exp/`, params });
}

export function getMajorFinding(id, params) {
  return authorizedGet({ uri: `/icq/major_findings/${id}/`, params });
}

export function getOverallRating(id, params) {
  return authorizedGet({ uri: `/icq/overallrating/${id}/`, params });
}

export function getOverallRatingExp(id, params) {
  return authorizedGet({ uri: `/icq/overallrating-exp/${id}/`, params });
}

export function getFollowUps(id, params) {
  return authorizedGet({ uri: `/icq/mgmts/follow_ups/${id}/`, params })
}

export function patchManagementReport(id, mgmtId, body) {
  return authorizedPatch({ uri: `/icq/agreements/${id}/mgmts/${mgmtId}/`, body });
}

export function editGeneralReport(id, body) {
  return authorizedPatch({ uri: `/icq/mgmts/general/${id}/`, body });
}
//location

export function addLocations(body) {
  return authorizedPost({ uri: `/projects/locations/`, body })
}

export function patchLocations(id, body) {
  return authorizedPatch({ uri: `/projects/locations/${id}/`, body })
}

export function deleteLocations(id) {
  return authorizedDelete({ uri: `/projects/delete-locations/${id}/` })
}

export function patchICQ(id, body) {
  return authorizedPatch({ uri: `/icq/${id}/`, body });
}

// default

export function setDefault(userRole, userId) {
  return authorizedPatch({ uri: `/accounts/set-default/${userRole}/${userId}/` })
}

export function bulkICQPost(year, body) {
  return authorizedPost({ uri: `/icq/icq-bulk-upload/${year}/`, body })
}

export function getBulkICQErrors(params) {
  return authorizedGet({ uri: `/icq/icq-bulk-upload-error/`, params })
}

//quality assurance
export function getAuditFirmCountriesList(params) {
  if (params.audit_agency) {
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-compositions/${params.year}/?audit_agency=${params.audit_agency}`,
    });
  } else {
    if (typeof (params) == "number" || typeof (params) == "string") {
      return authorizedGet({
        uri: `/quality-assurance/kpi4/team-compositions/${params}/`,
      });
    }
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-compositions/${params.year}/`,
    });
  }
}

export function postSampleAuditFirmCountriesList(year, body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi4/team-composition/sample-countries/${year}/`,
    body,
  });
}

export function postConfirmAuditFirmCountriesList(year, body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi4/team-compositions/${year}/`,
    body,
  });
}

export function patchKpi4TeamCompositionReport(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi4/team-composition/${id}/`,
    body,
  });
}

export function getTeamCompositionUsersById(teamCompositionId) {
  return authorizedGet({
    uri: `/quality-assurance/kpi4/team-composition/${teamCompositionId}/`,
  });
}

export function getKpi4TeamCompositionReport(teamCompositionId) {
  return authorizedGet({
    uri: `/quality-assurance/kpi4/team-composition/${teamCompositionId}/`,
  });
}

export function addImprovementArea(body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi4/team-composition/improvement-area/`, body
  });
}

export function updateImprovementArea(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi4/team-composition/improvement-area/${id}/`, body
  });
}

export function deleteImprovementArea(id) {
  return authorizedDelete({
    uri: `/quality-assurance/kpi4/team-composition/improvement-area/${id}/`,
  });
}

export function exportUsersApi() {
  return authorizedGet({
    uri: `/accounts/user-export/`,
  });
}

export function uploadImportDataApi(body) {
  return authorizedPostUpload({
    uri: `/dashboard/import-data/`, body
  });
}

export function importDeltasApi(body) {
  return authorizedPostUpload({
    uri: `/dashboard/import-data/`, body
  });
}

export function deleteFieldAssessment(id) {
  return authorizedDelete({
    uri: `/fields/delete-project/${id}/`,
  });
}
export function getKpi4TeamCompositionCountryCriterias() {
  return authorizedGet({
    uri: `/quality-assurance/kpi4/team-composition/criterias/`,
  });
}

export function sendTeamCompositionNotification(body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi4/team-composition/notify/`, body
  });
}

export function updateMemberProfile(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi4/team-composition/update-profile/${id}/`, body
  });
}

export function getPPAbyMemberId(id, year) {
  return authorizedGet({
    uri: `/quality-assurance/kpi4/agreement-member-projects/?agreement_member_id=${id}&year=${year}`,
  });
}

export function copyPPAprofiles(body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi4/team-composition/update-agreement-profiles/`, body
  });
}

export function getTeamCompositionMemebrsByNameOrRole(id, year, name, role) {
  if (name && name != "") {
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-composition-profile/${id}/?year=${year}&name=${name}`,
    });
  } else if (role && role != "") {
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-composition-profile/${id}/?year=${year}&role=${role}`,
    });
  } else if (name && name != "" && role && role != "") {
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-composition-profile/${id}/?year=${year}&name=${name}&role=${role}`,
    });
  } else {
    return authorizedGet({
      uri: `/quality-assurance/kpi4/team-composition-profile/${id}/?year=${year}`,
    });
  }

}

export function startSurvey(year) {
  return authorizedPost({
    uri: `/quality-assurance/agreement-survey/start/${year}/`,
  });
}

export function getSurveyDetailsByAgreementNumber(agreement_number) {
  return authorizedGet({
    uri: `/quality-assurance/agreement-surveys/${agreement_number}/`,
  });
}

export function saveKpi3SurveyResponse(id, body) {
  return authorizedPost({
    uri: `/quality-assurance/agreement-survey/${id}/response/`, body
  });
}

export function getSurveyRecords(year, audit_agency) {
  if (year && audit_agency) {
    return authorizedGet({
      uri: `/quality-assurance/agreement-surveys/?year=${year}&audit_agency_id=${audit_agency}`,
    });
  } else if (year) {
    return authorizedGet({
      uri: `/quality-assurance/agreement-surveys/?year=${year}`,
    });
  }
}

export function saveKpi3Report(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/agreement-survey-report-update/${id}/`, body
  });
}

export function notifyAgreementSurveys(body) {
  return authorizedPost({
    uri: `/quality-assurance/agreement-surveys/notify/`, body
  });
}

export function getKpi3Report(year, firm_id) {
  return authorizedGet({
    uri: `/quality-assurance/agreement-survey-report/${year}/${firm_id}/`,
  });
}

export function getKpi3QuestionResponse(year, questionId, survey_type) {
  return authorizedGet({
    uri: `/quality-assurance/survey-stats/?year=${year}&question=${questionId}&survey_type=${survey_type}`,
  });
}

export function getKpi3QuestionComments(year, survey_type, page, bu_code, agreement_number, answer_option, partner_name) {
  let url = `/quality-assurance/agreement-survey-response/${year}/?survey_type=${survey_type}`;
  if (page) {
    url = url + `&page=${page}`
  }
  if (bu_code) {
    url = url + `&business_unit=${bu_code}`
  }
  if (agreement_number) {
    url = url + `&number=${agreement_number}`
  }
  if (answer_option) {
    url = url + `&answer_option=${answer_option}`
  }
  if (partner_name) {
    url = url + `&partner_name=${partner_name}`
  }
  return authorizedGet({
    uri: url,
  });
}

export function checkSurveyQuestionsUploaded(year) {
  return authorizedGet({
    uri: `/quality-assurance/survey-questions-imported/${year}/`,
  });
}

export function uploadSurveyQuestions(body) {
  return authorizedPostUpload({
    uri: `/quality-assurance/import-survey-questions/`, body
  });
}

export function requestNewWorkingPaper(body) {
  return authorizedPost({
    uri: `/quality-assurance/kpi2/request-working-papers/`, body
  });
}

export function requestUpdateWorkingPaper(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi2/working-papers/${id}/`, body
  });
}

export function uploadWorkingPaper(id, body) {
  return authorizedPatchUpload({
    uri: `/quality-assurance/kpi2/upload-working-papers/${id}/`, body
  });
}

export function deleteWorkingPaper(id) {
  return authorizedDelete({
    uri: `/quality-assurance/kpi2/delete-working-papers/${id}/`,
  });
}

export function startKpi2Survey(year) {
  return authorizedPost({
    uri: `/quality-assurance/kpi2/start/${year}/`,
  });
}

export function getKpi2AuditFirms(auditFirmName, year) {
  if (auditFirmName) {
    return authorizedGet({
      uri: `/quality-assurance/kpi2/audit-firms/${year}/?firm=${auditFirmName}`,
    });
  }
  return authorizedGet({
    uri: `/quality-assurance/kpi2/audit-firms/${year}/`,
  });
}

export function checkIfAuditWorkCompleted(year) {
  return authorizedGet({
    uri: `/quality-assurance/audit-work-status/${year}/`,
  });
}

export function updateFinalReport(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi2/final-report/${id}/`, body
  });
}

export function updateSummaryReport(id, body) {
  return authorizedPatch({
    uri: `/quality-assurance/kpi2/summary-report/${id}/`, body
  });
}

export function getKpi2FinalReport(id) {
  return authorizedGet({
    uri: `/quality-assurance/kpi2/final-report/${id}/`,
  });
}

export function getKpi2SummaryReport(id) {
  return authorizedGet({
    uri: `/quality-assurance/kpi2/summary-report/${id}/`,
  });
}

export function checkSurveyUploadStatus(year) {
  return authorizedGet({
    uri: `/quality-assurance/survey-upload-status/${year}/`,
  });
}

export function getKpi2KeyFindings(params) {
  return authorizedGet({
    uri: `/quality-assurance/kpi2/key-findings/${params.firm_id}/`, params
  });
}