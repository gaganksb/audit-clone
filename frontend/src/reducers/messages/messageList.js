import R from 'ramda';
import { getMessageList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const MESSAGES_LOAD_STARTED = 'MESSAGES_LOAD_STARTED';
export const MESSAGES_LOAD_SUCCESS = 'MESSAGES_LOAD_SUCCESS';
export const MESSAGES_LOAD_FAILURE = 'MESSAGES_LOAD_FAILURE';
export const MESSAGES_LOAD_ENDED = 'MESSAGES_LOAD_ENDED';
export const NEW_MESSAGES_LOAD_SUCCESS = 'NEW_MESSAGES_LOAD_SUCCESS';

export const messagesLoadStarted = () => ({ type: MESSAGES_LOAD_STARTED });
export const messagesLoadSuccess = response => ({ type: MESSAGES_LOAD_SUCCESS, response });
export const newMessagesLoadSuccess = response => ({ type: NEW_MESSAGES_LOAD_SUCCESS, response });
export const messagesLoadFailure = error => ({ type: MESSAGES_LOAD_FAILURE, error });
export const messagesLoadEnded = () => ({ type: MESSAGES_LOAD_ENDED });

const saveMessages = (state, action) => {
  const messages = R.assoc('messages', action.response.results, state);
  return R.assoc('totalCount', action.response.count, messages);
};

const saveNewMessages = (state, action) => {
  const messages = R.assoc('newMessages', action.response.results, state);
  return R.assoc('totalCountNew', action.response.count, messages);
};

const messages = {
  loadFailed: 'Loading messages failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  messages: [],
  newMessages: [],
  totalCountNew: 0
};

export const loadMessageList = (params) => (dispatch) => {
  dispatch(messagesLoadStarted());
  return getMessageList(params)
    .then((msgs) => {
      dispatch(messagesLoadEnded());
      dispatch(messagesLoadSuccess(msgs));
    })
    .catch((error) => {
      dispatch(messagesLoadEnded());
      dispatch(messagesLoadFailure(error));
    });
};

export const loadNewMessageList = (params) => (dispatch) => {
  dispatch(messagesLoadStarted());
  return getMessageList(params)
    .then((msgs) => {
      dispatch(messagesLoadEnded());
      dispatch(newMessagesLoadSuccess(msgs));
    })
    .catch((error) => {
      dispatch(messagesLoadEnded());
      dispatch(messagesLoadFailure(error));
    });
};

export default function messageListReducer(state = initialState, action) {
  switch (action.type) {
    case MESSAGES_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case MESSAGES_LOAD_ENDED: {
      return stopLoading(state);
    }
    case MESSAGES_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case MESSAGES_LOAD_SUCCESS: {
      return saveMessages(state, action);
    }
    case NEW_MESSAGES_LOAD_SUCCESS: {
      return saveNewMessages(state, action);
    }
    default:
      return state;
  }
}
