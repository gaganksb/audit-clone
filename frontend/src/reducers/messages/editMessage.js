import { updateMessage } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../reducers/apiMeta';

export const PATCH_MESSAGE = 'PATCH_MESSAGE';

export const editMessage = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_MESSAGE));
  return updateMessage(id, body)
    .then((message) => {
      dispatch(loadEnded(PATCH_MESSAGE));
      dispatch(loadSuccess(PATCH_MESSAGE));
      return message;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_MESSAGE));
      dispatch(loadFailure(PATCH_MESSAGE, error));
      throw error;
    });
};