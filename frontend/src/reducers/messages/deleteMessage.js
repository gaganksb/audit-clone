import { deleteMessage } from '../../helpers/api/api';
import { errorToBeAdded } from '../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../reducers/apiMeta';

export const DELETE_MESSAGE = 'DELETE_MESSAGE';
const errorMsg = 'Unable to delete User';

export const deleteMessageRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_MESSAGE));
  return deleteMessage(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_MESSAGE));
      dispatch(loadSuccess(DELETE_MESSAGE));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_MESSAGE));
      dispatch(loadFailure(DELETE_MESSAGE, error));
      dispatch(errorToBeAdded(error, 'messageDelete', errorMsg));
    });
};
