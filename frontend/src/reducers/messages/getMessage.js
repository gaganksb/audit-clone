import { getMessageDetail } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../reducers/apiMeta';

export const GET_MESSAGE = 'GET_MESSAGE';

export const getMessage = (id, body) => (dispatch) => {
  dispatch(loadStarted(GET_MESSAGE));
  return getMessageDetail(id, body)
    .then(message => {
      dispatch(loadEnded(GET_MESSAGE));
      dispatch(loadSuccess(GET_MESSAGE));
      return message;
    })
    .catch(error => {
      dispatch(loadEnded(GET_MESSAGE));
      dispatch(loadFailure(GET_MESSAGE, error));
      throw error;
    });
};
