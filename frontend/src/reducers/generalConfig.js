import { getGlobalConfig } from '../helpers/api/api';
import { sessionError } from '../reducers/session';

export const LOAD_GENERAL_CONFIG_SUCCESS = 'LOAD_GENERAL_CONFIG_SUCCESS';

const initialState = {};

const loadGeneralConfigSuccess = config => ({ type: LOAD_GENERAL_CONFIG_SUCCESS, config });

export const loadGeneralConfig = () => dispatch => getGlobalConfig()
  .then(config => dispatch(loadGeneralConfigSuccess(config)))
  .catch(error => dispatch(sessionError(error)));

export default function generalConfigReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_GENERAL_CONFIG_SUCCESS: {
      return action.config;
    }
    default:
      return state;
  }
}
