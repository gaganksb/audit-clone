import { browserHistory as history } from 'react-router';
import R from 'ramda';
import {
  getUserData,
  login,
  logout,
} from '../helpers/api/api';
import { SESSION_STATUS, ACCOUNT_TYPES, OTHER  } from '../helpers/constants';

export const SESSION_CHANGE = 'SESSION_CHANGE';
export const SESSION_READY = 'SESSION_READY';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGIN_SUBMITTING = 'LOGIN_SUBMITTING';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const SESSION_ERROR = 'SESSION_ERROR';

const initialState = {
  state: SESSION_STATUS.INITIAL,
  authorized: false,
  fontSize: '16px',
  user: null,
  userId: null,
  token: null,
  userStatus: null,
  userLogged: false,
  error: null,
  email: null,
  user_type: null,
  user_role: null,
  membership: null,
};

// Action creators
export const initSession = session => ({ type: SESSION_CHANGE, session });
export const sessionInitializing = () => ({
  type: SESSION_CHANGE,
  session: { state: SESSION_STATUS.CHANGING }
});
export const sessionChange = session => ({
  type: SESSION_CHANGE,
  session: { ...session, state: SESSION_STATUS.READY }
});
export const sessionReady = getState => ({
  type: SESSION_READY,
  session: { state: SESSION_STATUS.READY },
  getState
});
export const sessionError = error => ({ type: SESSION_ERROR, error });
export const loginSuccess = session => ({ type: LOGIN_SUCCESS, session });
export const logoutSuccess = () => ({ type: LOGOUT_SUCCESS });
export const userData = session => ({ type: USER_INFO, session });

const setUserTypeRole = (input) => {
  let user_type = OTHER
  let user_role = null
  for (const [key, value] of Object.entries(input.accounts)) {
    for (let i = 0; i < value.length; i++) {
      if (value[i].is_default) {
        user_type = ACCOUNT_TYPES[key]
        user_role = value[i].role
      }
    }
  }
  const res = {
    user_type,
    user_role
  }
  return res
}

// 
export const loadUserData = () => (dispatch, getState) => {
  const { session } = getState();
  const token = session.token;

  dispatch(sessionInitializing());
  return getUserData(token)
    .then((response) => {
      
      const { user_type, user_role } = setUserTypeRole(response);
      window.localStorage.setItem('user_type', user_type);
      window.localStorage.setItem('user_role', user_role);
      let sessionObject = {
        user_type,
        user_role,
        name: response.name,
        userId: response.id,
        email: response.email,
        authorized: true,
      };
      const addToSession = R.mergeDeepRight(sessionObject);
      sessionObject = addToSession(sessionObject);

      dispatch(initSession(sessionObject));
      dispatch(sessionReady(getState));

      return sessionObject;
    })
    .catch((error) => {
      if (error.response.status === 404) {
        history.push('/registration');
      } else {
        window.localStorage.removeItem('token');
        window.location.href = '/landing/';
      }

      dispatch(initSession({
        authorized: false,
        error,
      }));
    });
};

export const loginUser = creds => dispatch => login(creds)
  .then((response) => {
    window.localStorage.setItem('token', response.key);
    dispatch(loginSuccess({ user: creds.email, token: response.key }));
    dispatch(loadUserData());
    history.push('/dashboard');
  });

export const logoutUser = () => (dispatch, getState) => {
  const logoutAzure = getState().generalConfig['active-directory-logout-url'];

  return logout()
    .then(() => {
      window.localStorage.removeItem('token');
      dispatch(logoutSuccess());
      if (logoutAzure === undefined) {
        window.location.href = '/direct-login';
      } else {
        window.location.href = logoutAzure;
      }
    }).catch(() => {
      window.localStorage.removeItem('token');
      dispatch(logoutSuccess());
      if (logoutAzure === undefined) {
        window.location.href = '/direct-login';
      } else {
        window.location.href = logoutAzure;
      }
    })
  }

// Reducer helper function
const setSession = (state, session) => {
  return R.mergeDeepRight(state, session);
};

// Reducer
export default function sessionReducer(state = initialState, action) {
  switch (action.type) {
    case SESSION_READY: {
      return setSession(state, action.session);
    }
    case SESSION_CHANGE: {
      return setSession(state, action.session);
    }
    case LOGIN_SUCCESS: {
      return setSession(state, { userLogged: true, ...action.session });
    }
    case LOGOUT_SUCCESS: {
      return initialState;
    }
    case SESSION_ERROR: {
      return R.assoc('error', action.error, state);
    }
    default:
      return state;
  }
}