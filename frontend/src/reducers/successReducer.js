

export const SNACKBAR_SUCCESS = 'SNACKBAR_SUCCESS';
export const SNACKBAR_CLEAR = 'SNACKBAR_CLEAR';

export const showSuccessSnackbar = message => {
    return dispatch => {
      dispatch({ type: "SNACKBAR_SUCCESS", message });
    };
  };
  
  export const clearSnackbar = () => {
    return dispatch => {
      dispatch({ type: "SNACKBAR_CLEAR" });
    };
  };

  const initialState = {
    successSnackbarOpen: false,
    successSnackbarMessage: 'SUCCESS !',
  };

  export default function successReducer(state = initialState, action) {
    switch (action.type) {
        case SNACKBAR_SUCCESS:
          return {
            ...state,
            successSnackbarOpen: true,
            successSnackbarMessage: action.message
          };
        case SNACKBAR_CLEAR:
          return {
            ...state,
            successSnackbarOpen: false,
          };
        default:
          return state;
      }
  }