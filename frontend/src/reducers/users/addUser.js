import { postNewUser } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';

export const NEW_USER = 'NEW_USER';

export const addNewUser = body => (dispatch) => {
  dispatch(loadStarted(NEW_USER));
  return postNewUser(body)
    .then((user) => {
      dispatch(loadEnded(NEW_USER));
      dispatch(loadSuccess(NEW_USER));
      return user;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_USER));
      dispatch(loadFailure(NEW_USER, error));
      throw error;
    });
};
