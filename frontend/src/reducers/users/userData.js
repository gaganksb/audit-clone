import { combineReducers } from 'redux';
import { sendRequest } from '../../helpers/apiHelper';
import apiMeta, { success } from '../apiMeta';
import { getUserData } from '../../helpers/api/api';

const USERDATA = 'USERDATA';
const tag = 'userData';

const errorMsg = 'Couldn\'t load dashboard information, ' +
'please refresh page and try again';

const initialState = {};

export const loadUser = () => sendRequest({
  loadFunction: getUserData,
  meta: {
    reducerTag: tag,
    actionTag: USERDATA,
  },
  errorHandling: { userMessage: errorMsg },
});

const UserData = (state = initialState, action) => {
  switch (action.type) {
    case success`${USERDATA}`: {
      return action.results;
    }
    default:
      return state;
  }
};

export default combineReducers({ data: UserData, status: apiMeta(USERDATA) });