import { 
  addAccount, 
  deleteAccount, 
  editAccount, 
  setDefaultAccount } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';
import { errorToBeAdded } from '../../reducers/errorReducer';

const errors = {
  addAccount: 'Unable to add account',
  deleteAccount: 'Unable to delete account',
  editAccount: 'Unable to edit account',
  setDefaultAccount: 'Unable to select account',
};

const ADD_ACCOUNT = 'ADD_ACCOUNT';
export const addUserAccount = (id, body) => (dispatch) => {
  dispatch(loadStarted(ADD_ACCOUNT));
  return addAccount(id, body)
    .then((success) => {
      dispatch(loadEnded(ADD_ACCOUNT));
      dispatch(loadSuccess(ADD_ACCOUNT));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(ADD_ACCOUNT));
      dispatch(loadFailure(ADD_ACCOUNT, error));
      dispatch(errorToBeAdded(error, 'addUserAccount', errors.addAccount));
    });
};

const DELETE_ACCOUNT = 'DELETE_ACCOUNT';
export const deleteUserAccount = (id, type) => (dispatch) => {
  dispatch(loadStarted(DELETE_ACCOUNT));
  return deleteAccount(id, type)
    .then((success) => {
      dispatch(loadEnded(DELETE_ACCOUNT));
      dispatch(loadSuccess(DELETE_ACCOUNT));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_ACCOUNT));
      dispatch(loadFailure(DELETE_ACCOUNT, error));
      dispatch(errorToBeAdded(error, 'deleteUserAccount', errors.deleteAccount));
    });
};

const EDIT_ACCOUNT = 'EDIT_ACCOUNT';
export const editUserAccount = (id, body) => (dispatch) => {
  dispatch(loadStarted(EDIT_ACCOUNT));
  return editAccount(id, body)
    .then((success) => {
      dispatch(loadEnded(EDIT_ACCOUNT));
      dispatch(loadSuccess(EDIT_ACCOUNT));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(EDIT_ACCOUNT));
      dispatch(loadFailure(EDIT_ACCOUNT, error));
      dispatch(errorToBeAdded(error, 'editUserAccount', errors.editAccount));
      return error;
    });
};

const SET_DEFAULT_ACCOUNT = 'SET_DEFAULT_ACCOUNT';
export const setDefaultUserAccount = (id, type, body) => (dispatch) => {
  dispatch(loadStarted(SET_DEFAULT_ACCOUNT));
  return setDefaultAccount(id, type, body)
    .then((success) => {
      dispatch(loadEnded(SET_DEFAULT_ACCOUNT));
      dispatch(loadSuccess(SET_DEFAULT_ACCOUNT));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(SET_DEFAULT_ACCOUNT));
      dispatch(loadFailure(SET_DEFAULT_ACCOUNT, error));
      dispatch(errorToBeAdded(error, 'setDefaultUserAccount', errors.setDefaultAccount));
    });
};
