import R from 'ramda';
import { getPermissionList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const PERMISSIONS_LOAD_STARTED = 'PERMISSIONS_LOAD_STARTED';
export const PERMISSIONS_LOAD_SUCCESS = 'PERMISSIONS_LOAD_SUCCESS';
export const PERMISSIONS_LOAD_FAILURE = 'PERMISSIONS_LOAD_FAILURE';
export const PERMISSIONS_LOAD_ENDED = 'PERMISSIONS_LOAD_ENDED';

export const permissionsLoadStarted = () => ({ type: PERMISSIONS_LOAD_STARTED });
export const permissionsLoadSuccess = response => ({ type: PERMISSIONS_LOAD_SUCCESS, response });
export const permissionsLoadFailure = error => ({ type: PERMISSIONS_LOAD_FAILURE, error });
export const permissionsLoadEnded = () => ({ type: PERMISSIONS_LOAD_ENDED });

const savePermissions = (state, action) => {
  return R.assoc('list', action.response, state);
};

const messages = {
  loadFailed: 'Loading permissions failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};


export const loadPermissionList = params => (dispatch) => {
  dispatch(permissionsLoadStarted());
  return getPermissionList(params)
    .then((permissions) => {
      dispatch(permissionsLoadEnded());
      dispatch(permissionsLoadSuccess(permissions));
    })
    .catch((error) => {
      dispatch(permissionsLoadEnded());
      dispatch(permissionsLoadFailure(error));
    });
};

export default function permissionsListReducer(state = initialState, action) {
  switch (action.type) {
    case PERMISSIONS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PERMISSIONS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PERMISSIONS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PERMISSIONS_LOAD_SUCCESS: {
      return savePermissions(state, action);
    }
    default:
      return state;
  }
}
