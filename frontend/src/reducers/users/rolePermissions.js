import R from 'ramda';
import { getRolePermissions } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const ROLE_PERMISSIONS_LOAD_STARTED = 'ROLE_PERMISSIONS_LOAD_STARTED';
export const ROLE_PERMISSIONS_LOAD_SUCCESS = 'ROLE_PERMISSIONS_LOAD_SUCCESS';
export const ROLE_PERMISSIONS_LOAD_FAILURE = 'ROLE_PERMISSIONS_LOAD_FAILURE';
export const ROLE_PERMISSIONS_LOAD_ENDED = 'ROLE_PERMISSIONS_LOAD_ENDED';

export const rolePermissionsLoadStarted = () => ({ type: ROLE_PERMISSIONS_LOAD_STARTED });
export const rolePermissionsLoadSuccess = response => ({ type: ROLE_PERMISSIONS_LOAD_SUCCESS, response });
export const rolePermissionsLoadFailure = error => ({ type: ROLE_PERMISSIONS_LOAD_FAILURE, error });
export const rolePermissionsLoadEnded = () => ({ type: ROLE_PERMISSIONS_LOAD_ENDED });

const saveRolePermissions = (state, action) => {
  return R.assoc('details', action.response, state);
};

const messages = {
  loadFailed: 'Loading role permissions failed.',
};

const initialState = {
  loading: false,
  details: [],
};


export const loadRolePermissionList = (user_type, user_role__id, params) => (dispatch) => {
  dispatch(rolePermissionsLoadStarted());
  return getRolePermissions(user_type, user_role__id, params)
    .then((permissions) => {
      dispatch(rolePermissionsLoadEnded());
      dispatch(rolePermissionsLoadSuccess(permissions));
    })
    .catch((error) => {
      dispatch(rolePermissionsLoadEnded());
      dispatch(rolePermissionsLoadFailure(error));
    });
};

export default function rolePermissionsListReducer(state = initialState, action) {
  switch (action.type) {
    case ROLE_PERMISSIONS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ROLE_PERMISSIONS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case ROLE_PERMISSIONS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ROLE_PERMISSIONS_LOAD_SUCCESS: {
      return saveRolePermissions(state, action);
    }
    default:
      return state;
  }
}
