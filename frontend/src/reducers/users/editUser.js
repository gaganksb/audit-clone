import { patchAuditUser, patchFO, patchUNHCR, patchPartnerUser, patchOtherUser } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';

export const PATCH_USER = 'PATCH_USER';

export const editUser = (id, body, userType) => (dispatch) => {
  dispatch(loadStarted(PATCH_USER));
  return patchUser(id, body, userType)
    .then((user) => {
      dispatch(loadEnded(PATCH_USER));
      dispatch(loadSuccess(PATCH_USER));
      return user;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_USER));
      dispatch(loadFailure(PATCH_USER, error));
      throw error;
    });
};

function patchUser(id, body, userType) {
  switch(userType) {
    case 'audit': return patchAuditUser(id, body);
    case 'field': return patchFO(id, body);
    case 'partner': return patchPartnerUser(id, body);
    case 'unhcr': return patchUNHCR(id, body);
    case 'other': return patchOtherUser(id, body);
    case 'deactivate': return patchOtherUser(id, body);
    default: return null;
  }
}