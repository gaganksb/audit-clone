import R from 'ramda';
import { getUsersList, exportUsersApi, uploadImportDataApi } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const USERS_LOAD_STARTED = 'USERS_LOAD_STARTED';
export const USERS_LOAD_SUCCESS = 'USERS_LOAD_SUCCESS';
export const USERS_LOAD_FAILURE = 'USERS_LOAD_FAILURE';
export const USERS_LOAD_ENDED = 'USERS_LOAD_ENDED';
export const CLEAR_USER_LIST = 'CLEAR_USER_LIST';

export const usersLoadStarted = () => ({ type: USERS_LOAD_STARTED });
export const usersLoadSuccess = response => ({ type: USERS_LOAD_SUCCESS, response });
export const usersLoadFailure = error => ({ type: USERS_LOAD_FAILURE, error });
export const usersLoadEnded = () => ({ type: USERS_LOAD_ENDED });
export const clearUsersListSuccess = response => ({ type: CLEAR_USER_LIST, response });

const saveUsers = (state, action) => {
  if (action === "clear") {
    const users = R.assoc('users', [], state);
    return R.assoc('totalCount', 0, users);
  } else {
    const users = R.assoc('users', action.response.results, state);
    return R.assoc('totalCount', action.response.count, users);
  }
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  users: [],
};


export const loadUsersList = params => (dispatch) => {
  dispatch(usersLoadStarted());
  return getUsersList(params)
    .then((users) => {
      dispatch(usersLoadEnded());
      dispatch(usersLoadSuccess(users));
      return users;
    })
    .catch((error) => {
      dispatch(usersLoadEnded());
      dispatch(usersLoadFailure(error));
    });
};

export const exportUsers = () => (dispatch) => {
  dispatch(usersLoadStarted());
  return exportUsersApi()
    .then((response) => {
      dispatch(usersLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(usersLoadEnded());
      throw error;
    });
};

export const uploadUsers = (body) => (dispatch) => {
  dispatch(usersLoadStarted());
  return uploadImportDataApi(body)
    .then((response) => {
      dispatch(usersLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(usersLoadEnded());
      throw error;
    });
};

export const clearUserList = params => (dispatch) => {
  dispatch(clearUsersListSuccess());
}

export default function usersListReducer(state = initialState, action) {
  switch (action.type) {
    case USERS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case USERS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case USERS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case USERS_LOAD_SUCCESS: {
      return saveUsers(state, action);
    }
    case CLEAR_USER_LIST: {
      return saveUsers([], 'clear');
    }
    default:
      return state;
  }
}
