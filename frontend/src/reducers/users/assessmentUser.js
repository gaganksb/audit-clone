import R from 'ramda';
import { getUsersList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const USERS_LOAD_STARTED = 'USERS_LOAD_STARTED';
export const USERS_LOAD_FAILURE = 'USERS_LOAD_FAILURE';
export const USERS_LOAD_ENDED = 'USERS_LOAD_ENDED';
export const ASSSESSMENT_LOAD_SUCCESS = 'ASSSESSMENT_LOAD_SUCCESS';
export const PARTNER_LOAD_SUCCESS = 'PARTNER_LOAD_SUCCESS';

export const usersLoadStarted = () => ({ type: USERS_LOAD_STARTED });
export const assessmentLoadSuccess = response => ({ type: ASSSESSMENT_LOAD_SUCCESS, response });
export const partnerLoadSuccess = response => ({ type: PARTNER_LOAD_SUCCESS, response })
export const usersLoadFailure = error => ({ type: USERS_LOAD_FAILURE, error });
export const usersLoadEnded = () => ({ type: USERS_LOAD_ENDED });

const saveAssessmentUsers = (state, action) => {
  const assessmentUser = R.assoc('assessmentUser', action.response.results, state); 
  return R.assoc('totalCount', action.response.count, assessmentUser);
}

const savePartnerUsers = (state, action) => {
  const partnerUser = R.assoc('partnerUser', action.response.results, state); 
  return R.assoc('totalCount', action.response.count, partnerUser);
}

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  assessmentUser: [],
  partnerUser: []
};


export const loadUsersList = params => (dispatch) => {
  dispatch(usersLoadStarted());
  return getUsersList(params)
    .then((users) => {
        dispatch(usersLoadEnded());
      if(params.hasOwnProperty('my_own')) {
        dispatch(assessmentLoadSuccess(users));
      }
      else if(params.hasOwnProperty('partner')) {
        dispatch(partnerLoadSuccess(users));
      }
  })
    .catch((error) => {
      dispatch(usersLoadEnded());
      dispatch(usersLoadFailure(error));
    });
};

export default function usersListReducer(state = initialState, action) {
  switch (action.type) {
    case USERS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case USERS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case USERS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ASSSESSMENT_LOAD_SUCCESS: {
      return saveAssessmentUsers(state, action);
    }
    case PARTNER_LOAD_SUCCESS: {
       return savePartnerUsers(state, action)
    }
    default:
      return state;
  }
}
