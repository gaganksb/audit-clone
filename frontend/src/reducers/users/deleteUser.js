import { combineReducers } from 'redux';
import { deleteUser } from '../../helpers/api/api';
import { errorToBeAdded } from '../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';

export const DELETE_USER = 'DELETE_USER';
const errorMsg = 'Unable to delete User';

export const deleteUserRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_USER));
  return deleteUser(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_USER));
      dispatch(loadSuccess(DELETE_USER));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_USER));
      dispatch(loadFailure(DELETE_USER, error));
      dispatch(errorToBeAdded(error, 'userDelete', errorMsg));
    });
};
