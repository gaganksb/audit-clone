import { deleteDocument } from '../../helpers/api/api';
import { errorToBeAdded } from '../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';

export const DELETE_DOCUMENT = 'DELETE_DOCUMENT';
const errorMsg = 'Unable to delete Document';

export const deleteDocumentRequest = (projectId, id, params) => (dispatch) => {
  dispatch(loadStarted(DELETE_DOCUMENT));
  return deleteDocument(projectId, id, params)
    .then((response) => {
      dispatch(loadEnded(DELETE_DOCUMENT));
      dispatch(loadSuccess(DELETE_DOCUMENT));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_DOCUMENT));
      dispatch(loadFailure(DELETE_DOCUMENT, error));
      dispatch(errorToBeAdded(error, 'DocumentDelete', errorMsg));
    });
};