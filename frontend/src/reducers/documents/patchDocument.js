import { patchDocuments } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../apiMeta';

export const loadFile = (pdojectId, id, file) => (dispatch) => {
  const formData = new FormData();
  
  for (let i = 0; i < file.common_file.length; i++) {
    formData.append('agreement', file.agreement);
    formData.append('common_file', file.common_file[i]);
  }

  dispatch(loadStarted(formData));

  return patchDocuments(pdojectId, id, formData)
    .then((response) => {
      dispatch(loadEnded(formData));
      dispatch(loadSuccess(response));
      
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(formData));
      dispatch(loadFailure(error));
      throw error;
    });
};