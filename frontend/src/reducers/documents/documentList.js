import R from 'ramda';
import { getUploadedDocuments } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const DOCUMENT_LOAD_STARTED = 'DOCUMENT_LOAD_STARTED';
export const DOCUMENT_LOAD_SUCCESS = 'DOCUMENT_LOAD_SUCCESS';
export const DOCUMENT_LOAD_FAILURE = 'DOCUMENT_LOAD_FAILURE';
export const DOCUMENT_LOAD_ENDED = 'DOCUMENT_LOAD_ENDED';

export const documentLoadStarted = () => ({ type: DOCUMENT_LOAD_STARTED });
export const documentLoadSuccess = response => ({ type: DOCUMENT_LOAD_SUCCESS, response });
export const documentLoadFailure = error => ({ type: DOCUMENT_LOAD_FAILURE, error });
export const documentLoadEnded = () => ({ type: DOCUMENT_LOAD_ENDED });

const saveDocument = (state, action) => {
  return R.assoc('document', action.response, state);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  loading: false,
  document: [],
};


export const loadDocumentList = (id) => (dispatch) => {
  dispatch(documentLoadStarted());
  return getUploadedDocuments(id)
    .then((document) => {
      dispatch(documentLoadEnded());
      dispatch(documentLoadSuccess(document));
    })
    .catch((error) => {
      dispatch(documentLoadEnded());
      dispatch(documentLoadFailure(error));
    });
};

export default function documentListReducer(state = initialState, action) {
  switch (action.type) {
    case DOCUMENT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case DOCUMENT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case DOCUMENT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case DOCUMENT_LOAD_SUCCESS: {
      return saveDocument(state, action);
    }
    default:
      return state;
  }
}
