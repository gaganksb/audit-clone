import R from 'ramda';
import { partnerAssignmentList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const PARTNER_LIST_LOAD_STARTED = 'PARTNER_LIST_LOAD_STARTED';
export const PARTNER_LIST_LOAD_SUCCESS = 'PARTNER_LIST_LOAD_SUCCESS';
export const PARTNER_LIST_LOAD_FAILURE = 'PARTNER_LIST_LOAD_FAILURE';
export const PARTNER_LIST_LOAD_ENDED = 'PARTNER_LIST_LOAD_ENDED';

export const partnerListLoadStarted = () => ({ type: PARTNER_LIST_LOAD_STARTED });
export const partnerListLoadSuccess = response => ({ type: PARTNER_LIST_LOAD_SUCCESS, response });
export const partnerListLoadFailure = error => ({ type: PARTNER_LIST_LOAD_FAILURE, error });
export const partnerListLoadEnded = () => ({ type: PARTNER_LIST_LOAD_ENDED });

const savePartnerAssignmentList = (state, action) => {
  const assignment = R.assoc('assignment', action.response.results, state);
  return R.assoc('totalCount', action.response.count, assignment);
};

const messages = {
  loadFailed: 'Loading partners Assignment failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  assignment: [],
};


export const loadPartnerAssignmentList = (id, params) => (dispatch) => {
  dispatch(partnerListLoadStarted());
  return partnerAssignmentList(id, params)
    .then((success) => {
      dispatch(partnerListLoadEnded());
      dispatch(partnerListLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(partnerListLoadEnded());
      dispatch(partnerListLoadFailure(error));
    });
};

export default function partnerListReducer(state = initialState, action) {
  switch (action.type) {
    case PARTNER_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PARTNER_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PARTNER_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PARTNER_LIST_LOAD_SUCCESS: {
      return savePartnerAssignmentList(state, action);
    }
    default:
      return state;
  }
}
