/* eslint-disable camelcase */
import R from 'ramda';
import { getPartnerDetails } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';


export const PARTNER_LOAD_STARTED = 'PARTNER_LOAD_STARTED';
export const PARTNER_LOAD_SUCCESS = 'PARTNER_LOAD_SUCCESS';
export const PARTNER_LOAD_FAILURE = 'PARTNER_LOAD_FAILURE';
export const PARTNER_LOAD_ENDED = 'PARTNER_LOAD_ENDED';

export const partnerLoadStarted = () => ({ type: PARTNER_LOAD_STARTED });
export const partnerLoadSuccess = response => ({ type: PARTNER_LOAD_SUCCESS, response });
export const partnerLoadFailure = error => ({ type: PARTNER_LOAD_FAILURE, error });
export const partnerLoadEnded = () => ({ type: PARTNER_LOAD_ENDED });

const savePartner = (state, action) => {
  return R.assoc('partner', action.response, state);
};

const messages = {
  loadFailed: 'Loading partner details failed.',
};

const initialState = {
  loading: false,
  partner: [],
};


export const loadPartner = id => (dispatch) => {
  dispatch(partnerLoadStarted());
  return getPartnerDetails(id)
    .then((partner) => {
      dispatch(partnerLoadEnded());
      dispatch(partnerLoadSuccess(partner));
    })
    .catch((error) => {
      dispatch(partnerLoadEnded());
      dispatch(partnerLoadFailure(error));
    });
};

export default function partnerDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case PARTNER_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PARTNER_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PARTNER_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PARTNER_LOAD_SUCCESS: {
      return savePartner(state, action);
    }
    default:
      return state;
  }
}
