import React from 'react';
import Group from '@material-ui/icons/Group';
import DashboardSharp from '@material-ui/icons/DashboardSharp';
import Settings from '@material-ui/icons/Settings';
import ListSharp from '@material-ui/icons/List';
import ListAlt from '@material-ui/icons/ListAltRounded';
import ViewList from '@material-ui/icons/ViewList';
import Assessment from '@material-ui/icons/Assessment';
import AccountBox from '@material-ui/icons/AccountBox';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import QualityAssuranceIcon from "@material-ui/icons/AssignmentTurnedIn";
import DataImportIcon from '@material-ui/icons/AddCircleOutline';
import AccountCircle from "@material-ui/icons/AccountCircle";
import { UNHCR, PARTNER, AUDIT, FIELD, OTHER } from "../helpers/constants";
import DescriptionIcon from "@material-ui/icons/Description";
import store from "../store";
import { SESSION_READY } from "./session";

const initialState = [
  {
    name: "Dashboard",
    link: "/dashboard",
    icon: <DashboardSharp />,
    user_types: [UNHCR, FIELD, AUDIT, PARTNER],
  },
  {
    name: "Project selection",
    icon: <ViewList />,
    user_types: [UNHCR],
    children: [
      {
        name: "Preliminary list",
        icon: <ListAlt />,
        link: "/projects/preliminary",
        user_types: [UNHCR],
      },
      {
        name: "Interim list",
        icon: <ListAlt />,
        link: "/projects/interim",
        user_types: [UNHCR],
      },
      {
        name: "Final list",
        icon: <ListAlt />,
        link: "/projects/final",
        user_types: [UNHCR],
      },
      {
        name: "ICQ",
        icon: <ListAlt />,
        link: "/projects/icq",
        user_types: [UNHCR],
      },
      {
        name: "PQP",
        icon: <ListAlt />,
        link: "/projects/pqp",
        user_types: [UNHCR],
      },
      {
        name: "OIOS",
        icon: <ListAlt />,
        link: "/projects/oios",
        user_types: [UNHCR],
      },
      // {name: 'Summary Stats', icon: <ListAlt />, link: '/projects/summaryStats', user_types: [UNHCR]},
    ],
  },
  {
    name: "Project selection",
    icon: <ListSharp />,
    user_types: [FIELD],
    children: [
      { name: "Interim list", link: "/projects/interim", user_types: [FIELD] },
    ],
  },
  {
    name: "Field Assessment",
    link: "/fieldAssessment",
    icon: <Assessment />,
    user_types: [UNHCR, FIELD],
  },
  {
    name: "ICQ",
    icon: <ListAlt />,
    link: "/projects/icq",
    user_types: [AUDIT],
  },
  { name: "Partners", link: "/partner", icon: <Group />, user_types: [UNHCR] },
  {
    name: "Audit",
    link: "/auditFirms",
    icon: <AccountBox />,
    user_types: [UNHCR],
  },
  {
    name: "Audit",
    link: "/audit/auditManagement",
    icon: <AccountBox />,
    user_types: [AUDIT, PARTNER, FIELD],
  },
  {
    name: "Quality Assurance",
    link: "/quality-assurance",
    icon: <QualityAssuranceIcon />,
    user_types: [UNHCR, AUDIT],
  },
  {
    name: "Data Import",
    link: "/data-import",
    icon: <DataImportIcon />,
    user_types: [UNHCR],
  },
  {
    name: "Resource Library",
    icon: <LocalLibraryIcon />,
    user_types: [UNHCR, AUDIT, PARTNER, FIELD],
  },
  {
    name: "ICQ Summary",
    link: "/icqSummary",
    icon: <DescriptionIcon />,
    user_types: [UNHCR],
  },
  {
    name: "Users management",
    link: "/users",
    icon: <Settings />,
    user_types: [UNHCR, FIELD],
  },
  {
    name: "Users management",
    link: "/users",
    icon: <Settings />,
    user_types: [AUDIT, PARTNER],
    user_role: "ADMIN",
  },
];

export const filterItems = (state, user_type, user_role) => state.filter(item => (item.user_role) ? item.user_role === user_role && item.user_types.includes(user_type) : item.user_types.includes(user_type))

const LOAD_NAV = 'LOAD_NAV'
export const loadNavAction = () => ({ type: LOAD_NAV, user_type: store.getState().userData.data.membership.type });

export const loadNav = () => (dispatch) => {
  return dispatch(loadNavAction());
}

export default function navReducer(state = initialState, action) {
  switch (action.type) {
    case SESSION_READY: {
      return filterItems(initialState, action.getState().session.user_type, action.getState().session.user_role);
    }
    case LOAD_NAV: {
      return filterItems(initialState, action.user_type, action.user_role);
    }
    default:
      return state;
  }
}