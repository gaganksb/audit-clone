import { setDeadline, setDeltaDeadline, } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../apiMeta';

export const SET_DEADLINE = 'SET_DEADLINE';
export const setDeadLine = body => (dispatch) => {
  dispatch(loadStarted(SET_DEADLINE));
  return setDeadline(body)
    .then((success) => {
      dispatch(loadEnded(SET_DEADLINE));
      dispatch(loadSuccess(SET_DEADLINE));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(SET_DEADLINE));
      dispatch(loadFailure(SET_DEADLINE, error));
      throw error;
    });
};


export const setDeltaDeadLineApi = (cycleId, body) => (dispatch) => {
  dispatch(loadStarted(SET_DEADLINE));
  return setDeltaDeadline(cycleId, body)
    .then((success) => {
      dispatch(loadEnded(SET_DEADLINE));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(SET_DEADLINE));
      throw error;
    });
};