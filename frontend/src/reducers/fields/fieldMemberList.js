import R from 'ramda';
import { getFieldMemberList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const FO_MEMBER_LIST_LOAD_STARTED = 'FO_MEMBER_LIST_LOAD_STARTED';
export const FO_MEMBER_LIST_LOAD_SUCCESS = 'FO_MEMBER_LIST_LOAD_SUCCESS';
export const FO_MEMBER_LIST_LOAD_FAILURE = 'FO_MEMBER_LIST_LOAD_FAILURE';
export const FO_MEMBER_LIST_LOAD_ENDED = 'FO_MEMBER_LIST_LOAD_ENDED';

export const fieldMemberListLoadStarted = () => ({ type: FO_MEMBER_LIST_LOAD_STARTED });
export const fieldMemberListLoadSuccess = response => ({ type: FO_MEMBER_LIST_LOAD_SUCCESS, response });
export const fieldMemberListLoadFailure = error => ({ type: FO_MEMBER_LIST_LOAD_FAILURE, error });
export const fieldMemberListLoadEnded = () => ({ type: FO_MEMBER_LIST_LOAD_ENDED });

const saveFieldMemberList = (state, action) => {
    return R.assoc('list', action.response.results, state);
};

const messages = {
  loadFailed: 'Loading field failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};


export const loadFieldMemberList = (id, params) => (dispatch) => {
  dispatch(fieldMemberListLoadStarted());
  return getFieldMemberList(id, params)
    .then((success) => {
      dispatch(fieldMemberListLoadEnded());
      dispatch(fieldMemberListLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(fieldMemberListLoadEnded());
      dispatch(fieldMemberListLoadFailure(error));
    });
};

export default function fieldMemberListReducer(state = initialState, action) {
  switch (action.type) {
    case FO_MEMBER_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case FO_MEMBER_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case FO_MEMBER_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case FO_MEMBER_LIST_LOAD_SUCCESS: {
      return saveFieldMemberList(state, action);
    }
    default:
      return state;
  }
}
