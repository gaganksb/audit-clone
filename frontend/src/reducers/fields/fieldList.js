import R from 'ramda';
import { getFieldList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const FO_LIST_LOAD_STARTED = 'FO_LIST_LOAD_STARTED';
export const FO_LIST_LOAD_SUCCESS = 'FO_LIST_LOAD_SUCCESS';
export const FO_LIST_LOAD_FAILURE = 'FO_LIST_LOAD_FAILURE';
export const FO_LIST_LOAD_ENDED = 'FO_LIST_LOAD_ENDED';

export const fieldListLoadStarted = () => ({ type: FO_LIST_LOAD_STARTED });
export const fieldListLoadSuccess = response => ({ type: FO_LIST_LOAD_SUCCESS, response });
export const fieldListLoadFailure = error => ({ type: FO_LIST_LOAD_FAILURE, error });
export const fieldListLoadEnded = () => ({ type: FO_LIST_LOAD_ENDED });

const saveFieldList = (state, action) => {
  const list = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, list);
};

const messages = {
  loadFailed: 'Loading field failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};


export const loadFieldList = params => (dispatch) => {
  dispatch(fieldListLoadStarted());
  return getFieldList(params)
    .then((success) => {
      dispatch(fieldListLoadEnded());
      dispatch(fieldListLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(fieldListLoadEnded());
      dispatch(fieldListLoadFailure(error));
    });
};

export default function fieldListReducer(state = initialState, action) {
  switch (action.type) {
    case FO_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case FO_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case FO_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case FO_LIST_LOAD_SUCCESS: {
      return saveFieldList(state, action);
    }
    default:
      return state;
  }
}
