import { editRating } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../reducers/apiMeta';

export const EDIT_RATING = 'EDIT_RATING';

export const editAssessmentRating = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(EDIT_RATING));
  return editRating(id, body)
    .then((partner) => {
      dispatch(loadEnded(EDIT_RATING));
      dispatch(loadSuccess(EDIT_RATING));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(EDIT_RATING));
      dispatch(loadFailure(EDIT_RATING, error));
      throw error;
    });
};

