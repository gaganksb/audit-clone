import R from 'ramda';
import { getDeadline } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const DEADLINE_LOAD_STARTED = 'DEADLINE_LOAD_STARTED';
export const DEADLINE_LOAD_SUCCESS = 'DEADLINE_LOAD_SUCCESS';
export const DEADLINE_LOAD_FAILURE = 'DEADLINE_LOAD_FAILURE';
export const DEADLINE_LOAD_ENDED = 'DEADLINE_LOAD_ENDED';

export const deadlineLoadStarted = () => ({ type: DEADLINE_LOAD_STARTED });
export const deadlineLoadSuccess = response => ({ type: DEADLINE_LOAD_SUCCESS, response });
export const deadlineLoadFailure = error => ({ type: DEADLINE_LOAD_FAILURE, error });
export const deadlineLoadEnded = () => ({ type: DEADLINE_LOAD_ENDED });

const saveDeadline = (state, action) => {
  return R.assoc('deadline', action.response.results, state);
};

const messages = {
  loadFailed: 'Loading deadline failed.',
};

const initialState = {
  deadline: ''
};


export const loadDeadLine = params => (dispatch) => {
  dispatch(deadlineLoadStarted());
  return getDeadline(params)
    .then((deadline) => {
      dispatch(deadlineLoadEnded());
      dispatch(deadlineLoadSuccess(deadline));
    })
    .catch((error) => {
      dispatch(deadlineLoadEnded());
      dispatch(deadlineLoadFailure(error));
    });
};

export default function deadlineListReducer(state = initialState, action) {
  switch (action.type) {
    case DEADLINE_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case DEADLINE_LOAD_ENDED: {
      return stopLoading(state);
    }
    case DEADLINE_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case DEADLINE_LOAD_SUCCESS: {
      return saveDeadline(state, action);
    }
    default:
      return state;
  }
}
