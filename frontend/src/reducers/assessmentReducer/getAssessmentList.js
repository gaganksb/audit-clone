import R from 'ramda';
import { getFieldAssessment, deleteFieldAssessment } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const ASSESSMENT_LOAD_STARTED = 'ASSESSMENT_LOAD_STARTED';
export const ASSESSMENT_LOAD_SUCCESS = 'ASSESSMENT_LOAD_SUCCESS';
export const ASSESSMENT_LOAD_FAILURE = 'ASSESSMENT_LOAD_FAILURE';
export const ASSESSMENT_LOAD_ENDED = 'ASSESSMENT_LOAD_ENDED';

export const assessmentLoadStarted = () => ({ type: ASSESSMENT_LOAD_STARTED });
export const assessmentLoadSuccess = response => ({ type: ASSESSMENT_LOAD_SUCCESS, response });
export const assessmentLoadFailure = error => ({ type: ASSESSMENT_LOAD_FAILURE, error });
export const assessmentLoadEnded = () => ({ type: ASSESSMENT_LOAD_ENDED });

const saveAssessment = (state, action) => {
  const assessment = R.assoc('assessment', action.response.results, state);
  return R.assoc('totalCount', action.response.count, assessment);
};

const messages = {
  loadFailed: 'Loading field assessment failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  assessment: [],
};


export const loadAssessmentList = params => (dispatch) => {
  dispatch(assessmentLoadStarted());
  return getFieldAssessment(params)
    .then((assessment) => {
      dispatch(assessmentLoadEnded());
      dispatch(assessmentLoadSuccess(assessment));
    })
    .catch((error) => {
      dispatch(assessmentLoadEnded());
      dispatch(assessmentLoadFailure(error));
    });
};

export const deleteFieldAssessmentApi = params => (dispatch, getState) => {
  dispatch(assessmentLoadStarted());
  return deleteFieldAssessment(params)
    .then((response) => {
      dispatch(assessmentLoadEnded());
      let assessmentFiltered = getState().assessmentList.assessment.filter(a => a.id != params)
      let assessment = {
        results: assessmentFiltered,
        count: assessmentFiltered.length,
      }
      dispatch(assessmentLoadSuccess(assessment));
      return response;
    })
    .catch((error) => {
      dispatch(assessmentLoadEnded());
      throw error;
    });
};

export default function assessmentListReducer(state = initialState, action) {
  switch (action.type) {
    case ASSESSMENT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ASSESSMENT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case ASSESSMENT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ASSESSMENT_LOAD_SUCCESS: {
      return saveAssessment(state, action);
    }
    default:
      return state;
  }
}
