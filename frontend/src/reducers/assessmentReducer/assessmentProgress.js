import R from 'ramda';
import { dayDifference, formatDateForPrint } from '../../helpers/dates';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../apiMeta';

import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

import {
  postFieldAssessmentProgress,
  getFieldAssessmentProgress,
  getFieldOfficeProgress,
  assessmentPostDeadline,
  getFieldAssessmentCanProceed
} from '../../helpers/api/api'

const initialState = {
  loading: false,
  totalCount: 0,
  assessmentProgress: [],
  fieldOfficeProgress: [],
  canProceed: null,
  canProceedHQ: null
};

const messages = {
  loadFailed: "Loading field assessment progress failed."
}

export const NEW_FIELD_ASSESSMENT_PROGRESS = 'NEW_FIELD_ASSESSMENT_PROGRESS';

export const ASSESSMENT_PROGRESS_LOAD_STARTED = 'ASSESSMENT_PROGRESS_LOAD_STARTED';
export const ASSESSMENT_PROGRESS_LOAD_SUCCESS = 'ASSESSMENT_PROGRESS_LOAD_SUCCESS';
export const ASSESSMENT_PROGRESS_LOAD_FAILURE = 'ASSESSMENT_PROGRESS_LOAD_FAILURE';
export const ASSESSMENT_PROGRESS_LOAD_ENDED = 'ASSESSMENT_PROGRESS_LOAD_ENDED';

export const OFFICE_PROGRESS_LOAD_STARTED = 'OFFICE_PROGRESS_LOAD_STARTED';
export const OFFICE_PROGRESS_LOAD_SUCCESS = 'OFFICE_PROGRESS_LOAD_SUCCESS';
export const OFFICE_PROGRESS_LOAD_FAILURE = 'OFFICE_PROGRESS_LOAD_FAILURE';
export const OFFICE_PROGRESS_LOAD_ENDED = 'OFFICE_PROGRESS_LOAD_ENDED';

export const ASSESSMENT_CAN_PROCEED_LOAD_SCUCCESS = "ASSESSMENT_CAN_PROCEED_LOAD_SCUCCESS";
export const ASSESSMENT_CAN_PROCEED_LOAD_FALURE = "ASSESSMENT_CAN_PROCEED_LOAD_FALURE";

export const assessmentProgressLoadStarted = () => ({ type: ASSESSMENT_PROGRESS_LOAD_STARTED });
export const assessmentProgressLoadSuccess = response => ({ type: ASSESSMENT_PROGRESS_LOAD_SUCCESS, response });
export const assessmentProgressLoadFailure = error => ({ type: ASSESSMENT_PROGRESS_LOAD_FAILURE, error });
export const assessmentProgressLoadEnded = () => ({ type: ASSESSMENT_PROGRESS_LOAD_ENDED });

export const officeProgressLoadStarted = () => ({ type: OFFICE_PROGRESS_LOAD_STARTED });
export const officeProgressLoadSuccess = response => ({ type: OFFICE_PROGRESS_LOAD_SUCCESS, response });
export const officeProgressLoadFailure = error => ({ type: OFFICE_PROGRESS_LOAD_FAILURE, error });
export const officeProgressLoadEnded = () => ({ type: OFFICE_PROGRESS_LOAD_ENDED });

export const assessmentCanProceedLoadFailure = error => ({ type: ASSESSMENT_CAN_PROCEED_LOAD_FALURE, error });
export const assessmentCanProceedLoadSuccess = response => ({ type: ASSESSMENT_CAN_PROCEED_LOAD_SCUCCESS, response });

const saveAssessmentProgress = (state, action) => {
  return {
    ...state,
    assessmentProgress: action.response.results
  }
};

const saveOfficeProgress = (state, action) => {
  const assessment = R.assoc('fieldOfficeProgress', action.response.results, state);
  return R.assoc('totalCount', action.response.count, assessment);
};

const saveAssessmentCanProceed = (state, action) => {
  const data = R.assoc('canProceed', action.response.canproceed, state);
  const data2 = R.assoc('canProceedHQ', action.response, data);
  return R.assoc('status', action.response.status, data2);
};

export const loadAssessmentProgress = params => (dispatch) => {
  dispatch(assessmentProgressLoadStarted());
  return getFieldAssessmentProgress(params)
    .then((assessment) => {
      dispatch(assessmentProgressLoadEnded());
      dispatch(assessmentProgressLoadSuccess(assessment));
    })
    .catch((error) => {
      dispatch(assessmentProgressLoadEnded());
      dispatch(assessmentProgressLoadFailure(error));
    });
};

export const loadOfficeProgress = params => (dispatch) => {
  dispatch(officeProgressLoadStarted());
  return getFieldOfficeProgress(params)
    .then((assessment) => {
      dispatch(officeProgressLoadEnded());
      dispatch(officeProgressLoadSuccess(assessment));
    })
    .catch((error) => {
      dispatch(officeProgressLoadEnded());
      dispatch(officeProgressLoadFailure(error));
    });
};

export const checkAssessmentCanProceed = params => (dispatch) => {
  dispatch(assessmentProgressLoadStarted());
  return getFieldAssessmentCanProceed(params)
    .then((assessment) => {
      dispatch(assessmentProgressLoadEnded());
      dispatch(assessmentCanProceedLoadSuccess(assessment));
    })
    .catch((error) => {
      dispatch(assessmentProgressLoadEnded());
      dispatch(assessmentCanProceedLoadFailure(error));
    });
};

export const addFieldAssessmentProgress = (body) => (dispatch, getState) => {
  dispatch(loadStarted(NEW_FIELD_ASSESSMENT_PROGRESS));
  // let date = new Date(Date.now()).toLocaleString().split(',')[0]
  const date = formatDateForPrint(new Date(Date.now()))
  let deadlineDate = getState().deadlineDetails.deadline.deadline_date
  const diffDays = (deadlineDate) ? dayDifference(new Date(deadlineDate), new Date(date)) : null
  if (diffDays && diffDays < 0) {
    return assessmentPostDeadline(body)
      .then((success) => {
        dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
        dispatch(loadSuccess(NEW_FIELD_ASSESSMENT_PROGRESS));
        const params = { budget_ref: body.budget_ref, field_office: body.field_office }
        dispatch(loadAssessmentProgress(params))
        return success;
      })
      .catch((error) => {
        dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
        dispatch(loadFailure(NEW_FIELD_ASSESSMENT_PROGRESS, error));
        throw error;
      });
  }
  else {
    return postFieldAssessmentProgress(body)
      .then((success) => {
        dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
        dispatch(loadSuccess(NEW_FIELD_ASSESSMENT_PROGRESS));
        const params = { budget_ref: body.budget_ref, field_office: body.field_office }
        dispatch(loadAssessmentProgress(params))
        return success;
      })
      .catch((error) => {
        dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
        dispatch(loadFailure(NEW_FIELD_ASSESSMENT_PROGRESS, error));
        throw error;
      });
  }

}

export const startDeltaProjectSelection = (body) => (dispatch) => {
  dispatch(loadStarted(NEW_FIELD_ASSESSMENT_PROGRESS));
  return assessmentPostDeadline(body)
    .then((success) => {
      dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
      dispatch(loadSuccess(NEW_FIELD_ASSESSMENT_PROGRESS));
      const params = { budget_ref: body.budget_ref, field_office: body.field_office }
      dispatch(loadAssessmentProgress(params))
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_FIELD_ASSESSMENT_PROGRESS));
      dispatch(loadFailure(NEW_FIELD_ASSESSMENT_PROGRESS, error));
      throw error;
    });
}

export default function assessmentProgressReducer(state = initialState, action) {
  switch (action.type) {
    case ASSESSMENT_PROGRESS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ASSESSMENT_PROGRESS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case ASSESSMENT_PROGRESS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ASSESSMENT_PROGRESS_LOAD_SUCCESS: {
      return saveAssessmentProgress(state, action);
    }
    case OFFICE_PROGRESS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case OFFICE_PROGRESS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case OFFICE_PROGRESS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case OFFICE_PROGRESS_LOAD_SUCCESS: {
      return saveOfficeProgress(state, action);
    }
    case ASSESSMENT_CAN_PROCEED_LOAD_FALURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ASSESSMENT_CAN_PROCEED_LOAD_SCUCCESS: {
      return saveAssessmentCanProceed(state, action);
    }
    default:
      return state;
  }
}