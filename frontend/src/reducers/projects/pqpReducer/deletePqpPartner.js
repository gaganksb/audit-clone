import { deletePQPPartner } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_PQP_PARTNER = 'DELETE_PQP_PARTNER';
const errorMsg = 'Unable to delete PQP Partner';

export const deletePQPPartnerRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_PQP_PARTNER));
  return deletePQPPartner(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_PQP_PARTNER));
      dispatch(loadSuccess(DELETE_PQP_PARTNER));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_PQP_PARTNER));
      dispatch(loadFailure(DELETE_PQP_PARTNER, error));
      dispatch(errorToBeAdded(error, 'PQPPartnerDelete', errorMsg));
    });
};
