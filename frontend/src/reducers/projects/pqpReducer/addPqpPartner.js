import { postNewPQPPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const NEW_PQP = 'NEW_PQP';

export const addNewPQP = body => (dispatch) => {
  dispatch(loadStarted(NEW_PQP));
  return postNewPQPPartner(body)
    .then((success) => {
      dispatch(loadEnded(NEW_PQP));
      dispatch(loadSuccess(NEW_PQP));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_PQP));
      dispatch(loadFailure(NEW_PQP, error));
      throw error;
    });
};
