import { patchPQPPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_PQP_PARTNER = 'PATCH_PQP_PARTNER';

export const editPQPPartner = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_PQP_PARTNER));
  return patchPQPPartner(id, body)
    .then((partner) => {
      dispatch(loadEnded(PATCH_PQP_PARTNER));
      dispatch(loadSuccess(PATCH_PQP_PARTNER));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_PQP_PARTNER));
      dispatch(loadFailure(PATCH_PQP_PARTNER, error));
      throw error;
    });
};