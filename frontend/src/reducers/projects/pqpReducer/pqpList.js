import R from 'ramda';
import { getPqpList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PQP_LOAD_STARTED = 'PQP_LOAD_STARTED';
export const PQP_LOAD_SUCCESS = 'PQP_LOAD_SUCCESS';
export const PQP_LOAD_FAILURE = 'PQP_LOAD_FAILURE';
export const PQP_LOAD_ENDED = 'PQP_LOAD_ENDED';

export const pqpLoadStarted = () => ({ type: PQP_LOAD_STARTED });
export const pqpLoadSuccess = response => ({ type: PQP_LOAD_SUCCESS, response });
export const pqpLoadFailure = error => ({ type: PQP_LOAD_FAILURE, error });
export const pqpLoadEnded = () => ({ type: PQP_LOAD_ENDED });

const savePqp = (state, action) => {
  const pqp = R.assoc('pqp', action.response.results, state);
  return R.assoc('totalCount', action.response.count, pqp);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  pqp: [],
};


export const loadPqpList = params => (dispatch) => {
  dispatch(pqpLoadStarted());
  return getPqpList(params)
    .then((pqp) => {
      dispatch(pqpLoadEnded());
      dispatch(pqpLoadSuccess(pqp));
    })
    .catch((error) => {
      dispatch(pqpLoadEnded());
      dispatch(pqpLoadFailure(error));
    });
};

export default function pqpListReducer(state = initialState, action) {
  switch (action.type) {
    case PQP_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PQP_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PQP_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PQP_LOAD_SUCCESS: {
      return savePqp(state, action);
    }
    default:
      return state;
  }
}
