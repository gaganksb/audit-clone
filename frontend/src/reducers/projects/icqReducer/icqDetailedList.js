

import R from 'ramda';
import { getIcqDetailedList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const ICQ_LOAD_STARTED = 'ICQ_LOAD_STARTED';
export const ICQ_LOAD_SUCCESS = 'ICQ_LOAD_SUCCESS';
export const ICQ_LOAD_FAILURE = 'ICQ_LOAD_FAILURE';
export const ICQ_LOAD_ENDED = 'ICQ_LOAD_ENDED';

export const icqLoadStarted = () => ({ type: ICQ_LOAD_STARTED });
export const icqLoadSuccess = response => ({ type: ICQ_LOAD_SUCCESS, response });
export const icqLoadFailure = error => ({ type: ICQ_LOAD_FAILURE, error });
export const icqLoadEnded = () => ({ type: ICQ_LOAD_ENDED });

const saveIcq = (state, action) => {
  return R.assoc('icq', action.response, state);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  icq: [],
};


export const loadIcqList = (id, year, params) => (dispatch) => {
  dispatch(icqLoadStarted());
  return getIcqDetailedList(id, year, params)
    .then((icq) => {
      dispatch(icqLoadEnded());
      dispatch(icqLoadSuccess(icq));
    })
    .catch((error) => {
      dispatch(icqLoadEnded());
      dispatch(icqLoadFailure(error));
    });
};

export default function icqDetailListReducer(state = initialState, action) {
  switch (action.type) {
    case ICQ_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ICQ_LOAD_ENDED: {
      return stopLoading(state);
    }
    case ICQ_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ICQ_LOAD_SUCCESS: {
      return saveIcq(state, action);
    }
    default:
      return state;
  }
}
