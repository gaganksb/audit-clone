import { postNewICQPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const NEW_ICQ_PARTNER = 'NEW_ICQ_PARTNER';

export const addNewICQ = body => (dispatch) => {
  
  dispatch(loadStarted(NEW_ICQ_PARTNER));
  return postNewICQPartner(body)
    .then((partner) => {
      dispatch(loadEnded(NEW_ICQ_PARTNER));
      dispatch(loadSuccess(NEW_ICQ_PARTNER));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_ICQ_PARTNER));
      dispatch(loadFailure(NEW_ICQ_PARTNER, error));
      throw error;
    });
};
