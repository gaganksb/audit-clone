import { patchICQPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_ICQ_PARTNER = 'PATCH_ICQ_PARTNER';

export const editICQPartner = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_ICQ_PARTNER));
  return patchICQPartner(id, body)
    .then((partner) => {
      dispatch(loadEnded(PATCH_ICQ_PARTNER));
      dispatch(loadSuccess(PATCH_ICQ_PARTNER));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_ICQ_PARTNER));
      dispatch(loadFailure(PATCH_ICQ_PARTNER, error));
      throw error;
    });
};

