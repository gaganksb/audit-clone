import { deleteICQPartner } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_ICQ_PARTNER = 'DELETE_ICQ_PARTNER';
const errorMsg = 'Unable to delete ICQ Partner';

export const deleteICQPartnerRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_ICQ_PARTNER));
  return deleteICQPartner(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_ICQ_PARTNER));
      dispatch(loadSuccess(DELETE_ICQ_PARTNER));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_ICQ_PARTNER));
      dispatch(loadFailure(DELETE_ICQ_PARTNER, error));
      dispatch(errorToBeAdded(error, 'ICQPartnerDelete', errorMsg));
    });
};
