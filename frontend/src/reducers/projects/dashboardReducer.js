import R from 'ramda';
import { getDashboardDetails } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const DASHBOARD_LOAD_STARTED = 'DASHBOARD_LOAD_STARTED';
export const DASHBOARD_LOAD_SUCCESS = 'DASHBOARD_LOAD_SUCCESS';
export const DASHBOARD_LOAD_FAILURE = 'DASHBOARD_LOAD_FAILURE';
export const DASHBOARD_LOAD_ENDED = 'DASHBOARD_LOAD_ENDED';

export const dashboardLoadStarted = () => ({ type: DASHBOARD_LOAD_STARTED });
export const dashboardLoadSuccess = response => ({ type: DASHBOARD_LOAD_SUCCESS, response });
export const dashboardLoadFailure = error => ({ type: DASHBOARD_LOAD_FAILURE, error });
export const dashboardLoadEnded = () => ({ type: DASHBOARD_LOAD_ENDED });

const saveDashboard = (state, action) => {
  const dashboard = R.assoc('dashboard', action.response, state);
  return R.assoc('totalCount', action.response.count, dashboard);
};

const messages = {
  loadFailed: 'Loading dashboard data failed.',
};

const initialState = {
  loading: false,
  dashboard: [],
};

export const loadDashboard = (year, params) => dispatch => {
  dispatch(dashboardLoadStarted());
  return getDashboardDetails(year, params)
    .then((dashboard) => {
      dispatch(dashboardLoadEnded());
      dispatch(dashboardLoadSuccess(dashboard));
    })
    .catch((error) => {
      dispatch(dashboardLoadEnded());
      dispatch(dashboardLoadFailure(error));
    });
};


export default function  getDashboardDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case DASHBOARD_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case DASHBOARD_LOAD_ENDED: {
      return stopLoading(state);
    }
    case DASHBOARD_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case DASHBOARD_LOAD_SUCCESS: {
      return saveDashboard(state, action);
    }
    default:
      return state;
  }
}