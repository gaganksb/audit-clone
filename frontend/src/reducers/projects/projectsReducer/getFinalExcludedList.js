import R from 'ramda';
import { getExcludedFinalList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const FINAL_EXCLUDED_LOAD_STARTED = 'FINAL_EXCLUDED_LOAD_STARTED';
export const FINAL_EXCLUDED_LOAD_SUCCESS = 'FINAL_EXCLUDED_LOAD_SUCCESS';
export const FINAL_EXCLUDED_LOAD_FAILURE = 'FINAL_EXCLUDED_LOAD_FAILURE';
export const FINAL_EXCLUDED_LOAD_ENDED = 'FINAL_EXCLUDED_LOAD_ENDED';

export const finalExcludedLoadStarted = () => ({ type: FINAL_EXCLUDED_LOAD_STARTED });
export const finalExcludedLoadSuccess = response => ({ type: FINAL_EXCLUDED_LOAD_SUCCESS, response });
export const finalExcludedLoadFailure = error => ({ type: FINAL_EXCLUDED_LOAD_FAILURE, error });
export const finalExcludedLoadEnded = () => ({ type: FINAL_EXCLUDED_LOAD_ENDED });

const saveFinalExcluded = (state, action) => {
  const finalList = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, finalList);
};

const messages = {
  loadFailed: 'Loading FINAL excluded list failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};

export const loadFinalExcludedList = (year, params) => (dispatch) => {
  dispatch(finalExcludedLoadStarted());
  return getExcludedFinalList(year, params)
    .then((success) => {
      dispatch(finalExcludedLoadEnded());
      dispatch(finalExcludedLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(finalExcludedLoadEnded());
      dispatch(finalExcludedLoadFailure(error));
    });
};

export default function  finalExcludedListReducer(state = initialState, action) {
  switch (action.type) {
    case FINAL_EXCLUDED_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case FINAL_EXCLUDED_LOAD_ENDED: {
      return stopLoading(state);
    }
    case FINAL_EXCLUDED_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case FINAL_EXCLUDED_LOAD_SUCCESS: {
      return saveFinalExcluded(state, action);
    }
    default:
      return state;
  }
}
