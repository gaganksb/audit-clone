import R from 'ramda';
import { getFinalList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const FINAL_LOAD_STARTED = 'FINAL_LOAD_STARTED';
export const FINAL_LOAD_SUCCESS = 'FINAL_LOAD_SUCCESS';
export const FINAL_LOAD_FAILURE = 'FINAL_LOAD_FAILURE';
export const FINAL_LOAD_ENDED = 'FINAL_LOAD_ENDED';

export const finalLoadStarted = () => ({ type: FINAL_LOAD_STARTED });
export const finalLoadSuccess = response => ({ type: FINAL_LOAD_SUCCESS, response });
export const finalLoadFailure = error => ({ type: FINAL_LOAD_FAILURE, error });
export const finalLoadEnded = () => ({ type: FINAL_LOAD_ENDED });

const saveFinal = (state, action) => {
  const final = R.assoc('final', action.response.results, state);
  return R.assoc('totalCount', action.response.count, final);
};

const messages = {
  loadFailed: 'Loading final list failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  final: [],
};

export const loadFinalList = (year, params) => (dispatch) => {
  dispatch(finalLoadStarted());
  return getFinalList(year, params)
    .then((final) => {
      dispatch(finalLoadEnded());
      dispatch(finalLoadSuccess(final));
    })
    .catch((error) => {
      dispatch(finalLoadEnded());
      dispatch(finalLoadFailure(error));
    });
};

export default function  finalListReducer(state = initialState, action) {
  switch (action.type) {
    case FINAL_LOAD_FAILURE: {
      return saveErrorMsg(R.merge(state, initialState), action, messages.loadFailed);
    }
    case FINAL_LOAD_ENDED: {
      return stopLoading(state);
    }
    case FINAL_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case FINAL_LOAD_SUCCESS: {
      return saveFinal(state, action);
    }
    default:
      return state;
  }
}
