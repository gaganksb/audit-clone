import R from 'ramda';
import { getLatestCycle } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const LATEST_CYCLE_LOAD_STARTED = 'LATEST_CYCLE_LOAD_STARTED';
export const LATEST_CYCLE_LOAD_SUCCESS = 'LATEST_CYCLE_LOAD_SUCCESS';
export const LATEST_CYCLE_LOAD_FAILURE = 'LATEST_CYCLE_LOAD_FAILURE';
export const LATEST_CYCLE_LOAD_ENDED = 'LATEST_CYCLE_LOAD_ENDED';

export const latestCycleLoadStarted = () => ({ type: LATEST_CYCLE_LOAD_STARTED });
export const latestCycleLoadSuccess = response => ({ type: LATEST_CYCLE_LOAD_SUCCESS, response });
export const latestCycleLoadFailure = error => ({ type: LATEST_CYCLE_LOAD_FAILURE, error });
export const latestCycleLoadEnded = () => ({ type: LATEST_CYCLE_LOAD_ENDED });

const saveLatestCycle = (state, action) => {
  return R.assoc('details', action.response, state);
};

const messages = {
  loadFailed: 'Loading cycle failed.',
};

const initialState = {
  loading: false,
  details: null,
};


export const loadLatestCycle = params => (dispatch) => {
  dispatch(latestCycleLoadStarted());
  return getLatestCycle(params)
    .then((latestCycle) => {
      dispatch(latestCycleLoadEnded());
      dispatch(latestCycleLoadSuccess(latestCycle));
    })
    .catch((error) => {
      dispatch(latestCycleLoadEnded());
      dispatch(latestCycleLoadFailure(error));
    });
};

export default function latestCycleReducer(state = initialState, action) {
  switch (action.type) {
    case LATEST_CYCLE_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case LATEST_CYCLE_LOAD_ENDED: {
      return stopLoading(state);
    }
    case LATEST_CYCLE_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case LATEST_CYCLE_LOAD_SUCCESS: {
      return saveLatestCycle(state, action);
    }
    default:
      return state;
  }
}