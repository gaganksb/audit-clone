import R from 'ramda';
import { getPreviewIncluded } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PREVIEW_INCLUDED_LOAD_STARTED = 'PREVIEW_INCLUDED_LOAD_STARTED';
export const PREVIEW_INCLUDED_LOAD_SUCCESS = 'PREVIEW_INCLUDED_LOAD_SUCCESS';
export const PREVIEW_INCLUDED_LOAD_FAILURE = 'PREVIEW_INCLUDED_LOAD_FAILURE';
export const PREVIEW_INCLUDED_LOAD_ENDED = 'PREVIEW_INCLUDED_LOAD_ENDED';

export const previewIncludedLoadStarted = () => ({ type: PREVIEW_INCLUDED_LOAD_STARTED });
export const previewIncludedLoadSuccess = response => ({ type: PREVIEW_INCLUDED_LOAD_SUCCESS, response });
export const previewIncludedLoadFailure = error => ({ type: PREVIEW_INCLUDED_LOAD_FAILURE, error });
export const previewIncludedLoadEnded = () => ({ type: PREVIEW_INCLUDED_LOAD_ENDED });

const savePreviewIncluded = (state, action) => {
  const items = R.assoc('items', action.response.results, state);
  return R.assoc('totalCount', action.response.count, items);
};

const messages = {
  loadFailed: 'Loading included preview failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  items: [],
  year: null,
};


export const loadPreviewIncluded = (year, params) => (dispatch) => {
  dispatch(previewIncludedLoadStarted());
  return getPreviewIncluded(year, params)
    .then((items) => {
      dispatch(previewIncludedLoadEnded());
      dispatch(previewIncludedLoadSuccess(items));
    })
    .catch((error) => {
      dispatch(previewIncludedLoadEnded());
      dispatch(previewIncludedLoadFailure(error));
    });
};

export default function  previewIncludedReducer(state = initialState, action) {
  switch (action.type) {
    case PREVIEW_INCLUDED_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PREVIEW_INCLUDED_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PREVIEW_INCLUDED_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PREVIEW_INCLUDED_LOAD_SUCCESS: {
      return savePreviewIncluded(state, action);
    }
    default:
      return state;
  }
}
