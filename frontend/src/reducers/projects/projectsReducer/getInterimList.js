import R from 'ramda';
import { getInterimList, getInterimListPro } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const INTERIM_LOAD_STARTED = 'INTERIM_LOAD_STARTED';
export const INTERIM_LOAD_SUCCESS = 'INTERIM_LOAD_SUCCESS';
export const INTERIM_LOAD_FAILURE = 'INTERIM_LOAD_FAILURE';
export const INTERIM_LOAD_ENDED = 'INTERIM_LOAD_ENDED';

export const interimLoadStarted = () => ({ type: INTERIM_LOAD_STARTED });
export const interimLoadSuccess = response => ({ type: INTERIM_LOAD_SUCCESS, response });
export const interimLoadFailure = error => ({ type: INTERIM_LOAD_FAILURE, error });
export const interimLoadEnded = () => ({ type: INTERIM_LOAD_ENDED });

const saveInterim = (state, action) => {
  const interim = R.assoc('interim', action.response.results, state);
  return R.assoc('totalCount', action.response.count, interim);
};

const messages = {
  loadFailed: 'Loading interim partner failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  interim: [],
};

export const loadInterimList = (year, params) => (dispatch) => {
  dispatch(interimLoadStarted());
  return getInterimListPro(year, params)
    .then((interim) => {
      dispatch(interimLoadEnded());
      dispatch(interimLoadSuccess(interim));
    })
    .catch((error) => {
      dispatch(interimLoadEnded());
      dispatch(interimLoadFailure(error));
    });
};

export default function  interimListReducer(state = initialState, action) {
  switch (action.type) {
    case INTERIM_LOAD_FAILURE: {
      return saveErrorMsg(R.merge(state, initialState), action, messages.loadFailed);
    }
    case INTERIM_LOAD_ENDED: {
      return stopLoading(state);
    }
    case INTERIM_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case INTERIM_LOAD_SUCCESS: {
      return saveInterim(state, action);
    }
    default:
      return state;
  }
}
