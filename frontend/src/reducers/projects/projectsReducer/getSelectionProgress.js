import R from 'ramda';
import { getSelectionProgress } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PROGRESS_LOAD_STARTED = 'PROGRESS_LOAD_STARTED';
export const PROGRESS_LOAD_SUCCESS = 'PROGRESS_LOAD_SUCCESS';
export const PROGRESS_LOAD_FAILURE = 'PROGRESS_LOAD_FAILURE';
export const PROGRESS_LOAD_ENDED = 'PROGRESS_LOAD_ENDED';

export const progressLoadStarted = () => ({ type: PROGRESS_LOAD_STARTED });
export const progressLoadSuccess = response => ({ type: PROGRESS_LOAD_SUCCESS, response });
export const progressLoadFailure = error => ({ type: PROGRESS_LOAD_FAILURE, error });
export const progressLoadEnded = () => ({ type: PROGRESS_LOAD_ENDED });

const saveProgress = (state, action) => {
  const saveProgress = R.assoc('progress', action.response.results, state);
  return R.assoc('totalCount', action.response.count, saveProgress);
};

const messages = {
  loadFailed: 'Loading progress failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  progress: [],
};

export const loadSelectionProgressList = (year, params) => (dispatch) => {
  dispatch(progressLoadStarted());
  return getSelectionProgress(year, params)
    .then(activities => {
      dispatch(progressLoadEnded());
      dispatch(progressLoadSuccess(activities));
    })
    .catch(error => {
      dispatch(progressLoadEnded());
      dispatch(progressLoadFailure(error));
    });
};

export default function  selectionProgressReducer(state = initialState, action) {
  switch (action.type) {
    case PROGRESS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PROGRESS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PROGRESS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PROGRESS_LOAD_SUCCESS: {
      return saveProgress(state, action);
    }
    default:
      return state;
  }
}
