import R from 'ramda';
import { getPAList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PA_LOAD_STARTED = 'PA_LOAD_STARTED';
export const PA_LOAD_SUCCESS = 'PA_LOAD_SUCCESS';
export const PA_LOAD_FAILURE = 'PA_LOAD_FAILURE';
export const PA_LOAD_ENDED = 'PA_LOAD_ENDED';

export const paLoadStarted = () => ({ type: PA_LOAD_STARTED });
export const paLoadSuccess = response => ({ type: PA_LOAD_SUCCESS, response });
export const paLoadFailure = error => ({ type: PA_LOAD_FAILURE, error });
export const paLoadEnded = () => ({ type: PA_LOAD_ENDED });

const savePA = (state, action) => {
  const paList = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, paList);
};

const messages = {
  loadFailed: 'Loading PA list failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};

export const loadPAList = (year, params) => (dispatch) => {
  dispatch(paLoadStarted());
  return getPAList(year, params)
    .then((success) => {
      dispatch(paLoadEnded());
      dispatch(paLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(paLoadEnded());
      dispatch(paLoadFailure(error));
    });
};

export default function  paListReducer(state = initialState, action) {
  switch (action.type) {
    case PA_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PA_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PA_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PA_LOAD_SUCCESS: {
      return savePA(state, action);
    }
    default:
      return state;
  }
}
