import { promoteProject } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../apiMeta';

export const MODIFY_PROJECT = 'MODIFY_PROJECT';

export const modifyProject = (year, body) => (dispatch) => {
  dispatch(loadStarted(MODIFY_PROJECT));
  return promoteProject(year, body)
    .then((cycle) => {
      dispatch(loadEnded(MODIFY_PROJECT));
      dispatch(loadSuccess(MODIFY_PROJECT));
      return cycle;
    })
    .catch((error) => {
      dispatch(loadEnded(MODIFY_PROJECT));
      dispatch(loadFailure(MODIFY_PROJECT, error));
      throw error;
    });
};