import { patchCycle } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_CYCLE = 'PATCH_CYCLE';

export const editCycle = (year, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_CYCLE));
  return patchCycle(year, body)
    .then((cycle) => {
      dispatch(loadEnded(PATCH_CYCLE));
      dispatch(loadSuccess(PATCH_CYCLE));
      return cycle;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_CYCLE));
      dispatch(loadFailure(PATCH_CYCLE, error));
      throw error;
    });
};
