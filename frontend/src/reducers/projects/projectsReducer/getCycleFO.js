import R from 'ramda';
import { getInterimListCycleFO } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const CYCLE_FO_LOAD_STARTED = 'CYCLE_FO_LOAD_STARTED';
export const CYCLE_FO_LOAD_SUCCESS = 'CYCLE_FO_LOAD_SUCCESS';
export const CYCLE_FO_LOAD_FAILURE = 'CYCLE_FO_LOAD_FAILURE';
export const CYCLE_FO_LOAD_ENDED = 'CYCLE_FO_LOAD_ENDED';

export const cycleFOLoadStarted = () => ({ type: CYCLE_FO_LOAD_STARTED });
export const cycleFOLoadSuccess = response => ({ type: CYCLE_FO_LOAD_SUCCESS, response });
export const cycleFOLoadFailure = error => ({ type: CYCLE_FO_LOAD_FAILURE, error });
export const cycleFOLoadEnded = () => ({ type: CYCLE_FO_LOAD_ENDED });

const saveCycleFO = (state, action) => {
  return R.assoc('details', action.response, state);
};

const messages = {
  loadFailed: 'Loading field office cycle status failed.',
};

const initialState = {
  loading: false,
  details: null
};


export const loadCycleFO = (year, foId) => (dispatch) => {
  dispatch(cycleFOLoadStarted());
  return getInterimListCycleFO(year, foId)
    .then((success) => {
      dispatch(cycleFOLoadEnded());
      dispatch(cycleFOLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(cycleFOLoadEnded());
      dispatch(cycleFOLoadFailure(error));
    });
};

export default function  cycleFOReducer(state = initialState, action) {
  switch (action.type) {
    case CYCLE_FO_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case CYCLE_FO_LOAD_ENDED: {
      return stopLoading(state);
    }
    case CYCLE_FO_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case CYCLE_FO_LOAD_SUCCESS: {
      return saveCycleFO(state, action);
    }
    default:
      return state;
  }
}