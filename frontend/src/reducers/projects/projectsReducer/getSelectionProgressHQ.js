import R from 'ramda';
import { getSelectionProgressHQ } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const HQ_PROGRESS_LOAD_STARTED = 'HQ_PROGRESS_LOAD_STARTED';
export const HQ_PROGRESS_LOAD_SUCCESS = 'HQ_PROGRESS_LOAD_SUCCESS';
export const HQ_PROGRESS_LOAD_FAILURE = 'HQ_PROGRESS_LOAD_FAILURE';
export const HQ_PROGRESS_LOAD_ENDED = 'HQ_PROGRESS_LOAD_ENDED';

export const progressLoadStarted = () => ({ type: HQ_PROGRESS_LOAD_STARTED });
export const progressLoadSuccess = response => ({ type: HQ_PROGRESS_LOAD_SUCCESS, response });
export const progressLoadFailure = error => ({ type: HQ_PROGRESS_LOAD_FAILURE, error });
export const progressLoadEnded = () => ({ type: HQ_PROGRESS_LOAD_ENDED });

const saveProgress = (state, action) => {
  const saveProgress = R.assoc('results', action.response.results, state);
  const data = R.assoc('totalCount', action.response.count, saveProgress);
  return R.assoc('FIN01', action.response.FIN01, data);
};

const messages = {
  loadFailed: 'Loading progress failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  FIN01: 0,
  results: [],
};

export const loadSelectionProgressListHQ = (year, params) => (dispatch) => {
  dispatch(progressLoadStarted());
  return getSelectionProgressHQ(year, params)
    .then(activities => {
      dispatch(progressLoadEnded());
      dispatch(progressLoadSuccess(activities));
    })
    .catch(error => {
      dispatch(progressLoadEnded());
      dispatch(progressLoadFailure(error));
    });
};

export default function  selectionProgressReducerHQ(state = initialState, action) {
  switch (action.type) {
    case HQ_PROGRESS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case HQ_PROGRESS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case HQ_PROGRESS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case HQ_PROGRESS_LOAD_SUCCESS: {
      return saveProgress(state, action);
    }
    default:
      return state;
  }
}
