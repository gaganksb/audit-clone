import { rollbackInterimList, rollbackFinalList } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const ROLLBACK_INTERIM = 'ROLLBACK_INTERIM';
export const rollbackInterim = (year) => (dispatch) => {
  dispatch(loadStarted(ROLLBACK_INTERIM));
  return rollbackInterimList(year)
    .then((success) => {
      dispatch(loadEnded(ROLLBACK_INTERIM));
      dispatch(loadSuccess(ROLLBACK_INTERIM));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(ROLLBACK_INTERIM));
      dispatch(loadFailure(ROLLBACK_INTERIM, error));
      throw error;
    });
};

export const ROLLBACK_FINAL = 'ROLLBACK_FINAL';
export const rollbackFinal = (year) => (dispatch) => {
  dispatch(loadStarted(ROLLBACK_FINAL));
  return rollbackFinalList(year)
    .then((success) => {
      dispatch(loadEnded(ROLLBACK_FINAL));
      dispatch(loadSuccess(ROLLBACK_FINAL));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(ROLLBACK_FINAL));
      dispatch(loadFailure(ROLLBACK_FINAL, error));
      throw error;
    });
};