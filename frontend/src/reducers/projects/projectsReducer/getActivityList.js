import R from 'ramda';
import { getActivities } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const ACTIVITY_LOAD_STARTED = 'ACTIVITY_LOAD_STARTED';
export const ACTIVITY_LOAD_SUCCESS = 'ACTIVITY_LOAD_SUCCESS';
export const ACTIVITY_LOAD_FAILURE = 'ACTIVITY_LOAD_FAILURE';
export const ACTIVITY_LOAD_ENDED = 'ACTIVITY_LOAD_ENDED';

export const activityLoadStarted = () => ({ type: ACTIVITY_LOAD_STARTED });
export const activityLoadSuccess = response => ({ type: ACTIVITY_LOAD_SUCCESS, response });
export const activityLoadFailure = error => ({ type: ACTIVITY_LOAD_FAILURE, error });
export const activityLoadEnded = () => ({ type: ACTIVITY_LOAD_ENDED });

const saveActivities = (state, action) => {
  const activities = R.assoc('activities', action.response.results, state);
  return R.assoc('totalCount', action.response.count, activities);
};

const messages = {
  loadFailed: 'Loading activity failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  activities: [],
};

export const loadActivityList = (year, params) => (dispatch) => {
  dispatch(activityLoadStarted());
  return getActivities(year, params)
    .then(activities => {
      dispatch(activityLoadEnded());
      dispatch(activityLoadSuccess(activities));
    })
    .catch(error => {
      dispatch(activityLoadEnded());
      dispatch(activityLoadFailure(error));
    });
};

export default function  activityListReducer(state = initialState, action) {
  switch (action.type) {
    case ACTIVITY_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case ACTIVITY_LOAD_ENDED: {
      return stopLoading(state);
    }
    case ACTIVITY_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case ACTIVITY_LOAD_SUCCESS: {
      return saveActivities(state, action);
    }
    default:
      return state;
  }
}
