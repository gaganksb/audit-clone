import { editProjectDetails } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const UPDATE_PROJECT_DETAILS = 'UPDATE_PROJECT_DETAILS';

export const editProject = (id, body) => (dispatch) => {
  dispatch(loadStarted(UPDATE_PROJECT_DETAILS));
  return editProjectDetails(id, body)
    .then((projectDetails) => {
      dispatch(loadEnded(UPDATE_PROJECT_DETAILS));
      dispatch(loadSuccess(UPDATE_PROJECT_DETAILS));
      return projectDetails;
    })
    .catch((error) => {
      dispatch(loadEnded(UPDATE_PROJECT_DETAILS));
      dispatch(loadFailure(UPDATE_PROJECT_DETAILS, error));
      throw error;
    });
};