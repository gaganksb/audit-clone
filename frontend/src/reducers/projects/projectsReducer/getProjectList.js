import R from 'ramda';
import { getAgreementList, importDeltasApi } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PROJECT_LOAD_STARTED = 'PROJECT_LOAD_STARTED';
export const PROJECT_LOAD_SUCCESS = 'PROJECT_LOAD_SUCCESS';
export const PROJECT_LOAD_FAILURE = 'PROJECT_LOAD_FAILURE';
export const PROJECT_LOAD_ENDED = 'PROJECT_LOAD_ENDED';

export const projectLoadStarted = () => ({ type: PROJECT_LOAD_STARTED });
export const projectLoadSuccess = response => ({ type: PROJECT_LOAD_SUCCESS, response });
export const projectLoadFailure = error => ({ type: PROJECT_LOAD_FAILURE, error });
export const projectLoadEnded = () => ({ type: PROJECT_LOAD_ENDED });

const saveProject = (state, action) => {
  const project = R.assoc('project', action.response.results, state);
  return R.assoc('totalCount', action.response.count, project);
};

const messages = {
  loadFailed: 'Loading project partner failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  project: [],
};

export const loadProjectList = (params) => (dispatch) => {
  dispatch(projectLoadStarted());
  return getAgreementList(params)
    .then((project) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadSuccess(project));
    })
    .catch((error) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadFailure(error));
    });
};

export const importDeltas = (body) => (dispatch) => {
  dispatch(projectLoadStarted());
  return importDeltasApi(body)
    .then((response) => {
      dispatch(projectLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(projectLoadEnded());
      throw error;
    });
};

export default function projectListReducer(state = initialState, action) {
  switch (action.type) {
    case PROJECT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PROJECT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PROJECT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PROJECT_LOAD_SUCCESS: {
      return saveProject(state, action);
    }
    default:
      return state;
  }
}
