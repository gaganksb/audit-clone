import R from 'ramda';
import { getPreliminaryStats } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const STATS_LOAD_STARTED = 'STATS_LOAD_STARTED';
export const STATS_LOAD_SUCCESS = 'STATS_LOAD_SUCCESS';
export const STATS_LOAD_FAILURE = 'STATS_LOAD_FAILURE';
export const STATS_LOAD_ENDED = 'STATS_LOAD_ENDED';

export const statsLoadStarted = () => ({ type: STATS_LOAD_STARTED });
export const statsLoadSuccess = response => ({ type: STATS_LOAD_SUCCESS, response });
export const statsLoadFailure = error => ({ type: STATS_LOAD_FAILURE, error });
export const statsLoadEnded = () => ({ type: STATS_LOAD_ENDED });

const saveStats = (state, action) => {
  const stats = R.assoc('stats', action.response, state);
  return R.assoc('totalCount', action.response.count, stats);
};

const messages = {
  loadFailed: 'Loading stats data failed.',
};

const initialState = {
  loading: false,
  stats: [],
};

export const loadPreliminaryStats = (year, params) => dispatch => {
  dispatch(statsLoadStarted());
  return getPreliminaryStats(year, params)
    .then((stats) => {
      dispatch(statsLoadEnded());
      dispatch(statsLoadSuccess(stats));
    })
    .catch((error) => {
      dispatch(statsLoadEnded());
      dispatch(statsLoadFailure(error));
    });
};


export default function  getPreliminaryStatsReducer(state = initialState, action) {
  switch (action.type) {
    case STATS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case STATS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case STATS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case STATS_LOAD_SUCCESS: {
      return saveStats(state, action);
    }
    default:
      return state;
  }
}