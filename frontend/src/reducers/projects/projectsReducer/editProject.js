import { patchProject, patchILProject, patchFLProject } from '../../../helpers/api/api';
import { reset } from 'redux-form';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_PROJECT = 'PATCH_PROJECT';
export const PATCH_IL_PROJECT = 'PATCH_IL_PROJECT';
export const PATCH_FL_PROJECT = 'PATCH_FL_PROJECT';

export const editProject = (year, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_PROJECT));
  return patchProject(year, body)
    .then((project) => {
      dispatch(loadEnded(PATCH_PROJECT));
      dispatch(loadSuccess(PATCH_PROJECT));
      return project;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_PROJECT));
      dispatch(loadFailure(PATCH_PROJECT, error));
      throw error;
    });
};

export const editILProject = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_IL_PROJECT));
  return patchILProject(id, body)
    .then((project) => {
      dispatch(loadEnded(PATCH_IL_PROJECT));
      dispatch(loadSuccess(PATCH_IL_PROJECT));
      return project;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_IL_PROJECT));
      dispatch(loadFailure(PATCH_IL_PROJECT, error));
      throw error;
    });
};

export const editFLProject = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_FL_PROJECT));
  return patchFLProject(id, body)
    .then((project) => {
      dispatch(loadEnded(PATCH_FL_PROJECT));
      dispatch(loadSuccess(PATCH_FL_PROJECT));
      return project;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_FL_PROJECT));
      dispatch(loadFailure(PATCH_FL_PROJECT, error));
      throw error;
    });
};
