import R from 'ramda';
import { getILExcludedList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const IL_EXCLUDED_LOAD_STARTED = 'IL_EXCLUDED_LOAD_STARTED';
export const IL_EXCLUDED_LOAD_SUCCESS = 'IL_EXCLUDED_LOAD_SUCCESS';
export const IL_EXCLUDED_LOAD_FAILURE = 'IL_EXCLUDED_LOAD_FAILURE';
export const IL_EXCLUDED_LOAD_ENDED = 'IL_EXCLUDED_LOAD_ENDED';

export const ILExcludedLoadStarted = () => ({ type: IL_EXCLUDED_LOAD_STARTED });
export const ILExcludedLoadSuccess = response => ({ type: IL_EXCLUDED_LOAD_SUCCESS, response });
export const ILExcludedLoadFailure = error => ({ type: IL_EXCLUDED_LOAD_FAILURE, error });
export const ILExcludedLoadEnded = () => ({ type: IL_EXCLUDED_LOAD_ENDED });

const saveILExcluded = (state, action) => {
  const ILList = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, ILList);
};

const messages = {
  loadFailed: 'Loading IL excluded list failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};

export const loadILExcludedList = (year, params) => (dispatch) => {
  dispatch(ILExcludedLoadStarted());
  return getILExcludedList(year, params)
    .then((success) => {
      dispatch(ILExcludedLoadEnded());
      dispatch(ILExcludedLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(ILExcludedLoadEnded());
      dispatch(ILExcludedLoadFailure(error));
    });
};

export default function  ILExcludedListReducer(state = initialState, action) {
  switch (action.type) {
    case IL_EXCLUDED_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case IL_EXCLUDED_LOAD_ENDED: {
      return stopLoading(state);
    }
    case IL_EXCLUDED_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case IL_EXCLUDED_LOAD_SUCCESS: {
      return saveILExcluded(state, action);
    }
    default:
      return state;
  }
}
