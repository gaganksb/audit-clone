import { remindFO } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const SEND_REMINDER = 'SEND_REMINDER';
export const sendFOReminder = (body) => (dispatch) => {
  dispatch(loadStarted(SEND_REMINDER));
  return remindFO(body)
    .then((success) => {
      dispatch(loadEnded(SEND_REMINDER));
      dispatch(loadSuccess(SEND_REMINDER));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(SEND_REMINDER));
      dispatch(loadFailure(SEND_REMINDER, error));
      throw error;
    });
};