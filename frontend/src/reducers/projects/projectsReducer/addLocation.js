

import { patchLocations } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_LOCATION = 'PATCH_LOCATION';

export const editLocation = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_LOCATION));
  return patchLocations(id, body)
    .then((location) => {
      dispatch(loadEnded(PATCH_LOCATION));
      dispatch(loadSuccess(PATCH_LOCATION));
      return location;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_LOCATION));
      dispatch(loadFailure(PATCH_LOCATION, error));
      throw error;
    });
};
