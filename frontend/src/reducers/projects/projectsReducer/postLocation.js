import { addLocations } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const NEW_LOCATION = 'NEW_LOCATION';

export const newLocations = (body) => (dispatch) => {
  dispatch(loadStarted(NEW_LOCATION));
  return addLocations(body)
    .then((success) => {
      dispatch(loadEnded(NEW_LOCATION));
      dispatch(loadSuccess(NEW_LOCATION));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_LOCATION));
      dispatch(loadFailure(NEW_LOCATION, error));
      throw error;
    });
};
