import { patchFinalProject } from '../../../helpers/api/api';
import { reset } from 'redux-form';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_FINAL_PROJECT = 'PATCH_FINAL_PROJECT';

export const editFinalProject = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_FINAL_PROJECT));
  return patchFinalProject(id, body)
    .then((finalProject) => {
      dispatch(loadEnded(PATCH_FINAL_PROJECT));
      dispatch(loadSuccess(PATCH_FINAL_PROJECT));
      dispatch(reset('addFinalPADialog'));
      return finalProject;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_FINAL_PROJECT));
      dispatch(loadFailure(PATCH_FINAL_PROJECT, error));
      throw error;
    });
};

