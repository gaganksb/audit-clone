/* eslint-disable camelcase */
import R from 'ramda';
import { getProjectDetails, getProjectDetailsExp } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';


export const PROJECT_LOAD_STARTED = 'PROJECT_LOAD_STARTED';
export const PROJECT_LOAD_SUCCESS = 'PROJECT_LOAD_SUCCESS';
export const PROJECT_LOAD_FAILURE = 'PROJECT_LOAD_FAILURE';
export const PROJECT_LOAD_ENDED = 'PROJECT_LOAD_ENDED';

export const projectLoadStarted = () => ({ type: PROJECT_LOAD_STARTED });
export const projectLoadSuccess = response => ({ type: PROJECT_LOAD_SUCCESS, response });
export const projectLoadFailure = error => ({ type: PROJECT_LOAD_FAILURE, error });
export const projectLoadEnded = () => ({ type: PROJECT_LOAD_ENDED });

const saveProject = (state, action) => {
  return R.assoc('project', action.response, state);
};

const messages = {
  loadFailed: 'Loading project details failed.',
};

const initialState = {
  loading: false,
  project: [],
};


export const loadProject = id => (dispatch) => {
  dispatch(projectLoadStarted());
  return getProjectDetails(id)
    .then((project) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadSuccess(project));
    })
    .catch((error) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadFailure(error));
    });
};

export const loadProjectExp = id => (dispatch) => {
  dispatch(projectLoadStarted());
  return getProjectDetailsExp(id)
    .then((project) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadSuccess(project));
    })
    .catch((error) => {
      dispatch(projectLoadEnded());
      dispatch(projectLoadFailure(error));
    });
};

export default function projectDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case PROJECT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PROJECT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PROJECT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PROJECT_LOAD_SUCCESS: {
      return saveProject(state, action);
    }
    default:
      return state;
  }
}
