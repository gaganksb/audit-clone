import R from 'ramda';
import { getSettings } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const SETTING_LOAD_STARTED = 'SETTING_LOAD_STARTED';
export const SETTING_LOAD_SUCCESS = 'SETTING_LOAD_SUCCESS';
export const SETTING_LOAD_FAILURE = 'SETTING_LOAD_FAILURE';
export const SETTING_LOAD_ENDED = 'SETTING_LOAD_ENDED';

export const settingLoadStarted = () => ({ type: SETTING_LOAD_STARTED });
export const settingLoadSuccess = response => ({ type: SETTING_LOAD_SUCCESS, response });
export const settingLoadFailure = error => ({ type: SETTING_LOAD_FAILURE, error });
export const settingLoadEnded = () => ({ type: SETTING_LOAD_ENDED });

const saveSetting = (state, action) => {
  const details = R.assoc('details', action.response, state);
  return R.assoc('totalCount', action.response.count, details);
};

const messages = {
  loadFailed: 'Loading settings failed.',
};

const initialState = {
  loading: false,
  details: [],
};

export const loadSettings = (params) => dispatch => {
  dispatch(settingLoadStarted());
  return getSettings(params)
    .then((setting) => {
      dispatch(settingLoadEnded());
      dispatch(settingLoadSuccess(setting));
    })
    .catch((error) => {
      dispatch(settingLoadEnded());
      dispatch(settingLoadFailure(error));
    });
};


export default function  getSettingsReducer(state = initialState, action) {
  switch (action.type) {
    case SETTING_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case SETTING_LOAD_ENDED: {
      return stopLoading(state);
    }
    case SETTING_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case SETTING_LOAD_SUCCESS: {
      return saveSetting(state, action);
    }
    default:
      return state;
  }
}