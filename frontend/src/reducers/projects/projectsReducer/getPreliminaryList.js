import R from 'ramda';
import { getPreliminaryList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PRELIMINARY_LOAD_STARTED = 'PRELIMINARY_LOAD_STARTED';
export const PRELIMINARY_LOAD_SUCCESS = 'PRELIMINARY_LOAD_SUCCESS';
export const PRELIMINARY_LOAD_FAILURE = 'PRELIMINARY_LOAD_FAILURE';
export const PRELIMINARY_LOAD_ENDED = 'PRELIMINARY_LOAD_ENDED';

export const preliminaryLoadStarted = () => ({ type: PRELIMINARY_LOAD_STARTED });
export const preliminaryLoadSuccess = response => ({ type: PRELIMINARY_LOAD_SUCCESS, response });
export const preliminaryLoadFailure = error => ({ type: PRELIMINARY_LOAD_FAILURE, error });
export const preliminaryLoadEnded = () => ({ type: PRELIMINARY_LOAD_ENDED });

const savePreliminary = (state, action) => {
  const preliminary = R.assoc('preliminary', action.response.results, state);
  return R.assoc('totalCount', action.response.count, preliminary);
};

const messages = {
  loadFailed: 'Loading preliminary partner failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  preliminary: [],
};

export const loadPreliminaryList = (year, params) => (dispatch) => {
  dispatch(preliminaryLoadStarted());
  return getPreliminaryList(year, params)
    .then((preliminary) => {
      dispatch(preliminaryLoadEnded());
      dispatch(preliminaryLoadSuccess(preliminary));
    })
    .catch((error) => {
      dispatch(preliminaryLoadEnded());
      dispatch(preliminaryLoadFailure(error));
    });
};

export default function  preliminaryListReducer(state = initialState, action) {
  switch (action.type) {
    case PRELIMINARY_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PRELIMINARY_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PRELIMINARY_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PRELIMINARY_LOAD_SUCCESS: {
      return savePreliminary(state, action);
    }
    default:
      return state;
  }
}
