import { postProjectProgressHQ } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../apiMeta';

export const MODIFY_PROJECT_HQ = 'MODIFY_PROJECT_HQ';

export const modifyProjectHQ = (year, body) => (dispatch) => {
  dispatch(loadStarted(MODIFY_PROJECT_HQ));
  return postProjectProgressHQ(year, body)
    .then((res) => {
      dispatch(loadEnded(MODIFY_PROJECT_HQ));
      dispatch(loadSuccess(MODIFY_PROJECT_HQ));
      return res;
    })
    .catch((error) => {
      dispatch(loadEnded(MODIFY_PROJECT_HQ));
      dispatch(loadFailure(MODIFY_PROJECT_HQ, error));
      throw error;
    });
};