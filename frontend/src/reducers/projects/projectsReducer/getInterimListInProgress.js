import R from 'ramda';
import { getInterimListInProgress } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const INTERIM_IP_LOAD_STARTED = 'INTERIM_IP_LOAD_STARTED';
export const INTERIM_IP_LOAD_SUCCESS = 'INTERIM_IP_LOAD_SUCCESS';
export const INTERIM_IP_LOAD_FAILURE = 'INTERIM_IP_LOAD_FAILURE';
export const INTERIM_IP_LOAD_ENDED = 'INTERIM_IP_LOAD_ENDED';

export const interimIPLoadStarted = () => ({ type: INTERIM_IP_LOAD_STARTED });
export const interimIPLoadSuccess = response => ({ type: INTERIM_IP_LOAD_SUCCESS, response });
export const interimIPLoadFailure = error => ({ type: INTERIM_IP_LOAD_FAILURE, error });
export const interimIPLoadEnded = () => ({ type: INTERIM_IP_LOAD_ENDED });

const saveInterim = (state, action) => {
  const interim = R.assoc('interim', action.response.results, state);
  return R.assoc('totalCount', action.response.total_projects, interim);
};

const messages = {
  loadFailed: 'Loading interim partner in progress failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  interim: [],
};

export const loadInterimListInProgress = (year, params) => (dispatch) => {
  dispatch(interimIPLoadStarted());
  return getInterimListInProgress(year, params)
    .then((interim) => {
      dispatch(interimIPLoadEnded());
      dispatch(interimIPLoadSuccess(interim));
    })
    .catch((error) => {
      dispatch(interimIPLoadEnded());
      dispatch(interimIPLoadFailure(error));
    });
};

export default function  interimListInProgressReducer(state = initialState, action) {
  switch (action.type) {
    case INTERIM_IP_LOAD_FAILURE: {
      return saveErrorMsg(R.merge(state, initialState), action, messages.loadFailed);
    }
    case INTERIM_IP_LOAD_ENDED: {
      return stopLoading(state);
    }
    case INTERIM_IP_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case INTERIM_IP_LOAD_SUCCESS: {
      return saveInterim(state, action);
    }
    default:
      return state;
  }
}
