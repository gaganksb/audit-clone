import R from 'ramda';
import { getPAExcludedList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PA_EXCLUDED_LOAD_STARTED = 'PA_EXCLUDED_LOAD_STARTED';
export const PA_EXCLUDED_LOAD_SUCCESS = 'PA_EXCLUDED_LOAD_SUCCESS';
export const PA_EXCLUDED_LOAD_FAILURE = 'PA_EXCLUDED_LOAD_FAILURE';
export const PA_EXCLUDED_LOAD_ENDED = 'PA_EXCLUDED_LOAD_ENDED';

export const paExcludedLoadStarted = () => ({ type: PA_EXCLUDED_LOAD_STARTED });
export const paExcludedLoadSuccess = response => ({ type: PA_EXCLUDED_LOAD_SUCCESS, response });
export const paExcludedLoadFailure = error => ({ type: PA_EXCLUDED_LOAD_FAILURE, error });
export const paExcludedLoadEnded = () => ({ type: PA_EXCLUDED_LOAD_ENDED });

const savePAExcluded = (state, action) => {
  const paList = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, paList);
};

const messages = {
  loadFailed: 'Loading PA excluded list failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
};

export const loadPAExcludedList = (year, params) => (dispatch) => {
  dispatch(paExcludedLoadStarted());
  return getPAExcludedList(year, params)
    .then((success) => {
      dispatch(paExcludedLoadEnded());
      dispatch(paExcludedLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(paExcludedLoadEnded());
      dispatch(paExcludedLoadFailure(error));
    });
};

export default function  paExcludedListReducer(state = initialState, action) {
  switch (action.type) {
    case PA_EXCLUDED_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PA_EXCLUDED_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PA_EXCLUDED_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PA_EXCLUDED_LOAD_SUCCESS: {
      return savePAExcluded(state, action);
    }
    default:
      return state;
  }
}
