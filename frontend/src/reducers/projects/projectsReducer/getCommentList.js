import R from 'ramda';
import { getComments } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const COMMENT_LOAD_STARTED = 'COMMENT_LOAD_STARTED';
export const COMMENT_LOAD_SUCCESS = 'COMMENT_LOAD_SUCCESS';
export const COMMENT_LOAD_FAILURE = 'COMMENT_LOAD_FAILURE';
export const COMMENT_LOAD_ENDED = 'COMMENT_LOAD_ENDED';

export const commentLoadStarted = () => ({ type: COMMENT_LOAD_STARTED });
export const commentLoadSuccess = response => ({ type: COMMENT_LOAD_SUCCESS, response });
export const commentLoadFailure = error => ({ type: COMMENT_LOAD_FAILURE, error });
export const commentLoadEnded = () => ({ type: COMMENT_LOAD_ENDED });

const saveComments = (state, action) => {
  const comments = R.assoc('comments', action.response.results, state);
  return R.assoc('totalCount', action.response.count, comments);
};

const messages = {
  loadFailed: 'Loading comments failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  comments: [],
};

export const loadCommentList = (year, params) => (dispatch) => {
  dispatch(commentLoadStarted());
  return getComments(year, params)
    .then((comments) => {
      dispatch(commentLoadEnded());
      dispatch(commentLoadSuccess(comments));
    })
    .catch((error) => {
      dispatch(commentLoadEnded());
      dispatch(commentLoadFailure(error));
    });
};

export default function  commentListReducer(state = initialState, action) {
  switch (action.type) {
    case COMMENT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case COMMENT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case COMMENT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case COMMENT_LOAD_SUCCESS: {
      return saveComments(state, action);
    }
    default:
      return state;
  }
}
