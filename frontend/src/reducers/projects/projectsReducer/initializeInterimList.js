import { initializeInterimList, initializeFinalList, initializeFinalListFO } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const INITIALIZE_INTERIM = 'INITIALIZE_INTERIM';
export const initializeInterim = (year) => (dispatch) => {
  dispatch(loadStarted(INITIALIZE_INTERIM));
  return initializeInterimList(year)
    .then((success) => {
      dispatch(loadEnded(INITIALIZE_INTERIM));
      dispatch(loadSuccess(INITIALIZE_INTERIM));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(INITIALIZE_INTERIM));
      dispatch(loadFailure(INITIALIZE_INTERIM, error));
      throw error;
    });
};


export const INITIALIZED_FINAL = 'INITIALIZED_FINAL';
export const initializeFinal = (year) => (dispatch) => {
  dispatch(loadStarted(INITIALIZED_FINAL));
  return initializeFinalList(year)
    .then((success) => {
      dispatch(loadEnded(INITIALIZED_FINAL));
      dispatch(loadSuccess(INITIALIZED_FINAL));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(INITIALIZED_FINAL));
      dispatch(loadFailure(INITIALIZED_FINAL, error));
      throw error;
    });
};

export const INITIALIZED_FINAL_FO = 'INITIALIZED_FINAL_FO';
export const initializeFinalFO = (year, foId) => (dispatch) => {
  dispatch(loadStarted(INITIALIZED_FINAL_FO));
  return initializeFinalListFO(year, foId)
    .then((success) => {
      dispatch(loadEnded(INITIALIZED_FINAL_FO));
      dispatch(loadSuccess(INITIALIZED_FINAL_FO));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(INITIALIZED_FINAL_FO));
      dispatch(loadFailure(INITIALIZED_FINAL_FO, error));
      throw error;
    });
};