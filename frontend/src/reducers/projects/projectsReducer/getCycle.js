import R from 'ramda';
import { getCycle } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const CYCLE_LOAD_STARTED = 'CYCLE_LOAD_STARTED';
export const CYCLE_LOAD_SUCCESS = 'CYCLE_LOAD_SUCCESS';
export const CYCLE_LOAD_FAILURE = 'CYCLE_LOAD_FAILURE';
export const CYCLE_LOAD_ENDED = 'CYCLE_LOAD_ENDED';

export const cycleLoadStarted = () => ({ type: CYCLE_LOAD_STARTED });
export const cycleLoadSuccess = response => ({ type: CYCLE_LOAD_SUCCESS, response });
export const cycleLoadFailure = error => ({ type: CYCLE_LOAD_FAILURE, error });
export const cycleLoadEnded = () => ({ type: CYCLE_LOAD_ENDED });

const saveCycle = (state, action) => {
  const finalized = R.assoc('finalized', action.response.preliminary_list_finalized, state);
  return R.assoc('details', action.response, finalized);
};

const messages = {
  loadFailed: 'Loading cycles failed.',
};

const initialState = {
  loading: false,
  details: null,
  finalized: null,
};


export const loadCycle = (year, params) => (dispatch) => {
  dispatch(cycleLoadStarted());
  return getCycle(year, params)
    .then((success) => {
      dispatch(cycleLoadEnded());
      dispatch(cycleLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(cycleLoadEnded());
      dispatch(cycleLoadFailure(error));
    });
};

export default function  cycleReducer(state = initialState, action) {
  switch (action.type) {
    case CYCLE_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case CYCLE_LOAD_ENDED: {
      return stopLoading(state);
    }
    case CYCLE_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case CYCLE_LOAD_SUCCESS: {
      return saveCycle(state, action);
    }
    default:
      return state;
  }
}