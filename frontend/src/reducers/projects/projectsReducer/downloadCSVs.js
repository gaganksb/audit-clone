import R from 'ramda';
import { downloadCsv, downloadFieldAssessment } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const CSV_DOWNLOAD_STARTED = 'CSV_DOWNLOAD_STARTED';
export const CSV_LOAD_SUCCESS = 'CSV_LOAD_SUCCESS';
export const CSV_LOAD_FAILURE = 'CSV_LOAD_FAILURE';
export const CSV_LOAD_ENDED = 'CSV_LOAD_ENDED';

export const csvLoadStarted = () => ({ type: CSV_DOWNLOAD_STARTED });
export const csvLoadSuccess = response => ({ type: CSV_LOAD_SUCCESS, response });
export const csvLoadFailure = error => ({ type: CSV_LOAD_FAILURE, error });
export const csvLoadEnded = () => ({ type: CSV_LOAD_ENDED });

const saveCSV = (state, action) => {
  return R.assoc('csv', action.response, state);
};

const messages = {
  loadFailed: 'Loading csvs failed.',
};

const initialState = {
  loading: false,
  csv: null,
};


export const loadCsv = (year, type) => (dispatch) => {
  dispatch(csvLoadStarted());
  return downloadCsv(year, type)
    .then((success) => {
      dispatch(csvLoadEnded());
      dispatch(csvLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(csvLoadEnded());
      dispatch(csvLoadFailure(error));
    });
};

export const downloadFieldAssessmentCSV = (year) => (dispatch) => {
  dispatch(csvLoadStarted());
  return downloadFieldAssessment(year)
    .then((success) => {
      dispatch(csvLoadEnded());
      dispatch(csvLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(csvLoadEnded());
      dispatch(csvLoadFailure(error));
    });
};

export default function  csvReducer(state = initialState, action) {
  switch (action.type) {
    case CSV_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case CSV_LOAD_ENDED: {
      return stopLoading(state);
    }
    case CSV_DOWNLOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case CSV_LOAD_SUCCESS: {
      return saveCSV(state, action);
    }
    default:
      return state;
  }
}