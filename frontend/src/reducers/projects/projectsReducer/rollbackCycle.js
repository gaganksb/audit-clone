import { rollbackCycle } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_CYCLE_ROLLBACK = 'PATCH_CYCLE_ROLLBACK';

export const cycleRollback = (year, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_CYCLE_ROLLBACK));
  return rollbackCycle(year, body)
    .then((cycle) => {
      dispatch(loadEnded(PATCH_CYCLE_ROLLBACK));
      dispatch(loadSuccess(PATCH_CYCLE_ROLLBACK));
      return cycle;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_CYCLE_ROLLBACK));
      dispatch(loadFailure(PATCH_CYCLE_ROLLBACK, error));
      throw error;
    });
};