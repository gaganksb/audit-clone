import R from 'ramda';
import { getPreviewExcluded } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PREVIEW_EXCLUDED_LOAD_STARTED = 'PREVIEW_EXCLUDED_LOAD_STARTED';
export const PREVIEW_EXCLUDED_LOAD_SUCCESS = 'PREVIEW_EXCLUDED_LOAD_SUCCESS';
export const PREVIEW_EXCLUDED_LOAD_FAILURE = 'PREVIEW_EXCLUDED_LOAD_FAILURE';
export const PREVIEW_EXCLUDED_LOAD_ENDED = 'PREVIEW_EXCLUDED_LOAD_ENDED';

export const previewExcludedLoadStarted = () => ({ type: PREVIEW_EXCLUDED_LOAD_STARTED });
export const previewExcludedLoadSuccess = response => ({ type: PREVIEW_EXCLUDED_LOAD_SUCCESS, response });
export const previewExcludedLoadFailure = error => ({ type: PREVIEW_EXCLUDED_LOAD_FAILURE, error });
export const previewExcludedLoadEnded = () => ({ type: PREVIEW_EXCLUDED_LOAD_ENDED });

const savePreviewExcluded = (state, action) => {
  const items = R.assoc('items', action.response.results, state);
  return R.assoc('totalCount', action.response.count, items);
};

const messages = {
  loadFailed: 'Loading excluded preview failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  items: [],
  year: null,
};


export const loadPreviewExcluded = (year, params) => (dispatch) => {
  dispatch(previewExcludedLoadStarted());
  return getPreviewExcluded(year, params)
    .then((items) => {
      dispatch(previewExcludedLoadEnded());
      dispatch(previewExcludedLoadSuccess(items));
    })
    .catch((error) => {
      dispatch(previewExcludedLoadEnded());
      dispatch(previewExcludedLoadFailure(error));
    });
};

export default function  previewExcludedReducer(state = initialState, action) {
  switch (action.type) {
    case PREVIEW_EXCLUDED_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PREVIEW_EXCLUDED_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PREVIEW_EXCLUDED_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PREVIEW_EXCLUDED_LOAD_SUCCESS: {
      return savePreviewExcluded(state, action);
    }
    default:
      return state;
  }
}
