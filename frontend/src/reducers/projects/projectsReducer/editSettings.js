import { patchSettings } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_SETTINGS = 'PATCH_SETTINGS';

export const editSettings = (year, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_SETTINGS));
  return patchSettings(year, body)
    .then((settings) => {
      dispatch(loadEnded(PATCH_SETTINGS));
      dispatch(loadSuccess(PATCH_SETTINGS));
      return settings;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_SETTINGS));
      dispatch(loadFailure(PATCH_SETTINGS, error));
      throw error;
    });
};