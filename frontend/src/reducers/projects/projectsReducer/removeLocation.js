
import { deleteLocations } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_LOCATION = 'DELETE_LOCATION';
const errorMsg = 'Unable to delete Location';

export const deleteLocationRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_LOCATION));
  return deleteLocations(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_LOCATION));
      dispatch(loadSuccess(DELETE_LOCATION));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_LOCATION));
      dispatch(loadFailure(DELETE_LOCATION, error));
      dispatch(errorToBeAdded(error, 'locationDelete', errorMsg));
    });
};
