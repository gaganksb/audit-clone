import R from 'ramda';
import { getMessages } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const MESSAGE_LOAD_STARTED = 'MESSAGE_LOAD_STARTED';
export const MESSAGE_LOAD_SUCCESS = 'MESSAGE_LOAD_SUCCESS';
export const MESSAGE_LOAD_FAILURE = 'MESSAGE_LOAD_FAILURE';
export const MESSAGE_LOAD_ENDED = 'MESSAGE_LOAD_ENDED';

export const messageLoadStarted = () => ({ type: MESSAGE_LOAD_STARTED });
export const messageLoadSuccess = response => ({ type: MESSAGE_LOAD_SUCCESS, response });
export const messageLoadFailure = error => ({ type: MESSAGE_LOAD_FAILURE, error });
export const messageLoadEnded = () => ({ type: MESSAGE_LOAD_ENDED });

const saveMessage = (state, action) => {
  const message = R.assoc('message', action.response.results, state);
  return R.assoc('totalCount', action.response.count, message);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  message: [],
};


export const loadMessages = (id, params) => (dispatch) => {
  dispatch(messageLoadStarted());
  return getMessages(id, params)
    .then((message) => {
      dispatch(messageLoadEnded());
      dispatch(messageLoadSuccess(message));
    })
    .catch((error) => {
      dispatch(messageLoadEnded());
      dispatch(messageLoadFailure(error));
    });
};

export default function messageListReducer(state = initialState, action) {
  switch (action.type) {
    case MESSAGE_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case MESSAGE_LOAD_ENDED: {
      return stopLoading(state);
    }
    case MESSAGE_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case MESSAGE_LOAD_SUCCESS: {
      return saveMessage(state, action);
    }
    default:
      return state;
  }
}
