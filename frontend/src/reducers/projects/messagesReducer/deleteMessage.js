import { deleteMessages } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_MESSAGE = 'DELETE_MESSAGE';
const errorMsg = 'Unable to delete Message';

export const deleteMessageRequest = (id, msgId) => (dispatch) => {
  dispatch(loadStarted(DELETE_MESSAGE));
  return deleteMessages(id, msgId)
    .then((response) => {
      dispatch(loadEnded(DELETE_MESSAGE));
      dispatch(loadSuccess(DELETE_MESSAGE));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_MESSAGE));
      dispatch(loadFailure(DELETE_MESSAGE, error));
      dispatch(errorToBeAdded(error, 'DeleteFailError', errorMsg));
    });
};
