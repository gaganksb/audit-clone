import { editMessages } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';
  import { reset } from 'redux-form';

export const PATCH_MESSAGES = 'PATCH_MESSAGES';

export const editMessage = (rowId, id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_MESSAGES));
  return editMessages(rowId, id, body)
    .then((message) => {
      dispatch(loadEnded(PATCH_MESSAGES));
      dispatch(loadSuccess(PATCH_MESSAGES));
      dispatch(reset('editMessageDialog'));
      return message;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_MESSAGES));
      dispatch(loadFailure(PATCH_MESSAGES, error));
      throw error;
    });
};