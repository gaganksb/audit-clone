import { postMessages } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';
  import { reset } from 'redux-form';

export const NEW_MESSAGE = 'NEW_MESSAGE';

export const addNewMessage = (id, body) => (dispatch) => {
  dispatch(loadStarted(NEW_MESSAGE));
  return postMessages(id, body)
    .then((message) => {
      dispatch(loadEnded(NEW_MESSAGE));
      dispatch(loadSuccess(NEW_MESSAGE));
      dispatch(reset('messageDialog'));
      return message;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_MESSAGE));
      dispatch(loadFailure(NEW_MESSAGE, error));
      throw error;
    });
};