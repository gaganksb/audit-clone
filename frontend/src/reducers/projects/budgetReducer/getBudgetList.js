import R from 'ramda';
import { budgetList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const BUDGET_LOAD_STARTED = 'BUDGET_LOAD_STARTED';
export const BUDGET_LOAD_SUCCESS = 'BUDGET_LOAD_SUCCESS';
export const BUDGET_LOAD_FAILURE = 'BUDGET_LOAD_FAILURE';
export const BUDGET_LOAD_ENDED = 'BUDGET_LOAD_ENDED';

export const budgetLoadStarted = () => ({ type: BUDGET_LOAD_STARTED });
export const budgetLoadSuccess = response => ({ type: BUDGET_LOAD_SUCCESS, response });
export const budgetLoadFailure = error => ({ type: BUDGET_LOAD_FAILURE, error });
export const budgetLoadEnded = () => ({ type: BUDGET_LOAD_ENDED });

const saveBudget = (state, action) => {
  const budget = R.assoc('budget', action.response.results, state);
  return R.assoc('totalCount', action.response.count, budget);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  budget: [],
};


export const loadBudgetList = (id, params) => (dispatch) => {
  dispatch(budgetLoadStarted());
  return budgetList(id, params)
    .then((budget) => {
      dispatch(budgetLoadEnded());
      dispatch(budgetLoadSuccess(budget));
    })
    .catch((error) => {
      dispatch(budgetLoadEnded());
      dispatch(budgetLoadFailure(error));
    });
};

export default function budgetListReducer(state = initialState, action) {
  switch (action.type) {
    case BUDGET_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case BUDGET_LOAD_ENDED: {
      return stopLoading(state);
    }
    case BUDGET_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case BUDGET_LOAD_SUCCESS: {
      return saveBudget(state, action);
    }
    default:
      return state;
  }
}
