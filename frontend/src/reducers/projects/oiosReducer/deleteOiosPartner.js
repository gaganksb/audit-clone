import { deleteOIOSPartner } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_OIOS_PARTNER = 'DELETE_OIOS_PARTNER';
const errorMsg = 'Unable to delete OIOS Partner';

export const deleteOIOSPartnerRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_OIOS_PARTNER));
  return deleteOIOSPartner(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_OIOS_PARTNER));
      dispatch(loadSuccess(DELETE_OIOS_PARTNER));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_OIOS_PARTNER));
      dispatch(loadFailure(DELETE_OIOS_PARTNER, error));
      dispatch(errorToBeAdded(error, 'OIOSPartnerDelete', errorMsg));
    });
};
