import { postNewOIOSPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const NEW_OIOS = 'NEW_OIOS';

export const addNewOIOS = body => (dispatch) => {
  dispatch(loadStarted(NEW_OIOS));
  return postNewOIOSPartner(body)
    .then((success) => {
      dispatch(loadEnded(NEW_OIOS));
      dispatch(loadSuccess(NEW_OIOS));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_OIOS));
      dispatch(loadFailure(NEW_OIOS, error));
      throw error;
    });
};
