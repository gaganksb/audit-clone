import R from 'ramda';
import { getBUList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const BU_LOAD_STARTED = 'BU_LOAD_STARTED';
export const BU_LOAD_SUCCESS = 'BU_LOAD_SUCCESS';
export const BU_LOAD_FAILURE = 'BU_LOAD_FAILURE';
export const BU_LOAD_ENDED = 'BU_LOAD_ENDED';

export const BULoadStarted = () => ({ type: BU_LOAD_STARTED });
export const BULoadSuccess = response => ({ type: BU_LOAD_SUCCESS, response });
export const BULoadFailure = error => ({ type: BU_LOAD_FAILURE, error });
export const BULoadEnded = () => ({ type: BU_LOAD_ENDED });

const saveBU = (state, action) => {
  const bu = R.assoc('bu', action.response.results, state);
  return R.assoc('totalCount', action.response.count, bu);
};

const messages = {
  loadFailed: 'Loading BU failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  bu: [],
};


export const loadBUList = params => (dispatch) => {
  dispatch(BULoadStarted());
  return getBUList(params)
    .then((oios) => {
      dispatch(BULoadEnded());
      dispatch(BULoadSuccess(oios));
    })
    .catch((error) => {
      dispatch(BULoadEnded());
      dispatch(BULoadFailure(error));
    });
};

export default function BUListReducer(state = initialState, action) {
  switch (action.type) {
    case BU_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case BU_LOAD_ENDED: {
      return stopLoading(state);
    }
    case BU_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case BU_LOAD_SUCCESS: {
      return saveBU(state, action);
    }
    default:
      return state;
  }
}
