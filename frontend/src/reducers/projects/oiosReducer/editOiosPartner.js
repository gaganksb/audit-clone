import { patchOIOSPartner } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const PATCH_OIOS_PARTNER = 'PATCH_OIOS_PARTNER';

export const editOIOSPartner = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_OIOS_PARTNER));
  return patchOIOSPartner(id, body)
    .then((partner) => {
      dispatch(loadEnded(PATCH_OIOS_PARTNER));
      dispatch(loadSuccess(PATCH_OIOS_PARTNER));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_OIOS_PARTNER));
      dispatch(loadFailure(PATCH_OIOS_PARTNER, error));
      throw error;
    });
};