import R from 'ramda';
import { getOiosList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const OIOS_LOAD_STARTED = 'OIOS_LOAD_STARTED';
export const OIOS_LOAD_SUCCESS = 'OIOS_LOAD_SUCCESS';
export const OIOS_LOAD_FAILURE = 'OIOS_LOAD_FAILURE';
export const OIOS_LOAD_ENDED = 'OIOS_LOAD_ENDED';

export const oiosLoadStarted = () => ({ type: OIOS_LOAD_STARTED });
export const oiosLoadSuccess = response => ({ type: OIOS_LOAD_SUCCESS, response });
export const oiosLoadFailure = error => ({ type: OIOS_LOAD_FAILURE, error });
export const oiosLoadEnded = () => ({ type: OIOS_LOAD_ENDED });

const saveOios = (state, action) => {
  const oios = R.assoc('oios', action.response.results, state);
  return R.assoc('totalCount', action.response.count, oios);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  columns: [
    { name: 'fullname', title: 'Name' },
    { name: 'email', title: 'E-mail' },
  ],
  loading: false,
  totalCount: 0,
  oios: [],
};


export const loadOiosList = params => (dispatch) => {
  dispatch(oiosLoadStarted());
  return getOiosList(params)
    .then((oios) => {
      dispatch(oiosLoadEnded());
      dispatch(oiosLoadSuccess(oios));
    })
    .catch((error) => {
      dispatch(oiosLoadEnded());
      dispatch(oiosLoadFailure(error));
    });
};

export default function oiosListReducer(state = initialState, action) {
  switch (action.type) {
    case OIOS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case OIOS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case OIOS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case OIOS_LOAD_SUCCESS: {
      return saveOios(state, action);
    }
    default:
      return state;
  }
}
