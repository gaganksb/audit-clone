import R from 'ramda';
import { getCompletionStatus } from '../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from './apiStatus';

export const BGJOB_LOAD_STARTED = 'BGJOB_LOAD_STARTED';
export const BGJOB_LOAD_SUCCESS = 'BGJOB_LOAD_SUCCESS';
export const BGJOB_LOAD_FAILURE = 'BGJOB_LOAD_FAILURE';
export const BGJOB_LOAD_ENDED = 'BGJOB_LOAD_ENDED';

export const bgJobLoadStarted = () => ({ type: BGJOB_LOAD_STARTED });
export const bgJobLoadSuccess = response => ({ type: BGJOB_LOAD_SUCCESS, response });
export const bgJobLoadFailure = error => ({ type: BGJOB_LOAD_FAILURE, error });
export const bgJobLoadEnded = () => ({ type: BGJOB_LOAD_ENDED });

const saveBgJob = (state, action) => {
  return R.assoc('bgJob', action.response, state);
};

const messages = {
  loadFailed: 'Loading bgJob failed.',
};

const initialState = {
  bgJob: null,
};


export const loadBgJob = (params) => (dispatch) => {
  dispatch(bgJobLoadStarted());
  return getCompletionStatus(params)
    .then((bgJob) => {
      dispatch(bgJobLoadEnded());
      dispatch(bgJobLoadSuccess(bgJob));
    })
    .catch((error) => {
      dispatch(bgJobLoadEnded());
      dispatch(bgJobLoadFailure(error));
    });
};

export default function bgJobListReducer(state = initialState, action) {
  switch (action.type) {
    case BGJOB_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case BGJOB_LOAD_ENDED: {
      return stopLoading(state);
    }
    case BGJOB_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case BGJOB_LOAD_SUCCESS: {
      return saveBgJob(state, action);
    }
    default:
      return state;
  }
}
