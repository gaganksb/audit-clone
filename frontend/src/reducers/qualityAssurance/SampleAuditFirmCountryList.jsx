import R, { props } from "ramda";
import {
  postSampleAuditFirmCountriesList,
  postConfirmAuditFirmCountriesList,
} from "../../helpers/api/api";

import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from "../apiStatus";

export const SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_STARTED =
  "SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_STARTED";
export const SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_SUCCESS =
  "SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_SUCCESS";
export const SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_FAILURE =
  "SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_FAILURE";
export const SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_ENDED =
  "SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_ENDED";
export const SAMPLE_CLEAR_AUDIT_FIRM_COUNTRY =
  "SAMPLE_CLEAR_AUDIT_FIRM_COUNTRY";

export const sampleAuditFirmCountryLoadStarted = () => ({
  type: SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_STARTED,
});
export const sampleAuditFirmCountryLoadSuccess = (response) => ({
  type: SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_SUCCESS,
  response,
});
export const sampleAuditFirmCountryLoadFailure = (error) => ({
  type: SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_FAILURE,
  error,
});
export const sampleAuditFirmCountryLoadEnded = () => ({
  type: SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_ENDED,
});
export const clearSampleAuditFirmCountrySuccess = (response) => ({
  type: SAMPLE_CLEAR_AUDIT_FIRM_COUNTRY,
  response,
});

const saveSampleAuditFirmCountries = (state, action) => {
  if (action === "clear") {
    const sampleAuditFirmCountries = R.assoc(
      "sampleAuditFirmCountries",
      [],
      state
    );
    return R.assoc("totalCount", 0, sampleAuditFirmCountries);
  } else {
    const sampleAuditFirmCountries = R.assoc(
      "sampleAuditFirmCountries",
      action.response.sampleAuditFirmCountries,
      state
    );
    return R.assoc(
      "totalCount",
      action.response.count,
      sampleAuditFirmCountries
    );
  }
};

const messages = {
  loadFailed: "Loading audit firm with countries failed.",
};

const initialState = {
  loading: false,
  sampleAuditFirmCountries: [],
};

const processSampledAuditFirmCountryList = (auditFirmCountries) => {
  let auditFirmCountryList = [];
  if (auditFirmCountries) {
    auditFirmCountries.map((record) => {
      let auditFirmId = record.audit_firm.id;
      let auditFirmName = record.audit_firm.legal_name;
      let countries = "";
      let countryIds = [];
      if (record.countries) {
        let countryArray = [];
        record.countries.map((country) => {
          countryArray.push(country.name);
          countryIds.push(country.id);
        });
        
        countries = countryArray
          .map((record, index) => (index % 5 == 0 ? record : "\n" + record))
          .toString();
      }

      auditFirmCountryList.push({
        auditFirmId,
        auditFirmName,
        countries,
        countryIds,
      });
    });
  }
  return auditFirmCountryList;
};

export const confirmedAuditFirmCountriesList = (year, body) => (dispatch) => {
  dispatch(sampleAuditFirmCountryLoadStarted());
  return postConfirmAuditFirmCountriesList(year, body)
    .then((response) => {
      dispatch(sampleAuditFirmCountryLoadEnded());
    })
    .catch((error) => {
      dispatch(sampleAuditFirmCountryLoadEnded());
      dispatch(sampleAuditFirmCountryLoadFailure(error));
    });
};

export const loadSampleAuditFirmCountriesList = (params) => (dispatch) => {
  dispatch(clearSampleAuditFirmCountriesList());
  dispatch(sampleAuditFirmCountryLoadStarted());
  return postSampleAuditFirmCountriesList(params)
    .then((auditFirmCountries) => {
      let auditFirmCountryList =
        processSampledAuditFirmCountryList(auditFirmCountries);
      dispatch(
        sampleAuditFirmCountryLoadSuccess({
          sampleAuditFirmCountries: auditFirmCountryList,
        })
      );
      dispatch(sampleAuditFirmCountryLoadEnded());
    })
    .catch((error) => {
      dispatch(sampleAuditFirmCountryLoadEnded());
      dispatch(sampleAuditFirmCountryLoadFailure(error));
    });
};

export const clearSampleAuditFirmCountriesList = (params) => (dispatch) => {
  dispatch(clearSampleAuditFirmCountrySuccess());
};

export default function auditFirmCountriesListReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_ENDED: {
      return stopLoading(state);
    }
    case SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case SAMPLE_AUDIT_FIRM_COUNTRY_LOAD_SUCCESS: {
      return saveSampleAuditFirmCountries(state, action);
    }
    case SAMPLE_CLEAR_AUDIT_FIRM_COUNTRY: {
      return saveSampleAuditFirmCountries([], "clear");
    }
    default:
      return state;
  }
}
