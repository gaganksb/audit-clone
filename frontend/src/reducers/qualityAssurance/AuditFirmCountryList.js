import R from 'ramda';
import {
  getAuditFirmCountriesList,
  patchKpi4TeamCompositionReport,
  getKpi4TeamCompositionReport,
  addImprovementArea,
  updateImprovementArea,
  deleteImprovementArea,
  getKpi4TeamCompositionCountryCriterias,
  sendTeamCompositionNotification,
  updateMemberProfile,
  getPPAbyMemberId,
  copyPPAprofiles,
  getTeamCompositionMemebrsByNameOrRole,
} from "../../helpers/api/api";

import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from "../apiStatus";

export const AUDIT_FIRM_COUNTRY_LOAD_STARTED =
  "AUDIT_FIRM_COUNTRY_LOAD_STARTED";
export const AUDIT_FIRM_COUNTRY_LOAD_SUCCESS =
  "AUDIT_FIRM_COUNTRY_LOAD_SUCCESS";
export const AUDIT_FIRM_COUNTRY_LOAD_FAILURE =
  "AUDIT_FIRM_COUNTRY_LOAD_FAILURE";
export const AUDIT_FIRM_COUNTRY_LOAD_ENDED = "AUDIT_FIRM_COUNTRY_LOAD_ENDED";
export const CLEAR_AUDIT_FIRM_COUNTRY = "CLEAR_AUDIT_FIRM_COUNTRY";
export const UPDATE_AUDIT_FIRM_COUNTRY = "UPDATE_AUDIT_FIRM_COUNTRY";

export const auditFirmCountryLoadStarted = () => ({
  type: AUDIT_FIRM_COUNTRY_LOAD_STARTED,
});
export const auditFirmCountryLoadSuccess = (response) => ({
  type: AUDIT_FIRM_COUNTRY_LOAD_SUCCESS,
  response,
});
export const auditFirmCountryLoadFailure = (error) => ({
  type: AUDIT_FIRM_COUNTRY_LOAD_FAILURE,
  error,
});
export const auditFirmCountryLoadEnded = () => ({
  type: AUDIT_FIRM_COUNTRY_LOAD_ENDED,
});
export const clearAuditFirmCountrySuccess = (response) => ({
  type: CLEAR_AUDIT_FIRM_COUNTRY,
  response,
});

export const updateAuditFirmCountry = (response) => ({
  type: UPDATE_AUDIT_FIRM_COUNTRY,
  response,
});

const saveAuditFirmCountries = (state, action) => {
  if (action === "clear") {
    const auditFirmCountries = R.assoc("auditFirmCountries", [], state);
    return R.assoc("totalCount", 0, auditFirmCountries);
  } else {
    const auditFirmCountries = R.assoc(
      "auditFirmCountries",
      action.response.auditFirmCountries,
      state
    );
    return R.assoc("totalCount", action.response.count, auditFirmCountries);
  }
};

const updateTeamComposition = (state, action) => {
  let currentAuditFirmCountries = state.auditFirmCountries;
  currentAuditFirmCountries = currentAuditFirmCountries.map((record) => {
    if (record.id == action.response.id) {
      record = response;
    }
    return record;
  });
  const auditFirmCountries = R.assoc(
    "auditFirmCountries",
    currentAuditFirmCountries,
    state
  );
  return R.assoc("totalCount", action.response.count, auditFirmCountries);
};

const messages = {
  loadFailed: "Loading audit firm with countries failed.",
};

const initialState = {
  totalCount: 0,
  loading: false,
  auditFirmCountries: [],
  error: {}
};

const processCurrentAuditFirmCountryList = (auditFirmCountriesResults) => {
  let auditFirmCountryList = [];
  if (auditFirmCountriesResults) {
    if (
      auditFirmCountriesResults.results &&
      auditFirmCountriesResults.results.length > 0
    ) {
      auditFirmCountriesResults.results.map((record) => {
        if (record.team_composition_users && record.team_composition_users.length == 0) {
          record.team_composition_users = [];
          record.team_composition_users.push({ fullname: "John Doe" });
          record.team_composition_users.push({ fullname: "Steve Smith" });
        }
        auditFirmCountryList.push({
          ...record,
        });
      });
    }
  }
  return auditFirmCountryList;
};

export const getTeamCompositionUsers = (teamCompositionId) => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return getTeamCompositionUsers(teamCompositionId)
    .then((response) => {
      let users = response.team_composition_users;
      dispatch(auditFirmCountryLoadEnded());
      return users;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
    });
};

export const getTeamCompositionCountryCriteriasAPI = () => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return getKpi4TeamCompositionCountryCriterias()
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response.results;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
    });
};

export const sendTeamCompositionNotificationAPI = (params) => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return sendTeamCompositionNotification(params)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      return error;
    });
};

export const getCurrentAuditFirmCountriesList = (params) => (dispatch) => {
  dispatch(clearAuditFirmCountriesList());
  dispatch(auditFirmCountryLoadStarted());
  return getAuditFirmCountriesList(params)
    .then((response) => {
      let auditFirmCountryList = processCurrentAuditFirmCountryList(response);
      dispatch(
        auditFirmCountryLoadSuccess({
          auditFirmCountries: auditFirmCountryList,
          totalCount: response.count,
        })
      );
      dispatch(auditFirmCountryLoadEnded());
      return auditFirmCountryList;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
    });
};

export const getTeamCompositionReport = (id) => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return getKpi4TeamCompositionReport(id)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
    });
};

export const addNewImprovementArea = (body) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return addImprovementArea(body)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      throw error;
    });
};

export const updateNewImprovementArea = (id, body) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return updateImprovementArea(id, body)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      throw error;
    });
};

export const deleteNewImprovementArea = (id) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return deleteImprovementArea(id)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      throw error;
    });
};

export const saveTeamCompositionReport = (id, body) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return patchKpi4TeamCompositionReport(id, body)
    .then((response) => {
      dispatch(
        updateAuditFirmCountry({
          response,
        })
      );
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      throw error;
    });
};

export const updateMemberProfileApi = (id, body) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return updateMemberProfile(id, body)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      dispatch(auditFirmCountryLoadFailure(error));
      throw error;
    });
};

export const getPPAbyMemberIdApi = (id, year) => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return getPPAbyMemberId(id, year)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      throw error;
    });
};

export const copyProfilesApi = (body) => dispatch => {
  dispatch(auditFirmCountryLoadStarted());
  return copyPPAprofiles(body)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      throw error;
    });
};

export const getTeamCompositionMembersByNameRole = (id, year, name, role) => (dispatch) => {
  dispatch(auditFirmCountryLoadStarted());
  return getTeamCompositionMemebrsByNameOrRole(id, year, name, role)
    .then((response) => {
      dispatch(auditFirmCountryLoadEnded());
      return response.results;
    })
    .catch((error) => {
      dispatch(auditFirmCountryLoadEnded());
      throw error;
    });
};

export const clearAuditFirmCountriesList = (params) => (dispatch) => {
  dispatch(clearAuditFirmCountrySuccess());
};

export default function auditFirmCountriesListReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case AUDIT_FIRM_COUNTRY_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case AUDIT_FIRM_COUNTRY_LOAD_ENDED: {
      return stopLoading(state);
    }
    case AUDIT_FIRM_COUNTRY_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case AUDIT_FIRM_COUNTRY_LOAD_SUCCESS: {
      return saveAuditFirmCountries(state, action);
    }
    case UPDATE_AUDIT_FIRM_COUNTRY: {
      return updateTeamComposition(state, action);
    }
    case CLEAR_AUDIT_FIRM_COUNTRY: {
      return saveAuditFirmCountries([], "clear");
    }
    default:
      return state;
  }
}
