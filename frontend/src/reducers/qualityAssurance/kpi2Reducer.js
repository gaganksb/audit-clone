import R from 'ramda';
import {
  requestNewWorkingPaper,
  requestUpdateWorkingPaper,
  uploadWorkingPaper,
  deleteWorkingPaper,
  startKpi2Survey,
  getKpi2AuditFirms,
  checkIfAuditWorkCompleted,
  updateFinalReport,
  updateSummaryReport,
  getKpi2FinalReport,
  getKpi2SummaryReport,
  checkSurveyUploadStatus,
  getKpi2KeyFindings,
} from '../../helpers/api/api';

import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from "../apiStatus";

export const KPI2_LOAD_STARTED =
  "KPI2_LOAD_STARTED";
export const KPI2_LOAD_SUCCESS =
  "KPI2_LOAD_SUCCESS";
export const KPI2_LOAD_WORKING_PAPER =
  "KPI2_LOAD_WORKING_PAPER";
export const KPI2_LOAD_FAILURE =
  "KPI2_LOAD_FAILURE";
export const KPI2_LOAD_ENDED = "KPI2_LOAD_ENDED";

export const kpi2LoadStarted = () => ({
  type: KPI2_LOAD_STARTED,
});
export const kpi2LoadSuccess = (response) => ({
  type: KPI2_LOAD_SUCCESS,
  response,
});
export const kpi2LoadWorkingPapers = (response) => ({
  type: KPI2_LOAD_WORKING_PAPER,
  response,
});
export const kpi2LoadFailure = (error) => ({
  type: KPI2_LOAD_FAILURE,
  error,
});
export const kpi2LoadEnded = () => ({
  type: KPI2_LOAD_ENDED,
});

const saveKpi2 = (state, action) => {
  if (action === "clear") {
    const results = R.assoc("results", [], state);
    return R.assoc(results);
  } else {
    const results = R.assoc(
      "results",
      action.response.results,
      state
    );
    return R.assoc(action.response.count, results);
  }
};

export const addNewWorkingPaper = (body) => dispatch => {
  dispatch(kpi2LoadStarted());
  return requestNewWorkingPaper(body)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const updateWorkingPaper = (id, body) => dispatch => {
  dispatch(kpi2LoadStarted());
  return requestUpdateWorkingPaper(id, body)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const uploadWorkingPaperApi = (id, body) => dispatch => {
  dispatch(kpi2LoadStarted());
  return uploadWorkingPaper(id, body)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const deleteWorkingPaperApi = (id) => dispatch => {
  dispatch(kpi2LoadStarted());
  return deleteWorkingPaper(id)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const startKpi2SurveyApi = (year) => dispatch => {
  dispatch(kpi2LoadStarted());
  return startKpi2Survey(year)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const getKpi2AuditFirmsApi = (auditFirmName, year) => dispatch => {
  dispatch(kpi2LoadStarted());
  return getKpi2AuditFirms(auditFirmName, year)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response.results;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const checkIfAuditWorkCompletedApi = (year) => dispatch => {
  dispatch(kpi2LoadStarted());
  return checkIfAuditWorkCompleted(year)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const updateFinalReportApi = (id, body) => dispatch => {
  dispatch(kpi2LoadStarted());
  return updateFinalReport(id, body)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const updateSummaryReportApi = (id, body) => dispatch => {
  dispatch(kpi2LoadStarted());
  return updateSummaryReport(id, body)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const getKpi2FinalReportApi = (id) => dispatch => {
  dispatch(kpi2LoadStarted());
  return getKpi2FinalReport(id)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const getKpi2SummaryReportApi = (id) => dispatch => {
  dispatch(kpi2LoadStarted());
  return getKpi2SummaryReport(id)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const checkSurveyUploadStatusApi = (year) => dispatch => {
  dispatch(kpi2LoadStarted());
  return checkSurveyUploadStatus(year)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};

export const getKpi2KeyFindingsApi = (params) => dispatch => {
  dispatch(kpi2LoadStarted());
  return getKpi2KeyFindings(params)
    .then((response) => {
      dispatch(kpi2LoadEnded());
      return response;
    })
    .catch((error) => {
      dispatch(kpi2LoadEnded());
      throw error;
    });
};



const messages = {
  loadFailed: "Loading kpi2 failed.",
};

const initialState = {
  loading: false,
  workingPapers: [],
  results: null,
  error: {}
};


export default function kpi2Reducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case KPI2_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case KPI2_LOAD_ENDED: {
      return stopLoading(state);
    }
    case KPI2_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case KPI2_LOAD_SUCCESS: {
      return saveKpi2(state, action);
    }
    default:
      return state;
  }
}
