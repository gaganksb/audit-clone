import R from 'ramda';
import {
    startSurvey,
    getSurveyDetailsByAgreementNumber,
    saveKpi3SurveyResponse,
    getSurveyRecords,
    saveKpi3Report,
    notifyAgreementSurveys,
    getKpi3Report,
    getKpi3QuestionResponse,
    getKpi3QuestionComments,
    checkSurveyQuestionsUploaded,
    uploadSurveyQuestions,
} from "../../helpers/api/api";

import {
    clearError,
    startLoading,
    stopLoading,
    saveErrorMsg,
} from "../apiStatus";

export const KPI3_LOAD_STARTED =
    "KPI3_LOAD_STARTED";
export const KPI3_LOAD_SUCCESS =
    "KPI3_LOAD_SUCCESS";
export const KPI3_LOAD_FAILURE =
    "KPI3_LOAD_FAILURE";
export const KPI3_LOAD_ENDED = "KPI3_LOAD_ENDED";
export const CLEAR_KPI3 = "CLEAR_KPI3";
export const UPDATE_KPI3 = "UPDATE_KPI3";

export const kpi3LoadStarted = () => ({
    type: KPI3_LOAD_STARTED,
});
export const kpi3LoadSuccess = (response) => ({
    type: KPI3_LOAD_SUCCESS,
    response,
});
export const kpi3LoadFailure = (error) => ({
    type: KPI3_LOAD_FAILURE,
    error,
});
export const kpi3LoadEnded = () => ({
    type: KPI3_LOAD_ENDED,
});
export const clearKpi3Success = (response) => ({
    type: CLEAR_KPI3,
    response,
});

export const updateKpi3 = (response) => ({
    type: UPDATE_KPI3,
    response,
});

const saveKpi3 = (state, action) => {
    if (action === "clear") {
        const kpi3 = R.assoc("kpi3", [], state);
        return R.assoc("totalCount", 0, kpi3);
    } else {
        const kpi3 = R.assoc(
            "kpi3",
            action.response.kpi3,
            state
        );
        return R.assoc("totalCount", action.response.count, kpi3);
    }
};



const messages = {
    loadFailed: "Loading kpi3 failed.",
};

const initialState = {
    totalCount: 0,
    loading: false,
    kpi3: [],
    error: {}
};

export const startSurveyApi = (year) => dispatch => {
    dispatch(kpi3LoadStarted());
    return startSurvey(year)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const getSurveyDetails = (surveyId) => dispatch => {
    dispatch(kpi3LoadStarted());
    return getSurveyDetailsByAgreementNumber(surveyId)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const getSurveyRecordsApi = (year, audit_agency) => dispatch => {
    dispatch(kpi3LoadStarted());
    return getSurveyRecords(year, audit_agency)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const postSurveyResponse = (id, body) => dispatch => {
    dispatch(kpi3LoadStarted());
    return saveKpi3SurveyResponse(id, body)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const saveKpi3ReportApi = (id, body) => dispatch => {
    dispatch(kpi3LoadStarted());
    return saveKpi3Report(id, body)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const notifySurveyApi = (body) => dispatch => {
    dispatch(kpi3LoadStarted());
    return notifyAgreementSurveys(body)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const getKpi3ReportApi = (year, firm_id) => dispatch => {
    dispatch(kpi3LoadStarted());
    return getKpi3Report(year, firm_id)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const getKpi3QuestionResponseApi = (year, questionId, survey_type) => dispatch => {
    dispatch(kpi3LoadStarted());
    return getKpi3QuestionResponse(year, questionId, survey_type)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const getKpi3QuestionCommentsApi = (year, survey_type, page, bu_code, agreement_number, answer_option, partner_name) => dispatch => {
    dispatch(kpi3LoadStarted());
    return getKpi3QuestionComments(year, survey_type, page, bu_code, agreement_number, answer_option, partner_name)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const checkSurveyQuestionsUploadedApi = (year) => dispatch => {
    dispatch(kpi3LoadStarted());
    return checkSurveyQuestionsUploaded(year)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const uploadSurveyQuestionsApi = (body) => dispatch => {
    dispatch(kpi3LoadStarted());
    return uploadSurveyQuestions(body)
        .then((response) => {
            dispatch(kpi3LoadEnded());
            return response;
        })
        .catch((error) => {
            dispatch(kpi3LoadEnded());
            throw error;
        });
};

export const clearKpi3 = (params) => (dispatch) => {
    dispatch(clearKpi3Success());
};

export default function kpi3Reducer(
    state = initialState,
    action
) {
    switch (action.type) {
        case KPI3_LOAD_FAILURE: {
            return saveErrorMsg(state, action, messages.loadFailed);
        }
        case KPI3_LOAD_ENDED: {
            return stopLoading(state);
        }
        case KPI3_LOAD_STARTED: {
            return startLoading(clearError(state));
        }
        case KPI3_LOAD_SUCCESS: {
            return saveKpi3(state, action);
        }
        case UPDATE_KPI3: {
            return updateKpi3(state, action);
        }
        case CLEAR_KPI3: {
            return saveKpi3([], "clear");
        }
        default:
            return state;
    }
}
