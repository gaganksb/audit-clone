import R from 'ramda';
import { getAuditReports } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const REPORT_LOAD_STARTED = 'REPORT_LOAD_STARTED';
export const REPORT_LOAD_SUCCESS = 'REPORT_LOAD_SUCCESS';
export const REPORT_LOAD_FAILURE = 'REPORT_LOAD_FAILURE';
export const REPORT_LOAD_ENDED = 'REPORT_LOAD_ENDED';

export const reportLoadStarted = () => ({ type: REPORT_LOAD_STARTED });
export const reportLoadSuccess = response => ({ type: REPORT_LOAD_SUCCESS, response });
export const reportLoadFailure = error => ({ type: REPORT_LOAD_FAILURE, error });
export const reportLoadEnded = () => ({ type: REPORT_LOAD_ENDED });

const saveReport = (state, action) => {
  return R.assoc('report', action.response, state);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  report: [],
};


export const loadReportList = (id, params) => (dispatch) => {
  dispatch(reportLoadStarted());
  return getAuditReports(id, params)
    .then((report) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadSuccess(report));
    })
    .catch((error) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadFailure(error));
    });
};

export default function reportListReducer(state = initialState, action) {
  switch (action.type) {
    case REPORT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case REPORT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case REPORT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case REPORT_LOAD_SUCCESS: {
      return saveReport(state, action);
    }
    default:
      return state;
  }
}
