import { patchAuditReports } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_AUDIT_REPORT = 'PATCH_AUDIT_REPORT';

export const editAuditReport = (id, body) => (dispatch, getState) => {
  
  const formData = new FormData();
  
Object.keys(body).map((obj) => {
  (obj == 'common_file' && !body['common_file'] )?null: formData.append(obj, body[obj])} )
  dispatch(loadStarted(PATCH_AUDIT_REPORT));
  // if(body.published) {
  //   return publishAuditReport(id, body)
  // }
  return patchAuditReports(id, formData)
    .then((report) => {
      dispatch(loadEnded(PATCH_AUDIT_REPORT));
      dispatch(loadSuccess(PATCH_AUDIT_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_AUDIT_REPORT));
      dispatch(loadFailure(PATCH_AUDIT_REPORT, error));
      throw error;
    });
};