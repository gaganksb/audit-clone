import R from 'ramda';
import { getBulkICQErrors } from '../../../helpers/api/api';
import {
    clearError,
    startLoading,
    stopLoading,
    saveErrorMsg,
} from '../../apiStatus';

export const ICQERROR_LOAD_STARTED = 'ICQERROR_LOAD_STARTED';
export const ICQERROR_LOAD_SUCCESS = 'ICQERROR_LOAD_SUCCESS';
export const ICQERROR_LOAD_FAILURE = 'ICQERROR_LOAD_FAILURE';
export const ICQERROR_LOAD_ENDED = 'ICQERROR_LOAD_ENDED';

export const icqErrorLoadStarted = () => ({ type: ICQERROR_LOAD_STARTED });
export const icqErrorLoadSuccess = response => ({ type: ICQERROR_LOAD_SUCCESS, response });
export const icqErrorLoadFailure = error => ({ type: ICQERROR_LOAD_FAILURE, error });
export const icqErrorLoadEnded = () => ({ type: ICQERROR_LOAD_ENDED });

const saveICQError = (state, action) => {
    const results = R.assoc('results', action.response.results, state);
    return R.assoc('totalCount', action.response.count, results);
};

const messages = {
    loadFailed: 'Loading icq error failed.',
};

const initialState = {
    loading: false,
    totalCount: 0,
    results: [],
};


export const loadICQError = (params) => (dispatch) => {
    dispatch(icqErrorLoadStarted());
    return getBulkICQErrors(params)
        .then((status) => {
            dispatch(icqErrorLoadEnded());
            dispatch(icqErrorLoadSuccess(status));
        })
        .catch((error) => {
            dispatch(icqErrorLoadEnded());
            dispatch(icqErrorLoadFailure(error));
        });
};

export default function icqErrorListReducer(state = initialState, action) {
    switch (action.type) {
        case ICQERROR_LOAD_FAILURE: {
            return saveErrorMsg(state, action, messages.loadFailed);
        }
        case ICQERROR_LOAD_ENDED: {
            return stopLoading(state);
        }
        case ICQERROR_LOAD_STARTED: {
            return startLoading(clearError(state));
        }
        case ICQERROR_LOAD_SUCCESS: {
            return saveICQError(state, action);
        }
        default:
            return state;
    }
}