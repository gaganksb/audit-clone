import { patchICQQuestionnaire, bulkICQPost } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure,
} from '../../../reducers/apiMeta';

export const PATCH_ICQ_REPORT = 'PATCH_ICQ_REPORT';
export const BULK_UPLOAD_ICQ = 'BULK_UPLOAD_ICQ';

export const editICQ = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_ICQ_REPORT));
  return patchICQQuestionnaire(id, body)
    .then((partner) => {
      dispatch(loadEnded(PATCH_ICQ_REPORT));
      dispatch(loadSuccess(PATCH_ICQ_REPORT));
      return partner;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_ICQ_REPORT));
      dispatch(loadFailure(PATCH_ICQ_REPORT, error));
      throw error;
    });
};

export const bulkICQUpload = (year, body) => (dispatch, getState) => {
  dispatch(loadStarted(BULK_UPLOAD_ICQ));
  return bulkICQPost(year, body)
    .then((data) => {
      dispatch(loadEnded(BULK_UPLOAD_ICQ));
      dispatch(loadSuccess(BULK_UPLOAD_ICQ));
      return data;
    })
    .catch((error) => {
      dispatch(loadEnded(BULK_UPLOAD_ICQ));
      dispatch(loadFailure(BULK_UPLOAD_ICQ, error));
      throw error;
    });
};

