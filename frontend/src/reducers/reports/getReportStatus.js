import R from 'ramda';
import { getReportStatus } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const STATUS_LOAD_STARTED = 'STATUS_LOAD_STARTED';
export const STATUS_LOAD_SUCCESS = 'STATUS_LOAD_SUCCESS';
export const STATUS_LOAD_FAILURE = 'STATUS_LOAD_FAILURE';
export const STATUS_LOAD_ENDED = 'STATUS_LOAD_ENDED';

export const statusLoadStarted = () => ({ type: STATUS_LOAD_STARTED });
export const statusLoadSuccess = response => ({ type: STATUS_LOAD_SUCCESS, response });
export const statusLoadFailure = error => ({ type: STATUS_LOAD_FAILURE, error });
export const statusLoadEnded = () => ({ type: STATUS_LOAD_ENDED });

const saveStatus = (state, action) => {
  return R.assoc('status', action.response, state);
};

const messages = {
  loadFailed: 'Loading status failed.',
};

const initialState = {
  status: [],
};


export const loadReportStatus = (id, params) => (dispatch) => {
  dispatch(statusLoadStarted());
  return getReportStatus(id, params)
    .then((status) => {
      dispatch(statusLoadEnded());
      dispatch(statusLoadSuccess(status));
    })
    .catch((error) => {
      dispatch(statusLoadEnded());
      dispatch(statusLoadFailure(error));
    });
};

export default function statusListReducer(state = initialState, action) {
  switch (action.type) {
    case STATUS_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case STATUS_LOAD_ENDED: {
      return stopLoading(state);
    }
    case STATUS_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case STATUS_LOAD_SUCCESS: {
      return saveStatus(state, action);
    }
    default:
      return state;
  }
}
