import { editManagementReport } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_MANAGEMENT_REPORT = 'PATCH_MANAGEMENT_REPORT';

export const editReports = (id,mgmtID, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_MANAGEMENT_REPORT));
  return editManagementReport(id, mgmtID, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_MANAGEMENT_REPORT));
      dispatch(loadSuccess(PATCH_MANAGEMENT_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_MANAGEMENT_REPORT));
      dispatch(loadFailure(PATCH_MANAGEMENT_REPORT, error));
      throw error;
    });
};