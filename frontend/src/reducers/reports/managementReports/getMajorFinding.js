import R from 'ramda';
import { getMajorFinding } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const MAJOR_REPORT_LOAD_STARTED = 'MAJOR_REPORT_LOAD_STARTED';
export const MAJOR_REPORT_LOAD_SUCCESS = 'MAJOR_REPORT_LOAD_SUCCESS';
export const MAJOR_REPORT_LOAD_FAILURE = 'MAJOR_REPORT_LOAD_FAILURE';
export const MAJOR_REPORT_LOAD_ENDED = 'MAJOR_REPORT_LOAD_ENDED';

export const reportLoadStarted = () => ({ type: MAJOR_REPORT_LOAD_STARTED });
export const reportLoadSuccess = response => ({ type: MAJOR_REPORT_LOAD_SUCCESS, response });
export const reportLoadFailure = error => ({ type: MAJOR_REPORT_LOAD_FAILURE, error });
export const reportLoadEnded = () => ({ type: MAJOR_REPORT_LOAD_ENDED });

const saveReport = (state, action) => {
  return R.assoc('majorFindingreport', action.response.results, state);
};

const messages = {
  loadFailed: 'Loading report failed.',
};

const initialState = {
  loading: false,
  majorFindingreport: [],
};


export const loadMajorReportList = (id, params) => (dispatch) => {
  dispatch(reportLoadStarted());
  return getMajorFinding(id, params)
    .then((report) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadSuccess(report));
    })
    .catch((error) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadFailure(error));
    });
};

export default function majorReportListReducer(state = initialState, action) {
  switch (action.type) {
    case MAJOR_REPORT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case MAJOR_REPORT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case MAJOR_REPORT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case MAJOR_REPORT_LOAD_SUCCESS: {
      return saveReport(state, action);
    }
    default:
      return state;
  }
}