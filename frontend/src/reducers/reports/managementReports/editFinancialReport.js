import { editFinancialFinding } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_FINANCIAL_REPORT = 'PATCH_FINANCIAL_REPORT';

export const editFinancialReports = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_FINANCIAL_REPORT));
  return editFinancialFinding(id, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_FINANCIAL_REPORT));
      dispatch(loadSuccess(PATCH_FINANCIAL_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_FINANCIAL_REPORT));
      dispatch(loadFailure(PATCH_FINANCIAL_REPORT, error));
      throw error;
    });
};