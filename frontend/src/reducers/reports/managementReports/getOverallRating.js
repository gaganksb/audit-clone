import R from 'ramda';
import { getOverallRating, getOverallRatingExp } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const OVERALL_REPORT_LOAD_STARTED = 'OVERALL_REPORT_LOAD_STARTED';
export const OVERALL_REPORT_LOAD_SUCCESS = 'OVERALL_REPORT_LOAD_SUCCESS';
export const OVERALL_REPORT_LOAD_FAILURE = 'OVERALL_REPORT_LOAD_FAILURE';
export const OVERALL_REPORT_LOAD_ENDED = 'OVERALL_REPORT_LOAD_ENDED';

export const reportLoadStarted = () => ({ type: OVERALL_REPORT_LOAD_STARTED });
export const reportLoadSuccess = response => ({ type: OVERALL_REPORT_LOAD_SUCCESS, response });
export const reportLoadFailure = error => ({ type: OVERALL_REPORT_LOAD_FAILURE, error });
export const reportLoadEnded = () => ({ type: OVERALL_REPORT_LOAD_ENDED });

const saveReport = (state, action) => {
  return R.assoc('overallreport', action.response.results, state);
};

const messages = {
  loadFailed: 'Loading report failed.',
};

const initialState = {
  loading: false,
  overallreport: [],
};


export const loadOverallReportList = (id, params) => (dispatch) => {
  dispatch(reportLoadStarted());
  return getOverallRating(id, params)
    .then((report) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadSuccess(report));
    })
    .catch((error) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadFailure(error));
    });
};

export const loadOverallReportListExp = (id, params) => (dispatch) => {
  dispatch(reportLoadStarted());
  return getOverallRatingExp(id, params)
    .then((report) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadSuccess(report));
    })
    .catch((error) => {
      dispatch(reportLoadEnded());
      dispatch(reportLoadFailure(error));
    });
};

export default function overallReportListReducer(state = initialState, action) {
  switch (action.type) {
    case OVERALL_REPORT_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case OVERALL_REPORT_LOAD_ENDED: {
      return stopLoading(state);
    }
    case OVERALL_REPORT_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case OVERALL_REPORT_LOAD_SUCCESS: {
      return saveReport(state, action);
    }
    default:
      return state;
  }
}