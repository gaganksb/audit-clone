import { deleteMajorFinding } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_MAJOR_FINDING = 'DELETE_MAJOR_FINDING';
const errorMsg = 'Unable to delete';

export const deleteMajorFindingRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_MAJOR_FINDING));
  return deleteMajorFinding(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_MAJOR_FINDING));
      dispatch(loadSuccess(DELETE_MAJOR_FINDING));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_MAJOR_FINDING));
      dispatch(loadFailure(DELETE_MAJOR_FINDING, error));
      dispatch(errorToBeAdded(error, 'MajorFindingDelete', errorMsg));
    });
};