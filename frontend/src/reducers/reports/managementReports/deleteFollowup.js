import { deleteFollowup } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_FOLLOWUP = 'DELETE_FOLLOWUP';
const errorMsg = 'Unable to delete';

export const deleteFollowupRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_FOLLOWUP));
  return deleteFollowup(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_FOLLOWUP));
      dispatch(loadSuccess(DELETE_FOLLOWUP));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_FOLLOWUP));
      dispatch(loadFailure(DELETE_FOLLOWUP, error));
      dispatch(errorToBeAdded(error, 'FollowupDelete', errorMsg));
    });
};