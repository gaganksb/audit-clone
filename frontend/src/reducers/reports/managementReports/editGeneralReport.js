import { editGeneralReport } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_GENERAL_REPORT = 'PATCH_GENERAL_REPORT';

export const editGeneralReports = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_GENERAL_REPORT));
  return editGeneralReport(id, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_GENERAL_REPORT));
      dispatch(loadSuccess(PATCH_GENERAL_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_GENERAL_REPORT));
      dispatch(loadFailure(PATCH_GENERAL_REPORT, error));
      throw error;
    });
};