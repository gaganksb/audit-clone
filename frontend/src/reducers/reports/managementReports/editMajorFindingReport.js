import { editMajorFinding } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_MAJOR_FINDING_REPORT = 'PATCH_MAJOR_FINDING_REPORT';

export const editMajorFindingReports = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_MAJOR_FINDING_REPORT));
  return editMajorFinding(id, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_MAJOR_FINDING_REPORT));
      dispatch(loadSuccess(PATCH_MAJOR_FINDING_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_MAJOR_FINDING_REPORT));
      dispatch(loadFailure(PATCH_MAJOR_FINDING_REPORT, error));
      throw error;
    });
};