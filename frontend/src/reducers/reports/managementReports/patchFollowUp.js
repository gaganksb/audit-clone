import { postFollowUp } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_FOLLOW_UP = 'PATCH_FOLLOW_UP';

export const editFollowUps = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_FOLLOW_UP));
  return postFollowUp(id, body)
    .then((followup) => {
      dispatch(loadEnded(PATCH_FOLLOW_UP));
      dispatch(loadSuccess(PATCH_FOLLOW_UP));
      return followup;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_FOLLOW_UP));
      dispatch(loadFailure(PATCH_FOLLOW_UP, error));
      throw error;
    });
};