import { deleteOverallAssessment } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_OVERALL_ASSESSMENT = 'DELETE_OVERALL_ASSESSMENT';
const errorMsg = 'Unable to delete';

export const deleteOverallAssessmentRequest = id => (dispatch) => {
  dispatch(loadStarted(DELETE_OVERALL_ASSESSMENT));
  return deleteOverallAssessment(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_OVERALL_ASSESSMENT));
      dispatch(loadSuccess(DELETE_OVERALL_ASSESSMENT));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_OVERALL_ASSESSMENT));
      dispatch(loadFailure(DELETE_OVERALL_ASSESSMENT, error));
      dispatch(errorToBeAdded(error, 'OverallAssessmentDelete', errorMsg));
    });
};