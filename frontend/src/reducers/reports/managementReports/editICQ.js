import { patchICQ } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_ICQ = 'PATCH_ICQ';

export const editICQ = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_ICQ));
  return patchICQ(id, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_ICQ));
      dispatch(loadSuccess(PATCH_ICQ));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_ICQ));
      dispatch(loadFailure(PATCH_ICQ, error));
      throw error;
    });
};