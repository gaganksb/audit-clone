import { patchManagementReport } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_MGMT_REPORT = 'PATCH_MGMT_REPORT';

export const editMgmtReports = (id, mgmtId, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_MGMT_REPORT));
  return patchManagementReport(id, mgmtId, body)
    .then((report) => {
      dispatch(loadEnded(PATCH_MGMT_REPORT));
      dispatch(loadSuccess(PATCH_MGMT_REPORT));
      return report;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_MGMT_REPORT));
      dispatch(loadFailure(PATCH_MGMT_REPORT, error));
      throw error;
    });
};