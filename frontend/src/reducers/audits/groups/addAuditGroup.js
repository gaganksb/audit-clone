import { createAuditGroup } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const NEW_AUDIT_GROUP = 'NEW_AUDIT_GROUP';

export const addNewGroup = body => (dispatch) => {
  dispatch(loadStarted(NEW_AUDIT_GROUP));
  return createAuditGroup(body)
    .then((group) => {
      dispatch(loadEnded(NEW_AUDIT_GROUP));
      dispatch(loadSuccess(NEW_AUDIT_GROUP));
      return group;
    })
    .catch((error) => {
      dispatch(loadEnded(NEW_AUDIT_GROUP));
      dispatch(loadFailure(NEW_AUDIT_GROUP, error));
      throw error;
    });
};
