import { deleteAuditGroup } from '../../../helpers/api/api';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../../reducers/apiMeta';

export const DELETE_AUDIT_GROUP = 'DELETE_AUDIT_GROUP';
const errorMsg = 'Unable to delete Audit Group';

export const deleteGroup = id => (dispatch) => {
  dispatch(loadStarted(DELETE_AUDIT_GROUP));
  return deleteAuditGroup(id)
    .then((response) => {
      dispatch(loadEnded(DELETE_AUDIT_GROUP));
      dispatch(loadSuccess(DELETE_AUDIT_GROUP));
      return response;
    })
    .catch((error) => {
      dispatch(loadEnded(DELETE_AUDIT_GROUP));
      dispatch(loadFailure(DELETE_AUDIT_GROUP, error));
      dispatch(errorToBeAdded(error, 'DeleteAuditGroup', errorMsg));
    });
};
