import R from 'ramda';
import { getGroupList } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const GROUP_LIST_LOAD_STARTED = 'GROUP_LIST_LOAD_STARTED';
export const GROUP_LIST_LOAD_SUCCESS = 'GROUP_LIST_LOAD_SUCCESS';
export const GROUP_LIST_LOAD_FAILURE = 'GROUP_LIST_LOAD_FAILURE';
export const GROUP_LIST_LOAD_ENDED = 'GROUP_LIST_LOAD_ENDED';
export const GROUP_DROP_LIST_LOAD_SUCCESS = 'GROUP_DROP_LIST_LOAD_SUCCESS';

export const groupLoadStarted = () => ({ type: GROUP_LIST_LOAD_STARTED });
export const groupLoadSuccess = response => ({ type: GROUP_LIST_LOAD_SUCCESS, response });
export const groupLoadFailure = error => ({ type: GROUP_LIST_LOAD_FAILURE, error });
export const groupListLoadDropdownSuccess = response => ({ type: GROUP_DROP_LIST_LOAD_SUCCESS, response });
export const groupLoadEnded = () => ({ type: GROUP_LIST_LOAD_ENDED });

const saveGroup = (state, action) => {
  const group = R.assoc('group', action.response.results, state);
  return R.assoc('totalCount', action.response.count, group);
};

const saveGroupDropList = (state, action) => {
  const groupDropList = R.assoc('groupList', action.response.results, state);
  return R.assoc('totalCount', action.response.count, groupDropList);
};


const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  group: [],
  groupList: []
};

export const loadGroupListDropdown = params => (dispatch) => {
  dispatch(groupLoadStarted());
  return getGroupList(params)
    .then((group) => {
      dispatch(groupLoadEnded());
      dispatch(groupListLoadDropdownSuccess(group));
    })
    .catch((error) => {
      dispatch(groupLoadEnded());
      dispatch(groupLoadFailure(error));
    });
};

export const loadGroupList = params => (dispatch) => {
  dispatch(groupLoadStarted());
  return getGroupList(params)
    .then((group) => {
      dispatch(groupLoadEnded());
      dispatch(groupLoadSuccess(group));
    })
    .catch((error) => {
      dispatch(groupLoadEnded());
      dispatch(groupLoadFailure(error));
    });
};

export default function groupListReducer(state = initialState, action) {
  switch (action.type) {
    case GROUP_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case GROUP_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case GROUP_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case GROUP_LIST_LOAD_SUCCESS: {
      return saveGroup(state, action);
    }
    case GROUP_DROP_LIST_LOAD_SUCCESS: {
      return saveGroupDropList(state, action);
    }
    default:
      return state;
  }
}
