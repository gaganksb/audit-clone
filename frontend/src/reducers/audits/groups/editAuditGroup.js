import { patchAuditGroup } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_AUDIT_GROUP = 'PATCH_AUDIT_GROUP';

export const editAuditGroup = (id, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_AUDIT_GROUP));
  return patchAuditGroup(id, body)
    .then((group) => {
      dispatch(loadEnded(PATCH_AUDIT_GROUP));
      dispatch(loadSuccess(PATCH_AUDIT_GROUP));
      return group;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_AUDIT_GROUP));
      dispatch(loadFailure(PATCH_AUDIT_GROUP, error));
      throw error;
    });
};

