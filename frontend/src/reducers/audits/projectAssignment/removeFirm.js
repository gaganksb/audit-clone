import { unassignFirm } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const UNASSIGN_FIRM = 'UNASSIGN_FIRM';
export const removeFirm = (year, body) => (dispatch) => {
  dispatch(loadStarted(UNASSIGN_FIRM));
  return unassignFirm(year, body)
    .then((success) => {
      dispatch(loadEnded(UNASSIGN_FIRM));
      dispatch(loadSuccess(UNASSIGN_FIRM));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(UNASSIGN_FIRM));
      dispatch(loadFailure(UNASSIGN_FIRM, error));
      throw error;
    });
};