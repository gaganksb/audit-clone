import { patchProjectAssignments } from '../../../helpers/api/api';
import { reset } from 'redux-form';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const PATCH_PROJECT_ASSIGNMENT = 'PATCH_PROJECT_ASSIGNMENT';

export const editProjectAssignment = (year, body) => (dispatch, getState) => {
  dispatch(loadStarted(PATCH_PROJECT_ASSIGNMENT));
  return patchProjectAssignments(year,  body)
    .then((assignmnents) => {
      dispatch(loadEnded(PATCH_PROJECT_ASSIGNMENT));
      dispatch(loadSuccess(PATCH_PROJECT_ASSIGNMENT));
      dispatch(reset('assignmentModal'));
      return assignmnents;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_PROJECT_ASSIGNMENT));
      dispatch(loadFailure(PATCH_PROJECT_ASSIGNMENT, error));
      throw error;
    });
};

