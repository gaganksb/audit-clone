import { sendAuditReminder } from '../../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure } from '../../apiMeta';

export const SEND_REMINDER = 'SEND_REMINDER';
export const sendReminder = (year) => (dispatch) => {
  dispatch(loadStarted(SEND_REMINDER));
  return sendAuditReminder(year)
    .then((success) => {
      dispatch(loadEnded(SEND_REMINDER));
      dispatch(loadSuccess(SEND_REMINDER));
      return success;
    })
    .catch((error) => {
      dispatch(loadEnded(SEND_REMINDER));
      dispatch(loadFailure(SEND_REMINDER, error));
      throw error;
    });
};