import R from 'ramda';
import { remindAuditAssignment } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const CHECK_REMINDER_LOAD_STARTED = 'CHECK_REMINDER_LOAD_STARTED';
export const CHECK_REMINDER_LOAD_SUCCESS = 'CHECK_REMINDER_LOAD_SUCCESS';
export const CHECK_REMINDER_LOAD_FAILURE = 'CHECK_REMINDER_LOAD_FAILURE';
export const CHECK_REMINDER_LOAD_ENDED = 'CHECK_REMINDER_LOAD_ENDED';

export const check_reminderLoadStarted = () => ({ type: CHECK_REMINDER_LOAD_STARTED });
export const check_reminderLoadSuccess = response => ({ type: CHECK_REMINDER_LOAD_SUCCESS, response });
export const check_reminderLoadFailure = error => ({ type: CHECK_REMINDER_LOAD_FAILURE, error });
export const check_reminderLoadEnded = () => ({ type: CHECK_REMINDER_LOAD_ENDED });

const save_Reminder = (state, action) => {
    return R.assoc('check_reminder', action.response, state);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  check_reminder: {},
};


export const loadCheck_ReminderList = (year) => (dispatch) => {
  dispatch(check_reminderLoadStarted());
  return remindAuditAssignment(year)
    .then((check_reminder) => {
      dispatch(check_reminderLoadEnded());
      dispatch(check_reminderLoadSuccess(check_reminder));
    })
    .catch((error) => {
      dispatch(check_reminderLoadEnded());
      dispatch(check_reminderLoadFailure(error));
    });
};

export default function check_reminderListReducer(state = initialState, action) {
  switch (action.type) {
    case CHECK_REMINDER_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case CHECK_REMINDER_LOAD_ENDED: {
      return stopLoading(state);
    }
    case CHECK_REMINDER_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case CHECK_REMINDER_LOAD_SUCCESS: {
      return save_Reminder(state, action);
    }
    default:
      return state;
  }
}
