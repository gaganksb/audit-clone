import R from 'ramda';
import { getProjectAssignments } from '../../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../../apiStatus';

export const PROJECT_ASSIGNMENT_LIST_LOAD_STARTED = 'PROJECT_ASSIGNMENT_LIST_LOAD_STARTED';
export const PROJECT_ASSIGNMENT_LIST_LOAD_SUCCESS = 'PROJECT_ASSIGNMENT_LIST_LOAD_SUCCESS';
export const PROJECT_ASSIGNMENT_LIST_LOAD_FAILURE = 'PROJECT_ASSIGNMENT_LIST_LOAD_FAILURE';
export const PROJECT_ASSIGNMENT_LIST_LOAD_ENDED = 'PROJECT_ASSIGNMENT_LIST_LOAD_ENDED';

export const assignmentLoadStarted = () => ({ type: PROJECT_ASSIGNMENT_LIST_LOAD_STARTED });
export const assignmentLoadSuccess = response => ({ type: PROJECT_ASSIGNMENT_LIST_LOAD_SUCCESS, response });
export const assignmentLoadFailure = error => ({ type: PROJECT_ASSIGNMENT_LIST_LOAD_FAILURE, error });
export const assignmentLoadEnded = () => ({ type: PROJECT_ASSIGNMENT_LIST_LOAD_ENDED });

const saveAssignment = (state, action) => {
  const assignment = R.assoc('assignment', action.response.results, state);
  return R.assoc('totalCount', action.response.count, assignment);
};

const messages = {
  loadFailed: 'Loading users failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  assignment: [],
};


export const loadProjectAssignmentList = (year, params) => (dispatch) => {
  dispatch(assignmentLoadStarted());
  return getProjectAssignments(year, params)
    .then((assignment) => {
      dispatch(assignmentLoadEnded());
      dispatch(assignmentLoadSuccess(assignment));
    })
    .catch((error) => {
      dispatch(assignmentLoadEnded());
      dispatch(assignmentLoadFailure(error));
    });
};

export default function assignmentListReducer(state = initialState, action) {
  switch (action.type) {
    case PROJECT_ASSIGNMENT_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case PROJECT_ASSIGNMENT_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case PROJECT_ASSIGNMENT_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case PROJECT_ASSIGNMENT_LIST_LOAD_SUCCESS: {
      return saveAssignment(state, action);
    }
    default:
      return state;
  }
}
