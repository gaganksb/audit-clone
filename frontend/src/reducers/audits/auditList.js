import R from 'ramda';
import { getAuditList } from '../../helpers/api/api';
import {
  clearError,
  startLoading,
  stopLoading,
  saveErrorMsg,
} from '../apiStatus';

export const AUDIT_LIST_LOAD_STARTED = 'AUDIT_LIST_LOAD_STARTED';
export const AUDIT_LIST_LOAD_SUCCESS = 'AUDIT_LIST_LOAD_SUCCESS';
export const AUDIT_LIST_LOAD_FAILURE = 'AUDIT_LIST_LOAD_FAILURE';
export const AUDIT_LIST_LOAD_ENDED = 'AUDIT_LIST_LOAD_ENDED';
export const AUDIT_DROP_LIST_LOAD_SUCCESS = 'AUDIT_DROP_LIST_LOAD_SUCCESS';

export const auditListLoadStarted = () => ({ type: AUDIT_LIST_LOAD_STARTED });
export const auditListLoadSuccess = response => ({ type: AUDIT_LIST_LOAD_SUCCESS, response });
export const auditListLoadDropdownSuccess = response => ({ type: AUDIT_DROP_LIST_LOAD_SUCCESS, response });
export const auditListLoadFailure = error => ({ type: AUDIT_LIST_LOAD_FAILURE, error });
export const auditListLoadEnded = () => ({ type: AUDIT_LIST_LOAD_ENDED });

const saveAuditList = (state, action) => {
  const list = R.assoc('list', action.response.results, state);
  return R.assoc('totalCount', action.response.count, list);
};

const saveAuditDropList = (state, action) => {
  const list = R.assoc('firmList', action.response.results, state);
  return R.assoc('totalCount', action.response.count, list);
};

const messages = {
  loadFailed: 'Loading audit failed.',
};

const initialState = {
  loading: false,
  totalCount: 0,
  list: [],
  firmList: []
};

export const loadAuditListDropdown = params => (dispatch) => {
  dispatch(auditListLoadStarted());
  return getAuditList(params)
    .then((success) => {
      dispatch(auditListLoadEnded());
      dispatch(auditListLoadDropdownSuccess(success));
    })
    .catch((error) => {
      dispatch(auditListLoadEnded());
      dispatch(auditListLoadFailure(error));
    });
};

export const loadAuditList = params => (dispatch) => {
  dispatch(auditListLoadStarted());
  return getAuditList(params)
    .then((success) => {
      dispatch(auditListLoadEnded());
      dispatch(auditListLoadSuccess(success));
    })
    .catch((error) => {
      dispatch(auditListLoadEnded());
      dispatch(auditListLoadFailure(error));
    });
};

export default function auditListReducer(state = initialState, action) {
  switch (action.type) {
    case AUDIT_LIST_LOAD_FAILURE: {
      return saveErrorMsg(state, action, messages.loadFailed);
    }
    case AUDIT_LIST_LOAD_ENDED: {
      return stopLoading(state);
    }
    case AUDIT_LIST_LOAD_STARTED: {
      return startLoading(clearError(state));
    }
    case AUDIT_LIST_LOAD_SUCCESS: {
      return saveAuditList(state, action);
    }
    case AUDIT_DROP_LIST_LOAD_SUCCESS: {
      return saveAuditDropList(state, action);
    }
    default:
      return state;
  }
}
