import { patchAuditFirm } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../reducers/apiMeta';

export const PATCH_AUDIT_FIRM = 'PATCH_AUDIT_FIRM';

export const editAuditFirm = (id, body) => (dispatch) => {
  dispatch(loadStarted(PATCH_AUDIT_FIRM));
  return patchAuditFirm(id, body)
    .then((user) => {
      dispatch(loadEnded(PATCH_AUDIT_FIRM));
      dispatch(loadSuccess(PATCH_AUDIT_FIRM));
      return user;
    })
    .catch((error) => {
      dispatch(loadEnded(PATCH_AUDIT_FIRM));
      dispatch(loadFailure(PATCH_AUDIT_FIRM, error));
      throw error;
    });
};
