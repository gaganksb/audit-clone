import { createAuditFirm } from '../../helpers/api/api';
import {
  loadStarted,
  loadEnded,
  loadSuccess,
  loadFailure
} from '../../reducers/apiMeta';

export const CREATE_AUDIT_FIRM = 'CREATE_AUDIT_FIRM';

export const addAuditFirm = body => (dispatch) => {
  dispatch(loadStarted(CREATE_AUDIT_FIRM));
  return createAuditFirm(body)
    .then(user => {
      dispatch(loadEnded(CREATE_AUDIT_FIRM));
      dispatch(loadSuccess(CREATE_AUDIT_FIRM));
      return user;
    })
    .catch(error => {
      dispatch(loadEnded(CREATE_AUDIT_FIRM));
      dispatch(loadFailure(CREATE_AUDIT_FIRM, error));
      throw error;
    });
};
