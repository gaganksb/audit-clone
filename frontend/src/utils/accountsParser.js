import { USERS, OTHER, ACCOUNT_TYPES } from '../helpers/constants'

export const parseTypeRole = (userType, userRole) => {
  let type = OTHER
  let role = '-'
  let model = '-'
  for (let i = 0; i < USERS.length; i++) {
    if (USERS[i].ENUM == userType) {
      type = USERS[i].NAME
      model = USERS[i].MODEL
      for (let j = 0; j < USERS[i].ROLES.length; j++) {
        if (USERS[i].ROLES[j].ENUM == userRole) {
          role = USERS[i].ROLES[j].NAME
          break
        }
      }
      break
    }
  }
  return {
    type,
    role,
    model
  }
}

export const setUserTypeRole = (accounts) => {
  let res = []
  if (accounts) {
    for (const [key, value] of Object.entries(accounts)) {
      for (let i = 0; i < value.length; i++) {
        const {type, role, model } = parseTypeRole(ACCOUNT_TYPES[key], value[i].role)
        let account = {
          id: value[i].id,
          type,
          role,
          model,
          office: (value[i].office) ? value[i].office : '-',
          is_default: value[i].is_default,
        }
        res.push(account)
      }
    }
  }
  return res
}