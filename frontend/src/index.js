import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './App';
import {IntlProvider} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from 'react-intl/locale-data/en';
import locale_it from 'react-intl/locale-data/it';
import * as Sentry from '@sentry/browser';

import messages_it from "./translations/it.json";
import messages_en from "./translations/en.json";

addLocaleData([...locale_en, ...locale_it]);
const messages = {
    'it': messages_it,
    'en': messages_en
};
const language = navigator.language.split(/[-_]/)[0];
if (process.env.NODE_ENV == 'production') {
    Sentry.init({dsn: "https://4f8fcd5866744cd8aa997fcfc6636f74@sentry.io/5177548"});
}
// eslint-disable-next-line
ReactDOM.render(
    <IntlProvider locale={language} messages={messages[language]}>
        <App/>
    </IntlProvider>,
    document.getElementById('root')
);
