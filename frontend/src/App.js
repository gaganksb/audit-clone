import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Router from './routes';
import ErrorBoundary from 'react-error-boundary';
import Error from './components/errorPage'

// has to be React class otherwise react-scripts throw error
// eslint-disable-next-line
class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <ErrorBoundary FallbackComponent={Error}>
          <Router />
        </ErrorBoundary>
      </Provider>
    );  
  }
}

export default App;
