import React from 'react';
import { connect } from 'react-redux';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './layout/theme';
import ErrorSnackbar from './common/errorSnackbar';
import SuccessSnackbar from './common/sucessSnackbar'
import { loadGeneralConfig } from '../reducers/generalConfig';

const Main = ({ getGeneralConfig, children }) => {
  const [configLoaded, setConfigLoaded] = React.useState(false);
  const [error, setError] = React.useState(null);

  React.useState(() => {
    const configPromises = [
      getGeneralConfig(),
    ];
    Promise.all(configPromises).then(() => {
      setConfigLoaded(true);
    }).catch(error => {
      setError(error);
    });
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <ErrorSnackbar>
        {children}
      </ErrorSnackbar>      
      <SuccessSnackbar />
    </ThemeProvider>
  );
}

const mapStateToProps = state => ({
  session: state.session,
});


const mapDispatchToProps = dispatch => ({
  getGeneralConfig: () => dispatch(loadGeneralConfig()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
