import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import Title from '../common/customTitle';
import R from 'ramda'
import { loadAssessmentList } from '../../reducers/assessmentReducer/getAssessmentList'
import { loadBUList } from '../../reducers/projects/oiosReducer/buList'
import AssessmentFilter from './assessmentFilter'
import Paper from '@material-ui/core/Paper'
import { loadUser } from '../../reducers/users/userData'
import AssessmentTable from './assessmentTable'
import CustomizedButton from '../common/customizedButton'
import { buttonType } from '../../helpers/constants';
import Grid from "@material-ui/core/Grid"
import AssessmentActions from './assessmentActions'
import { loadFieldDashboard } from '../../reducers/fields/fieldDashboard'
import { USERS_TYPES } from '../../helpers/constants'
import AssessmentProgress from './progress'
import { loadDeadLine } from '../../reducers/assessmentReducer/getDeadline';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { checkAssessmentCanProceed } from '../../reducers/assessmentReducer/assessmentProgress'
import AssessmentProgressButton from './progress/assessmentProgressButton';
import { dayDifference } from '../../helpers/dates'
import BGTaskLoader from '../common/bgTaskLoader';
import { normalizeDate, isDateBefore } from '../../helpers/dates';

const currentYear = new Date().getFullYear()
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(3),
    width: '28%'
  },
  paper: {
    color: theme.palette.text.secondary,
    minHeight: 150
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  deadline: {
    color: 'red',
    fontWeight: 'bold',
    position: 'absolute',
    right: theme.spacing(2),
    margin: theme.spacing(1)
  }
}))

const messages = {
  title: 'Field Assessment',
  tableHeader: [
    { title: '', align: 'left', field: '' },
    { title: 'BU + PA ID', align: 'left', field: 'field_assessments__business_unit__code' },
    { title: 'Partner', align: 'left', field: 'field_assessments__partner__legal_name' },
    { title: 'Description | Value', align: 'left', field: 'score__description' },
    { title: 'Is Delta', align: 'left', field: 'delta' },
    { title: 'Score', align: 'left', field: 'score__value' },
    { title: '', align: 'left', field: '' },
  ],
  error: 'Could not complete the action',
  buttonText: 'View List Of Business Unit',
  totalFA: 'Total Number of FA',
  completedFA: 'Total Number of FA completed',
  field_assessment_hq: 'field_assessment_hq'
}

const Assessment = props => {

  const [fieldAssessment, setFieldAssessment] = useState([]);
  const [totalCount, setTotalCount] = useState(null);
  const [deltaDeadline, setDeltaDeadline] = useState(null);


  useEffect(() => {
    if (props.fieldAssessment) {
      setFieldAssessment([...props.fieldAssessment])
      setTotalCount(props.fieldAssessment.length);

      if (props.fieldAssessment && props.fieldAssessment.length > 0) {
        let deltaRecord = props.fieldAssessment.find(record => record.cycle && record.cycle.delta_enabled);
        if (deltaRecord) {
          setDeltaDeadline(deltaRecord.cycle.delta_field_assessment_deadline);
        }
      }
    }
  }, [props.fieldAssessment])

  const { fieldDashboard, loadFieldDashboard, loadAssessmentList, loadBUList, loading,
    query, loadUserData, BUList, membership, loadDeadLine, deadlineDate, partnerList, loadPartnerList,
    checkAssessmentCanProceed
  } = props;
  let diffDays = null;

  if (deadlineDate) {
    let date = normalizeDate(new Date())
    diffDays = deadlineDate.deadline_date ? dayDifference(normalizeDate(deadlineDate.deadline_date), date) : 0
  }
  const classes = useStyles()
  const [params, setParams] = useState({ page: 0, budget_ref: currentYear })
  const [year, setYear] = useState(currentYear)
  const [items, setItems] = useState([])
  const [partnerLists, setPartnerList] = useState([])
  const [partner, setSelectedPartner] = useState([])
  const [key, setKey] = useState('default')
  const [startMonitoring, shouldStartMonitoring] = useState(false)
  const [delta, setDelta] = useState(false);

  var date = new Date()
  useEffect(() => {
    setItems(BUList)
  }, [BUList])

  useEffect(() => {
    const { pathName } = props;
    if ("budget_ref" in query === false) {
      setYear(currentYear);
      if ("delta" in query == false) {
        setDelta(false)
      }
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          budget_ref: currentYear,
        })
      })
    } else {
      setYear(query.budget_ref)
      loadAssessmentList(query)
      loadUserData()
      loadDeadLine({ budget_ref: query.budget_ref })
      checkAssessmentCanProceed({ budget_ref: query.budget_ref })
    }
  }, [query])


  useEffect(() => {
    setPartnerList(partnerList);
  }, [partnerList]);

  useEffect(() => {
    let params = { partial: true, budget_ref: query.budget_ref }
    query.budget_ref && membership && membership.type == USERS_TYPES.HQ && loadFieldDashboard(params)
  }, [membership])

  const reloadDashboard = () => {
    let params = { partial: true, budget_ref: query.budget_ref }
    loadFieldDashboard(params)
  }

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field,
    order: 'desc'
  })

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }))
  }

  function handleSort(field, order) {
    const { pathName, query } = props

    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`,
      }),
    })
  }

  function resetFilter() {
    const { pathName } = props;
    setSelectedPartner([])
    setKey(date.getTime())
    setParams({ page: 0 });
    setDelta(false);
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        delta: undefined,
      },
    })
  }

  function handleSearch(values) {
    const { pathName, query } = props
    const { partner, score__id, business__unit, year } = values
    if (score__id === '0') {
      delete query.score__id
    }

    setParams({ page: 0 });

    history.push({
      pathname: pathName,
      query: score__id > 0 ?
        R.merge(query, {
          page: 1,
          partner,
          score__id,
          budget_ref: year,
          business_unit: business__unit,
          delta: delta ? delta : undefined,
        }) :
        R.merge(query, {
          page: 1,
          partner,
          budget_ref: year,
          business_unit: business__unit,
          delta: delta ? delta : undefined,
        })
    })
  }

  function viewList() {
    history.push(`/fieldProgress?budget_ref=${query.budget_ref}&page=1`)
  }

  function handleChange(e, v) {
    if (v) {
      let params = {
        code: v,
      }
      loadBUList(params)
    }
  }


  function handleChangePartner(e, v) {
    if (v) {
      let params = {
        legal_name: v,
      }
      loadPartnerList(params);
    }
  }

  const handleDeltaCheckboxChange = (e) => {
    setDelta(e.target.checked);
  }

  const checkIfDeltaCycleEnabled = () => {
    if (fieldAssessment && fieldAssessment.length > 0) {
      return fieldAssessment.some(record => record.cycle && record.cycle.delta_enabled);
    }
    return false;
  }

  const getCycleId = () => {
    if (fieldAssessment && fieldAssessment.length > 0) {
      let deltaRecord = fieldAssessment.find(record => record.cycle && record.cycle.delta_enabled);
      if (deltaRecord) {
        return deltaRecord.cycle.id;
      }
    }
    return null;
  }

  const updateDeltaDeadline = date => {
    setDeltaDeadline(date);
  }

  const isDeltaDeadlineAvailable = () => {
    if (fieldAssessment) {
      return fieldAssessment.some(item => item.cycle
        && (!item.cycle.delta_field_assessment_deadline ||
          isDeadlineAvailable(item.cycle.delta_field_assessment_deadline)))
    }
    return false;
  }

  const isDeadlineAvailable = deadline_date => {
    if (deadline_date) {
      let today = normalizeDate(new Date());
      let delta_deadline = normalizeDate(deadline_date);
      let availability = isDateBefore(today, delta_deadline);
      return availability;
    }
    return false;
  }


  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <BGTaskLoader
        pendingTaskName={messages.field_assessment_hq}
        shouldStartMonitoring={shouldStartMonitoring}
      />
      {startMonitoring === false &&
        <div>
          <Title show={false} title={messages.title} />
          <AssessmentFilter
            onSubmit={handleSearch}
            resetFilter={resetFilter}
            partnerList={partnerLists}
            items={items}
            partner={partner}
            setSelectedPartner={setSelectedPartner}
            handleChangePartner={handleChangePartner}
            handleChange={handleChange}
            year={year}
            key={key}
            setYear={setYear}
            delta={delta}
            handleDeltaCheckboxChange={handleDeltaCheckboxChange}
          />
          {(membership && membership.type == USERS_TYPES.HQ
            && fieldAssessment && fieldAssessment.length != 0) ?
            <AssessmentActions fieldDashboard={fieldDashboard}
              year={year}
              isDeltaEnabled={checkIfDeltaCycleEnabled()}
              cycleId={getCycleId()}
              updateDeltaDeadline={updateDeltaDeadline}
              isDeltaDeadlineAvailable={isDeltaDeadlineAvailable()} />
            :
            null
          }
          {(membership && fieldAssessment && fieldAssessment.length !== 0 && membership.type != USERS_TYPES.HQ && diffDays >= 0) && <AssessmentProgressButton />}
          {deltaDeadline ?
            <div className={classes.deadline}>Deadline: {deltaDeadline}</div>
            : deadlineDate.deadline_date && <div className={classes.deadline}>Deadline: {deadlineDate.deadline_date}</div>
          }
          {membership && membership.type == USERS_TYPES.HQ
            && fieldDashboard.total != fieldDashboard.completed
            && deadlineDate.deadline_date && <div className={classes.deadline}>Deadline: {deadlineDate.deadline_date}</div>
          }
          <AssessmentTable
            messages={messages}
            sort={sort}
            items={fieldAssessment}
            loading={loading}
            totalItems={fieldDashboard.total}
            page={params.page}
            handleChangePage={handleChangePage}
            handleSort={handleSort}
            reloadDashboard={reloadDashboard}
          />

          {(membership && fieldAssessment && fieldAssessment.length !== 0 && membership.type != USERS_TYPES.HQ) ? <AssessmentProgress membership={membership} /> : null}
          {(membership && membership.type == USERS_TYPES.HQ && fieldAssessment && fieldAssessment.length !== 0) ?
            <div className={classes.root}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <div style={{ marginBottom: 30 }}>
                      <div style={{ margin: 24 }}>
                        <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light', paddingTop: 20 }}>
                          {messages.totalFA}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold' }}>{fieldDashboard.total}</span>
                        </p>
                        <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light' }}>
                          {messages.completedFA}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold' }}>{fieldDashboard.completed}</span>
                        </p>
                        <Grid className={classes.buttonGrid} >
                          <CustomizedButton buttonType={buttonType.submit} buttonText={messages.buttonText} clicked={viewList} />
                        </Grid>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              </Grid>
            </div> : null}
        </div>
      }
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    fieldAssessment: state.assessmentList.assessment,
    totalCount: state.assessmentList.totalCount,
    loading: state.assessmentList.loading,
    query: ownProps.location.query,
    location: ownProps.location,
    pathName: ownProps.location.pathname,
    BUList: state.BUListReducer.bu,
    fieldDashboard: state.fieldDashboard.dashboard,
    membership: state.userData.data.membership,
    assessmentFilter: state.form.assessmentFilter,
    assessmentProgress: state.assessmentProgress.assessmentProgress,
    deadlineDate: state.deadlineDetails.deadline,
    partnerList: state.partnerList.list,
  }
}

const mapDispatch = dispatch => ({
  loadUserData: () => dispatch(loadUser()),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  loadAssessmentList: (params) => dispatch(loadAssessmentList(params)),
  loadFieldDashboard: (params) => dispatch(loadFieldDashboard(params)),
  loadDeadLine: (params) => dispatch(loadDeadLine(params)),
  checkAssessmentCanProceed: (params) => dispatch(checkAssessmentCanProceed(params))
})

const connectedFieldAssessment = connect(mapStateToProps, mapDispatch)(Assessment)
export default withRouter(connectedFieldAssessment)