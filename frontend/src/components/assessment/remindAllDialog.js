import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  },
  button: {
    marginRight: theme.spacing(1)
  }
}));

const messages ={ 
  yes: 'YES',
  no: 'CANCEL'
}

function AlertDialog(props) {
  const {open, handleClose, sendReminder, dialogText, dialogTitle } = props;

  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{dialogTitle}</DialogTitle>
        <Divider />
        <DialogContent>
         <DialogContentText className={classes.dialogContentText} >
         <div dangerouslySetInnerHTML={{ __html: dialogText }}></div>
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.button}>
        <CustomizedButton 
          clicked={handleClose} 
          buttonText={messages.no} 
          buttonType={buttonType.cancel} />      
          <CustomizedButton 
          clicked={() => sendReminder()} 
          buttonText={messages.yes} 
          buttonType={buttonType.submit} />
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialog;
