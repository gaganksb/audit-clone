import React from 'react'
import CustomizedButton from '../common/customizedButton'
import { buttonType } from '../../helpers/constants';
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import { Field, reduxForm } from 'redux-form'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import InputLabel from '@material-ui/core/InputLabel'
import renderTypeAhead from '../common/fields/commonTypeAhead'
import { renderSelectField, textArea } from '../common/renderFields'
import { ASSESSMENT_RATINGS } from '../../helpers/constants'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  textField: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  formControl: {
    marginTop: 2,
    marginRight: theme.spacing(1),
    width: '100%',
  },
  autoCompleteField: {
    marginTop: 2,
    marginRight: theme.spacing(1),
    width: '100%',
    display: "inline-block"
  },
  label: {
    fontSize: '12px'
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  commentLabel: {
    fontSize: '12px',
    color: 'grey'
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54,
    marginTop: 20,
    width: '100%'
  },
}))


var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    'comment',
    'unhcr_focal_point',
    'partner_focal_point',
  ]

  requiredFields.forEach(field => {
    if (!values[field] || values[field] === undefined) {
      errors[field] = "Required";
    }
  });

  requiredFields.forEach(field => {
    if (field === 'comment') {
      if (values.comment == '' || (values.comment && values.comment.message == '')) {
        errors[field] = 'Required'
      }
    }
    if (field === 'unhcr_focal_point') {
      if (values.unhcr_focal_point == '' || (values.unhcr_focal_point && values.unhcr_focal_point.email == '')) {
        errors[field] = 'Required'
      }
    }
  })

  return errors
}

function EditDialog(props) {
  const {
    open, handleClose, handleSubmit, partner, userList, handleInputChange, handleChange,
    handlePartnerChange, partnerList, initialValues
  } = props;

  const classes = useStyles()
  const [score, setScore] = React.useState('1')
  const [comment, setComment] = React.useState('')

  const messages = {
    submit: "Submit",
    cancel: "Cancel"
  }

  React.useEffect(() => {
    setScore(partner && partner.score.id.toString())
    setComment(partner && partner.comment && partner.comment.message)
  }, [partner, open])

  const defaultValue = {
  }

  const getOptionLabel = option => {
    if (option.name && option.email) {
      let label = option.name + " - " + option.email;
      return label;
    } else {
      return option.fullname;
    }
  }

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle>{"Edit Assessment Rating"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText>
            <form className={classes.container} onSubmit={handleSubmit}>
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <div>
                  <InputLabel className={classes.inputLabel}>Partner</InputLabel>
                  <p className={classes.value}>{partner && partner.partner.legal_name}</p>
                </div>
                <div>
                  <InputLabel className={classes.inputLabel}>BU + PA ID</InputLabel>
                  <p className={classes.value}>{partner && partner.business_unit.code} {partner && partner.agreement && partner.agreement.number}</p>
                </div>

                <InputLabel className={classes.inputLabel}>Score</InputLabel>
                <Field
                  name='newScore'
                  component={renderSelectField}
                  className={classes.formControl}
                  onChange={e => {
                    setScore(e.target.value)
                  }}
                  selectedItem={score}
                >
                  {ASSESSMENT_RATINGS.map(item => (
                    (item.key > 0) && <option key={item.key} value={item.key} className={classes.menu}>{item.value}</option>
                  ))}
                </Field>

                <InputLabel className={classes.inputLabel}>UNHCR Audit Focal Point</InputLabel>
                <Field
                  name="unhcr_focal_point"
                  margin="none"
                  component={renderTypeAhead}
                  onChange={handleChange}
                  handleInputChange={handleInputChange}
                  options={userList ? userList : []}
                  getOptionLabel={(option => option.name ? getOptionLabel(option) : initialValues.unhcr_focal_point ? initialValues.unhcr_focal_point.fullname : null)}
                  className={classes.autoCompleteField}
                  defaultValue={initialValues.unhcr_focal_point ? initialValues.unhcr_focal_point.fullname : null}

                />
                <InputLabel className={classes.inputLabel}>UNHCR Audit Alternate Focal Point</InputLabel>
                <Field
                  name="unhcr_alt_focal_point"
                  margin="normal"
                  component={renderTypeAhead}
                  onChange={handleChange}
                  handleInputChange={handleInputChange}
                  options={userList ? userList : []}
                  getOptionLabel={(option => option.name ? getOptionLabel(option) : initialValues.unhcr_alt_focal_point ? initialValues.unhcr_alt_focal_point.fullname : null)}
                  className={classes.autoCompleteField}
                  defaultValue={initialValues.unhcr_alt_focal_point ? initialValues.unhcr_alt_focal_point.fullname : null}
                />
                <InputLabel className={classes.inputLabel}>Partner Audit Focal Point</InputLabel>
                <Field
                  name="partner_focal_point"
                  margin="normal"
                  component={renderTypeAhead}
                  onChange={handlePartnerChange}
                  handleInputChange={handleInputChange}
                  options={partnerList ? partnerList : []}
                  getOptionLabel={(option => option.name ? getOptionLabel(option) : initialValues.partner_focal_point ? initialValues.partner_focal_point.fullname : null)}
                  className={classes.autoCompleteField}
                  defaultValue={initialValues.partner_focal_point ? initialValues.partner_focal_point.fullname : null}
                />
                <InputLabel className={classes.inputLabel}>Partner Audit Alternate Focal Point</InputLabel>
                <Field
                  name="partner_alt_focal_point"
                  margin="normal"
                  component={renderTypeAhead}
                  onChange={handlePartnerChange}
                  handleInputChange={handleInputChange}
                  options={partnerList ? partnerList : []}
                  getOptionLabel={(option => option.name ? getOptionLabel(option) : initialValues.partner_focal_point ? initialValues.partner_alt_focal_point.fullname : null)}
                  className={classes.autoCompleteField}
                  defaultValue={initialValues.partner_alt_focal_point ? initialValues.partner_alt_focal_point.fullname : null}
                />
                {score === '5' && (
                  <div style={{ width: '100%' }}>
                    <InputLabel className={classes.inputLabel}>Comment</InputLabel>
                    <Field
                      name='comment'
                      comingValue={comment}
                      component={textArea}
                      className={classes}
                      onChange={e => {
                        setComment(e.target.value)
                      }}
                    />
                  </div>
                )}
                <Grid className={classes.buttonGrid}>
                  <CustomizedButton
                    clicked={handleClose}
                    buttonType={buttonType.cancel}
                    buttonText={messages.cancel}
                  />
                  <CustomizedButton buttonText={messages.submit} buttonType={buttonType.submit}
                    type="submit" />
                </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  )
}

const editAssessment = reduxForm({
  form: 'editPartnerRating',
  validate,
  enableReinitialize: true,
  destroyOnUnmount: true,
  shouldValidate: () => true,
})(EditDialog)

const mapStateToProps = (state, ownProps) => {
  let initialValues = {
    newScore: ownProps.partner && ownProps.partner.score.id,
    comment: ownProps.partner && ownProps.partner.comment,
    unhcr_focal_point: ownProps.partner && ownProps.partner.focal_points.unhcr_focal_point,
    unhcr_alt_focal_point: ownProps.partner && ownProps.partner.focal_points.unhcr_alt_focal_point,
    partner_focal_point: ownProps.partner && ownProps.partner.focal_points.partner_focal_point,
    partner_alt_focal_point: ownProps.partner && ownProps.partner.focal_points.partner_alt_focal_point
  }
  return {
    userList: state.assessmentUsers.assessmentUser,
    partnerList: state.assessmentUsers.partnerUser,
    initialValues
  }
}

const mapDispatchToProps = dispatch => ({
})



export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(editAssessment)