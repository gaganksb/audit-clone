import React, { useEffect, useState } from 'react';
import { loadFieldDashboard } from '../../reducers/fields/fieldDashboard';
import AssessmentProgress from './progress/assessmentProgress';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import R from 'ramda';
import { browserHistory as history } from 'react-router';
import Title from '../common/customTitle';

const messages = {
  title: 'Field Assessment Progress',
  progressHeader: [
    { title: 'Business Unit', align: 'left', field: 'business_unit__code' },
    { title: 'Total', align: 'left', field: 'total' },
    { title: 'Completed', align: 'left', field: 'completed' },
    { title: 'Status', align: 'left', field: 'Finished' }
  ],
 }

 const buProgress = (props) => {
  const { loadFieldDashboard, totalBUList, progressList, query, loading } = props;
  const [params, setParams] = useState({page: 0}); 

  useEffect(() => {
    let params= { ...query, partial: false }
    loadFieldDashboard(params);
  }, [query]);

  useEffect(() => {
    let params= { ...query, partial: false }
    loadFieldDashboard(params);
  }, []);

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  return (
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
        <Title show={true} title={messages.title} handleClick={() => history.push('/fieldAssessment')}/>
        <AssessmentProgress
          handleChangePage={handleChangePage}
          loading={loading}
          messages={messages}
          items={progressList}
          totalItems={totalBUList}
          page={params.page}
        />
      </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    query: ownProps.location.query,
    progressList: state.fieldDashboard.dashboard.results,
    totalBUList: state.fieldDashboard.totalCount,
    loading: state.fieldDashboard.loading
  }
};

const mapDispatchToProps = dispatch => ({
    loadFieldDashboard: (params) => dispatch(loadFieldDashboard(params))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(buProgress);

export default withRouter(connected)