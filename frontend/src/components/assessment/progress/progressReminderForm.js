import React, { useState } from 'react'
import CustomizedButton from '../../common/customizedButton'
import {buttonType} from '../../../helpers/constants'
import Divider from '@material-ui/core/Divider'
import { makeStyles } from '@material-ui/core/styles'
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core'
import { Grid, TextField, Typography } from "@material-ui/core"
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import { Autocomplete } from '@material-ui/lab'
import { loadFieldMemberList } from '../../../reducers/fields/fieldMemberList'
import CustomTextArea from '../../common/fields/textArea';
import CustomAutoCompleteField from '../../common/fields/autocompleteField';
// import {renderTextField} from '../../common/renderFields';
// import InputLabel from '@material-ui/core/InputLabel';

const messages = {
    finalize: "Finalize",
    title: "Send Reminder to Field Office",
    comment: "Comments",
    content: (fo)=>`Send a notification to Field Office ${fo} to Remind them to complete their Field Assessments`,
    cancel: "Cancel",
    submit: "Submit",
    user: "User"
}

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },    
    container: {
        display: 'flex',
        width: '100%'
    },
    subText: {
        fontSize: 14,
        color: '#3A8FCC',
        marginTop: 24,
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54
    },
    button: {
        margin: theme.spacing(1),
        minWidth: 85,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    autoCompleteField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 500
    },
    formControl: {
        width: '100%'
    },
    dialogContentText: {
        marginTop: theme.spacing(3),
    }
}))

const ProgressReminder = (props) => {
    const {
        open,
        handleDialogueBox,
        handleSubmit,
        fieldMemberList,
        loadFieldMemberList,
        selectedOffice,
        setSelectedUsers
    } = props
    const classes = useStyles()

    const handleChange = e => {
        const params = { name: e.target.value, field_office: selectedOffice.field_office.id }
        loadFieldMemberList(selectedOffice.field_office.id, params)
    }

    const handleValueChange = val => {
        setSelectedUsers(val)
    }

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                className={classes.root}
                fullWidth
                maxWidth={'lg'}
            >
                <DialogTitle>
                    {messages.title}
                </DialogTitle>
                <Divider />
                <DialogContent>
                    <Typography style={{ margin: "10px 0px 10px 0px" }}>
                        {selectedOffice&&messages.content(selectedOffice.field_office.name)}
                    </Typography>
                    <Grid container >
                        <form
                            className={classes.container}
                            onSubmit={handleSubmit}
                        >
                            <div style={{ width: '100%' }}>
                                <Field
                                    name='comment'
                                    required
                                    fullWidth
                                    label={messages.comment}
                                    className={classes.textField}
                                    component={CustomTextArea}
                                />
                                <Field
                                    name='user'
                                    label={messages.user}
                                    className={classes.autoCompleteField}
                                    component={CustomAutoCompleteField}
                                    handleValueChange={handleValueChange}
                                    onChange={handleChange}
                                    items={fieldMemberList}
                                    multiple={true}
                                />
                                <div className={classes.buttonGrid}>
                                     <CustomizedButton buttonType={buttonType.cancel} 
                                      buttonText={messages.cancel} 
                                      clicked={() => {handleDialogueBox(false)}} />

                                    <CustomizedButton buttonType={buttonType.submit} 
                                      type="submit"
                                      buttonText={messages.submit} 
                                       />
                                </div>
                            </div>
                        </form>
                    </Grid>
                </DialogContent>
                <Divider />
            </Dialog>
        </div>
    )
}

const ProgressReminderForm = reduxForm({
    form: 'ProgressReminderForm',
    enableReinitialize: false,
})(ProgressReminder)

const mapStateToProps = (state, ownProps) => {
    return {
        query: ownProps.location.query,
        fieldMemberList: state.fieldMemberList.list
    }
}

const mapDispatchToProps = dispatch => ({
    loadFieldMemberList: (id, params) => dispatch(loadFieldMemberList(id, params)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProgressReminderForm))
