import React, { useState, useEffect } from 'react';
import AssessmentProgressForm from './assessmentProgressForm'
import CustomizedButton from '../../common/customizedButton'
import { buttonType } from '../../../helpers/constants';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import {
    checkAssessmentCanProceed, addFieldAssessmentProgress, loadAssessmentProgress
} from '../../../reducers/assessmentReducer/assessmentProgress'
import { USERS_TYPES } from '../../../helpers/constants';
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { reset } from 'redux-form';
import Grid from '@material-ui/core/Grid'
import _ from 'lodash';

const messages = {
    finalize: "Complete",
    toolTip: "Tooltip text to be provided",
    sendToHQ: "Send To HQ",
    success: "Submitted Successfully!",
    error: "Action Failed!"
}

const useStyles = makeStyles(theme => ({
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-start',
        width: '100%',
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
    }
}));

const AssessmentProgressButton = (props) => {
    const {
        canCompleteFieldAsses, query, membership, addProgress, postError,
        fieldAssessment, checkAssessmentCanProceed, assessmentProgress, reset, loadAssessmentProgress, successReducer
    } = props;
    const classes = useStyles();
    const [showDialogueBox, setDialogueBox] = useState(false)
    const handleDialogueBox = (args) => {
        setDialogueBox(args)
    }

    useEffect(() => {
        const params = {
            budget_ref: query.budget_ref,
            field_office: membership && membership.field_office.id
        }
        query.budget_ref && membership && membership.type == USERS_TYPES.FO && checkAssessmentCanProceed(params)
    }, [membership, query])

    const handleSubmit = (values) => {
        let pending_with;
        if (membership && membership.role === "REVIEWER" && membership.type === "FO") {
            pending_with = "APPROVER";
        } else if (membership && membership.role === "APPROVER" && membership.type === "FO") {
            pending_with = "HQ";
        } else if (membership.type === USERS_TYPES.HQ) {
            pending_with = "COMPLETED";
        }

        const params = {
            budget_ref: query.budget_ref,
            field_office: membership && membership.field_office.id
        }
        const body = {
            pending_with,
            "budget_ref": params.budget_ref,
            "field_office": params.field_office,
            "comments": [{
                "message": values.comment
            }]
        }
        if (membership && membership.type == USERS_TYPES.HQ) {
            delete body.field_office;
        }
        membership && (membership.type == USERS_TYPES.FO || membership.type == USERS_TYPES.HQ) && addProgress(body).then((res) => {
            checkAssessmentCanProceed(params);
            loadAssessmentProgress(params)
            handleDialogueBox(false)
            successReducer(messages.success)
        }).catch(error => {
            postError(error, messages.error)
        })
    }

    const pending_with = _.get(assessmentProgress, ['assessmentProgress', '0', 'pending_with'], '');
    return (
        <React.Fragment>
            {
                ((fieldAssessment.length > 0
                    && (
                        (membership.role === 'REVIEWER' && pending_with === 'REVIEWER')
                        || (["REVIEWER", "APPROVER"].includes(pending_with) && membership.role === 'APPROVER')
                    ))
                    || assessmentProgress && assessmentProgress.assessmentProgress.length == 0) ?
                    <Grid className={classes.buttonGrid}>
                        <CustomizedButton buttonType={buttonType.submit}
                            disabled={!canCompleteFieldAsses}
                            buttonText={membership.role === "REVIEWER" ? messages.finalize : messages.sendToHQ}
                            clicked={() => { reset(); handleDialogueBox(true) }} />
                    </Grid>
                    : null
            }
            <AssessmentProgressForm
                open={showDialogueBox}
                handleDialogueBox={handleDialogueBox}
                onSubmit={handleSubmit}
            />
        </React.Fragment>
    )
}

const mapState = (state, ownProps) => {
    return {
        fieldAssessment: state.assessmentList.assessment,
        query: ownProps.location.query,
        membership: state.userData.data.membership,
        canCompleteFieldAsses: state.assessmentProgress.canProceed,
        assessmentProgress: state.assessmentProgress
    }
}
const mapDispatch = (dispatch) => {
    return {
        loadAssessmentProgress: (params) => dispatch(loadAssessmentProgress(params)),
        addProgress: (body) => dispatch(addFieldAssessmentProgress(body)),
        postError: (error, message) => dispatch(errorToBeAdded(error, 'AddProgress', message)),
        checkAssessmentCanProceed: (params) => dispatch(checkAssessmentCanProceed(params)),
        reset: () => dispatch(reset('FinalizeAssessmentProgressForm')),
        successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    }
}
const connectedProgressButton = connect(mapState, mapDispatch)(AssessmentProgressButton)
export default withRouter(connectedProgressButton)