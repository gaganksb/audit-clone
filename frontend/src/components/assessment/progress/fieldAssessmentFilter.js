import React, {useState} from 'react';
import { Grid, TextField, Paper, Tooltip, Fab } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../common/customizedButton';
import {buttonType, filterButtons} from '../../../helpers/constants';
import { reduxForm, Field, reset, change } from 'redux-form';
import {connect} from 'react-redux';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import InputLabel from '@material-ui/core/InputLabel';

const messages = {
    title: "Field Office"
}

const type_to_search = 'field__name'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        margin: theme.spacing(3)
    },
    container: {
        display: 'flex',
        paddingBottom: 8
    },
    inputLabel: {
      fontSize: 14,
      color: '#344563',
      marginTop: 18,
      paddingLeft: 14
      },
    buttonGrid: {
        margin: theme.spacing(3),
        display: 'flex',
        justifyContent: 'flex-end',
      },
    paper: {
        color: theme.palette.text.secondary,
    },
    fab: {
        margin: theme.spacing(1),
    },
    autoCompleteField: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
      width: '80%',
      marginTop: 2
  },
}))

const FieldOffice = (props) => {
    const {fieldList, updateField, handleSubmit, handleChange, reset, resetFilter} = props;
    const classes = useStyles()
    const [office, setOffice]= React.useState()
    const [key, updateKey] = React.useState('default')

    var date = new Date()

    const handleValueChange = val => {
    setOffice(val)
  }

function resetFilters() {
  setOffice([]);
  resetFilter();
}

    return (
        <React.Fragment>
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <form className={classes.container} onSubmit={handleSubmit}>
                        <Grid item sm={10}>
                        <InputLabel className={classes.inputLabel}>{messages.title}</InputLabel>
                            <Field
                                key={key}
                                name='fieldOffice'
                                className={classes.autoCompleteField}
                                component={CustomAutoCompleteField}
                                handleValueChange={handleValueChange}
                                onChange={handleChange}
                                items={fieldList}
                                comingValue={office}
                                type_to_search={type_to_search}
                            />
                        </Grid> 
                        <Grid item sm={2} className={classes.buttonGrid}>
                            <CustomizedButton buttonType={buttonType.submit}
                                buttonText={filterButtons.search}
                                type="submit" />
                            <CustomizedButton buttonType={buttonType.submit}
                                reset={true}
                                clicked={() => { reset; resetFilters(); updateKey(date.getTime()) }}
                                buttonText={filterButtons.reset}
                            />
                        </Grid>
                    </form>
                </Paper>
            </div>
        </React.Fragment>
    )
}

const FieldOfficeForms = reduxForm({
    form: 'FieldOfficeForm',
    enableReinitialize: true,
})(FieldOffice);

const mapState = (state) => ({
    
})

const mapDispatch = (dispatch) => {
    return{
  reset: () => dispatch(reset('FieldOfficeForm')),
  updateField: (form, field, newValue) => dispatch(change(form, field, newValue))
    }
}

export default connect(mapState, mapDispatch)(FieldOfficeForms);
