import React from 'react';
import { Grid, Paper } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles'
import { normalizeDate } from '../../../helpers/dates';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    paper: {
        color: theme.palette.text.secondary,
        minHeight: 150
    },
    expandHeading: {
        fill: "#a7b1be",
        fontSize: "17px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    expandText: {
        fill: "black",
        color: "black",
        fontSize: "14px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    commentBox: {
        maxWidth: "100%",
    }
}))

const COMMENT = 'Comment'
const AUTHOR = 'Author: '
const DATE = 'Date: '

const ActivityStream = (props) => {
    const classes = useStyles()
    const { assessmentProgress } = props;

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item style={{ width: "100%" }}>
                    <Paper className={classes.paper}>
                        {assessmentProgress.length>0 && assessmentProgress[0].comments.map((item, index)=> (
                            <div key={index} style={{ marginTop: 10, padding: 20 }}>
                            <div className={classes.commentBox}>
                                <span className={classes.expandHeading}>
                                    {COMMENT}
                                </span>
                                <div style={{ margin: "10px 0px 10px 0px" }}>
                                    <div dangerouslySetInnerHTML={{ __html: item.message }}></div>
                                </div>
                                <div>
                                    <span className={classes.expandHeading}>
                                        {AUTHOR}
                                    </span>
                                    <span className={classes.expandText}>
                                        {item.user.fullname} ({item.user.email})
                                </span>
                                </div>
                                <div>
                                    <span className={classes.expandHeading}>
                                        {DATE}
                                    </span>
                                    <span className={classes.expandText}>
                                        {normalizeDate(item.created_date)}
                                    </span>
                                </div>
                            </div>
                        </div>
                        ))}
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}
export default ActivityStream;