import React, { useState } from 'react';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Dialog, DialogContent, DialogTitle } from '@material-ui/core';
import { Grid, TextField, Typography } from "@material-ui/core";
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form';
import { textArea } from '../../common/renderFields'
import { withRouter } from 'react-router'

const messages = {
    finalize: "Finalize",
    title: "Mark the Field Assessment as Completed",
    comment: "Comments",
    content: "Provide a comment before finalizing the Field Assessment for Year ",
    cancel: "Cancel",
    submit: "Submit"
}

const validate = values => {
    const errors = {}
    const requiredFields = [
        'comment',
    ]
    requiredFields.forEach(field => {
        if (values.comment == '' || !values) {
            errors['comment'] = 'Required'
        }
    })
    return errors
}

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    container: {
        display: 'flex',
        width: '100%'
    },
    subText: {
        fontSize: 14,
        color: '#3A8FCC',
        marginTop: 24,
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54,
        marginTop: theme.spacing(2)
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    formControl: {
        width: '100%'
    },
    dialogContentText: {
        marginTop: theme.spacing(3),
    }
}))

const required = value => value && value != "" ? undefined : 'Comment is required';

const FinalizeAssessmentProgress = (props) => {
    const { open, handleDialogueBox, handleSubmit, query } = props;
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                className={classes.root}
                fullWidth
                maxWidth={'md'}
            >
                <DialogTitle>
                    {messages.title}
                </DialogTitle>
                <Divider />
                <DialogContent>
                    <Typography style={{ margin: "10px 0px 10px 0px" }}>
                        {messages.content + query.budget_ref}
                    </Typography>
                    <Grid container >
                        <form
                            className={classes.container}
                            onSubmit={handleSubmit}
                        >
                            <div style={{ width: '100%' }}>
                                <Field
                                    name='comment'
                                    fullWidth
                                    label={messages.comment}
                                    className={classes.textField}
                                    component={textArea}
                                    validate={[required]}
                                />
                                <div className={classes.buttonGrid}>
                                    <CustomizedButton
                                        clicked={() => handleDialogueBox(false)}
                                        buttonType={buttonType.cancel}
                                        buttonText={messages.cancel}
                                    />
                                    <CustomizedButton
                                        type="submit"
                                        buttonType={buttonType.submit}
                                        buttonText={messages.submit}
                                    />
                                </div>
                            </div>
                        </form>
                    </Grid>
                </DialogContent>
                <Divider />
            </Dialog>
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        query: ownProps.location.query,
    }
}

const mapDispatchToProps = dispatch => ({
})


const FinalizeAssessmentProgressForm = reduxForm({
    form: 'FinalizeAssessmentProgressForm',
    validate,
    enableReinitialize: false,
})(FinalizeAssessmentProgress);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FinalizeAssessmentProgressForm))