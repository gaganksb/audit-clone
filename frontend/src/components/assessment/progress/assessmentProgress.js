import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory as history } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination } from '@material-ui/core'
import { Paper, Typography } from '@material-ui/core';
import CustomizedButton from '../../common/customizedButton'
import {buttonType} from '../../../helpers/constants';
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid'
import classNames from 'classnames';
import { ExpandMore, ExpandLess } from '@material-ui/icons';
import TablePaginationActions from '../../common/tableActions';
import ProgressReminderForm from './progressReminderForm';
import AssessmentProgressFilter from './fieldAssessmentFilter';
import { loadFieldDashboard } from '../../../reducers/fields/fieldDashboard';
import { normalizeDate } from '../../../helpers/dates';
import { sendFOReminder } from '../../../reducers/fields/remind';
import { loadFieldList } from '../../../reducers/fields/fieldList';
import ListLoader from '../../common/listLoader';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import R from 'ramda'

const COMMENT = 'Comment'
const AUTHOR = 'Author: '
const DATE = 'Date: '

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#344563'
  },
  tableCellRed: {
    backgroundColor: "#F5DADA"
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8,
    backgroundColor: '#0072BC'
  },
  expandIconWrapper: {
    cursor: 'pointer',
    display: 'inline-flex'
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  expandHeading: {
    fill: "#a7b1be",
    fontSize: "17px",
    fontFamily: "Roboto-Regular, Roboto"
  },
  expandText: {
    fill: "black",
    color: "black",
    fontSize: "14px",
    fontFamily: "Roboto-Regular, Roboto"
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54,
    width: '100%'
  },
  commentBox: {
    width: "100%"
  }
}))

const messages = {
  tableHeader: ["", "Field Office", "Progress", "Confirmed", ""],
  remind: "Remind",
  subject: "Remind to complete your Field Assessments",
  success: {
    reminder: "Reminder Sent !"
  },
  title: "Field Assessment Progress"
}
const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items,
    loading,
    totalItems,
    page,
    handleChangePage,
    fieldList,
    loadFieldList,
    sendFOReminder,
    successReducer,
    postRemindError
  } = props

  const classes = useStyles()
  const [showDialogueBox, setDialogueBox] = useState(false)
  const [selectedOffice, setSelectedOffice] = useState(null)
  const [expand, setExpand] = React.useState(false)
  const [activeItem, setActiveItem] = useState(-1)
  const [searchItems, setSearchItems] = useState([])
  const [selectedUsers, setSelectedUsers] = useState([])
  const [errorMsg, updateErrorMsg] = useState(null)

  useEffect(() => {
    setSearchItems(fieldList)
  }, [fieldList])

  const handleChange = (e, v) => {
    if (v) {
      const params = { legal_name: v }
      loadFieldList(params)
    }
  }

  const handleDialogueBox = (args) => {
    setDialogueBox(args)
  }

  const handleSendReminder = (value) => {
    const body = {
      field_office_id: selectedOffice.field_office.id,
      subject: messages.subject,
      content: value.comment,
      to_emails: selectedUsers.map(user => user.email),
    }
    sendFOReminder(body).then(res => {
      setDialogueBox(false)
      successReducer(messages.success.reminder)
    }).catch(error => {
      postRemindError(error, error.response && error.response.data.detail);
      throw error
    })
  }

  const handleClose = () => {
    updateErrorMsg(null)
  }

  function resetFilter() {
    const { pathName, query } = props
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        budget_ref: query.budget_ref
      },
    })
  }

  const handleSearchFieldOffice = (value) => {
    const { fieldList, query, pathName } = props
    const fo = fieldList.find(item => item.name === value.fieldOffice)
    if (fo) {
      history.push({
        pathname: pathName,
        query: {
          budget_ref: query.budget_ref, 
          field_office: fo.id, 
          page: 1
        },
      })
      // const params = { budget_ref: query.budget_ref, field_office: fo.id, page: 1 }
      // loadFieldDashboard(params)
    }
    else {
      updateErrorMsg('The searched Field Office does not exist')
    }
  }

  function handleTableCellStyle(item) {
    if (item.total === item.completed) {
      return classes.tableCell
    } else {
      return classNames(classes.tableCell, classes.tableCellRed)
    }
  }

  return (
    <React.Fragment>
      <AssessmentProgressFilter
        onSubmit={handleSearchFieldOffice}
        fieldList={searchItems}
        resetFilter={resetFilter}
        handleChange={handleChange} />
      <Paper className={classes.root}>
        <ListLoader loading={loading}>
        <div className={classes.tableWrapper}>
        <div className={classes.heading}>
            {messages.title}
          </div>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, index) => {
                  return (
                    <TableCell key={index} id={index}>
                      {item}
                    </TableCell>
                  )
                })}
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.map(item => (
                <React.Fragment key={item.field_office.id}>
                  <TableRow>
                    <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                      {item.status && (
                        <div className={classes.expandIconWrapper}>
                          {activeItem === item.field_office.id && expand ? (
                            <ExpandLess
                              className={classes.icon}
                              onClick={() => {
                                setExpand(false)
                                setActiveItem(-1)
                              }}
                            />
                          ) : (
                              <ExpandMore
                                className={classes.icon}
                                onClick={() => {
                                  setExpand(true)
                                  setActiveItem(item.field_office.id)
                                }}
                              />
                            )}
                        </div>
                      )}
                    </TableCell>
                    <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                      {item.field_office.name}
                    </TableCell>
                    <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                      {item.completed} / {item.total}
                    </TableCell>
                    <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                      {item.pending_with && item.pending_with.pending_with && (item.pending_with.pending_with === "COMPLETED" || item.pending_with.pending_with === "HQ") ? "Yes" : "No"}
                    </TableCell>
                    <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                      {item.total !== item.completed && 
                      <Grid className={classes.buttonGrid}>
                      <CustomizedButton buttonType={buttonType.submit} buttonText={messages.remind} clicked={() => {handleDialogueBox(true); setSelectedOffice(item);}} />
                      </Grid>
                      }
                    </TableCell>
                  </TableRow>
                  {activeItem === item.field_office.id && item.status && expand && item.status.comments.map((item, index) => (
                    <TableRow key={index}>
                      <TableCell colSpan={5} className={handleTableCellStyle(item)}>
                        <div style={{ marginTop: 10 }}>
                          <div className={classes.commentBox}>
                            <span className={classes.expandHeading}>
                              {COMMENT}
                            </span>
                            <Typography style={{ margin: "10px 0px 10px 0px" }}>
                              <div dangerouslySetInnerHTML={{ __html: item.message }}></div>
                            </Typography>
                            <div>
                              <span className={classes.expandHeading}>
                                {AUTHOR}
                              </span>
                              <span className={classes.expandText}>
                                {item.user.fullname} ({item.user.email})
                            </span>
                            </div>
                            <div>
                              <span className={classes.expandHeading}>
                                {DATE}
                              </span>
                              <span className={classes.expandText}>
                                {normalizeDate(item.created_date)}
                              </span>
                            </div>
                          </div>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
                  </React.Fragment>
              )
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={0}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
          <ProgressReminderForm
            open={showDialogueBox}
            handleDialogueBox={handleDialogueBox}
            selectedOffice={selectedOffice}
            setSelectedUsers={users => setSelectedUsers(users)}
            onSubmit={handleSendReminder}
          />
          </div>
        </ListLoader>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={errorMsg}
          message={errorMsg}
          autoHideDuration={4e3}
          onClose={handleClose}
        />
      </Paper>
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  fieldList: state.fieldList.list,
  pathName: ownProps.location.pathname
})

const mapDispatchToProps = (dispatch) => ({
  loadFieldDashboard: (params) => dispatch(loadFieldDashboard(params)),
  sendFOReminder: (body) => dispatch(sendFOReminder(body)),
  loadFieldList: (params) => dispatch(loadFieldList(params)),
  postRemindError: (error, message) => dispatch(errorToBeAdded(error, 'remind', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)