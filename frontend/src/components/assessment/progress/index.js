import React, {useState, useEffect} from 'react';
import { browserHistory as history, withRouter } from 'react-router'
import {connect} from 'react-redux';
import {loadAssessmentProgress} from '../../../reducers/assessmentReducer/assessmentProgress';
import AssessmentProgressButton from './assessmentProgressButton';
import AssessmentProgressComments from './assessmentProgressComments';
import {USERS_TYPES} from '../../../helpers/constants';
import ActivityStream from './assessmentActivities';

const AssessmentProgress = (props) => {
    const {
        membership, loadAssessmentProgress, fieldAssessment,
        assessmentProgress, query
    } = props;
    
    // useEffect(()=>{
    //     const params = {
    //         budget_ref: query.budget_ref,
    //         field_office: membership && membership.field_office.id
    //     }
    //     membership && membership.type==USERS_TYPES.FO && loadAssessmentProgress(params)
    // }, [membership, query])
    
    useEffect(()=>{
        const params = {
            budget_ref: query.budget_ref,
            field_office: membership && membership.field_office.id
        }
        query.budget_ref && membership && membership.type==USERS_TYPES.FO && loadAssessmentProgress(params)
    }, [query])
    
    return (
        <React.Fragment>
            {assessmentProgress&&assessmentProgress.length > 0 && (
                <React.Fragment>
                    <AssessmentProgressComments assessmentProgress={assessmentProgress}/>
                    <ActivityStream assessmentProgress={assessmentProgress}/>
                    {/* <AssessmentProgressButton/> */}
                </React.Fragment>
            )}
        </React.Fragment>
    )
}

const mapState = (state, ownProps) => {
    return{
        membership: state.userData.data.membership,
        fieldAssessment: state.assessmentList.assessment,
        query: ownProps.location.query,
        assessmentProgress: state.assessmentProgress.assessmentProgress,
    }
}

const mapDispatch = (dispatch) => {
    return {
        loadAssessmentProgress: (params) => dispatch(loadAssessmentProgress(params)),
        addProgress: (body) => dispatch(addFieldAssessmentProgress(body)),
        postError: (error, message) => dispatch(errorToBeAdded(error, 'AddProgress', message))
    }
}
export default withRouter(connect(mapState, mapDispatch)(AssessmentProgress));
