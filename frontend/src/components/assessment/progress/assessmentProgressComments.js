import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { normalizeDate } from '../../../helpers/dates';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    paper: {
        color: theme.palette.text.secondary,
    },
    table: {
        minWidth: 500,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    tableRow: {
        borderStyle: 'hidden',
    },
    tableHeader: {
        color: '#344563',
        fontFamily: 'Roboto',
        fontSize: 14,
    },
    headerRow: {
        backgroundColor: '#F6F9FC',
        border: '1px solid #E9ECEF',
        height: 60,
        font: 'medium 12px/16px Roboto'
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    tableCellRed: {
        backgroundColor: "#F5DADA"
    },
    heading: {
        paddingLeft: 22,
        paddingTop: 19,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#344563',
        font: 'bold 16px/21px Roboto',
        letterSpacing: 0,
        paddingBottom: 20
    },
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8,
        backgroundColor: '#0072BC'
    },
    expandIconWrapper: {
        cursor: 'pointer',
        display: 'inline-flex'
    },
    expandHeading: {
        fill: "#a7b1be",
        fontSize: "17px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    expandText: {
        fill: "black",
        color: "black",
        fontSize: "14px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    commentBox: {
        maxWidth: "100%",
    }
}))

const COMMENT = 'Comment'
const AUTHOR = 'Author: '
const DATE = 'Date: '

const AssessmentProgressComment = (props) => {
    let { assessmentProgress } = props;
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                {assessmentProgress && assessmentProgress.map((item, index) => {
                    const comment = item.comments ? item.comments[0] : null
                    {
                        comment ? (
                            (
                                <div key={index} style={{ marginTop: 10, padding: 20 }}>
                                    <div className={classes.commentBox}>
                                        <span className={classes.expandHeading}>
                                            {COMMENT}
                                        </span>
                                        <div style={{ margin: "10px 0px 10px 0px" }}>
                                            <div dangerouslySetInnerHTML={{ __html: comment.message }}></div>
                                        </div>
                                        <div>
                                            <span className={classes.expandHeading}>
                                                {AUTHOR}
                                            </span>
                                            <span className={classes.expandText}>
                                                {comment.user && comment.user.fullname} ({comment.user && comment.user.email})
                                    </span>
                                        </div>
                                        <div>
                                            <span className={classes.expandHeading}>
                                                {DATE}
                                            </span>
                                            <span className={classes.expandText}>
                                                {normalizeDate(comment.created_date)}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            )
                        ) : null
                    }
                })}
            </Paper>
        </div>
    )
}
export default AssessmentProgressComment