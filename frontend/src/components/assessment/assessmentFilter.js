import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import { renderTextField, renderSelectField } from '../common/renderFields';
import CustomizedButton from '../common/customizedButton';
import { buttonType, filterButtons } from '../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import CustomAutoCompleteField from '../forms/TypeAheadFields';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { ASSESSMENT_RATINGS } from '../../helpers/constants'

const type_to_search = 'business_unit__code'
const partner_type_to_search = 'partner__legal_name';
// const currentYear = new Date().getFullYear()

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing(3)
  },
  container: {
    display: 'flex',
    paddingBottom: 6
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '85%',
    marginTop: 2
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
    marginLeft: -theme.spacing(2),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  fab: {
    margin: theme.spacing(1),
  },
  formControl: {
    minWidth: 100,
    paddingRight: theme.spacing(2),
    width: '99%',
    marginTop: 1,
    marginLeft: 8,
    paddingTop: 0,
    paddingBottom: 0
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '90%',
    marginTop: 2
  },
  delta: {
    display: 'flex',
    marginTop: theme.spacing(2),
  },
}));

function FieldAssessmentFilter(props) {
  const classes = useStyles();
  const [delta, setDelta] = useState(false);

  useEffect(() => {
    setDelta(props.delta);
  }, [props.delta])

  const { handleSubmit,
    reset,
    resetFilter,
    items,
    handleChange,
    setYear,
    partner,
    partnerList,
    key,
    setSelectedPartner,
    handleChangePartner,
    year,
    handleDeltaCheckboxChange
  } = props;
  const [bu, setBu] = React.useState();
  const yearRef = useRef();

  function resetFilters() {
    setBu([]);
    resetFilter();
  }


  const handleValueChange = val => {
    setSelectedPartner(val)
  }

  const handleBUValueChange = val => {
    setBu(val)
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={1}>
            <Grid item sm={3} >
              <InputLabel className={classes.inputLabel}>Partner</InputLabel>
              <Field
                name='partner'
                key={key}
                className={classes.autoCompleteField}
                component={CustomAutoCompleteField}
                handleValueChange={handleValueChange}
                onChange={handleChangePartner}
                items={partnerList}
                comingValue={partner}
                type_to_search={partner_type_to_search}
              />
            </Grid>
            <Grid item sm={2} style={{ marginLeft: -9 }}>
              <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
              <Field
                name='business__unit'
                key={key}
                className={classes.autoCompleteField}
                component={CustomAutoCompleteField}
                handleValueChange={handleBUValueChange}
                onChange={handleChange}
                items={items}
                comingValue={bu}
                type_to_search={type_to_search}
              />
            </Grid>
            <Grid item sm={3}>
              <InputLabel className={classes.inputLabel}>Score</InputLabel>
              <Field
                name='score__id'
                label='Score'
                margin='none'
                className={classes.formControl}
                component={renderSelectField}
              >
                {ASSESSMENT_RATINGS.map(item => (
                  <option key={item.key} value={item.key} className={classes.menu}>{item.value}</option>
                ))}
              </Field>
            </Grid>
            <Grid item sm={2} >
              <InputLabel className={classes.inputLabel}>Year</InputLabel>
              <Field
                name='year'
                type='number'
                className={classes.textField}
                margin='none'
                component={renderTextField}
                onChange={(e) => { setYear(e.target.value); yearRef.current.focus(); }}
                InputProps={{
                  inputProps: {
                    min: 2014,
                    value: year
                  },
                }}
                inputRef={yearRef}
              />
            </Grid>
            <Grid item sm={2}>
              <div className={classes.delta}>
                <InputLabel className={classes.inputLabel}>Show Delta </InputLabel>
                <Field name="delta" id="delta" component="input" style={{ marginTop: 16, width: 20, height: 20 }}
                  margin='none'
                  type="checkbox" checked={delta} onChange={handleDeltaCheckboxChange} />
              </div>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={10}></Grid>
              <Grid item sm={2} className={classes.buttonGrid}>
                <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type="submit" />
                <CustomizedButton buttonType={buttonType.submit}
                  reset={true}
                  clicked={() => { reset(); resetFilters(); }}
                  buttonText={filterButtons.reset}
                />
              </Grid>
            </Grid>

          </Grid>
        </form>
      </Paper>
    </div>
  );
}

const assessmentFilter = reduxForm({
  form: 'assessmentFilter',
  enableReinitialize: true,
})(FieldAssessmentFilter);

const mapStateToProps = (state, ownProps) => {
  let year = ownProps.year;
  return {
    initialValues: {
      year,
    }
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(assessmentFilter);

export default withRouter(connected);