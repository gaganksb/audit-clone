import React, { useState, useEffect, } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import { connect } from 'react-redux';
import { sendFOReminder } from '../../reducers/fields/remind';
import { setDeadLine, setDeltaDeadLineApi, } from '../../reducers/fields/deadline';
import CustomAlert from '../common/customAlert';
import DeadlineAlert from '../common/deadlineAlert';
import { SubmissionError } from 'redux-form';
import { withRouter } from 'react-router';
import { errorToBeAdded } from '../../reducers/errorReducer';
import R from 'ramda';
import AssessmentProgressForm from './progress/assessmentProgressForm'
import {
  addFieldAssessmentProgress, startDeltaProjectSelection,
} from '../../reducers/assessmentReducer/assessmentProgress'
import { downloadFieldAssessmentCSV } from '../../reducers/projects/projectsReducer/downloadCSVs';
import { reset } from 'redux-form';
import { showSuccessSnackbar } from '../../reducers/successReducer';
import _ from 'lodash';
import { loadDeadLine } from '../../reducers/assessmentReducer/getDeadline';
import moment from 'moment-timezone';
import { dayDifference, formatDateForPrint } from '../../helpers/dates';
import RemindAll from './remindAllDialog';
import { loadBgJob } from '../../reducers/bgTaskReducer';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
}));


const deadlineMessage = {
  button: 'Remind Field Offices',
  title: 'Set Deadline',
  message: 'Select the deadline for the completion of all the Field Assessments',
  error: 'Error',
  setDeadline: 'SET DEADLINE',
  remind: 'REMIND ALL',
  projectSelection: 'START PROJECT SELECTION',
  remindMessage: 'Send Remind Message for the completion of all the Field Assesssments',
  dialogText: 'Confirm that you want to remind all Field Offices to complete the assessement by clicking on <b>YES</b>'
}

const messages = {
  error: 'Error',
  success: {
    deadline: 'Deadline set !',
    finalized: 'Successfully Finalized !',
    reminder: 'Reminder Sent !'
  },
  field_assessment_hq: 'field_assessment_hq'
}

const errors = "Something went wrong. Please try again"

function AssessmentActions(props) {
  const classes = useStyles();
  const {
    setDeadLine, setDeltaDeadLineApi, sendFOReminder, postError, postRemindError, membership, addProgress,
    fieldDashboard, reset, successReducer, assessmentProgress, canProceedHQ, loadDeadLine,
    deadline, loadBgJob, downloadFieldAssessmentCSV, year, isDeltaEnabled, cycleId, updateDeltaDeadline,
    startDeltaProjectSelection,
    isDeltaDeadlineAvailable,
  } = props;
  const [open, setOpen] = useState({ state: false, action: null })
  const [show, setShow] = useState(true);
  const [showProjectSelectionButton, setShowProjectSelectionButton] = useState(false);

  useEffect(() => {
    if (isDeltaEnabled) {
      setShowProjectSelectionButton((!isDeltaDeadlineAvailable) || (fieldDashboard.completed == fieldDashboard.total));
    } else {
      setShowProjectSelectionButton(false);
    }
  }, [isDeltaEnabled, fieldDashboard])


  let diffDays = null;

  if (deadline) {
    // let date = new Date(Date.now()).toLocaleString().split(',')[0]
    const date = formatDateForPrint(new Date(Date.now()))
    let deadlineDate = moment(deadline.deadline_date).format('YYYY-MM-DD')
    diffDays = dayDifference(new Date(deadlineDate), new Date(date))
  }

  const handleDialogueBox = (args) => {
    setOpen({ ...open, state: args })
  }

  function handleDeadlineClose() {
    setOpen({ ...open, state: false });
  }

  const handleSubmit = (values) => {
    const { query } = props;

    if (isDeltaEnabled) {
      const body = {
        field_assessment_deadline: values.deadline
      }
      return setDeltaDeadLineApi(cycleId, body).then((response) => {
        handleDeadlineClose();
        successReducer(messages.success.deadline);
        updateDeltaDeadline(response.field_assessment_deadline);
      }).catch((error) => {
        postRemindError(error, error.response && error.response.data.detail);
        throw error
      });
    } else {
      const body = {
        budget_ref: query.budget_ref,
        deadline: values.deadline
      }
      return setDeadLine(body).then(() => {
        handleDeadlineClose();
        loadDeadLine({ budget_ref: query.budget_ref })
        successReducer(messages.success.deadline)
      }).catch((error) => {
        postRemindError(error, error.response && error.response.data.detail);
        throw error
      });
    }
  }

  const handleAssessmentSubmit = (values) => {
    const { query } = props;
    let status = "COMPLETED";

    const body = {
      "budget_ref": query.budget_ref,
      "pending_with": status,
      "comments": [{
        "message": values.comment
      }],
    }

    if (isDeltaEnabled) {
      body.type = "delta";
      startDeltaProjectSelection(body).then((res) => {
        successReducer(messages.success.finalized)
        setShow(false)
        handleDialogueBox(false)
        loadBgJob({ "pending_task": messages.field_assessment_hq })
      }).catch(error => {
        postError(error, messages.error)
      })
    } else {
      addProgress(body).then((res) => {
        successReducer(messages.success.finalized)
        setShow(false)
        handleDialogueBox(false)
        loadBgJob({ "pending_task": messages.field_assessment_hq })
      }).catch(error => {
        postError(error, messages.error)
      })
    }


  }

  const sendReminder = () => {
    const { query } = props;
    const body = {
      budget_ref: query.budget_ref,
    }
    return sendFOReminder(body).then(() => {
      handleDeadlineClose();
      successReducer(messages.success.reminder)
    }).catch((error) => {
      postRemindError(error, error.response && error.response.data.detail);
      throw error
    });
  }

  const pendingWithHQ = canProceedHQ && canProceedHQ.response && canProceedHQ.response.find(item => item.pending_with == "HQ")
  const isCompleted = canProceedHQ && canProceedHQ.response && canProceedHQ.response.length > 0 && canProceedHQ.response.every(item => item.pending_with == "COMPLETED")

  function loadCSVs() {
    let params = {};
    downloadFieldAssessmentCSV(year).then((response) => {
      successReducer("An email will be send to you with the details")
    }).catch((error) => {
      postError(error, error.response && error.response.data || errors)
      throw new SubmissionError({
        _error: errors
      })
    })
  }

  const showDeadlineButton = () => {
    return <CustomizedButton
      clicked={() => { setOpen({ state: true, action: 'Deadline' }) }}
      buttonType={buttonType.submit}
      buttonText={deadlineMessage.setDeadline} />
  }

  const shouldShowDeltaProjectDeadlineButton = () => {
    if (isDeltaEnabled) {
      return fieldDashboard.completed !== fieldDashboard.total;
    }
    return false;
  }

  const getProjectSelectionButton = () => {
    return <CustomizedButton
      clicked={() => { reset(); setOpen({ state: true, action: 'Progress' }) }}
      buttonType={buttonType.submit}
      buttonText={deadlineMessage.projectSelection}
    />
  }


  return (
    <div className={classes.root}>

      <div>
        {membership.type === 'HQ' && <CustomizedButton
          clicked={(loadCSVs)}
          buttonType={buttonType.submit}
          buttonText={'Export'}
        />}
        {(!show || isCompleted) ?
          null
          : (
            <React.Fragment>
              {shouldShowDeltaProjectDeadlineButton() ? showDeadlineButton()
                : canProceedHQ && (((canProceedHQ.response
                  && canProceedHQ.response.length === 0) || (pendingWithHQ && canProceedHQ.total_assessments !== pendingWithHQ.pending_with__count)))
                && showDeadlineButton()}

              {canProceedHQ && (((canProceedHQ.response && canProceedHQ.response.length === 0) || (pendingWithHQ && canProceedHQ.total_assessments !== pendingWithHQ.pending_with__count))) && <CustomizedButton
                clicked={() => { setOpen({ state: true, action: 'RemindAll' }) }}
                buttonType={buttonType.submit}
                buttonText={deadlineMessage.remind}
              />}

              {showProjectSelectionButton ? getProjectSelectionButton()
                : ((diffDays && diffDays < 0) || (fieldDashboard.total == fieldDashboard.completed
                  && (canProceedHQ && pendingWithHQ && canProceedHQ.total_assessments === pendingWithHQ.pending_with__count)
                  && show))
                && getProjectSelectionButton()
              }
            </React.Fragment>
          )
        }
      </div>
      <DeadlineAlert
        title={deadlineMessage.title}
        message={deadlineMessage.message}
        open={open.state && open.action === 'Deadline'}
        onSubmit={handleSubmit}
        handleClose={handleDeadlineClose}
      />
      <AssessmentProgressForm
        open={open.state && open.action === 'Progress'}
        handleDialogueBox={handleDialogueBox}
        onSubmit={handleAssessmentSubmit}
      />
      <RemindAll
        open={open.state && open.action === 'RemindAll'}
        sendReminder={sendReminder}
        handleClose={handleDeadlineClose}
        dialogText={deadlineMessage.dialogText}
        dialogTitle={deadlineMessage.remind}
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  partnerList: state.partnerList.list,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  membership: state.userData.data.membership,
  canProceedHQ: state.assessmentProgress.canProceedHQ,
  deadline: state.deadlineDetails.deadline,
  bgJob: state.bgJobStatus.bgJob
});


const mapDispatch = dispatch => ({
  sendFOReminder: (body) => dispatch(sendFOReminder(body)),
  setDeadLine: body => dispatch(setDeadLine(body)),
  setDeltaDeadLineApi: (cycleId, body) => dispatch(setDeltaDeadLineApi(cycleId, body)),
  addProgress: (body) => dispatch(addFieldAssessmentProgress(body)),
  startDeltaProjectSelection: body => dispatch(startDeltaProjectSelection(body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'setDeadline', message)),
  postRemindError: (error, message) => dispatch(errorToBeAdded(error, 'remind', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  reset: () => dispatch(reset('FinalizeAssessmentProgressForm')),
  loadDeadLine: (params) => dispatch(loadDeadLine(params)),
  loadBgJob: (body) => dispatch(loadBgJob(body)),
  downloadFieldAssessmentCSV: (year) => dispatch(downloadFieldAssessmentCSV(year)),
});

const connected = connect(mapStateToProps, mapDispatch)(AssessmentActions);
export default withRouter(connected);