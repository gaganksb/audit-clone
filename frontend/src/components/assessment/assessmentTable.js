import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { connect } from 'react-redux'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import ListLoader from '../common/listLoader'
import { browserHistory as history, withRouter } from 'react-router'
import R from 'ramda';
import TablePaginationActions from '../common/tableActions'
import EditDialog from './editRating'
import { errorToBeAdded } from '../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'
import { loadAssessmentList, deleteFieldAssessmentApi } from '../../reducers/assessmentReducer/getAssessmentList'
import { editAssessmentRating } from '../../reducers/assessmentReducer/editAssessmentRating'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ExpandLess from '@material-ui/icons/ExpandLess'
import Tooltip from '@material-ui/core/Tooltip'
import Fab from '@material-ui/core/Fab'
import EditIcon from '@material-ui/icons/Edit'
import { USERS_TYPES } from '../../helpers/constants'
import { Typography } from '@material-ui/core'
import { normalizeDate, isDateBefore } from '../../helpers/dates'
import classNames from 'classnames';
import { loadUsersList } from '../../reducers/users/assessmentUser';
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { checkAssessmentCanProceed } from '../../reducers/assessmentReducer/assessmentProgress'
import { reset } from 'redux-form'
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import FieldAssessmentDeleteDialog from './FieldAssessmentDeleteDialog'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  noDataRow: {
    margin: 16,
    width: '100%',
    textAlign: 'center',
    position: 'absolute'
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#344563',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  tableCellRed: {
    backgroundColor: "#F5DADA"
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8,
    backgroundColor: '#0072BC'
  },
  expandIconWrapper: {
    cursor: 'pointer',
    display: 'inline-flex'
  },
  expandHeading: {
    fill: "#606060",
    fontSize: "16px",
    fontFamily: "Roboto-Regular, Roboto"
  },
  expandText: {
    fill: "black",
    color: "black",
    fontSize: "14px",
    fontFamily: "Roboto-Regular, Roboto"
  },
  commentBox: {
    maxWidth: "45%",
  }
}))

const message = {
  edit: 'Edit',
  comment: 'Comment',
  author: 'Author: ',
  date: 'Date: ',
  success: {
    editRating: 'Assessment updated successfully !'
  }
}

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const [items, setItems] = useState([]);
  const [totalItems, setTotalItems] = useState(null);

  useEffect(() => {
    if (props.items) {
      setItems([...props.items])
      setTotalItems(totalCount);
    }
  }, [props.items])

  const {
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort,
    editAssessmentRating,
    loadAssessmentList,
    membership,
    postError,
    successReducer,
    loadUsers,
    canCompleteFieldAsses,
    totalCount,
    checkAssessmentCanProceed,
    deleteFieldAssessment
  } = props

  const classes = useStyles()
  const [open, setOpen] = React.useState({ status: false, type: null })
  const [partner, setPartner] = React.useState(null)
  const [expand, setExpand] = React.useState(false)
  const [activeItem, setActiveItem] = React.useState(-1)
  const [formData, setFormData] = useState({})
  const [focal, setFocal] = useState();
  const [currentDeleteRecordId, setCurrentDeleteRecordId] = useState(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [currentDeleteRecordText, setCurrentDeleteRecordText] = useState(null);

  function handleEditAssessment(values) {
    let { id, score, comment, newScore, newComment } = values
    if (!newScore) {
      newScore = score.id
    }

    const _body = R.mergeDeepRight(values, formData)

    const body = (newScore == '5') ?
      {
        score: newScore, comment: (comment.message) ? comment.message : comment,
        unhcr_focal_point: _body.unhcr_focal_point && _body.unhcr_focal_point.id,
        unhcr_alt_focal_point: _body.unhcr_alt_focal_point && _body.unhcr_alt_focal_point.id,
        partner_focal_point: _body.partner_focal_point && _body.partner_focal_point.id,
        partner_alt_focal_point: _body.partner_alt_focal_point && _body.partner_alt_focal_point.id
      } :
      {
        score: newScore, comment: '',
        unhcr_focal_point: _body.unhcr_focal_point && _body.unhcr_focal_point.id,
        unhcr_alt_focal_point: _body.unhcr_alt_focal_point && _body.unhcr_alt_focal_point.id,
        partner_focal_point: _body.partner_focal_point && _body.partner_focal_point.id,
        partner_alt_focal_point: _body.partner_alt_focal_point && _body.partner_alt_focal_point.id
      }
    const { query } = props
    return editAssessmentRating(partner.id, body).then(() => {
      handleClose()
      loadAssessmentList(query)
      checkAssessmentCanProceed(...query, { budget_ref: query.budget_ref, field_office: membership && membership.field_office.id })
      successReducer(message.success.editRating)
    }).catch((error) => {
      postError(error, messages.error)
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error
      })
    })
  }

  const handleChange = (e, v) => {
    let params = { fullname: v, my_own: true, has_account: 'field' };
    loadUsers(params)
  }

  const handlePartnerChange = (e, v) => {
    let params = { fullname: v, partner: partner.partner.id };
    loadUsers(params)
  }

  function handleClickOpen(item, action) {
    setPartner(item)
    setFocal()
    loadUsers({ fullname: 'invalidName', my_own: null })
    loadUsers({ fullname: 'invalidName', partner: -20 })
    setOpen({ status: true, type: action })
  }

  function handleClose() {
    setOpen({ status: false, type: null })
    // reset();
  }

  function handleTableCellStyle(item) {
    //color based on the score
    if (item && item.score.value === 5 && item.score.id === 4) {
      return classNames(classes.tableCell, classes.tableCellRed)
    } else {
      return classes.tableCell
    }
  }

  const handleInputChange = async (e, v, label) => {
    if (!v) return;
    setFormData({
      ...formData,
      [label]: v
    })
  }

  const deleteFieldAssessmentRow = () => {
    setOpenDeleteDialog(false);
    deleteFieldAssessment(currentDeleteRecordId).then(response => {
      successReducer("Successfully deleted")
      props.reloadDashboard();
    }).catch(error => {
      postError(error, "Error occurred in delete")
    })
    setCurrentDeleteRecordId(null);
    setCurrentDeleteRecordText(null);
  }

  const cancelDelete = () => {
    setCurrentDeleteRecordId(null);
    setOpenDeleteDialog(false);
    setCurrentDeleteRecordText(null);
  }

  const isDeltaEnabled = item => {
    if (item && item.cycle && item.cycle.delta_enabled) {
      return true;
    }
    return false;
  }

  const isDeltaProject = item => {
    if (item && item.agreement && !item.agreement.fin_sense_check) {
      return true;
    }
    return false;
  }

  const isDeadlineAvailable = deadline_date => {
    if (deadline_date) {
      let today = normalizeDate(new Date());
      let delta_deadline = normalizeDate(deadline_date);
      let availability = isDateBefore(today, delta_deadline);
      return availability;
    }
    return false;
  }

  const isUserAllowedEdit = () => {
    return isFieldOfficeUser() && isPendingWithCurrentUser();
  }

  const isFieldOfficeUser = () => {
    return membership && membership.type == USERS_TYPES.FO;
  }

  const isPendingWithCurrentUser = () => {
    if (canCompleteFieldAsses && canCompleteFieldAsses.pending_with) {
      return (membership.role === 'REVIEWER' && canCompleteFieldAsses.pending_with === 'REVIEWER')
        || (membership.role === 'APPROVER' && ["APPROVER", "REVIEWER"].includes(canCompleteFieldAsses.pending_with));
    }
    return false;
  }

  const checkDeltaProjectEditValidity = (item) => {
    return isFieldOfficeUser()
      && isDeltaEnabled(item)
      && isDeltaProject(item)
      && isDeadlineAvailable(item.cycle.delta_field_assessment_deadline);
  }

  const checkNonDeltaProjectEditValidity = (item) => {
    return isUserAllowedEdit() && isDeadlineAvailable(item.deadline_date);
  }

  const showEditButton = item => {
    return <TableCell align='right' className={handleTableCellStyle(item)}>
      <Tooltip title={message.edit}>
        <Fab color="primary" size='small' className={classes.fab} onClick={() => handleClickOpen(item, message.edit)} style={{ backgroundColor: '#0072BC' }} >
          <EditIcon />
        </Fab>
      </Tooltip>
    </TableCell>
  }

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <div className={classes.tableWrapper}>
          <div className={classes.heading}>
            {messages.title}
          </div>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key} className={classes.tableHeader}>
                    {item.title ? (
                      <TableSortLabel
                        active={item.field === sort.field}
                        direction={sort.order}
                        onClick={() => handleSort(item.field, sort.order)}
                      >
                        {item.title}
                      </TableSortLabel>
                    ) : (
                      item.title
                    )}
                  </TableCell>
                )}

                {membership && membership.type == 'HQ' &&
                  <TableCell align={'left'} key={messages && messages.tableHeader.length + 1} className={classes.tableHeader}>
                    Action
                  </TableCell>
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {!items || (items && items.length === 0) ?
                <TableRow>
                  <TableCell>
                    <div className={classes.noDataRow}>No data for the selected criteria</div>
                  </TableCell>
                </TableRow>
                : items.map(item => (
                  <React.Fragment key={item.id}>
                    <TableRow>
                      <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                        {item.score.id === 5 && (
                          <div className={classes.expandIconWrapper}>
                            {activeItem === item.id && expand ? (
                              <ExpandLess
                                className={classes.icon}
                                onClick={() => {
                                  setExpand(false)
                                  setActiveItem(-1)
                                }}
                              />
                            ) : (
                              <ExpandMore
                                className={classes.icon}
                                onClick={() => {
                                  setExpand(true)
                                  setActiveItem(item.id)
                                }}
                              />
                            )}
                          </div>
                        )}
                      </TableCell>
                      <TableCell onClick={() => history.push("/projects/" + `${item.agreement.id}`)} className={handleTableCellStyle(item)} component="th" scope="row">
                        {item.business_unit.code}{item.agreement.number}
                      </TableCell>

                      <TableCell className={handleTableCellStyle(item)} onClick={() => history.push("/projects/" + `${item.agreement.id}`)} component="th" scope="row">
                        {item.partner.legal_name}
                      </TableCell>
                      <TableCell className={handleTableCellStyle(item)} onClick={() => history.push("/projects/" + `${item.agreement.id}`)} >
                        {
                          item.score && item.score.value === 5 && item.score.id === 4 ? (
                            <span style={{ color: "black", fontWeight: "bold" }}>{item.score.description}</span>
                          ) : (
                            <span >{item.score.description}</span>
                          )
                        }
                      </TableCell>
                      <TableCell className={handleTableCellStyle(item)} onClick={() => history.push("/projects/" + `${item.agreement.id}`)} >
                        {item.agreement.delta ? 'Yes' : 'No'}
                      </TableCell>
                      <TableCell className={handleTableCellStyle(item)} onClick={() => history.push("/projects/" + `${item.agreement.id}`)} >
                        {item.score.value}
                      </TableCell>
                      {
                        checkDeltaProjectEditValidity(item) ? showEditButton(item) : (
                          checkNonDeltaProjectEditValidity(item) ? showEditButton(item) : <TableCell className={handleTableCellStyle(item)} />
                        )
                      }

                      {membership && membership.type == 'HQ' &&
                        <TableCell className={handleTableCellStyle(item)} >
                          <Tooltip title={"Delete"}>
                            <IconButton aria-label="delete"
                              onClick={() => {
                                setOpenDeleteDialog(true);
                                setCurrentDeleteRecordId(item.id);
                                setCurrentDeleteRecordText(item.business_unit.code + '' + item.agreement.number)
                              }}>
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      }
                    </TableRow>
                    {activeItem === item.id && item.score.id === 5 && expand && (
                      <TableRow>
                        <TableCell colSpan={6} className={handleTableCellStyle(item)}>
                          <div style={{ marginTop: 16, marginBottom: 16 }}>
                            {item.comment && (
                              <div className={classes.commentBox}>
                                <span className={classes.expandHeading}>
                                  {message.comment}
                                </span>
                                <Typography>
                                  <div dangerouslySetInnerHTML={{ __html: item.comment.message }}></div>
                                </Typography>
                                <div style={{ marginTop: 16 }}>
                                  <span className={classes.expandHeading}>
                                    {message.author}
                                  </span>
                                  <span className={classes.expandText}>
                                    {item.comment.user.fullname} ({item.comment.user.email})
                                  </span>
                                </div>
                                <div>
                                  <span className={classes.expandHeading}>
                                    {message.date}
                                  </span>
                                  <span className={classes.expandText}>
                                    {normalizeDate(item.comment.created_date)}
                                  </span>
                                </div>
                              </div>
                            )}
                          </div>
                        </TableCell>
                      </TableRow>
                    )}
                  </React.Fragment>
                ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={0}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
          {open.status && open.type === message.edit &&
            <EditDialog handleClose={handleClose}
              open={open.status && open.type === message.edit}
              partner={partner}
              handleChange={handleChange}
              handlePartnerChange={handlePartnerChange}
              handleInputChange={handleInputChange}
              onSubmit={handleEditAssessment} />}
        </div>
      </ListLoader>
      <FieldAssessmentDeleteDialog open={openDeleteDialog}
        currentDeleteRecordText={currentDeleteRecordText}
        onOk={deleteFieldAssessmentRow} onClose={cancelDelete} />
    </Paper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  membership: state.userData.data.membership,
  assessmentProgress: state.assessmentProgress,
  canCompleteFieldAsses: state.assessmentProgress.canProceedHQ,
  deadlineDate: state.deadlineDetails.deadline,
  totalCount: state.assessmentList.totalCount
})

const mapDispatchToProps = (dispatch) => ({
  loadAssessmentList: (params) => dispatch(loadAssessmentList(params)),
  deleteFieldAssessment: (params) => dispatch(deleteFieldAssessmentApi(params)),
  editAssessmentRating: (id, body) => dispatch(editAssessmentRating(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editAssessmentRating', message)),
  loadUsers: params => dispatch(loadUsersList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  checkAssessmentCanProceed: (params) => dispatch(checkAssessmentCanProceed(params)),
  reset: () => dispatch(reset('editPartnerRating'))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)
