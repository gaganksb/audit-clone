import React, { useState, useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { browserHistory as history } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({ 
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
},
  button: {
  margin: theme.spacing(1),
  justifyContent: 'flex-end'
},
}));

const ErrorPage = (props) => {
  const classes = useStyles();
  const {componentStack, error} = props;
  const [displayError, updateDisplay] = useState(false)
 return (
    <div>
      <Dialog
        open={true}
        fullWidth
      >
        <DialogTitle >{"Something Failed"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
          Oops! Something went wrong. Please choose a suitable option to proceed
          </DialogContentText>
          <div className={classes.buttonGrid}>
          <Button variant="contained" className={classes.button}  onClick= {() => {history.push('/dashboard');
        location.reload()}} >Return</Button>
          <Button variant="contained" className={classes.button} onClick= {() => {history.go(-2) 
           location.reload()}} >Retry</Button>
           <Button variant="contained" className={classes.button} onClick= {() => updateDisplay(true)}>Load Details</Button>
          </div>
         {(displayError) ?<div style= {{maxHeight: 140, overflowY: 'auto'}}>
            <details style={{ whiteSpace: "pre-wrap" }}>
                {error && error.toString()}
                <hr />
                {componentStack}
              </details>
          </div>: null}
        </DialogContent>
      </Dialog>
    </div>
  )
};

export default ErrorPage


