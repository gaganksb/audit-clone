import React, { Component } from 'react';
import Title from './common/customTitle';
import CustomTabs from './layout/examples/tabs';


const messages = {
  title: 'Test Layout',
}

class TestComponent extends Component {

  render() {
    const {children} = this.props;
    return (
      <div>
        <Title title={messages.title} />
        <CustomTabs >
          {children}
        </CustomTabs>
      </div>
    );
  }
}

export default TestComponent;