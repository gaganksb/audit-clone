import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Paper from '@material-ui/core/Paper';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import ListLoader from '../common/listLoader';
import { formatMoney } from '../../utils/currencyFormatter';
import TablePaginationActions from '../common/tableActions';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { browserHistory as history, withRouter } from 'react-router'


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  }, 
  paper: {
    color: theme.palette.text.secondary,
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    '& th': {
      color: '#a0adbc',
      padding: '18px 25px'
    }
  },
  heading: {
    paddingLeft: 24,
    padding: '10px 20px',
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    display: 'flex',
    justifyContent: 'space-between'
  },
  tableRow: {
    '& td': {
      color: '#8f9eaf',
      padding: '18px 25px',
      borderBottom: 'solid 2px #f3f5f6',
      cursor: 'pointer'
    }
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  filterContainer: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      width: 20,
      height: 20,
      marginRight: 20,
      cursor: 'pointer'
    }
  }
}));

const rowsPerPage = 10;

export default function AgreementTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
        <Paper className={classes.paper}>
      <ListLoader loading={loading} >
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key}>
                    {item.title}
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map(item => (
                <TableRow key={item.email} className={classes.tableRow}>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.business_unit}
                  </TableCell>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>{item.number}</TableCell>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>{item.title}</TableCell>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>{item.agreement_type}</TableCell>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>{item.country}</TableCell>
                  <TableCell onClick={() => history.push("/projects/" + `${item.id}`)}>${formatMoney(item.budget)}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={11}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </ListLoader>
      </Paper>
    </div>
  );
}
