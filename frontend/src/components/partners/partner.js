import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import R from 'ramda';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import PartnerTable from './partnerTable';
import PartnerFilter from './partnerFilter';
import Title from '../common/customTitle';


const messages = {
  title: "Partner List",
  tableHeader: [
    {title: 'Partner Number', align: 'left', field: 'number'},
    {title: 'Partner', align: 'left', field: 'legal_name'},
    {title: 'Implementer Type', align: 'left', field: 'implementer_type__implementer_type'},
    {title: 'Partner Type', align: 'left', field: 'partner_type'},
  ],
}

const Partner = props => {
  const { partner, loadPartner, loading, totalCount, query } = props

  const [params, setParams] = useState({page: 0});  

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field, 
    order: 'desc'
  })

  useEffect(() => {
    loadPartner(query);
  }, [query]);

  function goTo(item) {
    history.push(item);
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleSort(field, order) {
    const {pathName, query} = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order,
      }),
    })
  }

  function resetFilter() {
    const {pathName} = props;

    setParams({page: 0});
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  function handleSearch(values) {
    const {pathName, query} = props;
    const {legal_name, implementer_type, number} = values;

    setParams({page: 0});
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        legal_name,
        implementer_type,
        number
      }),
    })
  }

  return (
    <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
     <Title show={false} title={messages.title} />
      <PartnerFilter onSubmit={handleSearch} resetFilter={resetFilter} />
      <PartnerTable 
        messages={messages} 
        sort={sort} 
        items={partner} 
        loading={loading}
        totalItems={totalCount}  
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  partner: state.partnerList.list,
  totalCount: state.partnerList.totalCount,
  loading: state.partnerList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
});

const mapDispatch = dispatch => ({
    loadPartner: params => dispatch(loadPartnerList(params))
});

const connectedPartners = connect(mapStateToProps, mapDispatch)(Partner);
export default withRouter(connectedPartners);