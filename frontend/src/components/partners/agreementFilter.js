import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm, reset } from 'redux-form';
import {buttonType, filterButtons} from '../../helpers/constants';
import CustomizedButton from '../common/customizedButton';
import TypeAheadField from '../forms/TypeAheadField';
import { connect } from 'react-redux';

const type_to_search = 'business_unit__code'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    padding: theme.spacing(2),
    paddingBottom: 12
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '90%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
  },
}));

function renderTextField({
  label, 
  className, 
  margin, 
  input, 
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <TextField
      label={label}
      className={className}
      margin={margin}
      InputLabelProps={{
        shrink: true,
      }}
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
    />
  );
}

function AgreementFilter(props) {
  const classes = useStyles();
  const {handleSubmit, reset, resetFilter, handleChange, items, updateField} = props;
  const [bu, setBu] = React.useState()

  const handleBUChange = (e) => {
    setBu(e.inputValue)
      handleChange(e)
  }

  function resetFilters() {
    setBu([]);
    resetFilter();
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={3}>
            <TypeAheadField 
            name="business_unit" 
            margin="normal" 
            comingValue={bu}
            items={items} 
            type_to_search={type_to_search} 
            label='Business Unit' 
            handleChange={handleBUChange}/>
        </Grid>
        <Grid item sm={3} style={{paddingLeft: 16}}>
            <Field 
              name = 'number'
              label='Agreement Number'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
        </Grid>
        <Grid items sm={3}>
             <Field 
              name='agreement_type'
              label='Agreement Type'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
        </Grid>
        <Grid items sm={2}>
           <Field 
              name='budget_ref'
              label='Year'
              type='number'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
              // onChange={(e)=>{setYear(e.target.value)}}
              InputProps={{
                inputProps: {
                  min: 2014
                }
              }}
            />
          </Grid>
          <Grid item sm={2} className={classes.buttonGrid}>
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={filterButtons.search}
              type="submit" />
            <CustomizedButton buttonType={buttonType.submit}
              reset={true}
              clicked={() => { reset(); resetFilters(); }}
              buttonText={filterButtons.reset}
            />
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

const AgreementFilterForm = reduxForm({
  form: 'agreementFilter',
  enableReinitialize: true,
})(AgreementFilter);

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: {
      budget_ref: ownProps.year,
    }
  }
}

const mapDispatch = dispatch => ({
  
  reset: () => dispatch(reset('agreementFilter'))
})

const connected = connect(
  mapStateToProps, mapDispatch
)(AgreementFilterForm)

export default connected
