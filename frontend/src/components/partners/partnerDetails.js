import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { loadPartner } from '../../reducers/partners/getPartnerDetails';
import ListLoader from '../common/listLoader';
import AgreementFilter from './agreementFilter';
import PartnerOverview from './partnerOverview';
import AgreementTable from './agreementTable';
import { loadBUList } from '../../reducers/projects/oiosReducer/buList';
import { loadPartnerAssignmentList } from '../../reducers/partners/partnerAssignmentList';
import R from 'ramda';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    minHeight: '240px',
  },
  typography: {
    marginLeft: theme.spacing(2),
  },
  titleContainer: {
    padding: '15px 30px',
    minHeight: 'unset'
  },
  titleWrapper: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      width: 20,
      height: 20,
      cursor: 'pointer',
      marginRight: 15
    }
  },
  title: {
    flexGrow: 1,
    color: '#606060',
    font: 'bold 18px/24px Roboto',
  },
  overviewContainer: {
    padding: '25px 30px 15px',
    minHeight: 'unset'
  },
}));

const messages = {
  tableHeader: [
    { title: 'Business Unit', align: 'left'},
    { title: 'Agreement Number', align: 'left'},
    { title: 'Agreement', align: 'left'},
    { title: 'Agreement Type', align: 'left' },
    { title: 'Country', align: 'left' },
    { title: 'Budget', align: 'left' },
    { title: 'TBD', align: 'left' }]
};

const PartnerDetails = props => {

  const classes = useStyles();
  const { pathName, query, loadPartnerDetails, partner, totalCount, loading, loadBUList, BUList, loadPartnerAssignmentList, partnerAssignments } = props
  const [items, setItems] = useState([])
  const currentYear = new Date().getFullYear()
  const [year, setYear] = useState(currentYear)
  const [params, setParams] = useState({page: 0}); 
  // const [ids, setId] = useState(); 
  const id = pathName.split('/').slice(-2)[1]


  useEffect(() => {
    loadPartnerDetails(id);
    loadPartnerAssignmentList(id, query)
  }, [query]);

  useEffect(() => {
    setItems(BUList)
  }, [BUList])

  
  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleChange(e) {
    if (e.inputValue) {
      let params = {
        code: e.inputValue,
      }
      loadBUList(params)
    }
  }

  function handleSearch(values) {
    const {pathName} = props;
    const {business_unit, number, agreement_type, budget_ref} = values;

    setParams({page: 0});
      
    const query =   { page: 1,
        business_unit,
        number,
        agreement_type,
        budget_ref }
    loadPartnerAssignmentList(id, query)   
  }

  function resetFilter() {

     const query =  {
        page: 1,
      }
      loadPartnerAssignmentList(id, query) 
  }
  return (
    <div>
      <Paper className={`${classes.root} ${classes.titleContainer}`}>
        <div className={classes.titleWrapper}>
          <svg onClick={() => window.history.back()} fill="#0072bc" version="1.1" id="Capa_1" x="0px" y="0px" width="459px" height="459px" viewBox="0 0 459 459" style={{enableBackground: 'new 0 0 459 459'}} xmlSpace="preserve">
            <g><g id="reply"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z"/></g></g>
          </svg>
          <Typography className={classes.title} >{partner.legal_name}</Typography>
        </div>
      </Paper>
      <Paper className={`${classes.root} ${classes.overviewContainer}`}>
        <ListLoader loading={loading}>
          <PartnerOverview
            loading={loading}
            partner={partner}
          />
        </ListLoader>
      </Paper>      
      <AgreementFilter items={items} handleChange={handleChange} onSubmit={handleSearch} year={''} resetFilter={resetFilter} />
      <AgreementTable 
      messages={messages} 
      items={partnerAssignments} 
      totalItems={totalCount}
      page={params.page}
      handleChangePage={handleChangePage} />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  partner: state.partnerDetails.partner,
  loading: state.partnerDetails.loading,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
  query: ownProps.location.query,
  partnerAssignments: state.partnerAssignmentList.assignment,
  partnerLoading: state.partnerAssignmentList.loading,  
  totalCount: state.partnerAssignmentList.totalCount,
});

const mapDispatch = dispatch => ({
  loadPartnerDetails: id => dispatch(loadPartner(id)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  loadPartnerAssignmentList: (id, params) => dispatch(loadPartnerAssignmentList(id, params))
});

const connectedPartnerDetails = connect(mapStateToProps, mapDispatch)(PartnerDetails);
export default withRouter(connectedPartnerDetails);
