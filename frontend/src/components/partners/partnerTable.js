import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../common/listLoader';
import TablePaginationActions from '../common/tableActions';
import { connect } from 'react-redux';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { browserHistory as history, withRouter } from 'react-router';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  noDataRow: {
    margin: 16,
    width: '247%',
    textAlign: 'center'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    cursor: 'pointer',
    color: '#344563'},  
    heading: {
      padding: theme.spacing(3),
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
      color: '#344563',
      font: 'bold 16px/21px Roboto',
      letterSpacing: 0,
    },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort,
  } = props;

  const classes = useStyles();


  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        <div className={classes.tableWrapper}>
        <div className={classes.heading}>
            {messages.title}
          </div>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key} className={classes.tableHeader}>
                    <TableSortLabel
                      active={item.field === sort.field}
                      direction={sort.order}
                      onClick={() => handleSort(item.field, sort.order)}
                    >
                      {item.title}
                    </TableSortLabel>
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            {(items.length == 0 && !loading)? <div className= {classes.noDataRow}>No data for the selected criteria</div> :
            <TableBody>
              {items.map(item => (
                <TableRow key={item.id} onClick={() => history.push("/partners/" + `${item.id}`)}>
                  <TableCell className={classes.tableCell}>{item.number}</TableCell>
                  <TableCell className={classes.tableCell} component="th" scope="row">
                    {item.legal_name}
                  </TableCell>
                  <TableCell className={classes.tableCell}>{(item.implementer_type)?item.implementer_type.implementer_type: null}</TableCell>
                  <TableCell className={classes.tableCell}>{item.partner_type}</TableCell>
                </TableRow>))}
            </TableBody> }
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={5}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
});

const mapDispatchToProps = (dispatch) => ({
  loadPartner: params => dispatch(loadPartnerList(params))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable);

export default withRouter(connected);
