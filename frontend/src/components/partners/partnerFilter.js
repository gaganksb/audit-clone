import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import {buttonType, filterButtons} from '../../helpers/constants';
import CustomizedButton from '../common/customizedButton';
import { renderTextField } from '../common/renderFields'
import CustomAutoCompleteField from '../forms/TypeAheadFields';
import InputLabel from '@material-ui/core/InputLabel';

const type_to_search = 'partner__legal_name';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 12
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '95%',
    marginTop: 2,
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: 546,
    marginTop: theme.spacing(2)
},
inputLabel: {
  fontSize: 14,
  color: '#344563',
  marginTop: 18,
  paddingLeft: 14
  },
  fab: {
    margin: theme.spacing(1),
  },
}));


function PartnerFilter(props) {
  const classes = useStyles();
  const {handleSubmit, reset, resetFilter, handleChange, items, partner} = props;

  const handleValueChange = val => {
    setSelectedPartner(val)
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={4} >
          <InputLabel className={classes.inputLabel}>Partner</InputLabel>
          <Field 
              name = 'legal_name'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            </Grid>
            <Grid item sm={3} >
            <InputLabel className={classes.inputLabel}>Implementer Type</InputLabel>
            <Field 
              name='implementer_type'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            </Grid>
            <Grid item sm={3} >
            <InputLabel className={classes.inputLabel}>Partner Number</InputLabel>
            <Field 
              name='number'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
          </Grid>
          <Grid item sm={2} className={classes.buttonGrid}>
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={filterButtons.search}
              type="submit" />
            <CustomizedButton buttonType={buttonType.submit}
              reset={true}
              clicked={() => { reset(); resetFilter(); }}
              buttonText={filterButtons.reset}
            />
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

export default reduxForm({
  form: 'partnerFilter',
  enableReinitialize: true,
})(PartnerFilter);
