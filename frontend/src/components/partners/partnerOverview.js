import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  typography: {
    marginLeft: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#606060'
  },
  typographybold: {
    font: 'bold 14px/19px Roboto',
    color: '#0072bc',
  },
  title: {
    flexGrow: 1,
    color: '#606060',
    font: 'bold 18px/24px Roboto',
  },
  container: {
    borderBottom: '1px solid #E9ECEF',
    '& > div': {
      marginBottom: 12,
      paddingLeft: 60,
      '& > div': {
        marginBottom: '3px'
      },
      '&:first-child': {
        paddingLeft: 0
      }
    }
  },
  columns: {
    marginBottom: 15,
  },
  titleWrapper: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    marginLeft: -18,
    '& > svg': {
      width: 20,
      height: 20,
      cursor: 'pointer',
      marginRight: 15
    }
  }
}));

const PartnerOverview = props => {

  const classes = useStyles();
  const { partner, loading } = props

  return (
<div>
<div className={classes.titleWrapper}>
  <Typography className={classes.title} style={{ marginLeft: 8 }} >Partner Details</Typography>
</div>
<Grid container spacing={2} className={classes.container}>
  <Grid item xs={6} className={classes.columns}>

    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Typography className={classes.typographybold} >Partner Legal Name</Typography>
      </Grid>
      <Grid item xs={8}>
        <Typography className={classes.typography}><div>{partner.legal_name}</div></Typography>
      </Grid>
    </Grid>
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Typography className={classes.typographybold} >Implementer Type</Typography>
      </Grid>
      <Grid item xs={8}>
        <Typography className={classes.typography}>{(partner.implementer_type)?<div>{partner.implementer_type.implementer_type}</div>:null}</Typography>
      </Grid>
    </Grid>
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Typography className={classes.typographybold} >Description</Typography>
      </Grid>
      <Grid item xs={8}>
<Typography className={classes.typography}>{(partner.implementer_type)?<div>{partner.implementer_type.description}</div>: ""}</Typography>
      </Grid>
    </Grid>
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Typography className={classes.typographybold} >Partner Number</Typography>
      </Grid>
      <Grid item xs={8}>
<Typography className={classes.typography}>{partner.number}</Typography>
      </Grid>
    </Grid>
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <Typography className={classes.typographybold} >Partner Type</Typography>
      </Grid>
      <Grid item xs={8}>
        <Typography className={classes.typography}><div>{partner.partner_type}</div></Typography>
      </Grid>
    </Grid>
    </Grid>
   </Grid>
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
});

const mapDispatch = dispatch => ({
});

const connectedPartnerOverview = connect(mapStateToProps, mapDispatch)(PartnerOverview);
export default withRouter(connectedPartnerOverview);
