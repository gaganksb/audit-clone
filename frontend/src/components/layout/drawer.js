import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';

const drawerWidth = 240;

const styles = theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  activeItem: {
    color: 'white',
    backgroundColor: '#0072BC',
    '&:hover': {
      color: 'white',
      backgroundColor: '#0072BC',
    }
  }
});

class CustomDrawer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleClick(item) {
    this.setState(prevState => (
      { [item]: !prevState[item] }
    ))
  }


  handler(children) {
    var pathName = window.location.pathname
    const { goTo, classes } = this.props;
    const { state } = this

    return children.map((subOption, key) => {
      if (!subOption.children) {
        if (subOption.name === 'Resource Library') {
          // Old resource library link https://unpp-prod.s3-eu-west-1.amazonaws.com/UNHCR+Integrity+and+Assurance+Module+-+User+Guide+for+Project+Selection+for+Audit.pdf
          // New resource libary link https://ashraf2033.github.io/am-docs/
          // Latest resource library http://unhcr-pmm-docs.unpartnerportal.org/
          return (
            <ListItem button onClick={() => window.open('http://unhcr-pmm-docs.unpartnerportal.org/', "_blank")} key={key}
              className={pathName.includes(subOption.link) ? classes.activeItem : null}>
              {subOption.icon}
              <ListItemText primary={subOption.name} style={{ paddingLeft: 25 }} />
            </ListItem>)
        }
        if (subOption.name !== 'Sign Out' && subOption.name !== 'Resource Library') {
          return (
            <ListItem button onClick={() => goTo(subOption.link)} key={key}
              className={pathName.includes(subOption.link) ? classes.activeItem : null}>
              {subOption.icon}
              <ListItemText primary={subOption.name} style={{ paddingLeft: 25 }} />
            </ListItem>)
        } else {
          return (
            <div>
              {subOption.icon}
            </div>
          )
        }
      }
      return (
        <div key={key}>
          <ListItem button onClick={() => this.handleClick(subOption.name)} style={{ paddingRight: 2 }} >
            {subOption.icon}
            <ListItemText primary={subOption.name} style={{ paddingLeft: 25 }} />
            {state[subOption.name] ?
              <ExpandLess /> :
              <ExpandMore />
            }
          </ListItem>
          <Collapse
            in={state[subOption.name]}
            timeout="auto"
            unmountOnExit
          >
            <div style={{ paddingLeft: 30 }}>
              {this.handler(subOption.children)}
            </div>
          </Collapse>
        </div>
      )
    })
  }


  render() {
    const { classes, theme, handleDrawerClose, goTo, open, menu, pathName } = this.props;

    return (
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {this.handler(menu)}
        </List>
      </Drawer>
    )
  }
}

CustomDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  menu: state.nav,
  pathName: ownProps.location ? ownProps.location.pathname : null,
});

const mapDispatchToProps = () => ({
});

const connectedCustomDrawer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomDrawer);

export default withStyles(styles, { withTheme: true })(connectedCustomDrawer);