import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logoutUser } from '../../reducers/session';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';


const styles = theme => ({
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
});

const messages = {
  signOut: 'Sign out',
};


class Logout extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    const { logout } = this.props;
    return (
      <div>
        <ListItem button onClick={logout}>
          <ExitToAppIcon />
          <ListItemText primary={messages.signOut} />
        </ListItem>
      </div>
    );
  }
}

Logout.propTypes = {
  classes: PropTypes.object,
  logout: PropTypes.func,
};

const mapDispatch = dispatch => ({
  logout: () => dispatch(logoutUser()),
});

const connectedLogout =
  connect(null, mapDispatch)(Logout);

export default withStyles(styles, { name: 'Logout' })(connectedLogout);

