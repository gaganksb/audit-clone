import React from 'react';
import { browserHistory as history } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import CustomAppBar from './appbar';
import CustomAppbar from './customAppbar';
import CustomDrawer from './drawer';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
    overflowX: 'auto'
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
});

class CustomMenu extends React.Component {
  state = {
    open: false,
  };

  constructor(props) {
    super(props);
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this.goTo = this.goTo.bind(this);
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  goTo = () => item => {
    history.push(item);
    this.setState({ open: false });
  }

  render() {
    const { classes, children } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root} >
        <CssBaseline />
        <CustomAppBar 
          open={open}
          handleDrawerOpen={this.handleDrawerOpen}
        />
        {/* <CustomAppbar 
          open={open}
          handleDrawerOpen={this.handleDrawerOpen}
        /> */}
        <CustomDrawer 
          handleDrawerClose={this.handleDrawerClose}
          open={open}
          goTo={this.goTo()}
        />
        <main 
          className={classNames(classes.content, {
            [classes.contentShift]: open,
          })}>
          <div className={classes.drawerHeader}/>
          {children}
        </main>
      </div>
    );
  }
}

CustomMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(CustomMenu);