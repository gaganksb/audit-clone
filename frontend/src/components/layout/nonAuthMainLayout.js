import React, { Component } from 'react';
import CustomMenu from './nonAuthMenu';
import { withStyles } from '@material-ui/core/styles';
import { BACKGROUND_IMG } from '../../helpers/constants';

const styles = theme => ({
  "@global": {
    body: {
      backgroundImage: `url(${BACKGROUND_IMG})`,
      backgroundRepeat  : 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
    },
  },
  root: {
    marginLeft: '20px',
    marginRight: '20px',
  },
});

class NonAuthMainLayout extends Component {

  render() {
    const { classes, children } = this.props;
    return (
      <div className={classes.main}>
        <CustomMenu>
          {children}
        </CustomMenu>
      </div>
    );
  }
}

export default withStyles(styles)(NonAuthMainLayout);
