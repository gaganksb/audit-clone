import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import UserPopover from './UserPopover'
import MessagePopover from './MessagePopover'
import { loadNewMessageList } from '../../../reducers/messages/messageList'

const styles = theme => ({
  root: {
    margin: '0 50px',
    display: 'flex'
  },
  topBarIcon: {
    width: 20,
    height: 20,
    // transform: 'scaleX(1.2)',
    fill: 'white',
    cursor: 'pointer'
  },
  dropdown: {
    marginTop: 15,
    width: 200,
    padding: '0 10px',
    marginLeft: -81,
    '&::before': {
      content: "''",
      position: 'absolute',
      top: 0,
      left: '69%',
      width: 0,
      height: 0,
      border: '8px solid transparent',
      borderBottomColor: '#fff',
      borderTop: 0,
      marginTop: -8
    },
    '& ul li': {
      borderBottom: 'solid 1px #f5f5f6',
      fontSize: '14px',
      textAlign: 'center',
      justifyContent: 'center'
    }
  },
  bellIcon: {
    position: 'relative',
    marginRight: 34,
    '& > span': {
      marginLeft: 8,
      marginTop: -30,
      width: 17,
      height: 18,
      backgroundColor: '#72BC00',
      color: 'white',
      position: 'absolute',
      borderRadius: '50%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 9
    }
  }
})

const AppBarRight = ({ classes, newMessageCount, loadNewMessages }) => {
  const [openUserPopover, setOpenUserPopover] = React.useState(false)
  const [openMessagePopover, setOpenMessagePopover] = React.useState(false)
  const anchorRefUSer = React.useRef(null)
  const anchorRefMessage = React.useRef(null)

  React.useEffect(() => {
    loadNewMessages({ did_read: false })
  }, [])

  const handleToggleUserPopover = () => {
    setOpenUserPopover(prevOpen => !prevOpen)
  }

  const handleToggleMessagePopover = () => {
    setOpenMessagePopover(prevOpen => !prevOpen)
  }

  return (
    <div className={classes.root}>
      <span
        className={`${classes.bellIcon} ${classes.topBarIcon}`}
        ref={anchorRefMessage}
        onClick={handleToggleMessagePopover}
      >
        <svg
          version="1.1" id="Capa_1" x="0px" y="0px"
          viewBox="0 0 512 512" style={{ enableBackground: "new 0 0 512 512" }}
        >
          <g><g>
            <path d="M467.812,431.851l-36.629-61.056c-16.917-28.181-25.856-60.459-25.856-93.312V224c0-67.52-45.056-124.629-106.667-143.04
              V42.667C298.66,19.136,279.524,0,255.993,0s-42.667,19.136-42.667,42.667V80.96C151.716,99.371,106.66,156.48,106.66,224v53.483
              c0,32.853-8.939,65.109-25.835,93.291l-36.629,61.056c-1.984,3.307-2.027,7.403-0.128,10.752c1.899,3.349,5.419,5.419,9.259,5.419
              H458.66c3.84,0,7.381-2.069,9.28-5.397C469.839,439.275,469.775,435.136,467.812,431.851z"
            />
          </g></g>
          <g><g>
            <path d="M188.815,469.333C200.847,494.464,226.319,512,255.993,512s55.147-17.536,67.179-42.667H188.815z"/>
          </g></g>
        </svg>
        <span>{newMessageCount || 0}</span>
      </span>
      <svg
        className={classes.topBarIcon}
        ref={anchorRefUSer}
        onClick={handleToggleUserPopover}
        x="0px" y="0px"
        viewBox="0 0 258.75 258.75"
        style={{enableBackground: 'new 0 0 258.75 258.75'}}
      >
        <g><circle cx="129.375" cy="60" r="60"/><path d="M129.375,150c-60.061,0-108.75,48.689-108.75,108.75h217.5C238.125,198.689,189.436,150,129.375,150z"/></g>
      </svg>

      <MessagePopover
        anchorRef={anchorRefMessage}
        isOpen={openMessagePopover}
        setOpenState={(val) => setOpenMessagePopover(val)}
      />

      <UserPopover
        anchorRef={anchorRefUSer}
        isOpen={openUserPopover}
        setOpenState={(val) => setOpenUserPopover(val)}
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  newMessageCount: state.messageList.totalCountNew
})

const mapDispatchToProps = dispatch => ({
  loadNewMessages: (params) => dispatch(loadNewMessageList(params))
})

const connectedAppBarRight =
  connect(mapStateToProps, mapDispatchToProps)(AppBarRight)

export default withStyles(styles, { withTheme: true })(connectedAppBarRight)
