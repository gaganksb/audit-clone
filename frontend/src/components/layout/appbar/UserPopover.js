import React from 'react'
import { connect } from 'react-redux'
import { browserHistory as history } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import { logoutUser } from '../../../reducers/session'

const styles = theme => ({
  dropdown: {
    marginTop: 15,
    width: 200,
    padding: '0 10px',
    marginLeft: -81,
    '&::before': {
      content: "''",
      position: 'absolute',
      top: 15,
      left: '48%',
      width: 0,
      height: 0,
      border: '8px solid transparent',
      borderBottomColor: '#fff',
      borderTop: 0,
      marginTop: -8
    },
    '& ul li': {
      borderBottom: 'solid 1px #f5f5f6',
      fontSize: '14px',
      textAlign: 'center',
      justifyContent: 'center'
    }
  },  
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
})

const UserPopover = ({ classes, logout, anchorRef, isOpen, setOpenState }) => {
  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setOpenState(false)
  }

  return (
    <Popper open={isOpen} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
        >
          <Paper className={classes.dropdown}>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList autoFocusItem={open} id="menu-list-grow">
                <MenuItem className={classes.menu}
                  onClick={() => {
                    history.push('/profile')
                    setOpenState(false)
                  }}
                >
                  User Profile
                </MenuItem>
                <MenuItem className={classes.menu}
                  onClick={() => {
                    logout()
                    setOpenState(false)
                  }}
                >
                  Sign Out
                </MenuItem>
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  )
}

const mapDispatch = dispatch => ({
  logout: () => dispatch(logoutUser()),
})

const connectedUserPopover =
  connect(null, mapDispatch)(UserPopover)

export default withStyles(styles, { withTheme: true })(connectedUserPopover)
