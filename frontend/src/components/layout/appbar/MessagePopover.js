import React from 'react'
import { connect } from 'react-redux'
import { browserHistory as history } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuList from '@material-ui/core/MenuList'
import { editMessage } from '../../../reducers/messages/editMessage'
import { loadNewMessageList } from '../../../reducers/messages/messageList'

const styles = theme => ({
  dropdown: {
    marginTop: 15,
    width: 200,
    padding: '0 8px',
    marginLeft: -81,
    '&::before': {
      content: "''",
      position: 'absolute',
      top: 15,
      left: '48%',
      width: 0,
      height: 0,
      border: '8px solid transparent',
      borderBottomColor: '#fff',
      borderTop: 0,
      marginTop: -8
    },
    '& ul li': {
      borderBottom: 'solid 1px #f5f5f6',
      fontSize: '14px',
      justifyContent: 'left'
    }
  },
  msgContainer: {
    display: 'flex',
    padding: '4px 4px',
    borderBottom: 'solid 1px #F1F1F3',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#E5F1F8'
    }
  },
  msgContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    '& > p': {
      fontSize: 14,
      color: '#4D565C',
      margin: '0 0 5px'
    },
    '& > span': {
      fontSize: 12,
      color: '#808495'
    }
  },
  msgHeader: {
    fontSize: 14,
    color: '#4D4F5C',
    padding: '4px 4px',
    borderBottom: 'solid 1px #f5f5f6',
    outline: 'none',
  },
  msgFooter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#0070BF',
    fontSize: 14,
    padding: '4px 4px',
    outline: 'none',
    '& > span': {
      cursor: 'pointer'
    }
  }
})

const Message = ({ msg, classes, handlePopup, editMessage }) => {
  return(
  <div
    key={msg.id}
    className={classes.msgContainer}
    onClick={() => {
      history.push({
        pathname: '/messages',
        state: {
          openMsg: msg.notification.id
        }
      })
      handlePopup(false)
      !msg.did_read && editMessage(msg.notification.id, {did_read: true} )
    }}
  >
    <div className={classes.msgContent}>
        <p>{msg.notification.name}</p>
      <span>{msg.notification.last_modified_date}</span>
    </div>
  </div>
)}

const MessagePopover = ({ classes, anchorRef, isOpen, setOpenState, newMessages, editMessage, loadNewMessages }) => {
  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }
    setOpenState(false)
  }

  return (
    <Popper open={isOpen} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          // style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
        >
          <Paper className={classes.dropdown}>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList autoFocusItem={open} id="menu-list-grow">
                <div className={classes.msgHeader}>
                  Notifications
                </div>
                {newMessages.map(msg => (
                  <Message msg={msg} classes={classes} handlePopup={setOpenState} editMessage={editMessage} loadNewMessages={loadNewMessages} />
                ))}
                <div className={classes.msgFooter}>
                  <span onClick={() => {
                      history.push('/messages')
                      setOpenState(false)
                    }}
                  >
                    View all notifications
                  </span>
                </div>
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  newMessages: state.messageList.newMessages
})

const mapDispatch = dispatch => ({
  editMessage: (id, body) => dispatch(editMessage(id, body)),
  loadNewMessages: (params) => dispatch(loadNewMessageList(params))
})

const connectedMessagePopover =
  connect(mapStateToProps, mapDispatch)(MessagePopover)

export default withStyles(styles, { withTheme: true })(connectedMessagePopover)