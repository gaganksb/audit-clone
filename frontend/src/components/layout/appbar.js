import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBarRight from './appbar/appBarRight';
import { LOGO } from '../../helpers/constants';

const drawerWidth = 240;
const messages = {
  title: 'Integrity & Assurance Module'
}

const styles = theme => ({
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    '& > div > div:last-child': {
      marginRight: 26
    }
  },
  appToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  appBarLeft: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  img: {
    maxHeight: 50,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
    color: 'white'
  },
  hide: {
    display: 'none',
  },
});

const CustomAppBar = (props) => {
  const { classes, open, handleDrawerOpen, logout } = props;

  return (
    <AppBar
      position="fixed"
      className={classNames(classes.appBar, {
        [classes.appBarShift]: open,
      })}
      style={{
        backgroundColor: '#0072BC',
        zIndex: 1103,
        boxShadow: 'none',
        opacity: 1}}
    >
      <Toolbar 
        disableGutters={!open}
        className={classes.appToolbar}
      >
        <div className={classes.appBarLeft}>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            className={classNames(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography>
            <img src={LOGO} alt="Logo" className={classes.img} />
          </Typography>
        </div>
        <Typography className={classes.grow}>
              {messages.title}
        </Typography>

        <AppBarRight/>
      </Toolbar>
    </AppBar>
  )
};

export default withStyles(styles, { withTheme: true })(CustomAppBar);
