import React, { Component } from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './layout/theme';
// component for routes that don't need to authenthicate token on the backend like login 
// and registration
class NonAuth extends Component {

  render() {
    const { children } = this.props;
    return (
      <ThemeProvider  theme={theme}>
          {children}
      </ThemeProvider>);
  }
}

export default NonAuth;
