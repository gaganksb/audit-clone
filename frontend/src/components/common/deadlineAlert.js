import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField } from '../common/renderFields';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import CustomizedButton from './customizedButton';
import { buttonType } from '../../helpers/constants';
import { dayDifference } from '../../helpers/dates';
import InputLabel from '@material-ui/core/InputLabel'


var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    'deadline'
  ]

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
    else {
      let year = ('' + new Date(values[field]).getFullYear()).length
      if (year > 4) {
        errors[field] = 'Year is out of limit'
      }

      let date = new Date(Date.now()).toLocaleString().split(',')[0]
      let deadlineDate = new Date(values[field])
      const diffDays = dayDifference(new Date(deadlineDate), new Date(date))
      if(diffDays < 0) {
        errors[field] = 'Deadline Date can not be in past'
      }
    }
  })
  return errors
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  button: {
    marginTop: theme.spacing(1),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  formControl: {
    minWidth: 160,
  },
  dialogContentText: {
    marginTop: theme.spacing(3),
    color: '#344563',
  },
  buttonGrid: {
    marginTop: 16,
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end'
  },

}));

function DeadlineAlert(props) {
  const { open, title, message, handleClose, handleSubmit } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{title}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText className={classes.dialogContentText} >
            {message}
          </DialogContentText>
          <form className={classes.container} onSubmit={handleSubmit} >
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="flex-start"
            >

              <InputLabel className={classes.inputLabel}>Set Deadline</InputLabel>
              <Field
                name='deadline'
                margin='normal'
                type='date'
                component={renderTextField}
              />
              <div className={classes.buttonGrid}>
                <CustomizedButton clicked={handleClose} buttonText={buttonType.cancel} buttonType={buttonType.cancel} />
                <CustomizedButton buttonType={buttonType.submit}
                  buttonText={title}
                  type="submit"
                />
              </div>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const deadlineForm = reduxForm({
  form: 'setDeadline',
  validate,
  enableReinitialize: true,
})(DeadlineAlert);


const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(deadlineForm);

export default withRouter(connected);
