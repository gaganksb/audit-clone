import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import Grid from "@material-ui/core/Grid";


const useStyles = makeStyles(theme => ({
  icon: {
    '&:hover': {
      cursor: 'pointer'
    }
  },
}));

function CustomDialogTitle(props) {
  const { 
    title,
    handleClose
  } = props;
  const classes = useStyles();


  return (
    <DialogTitle >
      <Grid container spacing={3}>
        <Grid item xs={6} sm={10}>
          {title}
        </Grid>
        <Grid item xs={6} sm={2} style={{textAlign: 'right'}}>
          <CloseIcon className={classes.icon} onClick={handleClose} />
        </Grid>
      </Grid>
    </DialogTitle>
  );
}


export default CustomDialogTitle;
