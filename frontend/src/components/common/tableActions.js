import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import { browserHistory as history, withRouter } from 'react-router';
import { connect } from 'react-redux';
import R from 'ramda';


const useStyles = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.primary,
  },
}));

function TablePaginationActions(props) {

  const classes = useStyles();
  const theme = useTheme();

  const { count, page, rowsPerPage, onChangePage, query, pathName } = props;

  function handleFirstPageButtonClick(event) {
    onChangePage(event, 0);

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
      }),
    });
  }

  function handleBackButtonClick(event) {
    onChangePage(event, page - 1);

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: page,
      }),
    });
  }

  function handleNextButtonClick(event) {
    onChangePage(event, page + 1);

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: page + 2,
      }),
    });
  }

  function handleLastPageButtonClick(event) {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: Math.max(0, Math.ceil(count / rowsPerPage)),
      }),
    });
  }

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="First Page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="Previous Page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="Next Page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="Last Page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};


const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
});


const connectedTablePaginationActions = connect(mapStateToProps)(TablePaginationActions);
export default withRouter(connectedTablePaginationActions);