import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
// import Button from '@material-ui/core/Button';
import MuiButton from "@material-ui/core/Button";
import { buttonType } from '../../helpers/constants';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: theme.spacing(1),
    justifyContent: 'flex-end'
  },
  customizedButton: {
    backgroundColor: props => (props.buttonType == buttonType.submit) ? '#0072BC' : (props.buttonType == buttonType.cancel) ? '#E2315A' : '#EDB57E',
    color: 'white',
    textTransform: 'uppercase',
    borderRadius: 5,
    marginLeft: props => (props.fullWidth) ? 0 : theme.spacing(2),
    height: 40,
    minWidth: 90,
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: props => (props.buttonType == buttonType.submit) ? '#0072BC' : (props.buttonType == buttonType.cancel) ? '#E2315A' : '#EDB57E'
    }
  },
  progress: {
    width: "15px !important",
    height: "15px !important",
    margin: "5px auto"
  }
}));

const Button = withStyles({
  root: {
    "&.Mui-disabled": {
      pointerEvents: "auto"
    }
  }
})(MuiButton);

const ButtonWithTooltip = (props) => {
  const { disabled, onClick, } = props;
  const adjustedButtonProps = {
    disabled: disabled,
    component: disabled ? "div" : undefined,
    onClick: disabled ? undefined : onClick
  };
  const { enableTooltip, tooltipText, ...otherProps } = props;
  return (
    enableTooltip && tooltipText ? (
      <Tooltip title={tooltipText}>
        <Button {...otherProps} {...adjustedButtonProps} />
      </Tooltip>
    ) : (
      <Button {...otherProps} />
    )
  );
};


function Buttons(props) {
  const classes = useStyles(props);
  const {
    buttonText,
    clicked,
    disabled,
    buttonType,
    fullWidth,
    type,
    reset,
    submitting,
    enableTooltip,
    tooltipText
  } = props;

  return (
    <ButtonWithTooltip
      onClick={clicked}
      style={props.disabled ? { backgroundColor: '#cccccc' } : null}
      className={classes.customizedButton}
      type={type}
      fullWidth={fullWidth}
      style={disabled ? { backgroundColor: '#D5D5D5' } : reset ? { backgroundColor: '#BF0301' } : null}
      disabled={disabled}
      enableTooltip={enableTooltip}
      tooltipText={tooltipText}
    >
      {buttonType === "Submit" && submitting ? <CircularProgress className={classes.progress} color="white" /> : buttonText}
    </ButtonWithTooltip>
  )
}

export default Buttons;