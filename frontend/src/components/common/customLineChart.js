import React, { PureComponent } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  LineChart, Line, XAxis, YAxis, Tooltip, ReferenceLine, ResponsiveContainer
} from 'recharts';

const useStyles = makeStyles(theme => ({
  tooltipContainer: {
    backgroundColor: '#fff',
    borderRadius: '10px',
    boxShadow: '-1px 3px 10px 0px rgba(0,0,0,0.3)',
    padding: '5px',
    '& > p': {
      margin: '3px'
    },
    fontSize: '12px'
  },
}));

const CustomizedDot = (props) => {
  const {
    cx, cy, stroke, payload, value,
  } = props;
  return (
    <circle cx={cx - 10} cy={cy - 10} r={25} stroke={stroke} strokeWidth={3} fill="red" />
  );
};

class CustomizedLabel extends PureComponent {
  render() {
    const {
      x, y, stroke, value,
    } = this.props;

    return <text x={x} y={y} dy={20} fill={stroke} fontSize={13} textAnchor="middle">{value}</text>;
  }
}

class CustomizedAxisTick extends PureComponent {
  render() {
    const {
      x, y, stroke, payload,
    } = this.props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={30} fill="#858997" fontWeight={600} fontSize={13} textAnchor="middle">{payload.value}</text>
      </g>
    );
  }
}

const CustomTooltip = ({ active, payload, label }) => {
  if (!active) {
    return null;
  }
  const classes = useStyles();
  return (
    <div className={classes.tooltipContainer}>
      <p className="label">{payload[0].value} / 100%</p>
      <p className="intro">{payload[1].value} / 100%</p>
    </div>
  );
};

export default class CustomLineChart extends PureComponent {
  render() {
    const {
      width,
      height,
      data,
      dataKeyX,
      dataKeys,
      xValues,
      yValues,
      domain
    } = this.props;
    return (
      <ResponsiveContainer width={width} height={height}>
        <LineChart
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
        >
          {xValues.map((item, index) => (
            <ReferenceLine key={`x-${index}`} x={item.key} stroke={item.color} label={item.label} />
          ))}
          {yValues.map((item, index) => (
            <ReferenceLine key={`y-${index}`} y={item.key} stroke={item.color} label={item.label} />
          ))}

          <XAxis
            dataKey={dataKeyX}
            padding={{ left: 30, right: 30 }}
            tickLine={false}
            axisLine={false}
            stroke={'#858997'}
            strokeWidth={1}
            height={40}
            tick={<CustomizedAxisTick/>}
          />
          <YAxis
            tick={{fontSize: '14px', fill: '#858997'}}
            tickLine={false}
            axisLine={false}
            domain={domain}
          />

          {/*<Tooltip />*/}
          <Tooltip content={<CustomTooltip />} active={true} />

          {dataKeys.map(item => (
            <Line
              type="monotone"
              dataKey={item.key}
              stroke={item.color}
              strokeWidth={2}
              dot={{ r: 4 }}
            />
          ))}
        </LineChart>
      </ResponsiveContainer>
    );
  }
}
