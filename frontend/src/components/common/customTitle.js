import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';


const styles = () => ({
  title: {
        padding: 24,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#344563',
        font: 'bold 18px/24px Roboto',
        letterSpacing: 0,
        backgroundColor: 'white',
        paddingBottom: 20
  },
  grid: {
    backgroundColor: "white",
  }
})


class Title extends Component {
    
  render() {
    const { classes, title, show, handleClick } = this.props;

    return (
      <div>
        <Grid item className={classes.grid} position='sticky'>
          <Typography
            className={classes.title}
          >
            {(show)?<ChevronLeftIcon style={{position:'relative', top: 5, cursor:'pointer'}} onClick= {handleClick}/> : null}
            {title}
          </Typography>   
        </Grid>  
      </div>
    );
  }
}


Title.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(Title);
