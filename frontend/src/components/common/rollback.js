import React, { useState } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import CustomizedButton from './customizedButton';
import {buttonType} from '../../helpers/constants';
import R from 'ramda';
import CustomAlert from './customAlert';
import { editCycle } from '../../reducers/projects/projectsReducer/editCycle';
import { loadCycle } from '../../reducers/projects/projectsReducer/getCycle';
import { loadInterimList } from '../../reducers/projects/projectsReducer/getInterimList';
import { loadFinalList } from '../../reducers/projects/projectsReducer/getFinalList';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { rollbackInterim, rollbackFinal } from '../../reducers/projects/projectsReducer/rollbackReducer';

const messages = {
  button: 'Rollback',
  title: 'Rollback to previous Stage',
  message: 'Do you really want to rollback the Project Selection to the previous Stage?',
  error: 'Error',
}

const Rollback = (props) => {
  const { cycle, editCycle, loadCycle, loadInterim, loadFinal, postError, rollbackInterim, rollbackFinal, query } = props

  const initialAlert = {
    open: false,
    title: messages.title,
    message: messages.message,
    handleClose: handleClose,
    handleYes: handleRollback
  }
  const [alert, setAlert] = useState(initialAlert);
  const [disabled, setDisabled] = useState(false);

  function handleClose() {
    setAlert(R.merge(initialAlert, { open: false }))
  }

  function handleAlert() {
    setDisabled(false);
    setAlert(R.merge(initialAlert, {open: true}));
  }

  function handleCycleChange(body) {
    const newStatus = body.status
    if ([1,2].includes(newStatus)) {
      editCycle(cycle.year, R.merge(body, {list_type: 'preliminarylist'}))
        .then(loadCycle(cycle.year))
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } else if (newStatus == 3) {
      rollbackInterim(cycle.year, body)
        .then(() => { 
          editCycle(cycle.year, body_)
            .then(loadInterim(cycle.year, query).then(loadCycle(cycle.year, query)))
        })
        .catch((error)=> { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    }  else if (newStatus == 4) {
      editCycle(cycle.year, R.merge(body, {list_type: 'interimlist'}))
        .then(loadCycle(cycle.year))
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } else if (newStatus == 5) {
      rollbackFinal(cycle.year, body)
        .then(() => { 
          editCycle(cycle.year, body_)
            // .then(loadFinal(cycle.year, query).then(loadCycle(cycle.year, query))) 
            .then(loadCycle(cycle.year, query))
        })
        .catch((error)=> { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    }  else if (newStatus == 4) {
      editCycle(cycle.year, R.merge(body, {list_type: 'finallist'}))
        .then(loadCycle(cycle.year))
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } 
  }

  function handleRollback() {
    const body ={ status: cycle.status - 1 };
    setDisabled(true);
    handleCycleChange(body);
  }

  return (
    <span>
    <CustomizedButton buttonType={buttonType.cancel} 
    buttonText={messages.button} 
    clicked={handleAlert} />
      <CustomAlert 
        {...alert}
        disabled={disabled}
      />
    </span>
  )
}


const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  query: ownProps.location.query,
});

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editCycle', message)),
  loadInterim: (year, params) => dispatch(loadInterimList(year, params)),
  loadFinal: (year, params) => dispatch(loadFinalList(year, params)),
  rollbackInterim: (year) => dispatch(rollbackInterim(year)),
  rollbackFinal: (year) => dispatch(rollbackFinal(year)),
});

const connectedRollback = connect(mapStateToProps, mapDispatch)(Rollback);
export default withRouter(connectedRollback);




