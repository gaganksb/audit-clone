import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import CustomizedButton from './customizedButton';
import {buttonType} from '../../helpers/constants';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  },
  commentField: {
    width: '100%'
  },
  invalid: {
    '& > label': {
      color: 'red'
    }
  }
}))

const NO = 'No'
const YES = 'Yes'

function CustomAlert(props) {
  const { open, title, message, handleClose, handleYes, handleNo, disabled, showComment } = props
  const [comment, setComment] = React.useState('')
  const classes = useStyles()

  return (
    <span className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle>{title}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText className={classes.dialogContentText}>
            {message}
          </DialogContentText>
          {showComment && (
            <TextField
              id="comment"
              value={comment}
              label="Comment"
              placeholder=""
              className={`${classes.commentField} ${!comment && classes.invalid}`}
              onChange={e => setComment(e.target.value)}
              multiline
            />
          )}
        </DialogContent>
        <Divider />
        <DialogActions>
        <CustomizedButton buttonType={buttonType.cancel} 
          buttonText={NO} 
          clicked={handleNo || handleClose}
          disabled={disabled} />
          <CustomizedButton buttonType={buttonType.submit} 
          buttonText={YES} 
          clicked={() => {
            if (showComment) {
              comment && handleYes(comment)
            } else {
              handleYes()
            }
            setComment('')
          }}
          disabled={disabled} />
        </DialogActions>
      </Dialog>
    </span>
  )
}

export default CustomAlert
