import React, { useState, useEffect } from 'react';
import { clearSnackbar } from "../../reducers/successReducer";
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }


export function SuccessSnackbar(props) {
  
  const { clearSnackbar, success, children } = props;

  const { successSnackbarMessage, successSnackbarOpen } = success;

  function handleClose() {
    clearSnackbar();
  }

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "left"
      }}
      open={successSnackbarOpen}
      autoHideDuration={4e3}
      onClose={handleClose}
      aria-describedby="client-snackbar">
      <Alert 
              onClose={handleClose} 
              severity="success" style={{minWidth: 316}}>
                    {(successSnackbarMessage)? successSnackbarMessage: 'Success !'}
             </Alert>
    </Snackbar>
  );
}

const mapStateToProps = state => ({
  success: state.success,
});

const mapDispatchToProps = dispatch => ({
  clearSnackbar: () => dispatch(clearSnackbar()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SuccessSnackbar);