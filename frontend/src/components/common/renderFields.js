import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputBase from '@material-ui/core/InputBase';
import React from 'react';
import Select from '@material-ui/core/Select';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

export const renderFromHelper = ({ touched, error }) => {
  if (!(touched && error)) {
    return
  } else {
    return <FormHelperText>{touched && error}</FormHelperText>
  }
}

// TODO, do not use this component but the renderFixedTextField since this component is not working property
// read the comment on the renderFixedTextField to get more information
export function renderTextField({
  label,
  className,
  margin,
  input,
  value,
  readOnly,
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <TextField
      label={label}
      className={className}
      margin={margin}
      InputProps={{
        readOnly: readOnly,
        value: value
      }}
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
      fullWidth
    />
  );
}

// TODO: this is a fix of renderTextField, note the use of `input.value` instead of `value`
// without this fix, the component was uncontrolled and the redux state was not updated property
export function renderFixedTextField({
  label,
  className,
  margin,
  input,
  value,
  readOnly,
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <TextField
      label={label}
      className={className}
      margin={margin}
      InputProps={{
        readOnly: readOnly,
        value: input.value
      }}
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
      fullWidth
    />
  );
}

// export const renderSelectField = ({
//   input,
//   label,
//   meta: { touched, error },
//   children,
//   className,
//   name,  
//   selectedItem,
//   ...custom
// }) => {
//   return(<FormControl error={touched && error} className={className} >
//     <Select
//       label={label}
//       native
//       value={selectedItem}
//       {...input}
//       {...custom}
//       inputlabelprops={{
//         shrink: true,
//       }}
//     >
//       {children}
//     </Select>
//     {renderFromHelper({ touched, error })}
//   </FormControl>)}

export const renderSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  className,
  selectedItem,
  defaultValue,
  key,
  disabled,
  ...custom
}) => (
    <FormControl error={touched && error} className={className} disabled={disabled}>
      <Select
        key={key}
        value={selectedItem}
        className={className}
        defaultValue={defaultValue}
        {...input}
        {...custom}
        inputlabelprops={{
          shrink: true,
        }}
      >
        {children}
      </Select>
      {renderFromHelper({ touched, error })}
    </FormControl>
  )

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      flexGrow: 1,
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(3),
      minWidth: '90%'
    },
  },
  input: {
    borderRadius: 10,
    position: 'relative',
    backgroundColor: '#F0F2F7',
    border: '1px solid #E3E8F0',
    fontSize: 16,
    width: '98%',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: theme.palette.primary.main,
    },
  },
}))(InputBase);

export function renderTextArea({
  label,
  className,
  margin,
  input,
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <div style={{ width: '100%' }}>
      <BootstrapInput
        style={{ width: '100%' }}
        label={label}
        margin="none"
        variant="outlined"
        variant="filled"
        inputlabelprops={{
          shrink: true
        }}
        multiline={true}
        rows={3}
        rowsMax={10}
        error={touched && invalid}
        {...input}
        {...custom}
      />
      {touched && ((error && <div style={{ color: "red", fontSize: "12px", fontWeight: "bold" }}>{error}</div>))}
    </div>
  );
}

const CommentInput = withStyles(theme => ({
  root: {
    marginTop: 10,
    'label + &': {
      flexGrow: 1,
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(3),
      minWidth: '90%'
    },
  },
  input: {
    position: 'relative',
    border: '1px solid #ced4da',
    fontSize: 16,
    width: '100%',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: theme.palette.primary.main,
    },
  },
}))(InputBase);


export function textArea({
  label,
  className,
  margin,
  input,
  meta: { touched, invalid, error },
  comingValue,
  ...custom
}) {
  return (
    <div style={{ width: '100%', marginTop: '16px' }}>
      <CommentInput
        style={{ width: '100%', marginTop: 0 }}
        label={label}
        margin="none"
        variant="outlined"
        inputlabelprops={{
          shrink: true
        }}
        multiline={true}
        rows={6}
        rowsMax={10}
        error={touched && invalid}
        helpertext={touched && error}
        {...input}
        {...custom}
        value={comingValue}
      />
      {touched && ((error && <div style={{ color: "#FF1714", fontSize: "12px" }}>{error}</div>))}
    </div>
  );
}
