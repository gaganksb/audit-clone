import React from 'react';
import _ from 'lodash';

class FieldFileInput extends React.Component {
    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this)
        this.state = {
            text: null
        }
    }

    onChange(e) {
        const { input: { onChange } } = this.props
        this.setState({
            text: e.target.files[0].name
        })
        onChange(e.target.files[0])
    }

    componentDidMount(){
        const {report} = this.props;
        if (report && report.report_doc){
            this.setState({text:report.report_doc.filename})
        }
    }

    render() {

        const { input: { value }, classes, report, meta: {touched}, error } = this.props
        return (
            <div>
                <input type='text' className={classes.inputFile} value={this.state.text ? this.state.text : 'N/A'}  />
                <span className={classes.buttonWrapper}>
                    <button className={classes.btn}>{(!this.state.text)? 'Choose file' : 'Upload'}</button>
                    <input
                        type='file'
                        onChange={this.onChange}
                    />
                </span>
                {touched && (error && <div style={{color:"#FF1714", fontSize:"12px"}}>{error}</div>)}
            </div>
        )
    }
}

export default FieldFileInput;