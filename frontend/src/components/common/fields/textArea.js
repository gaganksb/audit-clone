import React, { useState } from 'react'
import { TextField } from "@material-ui/core"

function RenderTextArea({
    label,
    className,
    margin,
    input,
    meta: { touched, invalid, error },
    ...custom
}) {
    return (
        <TextField
            id="outlined-multiline-flexible"
            label={label}
            multiline
            rows={6}
            rowsMax={10}
            fullWidth
            margin="normal"
            variant="outlined"
            InputLabelProps={{
                shrink: true,
            }}
            error={touched && invalid && error}
            helperText={touched && error}
            {...input}
            {...custom}
        />
    )
}

export default RenderTextArea;