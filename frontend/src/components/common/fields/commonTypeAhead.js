import React, { useState, useEffect } from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { TextField, CircularProgress } from "@material-ui/core";

const AutoCompleteField = props => {
  const {
    input,
    options,
    meta: { touched, error },
    handleInputChange,
    getOptionLabel,
    defaultValue,
    multiple,
    label,
    className,
    disabled,
    key,
    disableClearable,
    ...custom
  } = props;

  const [open, setOpen] = useState(false)
  return (
    <Autocomplete
      key={key}
      id={input.name}
      name={input.name}
      onChange={(e, v) => handleInputChange(e, v, input.name)}
      getOptionLabel={getOptionLabel}
      options={options}
      className={className}
      disabled={disabled ? true : false}
      selectOnFocus={false}
      defaultValue={defaultValue}
      disableClearable={disableClearable}
      renderInput={params => (
        <TextField
          {...params}
          {...input}
          error={touched && error ? true : false}
          helperText={touched && error ? error : null}
          label={label}
        />
      )}
      multiple={multiple}
    />
  );
};

export default AutoCompleteField;