import React from 'react';
import { Autocomplete } from '@material-ui/lab'
import {TextField} from "@material-ui/core"

function RenderAutoCompleteField({
    label,
    className,
    margin,
    handleChange,
    handleValueChange,
    items,
    input,
    comingValue,
    meta: {touched, invalid, error},
    multiple,
    ...custom,
}) {
    return(
        <Autocomplete
            options={items}
            getOptionLabel={option => (option.user) ? `${option.user} - ${option.email}`: (option.code) ? option.code : option.name}
            renderInput={params => (
                <TextField
                    {...params}
                    label={label}
                    className={className}
                    margin={margin}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    {...input}
                    {...custom}
                />
            )}
            
            defaultValue={(comingValue)? comingValue: []}
            multiple={multiple}
            onChange={(e, v) => handleValueChange(v)}
        />
    )
}

export default RenderAutoCompleteField