import React, { createRef, useEffect } from 'react';
import * as d3 from "d3";

// var data = [
//   {label: "Included PA", value: 140},
//   {label: "Excluded PA", value: 220}
//   ];
  
var text = "";

<style>

.pie {{
  margin: '0 auto'
}}

.pie text{ {
  fontFamily: "Verdana",
  fill: '#888'
}
}
.pie .name-text{{
  fontSize: '1em'
}}

.pie .value-text{{
  fontSize:' 3em'
}}
</style> 

export default function DonutChart(props) {
    var data = props.data
    var width = 310;
    var height = 310;
    var thickness = 40;
    var duration = 750;
    // const cScale = d3.scaleOrdinal(d3.schemeCategory20b);
    var nodeWidth = (d) => d.getBBox().width;
    // const data= props.data;
    
    var radius = Math.min(width, height) / 2;
    var color = d3.scaleOrdinal()
    .range(["#0070BF", "#344563"]);
    

 

    useEffect(()=>{  
        drawbars()
      },[])
      
    
      const drawbars = () => {
    
    var svg = d3.select("#chart")
      .append('svg')
      .attr('class', 'pie')
      .attr('width', width)
      .attr('height', height + 40)
      .append("g");
      
      var g = svg.append('g')
      .attr('transform', 'translate(' + (width/2) + ',' + (height/2) + ')');
      
      var arc = d3.arc()
      .innerRadius(radius - thickness)
      .outerRadius(radius);
      
      var pie = d3.pie()
      .value(function(d) { return d.value; })
      .sort(null);
      
      var path = g.selectAll('path')
      .data(pie(data))
      .enter()
      .append("g")
      .on("mouseover", function(d) {
            let g = d3.select(this)
              .style("cursor", "pointer")
              .style("fill", "black")
              .append("g")
              .attr("class", "text-group");
       
            g.append("text")
              .attr("class", "name-text")
              .text(`${d.data.label}`)
              .attr('text-anchor', 'middle')
              .attr('dy', '-1.2em');
        
            g.append("text")
              .attr("class", "value-text")
              .text(`${d.data.value}`)
              .attr('text-anchor', 'middle')
              .attr('dy', '.6em');
          })
        .on("mouseout", function(d) {
            d3.select(this)
              .style("cursor", "none")  
              .style("fill", color(this._current))
              .select(".text-group").remove();
          })
        .append('path')
        .attr('d', arc)
        .attr('fill', (d,i) => color(i))
        .on("mouseover", function(d) {
            d3.select(this)     
              .style("cursor", "pointer")
              .style("fill", "black");
          })
        .on("mouseout", function(d) {
            d3.select(this)
              .style("cursor", "none")  
              .style("fill", color(this._current));
          })
        .each(function(d, i) { this._current = i; });
      
      
      g.append('text')
        .attr('text-anchor', 'middle')
        .attr('dy', '.35em')
        .text(text);

        const legend = svg.append('g')
          .attr('class', 'legend')
          .attr('transform', 'translate(0,0)');

        const lg = legend.selectAll('g')
          .data(data)
          .enter()
        .append('g')
          .attr('transform', (d,i) => `translate(${i * 100},${height + 15})`);

        lg.append('rect')
        .style('fill', function (d, i) { return color(i); })
          .attr('x', 0)
          .attr('y', 0)
          .attr('width', 30)
          .attr('height', 30);

        lg.append('text')
          .style('font-family', 'Georgia')
          .style('font-size', '13px')
          .attr('x', 38.5)
          .attr('y', 20)
          .text(d => d.label);

        let offset = 0;
        lg.attr('transform', function(d, i) {
            let x = offset;
            offset += nodeWidth(this) + 10;
            return `translate(${x},${height + 10})`;
        });

        legend.attr('transform', function() {
          return `translate(${(width - nodeWidth(this)) / 2},${0})`
        });
    }

    return (

        // <span>

            <div id="chart" style={{margin: 'auto', width: 360, marginTop: 30}}>

            </div>

        // </span> 

    );


}