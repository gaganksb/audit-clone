import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';


function TabContainer(props) {
  return (
    <Typography component="div" style={{ height: '100vh' }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  indicator: {
    backgroundColor: '#0072bc',
  },
  tabContainer: {
    '& button': {
      color: '#0072bc !important',
    },
    '& > div > div > span': {
      backgroundColor: '#0072bc !important'
    },
    '& .Mui-selected': {
      backgroundColor: '#0072bc !important',
      color: '#FFFFFF !important'
    }
  }
});

class HeaderTabs extends React.Component {

  render() {
    const { classes, tabs, children, value, goTo, handleChange } = this.props;

    return (
      // <div className={classes.root} >
        <Paper className={classes.root}>
          <Tabs value={value} 
            onChange={handleChange} 
            textColor="#0072BC" 
            classes={{
              indicator: classes.indicator
            }}
            className={classes.tabContainer} >
            { tabs.items.map((item, key) => 
                <Tab key={key} label={item.label} onClick={()=>goTo(item.path)}/>
              ) }
          </Tabs>
        </Paper>
      // </div>
    );
  }
}

HeaderTabs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HeaderTabs);
