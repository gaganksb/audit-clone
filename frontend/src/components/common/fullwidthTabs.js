import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles ({
  root: {
    flexGrow: 1,
  },
  tabContainer: {
    '& button': {
      color: '#0072bc !important',
    },
    '& > div > div > span': {
      backgroundColor: '#0072bc !important'
    },
    '& .Mui-selected': {
      backgroundColor: '#0072bc !important',
      color: '#FFFFFF !important'
    }
  }
});

export default function CenteredTabs(props) {
  const classes = useStyles();
  const { tabs, children, value, goTo, handleChange } = props;
  const width = 100/(tabs.items.length);
  return (
    <Paper className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        className={classes.tabContainer}
      >
        {tabs.items.map((item, key) => 
         <Tab style={{ width: `${width}%`, maxWidth: '700px'}} key={key} label={item.label} />)
        }
      </Tabs>
    </Paper>
  );
}