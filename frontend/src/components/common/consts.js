export const PROJECT_SENSE_CHECK = {
    "1": "Projects above Risk Threshold",
    "2": "Remaining Projects having Adverse or Disclaimer of Opinion in last 4 years",
    "3": "Remaining Projects implemented by New Partner to UNHCR with budget >= USD 50k",
    "4": "Remaining Partners not Audited Before for their PAs created in 2013 and 2014 and with budget >=50k",
    "5": "Remaining Partners not Audited Before for their PAs created in 2015 and 2016 and with budget >=50k",
    "6": "Remaining Partners not Audited for their PAs created in 2017 with budget >=50,000 and Risk rating >2.7",
    "7": "Remaining Partners with qualified opinions in 2017 audit cycle and budget >=50k",
    "8": "Remaining Projects with Budget >=5M",
    "9": "Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) - 10 Highest Budget",
    "10": "Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) -  out of 10 Highest Budget but with recent ICQ <0.70",
    "11": "From/Included in the Interim list as field/HQ requests or category changed",
    "12": "PA Manually Excluded",
    "13": "PA Manually Included"
}

export const BACKGROUND_TASKS = {
    // 'risk_rating_update': "Risk Rating",
    'field_assessment_hq': "Field Assessment",
    'assign_agreement_member_bgtask': 'Agreement Member list',
    'update_risk_rating_bg': "update_risk_rating_bg"
}