import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import CustomizedButton from './customizedButton';
import {buttonType} from '../../helpers/constants';
import R from 'ramda';
import CustomAlert from './customAlert';
import { editCycle } from '../../reducers/projects/projectsReducer/editCycle';
import { loadCycle } from '../../reducers/projects/projectsReducer/getCycle';
import { loadCycleFO } from '../../reducers/projects/projectsReducer/getCycleFO';
import { initializeInterim, initializeFinal, initializeFinalFO } from '../../reducers/projects/projectsReducer/initializeInterimList';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { loadUser } from '../../reducers/users/userData';

const messages = {
  title: 'Step forward to the next Stage',
  error: 'Error',
}

const StepForward = (props) => {
  const { 
    cycle, 
    cycleFO,
    editCycle, 
    loadCycle, 
    loadCycleFO,
    membership, 
    postError, 
    initializeInterim, 
    initializeFinal, 
    initializeFinalFO, 
    loadUser,
    actionTitle,
   } = props

  function message() {
    if (cycle && cycle.status == 1) {
      return (<div>Confirm that you want to send the <b>Preliminary List</b> to <b>review</b> by clicking on YES </div>)
    }
    if (cycle && cycle.status == 2) {
      return (<div>Confirm that you want to send the <b>Preliminary List</b> to <b>approval</b> by clicking on YES </div>)
    }
    if (cycle && cycle.status == 3) {
      return (<div>Confirm that you want to approve the Preliminary List by clicking on Yes. This will finalise the Preliminary List and initialise the related Interim List.</div>)
    }
    if (cycle && cycle.status == 4) {
      return (<div>Confirm that you want to send the <b>Interim List</b> to <b>approval</b> by clicking on YES </div>)
    }
    if (cycle && cycle.status == 5) {
      return (<div>Confirm that you want to approve the Interim List by clicking on Yes. This will finalise the Interim List and initialise the related Final List.</div>)
    }
    if (cycle && cycle.status == 6) {
      return (<div>Confirm that you want to start the Auditor selection process by clicking on YES </div>)
    }
    if (cycle && cycle.status == 7) {
      return (<div>Confirm that you want to finalize the Project Selection by clicking on YES </div>)
    }
    
  }
  function label() {
    if (cycle && cycle.status == 1) {
      return 'Send for Review';
    }
    if (cycle && cycle.status == 2) {
      return 'Send for Approval';
    }
    if (cycle && cycle.status == 3) {
      return 'Finalize';
    }
    if (cycleFO && cycleFO.cycle.status == 4) {
      return 'Send for Approval';
    }
    if (cycleFO && cycleFO.cycle.status == 5) {
      return 'Finalize';
    }
    if (cycle && cycle.status == 6) {
      return 'Start Auditor Selection';
    }
    if (cycle && cycle.status == 7) {
      return 'Finalize';
    }
  }

  const initialAlert = {
    open: false,
    title: messages.title,
    message: message(),
    handleClose: handleClose,
    handleYes: handleStepForward
  }
  const [alert, setAlert] = useState(initialAlert);
  const [disabled, setDisabled] = useState(false);
  // const [actionTitle, setActionTitle] = useState('');

  useEffect(() => {
    loadUser()
  }, []);

  // useEffect(() => {
  //   setActionTitle(label());
  // }, [cycle]);

  useEffect(() => {
    if (membership) {
      if (Object.keys(membership).includes('field_office')) {
        loadCycleFO(cycle.year, membership.field_office.id)
      }
    }
  }, [membership]);

  function handleClose() {
    setAlert(R.merge(initialAlert, { open: false }))
  }

  function handleAlert() {
    setDisabled(false);
    setAlert(R.merge(initialAlert, {open: true}));
  }

  function handleCycleChange(body) {
    const newStatus = body.status
    if ([2,3].includes(newStatus)) {
      editCycle(cycle.year, R.merge(body, {list_type: 'preliminarylist'}))
        .then(loadCycle(cycle.year))
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } else if (newStatus == 4) {
      initializeInterim(cycle.year)
        .then(() => { 
          editCycle(cycle.year, R.merge(body, {list_type: 'interimlist'}))
            .then(loadCycle(cycle.year))
        })
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } else if (newStatus == 5) {
      editCycle(cycle.year, R.merge(body, {list_type: 'interimlist'}))
        .then(() => {
          if (membership) {
            if (Object.keys(membership).includes('field_office')) {
              loadCycleFO(cycle.year, membership.field_office.id)
            }
          }
        })
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } else if (newStatus == 6) {
      if (membership) {
        if (Object.keys(membership).includes('field_office')) {
          const foId = membership.field_office.id
          initializeFinalFO(cycle.year, foId)
            .then(() => {
              if (membership) {
                if (Object.keys(membership).includes('field_office')) {
                  loadCycleFO(cycle.year, membership.field_office.id)
                }
              }
            })
            .catch((error) => { postError(error, messages.error); })
            .finally(() => { handleClose(); });
        }
      }
    } else if (newStatus == 7) {
      editCycle(cycle.year, R.merge(body, {list_type: 'finallist'}))
        .then(loadCycle(cycle.year))
        .catch((error) => { postError(error, messages.error); })
        .finally(() => { handleClose(); });
    } 
  }

  function handleStepForward() {
    let body = null
    if ([4,5].includes(cycle.status)) {
      if (membership) {
        if (Object.keys(membership).includes('field_office')) {
          body = {status: cycleFO.cycle.status + 1}
        }
      }
    } else {
      body = {status: cycle.status + 1}
    }
    setDisabled(true);
    handleCycleChange(body);
  }

  return (      
    <span>
      {(cycle) ? 
      <CustomizedButton buttonType={buttonType.submit} 
      buttonText={actionTitle} 
      clicked={handleAlert} />
       : null }
      <CustomAlert 
        {...alert}
        disabled={disabled}
      />
    </span>
  )
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  cycleFO: state.cycleFO.details,
  membership: state.userData.data.membership,
});

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  loadCycleFO: (year, foId) => dispatch(loadCycleFO(year, foId)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editCycle', message)),
  initializeInterim: (year) => dispatch(initializeInterim(year)),
  initializeFinal: (year) => dispatch(initializeFinal(year)),
  initializeFinalFO: (year, foId) => dispatch(initializeFinalFO(year, foId)),
  loadUser: () => dispatch(loadUser()),
});

const connectedStepForward = connect(mapStateToProps, mapDispatch)(StepForward);
export default withRouter(connectedStepForward);




