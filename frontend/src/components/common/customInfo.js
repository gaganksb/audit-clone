import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import InfoIcon from '@material-ui/icons/Info';

const useStyles = makeStyles(theme => ({
  root: {
  },
  paper: {
    padding: theme.spacing(2),
  },
}));

export default function CenteredGrid(props) {
  const classes = useStyles();
  const {info} = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <InfoIcon style={{ display: "inline-block", marginBottom:"-5px" }} /> <b>{info}</b>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}