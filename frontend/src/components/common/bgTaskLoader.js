import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux'
import { loadBgJob } from '../../reducers/bgTaskReducer';
import { BACKGROUND_TASKS } from './consts';
import Backdrop from '@material-ui/core/Backdrop';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
        display: 'flex',
        justifyContent: 'center'

    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
    inputLabel: {
        display: 'flex',
        justifyContent: 'center',
        fontSize: 20,
        color: '#344563',
        marginTop: 20,
        paddingLeft: 14
    },
}));

const BGTaskLoader = props => {

    const classes = useStyles();
    const [hasPendingTask, setHasPendingTask] = useState(false)
    const [taskId, setTaskID] = useState(null)
    const [taskHash, setTaskHash] = useState(null)
    const { pendingTaskName, loadBgJob, bgJob, shouldStartMonitoring } = props;

    const [open, setOpen] = React.useState(false);

    useEffect(() => {
        const params = {
            "pending_task": pendingTaskName
        }
        loadBgJob(params)
    }, [])

    useEffect(() => {
        if (bgJob && !bgJob.pending_tasks && bgJob.completed.status === true) {
            setHasPendingTask(false)
            shouldStartMonitoring(false)
            setTaskID(null)
            setTaskHash(null)
            setOpen(false);
            window.location.reload();
        }

        if (bgJob && bgJob.pending_tasks && bgJob.pending_tasks.task_name.includes(pendingTaskName)) {
            setHasPendingTask(true)
            shouldStartMonitoring(true)
            setTaskID(bgJob.pending_tasks.verbose_name)
            setTaskHash(bgJob.pending_tasks.task_hash)
            setOpen(true);
        }

    }, [bgJob])

    useEffect(() => {
        if (taskId && taskHash) {
            const interval = setInterval(() => {
                const params = {
                    "task_hash": taskHash,
                    "task_id": taskId
                }
                loadBgJob(params)
            }, 5000);
            return () => clearInterval(interval);
        }
    }, [hasPendingTask])

    return (
        <React.Fragment>
            {hasPendingTask && pendingTaskName == 'update_risk_rating_bg' &&
                <div style={{ color: '#f27e27', fontWeight: 'bold' }}>Computing</div>}
            {hasPendingTask && pendingTaskName != 'update_risk_rating_bg' && <div className={classes.root}>
                <Backdrop className={classes.backdrop} open={open}>
                    <CircularProgress />
                    <div className={classes.inputLabel}>{`A background task is running please wait while ${BACKGROUND_TASKS[pendingTaskName]} is updating`}</div>
                </Backdrop>
            </div>}
        </React.Fragment>
    )
}

const mapState = (state) => ({
    bgJob: state.bgJobStatus.bgJob
})

const mapDispatch = dispatch => ({
    loadBgJob: (params) => dispatch(loadBgJob(params))
})

export default connect(mapState, mapDispatch)(BGTaskLoader);