import React, { Component } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import { SENSE_CHECKS } from '../../helpers/constants';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import _ from 'lodash'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  paper: {
    color: theme.palette.text.secondary,
    width: '66%',
    float: 'left',
    boxShadow: '2px 3px 6px 2px #0072BD1A',
  },
  content: {
    font: 'Regular 14px/19px Roboto',
    color: '#344563'
  },
  reason: {
    marginTop: theme.spacing(3),
    height: 24,
  },
  card: {
    backgroundColor: '#F9FAFC',
    marginTop: 32,
    marginLeft: theme.spacing(4),
    padding: theme.spacing(3),
    width: '28%',
    height: 104,
    display: 'inline-flex',
    boxShadow: '0px 3px 6px #0072BC27',
    marginBottom: theme.spacing(4)
  },
  value: {
    font: 'bold 20px/19px Roboto',
    color: '#344563'
  },
  pre: {
    backgroundColor: '#e8e9ed',
    marginTop: 8
  },
  preIcon: {
    color: '#040b2e'
  },
  int: {
    backgroundColor: '#DFECF5',
    marginTop: 8
  },
  intIcon: {
    color: '#0072BC'
  },
  fin: {
    backgroundColor: '#E9F3DF',
    marginTop: 8
  },
  finIcon: {
    color: '#72BC00'
  },
  formControl: {
    float: 'right',
    height: 32,
    width: 192,
    marginTop: -10,
    marginRight: theme.spacing(4),
    border: '1px solid #E5E5E5'
  },
  select: {
    height: 32,
    font: '14px/19px Roboto',
    color: '#344563',
  },
  title: {
    color: '#344563',
    marginTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    width: '100%',
    font: 'bold 16px/21px Roboto',
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
  },
}));


const data = [
  {
    year: '2017', preliminary: 100, interim: 200, final: 300,
  },
  {
    year: '2018', preliminary: 90, interim: 138, final: 210,
  },
  {
    year: '2019', preliminary: 110, interim: 110, final: 280,
  },
  {
    year: '2020', preliminary: 400, interim: 350, final: 300,
  },
];



export default function LineCharts(props) {

  const { messages, year, dashboardDetails, handleSearch } = props;
  const classes = useStyles();
  const currYear = new Date().getFullYear()
  const years = Array.from(new Array(15), (val, index) => currYear - index);
  const handleChange = (event) => {
    handleSearch(event)
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid item sm={12}>
          <div className={classes.title} >{messages}
            <FormControl variant="outlined" className={classes.formControl}>
              <Select
                value={year}
                onChange={handleChange}
                className={classes.select}
              >
                {
                  years.map((year, index) => {
                    return <MenuItem value={year}>{year}</MenuItem>
                  })
                }
              </Select>
            </FormControl>
          </div>
          {/* <div className={classes.reason}>
        <FormControl variant="outlined" className={classes.formControl} style={{width: 392}}>
        <Select
          value={reason}
          onChange={handleChangeReason}
          className={classes.select}
        >
             {optionsList.map(option => (
                <MenuItem key={option.id} value={option.code} style={{ fontSize: '14px' }} className={classes.menu}>
                  {option.description}
                </MenuItem>
              ))}
        </Select>
      </FormControl>
      </div> */}
          <div>
            <Card className={classes.card}>
              <Grid item sm={10}>
                <Typography className={classes.content}>
                  Preliminary list
                  <p className={classes.value}>{_.get(dashboardDetails, ['small_stats', 'pre'], 0)}
                  </p>
                </Typography>
              </Grid>
              <Grid item sm={2}>
                <Fab size='small' className={classes.pre} align='right'>
                  <TrendingUpIcon className={classes.preIcon} />
                </Fab>
              </Grid>
            </Card>
            <Card className={classes.card}>
              <Grid item sm={10}>
                <Typography className={classes.content}>
                  Interim list
                  <p className={classes.value}>{_.get(dashboardDetails, ['small_stats', 'int'], 0)}</p>
                </Typography>
              </Grid>
              <Grid item sm={2}>
                <Fab size='small' className={classes.int} align='right'>
                  <TrendingUpIcon className={classes.intIcon} />
                </Fab>
              </Grid>
            </Card>
            <Card className={classes.card}>
              <Grid item sm={10}>
                <Typography className={classes.content}>
                  Final list
                  <p className={classes.value}>{_.get(dashboardDetails, ['small_stats', 'fin'], 0)}</p>
                </Typography>
              </Grid>
              <Grid item sm={2}>
                <Fab size='small' className={classes.fin} align='right'>
                  <TrendingUpIcon className={classes.finIcon} />
                </Fab>
              </Grid>
            </Card>
          </div>
          {/* <ResponsiveContainer 
        width='95%'
        height={350}>
      <LineChart
        data={data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="year" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="preliminary" stroke="#556CD6"  />
        <Line type="monotone" dataKey="interim" stroke="#0072BC" />
        <Line type="monotone" dataKey="final" stroke="#72BC00" />
      </LineChart>
      </ResponsiveContainer> */}
          <div style={{ height: '190px' }}>
            <Typography style={{ textAlign: 'center' }}>No Data currently Available</Typography>
          </div>
        </Grid>
      </Paper>
    </div>
  );
}
