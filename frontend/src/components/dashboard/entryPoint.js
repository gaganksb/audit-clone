import React from 'react';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import { OTHER } from '../../helpers/constants';
import Dashboard from './dashboard';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Title from '../common/customTitle';


const messages = {
  title: 'Dashboard',
  pendingAccount: 'Pending Account',
  stepperTitle: 'Project Selection Progress',
  chartTitle: 'Projects Statistics'
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  title: {
    paddingTop: 24,
    marginLeft: theme.spacing(3),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  card: {
    maxWidth: 600,
  },
  media: {
    height: 140,
  },
}));


const EntryPoint = props => {
  const { userType } = props;
  const classes = useStyles();

  return (
    <div>

      <Title show={false} title={(userType == OTHER) ? messages.pendingAccount : messages.title} />
      {(userType == OTHER) ?
        <div className={classes.root}>
          <Grid
            container
            spacing={0}
            justify="center">
            <Card className={classes.card}>
              <CardActionArea>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Warning
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Please be informed that your profile is not associated to any user account yet. Please contact UNHCR Global Service Desk or reach out to IMAS at <a href={`mailto:epartner@unhcr.org`}>epartner@unhcr.org</a>; with the description of the issue and for asking assistance with creation of a user account.
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </div>
        :
        <Dashboard />
      }
      <div style={{ height: 30 }}></div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  userType: state.session.user_type,
  membership: state.userData.data.membership,
});

const connectedEntryPoint = connect(mapStateToProps, null)(EntryPoint);
export default withRouter(connectedEntryPoint);