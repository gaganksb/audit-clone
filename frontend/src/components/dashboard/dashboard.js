import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import ProgressStepper from './progressStepper';
import { connect } from 'react-redux';
import { loadDashboard } from '../../reducers/projects/dashboardReducer';
import DashboardStats from './dashboardLineChart'
import { browserHistory as history } from 'react-router';
import { AUDIT, PARTNER } from '../../helpers/constants';
import R from 'ramda';
import { loadCycle } from '../../reducers/projects/projectsReducer/getCycle';


const messages = {
  title: 'Dashboard',
  stepperTitle: 'Project Selection Progress',
  chartTitle: 'Over all projects status'
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    overflow: 'hidden'
  },
  title: {
    paddingTop: 24,
    marginLeft: theme.spacing(3),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));



const currentYear = new Date().getFullYear()

const Dashboard = props => {
  const { loadDashboard, query, dashboardDetails, membership, userType, loadCycle, cycle } = props;
  const [year, setYear] = useState(currentYear);
  const classes = useStyles();

  useEffect(() => {
    // loadDashboard(query)
    if (userType === AUDIT || userType === PARTNER) {
      history.push('/audit/auditManagement')
    }
  }, []);

  useEffect(() => {
    const { pathName } = props;
    if (query.year) {
      setYear(query.year)
      loadDashboard(query.year)
    }
    if ("year" in query === false) {
      setYear(currentYear)
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          year: currentYear
        })
      })
    }

  }, [query]);

  const handleSearch = (event) => {
    const { pathName } = props;
    history.push({
      pathname: pathName,
      query: R.merge(query,
        { year: event.target.value }
      )
    })
  }

  return (
    <div className={classes.root}>
      <DashboardStats messages={messages.chartTitle} year={year} setYear={setYear} handleSearch={handleSearch} dashboardDetails={dashboardDetails} />
      <ProgressStepper messages={messages.stepperTitle} cycle={cycle} dashboardDetails={dashboardDetails} />
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  dashboardDetails: state.dashboardDetails.dashboard,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  userType: state.session.user_type,
  membership: state.userData.data.membership,
  cycle: state.cycle.details,
});

const mapDispatch = dispatch => ({
  loadDashboard: (year, params) => dispatch(loadDashboard(year, params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
});

const connectedDashboard = connect(mapStateToProps, mapDispatch)(Dashboard);
export default withRouter(connectedDashboard);