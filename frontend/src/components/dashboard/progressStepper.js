import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Paper from '@material-ui/core/Paper';
import _ from 'lodash'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  paper: {
    color: theme.palette.text.secondary,
    width: '30%',
    float: 'right',
    boxShadow: '2px 3px 6px 2px #0072BD1A',
  },
  title: {
    color: '#344563',
    marginTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    paddingLeft: theme.spacing(3),
    width: '100%',
    font: 'bold 16px/21px Roboto',
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
  },
  assessmentContainer: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    backgroundColor: '#F9E8EB',
    paddingTop: 8,
    paddingLeft: 8
  },
  preContainer: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    backgroundColor: '#e8e9ed',
    paddingTop: 8,
    paddingLeft: 8
  },
  intContainer: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    backgroundColor: '#DFECF5',
    paddingTop: 8,
    paddingLeft: 8
  },
  finContainer: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    backgroundColor: '#E9F3DF',
    paddingTop: 8,
    paddingLeft: 8
  },
  assessmentLabelContainer: {
    backgroundColor: '#F9E8EB',
    width: 153,
    marginLeft: 20,
    padding: 8,
  },
  preLabelContainer: {
    backgroundColor: '#e8e9ed',
    width: 153,
    marginLeft: 20,
    padding: 8,
  },
  intLabelContainer: {
    backgroundColor: '#DFECF5',
    width: 153,
    marginLeft: 20,
    padding: 8
  },
  finLabelContainer: {
    backgroundColor: '#E9F3DF',
    width: 153,
    marginLeft: 20,
    padding: 8
  },
  assessmentLabel: {
    font: '14px/19px Roboto',
    color: '#EA5D76 !important',
    paddingLeft: 8
  },
  preLabel: {
    font: '14px/19px Roboto',
    color: '#040b2e !important',
    paddingLeft: 8
  },
  intLabel: {
    font: '14px/19px Roboto',
    color: '#0072BC !important',
    paddingLeft: 8
  },
  finLabel: {
    font: '14px/19px Roboto',
    color: '#72BC00 !important',
    paddingLeft: 8
  },
  assessmentPointer: {
    position: 'absolute',
    color: '#EA5D76 !important',
    enableBackground: 'new 0 0 330.002 330.002',
    position: 'absolute',
    marginTop: 9,
    marginLeft: 48
  },
  inactivePointer: {
    // position: 'absolute',
    // color: '#EA5D76 !important',
    // enableBackground:'new 0 0 330.002 330.002', 
    // position: 'absolute',
    // marginTop: 9, 
    // marginLeft: 48,
    display: 'none'
  },
  prePointer: {
    position: 'absolute',
    color: '#040b2e !important',
    enableBackground: 'new 0 0 330.002 330.002',
    position: 'absolute',
    marginTop: 9,
    marginLeft: 48
  },
  intPointer: {
    position: 'absolute',
    color: '#0072BC !important',
    enableBackground: 'new 0 0 330.002 330.002',
    position: 'absolute',
    marginTop: 9,
    marginLeft: 48
  },
  finPointer: {
    position: 'absolute',
    color: '#72BC00 !important',
    enableBackground: 'new 0 0 330.002 330.002',
    position: 'absolute',
    marginTop: 9,
    marginLeft: 48
  },
  active: {
    marginLeft: 6,
    padding: 0,
  },
  completed: {
    marginLeft: 6,
    padding: 0,
  },
  connectorLine: {
    height: 40,
    border: 0,
    backgroundColor: activeStep => (activeStep <= 3) ? '#e8e9ed' : (activeStep > 3 && activeStep <= 5) ? '#DFECF5' : '#E9F3DF',
    borderRadius: 1,
    width: 8,
    marginLeft: 0,
    padding: 0
  },
  lineVertical: {
    marginLeft: 9,
    padding: 0,
    height: 40,
  },
  disabled: {
    marginLeft: 6,
    padding: 0,
  },
  assessment: {
    color: '#EA5D76 !important'
  },
  pre: {
    color: '#040b2e !important'
  },
  int: {
    color: '#0072BC !important'
  },
  fin: {
    color: '#72BC00 !important'
  },
  inactive: {
    color: '#D5D5D5 !important'
  }
}));

let activeStep = undefined;

function getSteps(statusCode) {
  switch (statusCode) {
    case null: activeStep = 0;
      break;
    case 'FIELD_ASSESSMENT': activeStep = 1;
      break;
    case 'PRE01': activeStep = 2;
      break;
    case 'PRE02': activeStep = 3;
      break;
    case 'PRE03': activeStep = 4;
      break;
    case 'INT01': activeStep = 5;
      break;
    case 'INT02': activeStep = 5;
      break;
    case 'FIN01': activeStep = 6;
      break;
    case 'FIN02': activeStep = 7;
      break;
    default: activeStep = 0;
      break;
  }

  return ['Field Assessment', 'Preliminary list - 01', 'Preliminary list - 02', 'Preliminary list - 03',
    'Interim list ', 'Final List - 01', 'Final List - 02'];
}

function progressStepper(props) {
  const { messages, dashboardDetails } = props;
  const classes = useStyles();

  let statusCode = _.get(dashboardDetails, ['cycle', 'status__code'], 0)
  const steps = getSteps(statusCode);

  function handleClass(index, activeClass) {

    if (index > activeClass - 1) {
      return classes.inactive
    }
    if (index < 1) {
      return classes.assessment
    }
    if (index >= 1 && index <= 3) {
      return classes.pre
    }
    if (index > 3 && index < 5) {
      return classes.int
    }
    else {
      return classes.fin
    }
  }

  function handleContainerClass(index) {
    if (index < 1) {
      return classes.assessmentContainer
    }
    if (index >= 1 && index <= 3) {
      return classes.preContainer
    }
    if (index > 3 && index < 5) {
      return classes.intContainer
    }
    else {
      return classes.finContainer
    }
  }

  function handleLabelContainer(index) {
    if (index < 1) {
      return classes.assessmentLabelContainer
    }
    if (index >= 1 && index <= 3) {
      return classes.preLabelContainer
    }
    if (index > 3 && index < 5) {
      return classes.intLabelContainer
    }
    else {
      return classes.finLabelContainer
    }
  }

  function handleLabel(index) {
    if (index < 1) {
      return classes.assessmentLabel
    }
    if (index >= 1 && index <= 3) {
      return classes.preLabel
    }
    if (index > 3 && index < 5) {
      return classes.intLabel
    }
    else {
      return classes.finLabel
    }
  }

  let color = 'white'
  function handlePointerClass(index, activeClass) {
    if (index != activeClass - 1) {
      return classes.inactivePointer
    }
    if (index < 1) {
      color = '#EA5D76'
      return classes.assessmentPointer
    }
    if (index >= 1 && index <= 3) {
      color = '#040b2e'
      return classes.prePointer
    }
    if (index > 3 && index < 5) {
      color = '#0072BC'
      return classes.intPointer
    }
    else {
      color = '#72BC00'
      return classes.finPointer
    }
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid item sm={12}>
          <div className={classes.title} >{messages}</div>
          <Stepper activeStep={activeStep} orientation="vertical"
            connector={<StepConnector
              activeStep={activeStep}
              classes={{
                line: classes.connectorLine,
                active: classes.active,
                completed: classes.completed,
                lineVertical: classes.lineVertical,
                disabled: classes.disabled
              }} />}>

            {steps.map((label, index) => {
              const stepProps = {};
              const labelProps = {};
              return (
                <Step key={index} {...stepProps}>
                  <svg width="64px" height="64px" viewBox="0 0 980.002 980.002" className={handlePointerClass(index, activeStep)} fill={color} >
                    <path id="XMLID_19_" d="M229.966,0.847c-6.011-2.109-12.698-0.19-16.678,4.784L93.288,155.635  c-4.382,5.478-4.382,13.263,0.001,18.741l120,149.996c2.902,3.628,7.245,5.63,11.716,5.63c1.658,0,3.336-0.276,4.962-0.847  c6.012-2.108,10.035-7.784,10.035-14.154v-300C240.001,8.63,235.978,2.955,229.966,0.847z" />
                  </svg>
                  <StepLabel {...labelProps} classes={{
                    iconContainer: handleContainerClass(index),
                    labelContainer: handleLabelContainer(index),
                    label: handleLabel(index)
                  }}
                    StepIconProps={{
                      classes: { root: handleClass(index, activeStep) }
                    }}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
        </Grid>
      </Paper>
    </div>
  );
}

export default progressStepper;