import React, { PureComponent } from 'react';
import {
  ResponsiveContainer, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import {graphData} from '../../helpers/constants'
import Grid from '@material-ui/core/Grid';
// const {ResponsiveContainer, BarChart, Bar, XAxis, YAxis, Cell, Tooltip, Legend} = Recharts;


let optionsList = [
  'Total Projects',
  'Projects above Risk Threshold',
 'Remaining Projects having Adverse or Disclaimer of Opinion in last 4 years',
 'Remaining Projects implemented by New Partner to UNHCR with budget >= USD 50k',
  'Remaining Partners not Audited Before for their PAs created in 2013 and 2014 and with budget >=50k',
 'Remaining Partners not Audited Before for their PAs created in 2015 and 2016 and with budget >=50k',
  'Remaining Partners not Audited for their PAs created in 2017 with budget >=50,000 and Risk rating >2.7',
 'Remaining Partners with qualified opinions in 2017 audit cycle and budget >=50k',
   'Remaining Projects with Budget >=5M',
   'Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) - 10 Highest Budget',
'Remaining Projects having Budget >=656K (In OIOS Top 20 Countries) -  out of 10 Highest Budget but with recent ICQ <0.70',
'From/Included in the Interim list as field/HQ requests or category changed',
// 'PA Manually Excluded',
'PA Manually Included'
];

const CustomTooltip = ({ active, payload, label }) => {
  if (active) {
    return (
      <div className="custom-tooltip">
        <p className="label">{`${payload[0].name}`}</p>
        <p className="label">{`${payload[0].value}`}</p>
        {/* <p className="intro">{getIntroOfPage(label)}</p> */}
      </div>
    );
  }

  return null;
};

export default class Example extends PureComponent {

  render() { 
    return (
      <div style = {{margin: 30, paddingLeft: 24}}>
        <h4>{this.props.title}</h4>
        <Grid>
       
      <Grid item sm={6} style={{float: 'right', fontColor: '#C3C6D1', fontSize: 13, paddingTop: 6, lineHeight: '20px'}}>
        {optionsList.map(reason => {
          return  (<div>
            {reason}
          </div>)
        })}
        </Grid>
        <Grid item sm={6}>
      <BarChart
        width={600}
        height={300}
        data={graphData}
        barCategoryGap={5}
        layout="vertical"
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}
      >
        <XAxis  type="number" />
        <YAxis type="category" width={10} dataKey="name"  tick={false}/>
        <Tooltip />
        <Bar dataKey="projects" fill="#344563" radius={[0, 10, 10, 0]} barSize={10}/>
      </BarChart>
      </Grid>
      </Grid>
      </div>
    );
  }
}
