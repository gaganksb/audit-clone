import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import PauseIcon from '@material-ui/icons/Pause';
import PlayIcon from '@material-ui/icons/PlayArrow';
import EditIcon from '@material-ui/icons/Edit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { OTHER, ACCOUNT_TYPES, USERS } from '../../helpers/constants';


const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  icon: {
    '&:hover': {
      cursor: 'pointer'
    }
  }
}));

const messages = {
  detailHeaders: [
    'USER TYPE',
    'USER ROLE',
    'OFFICE/PARTNER/AGENCY'
  ]
}

function UserDetails(props) {
  const {
    expanded,
    handleExpand,
    handleEdit,
    handleDelete,
    user,
    userId
  } = props;
  
  const classes = useStyles();

  const parseTypeRole = (userType, userRole) => {
    let parseType = OTHER
    let parseRole = '-'
    for (let i = 0; i < USERS.length; i++) {
      if (USERS[i].ENUM == userType) {
        parseType = USERS[i].NAME
        for (let j = 0; j < USERS[i].ROLES.length; j++) {
          if (USERS[i].ROLES[j].ENUM == userRole) {
            parseRole = USERS[i].ROLES[j].NAME
            break
          }
        }
        break
      }
    }
    return {
      type: parseType,
      role: parseRole
    }
  }

  const setUserTypeRole = (input) => {
    let accounts = []
    for (const [key, value] of Object.entries(input.accounts)) {
      for (let i = 0; i < value.length; i++) {
        const {type, role } = parseTypeRole(ACCOUNT_TYPES[key], value[i].role)
        let account = {
          type,
          role,
          office: (value[i].office) ? value[i].office : '-'
        }
        accounts.push(account)
      }
    }
    if (accounts == []) {
      accounts = {
        type: OTHER,
        role: '-',
        office: '-'
      }
    }
    return accounts
  }
  const [accounts, setAccounts] = useState(setUserTypeRole(user))

  useEffect(() => {
    setAccounts(setUserTypeRole(user))
  }, [user])

  return (
    <React.Fragment>
      <TableRow key={user.email}>
        <TableCell className={classes.tableCell} component="th" scope="row">
          {(expanded) ? 
            <ExpandLess className={classes.icon} onClick={handleExpand()} /> : 
            <ExpandMore className={classes.icon} onClick={handleExpand()} />
          }
        </TableCell>
        <TableCell className={classes.tableCell} component="th" scope="row">
          {user.name}
        </TableCell>
        <TableCell className={classes.tableCell}>{user.email}</TableCell>
        <TableCell align='right'>
          { userId === user.id || 
          <React.Fragment>
            {
              user.is_active ? 
              <Tooltip title={"Deactivate User"}>
                <Fab size='small' className={classes.fab} onClick={handleDelete()} >
                  <PauseIcon />
                </Fab>
              </Tooltip> 
              : 
              <Tooltip title={"Activate User"}>
                <Fab color="primary" size='small' className={classes.fab} onClick={handleDelete()}  style={{backgroundColor: '#0072BC'}}>
                  <PlayIcon />
                </Fab>
              </Tooltip> 
            }
          </React.Fragment>
          }
          <Tooltip title={"Edit User"}>
            <Fab color="primary" size='small' className={classes.fab} style={{backgroundColor: '#0072BC'}} onClick={handleEdit()} >
              <EditIcon />
            </Fab>
          </Tooltip>
        </TableCell>
      </TableRow>
      {(expanded) ?
        <TableRow>
          <TableCell colSpan={4} className={classes.tableCell}>
            <div style={{marginTop: 10}}>
              <table cellspacing="0" cellpadding="0" style={{width: '100%', borderCollapse: 'collapse'}}>
                <tr>
                  {messages.detailHeaders.map((item, key) => (
                    <th style={{paddingBottom: 10}}>{item}</th>
                  ))}
                </tr>
                {accounts.map((item, key) => (
                  <tr key={key}>
                    <td style={{paddingBottom: 5}} key={key + '-type'}>{item.type}</td>
                    <td style={{paddingBottom: 5}} key={key + '-role'}>{item.role}</td>
                    <td style={{paddingBottom: 5}} key={key + '-office'}>{item.office}</td>
                  </tr>
                ))}
              </table>
            </div>
          </TableCell>
        </TableRow> : null}
    </React.Fragment>
  );
}

const mapStateToProps = (state, ownProps) => ({
  userId: state.session.userId,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserDetails);

export default withRouter(connected);





