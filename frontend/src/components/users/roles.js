import React, { useState, useEffect } from 'react';
import Title from '../common/customTitle';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import RolesFilter from './rolesFilter';
import ListLoader from '../common/listLoader';
import { browserHistory as history, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { loadPermissionList } from '../../reducers/users/permissionList';

const styles = theme => ({
    root: {
      flexGrow: 1,
      textAlign: 'center',
    },
    paper: {
      padding: theme.spacing(2),
      marginTop: '20px',
      width: '80%',
      display: 'inline-block'
    },
  });
  
function Roles(props) {
  
  const { classes, loading, loadPermissions, permissions } = props;

  useEffect(() => {
    loadPermissions();
  }, []);

  return (
      <div className={classes.root}>
        <ListLoader loading={loading} >
          <Paper className={classes.paper}>
              <Grid container spacing={24}>
                  <Grid item xs={12} sm container>
                      <Grid item xs container direction="column" spacing={16}>
                        <RolesFilter permissions={ permissions } />
                      </Grid>
                  </Grid>
              </Grid>
          </Paper>
        </ListLoader>
      </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  loading: state.permissionList.loading,
  permissions: state.permissionList.list,
});

const mapDispatch = dispatch => ({
  loadPermissions: () => dispatch(loadPermissionList()),
});

const connectedRoles = connect(mapStateToProps, mapDispatch)(Roles);
export default withRouter(withStyles(styles)(connectedRoles));