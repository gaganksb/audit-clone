import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: 16,
    paddingLeft: 16,
    paddingBottom: 16,
    paddingTop: 16
  },
  title: {
    fontWeight: 'bold',
    marginBottom: theme.spacing(0.5),
    marginTop: theme.spacing(0.5),
  },
  subtitle: {
    marginBottom: theme.spacing(0.5),
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  default: {
    backgroundColor: '#c7edd1',
    marginTop: 16,
    paddingLeft: 16,
    paddingBottom: 16,
    paddingTop: 16
  },
  button: {
    marginTop: theme.spacing(1),
    backgroundColor: '#0072BC',
    minWidth: 94,
    borderRadius: 5,
    height: 30,
    marginLeft: 16,
    fontFamily: 'Open Sans',
    fontSize: 14,
    lineHeight: '12px',
    color: 'white',
    textTransform: 'capitalize',
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  },
}));

const messages = {
  type: 'Type',
  role: 'Role',
  office: 'Office/Partner/Agency',
  remove: 'Remove',
  default: 'Set Default'
}

function DeleteUserAccount(props) {
  const { account, handleDeleteAccount, setDefault } = props;
  const classes = useStyles();

  return (
    <Paper className={account.is_default ? classes.default : classes.paper}>
      {(account.is_default) ? <Chip
        label="Default"
        color="primary"
        style={{ float: 'right', marginRight: 16 }}
      /> : null}
      <Typography className={classes.title}>
        {messages.type}
      </Typography>
      <Typography className={classes.subtitle}>
        {account.type}
      </Typography>
      <Typography className={classes.title}>
        {messages.role}
      </Typography>
      <Typography className={classes.subtitle}>
        {account.role}
      </Typography>
      <Typography className={classes.title}>
        {messages.office}
      </Typography>
      <Typography className={classes.subtitle}>
        {account.office}
      </Typography>

      {(!account.is_default) ? <div className={classes.buttonGrid}>
        <Grid sm={6} style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: 16 }}>

          <CustomizedButton
            buttonText={messages.default}
            clicked={setDefault}
            buttonType={buttonType.submit} />
          <CustomizedButton
            clicked={handleDeleteAccount}
            buttonText={messages.remove}
            buttonType={buttonType.cancel} />
        </Grid>
      </div> : null}
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => {
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeleteUserAccount);

export default withRouter(connected);