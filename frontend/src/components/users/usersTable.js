import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../common/listLoader';
import TablePaginationActions from '../common/tableActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { loadUsersList } from '../../reducers/users/usersList';
import DeactivateDialog from './deactivateUserDialog';
import EditDialog from './editUserDialog';
import { editUser } from '../../reducers/users/editUser';
import R from 'ramda';
import { errorToBeAdded } from '../../reducers/errorReducer';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import UserDetails from './userDetails';
import { repeatRows } from '../../helpers/layout';
import { showSuccessSnackbar } from '../../reducers/successReducer';


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  noDataRow: {
    margin: 16,
    width: '1500%',
    textAlign: 'center'
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
}))

const DELETE = 'DELETE'
const EDIT = 'EDIT'
const success = {
  activate: 'User Activated !',
  deactivate: 'User Deactivated !'
}

const rowsPerPage = 10

function UsersTable(props) {
  const {
    data, 
    totalItems, 
    loading, 
    page, 
    handleChangePage, 
    handleSort, 
    loadUsers, 
    editUser, 
    postError, 
    messages,
    membrship,
    sort,
    successReducer,
    query } = props;
  
  const classes = useStyles();

  function handleDeactivation(value) {
    const {id, is_active} = value
    const body = {is_active: !is_active};
    const msg = (is_active) ? success.deactivate : success.activate

    return editUser(id, body, 'deactivate').then(() => {
      handleClose();
      if (R.isEmpty(query)) {
        (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ? loadUsers(R.merge(query, {
          field: messages.tableHeader[0].field, 
          order: 'asc',
          my_own: true
        })): loadUsers(R.merge(query, {
          field: messages.tableHeader[0].field, 
          order: 'asc'
        }));
      } else {
        (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ?loadUsers(query, {
          my_own: true
        }): loadUsers(query);
      }
      successReducer(msg)
    }).catch((error) => {
      postError(error, messages.error);
    });
  }

  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }
  
  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }

  const [open, setOpen] = React.useState({status: false, type: null})
  const [user, setUser] = React.useState({});  
  const [expanded, setExpanded] = React.useState(initialExpanded())

  useEffect(() => {
    setExpanded(initialExpanded())
  }, [data]);

  const handleClickOpen = (item, action) => () => {
    setUser(item);    
    setOpen({status: true, type: action})
  }

  function handleClose() {
    setOpen({status: false, type: null});
  }

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }


  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        <div className={classes.tableWrapper}>
          <div className={classes.heading}>
            {messages.tableTitle}
          </div>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                { messages.tableHeader.map((item, key) => 
                  <TableCell style={{width: item.width}} align={item.align} key={key} className={classes.tableHeader}>
                    {(item.field) ?
                    <TableSortLabel
                      active={item.field === sort.field}
                      direction={sort.order}
                      onClick={() => handleSort(item.field, sort.order)}
                    > 
                      {item.title}
                    </TableSortLabel> : 
                    <TableSortLabel active={false}> {item.title} </TableSortLabel>
                    }
                  </TableCell>
                  ) }
              </TableRow>
            </TableHead>
            {(data.length == 0 && !loading) ? 
            <div className= {classes.noDataRow}>No data for the selected criteria</div> :
              <TableBody>
                { data.map((item, key) => (
                  <UserDetails 
                    key={key}
                    user={item} 
                    expanded={expanded[key]} 
                    handleExpand={() => handleExpand(key)}
                    handleEdit={() => handleClickOpen(item, EDIT)}
                    handleDelete={() => handleClickOpen(item, DELETE)} />
                )) }  
              </TableBody> }
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={5}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                  style={{width:'10px'}}
                />
              </TableRow>
            </TableFooter>
          </Table>
          <DeactivateDialog handleClose={handleClose} open={open.status && open.type===DELETE} handleDeactivation={handleDeactivation} user={user} />
          <EditDialog handleClose={handleClose} open={open.status && open.type===EDIT} user={user} />
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  membrship: state.userData.data.membership
});

const mapDispatchToProps = (dispatch) => ({
  loadUsers: params => dispatch(loadUsersList(params)),
  editUser: (id, body, userType) => dispatch(editUser(id, body, userType)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editUser', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UsersTable);

export default withRouter(connected);





