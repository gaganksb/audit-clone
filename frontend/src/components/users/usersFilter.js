import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import CustomizedButton from '../common/customizedButton';
import { buttonType, filterButtons } from '../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import renderTypeAhead from '../common/fields/commonTypeAhead';
import { connect } from 'react-redux';
import { renderTextField, renderSelectField } from '../common/renderFields';

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 8
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '92%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  formControl: {
    marginTop: 1,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '92%',
  },
  fab: {
    margin: theme.spacing(1),
  },
  autoCompleteField: {
    marginTop: 2,
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '69%',
    minWidth: "10px",
    display: "inline-block"
  },
}));

const messages = {
  roles: {
    Auditor: ['REVIEWER', 'ADMIN'],
    Field: ['REVIEWER', 'APPROVER'],
    Partner: ['REVIEWER', 'ADMIN'],
    HQ: ['PREPARER', 'REVIEWER', 'APPROVER']
  }
}


function UsersFilter(props) {
  const classes = useStyles();
  const {
    handleSubmit, reset, resetFilter, handleChange, handleInputChange,
    auditList, partnerList, fieldList
  } = props;
  const [userType, setUserType] = useState(null)
  const [userRole, setUserRole] = useState(null)
  const [key, setKey] = useState('default')
  var date = new Date()
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={2} >
            <InputLabel className={classes.inputLabel}>Fullname</InputLabel>
            <Field
              name='fullname'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
              fullWidth={false}
            />
          </Grid>
          <Grid item sm={2} >
            <InputLabel className={classes.inputLabel}>Email</InputLabel>
            <Field
              name='email'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
              fullWidth={false}
            />
          </Grid>
          <Grid item sm={2} >
            <InputLabel className={classes.inputLabel}>User Type</InputLabel>
            <Field
              name="user_type"
              className={classes.formControl}
              component={renderSelectField}
              onChange={(e) => { setUserType(e.target.value); setKey(date.getTime()); setUserRole('') }}
              selectedItem={userType}
            >
              {
                ['Auditor', 'Partner', 'Field', 'HQ'].map((item, index) => {
                  return (<option className={classes.menu} key={item} value={item}>{item}</option>)
                })
              }
            </Field>
          </Grid>
          <Grid item sm={2} >
            <InputLabel className={classes.inputLabel}>User Role</InputLabel>
            <Field
              name="user_role"
              className={classes.formControl}
              component={renderSelectField}
              onChange={(e) => { setUserRole(e.target.value) }}
              selectedItem={userRole}
              disabled={userType ? false : true}
            >
              {
                messages.roles[userType] && messages.roles[userType].map((item, index) => {
                  return (<option className={classes.menu} value={item}>{item}</option>)
                })
              }
            </Field>
          </Grid>
          <Grid item sm={2} >
            <InputLabel className={classes.inputLabel}>Office</InputLabel>
            <Field
              name="office"
              margin="normal"
              key={key}
              component={renderTypeAhead}
              onChange={(e) => handleChange(e.target.value, userType)}
              handleInputChange={handleInputChange}
              options={userType === "Auditor" ? auditList : userType === "Partner" ? partnerList : userType === "Field" ? fieldList : []}
              getOptionLabel={(option => option.legal_name ? option.legal_name : option.name)}
              fullWidth={false}
              disabled={userType && userType !== "HQ" ? false : true}
              style={{ background: userType ? "white" : "#e6e6e6" }}
              className={classes.autoCompleteField}
            />
          </Grid>
          <Grid item sm={2}>
            <div item className={classes.buttonGrid}>
              <CustomizedButton buttonType={buttonType.submit}
                buttonText={filterButtons.search}
                type="submit" />
              <CustomizedButton buttonType={buttonType.submit}
                reset={true}
                clicked={() => { reset(); resetFilter(); setKey(date.getTime()); setUserRole(''); setUserType(''); }}
                buttonText={filterButtons.reset}
              />
            </div>
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

const mapState = (state, ownProps) => {
  return {
    auditList: state.auditList.list,
    partnerList: state.partnerList.list,
    fieldList: state.fieldList.list,
  }
}

const mapDispatch = (dispatch) => ({})


const UsersFilterForm = reduxForm({
  form: "usersFilter",
  enableReinitialize: true
})(UsersFilter);


export default connect(mapState, mapDispatch)(UsersFilterForm);
