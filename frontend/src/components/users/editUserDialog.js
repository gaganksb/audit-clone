import React, { useEffect, useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import AddUserAccount from './addUserAccount';
import EditUserAccount from './editUserAccount';
import DeleteUserAccount from './deleteUserAccount';
import R from 'ramda';
import { USERS, OFFICE_LABELS, OTHER, ACCOUNT_TYPES } from '../../helpers/constants';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { loadFieldList } from '../../reducers/fields/fieldList';
import { loadAuditList } from '../../reducers/audits/auditList';
import { loadUsersList } from '../../reducers/users/usersList';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { addUserAccount, deleteUserAccount, editUserAccount } from '../../reducers/users/account';
import CustomDialogTitle from '../common/customDialogTitle';
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { SubmissionError } from 'redux-form';
import { setDefaultAccounts } from '../../reducers/users/setDefaultAccount';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display: 'flex',
  },
  papers: {
    marginTop: 16,
    paddingLeft: 16,
    paddingBottom: 16,
    paddingTop: 16
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: theme.spacing(0.5),
    marginTop: theme.spacing(0.5),
  },
  icon: {
    '&:hover': {
      cursor: 'pointer'
    }
  },
}));

const messages = {
  title: 'Edit User',
  add: 'Add',
  edit: 'Edit',
  cancel: 'Cancel',
  remove: 'Remove',
  type: 'Type',
  role: 'Role',
  office: 'Office/Partner/Agency',
  errors: {
    edit: 'Unable to Edit User',
  },
  success: {
    edit: 'User details edited successfully !',
    delete: 'User deleted successfully !',
    update: 'User default Profile Updated !',
    add: 'User profile added successfully!'
  },

}

function EditDialog(props) {
  const {
    auditList,
    fieldList,
    partnerList,
    loadAuditList,
    loadFieldList,
    loadPartnerList,
    loadUsers,
    addUserAccount,
    deleteUserAccount,
    setDefaultAccounts,
    editUserAccount,
    open,
    handleClose,
    membrship,
    query,
    successReducer,
    user } = props;
  const classes = useStyles();

  const parseTypeRole = (userType, userRole) => {
    let type = OTHER
    let role = '-'
    let model = '-'
    for (let i = 0; i < USERS.length; i++) {
      if (USERS[i].ENUM == userType) {
        type = USERS[i].NAME
        model = USERS[i].MODEL
        for (let j = 0; j < USERS[i].ROLES.length; j++) {
          if (USERS[i].ROLES[j].ENUM == userRole) {
            role = USERS[i].ROLES[j].NAME
            break
          }
        }
        break
      }
    }
    return {
      type,
      role,
      model
    }
  }

  const setUserTypeRole = (input) => {
    let res = []
    if (input.accounts) {
      for (const [key, value] of Object.entries(input.accounts)) {
        for (let i = 0; i < value.length; i++) {
          const { type, role, model } = parseTypeRole(ACCOUNT_TYPES[key], value[i].role)
          let account = {
            id: value[i].id,
            type,
            role,
            model,
            office: (value[i].office) ? value[i].office : '-',
            is_default: value[i].is_default,
            user_type_role_id: value[i].user_type_role_id,
            user_id: input.id
          }
          res.push(account)
        }
      }
    }
    return res
  }
  const [accounts, setAccounts] = useState(null)
  const [items, setItems] = useState([]);
  const [roles, setRoles] = useState([]);
  const [office, setOffice] = useState(null);
  const [title, setTitle] = useState('Office');
  const [fo, isFO] = useState(false);

  useEffect(() => {
    if (office !== null) {
      if (office.label === OFFICE_LABELS.audit) {
        setItems(auditList)
      } else if (office.label === OFFICE_LABELS.field) {
        setItems(fieldList)
      } else if (office.label === OFFICE_LABELS.partner) {
        setItems(partnerList)
      }
    }
  }, [auditList, fieldList, partnerList]);

  useEffect(() => {
    if (user) {
      setAccounts(setUserTypeRole(user))
    }
  }, [user])

  const handleTypeChange = (event) => {
    const isKEY = (key) => (item) => item.KEY === key;
    const _ = R.filter(isKEY(event.target.value), USERS);
    if (membrship.type === 'FO' && _[0].ENUM === 'FO') {
      isFO(true)
    }
    else {
      isFO(false)
    }
    if (_.length === 0) {
      setRoles([]);
      setOffice(null);
    } else {
      setRoles(_[0].ROLES);
      setOffice(R.filter(isKEY(event.target.value), USERS)[0].OFFICE);
      if (_[0].KEY === USERS[0].KEY) {
        setTitle('Audit Firm')
      }
      else if (_[0].KEY === USERS[2].KEY) {
        setTitle('Partner')
      }
      else {
        setTitle('Office')
      }
    }
    setItems([]);
  }

  const handleChange = (event) => {
    if (event) {
      let params = {
        legal_name: event,
      }
      if (office !== null) {
        if (office.label === OFFICE_LABELS.audit) {
          loadAuditList(params)
        } else if (office.label === OFFICE_LABELS.field) {
          loadFieldList(params)
        } else if (office.label === OFFICE_LABELS.partner) {
          loadPartnerList(params)
        }
      }
    }
  }

  const handleEditAccount = (values) => {
    const { id, name } = values
    const body = {
      name
    }

    handleClose()
    return editUserAccount(id, body).then(() => {
      (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ?
        loadUsers({
          ...query,
          my_own: true
        }) : loadUsers(query);
      successReducer(messages.success.edit)
    })
  }

  const addAccountBody = (values) => {
    let { user_type, user_role, office } = values

    if (office && office.indexOf("-") > -1) {
      office = office.substring(0, office.lastIndexOf("-"));
    }
    if (membrship.type === 'AUDITOR' || membrship.type === 'PARTNER') {
      user_type = membrship.type
    }

    const type = R.find(R.propEq('KEY', user_type), USERS) || R.find(R.propEq('ENUM', user_type), USERS)
    const role = (type.ROLES == null) ? null : R.find(R.propEq('ID', parseInt(user_role)), type.ROLES)

    let membership = null
    let office_id = null
    if (user_type == 'unhcr') {
      membership = {
        type: type.ENUM,
        role: role.ENUM
      }
    } else if (user_type == 'field' && membrship.type === 'HQ') {
      office_id = R.find(R.propEq('name', office), items).id;
      membership = {
        type: type.ENUM,
        role: role.ENUM,
        office: office_id
      }
    } else if (user_type === 'field' && membrship.type === 'FO') {
      membership = {
        type: type.ENUM,
        role: role.ENUM,
        office: membrship.field_office ? membrship.field_office.id : null
      }
    }
    else if (user_type == 'audit') {
      office_id = R.find(R.propEq('legal_name', office), items).id;
      membership = {
        type: type.ENUM,
        role: role.ENUM,
        agency: office_id
      }
    } else if (user_type == 'partners') {
      office_id = R.find(R.propEq('legal_name', office), items).id;
      membership = {
        type: type.ENUM,
        role: role.ENUM,
        partner: office_id,
      }
    } else if (user_type == 'other') {
      membership = {
        type: type.ENUM,
      }
    }
    if (membrship.type == 'PARTNER') {
      membership = {
        type: membrship.type,
        role: role.ENUM,
        partner: membrship.partner.id,
      }
    }
    if (membrship.type == 'AUDITOR') {
      membership = {
        type: membrship.type,
        role: role.ENUM,
        agency: membrship.audit_agency.id,
      }
    }
    const body = {
      membership
    }
    return body
  }

  const handleAddAccount = (values) => {
    const { id } = values
    const body = addAccountBody(values)

    handleClose()
    return addUserAccount(id, body).then(() => {
      (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ? loadUsers({
        ...query,
        my_own: true
      }) : loadUsers(query);
      successReducer(messages.success.add)
    }).catch((error) => {
      postError(error, error.response && error.response.data || messages.error);
      throw new SubmissionError({
        _error: messages.error,
      });
    });
  }

  const handleDeleteAccount = (account) => {
    const { id, model } = account
    handleClose()
    deleteUserAccount(id, model).then(() => {
      (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ? loadUsers({
        ...query,
        my_own: true
      }) : loadUsers(query);
      successReducer(messages.success.delete)
    })
  }

  const setDefault = (account) => {
    const { user_type_role_id, user_id } = account
    handleClose()
    setDefaultAccounts(user_type_role_id, user_id).then(() => {

      (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ? loadUsers({
        ...query,
        my_own: true
      }) : loadUsers(query);
      successReducer(messages.success.update)
    })
  }

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <CustomDialogTitle handleClose={handleClose} title={messages.title} />
        <Divider />
        <DialogContent>
          <DialogContentText >
            <EditUserAccount
              onSubmit={handleEditAccount}
              membership={membrship}
              user={user} />
            <AddUserAccount
              user={user}
              title={title}
              onSubmit={handleAddAccount}
              handleChange={handleChange}
              handleTypeChange={handleTypeChange}
              items={items}
              office={office}
              membership={membrship}
              fo={fo}
              roles={roles} />
            {(accounts) ? accounts.map((item, key) => (
              <DeleteUserAccount account={item} handleDeleteAccount={() => handleDeleteAccount(item)} setDefault={() => setDefault(item)} key={key} membership={membrship} />
            )) : null}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  partnerList: state.partnerList.list,
  fieldList: state.fieldList.list,
  auditList: state.auditList.list,
  query: ownProps.location.query,
  membrship: state.userData.data.membership
});

const mapDispatchToProps = dispatch => ({
  addUserAccount: (id, body) => dispatch(addUserAccount(id, body)),
  deleteUserAccount: (id, type) => dispatch(deleteUserAccount(id, type)),
  editUserAccount: (id, body) => dispatch(editUserAccount(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editUser', message)),
  loadUsers: params => dispatch(loadUsersList(params)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadFieldList: (params) => dispatch(loadFieldList(params)),
  loadAuditList: (params) => dispatch(loadAuditList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  setDefaultAccounts: (userRole, userId) => dispatch(setDefaultAccounts(userRole, userId))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditDialog);

export default withRouter(connected);