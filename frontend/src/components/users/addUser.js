import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import AddUserForm from './addUserForm';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import Divider from '@material-ui/core/Divider';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { addNewUser } from '../../reducers/users/addUser';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import { SubmissionError } from 'redux-form';
import { loadUsersList } from '../../reducers/users/usersList';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { loadFieldList } from '../../reducers/fields/fieldList';
import { loadAuditList } from '../../reducers/audits/auditList';
import R from 'ramda';
import { USERS, OFFICE_LABELS } from '../../helpers/constants';
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { reset } from 'redux-form';

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  }
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose} size='small' >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const stylesAddUserDialog = (theme) => ({
  root: {
    marginTop: theme.spacing(1),
    // marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginLeft: theme.spacing(1),
    display: 'flex',
    justifyContent: 'flex-start',
    minWidth: 100
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
});

const messages = {
  error: 'Unable to create new user',
  add: 'Add User Profile',
  title: 'Add User',
  success: 'User created Successfully !',
  ERROR: 'Please select valid option from dropdown'
};

function AddUserDialog(props) {
  const {
    classes,
    auditList,
    fieldList,
    partnerList,
    loadAuditList,
    loadFieldList,
    loadPartnerList,
    loadUsers,
    membrship,
    postUser,
    successReducer,
    postError,
    reset
  } = props;

  const [open, setOpen] = useState(false);
  const [items, setItems] = useState([]);
  const [roles, setRoles] = useState([]);
  const [office, setOffice] = useState(null);
  const [title, setTitle] = useState('Office');
  const [fo, isFO] = useState(false);

  useEffect(() => {
    if (office !== null) {
      if (office.label === OFFICE_LABELS.audit) {
        setItems(auditList)
      } else if (office.label === OFFICE_LABELS.field) {
        setItems(fieldList)
      } else if (office.label === OFFICE_LABELS.partner) {
        setItems(partnerList)
      }
    }
  }, [auditList, fieldList, partnerList]);

  const handleTypeChange = (event) => {
    const isKEY = (key) => (item) => item.KEY === key;
    const _ = R.filter(isKEY(event.target.value), USERS);
    if (membrship.type === 'FO' && _[0].ENUM === 'FO') {
      isFO(true)
    }
    else {
      isFO(false)
    }
    if (_.length === 0) {
      setRoles([]);
      setOffice(null);
    } else {
      setRoles(_[0].ROLES);
      setOffice(R.filter(isKEY(event.target.value), USERS)[0].OFFICE);
      if (_[0].KEY === USERS[0].KEY) {
        setTitle('Audit Firm')
      }
      else if (_[0].KEY === USERS[2].KEY) {
        setTitle('Partner')
      }
      else {
        setTitle('Office')
      }
    }
    setItems([]);
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    reset()
    setOpen(false);
  };

  const handleChange = (event) => {
    if (event) {
      let params = {
        legal_name: event,
      }
      if (office !== null) {
        if (office.label === OFFICE_LABELS.audit) {
          loadAuditList(params)
        } else if (office.label === OFFICE_LABELS.field) {
          loadFieldList(params)
        } else if (office.label === OFFICE_LABELS.partner) {
          loadPartnerList(params)
        }
      }
    }
  }


  const handleSubmit = (values) => {
    let { name, email, user_type, user_role, office } = values

    if (office && office.indexOf("-") > -1) {
      office = office.substring(0, office.lastIndexOf("-"));
    }

    let body = {}
    if (membrship.type === 'AUDITOR' || membrship.type === 'PARTNER') {
      user_type = membrship.type
    }
    if (membrship.type == 'PARTNER') {
      const type = R.find(R.propEq('ENUM', user_type), USERS)
      const role = (type.ROLES == null) ? null : R.find(R.propEq('ID', parseInt(user_role)), type.ROLES)
      body = {
        email,
        name,
        membership: {
          type: membrship.type,
          role: role.ENUM,
          partner: membrship.partner.id,
        }
      }
    }

    else if (membrship.type == 'AUDITOR') {
      const type = R.find(R.propEq('ENUM', user_type), USERS)
      const role = (type.ROLES == null) ? null : R.find(R.propEq('ID', parseInt(user_role)), type.ROLES)
      body = {
        email,
        name,
        membership: {
          type: membrship.type,
          role: role.ENUM,
          agency: membrship.audit_agency.id,
        }
      }
    }

    else if (membrship.type != 'AUDITOR' && membrship.type != 'PARTNER') {
      const type = R.find(R.propEq('KEY', user_type), USERS)
      const role = R.find(R.propEq('ID', parseInt(user_role)), type.ROLES)
      let office_id = null
      body = {
        email,
        name,
        membership: {
          type: type.ENUM,
          role: role.ENUM,
        }
      }
      if (user_type === 'field' && membrship.type === 'FO') {
        office = membrship.field_office
      }
      if (user_type !== 'unhcr' && !office) {
        const error = new SubmissionError({ _error: messages.ERROR });
        postError(error, messages.ERROR);
        throw error;
      }
      else {
        if (user_type === 'field' && membrship.type === 'HQ') {
          office_id = R.find(R.propEq('name', office), items).id;
          body.membership.office = office_id;
        } else if (user_type === 'field' && membrship.type === 'FO') {
          body.membership.office = office.id
        }
        else if (['audit'].includes(user_type)) {
          office_id = R.find(R.propEq('legal_name', office), items).id;
          body.membership.agency = office_id;
        } else if (['partners'].includes(user_type)) {
          office_id = R.find(R.propEq('legal_name', office), items).id;
          body.membership.partner = office_id;
        }
      }
    }
    return postUser(body).then((success) => {
      handleClose();
      (membrship.type == 'AUDITOR' || membrship.type == 'PARTNER' || membrship.type == 'FO') ?
        loadUsers({
          my_own: true
        }) : loadUsers();
      successReducer(messages.success)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        _error: messages.error,
      });
    });
  }

  return (
    <div>

      <div className={classes.root}>
        <Grid className={classes.buttonGrid} >
          <CustomizedButton clicked={() => handleClickOpen()} buttonText={messages.add} buttonType={buttonType.submit} />
        </Grid>
      </div>
      <Dialog
        onClose={handleClose}
        open={open}
        fullWidth
      >
        <DialogTitle onClose={handleClose}>
          {messages.title}
        </DialogTitle>
        <Divider />
        <MuiDialogContent>
          <AddUserForm
            handleClose={handleClose}
            onSubmit={handleSubmit}
            handleChange={handleChange}
            items={items}
            handleTypeChange={handleTypeChange}
            office={office}
            membership={membrship}
            title={title}
            fo={fo}
            roles={roles} />
        </MuiDialogContent>
      </Dialog>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  partnerList: state.partnerList.list,
  fieldList: state.fieldList.list,
  auditList: state.auditList.list,
  membrship: state.userData.data.membership
});

const mapDispatchToProps = dispatch => ({
  postUser: body => dispatch(addNewUser(body)),
  loadUsers: params => dispatch(loadUsersList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'newUser', message)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadFieldList: (params) => dispatch(loadFieldList(params)),
  loadAuditList: (params) => dispatch(loadAuditList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  reset: () => dispatch(reset('addUserAccount'))
});

const connected = connect(mapStateToProps, mapDispatchToProps)(AddUserDialog);
export default withRouter(withStyles(stylesAddUserDialog)(connected));
