import React, { Component } from 'react';
import { loadUsersList, exportUsers, uploadUsers } from '../../reducers/users/usersList';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isQueryChanged } from '../../helpers/apiHelper';
import { browserHistory as history, withRouter } from 'react-router';
import R from 'ramda';
import { loadUser } from '../../reducers/users/userData';
import AddUser from './addUser';
import UsersFilter from './usersFilter';
import UsersTable from './usersTable';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { loadFieldList } from '../../reducers/fields/fieldList'
import { loadAuditList } from '../../reducers/audits/auditList';
import CustomizedButton from "../common/customizedButton";
import { buttonType } from "../../helpers/constants";
import ImportUsersDialog from './ImportUsersDialog';
import download from 'downloadjs';
import { showSuccessSnackbar } from "../../reducers/successReducer";
import { errorToBeAdded } from "../../reducers/errorReducer";
import { downloadURI } from '../../helpers/others';


const exportImportMessages = {
  export: {
    error: "Error in export occurred !"
  },

  import: {
    error: "Error in import occurred !",
    uploadSucces: "Uploaded successfully !"
  },

  sampleTemplate: {
    error: "Error in template download !",
  },

}

const messages = {
  tableHeader: [
    { title: '', align: 'left', width: '5%' },
    { title: 'Fullname', align: 'left', width: '40%', field: 'fullname' },
    { title: 'Email', align: 'left', width: '40%', field: 'email' },
    { title: '', align: 'left', width: '15%' },
  ],
  tableTitle: 'User Management',
}

const filters = {
  roles: {
    Auditor: ['REVIEWER', 'ADMIN'],
    Field: ['REVIEWER', 'APPROVER'],
    Partner: ['REVIEWER', 'ADMIN'],
    HQ: ['PREPARER', 'REVIEWER', 'APPROVER']
  }
}

class UsersContainer extends Component {
  constructor(props) {
    super(props);
    this.resetFilter = this.resetFilter.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      page: 0,
      sort: {
        field: messages.tableHeader[0].field,
        order: 'asc'
      },
      openImportDialog: false,
      selectedFile: null,
    }
  }

  setFileOnSelect = (value) => {
    this.setState({ selectedFile: value });
  }

  componentWillMount() {
    const { query } = this.props;
    this.props.loadUser().then((result) => {
      let initialQuery = (result.membership.type == "AUDITOR" || result.membership.type == "PARTNER" || result.membership.type == 'FO') ? R.merge(query, {
        field: messages.tableHeader[0].field,
        order: 'asc',
        my_own: true
      }) : R.merge(query, {
        field: messages.tableHeader[0].field,
        order: 'asc'
      })
      this.props.loadUsers(initialQuery);
    })

  }

  shouldComponentUpdate(nextProps) {
    const { query, membrship } = this.props;

    if (isQueryChanged(nextProps, query)) {
      if (membrship && (membrship.type == "AUDITOR" || membrship.type == 'PARTNER' || membrship.type == 'FO')) {
        this.props.loadUsers(nextProps.location.query, { my_own: true });
      }
      else {
        this.props.loadUsers(nextProps.location.query);
      }
      return false;
    }

    return true;
  }

  handleSearch(values) {
    const { pathName, query, membrship, auditList, partnerList, fieldList } = this.props;
    const { fullname, email, user_type } = values;
    let { user_role, office } = values
    if (user_role && user_type) {
      if (filters.roles[user_type] && filters.roles[user_type].indexOf(user_role) !== -1) {
        user_role = `${user_type}__${user_role}`;
      } else {
        user_role = undefined;
      }
    } else {
      user_role = undefined;
    }

    if (office && user_type) {
      if (
        (user_type === 'Auditor' && auditList.findIndex(item => item.legal_name === office) !== -1)
        || (user_type === 'Partner' && partnerList.findIndex(item => item.legal_name === office) !== -1)
        || (user_type === 'Field' && fieldList.findIndex(item => item.name === office) !== -1)
      ) {
        office = `${user_type}__${office}`;
      } else {
        office = undefined;
      }
    } else {
      office = undefined;
    }

    this.setState({ page: 0 });
    (membrship.type == 'PARTNER' || membrship.type == 'AUDITOR' || membrship.type == 'FO') ?
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          fullname,
          email,
          user_type,
          user_role,
          office,
          my_own: true
        }),
      }) :
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          fullname,
          email,
          user_type,
          user_role,
          office,
        }),
      })
  }

  handleChange(inputValue, userType) {
    const { loadAuditList, loadPartnerList, loadFieldList } = this.props;
    const params = { legal_name: inputValue }
    if (!userType) return
    if (userType === "Auditor") {
      loadAuditList(params)
    } else if (userType === "Field") {
      loadFieldList(params)
    } else if (userType === "Partner") {
      loadPartnerList(params)
    }
  }

  handleInputChange(e) {

  }


  handleSort(field, order) {
    const { pathName, query, membrship } = this.props;

    this.setState({
      sort: {
        field: field,
        order: R.reject(R.equals(order), ['asc', 'desc'])[0]
      }
    });

    (membrship.type == 'PARTNER' || membrship.type == 'AUDITOR' || membrship.type == 'FO') ?
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          field,
          order,
          my_own: true
        }),
      })
      : history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          field,
          order,
        }),
      })
  }

  resetFilter() {
    const { pathName, membrship } = this.props;

    this.setState({ page: 0 });
    (membrship.type == 'PARTNER' || membrship.type == 'AUDITOR' || membrship.type == 'FO') ? history.push({
      pathname: pathName,
      query: {
        page: 1,
        my_own: true
      },
    }) :
      history.push({
        pathname: pathName,
        query: {
          page: 1,
        },
      })
  }

  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }

  exportHandler = () => {
    this.props.exportUsers().then(response => {
      download(response, "exported-users.csv", "text/csv");
    }).catch(error => {
      props.postError(error, exportImportMessages.export.error);
    })
  }

  onCancelImport = () => {
    this.setState({ openImportDialog: false, selectedFile: null });
  }

  onImportClick = () => {
    this.setState({ openImportDialog: true });
  }

  downloadSampleTemplate = () => {
    downloadURI('/api/common/data-import-template/', 'data-import-template.xlsx');
  }

  onUpload = () => {
    this.setState({ openImportDialog: false });
    let body = new FormData();
    body.append("import_file", this.state.selectedFile);
    body.append("year", new Date().getFullYear());
    body.append("data_types[0]", "users");

    this.props.uploadUsers(body).then(response => {
      this.setState({ selectedFile: null });
      this.props.successReducer("Uploaded successfully !");
    }).catch(error => {
      this.setState({ selectedFile: null });
      this.props.postError(error, "Error in upload !");
    })
  }

  render() {
    const { users, totalCount, loading } = this.props;

    return (
      <div>
        <UsersFilter
          onSubmit={this.handleSearch}
          resetFilter={this.resetFilter}
          handleChange={this.handleChange}
          handleInputChange={this.handleInputChange}
        />
        {!loading && <AddUser />}
        <div style={{ marginLeft: 8 }}>
          <CustomizedButton
            buttonType={buttonType.submit}
            clicked={this.exportHandler}
            buttonText={"Export"}
          />
          <CustomizedButton
            buttonType={buttonType.submit}
            clicked={this.exportHandler}
            buttonText={"Import"}
            clicked={this.onImportClick}
          />
        </div>
        <ImportUsersDialog open={this.state.openImportDialog} onClose={this.onCancelImport}
          downloadSampleTemplate={this.downloadSampleTemplate}
          onUpload={this.onUpload}
          selectedFile={this.state.selectedFile}
          setFileOnSelect={this.setFileOnSelect} />
        <UsersTable
          loading={loading}
          data={users}
          totalItems={totalCount}
          page={this.state.page}
          handleChangePage={this.handleChangePage}
          messages={messages}
          sort={this.state.sort}
          handleSort={this.handleSort} />
      </div>
    );
  }
}

UsersContainer.propTypes = {
  users: PropTypes.array.isRequired,
  totalCount: PropTypes.number.isRequired,
  loadUsers: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  query: PropTypes.object,
};

const mapStateToProps = (state, ownProps) => ({
  users: state.usersList.users,
  totalCount: state.usersList.totalCount,
  loading: state.usersList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  membrship: state.userData.data.membership,
  auditList: state.auditList.list,
  partnerList: state.partnerList.list,
  fieldList: state.fieldList.list,
});

const mapDispatch = dispatch => ({
  loadUsers: params => dispatch(loadUsersList(params)),
  exportUsers: () => dispatch(exportUsers()),
  uploadUsers: (body) => dispatch(uploadUsers(body)),
  loadUser: () => dispatch(loadUser()),
  loadAuditList: params => dispatch(loadAuditList(params)),
  loadPartnerList: params => dispatch(loadPartnerList(params)),
  loadFieldList: params => dispatch(loadFieldList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'User Management', message)),
});

const connectedUsersContainer = connect(mapStateToProps, mapDispatch)(UsersContainer);
export default withRouter(connectedUsersContainer);

