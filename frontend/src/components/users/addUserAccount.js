import React, { useState } from 'react';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import { makeStyles } from '@material-ui/core/styles';
import { USERS, USERS_KEY, OFFICE_LABELS } from '../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderSelectField, renderTextField } from '../common/renderFields';
import CustomAutoCompleteField from '../forms/TypeAheadFields';


const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    width: '100%'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(2)
  },
  formControl: {
    minWidth: 160,
    marginRight: theme.spacing(1),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
  },
  autoCompleteField: {
    marginLeft: 2,
    marginRight: theme.spacing(1),
    width: 535,
    marginTop: 1
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
}));

const messages = {
  add: 'Add',
  cancel: 'Cancel',
  type: 'Type',
  role: 'Role',
  office: 'Office/Partner/Agency',
  // selectValid: 'Select valid option from dropdown',
  roleLabel: 'User Role',
}

const ReviewerUser = [
  {
    KEY: 'partners',
    NAME: 'Partner Member',
    ENUM: 'PARTNER',
    MODEL: 'partnermember',
    ROLES: [
      { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
      { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
    ],
    OFFICE: {
      label: OFFICE_LABELS.partner,
      type_to_search: 'partner__legal_name'
    }
  },
];

const ApproverUser = [{
  KEY: 'field',
  NAME: 'Field Office Member',
  ENUM: 'FO',
  MODEL: 'fieldmember',
  ROLES: [
    { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
    { NAME: 'Approver', ID: 2, ENUM: 'APPROVER' }
  ],
  OFFICE: {
    label: OFFICE_LABELS.field,
    type_to_search: 'field__name'
  }
},
{
  KEY: 'partners',
  NAME: 'Partner Member',
  ENUM: 'PARTNER',
  MODEL: 'partnermember',
  ROLES: [
    { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
    { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
  ],
  OFFICE: {
    label: OFFICE_LABELS.partner,
    type_to_search: 'partner__legal_name'
  }
},]

const required = (office) => {
  let requiredFields = [
    'user_type',
    'user_role'
  ]
  if (office) {
    if ([OFFICE_LABELS.audit, OFFICE_LABELS.field, OFFICE_LABELS.partner].includes(office.label)) {
      requiredFields.push('office')
    }
  }
  return requiredFields
}

const validate = (values, props) => {
  const { office } = props;
  const errors = {}
  const requiredFields = required(office)
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}

function RoleField(props) {
  const classes = useStyles();
  const { roles } = props;
  return (
    <div>
      {(roles.length === 0) ? null :
        <div>
          <InputLabel className={classes.inputLabel}>{messages.roleLabel}</InputLabel>
          <Field
            name='user_role'
            label='User Role'
            component={renderSelectField}
            className={classes.formControl}
            style={{ width: "532px" }}
          >
            {roles.map(item => (
              <option key={item.ID} className={classes.menu} value={item.ID}>{item.NAME}</option>
            ))}
          </Field>
        </div>}
    </div>
  )
}

function SuggestionField(props) {
  const { handleChange, items, office, errors, comingValue, updateSelectedOffice, className, title } = props;

  const handleValueChange = val => {
    updateSelectedOffice(val)
  }

  const legalNames = ['partner__legal_name', 'audit__legal_name'];
  const getUserItemDescription = (item, type_to_search) => {
    if (type_to_search === 'business_unit__code') {
      return item.code
    } else if (legalNames.includes(type_to_search)) {
      if (type_to_search == "partner__legal_name") {
        return item.legal_name && item.legal_name + '-' + item.number
      } else if (type_to_search == "audit__legal_name") {
        return item.legal_name && item.legal_name + '-' + item.id
      }
    } else if (type_to_search === 'field__name') {
      return item.name && item.business_unit && item.name + "-" + item.business_unit.code
    }
    else if (type_to_search === 'alternate__focal') {
      return item
    }
  }

  return (
    <div>
      {(office) ?
        <span>
          <InputLabel className={className.inputLabel}>{title}</InputLabel>
          <Field
            name='office'
            className={className.autoCompleteField}
            component={CustomAutoCompleteField}
            handleValueChange={handleValueChange}
            onChange={handleChange}
            items={items}
            comingValue={comingValue}
            fullWidth
            updateSelectedOffice={updateSelectedOffice}
            type_to_search={office.type_to_search}
            getUserItemDescription={getUserItemDescription}
            showCustomUserLabel={true}
          />
        </span>
        : null}

    </div>
  )
}

function AddUserAccount(props) {
  const { user, handleTypeChange, title, handleChange, handleSubmit, items, office, roles, membership, fo } = props;
  const classes = useStyles();


  var date = new Date()
  const [errors, showError] = useState(false);
  const [key, updateKey] = useState('default');
  const [officeSelected, updateSelectedOffice] = useState('');

  const handleChangeTypeAhead = (e, v) => {
    updateSelectedOffice(v);
    // if (!e.inputValue) {
    //     showError(true);
    // }
    if (items.indexOf(items.find(s => s.legal_name == e.inputValue)) > -1 || items.indexOf(items.find(s => s.name == e.inputValue)) > -1) {
      showError(false);
    }

    // else {
    //   showError(true);
    // }
    handleChange(v)
  }

  const handleChangeType = (event) => {
    updateSelectedOffice([])
    updateKey(date.getTime())
    handleTypeChange(event)
    props.clearFields('addUser', false, 'office')
    props.clearFields('addUser', false, 'user_role')
  }


  function handleSubmitTypeAhead(e) {
    if (!e.inputValue) {
      showError(true);
    }
    if (items.indexOf(items.find(s => s.legal_name == e.inputValue)) > -1 || items.indexOf(items.find(s => s.name == e.inputValue)) > -1) {
      showError(false);
    }
    else {
      showError(true);
    }
  }
  let role = USERS.filter(user => { if (user.ENUM == membership.type) return user })
  return (
    <form className={classes.container} onSubmit={handleSubmit} >
      <Grid
        container
        direction="column" >
        {(membership.type === 'HQ' || membership.type === 'FO') ?
          <div> <InputLabel className={classes.inputLabel}>User Type</InputLabel>
            <Field
              name='user_type'
              component={renderSelectField}
              className={classes.formControl}
              onChange={handleChangeType}
              style={{ width: "532px" }}
              fullWidth
            >
              {membership.type === 'HQ' && USERS.map(item => (
                (item.KEY == USERS_KEY.other) ? null : <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
              ))}
              {membership.type === 'FO' && membership.role === 'REVIEWER' && ReviewerUser.map(item => (
                <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
              ))}
              {membership.type === 'FO' && membership.role === 'APPROVER' && ApproverUser.map(item => (
                <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
              ))}
            </Field>
            <RoleField roles={roles} />
            {(!fo) ? <SuggestionField
              handleChange={handleChangeTypeAhead}
              className={classes}
              items={items}
              title={title}
              key={key}
              office={office}
              errors={errors}
              updateSelectedOffice={updateSelectedOffice}
              style={{ marginLeft: "10px" }}
              comingValue={officeSelected}
            /> : <div>
              <p className={classes.inputLabel}>{title}</p>
              <p className={classes.value}>{(membership && membership.field_office) ? membership.field_office.name : null}</p>
            </div>}
          </div>
          :
          <React.Fragment>
            <div>
              <p className={classes.inputLabel}>User Type</p>
              <p className={classes.value}>{membership.type}</p>
            </div>
            <RoleField roles={role[0].ROLES} />
            <div>
              <p className={classes.inputLabel}>Office/Partner/Agency</p>
              <p className={classes.value}>{membership.partner && membership.partner.name || membership.audit_agency && membership.audit_agency.name}</p>
            </div>
          </React.Fragment>
        }


        <div className={classes.buttonGrid}>
          <CustomizedButton type='submit' clicked={handleSubmitTypeAhead} buttonText={messages.add} buttonType={buttonType.submit} />
        </div>
      </Grid>
    </form>
  );
}

const addUserAccount = reduxForm({
  form: 'addUserAccount',
  validate,
  enableReinitialize: true,
})(AddUserAccount);

const mapStateToProps = (state, ownProps) => {
  let initialValues = {
    id: ownProps.user.id,
    office: (ownProps.membership) ? ownProps.membership.partner && ownProps.membership.partner.name || ownProps.membership.audit_agency && ownProps.membership.audit_agency.name : null
  }

  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(addUserAccount);

export default withRouter(connected);