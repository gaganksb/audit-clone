import React from 'react';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField } from '../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';


const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    width: '100%'
  },
  textField: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginRight: 8
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
    }, 
    label: {
      fontSize: '12px'
    },
    value: {
      fontSize: '16px',
      color: 'black'
    },
}));

const messages = {
  edit: 'Edit',
}

const validate = values => {
  const errors = {}
  const requiredFields = [
    'name',
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}

function EditUserAccount(props) {
  const { handleSubmit, user } = props;
  const classes = useStyles();

  return (
    <form className={classes.container} onSubmit={handleSubmit} >
      <Grid
        container
        direction="column" >
          
        <InputLabel className={classes.inputLabel}>Fullname</InputLabel>
        <Field
          name='name'
          className={classes.textField}
          component={renderTextField}
          defaultValue={user && user.name}
        />

        <div>
          <p className={classes.label}>E-mail</p>
          <p className={classes.value}>{user && user.email}</p>
        </div>

        <div className={classes.buttonGrid}>
          <Grid sm={2} style={{maxWidth: '19%', display: 'flex', justifyContent: 'flex-end'}}>        
          <CustomizedButton
          buttonText={messages.edit} 
          type="submit"
          buttonType={buttonType.action} />
          </Grid>
        </div>
      </Grid>
    </form>
  );
}

const editUserAccount = reduxForm({
  form: 'editUserAccount',
  validate,
  enableReinitialize: true,
})(EditUserAccount);

const mapStateToProps = (state, ownProps) => {
  let initialValues = {
    id: ownProps.user.id,
    name: ownProps.user.name,
    email: ownProps.user.email,
  }

  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(editUserAccount);

export default withRouter(connected);
