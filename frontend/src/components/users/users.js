import React, { Component } from 'react';
import Title from '../common/customTitle';
import HeaderTabs from '../common/headerTabs';
import { browserHistory as history, withRouter } from 'react-router';


const styles = theme => ({
})

const messages = {
  title: 'User management',
}

const tabs = {
  items: [
    {label: 'Users', path: '/users/list'},
    // {label: 'Roles', path: '/users/roles'},
  ]
}


class UsersWrapper extends Component {
  state = {
    value: 0,
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.goTo = this.goTo.bind(this);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  goTo = () => item => {
    history.push(item);
  }

  render() {
    const { children } = this.props;
    const { value } = this.state;
    return (
      <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
        <Title show={false} title={messages.title} />
        { children }
      </div>
    );
  }
}

export default withRouter(UsersWrapper);
