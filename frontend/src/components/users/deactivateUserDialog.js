import React from 'react';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  }
}));

const messages ={ 
  yes: 'YES',
  no: 'NO',
  deactivateText: 'Confirm that you want to deactivate ',
  activateText: 'Confirm that you want to activate ',
  click:' by clicking on '
}
function DeactivateDialog(props) {
  const {open, handleClose, handleDeactivation, user} = props;

  const classes = useStyles();

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{ user.is_active ? "User Deactivation Alert" : "User Activation Alert" }</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText className={classes.dialogContentText} >
            { user.is_active ? 
                <span>{messages.deactivateText} <b>{user.name}</b>{messages.click}<b>{messages.yes}</b></span> :
                <span>{messages.activateText}<b>{user.name}</b>{messages.click}<b>{messages.yes}</b></span> }
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <CustomizedButton 
          clicked={handleClose} 
          buttonText={messages.no} 
          buttonType={buttonType.cancel} />      
          <CustomizedButton 
          clicked={() => handleDeactivation({id: user.id, is_active: user.is_active})} 
          buttonText={messages.yes} 
          buttonType={buttonType.submit} />
          
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default DeactivateDialog;
