import React, { useState, useEffect } from 'react';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import TransferList from '../layout/examples/transferList';
import { USERS } from '../../helpers/constants';
import R from 'ramda';
import { usePrevious } from '../../helpers/customHooks';
import { connect } from 'react-redux';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import { browserHistory as history, withRouter } from 'react-router';
import { loadRolePermissionList } from '../../reducers/users/rolePermissions';

const BootstrapInput = withStyles(theme => ({
    root: {
        'label + &': {
            marginTop: theme.spacing(3),
            marginBottom: theme.spacing(3)
        },
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.background.paper,
        border: '1px solid #ced4da',
        fontSize: 16,
        padding: '10px 26px 10px 12px',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        },
    },
}))(InputBase);

const useStyles = makeStyles(theme => ({
    root: {
        flexWrap: 'wrap',
        textAlign: 'center'
    },
    heading: {
        color: '#344563',
        font: 'bold 16px/21px Roboto',
        fontSize: 14,
        marginBottom: '10px',
        paddingTop: '12px'
    },
    margin: {
        margin: theme.spacing(1),
        width: '80%',
    },
    dropDown: {
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(1),
        backgroundColor: '#3289C7',
        minWidth: 85,
        color: 'white',
        textTransform: 'capitalize'
    },
}));

const messages = {
    apply: 'Apply'
}
const RolesFilter = props => {
    const { permissions, loadRolePermissions, rolePermissions, loading } = props;
    const classes = useStyles();

    const [user, setUser] = useState(USERS[0].KEY);
    const [roles, setRoles] = useState(R.find(R.propEq('KEY', USERS[0].KEY), USERS).ROLES);
    const [role, setRole] = useState(USERS[0].ROLES[0].ID);
    const [readOnly, setReadOnly] = useState(false);
    
    const prevUser = usePrevious(user);
    const prevRole = usePrevious(role);



    const handleChange = event => {
        setUser(event.target.value);
        setRoles(R.find(R.propEq('KEY', event.target.value), USERS).ROLES);
    };
    const handleRoleChange = event => {
        setRole(event.target.value);
    }
    function test(e) {
        e.preventDefault();
    }
    useEffect(() => {
        loadRolePermissions(user, role);
    }, [user, role]);
    return (
        <form className={classes.root} autoComplete="off">
            <div style={{ marginTop: '25px' }}>
                <InputLabel className={classes.heading}><strong>Type of User</strong></InputLabel>
            </div>
            <FormControl className={classes.margin}>
                <NativeSelect
                    className={classes.dropDown}
                    value={user}
                    onChange={handleChange}
                    input={<BootstrapInput name="user" disabled={readOnly}/>}
                >
                    {
                        USERS.map(item => <option value={item.KEY}>{item.NAME}</option>)
                    }
                </NativeSelect>
            </FormControl>
            <div>
                <div style={{ marginTop: '25px' }}>
                    <InputLabel className={classes.heading}><strong>Role of User</strong></InputLabel>
                </div>
                <FormControl className={classes.margin}>
                    <NativeSelect
                        className={classes.dropDown}
                        value={role}
                        onChange={handleRoleChange}
                        input={<BootstrapInput name="role" disabled={readOnly} />}
                    >
                        {
                            roles.map(item => <option value={item.ID}>{item.NAME}</option>)
                        }
                    </NativeSelect>
                </FormControl>
            </div> 
            <div style={{ marginTop: '25px' }}>
                <InputLabel className={classes.heading}><strong>Manage Permissions</strong></InputLabel>
                <TransferList permissions={permissions} rolePermissions={rolePermissions} loading={loading}/>
                <div className={classes.buttonGrid}>
                    <Grid>
                        <CustomizedButton
                            buttonText={messages.apply}
                            type="submit"
                            buttonType={buttonType.submit} />
                    </Grid>
                </div>
            </div>
        </form>
    );
}


const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    location: ownProps.location,
    pathName: ownProps.location.pathname,
    rolePermissions: state.rolePermissions.details,
    loading: state.rolePermissions.loading
});

const mapDispatch = dispatch => ({
    loadRolePermissions: (user_type, user_role__id, params) => dispatch(loadRolePermissionList(user_type, user_role__id, params))
});

const connectedRolesFilter = connect(mapStateToProps, mapDispatch)(RolesFilter);
export default withRouter(connectedRolesFilter);