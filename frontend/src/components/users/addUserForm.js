import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import Grid from '@material-ui/core/Grid';
import { renderTextField, renderSelectField } from '../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import { USERS } from '../../helpers/constants';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import CustomAutoCompleteField from '../forms/TypeAheadFields';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { loadPartnerList } from '../../reducers/partners/partnerList';
import { OFFICE_LABELS, USERS_KEY } from '../../helpers/constants';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginRight: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
  },
  buttonGrid: {
    marginTop: 16,
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end'
  },
  formControl: {
    width: '100%',
    marginRight: theme.spacing(1),
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  autoCompleteField: {
    marginLeft: 2,
    marginRight: theme.spacing(1),
    width: '100%',
    marginTop: 1
  },
}));

const messages = {
  // selectValid: 'Select valid option from dropdown',
  submit: 'Submit',
  cancel: 'Cancel'
}

const ReviewerUser = [
  {
    KEY: 'partners',
    NAME: 'Partner Member',
    ENUM: 'PARTNER',
    MODEL: 'partnermember',
    ROLES: [
      { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
      { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
    ],
    OFFICE: {
      label: OFFICE_LABELS.partner,
      type_to_search: 'partner__legal_name'
    }
  },
];

const ApproverUser = [{
  KEY: 'field',
  NAME: 'Field Office Member',
  ENUM: 'FO',
  MODEL: 'fieldmember',
  ROLES: [
    { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
    { NAME: 'Approver', ID: 2, ENUM: 'APPROVER' }
  ],
  OFFICE: {
    label: OFFICE_LABELS.field,
    type_to_search: 'field__name'
  }
},
{
  KEY: 'partners',
  NAME: 'Partner Member',
  ENUM: 'PARTNER',
  MODEL: 'partnermember',
  ROLES: [
    { NAME: 'Reviewer', ID: 1, ENUM: 'REVIEWER' },
    { NAME: 'Admin', ID: 3, ENUM: 'ADMIN' },
  ],
  OFFICE: {
    label: OFFICE_LABELS.partner,
    type_to_search: 'partner__legal_name'
  }
},]
const required = (office) => {
  let requiredFields = [
    'name',
    'email',
    'user_type',
    'user_role'
  ]
  if (office) {
    if ([OFFICE_LABELS.audit, OFFICE_LABELS.field, OFFICE_LABELS.partner].includes(office.label)) {
      requiredFields.push('office')
    }
  }
  return requiredFields
}

const validate = (values, props) => {
  const { office } = props;
  const errors = {}
  const requiredFields = required(office)

  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}

function RoleField(props) {
  const classes = useStyles();
  const { roles } = props;

  return (
    <React.Fragment>
      {(roles.length === 0) ? null :

        <React.Fragment>
          <InputLabel className={classes.inputLabel}>User Role</InputLabel>
          <Field
            name='user_role'
            label='User Role'
            component={renderSelectField}
            className={classes.formControl}
          >
            {roles.map(item => (
              <option key={item.ID} value={item.ID} className={classes.menu}>{item.NAME}</option>
            ))}
          </Field>
        </React.Fragment>}
    </React.Fragment>
  )
}


function SuggestionField(props) {
  const { handleChange, items, office, errors, comingValue, className, updateSelectedOffice, title } = props;
  const handleValueChange = val => {
    updateSelectedOffice(val)
  }

  const legalNames = ['partner__legal_name', 'audit__legal_name'];
  const getUserItemDescription = (item, type_to_search) => {
    if (type_to_search === 'business_unit__code') {
      return item.code
    } else if (legalNames.includes(type_to_search)) {
      if (type_to_search == "partner__legal_name") {
        return item.legal_name && item.legal_name + '-' + item.number
      } else if (type_to_search == "audit__legal_name") {
        return item.legal_name && item.legal_name + '-' + item.id
      }
    } else if (type_to_search === 'field__name') {
      return item.name && item.business_unit && item.name + "-" + item.business_unit.code
    }
    else if (type_to_search === 'alternate__focal') {
      return item
    }
  }

  return (
    <div>
      {(office) ?
        <span style={{ width: '100%' }}>
          <InputLabel className={className.inputLabel}>{title}</InputLabel>
          <Field
            name='office'
            className={className.autoCompleteField}
            component={CustomAutoCompleteField}
            handleValueChange={handleValueChange}
            onChange={handleChange}
            items={items}
            comingValue={comingValue}
            fullWidth
            updateSelectedOffice={updateSelectedOffice}
            type_to_search={office.type_to_search}
            getUserItemDescription={getUserItemDescription}
            showCustomUserLabel={true}
          />
        </span>
        : null}
      {(office && errors) ? <div style={{ color: 'red', fontSize: 12 }}>{messages.selectValid}</div> : null}

    </div>
  )
}

function AddUserForm(props) {
  const classes = useStyles();
  const { handleTypeChange, handleChange, handleSubmit, items, title, office, roles, membership, handleClose, fo } = props;

  var date = new Date()
  const [errors, showError] = useState(false);
  const [key, updateKey] = useState('default')
  const [officeSelected, updateSelectedOffice] = useState('');

  function handleChangeTypeAhead(e, v) {
    updateSelectedOffice(v);
    if (items.indexOf(items.find(s => s.legal_name == v)) > -1 || items.indexOf(items.find(s => s.name == v)) > -1) {
      showError(false);
    }

    handleChange(v)
  }


  const handleChangeType = (event) => {
    updateSelectedOffice([])
    updateKey(date.getTime())
    handleTypeChange(event)
    props.clearFields('addUser', false, 'office')
    props.clearFields('addUser', false, 'user_role')
  }

  function handleSubmitTypeAhead(e) {
    if (!e.inputValue) {
      showError(true);
    }
    if (items.indexOf(items.find(s => s.legal_name == e.inputValue)) > -1 || items.indexOf(items.find(s => s.name == e.inputValue)) > -1) {
      showError(false);
    }
    else {
      showError(true);
    }
  }

  let role = USERS.filter(user => { if (user.ENUM == membership.type) return user })
  return (
    <div className={classes.root}>
      <form className={classes.container} onSubmit={handleSubmit}>
        <Grid
          container
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
        >
          <InputLabel className={classes.inputLabel}>Fullname</InputLabel>
          <Field
            name='name'
            className={classes.textField}
            component={renderTextField}
          />
          <InputLabel className={classes.inputLabel}>Email</InputLabel>
          <Field
            name='email'
            className={classes.textField}
            component={renderTextField}
          />
          {(membership.type === 'HQ' || membership.type === 'FO') ? (
            <div style={{ width: "100%" }}>
              <InputLabel className={classes.inputLabel}>User Type</InputLabel>
              {
                <Field
                  name='user_type'
                  component={renderSelectField}
                  className={classes.formControl}
                  onChange={handleChangeType}
                  fullWidth
                >
                  {membership.type === 'HQ' && USERS.map(item => (
                    (item.KEY == USERS_KEY.other) ? null : <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
                  ))}
                  {membership.type === 'FO' && membership.role === 'REVIEWER' && ReviewerUser.map(item => (
                    <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
                  ))}
                  {membership.type === 'FO' && membership.role === 'APPROVER' && ApproverUser.map(item => (
                    <option key={item.KEY} value={item.KEY} className={classes.menu}>{item.NAME}</option>
                  ))}
                </Field>
              }
              <RoleField
                className={classes.formControl}
                roles={roles}
              />
              {(!fo) ? <SuggestionField
                title={title}
                handleChange={handleChangeTypeAhead}
                items={items}
                key={key}
                className={classes}
                office={office}
                errors={errors}
                comingValue={officeSelected}
                updateSelectedOffice={updateSelectedOffice}
              /> : <div>
                <p className={classes.inputLabel}>{title}</p>
                <p className={classes.value}>{(membership && membership.field_office) ? membership.field_office.name : null}</p>
              </div>
              }
            </div>
          ) : (
            <React.Fragment>
              <div>
                <p className={classes.inputLabel}>User Type</p>
                <p className={classes.value}>{membership.type}</p>
              </div>
              <RoleField roles={role[0].ROLES} />
              <div>
                <p className={classes.inputLabel}>Office/Partner/Agency</p>
                <p className={classes.value}>{membership.partner && membership.partner.name || membership.audit_agency && membership.audit_agency.name}</p>
              </div>
            </React.Fragment>)
          }
          <div className={classes.buttonGrid}>
            <CustomizedButton clicked={handleClose} buttonText={messages.cancel} buttonType={buttonType.cancel} />
            <CustomizedButton type='submit' clicked={handleSubmitTypeAhead} buttonText={messages.submit} buttonType={buttonType.submit} />
          </div>
        </Grid>
      </form>
    </div>
  );
}

const addUser = reduxForm({
  form: 'addUser',
  validate,
  enableReinitialize: true,
})(AddUserForm);

const mapStateToProps = (state, ownProps) => {
  let initialValues = {
    // user_type: ownProps.membership.type,
    // office: (ownProps.membership) ? ownProps.membership.partner && ownProps.membership.partner.name || ownProps.membership.audit_agency && ownProps.membership.audit_agency.name : null
  }

  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'addUser', message))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(addUser);

export default withRouter(connected);
