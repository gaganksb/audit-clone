import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { Divider } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { setDefaultUserAccount } from '../../reducers/users/account';
import { loadUser } from '../../reducers/users/userData';
import { loadUserData } from '../../reducers/session';
import { loadNav } from '../../reducers/nav';
import { showSuccessSnackbar } from '../../reducers/successReducer';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    marginTop: '20px',
    marginLeft: '20px',
    marginRight: '20px'
  },
  title: {
    fontWeight: 600
  }
});


const messages = {
  fullname: 'Fullname',
  email: 'Email',
  accounts: 'Accounts',
  type: 'User Type',
  role: 'User Role',
  office: 'Office/Agency/Partner',
  default: 'Active',
  notDefault: 'Select Account',
  error: {
    unknown: 'Not specified',
  },
  success: {
    profile: 'Profile Selected !'
  }
}


function ProfileGrid(props) {
  const { classes, id, name, email, accounts, setDefaultUserAccount, loadUser, loadNav, loadUserData, successReducer } = props;

  const handleActivate = (account) => {
    const {id, model} = account
    const body = {
      is_default: true
    }
    return setDefaultUserAccount(id, model, body).then((success) => {
      loadUser().then(() => {
        loadUserData()
        loadNav()
        successReducer(messages.success.profile)
      })
    })
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={24}>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={16}>
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1" className={classes.title}>
                  {messages.fullname}
                </Typography>
                <Typography gutterBottom>{ name || messages.error.unknown }</Typography>
                <Typography color="textSecondary" gutterBottom>ID: { id }</Typography>
              </Grid>
              <Divider />
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1" style= {{marginTop: 4}} className={classes.title}>
                  {messages.email}
                </Typography>
                <Typography gutterBottom>{ email }</Typography>
              </Grid>
              <Divider />
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1" style= {{marginTop: 4}} className={classes.title}>
                  {messages.accounts}
                </Typography>
                <Grid container spacing={3}>
                  <Grid item xs={3}>
                    <Typography gutterBottom variant="subtitle2">
                      {messages.type}
                    </Typography>
                  </Grid>
                  <Grid item xs={3}>
                    <Typography gutterBottom variant="subtitle2">
                      {messages.role}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography gutterBottom variant="subtitle2">
                      {messages.office}
                    </Typography>
                  </Grid>
                </Grid>
                <Divider />
                {(accounts) ? accounts.map((item, key) => (
                  <div>
                    <Grid container alignItems="center" style={{margin: 2}}>
                      <Grid item xs={3}>
                        <Typography color="textSecondary">{item.type}</Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography color="textSecondary">{item.role}</Typography>
                      </Grid>
                      <Grid item xs={3}>
                        <Typography color="textSecondary">{item.office}</Typography>
                      </Grid>
                      <Grid item xs={3} style={{display:'flex', justifyContent:'flex-end'}}>
                        {(item.is_default) ? 
                          <Button color="primary" size='small' disabled>
                            {messages.default} {item.is_default}
                          </Button> : 
                          <Button color="primary" size='small' onClick={() => handleActivate(item)}>
                            {messages.notDefault} {item.is_default}
                          </Button>}
                      </Grid>
                    </Grid>
                    <Divider />
                  </div>
                )) : null}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}

ProfileGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
  setDefaultUserAccount: (id, type, body) => dispatch(setDefaultUserAccount(id, type, body)),
  loadUser: () => dispatch(loadUser()),
  loadNav: () => dispatch(loadNav()),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  loadUserData: () => dispatch(loadUserData())
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileGrid);

export default withStyles(styles)(connected);