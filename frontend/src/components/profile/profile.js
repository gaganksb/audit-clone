import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { loadUser } from '../../reducers/users/userData';
import ProfileGrid from './profileGrid';
import { setUserTypeRole } from '../../utils/accountsParser';

const messages = {
  title: 'Profile',
}

const Profile = (props) => {
  const { id, name, email, accounts, loadUser, user, nav } = props;
  const [parsedAccounts, setParsedAccounts] = useState(null)

  useEffect(() => {
    loadUser();
  }, []);

  useEffect(() => {
    setParsedAccounts(setUserTypeRole(accounts))
  }, [user]);

  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <div style={{
        paddingLeft: 22,
        paddingTop: 19,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#606060',
        font: 'bold 18px/24px Roboto',
        letterSpacing: 0,
        backgroundColor: 'white',
        paddingBottom: 20
      }}>
        {messages.title}
      </div>
      <ProfileGrid id={id} name={name} email={email} accounts={parsedAccounts} />
    </div>
  )
}

const mapStateToProps = state => ({
  user: state.userData.data,
  id: state.userData.data.id,
  name: state.userData.data.name,
  email: state.userData.data.email,
  accounts: state.userData.data.accounts,
});

const mapDispatchToProps = dispatch => ({
  loadUser: () => dispatch(loadUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
