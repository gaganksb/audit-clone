import React from "react";
import { Field } from "redux-form";
import Downshift from "downshift";
import TextField from '@material-ui/core/TextField';


const itemToString = item => (item ? item : "");

const legalNames = ['partner__legal_name', 'audit__legal_name'];
const getItemDescription = (item, type_to_search) => {
  if (type_to_search === 'business_unit__code') {
    return item.code
  } else if (legalNames.includes(type_to_search)) {
    return item.legal_name
  } else if (type_to_search === 'field__name') {
    return item.name
  }
  else if (type_to_search === 'alternate__focal') {
    return item
  }
}

const DownShiftInput = ({ 
  input, 
  meta: { touched, invalid, error },
  label,
  margin,
  type_to_search,
  handleChange,
  readOnly,
  comingValue,
  className,
  items }) => (
  
  <Downshift
    {...input}
    onStateChange={(e) => handleChange(e)}
    itemToString={itemToString}
    selectedItem={input.value}
  >
    {({
      getInputProps,
      getItemProps,
      isOpen,
      inputValue,
    }) => {
      const filteredItems = items.map(item => getItemDescription(item, type_to_search));
      return (
        <span>
            <TextField
              label={label}
              margin={margin}
              className={className}
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                readOnly: readOnly,
                value: (comingValue)? comingValue : null
              }}
              {...getInputProps({
                name: input.name,
                onBlur: input.onBlur
              })}
            />
            {isOpen &&
            !!filteredItems.length && (
              <div style={{ margin: '0 auto',border: '1px solid whitesmoke',  maxHeight: 100 ,overflowY: 'auto', backgroundColor: '#FFFF', position: 'relative', zIndex: 2, minWidth: '18%', width: '90%'}}>
                {filteredItems.map((item, index) => (
                  <div style={{marginBottom: '5px', cursor: 'pointer', marginLeft: 10, marginTop: 5}}
                    {...getItemProps({
                      key: item,
                      index,
                      item,
                    })}
                  >
                    {(type_to_search === 'alternate__focal')? item.name : item}
                  </div>
                ))}
              </div>
            )}
        </span>
      );
    }}
  </Downshift>
);

function TypeAheadField(props) {
  return(
    <div style={{width:'100%'}}>
      <Field defaultValue={props.comingValue} component={DownShiftInput} {...props} />
    </div>
  );
}

export default TypeAheadField;