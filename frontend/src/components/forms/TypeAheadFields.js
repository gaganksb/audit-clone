import React from 'react';
import { Autocomplete } from '@material-ui/lab'
import { TextField } from "@material-ui/core"


const legalNames = ['partner__legal_name', 'audit__legal_name'];
const getItemDescription = (item, type_to_search) => {
  if (type_to_search === 'business_unit__code') {
    return item.code
  } else if (legalNames.includes(type_to_search)) {
    return item.legal_name
  } else if (type_to_search === 'field__name') {
    return item.name
  }
  else if (type_to_search === 'alternate__focal') {
    return item
  }
}

function RenderAutoCompleteField({
  label,
  className,
  margin,
  handleChange,
  handleValueChange,
  items,
  input,
  comingValue,
  meta: { touched, invalid, error },
  multiple,
  errors,
  key,
  type_to_search,
  style,
  showCustomUserLabel,
  getUserItemDescription,
  ...custom,
}) {
  return (
    <Autocomplete
      options={items}
      key={key}
      getOptionLabel={option => showCustomUserLabel ? getUserItemDescription(option, type_to_search) : getItemDescription(option, type_to_search)}
      renderInput={params => (
        <TextField
          {...params}
          label={label}
          className={className}
          margin={margin}
          InputLabelProps={{
            shrink: true,
          }}
          error={touched && invalid}
          helperText={touched && error}
          {...input}
          {...custom}
          fullWidth
        />
      )}
      style={style}
      defaultValue={(comingValue) ? comingValue : []}
      multiple={multiple}
      onChange={(e, v) => handleValueChange(v)}
    />
  )
}

export default RenderAutoCompleteField