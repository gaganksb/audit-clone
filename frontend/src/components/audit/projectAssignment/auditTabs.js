import React, { useState } from 'react'
import { browserHistory as history, withRouter } from 'react-router'
import HeaderTabs from '../../common/headerTabs'

const messages = {
  title: 'Audit',
  tabTitle: ['AUDIT MANAGEMENT'],
  error: 'Error'
}

const tabs = {
  items: [
    { label: 'AUDIT MANAGEMENT', path: '/audit' }
  ]
}

const auditTabs = ({ children }) => {
  const [value, setValue] = useState(0)

  const handleChange = (event, value) => {
    setValue(value)
  }

  const goTo = () => item => {
    history.push(item)
  }

  return (
    <React.Fragment>
      <div>
        <div
          style={{
            paddingLeft: 22,
            paddingTop: 19,
            borderBottom: '1px solid rgba(224, 224, 224, 1)',
            color: '#606060',
            font: 'bold 18px/24px Roboto',
            letterSpacing: 0,
            backgroundColor: 'white',
            paddingBottom: 20
          }}
        >
          {messages.title}
        </div>
        <HeaderTabs tabs={tabs} handleChange={handleChange} goTo={goTo()} value={value} />
        { children }
      </div>
    </React.Fragment>
  )
}

export default withRouter(auditTabs)
