import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { formatMoney } from '../../../utils/currencyFormatter';
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router'
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';

const ADMIN_ROLES = ["ADMIN", "PREPARER", "REVIEWER", "APPROVER"]

const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    icon: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    tableIncludeRow: {
        backgroundColor: '#F5DADA'
    },
    detailHeader: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 500
    },
    details: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 300,
    },
    detailRow: {
        width: '468%',
        marginLeft: 64,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
    },
    viewProject: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        borderBottom: '1px solid rgb(112,112,112)',
        '&:hover': {
            cursor: 'pointer'
        },
    }
}));

const messages = {
    detailHeaders: [
        'AUDIT FIRM',
        'UNHCR Audit Focal Person',
        'UNHCR Audit Alternate Focal Person'
    ],
    foHeaders: [
        'AUDIT FIRM',
        'Field Office Audit Focal Person',
        'Field Office Audit Alternate Focal Person'
    ],
    auditHeader: [
        'Auditor Reviewers'
    ],
    partnerHeader: [
        'Partner Focal Person',
        'Partner Alternate Focal Person'
    ]
}

function AssignmentDetails(props) {
    const {
        expanded,
        handleExpand,
        item,
        userType,
        handleReason,
        allSelected,
        numSelected,
        isItemSelected,
        rowCount,
        year,
        handleClick,
        session,
        isAdmin
    } = props;

    const [checked, updateChecked] = React.useState(false);

    function handleCheckboxClick(event, item) {
        updateChecked(!checked);
        handleClick(event, item)
    }

    useEffect(() => {
        updateChecked(false)
    }, [item])


    function displayExpandedRow(item) {
        switch (userType) {
            case 'unhcr': return (<TableRow>
                <TableCell colSpan={18} className={classes.tableCell}>
                    <div style={{ marginTop: 10 }}>
                        <table cellspacing="0" cellPadding="0" style={{ width: '100%', borderCollapse: 'collapse' }}>
                            <tr>
                                {messages.detailHeaders.map((item, key) => (
                                    <th style={{ paddingBottom: 10 }}>{item}</th>
                                ))}
                            </tr>
                            <tr>
                                <td style={{ paddingBottom: 11 }}>
                                    {item.audit_firm && item.audit_firm.legal_name}
                                </td>
                                <td style={{ paddingBottom: 11 }}>
                                    {(item.agreement_member.unhcr.focal) ? `${item.agreement_member.unhcr.focal.fullname || ""} ${item.agreement_member.unhcr.focal.email}` : ' '}
                                </td>
                                <td style={{ paddingBottom: 11 }}>
                                    {(item.agreement_member.unhcr.alternate_focal) ? `${item.agreement_member.unhcr.alternate_focal.fullname || ""} ${item.agreement_member.unhcr.alternate_focal.email}` : ' '}
                                </td>
                            </tr>
                        </table>
                    </div>
                </TableCell>
            </TableRow>)
                break;
            case 'fo': return (<TableRow>
                <TableCell colSpan={18} className={classes.tableCell}>
                    <div style={{ marginTop: 10 }}>
                        <table cellspacing="0" cellPadding="0" style={{ width: '100%', borderCollapse: 'collapse' }}>
                            <tr>
                                {messages.foHeaders.map((item, key) => (
                                    <th style={{ paddingBottom: 10 }}>{item}</th>
                                ))}
                            </tr>
                            <tr>
                                <td style={{ paddingBottom: 11 }}>
                                    {item.audit_firm && item.audit_firm.legal_name}
                                </td>
                                <td style={{ paddingBottom: 11 }}>
                                    {(item.agreement_member.unhcr.focal) ? item.agreement_member.unhcr.focal.fullname : ' '}
                                </td>
                                <td style={{ paddingBottom: 11 }}>
                                    {(item.agreement_member.unhcr.alternate_focal) ? item.agreement_member.unhcr.alternate_focal.fullname : ' '}
                                </td>
                            </tr>

                            {/* <tr>
                                <th style={{ paddingTop: 10, paddingBottom: 5 }}>Reviewers</th>
                            </tr>
                            <tr>
                                <td style={{ paddingBottom: 5 }}>
                                    <tr>{(item.agreement_member.unhcr.reviewers) ? item.agreement_member.unhcr.reviewers.map((reviewer) => reviewer.fullname).join(", ") : null}
                                    </tr>
                                </td>
                            </tr> */}

                        </table>
                    </div>
                </TableCell>
            </TableRow>)
                break;
            case 'auditing': return (<TableRow>
                <TableCell colSpan={18} className={classes.tableCell}>
                    <div style={{ marginTop: 10 }}>
                        <table cellspacing="0" cellPadding="0" style={{ width: '100%', borderCollapse: 'collapse' }}>
                            <tr>
                                {messages.auditHeader.map((item, key) => (
                                    <th style={{ paddingBottom: 10 }}>{item}</th>
                                ))}
                            </tr>
                            <tr>
                                <td style={{ paddingBottom: 11 }}>
                                    {(item.agreement_member.auditing.reviewers) ? item.agreement_member.auditing.reviewers.map((reviewer) => reviewer.fullname).join(", ") : null}
                                </td>
                            </tr>
                        </table>
                    </div>
                </TableCell>
            </TableRow>)
                break;
            case 'partner': return (<TableRow>
                <TableCell colSpan={18} className={classes.tableCell}>
                    <div style={{ marginTop: 10 }}>
                        <table cellspacing="0" cellPadding="0" style={{ width: '100%', borderCollapse: 'collapse' }}>
                            <tr style={{ paddingBottom: 5 }}>
                                {messages.partnerHeader.map((item, key) => (
                                    <th style={{ paddingBottom: 10 }}>{item}</th>
                                ))}
                            </tr>
                            <tr colSpan={18} style={{ borderBottom: '1px solid rgba(224, 224, 224, 1)' }}>
                                <td style={{ paddingBottom: 11 }}> {(item.agreement_member.partner.focal) ? item.agreement_member.partner.focal.fullname : ' '}</td>
                                <td style={{ paddingBottom: 11 }}>{(item.agreement_member.partner.alternate_focal) ? item.agreement_member.partner.alternate_focal.fullname : ' '}</td>
                            </tr>
                        </table>
                    </div>
                </TableCell>
            </TableRow>)
                break;

        }
    }
    const classes = useStyles();

    const handleClassName = (item) => {
        if (userType === 'unhcr' || userType === 'fo') {
            if (
                item &&
                item.agreement_member &&
                item.agreement_member.unhcr &&
                item.agreement_member.unhcr.focal &&
                item.audit_firm
            ) {
                return
            }
            else {
                return classes.tableIncludeRow
            }
        }

        if (userType === 'auditing') {
            if (item && item.agreement_member && item.agreement_member.auditing && item.agreement_member.auditing.reviewers.length > 0) {
                return
            }
            else {
                return classes.tableIncludeRow
            }
        }

        if (userType === 'partner') {
            if (item && item.agreement_member
                && item.agreement_member.partner && item.agreement_member.partner.focal &&
                item.agreement_member.partner.alternate_focal) {
                return
            }
            else {
                return classes.tableIncludeRow
            }
        }

    }

    return (
        <React.Fragment>
            <TableRow key={item.partner.legal_name}
                className={handleClassName(item)}
                aria-checked={isItemSelected}
                selected={isItemSelected}
                tabIndex={-1}
            >
                {/* {
                    isAdmin() ? (!allSelected) ? (
                        <TableCell className={classes.viewProject} component="th" scope="row">
                            <Checkbox
                                checked={checked}
                                selected={isItemSelected}
                                onClick={event => handleCheckboxClick(event, item)}
                            />
                        </TableCell>
                    ) : (
                            <TableCell className={classes.viewProject}></TableCell>
                        ) : <TableCell className={classes.viewProject}></TableCell>
                } */}
                {
                    (isAdmin() && !allSelected) ? (
                        <TableCell className={classes.viewProject} component="th" scope="row">
                            <Checkbox
                                checked={checked}
                                selected={isItemSelected}
                                onClick={event => handleCheckboxClick(event, item)}
                            />
                        </TableCell>
                    ) : <TableCell className={classes.viewProject}></TableCell>
                }
                <TableCell className={classes.viewProject}>
                    {(expanded) ?
                        <ExpandLess className={classes.icon} onClick={handleExpand()} /> :
                        <ExpandMore className={classes.icon} onClick={handleExpand()} />
                    }
                </TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{handleReason(item)}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.business_unit}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.number}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.country}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.legal_name}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.number}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>${formatMoney(item.budget)}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>${formatMoney(item.installments_paid)}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.ws_group_list}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.implementer_type && item.partner.implementer_type.implementer_type}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.start_date}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.end_date}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.agreement_type}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.agreement_date}</TableCell>
                <TableCell className={classes.viewProject} onClick={() => history.push("/projects/" + `${item.id}`)}>{item.operation && item.operation.code}</TableCell>
            </TableRow>
            {(expanded) ?
                displayExpandedRow(item) : null
            }
        </React.Fragment>
    );
}
const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(AssignmentDetails);

export default withRouter(connected);