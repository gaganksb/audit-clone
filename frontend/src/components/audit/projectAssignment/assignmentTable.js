import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import { reset } from 'redux-form'
import ListLoader from '../../common/listLoader'
import TablePaginationActions from '../../common/tableActions'
import AssignmentDetail from './assignmentDetail'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { PROJECT_SENSE_CHECK } from '../../common/consts';
import { SENSE_CHECKS } from '../../../helpers/constants';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid #E9ECEF',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  noDataRow: {
    margin: 16,
    width: '100%',
    textAlign: 'center'
  },
  tableWrapper: {
    width: (window.navigator.userAgent.includes("Windows")) ? '95vw' : '97vw',
    maxHeight: 600,
    overflow: 'auto',
    borderRadius: 6
  },
  viewProject: {
    fontSize: 14,
    fontFamily: 'Roboto',
    color: '#344563',
    '&:hover': {
      cursor: 'pointer'
    },
    borderBottom: '1px solid rgb(112,112,112)',
    padding: '10px 56px 10px 24px'
  },
  deletedRow: {
    backgroundColor: 'red'
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
    transform: 'translateY(-1px)'
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#344563',
    borderBottom: '1px solid rgb(112,112,112)'
  },
  paginationSpacer: {
    flex: 'none'
  },
  paginationCaption: {
    marginRight: theme.spacing(6)
  }
}))

const rowsPerPage = 10

const EXCLUDE = 'EXCLUDE'

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    sort,
    messages,
    postError,
    handleClick,
    allSelected,
    cycle,
    userType,
    year,
    session,
    isAdmin
  } = props

  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }

  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }

  const classes = useStyles()
  const [open, setOpen] = React.useState({ status: false, type: null })
  const [selected, setSelected] = React.useState([]);
  const [expanded, setExpanded] = React.useState(initialExpanded())
  const [headers, setHeaders] = React.useState(messages.table.headers.slice(1))


  useEffect(() => {
    setExpanded(initialExpanded())
  }, [items]);

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }

  const handleReason = (item) => {

    if (item && item.reason) {
      for (let element in SENSE_CHECKS) {
        let obj = SENSE_CHECKS[element];
        if (obj.id && obj.id == item.reason) {
          return obj.description;
        }
      }
    }
    return 'Project below Risk Threshold';
  }

  const isSelected = key => selected.indexOf(key) !== -1;

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <Table className={classes.table}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>
              {messages.title}
            </div>
            <TableHead>
              <TableRow className={classes.headerRow}>
                {headers.map((item, key) =>
                  ((cycle && cycle.status < 4) && item.title == '') ?
                    null :
                    <TableCell align={item.align} key={key} className={classes.tableHeader} style={{ position: 'sticky', top: 0, backgroundColor: '#F6F9FC', zIndex: 2 }}>
                      {(item.title == '' || item.title == 'Sense Check') ?
                        item.title : <TableSortLabel
                          active={item.field === sort.field}
                          direction={sort.order}
                          onClick={() => handleSort(item.field, sort.order)}
                        >
                          {item.title}
                        </TableSortLabel>
                      }
                    </TableCell>
                )}
              </TableRow>
            </TableHead>
            {
              (items.length == 0 && !loading) ?
                <div className={classes.noDataRow}>No data for the selected criteria</div>
                :
                <TableBody>
                  {items.map((item, key) => {
                    const isItemSelected = isSelected(key);
                    return (<AssignmentDetail
                      session={session}
                      isAdmin={isAdmin}
                      key={key}
                      item={item}
                      allSelected={allSelected}
                      expanded={expanded[key]}
                      handleExpand={() => handleExpand(key)}
                      isItemSelected={isItemSelected}
                      formattingClass={classes}
                      handleReason={handleReason}
                      handleClick={handleClick}
                      userType={userType}
                      year={year}
                    />)
                  }
                  )}
                </TableBody>
            }
          </div>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={1}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                classes={{
                  spacer: classes.paginationSpacer,
                  caption: classes.paginationCaption
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </ListLoader>
    </Paper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
  session: state.session
})

const mapDispatchToProps = (dispatch) => ({
  resetCommentForm: () => dispatch(reset('commentDialog')),
  editProject: (id, body) => dispatch(editProject(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editProject', message)),
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)
