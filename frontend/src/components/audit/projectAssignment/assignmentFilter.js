import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import { renderTextField, renderSelectField } from '../../common/renderFields';
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import { Field, reduxForm, change } from 'redux-form'
import CustomizedButton from '../../common/customizedButton';
import { buttonType, filterButtons } from '../../../helpers/constants';
import { connect } from 'react-redux'
import ListLoader from '../../common/listLoader'
import Tooltip from '@material-ui/core/Tooltip'
import MenuItem from "@material-ui/core/MenuItem"
import { reset } from 'redux-form';
import Radio from '@material-ui/core/Radio'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { SENSE_CHECKS } from '../../../helpers/constants'
import { renderMultiSelectField, renderRadioField } from '../../../helpers/formHelper'
import renderTypeAhead from '../../common/fields/commonTypeAhead';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';
import InputLabel from '@material-ui/core/InputLabel';
import { loadGroupList } from '../../../reducers/audits/groups/groupList';
import { loadAuditList } from '../../../reducers/audits/auditList'

const useStyles = makeStyles(theme => ({
  root: {
    margin: theme.spacing(3),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
  },
  container: {
    display: 'flex',
    paddingBottom: 8
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '90%',
    marginTop: 2
  },
  menu: {
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8'
    }
  },
  formControl: {
    marginTop: 1,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '92%',
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '93%',
    marginTop: 2
  },
  typeAhead: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '90%',
  },
  label: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  select: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    '& > div': {
      width: '100%'
    }
  },
  radio: {
    marginLeft: theme.spacing(2),
  },
  radioButton: {
    '&.Mui-checked': {
      color: '#0072BC'
    }
  }
}))

const messages = {
  radio1: {
    assigned: {
      value: 'assigned',
      label: 'Assigned'
    },
    notAssigned: {
      value: 'not_assigned',
      label: 'Not Assigned'
    },
    all: {
      value: 'all',
      label: 'All'
    },
  },
  radio: {
    all: {
      value: 'all',
      label: 'All'
    },
    included: {
      value: 'included',
      label: 'Included'
    },
    excluded: {
      value: 'excluded',
      label: 'Excluded'
    }
  }
}


const partner_type_to_search = 'partner__legal_name';


function AuditFilter(props) {
  const classes = useStyles();
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());

  const { handleSubmit, loading, reset, resetFilter, reasons, loadGroupLists, groupList,
    loadAuditLists, firmList, userType, isAdmin, BUList, loadBUList, partnerList, partner, year,
    handleChangePartner, setSelectedPartner } = props;


  const [key, setKey] = React.useState('default')

  var date = new Date();
  const updateOptionsList = (view_items) => {
    let res = []
    for (let prop in SENSE_CHECKS) {
      if (view_items == messages.radio.included.value && SENSE_CHECKS[prop] == SENSE_CHECKS.manually_excluded) {
        continue
      } else if (view_items == messages.radio.excluded.value && SENSE_CHECKS[prop] != SENSE_CHECKS.manually_excluded) {
        continue
      } else {
        res.push(SENSE_CHECKS[prop])
      }
    }
    return res
  }
  const updateReasons = (view_items, reasons) => {
    let res = []
    if (view_items == messages.radio.included.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] != SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i])
        }
      }
    } else if (view_items == messages.radio.excluded.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] == SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i])
        }
      }
    } else { res = reasons }
    return res
  }

  const radioOptions = (() => {
    let res = []
    for (let prop in messages.radio1) {
      res.push(messages.radio1[prop])
    }
    return res
  })()

  const [optionsList, setOptionsList] = React.useState(updateOptionsList(messages.radio.all.value))
  const [selectedReasons, setSelectedReasons] = React.useState([])

  const handleSelect = event => {
    setSelectedReasons(event.target.value)
    reasons(event.target.value)
  }

  const handleValueChange = val => {
    setSelectedPartner(val)
  }

  const checkSelection = event => {
    let updatedReasons = updateReasons(event.target.value, selectedReasons)
    setSelectedReasons(updatedReasons)
    reasons(updatedReasons)
    setOptionsList(updateOptionsList(event.target.value))
  }


  const onResetFilter = () => {
    reset()
    resetFilter()
    setSelectedPartner([])
    setKey(date.getTime())
    setSelectedReasons([])
  }

  const handleChange = async (e, v) => {
    if (e) {
      let params = {
        code: v,
      }
      const bus = await loadBUList(params);
    }
  }

  const handleGroupChange = async (e, v) => {
    if (e) {
      let params = {
        name: v
      }
      loadGroupLists(params)
    }
  }

  const handleFirmChange = async (e, v) => {
    if (e) {
      let params = {
        legal_name: v
      }
      loadAuditLists(params)
    }
  }

  const handleInputChange = (e) => {

  }

  const getReasonDescriptionEval = (p) => {
    var yearExpr = "";
    var mySubString = p.substring(p.lastIndexOf("{") + 1, p.lastIndexOf("}"));
    if (mySubString != "") {
      var myArr = mySubString
        .replaceAll("current_year", selectedYear)
        .split(";");

      var startYearArr = myArr[0].split("-");
      var startYear = startYearArr[0] - startYearArr[1];
      yearExpr = startYear;

      if (myArr.length > 1) {
        var endYearArr = myArr[1].split("-");
        var endYear = endYearArr[0] - endYearArr[1];
        yearExpr = yearExpr + "-" + endYear;
      }
      p = p.replace("{", "");
      p = p.replace("}", "");
      p = p.replace(mySubString, yearExpr);
    }

    return p;
  };

  const handleYearChange = (e) => {
    setSelectedYear(e.target.value);
  };

  return (
    <div >
      <ListLoader loading={loading} >
        <Paper className={classes.paper}>
          <form className={classes.container} onSubmit={handleSubmit}>

            <Grid container sm={10}>
              <Grid item sm={3} >
                <InputLabel className={classes.inputLabel}>Partner</InputLabel>
                <Field
                  name='partner'
                  key={key}
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChangePartner}
                  items={partnerList}
                  comingValue={partner}
                  type_to_search={partner_type_to_search}
                />
              </Grid>
              <Grid item sm={2} >
                <InputLabel className={classes.inputLabel}>Year</InputLabel>
                <Field
                  name='budget_ref'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  className={classes.textField}
                  InputProps={{
                    inputProps: {
                      min: 2014
                    }
                  }}
                  onChange={handleYearChange}
                />
              </Grid>
              <Grid item sm={2} >
                <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
                <Field
                  name='business_unit'
                  margin='normal'
                  component={renderTypeAhead}
                  key={key}
                  className={classes.typeAhead}
                  onChange={handleChange}
                  handleInputChange={handleInputChange}
                  options={BUList ? BUList : []}
                  getOptionLabel={(option => option.code)}
                  multiple={false}
                />
              </Grid>
              <Grid item sm={2} >
                <InputLabel className={classes.inputLabel}>Agreement Number</InputLabel>
                <Field
                  name='number'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  className={classes.textField}
                />
              </Grid>
              <Grid item sm={3} >
                <InputLabel className={classes.inputLabel}>Reasons</InputLabel>
                <Field
                  name='reasons'
                  multiple={false}
                  style={{ width: "220px" }}
                  options={selectedReasons}
                  handleChange={handleSelect}
                  component={renderMultiSelectField}
                  className={classes.formControl}
                  margin='normal'
                >
                  {optionsList.map(option => (
                    <MenuItem key={option.id} value={option.code} style={{ fontSize: '12.5px' }} className={classes.menu}>
                      {getReasonDescriptionEval(option.description)}
                    </MenuItem>
                  ))}
                </Field>
              </Grid>
              {(userType === 'unhcr' || userType === "FO") ?
                <Grid item sm={4} >
                  <InputLabel className={classes.inputLabel}>Audit Firm</InputLabel>
                  <Field
                    name='firm'
                    margin='normal'
                    component={renderTypeAhead}
                    key={key}
                    className={classes.typeAhead}
                    onChange={handleFirmChange}
                    handleInputChange={handleInputChange}
                    options={firmList ? firmList : []}
                    getOptionLabel={(option => option.legal_name)}
                    multiple={false}
                  />
                </Grid>
                : null}
              <Grid item sm={4} >
                <InputLabel className={classes.inputLabel}>Worksplit Group</InputLabel>
                <Field
                  name='group'
                  margin='normal'
                  component={renderTypeAhead}
                  className={classes.typeAhead}
                  onChange={handleGroupChange}
                  key={key}
                  handleInputChange={handleInputChange}
                  options={groupList ? groupList : []}
                  getOptionLabel={(option => option.name)}
                  multiple={false}
                />
              </Grid>
              {isAdmin ?
                <Grid item sm={4} >
                  <Field
                    name="view_items"
                    component={renderRadioField}
                    checkSelection={checkSelection}
                    className={classes.radio}
                    margin='normal'
                  >
                    {radioOptions.map((option, index) => (
                      <FormControlLabel
                        key={index}
                        style={{ display: "inline-block" }}
                        value={option.value}
                        control={
                          <Radio
                            className={classes.radioButton}
                            style={{ display: "inline-block" }}
                            onClick={checkSelection}
                          />
                        }
                        label={option.label}
                        labelPlacement="end"
                      />
                    ))}
                  </Field>
                </Grid> : null}
            </Grid>
            <Grid item sm={2}>
              <div item className={classes.buttonGrid}>
                <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type="submit" />
                <CustomizedButton buttonType={buttonType.submit}
                  reset={true}
                  clicked={onResetFilter}
                  buttonText={filterButtons.reset}
                />
              </div>
            </Grid>
          </form>
        </Paper>
      </ListLoader>
    </div>
  )
}

AuditFilter.propTypes = {
  handleSubmit: PropTypes.func,
  loading: PropTypes.bool,
  reasons: PropTypes.func
}

const mapStateToProps = (state, ownProps) => {
  return {
    BUList: state.BUListReducer.bu,
    groupList: state.groupList.group,
    firmList: state.auditList.list,
    initialValues: {
      budget_ref: ownProps.year,
      view_items: messages.radio.all.value,
    }
  }
}

const mapDispatch = dispatch => ({
  loadBUList: (params) => dispatch(loadBUList(params)),
  reasons: (value) => dispatch(change('AssignmentFilterForm', 'reasons', value)),
  loadGroupLists: params => dispatch(loadGroupList(params)),
  loadAuditLists: (params) => dispatch(loadAuditList(params)),
  reset: () => dispatch(reset('AssignmentFilterForm')),
})

const AssignmentFilterForm = reduxForm({
  form: 'AssignmentFilterForm',
  enableReinitialize: true,
})(AuditFilter)

const connected = connect(
  mapStateToProps, mapDispatch
)(AssignmentFilterForm)

export default connected