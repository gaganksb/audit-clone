import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Field, reduxForm } from "redux-form";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Divider, TextField } from "@material-ui/core";
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import { Dialog, DialogContent, DialogTitle, DialogContentText } from "@material-ui/core";

import TypeAheadField from "../../forms/TypeAheadField";
import renderTypeAhead from '../../common/fields/commonTypeAhead';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    container: {
        display: "flex"
    },
    textField: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    buttonGrid: {
        display: "flex",
        justifyContent: "flex-end",
        width: "100%",
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: 18,
    },
}));


const messages = {
    cancel: 'Cancel',
    apply: 'Apply'
}

const validate = (values, props) => {
    const errors = {};
    const requiredFields = ['reviewers'];
    if (props.formData.reviewers && props.formData.reviewers.length !== 0) {
        return
    }

    else {
        if (!values['reviewers'] || values['reviewers'].length === 0) {
            errors['reviewers'] = 'Required'
        }
    }

    return errors;
};

function AssignmentDetailModal(props) {
    const {
        handleChange,
        open,
        handleClose,
        handleSubmit,
        handleInputChange,
        pristine,
        session,
        submitting,
        initialOption,
        userList,
        formData
    } = props;
    const classes = useStyles();


    return (
        <div className={classes.root}>
            <Dialog open={open} onClose={handleClose} fullWidth>
                <DialogTitle>Assign Selected Projects</DialogTitle>
                <Divider />
                <DialogContent>
                    <form className={classes.container} onSubmit={handleSubmit}>
                        <Grid
                            container
                            direction="column"
                        >
                            {session.user_type === "PARTNER" && <React.Fragment><InputLabel className={classes.inputLabel}>Partner Focal Person</InputLabel>
                                <Field
                                    name="focal"
                                    margin="normal"
                                    component={renderTypeAhead}
                                    onChange={handleChange}
                                    handleInputChange={handleInputChange}
                                    options={userList ? userList : []}
                                    getOptionLabel={(option => option.name ? option.name : option.fullname)}
                                    fullWidth
                                    className={classes.textField}
                                    defaultValue={initialOption.focal}
                                />
                                <InputLabel>Partner Alternate Focal Person</InputLabel>
                                <Field
                                    name="focal_alternate"
                                    margin="normal"
                                    component={renderTypeAhead}
                                    onChange={handleChange}
                                    handleInputChange={handleInputChange}
                                    options={userList ? userList : []}
                                    getOptionLabel={(option => option.name ? option.name : option.fullname)}
                                    fullWidth
                                    className={classes.textField}
                                    defaultValue={initialOption.alternate_focal}
                                /></React.Fragment>}
                            {session.user_type === "AUDITOR" &&
                                <div>
                                    <InputLabel>Auditor Reviewers</InputLabel>
                                    <Field
                                        name="reviewers"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        disableClearable={true}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={userList ? userList : []}
                                        getOptionLabel={(option => option.name ? option.name : option.fullname)}
                                        multiple={true}
                                        fullWidth
                                        className={classes.textField}
                                        style={{ width: "100%" }}
                                        defaultValue={initialOption.reviewers}
                                    />
                                </div>}
                            <div className={classes.buttonGrid}>
                                <CustomizedButton buttonType={buttonType.cancel}
                                    buttonText={messages.cancel}
                                    clicked={handleClose} />
                                <CustomizedButton buttonType={buttonType.submit}
                                    buttonText={messages.apply}
                                    type="submit"
                                />
                            </div>
                        </Grid>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}


const mapState = (state, ownProps) => {
    return {
        auditList: state.auditList.list,
        userList: state.usersList.users,
        initialValues: {
            ...ownProps.initialOption,
            focal_alternate: ownProps.initialOption.alternate_focal
        }
    }
}

const mapDispatch = (dispatch) => ({})


const AssignmentDetailForm = reduxForm({
    form: "PartnerAuditorAssignmentForm",
    validate,
    enableReinitialize: true
})(AssignmentDetailModal);


export default connect(mapState, mapDispatch)(AssignmentDetailForm);