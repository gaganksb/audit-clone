import React, { Fragment, useState, useEffect } from "react";
import { browserHistory as history, withRouter } from "react-router";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import R from "ramda";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { loadLatestCycle } from "../../../reducers/projects/projectsReducer/getLatestCycle";
import AssignmentTable from "./assignmentTable";
import { loadProjectAssignmentList } from "../../../reducers/audits/projectAssignment/assignmentList";
import { SubmissionError } from "redux-form";
import AuditFilter from "./assignmentFilter";
import { loadPartnerList } from '../../../reducers/partners/partnerList';
import AssignmentForm from './assignmentForm';
import BGTaskLoader from '../../common/bgTaskLoader';
import RemindAll from '../../assessment/remindAllDialog';
import { loadCheck_ReminderList } from '../../../reducers/audits/projectAssignment/remindAll';
import { sendReminder } from '../../../reducers/audits/projectAssignment/sendReminder';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { downloadURI } from '../../../helpers/others';
import { removeFirm } from '../../../reducers/audits/projectAssignment/removeFirm';
import Tooltip from '@material-ui/core/Tooltip'
import { clearUserList } from "../../../reducers/users/usersList";

const messages = {
  title: "Audit Project List",
  table: {
    headers: [
      {
        title: "",
        field: ""
      },
      {
        title: "",
        field: ""
      },
      {
        title: "",
        field: ""
      },
      {
        title: "Reason",
        field: "reason"
      },
      {
        title: "Business Unit",
        field: "business_unit"
      },
      {
        title: "Agreement Number",
        field: "number"
      },
      {
        title: "Country",
        field: "country__name"
      },
      {
        title: "Partner",
        field: "partner__legal_name"
      },
      {
        title: "Implementer Number",
        field: "partner__number"
      },
      {
        title: "Budget (in USD)",
        field: "budget"
      },
      {
        title: "Installments Paid",
        field: "installments_paid"
      },
      {
        title: "Worksplit Group",
        field: "group"
      },
      {
        title: "Implementer Type",
        field: "partner__implementer_type__implementer_type"
      },
      {
        title: "ProjectStart Date",
        field: "start_date"
      },
      {
        title: "ProjectEnd Date",
        field: "end_date"
      },
      {
        title: "Agreement Type",
        field: "agreement_type"
      },
      {
        title: "Agreement Date",
        field: "agreement_date"
      },
      {
        title: "Operation",
        field: "operation__code"
      }
    ]
  },
  error: "Error",
  success: "Reminder Sent !",
  reminderError: "Reminder failed !",
  edit: "Edit Selected",
  selectAll: "Select All",
  deselectAll: "Deselect All",
  assign_agreement_member: "assign_agreement_member_bgtask",
  remindAll: "Notify Auditors",
  export: "Export",
  unAssignText: "Please confirm that the selected Projects will be unassigned and the associated firms will be removed ",
  unassignTitle: "Unassign Projects",
  unassign: "Unassign Audit Firms",
  unassignSuccess: "Projects Unassigned !",
  dialogText: "Confirm that you want to remind all Auditor Admins to proceed with Audit Review by clicking on <b>YES</b>"
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  buttonGrid: {
    margin: '24px 0 24px 0',
    display: "flex",
    minWidth: 100,
    marginLeft: 0
  },
}));

let csvItem = [{}];
const currentYear = new Date().getFullYear()
const AssignmentView = props => {
  const {
    totalItems,
    projectAssignmentList,
    latestCycle,
    loadingLatestCycle,
    loadLatestCycle,
    loading,
    loadProject,
    query,
    auditList,
    users,
    session,
    partnerList,
    loadPartnerList,
    loadCheck_ReminderList,
    showReminder,
    postError,
    reset,
    successReducer,
    removeFirm,
    sendReminder,
    clearUserList
  } = props;
  const classes = useStyles();

  const [params, setParams] = useState({ page: 0 });
  const [year, setYear] = useState(currentYear);
  const [sort, setSort] = useState({ field: null, order: "desc" });
  const [shouldLatestCycleLoaded, setShouldLatestCycleLoaded] = useState(true);
  const [auditAssignment, setAuditAssignment] = useState({});
  const [userSuggestion, setUserSuggestion] = useState({});
  const [focal, setFocalValue] = useState({});
  const [alternateFocal, setAlternateFocal] = useState({});
  const [open, setOpen] = useState({ state: false, action: null });
  const [selectedAssignment, updateSelectedAssignment] = useState([]);
  const [allSelected, updateSelected] = useState(false);
  const [reviewer, setReviewer] = useState([]);
  const [partnerLists, setPartnerList] = useState([])
  const [partner, setSelectedPartner] = useState([])
  const [key, setKey] = useState('default')
  const [startMonitoring, shouldStartMonitoring] = useState(false)
  const [formData, setFormData] = useState({})

  var date = new Date();
  const isAdmin = () => {
    const { user_type, user_role } = session;
    if (user_type === "HQ") {
      return true
    } else if (user_role === "ADMIN") {
      return true
    } else if (user_type === "FO" && user_role === "APPROVER") {
      return true
    }
    else {
      return false
    }
  }


  function handleChangePartner(e, v) {
    if (v) {
      let params = {
        legal_name: v,
        finalized_in_year: query.budget_ref
      }
      loadPartnerList(params);
    }
  }

  useEffect(() => {
    setPartnerList(partnerList);
  }, [partnerList]);

  useEffect(() => {
    if (query.budget_ref) {
      setYear(query.budget_ref);
    } else if (latestCycle) {
      setYear(latestCycle.year);
    }
    else {
      setYear(currentYear);
    }
  }, [latestCycle, query]);

  const handleClick = (event, item) => {
    const selectedIndex = selectedAssignment.indexOf(item);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selectedAssignment, item);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selectedAssignment.slice(1));
    } else if (selectedIndex === selectedAssignment.length - 1) {
      newSelected = newSelected.concat(selectedAssignment.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selectedAssignment.slice(0, selectedIndex),
        selectedAssignment.slice(selectedIndex + 1)
      );
    }
    updateSelectedAssignment(newSelected);
  };

  function handleClose() {
    setOpen({ ...open, state: false });
    setAlternateFocal({});
    setFocalValue({});
    setReviewer([]);
    setFormData({});
  }

  function handleRemindAllClose() {
    setOpen({ ...open, state: false });
  }

  useEffect(() => {
    const { pathName } = props;
    updateSelectedAssignment([])
    if (query.budget_ref) {
      loadProject(query.budget_ref, query);
      loadCheck_ReminderList(query.budget_ref)
    }
    else if (latestCycle) {
      loadProject(latestCycle.year, query);
      loadCheck_ReminderList(latestCycle.year)
    }
    if ("budget_ref" in query === false) {
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          budget_ref: currentYear
        })
      })
    }
  }, [latestCycle, query]);

  useEffect(() => {
    setAuditAssignment(auditList);
  }, [auditList]);

  useEffect(() => {
    setUserSuggestion(users);
  }, [users]);

  const parseReasons = reasons => {
    let res = "";
    if (reasons) {
      for (let i = 0; i < reasons.length; i++) {
        res = res + reasons[i] + ",";
      }
      res = res.slice(0, -1);
    }
    return res;
  };
  const requestType = {
    HQ: "unhcr",
    PARTNER: "partner",
    AUDITOR: "auditing",
    FO: "fo"
  };

  let userType = window.localStorage.getItem("user_type");
  let userRole = window.localStorage.getItem("user_role");

  function handleSelectAll() {
    updateSelected(!allSelected);
  }

  const handleSearch = values => {
    const { pathName } = props;
    const { partner, budget_ref, reasons, firm, group, view_items, number, business_unit } = values;

    const view_item = requestType[userType].concat("__", view_items);

    setParams({ page: 0 });
    setYear(budget_ref);
    //const parsedReasons = parseReasons(reasons);
    if (reasons) {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          view_items: view_item,
          firm,
          number,
          business_unit,
          group,
          budget_ref,
          reason: reasons,
        }
      });
    } else {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          firm,
          group,
          budget_ref,
          number,
          business_unit,
          view_items: view_item
        }
      });
    }
  };

  const handleChangePage = (event, newPage) => {
    setParams(
      R.merge(params, {
        page: newPage
      })
    );
  };
  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ["asc", "desc"])[0]
    });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      })
    });
  }

  function resetFilter() {
    const { pathName } = props;

    // setKey(date.getTime())
    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        budget_ref: currentYear
      }
    });
  }

  function loadCSVs() {
    let params = {};
    let url = '';
    url = `/api/projects/export-audit-assignment/${year}/`;
    downloadURI(url, 'Audit Project Report')
  }

  function sendReminders() {
    sendReminder(year).then(() => {
      successReducer(messages.success)
      handleRemindAllClose()
    }).catch((error) => { postError(error, error.response && error.response.data && messages.reminderError) })
  }

  const unassignFirm = () => {
    let body = {}

    if (allSelected) {
      const url = `${window.location.origin}${window.location.pathname}/${year}/${window.location.search}`
      body['filter_url'] = url
    }

    else {
      const agreements = selectedAssignment.map(item => item.id)
      body['agreements'] = agreements
    }

    removeFirm(year, body).then(() => {
      successReducer(messages.unassignSuccess);
      handleRemindAllClose();
      loadProject(year, query);
      loadCheck_ReminderList(year);
      updateSelected(false);
      updateSelectedAssignment([]);
    }).catch((error) => {
      postError(error, error.response && error.response.data.detail || messages.error)
    })
  }

  return (
    <Fragment>
      <BGTaskLoader
        pendingTaskName={messages.assign_agreement_member}
        shouldStartMonitoring={shouldStartMonitoring}
      />
      {startMonitoring === false ? <div className={classes.root}>
        <AuditFilter
          onSubmit={handleSearch}
          loading={loadingLatestCycle}
          year={year}
          resetFilter={resetFilter}
          userType={requestType[userType]}
          isAdmin={isAdmin}
          key={key}
          partner={partner}
          partnerList={partnerLists}
          setSelectedPartner={setSelectedPartner}
          handleChangePartner={handleChangePartner}
        />
        {(projectAssignmentList && projectAssignmentList.length > 0) && <Grid className={classes.buttonGrid}>
          {isAdmin() ? <span style={{ marginLeft: -16 }}>
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={!allSelected ? messages.selectAll : messages.deselectAll}
              clicked={handleSelectAll} />
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={messages.edit}
              clicked={() => { setOpen({ state: true, action: 'AllSelected' }); setFormData({}); clearUserList({}); }}
              disabled={(allSelected === false && selectedAssignment.length === 0) ? true : false} />
          </span> : null}
          {
            (userType === "HQ" || userType === "AUDITOR") &&
            <div>
              {userType !== "AUDITOR" && <CustomizedButton buttonType={buttonType.submit}
                buttonText={messages.unassign}
                clicked={() => { setOpen({ state: true, action: 'unAssign' }) }}
                disabled={(allSelected === false && selectedAssignment.length === 0) ? true : false}
              />}
              {(userType === "HQ" || (userType === 'AUDITOR' && userRole === "ADMIN")) && <CustomizedButton buttonType={buttonType.submit}
                buttonText={messages.export}
                clicked={(loadCSVs)}
              />}
            </div>
          }
          {
            (userType === "HQ" || (userType === 'AUDITOR' && userRole === "ADMIN")) &&
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={messages.remindAll}
              clicked={() => { setOpen({ state: true, action: 'RemindAll' }) }}
              disabled={(showReminder && showReminder.audit_assignment_completed) ? false : true}
            />
          }
        </Grid>}

        <AssignmentTable
          isAdmin={isAdmin}
          messages={messages}
          sort={sort}
          items={projectAssignmentList}
          loading={loading}
          totalItems={totalItems}
          page={params.page}
          handleChangePage={handleChangePage}
          handleSort={handleSort}
          year={year}
          selectedAssignment={selectedAssignment}
          allSelected={allSelected}
          handleClick={handleClick}
          userType={requestType[userType]}
        />
        <RemindAll
          open={open.state && open.action === 'RemindAll'}
          sendReminder={sendReminders}
          handleClose={handleRemindAllClose}
          dialogText={messages.dialogText}
          dialogTitle={messages.remindAll}
        />
        <RemindAll
          open={open.state && open.action === 'unAssign'}
          sendReminder={unassignFirm}
          handleClose={handleRemindAllClose}
          dialogText={messages.unAssignText}
          dialogTitle={messages.unassignTitle}
        />
        <AssignmentForm
          formData={formData}
          setFormData={setFormData}
          updateSelectedAssignment={updateSelectedAssignment}
          handleSelectAll={handleSelectAll}
          selectedAssignment={selectedAssignment}
          handleClose={handleClose}
          allSelected={allSelected}
          open={open.state && open.action === 'AllSelected'}
        />
      </div> : null}
    </Fragment>
  );
};

const mapStateToProps = (state, ownProps) => ({
  loadingLatestCycle: state.latestCycle.loading,
  latestCycle: state.latestCycle.details,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  projectAssignmentList: state.projectAssignment.assignment,
  totalItems: state.projectAssignment.totalCount,
  auditList: state.auditList.list,
  loading: state.projectAssignment.loading,
  users: state.usersList.users,
  partnerList: state.partnerList.list,
  session: state.session,
  showReminder: state.checkReminder.check_reminder
});

const mapDispatch = dispatch => ({
  loadLatestCycle: params => dispatch(loadLatestCycle(params)),
  loadProject: (year, params) => dispatch(loadProjectAssignmentList(year, params)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadCheck_ReminderList: (year) => dispatch(loadCheck_ReminderList(year)),
  sendReminder: (year) => dispatch(sendReminder(year)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'forward', message)),
  removeFirm: (year, body) => dispatch(removeFirm(year, body)),
  clearUserList: params => dispatch(clearUserList(params)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatch
  )(AssignmentView)
);