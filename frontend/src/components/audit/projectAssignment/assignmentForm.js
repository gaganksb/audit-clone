import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { SubmissionError } from "redux-form";
import HQAssignmentModal from './HQAssignmentModal';
import PartnerAuditAssignmentModal from './PartnerAuditorAssignmentModal';
import { loadUsersList } from "../../../reducers/users/usersList";
import { loadAuditList } from "../../../reducers/audits/auditList";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { loadProjectAssignmentList } from "../../../reducers/audits/projectAssignment/assignmentList";
import { editProjectAssignment } from "../../../reducers/audits/projectAssignment/editAssigmentList";
import { LATEST_CYCLE_LOAD_ENDED } from '../../../reducers/projects/projectsReducer/getLatestCycle';
import { loadBgJob } from '../../../reducers/bgTaskReducer';
import R from 'ramda';
import { loadCheck_ReminderList } from '../../../reducers/audits/projectAssignment/remindAll';

const requestType = {
    HQ: "unhcr",
    PARTNER: "partner",
    AUDITOR: "auditing",
    FO: 'fo'
};

const messages = {
    error: "Error",
    success: "Update Successful !",
    assign_agreement_member: "assign_agreement_member_bgtask"
}

const AssignmentForm = (props) => {
    const {
        open,
        handleClose,
        loadAuditList,
        loadUsersList,
        users,
        session,
        allSelected,
        query,
        postError,
        loadProject,
        successReducer,
        selectedAssignment,
        loadBgJob,
        loadCheck_ReminderList,
        formData,
        setFormData,
        handleSelectAll,
    } = props;

    const [initialOption, setInitialOption] = useState({})

    useEffect(() => {
        if (selectedAssignment.length === 1) {
            const members = selectedAssignment[0].agreement_member
            const firm = selectedAssignment[0].audit_firm

            if (session.user_type === "HQ" || session.user_type === "FO") {
                setInitialOption({ ...members.unhcr, firm: firm })
            } else if (session.user_type === "PARTNER") {
                setInitialOption({ ...members.partner, firm: firm })
            } else if (session.user_type === "AUDITOR") {
                setInitialOption({ ...members.auditing, firm: firm })
            }
        } else {
            setInitialOption({})
        }
    }, [selectedAssignment])

    const handleChange = async (e, v) => {
        if (e.target.name === "firm") {
            let params = { legal_name: v };
            const loadedAudit = await loadAuditList(params);
        } else {
            if (session.user_type === "HQ" || session.user_type === "FO") {
                let params = { fullname: v, has_account: 'field' };
                const loadedUser = await loadUsersList(params)
            } else if (session.user_type === "PARTNER" || session.user_type === "AUDITOR") {
                let params = { fullname: v, my_own: true };
                const loadedUser = await loadUsersList(params)
            }
        }
    }

    const handleSubmit = (value) => {
        const { updateSelectedAssignment, editProjectAssignment, AssignmentFilterForm } = props;
        const year = AssignmentFilterForm.values ? AssignmentFilterForm.values.budget_ref : query.budget_ref
        const agreements = selectedAssignment.map(item => item.id)
        const _body = R.mergeDeepRight(value, formData)

        let body = {
            agreements: agreements,
            request_type: requestType[session.user_type]
        };

        if (session.user_type === "AUDITOR") {
            if (_body && _body.reviewers && _body.reviewers.length > 0) {
                body["reviewers"] = _body.reviewers.map(item => item.id)
            } else if (initialOption && initialOption.reviewers && initialOption.reviewers.length > 0) {
                body["reviewers"] = initialOption.reviewers.map(item => item.id)
            } else {
                body["reviewers"] = []
                return;
            }
        }
        else if (session.user_type === "FO") {
            body["focal"] = _body.focal.id ? _body.focal.id : initialOption.focal.id,
                body["focal_alternate"] = _body.focal_alternate.id ? _body.focal_alternate.id : initialOption.focal_alternate.id
        }

        if (session.user_type === "HQ" || session.user_type === "FO") {
            body["firm"] = _body.firm.id ? _body.firm.id : initialOption.firm.id
        }
        if (allSelected) {
            const url = `${window.location.origin}${window.location.pathname}/${year}/${window.location.search}`
            body['filter_url'] = url
        }
        return editProjectAssignment(year, body).then(() => {
            handleClose();
            if (allSelected) {
                handleSelectAll();
            }
            loadBgJob({ "pending_task": messages.assign_agreement_member })
            updateSelectedAssignment([]);
            loadProject(year, query);
            loadCheck_ReminderList(query.budget_ref)
            successReducer(messages.success)
            setFormData({})
        }).catch(error => {
            postError(error, messages.error);
            setFormData({});
            throw new SubmissionError({
                ...error.response,
                _error: messages.error
            });
        });
    }

    const handleInputChange = (e, v, label) => {

        if (!v) return;
        if (label === "reviewers") {
            setFormData({
                ...formData,
                reviewers: v
            })
        } else {
            setFormData({
                ...formData,
                [label]: v
            })
        }
    }

    return (
        <React.Fragment>
            {session && (session.user_type === "HQ" || session.user_type === "FO") ? (
                <HQAssignmentModal
                    open={open}
                    handleClose={handleClose}
                    handleChange={handleChange}
                    onSubmit={handleSubmit}
                    handleInputChange={handleInputChange}
                    initialOption={initialOption}
                    userType={session.user_type}
                />
            ) : (
                <PartnerAuditAssignmentModal
                    open={open}
                    handleClose={handleClose}
                    handleChange={handleChange}
                    onSubmit={handleSubmit}
                    handleInputChange={handleInputChange}
                    session={session}
                    formData={formData}
                    initialOption={initialOption}
                />
            )
            }
        </React.Fragment>
    )
}

const mapState = (state, ownProps) => ({
    auditList: state.auditList.list,
    loading: state.projectAssignment.loading,
    users: state.usersList.users,
    session: state.session,
    query: ownProps.location.query,
    AssignmentFilterForm: state.form.AssignmentFilterForm
});

const mapDispatch = dispatch => ({
    loadProject: (year, params) => dispatch(loadProjectAssignmentList(year, params)),
    loadAuditList: params => dispatch(loadAuditList(params)),
    editProjectAssignment: (year, body) => dispatch(editProjectAssignment(year, body)),
    loadUsersList: params => dispatch(loadUsersList(params)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, "AssignmentEdit", message)),
    loadBgJob: (params) => dispatch(loadBgJob(params)),
    loadCheck_ReminderList: (year) => dispatch(loadCheck_ReminderList(year)),
});

const connectedForm = connect(mapState, mapDispatch)(AssignmentForm)
export default withRouter(connectedForm)