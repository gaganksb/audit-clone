import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Grid from '@material-ui/core/Grid';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import Tooltip from '@material-ui/core/Tooltip';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import TablePaginationActions from '../../common/tableActions';
import BUTable from './businessUnitTable';
import { loadGroupList } from '../../../reducers/audits/groups/groupList';
import { editAuditGroup } from '../../../reducers/audits/groups/editAuditGroup';
import { addNewGroup } from '../../../reducers/audits/groups/addAuditGroup';
import { deleteGroup } from '../../../reducers/audits/groups/deleteAuditGroup';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import EditDialog from './groupModal';
import AuditGroupsFilter from './auditGroupFilter';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import DeleteDialog from './deleteGroupModal';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import R from 'ramda';


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
    '& svg': {
      cursor: 'pointer'
    }
  },
  noDataRow: {
    margin: 16,
    width: '100%',
    textAlign: 'center'
  },
  heading: {
      padding: theme.spacing(3),
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
      color: '#344563',
      font: 'bold 16px/21px Roboto',
      letterSpacing: 0,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  tableHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    borderBottom: 'solid 1px #e0e0e0',
    '& > span': {
      fontSize: 18,
      fontWeight: 600
    },
    '& > button': {
      margin: 0
    }
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#344563'
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(3)
  },
}))

const DELETE = 'DELETE'
const EDIT = 'EDIT'
const ADD = 'ADD'
const BUERROR = "Please check the business units"

const message = {
  success : {
    edit: 'Group Updated Successfully !',
    add: 'Group Added Successfully !',
    delete: 'Group Deleted Successfully !'
  }
}

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    title,
    handleSearch,
    BUList,
    loadBUList,
    handleChangePage,
    editAuditGroup,
    groupList,
    addNewGroup,
    setSelectedFilterBU,
    resetFilter,
    messages,
    key,
    postError,
    successReducer,
    deleteAuditGroup,
  } = props;

  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }

  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }

  const classes = useStyles();

  const [open, setOpen] = React.useState({ status: false, type: null })
  const [expanded, setExpanded] = React.useState(initialExpanded())
  const [curItem, setCurItem] = React.useState({})
  const [selectedBU, setSelectedBU] = React.useState([])
  const [item, setItem] = React.useState([]);

  useEffect(() => {
    setExpanded(initialExpanded())
  }, [items]);

  useEffect(() => {
    setItem(BUList);
  }, [BUList]);

  function handleDeleteGroup() {
    const { query, loadGroupList } = props
    return deleteAuditGroup(curItem.id).then(() => {
      handleClose()
      loadGroupList(query)
      successReducer(message.success.delete)
    }).catch((error) => {
      postError(error, messages.error)
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error
      })
    })
  }

  const handleInputChange = (e,v) => {
    setSelectedBU(v)
  }

  function handleGroup(values) {
    const { query, loadGroupList } = props;

    let business_units = [];
    selectedBU&&selectedBU.forEach(item=>{
      if(business_units.includes(item.id)){
        console.log("")
      }else{
        business_units.push(item.id)
      }
    })

    const { name } = values;
    let body = { name, business_units };
    
    if(business_units.length === 0){
      const error = new SubmissionError({ _error: BUERROR });
      postError(error, BUERROR);
      throw error;
    }

    if (open.type === EDIT) {
      return editAuditGroup(curItem.id, body).then(() => {
        handleClose()
        loadGroupList(query)
        successReducer(message.success.edit)
      }).catch((error) => {
        postError(error, messages.error)
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error
        })
      })
    } 
    if (open.type === ADD) {
      return addNewGroup(body).then(() => {
        handleClose()
        loadGroupList(query)
        successReducer(message.success.add)
      }).catch((error) => {
        postError(error, messages.error)
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error
        })
      })
    }
  }


  function handleClickOpen(item, action) {
    setCurItem(item)
    if(action===EDIT){
      setSelectedBU(item.business_units)
    }
    setOpen({ status: true, type: action })
  }

  function handleClose() {
    setOpen({ status: false, type: null })
  }

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }

  function handleChange(e, v) {
    if (e) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }

  const button = {
    add : "Add group"
  }

  return (
    <React.Fragment>
      {/* <AuditGroupsFilter
        onSubmit={handleSearch}
        key={key} 
        resetFilter={resetFilter}
        setSelectedBU={setSelectedFilterBU}
        handleChange={handleChange}
        item={item}
      /> */}
      <Grid className={classes.buttonGrid}> 
      <CustomizedButton buttonType={buttonType.submit} 
                buttonText={button.add} 
                clicked={() => handleClickOpen(null, ADD)} />
      </Grid>
      <Paper className={classes.root}>
        <ListLoader loading={loading}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>
              <span>{title}</span>
            </div>
            <Table className={classes.table}>
              { (items.length == 0 && !loading) ? <div className= {classes.noDataRow}>No data for the selected criteria</div> 
              : <TableBody>
                {items.map((item, key) => (
                  <BUTable
                    item={item}
                    key={key}
                    expanded={expanded[key]}
                    handleExpand={() => handleExpand(key)}
                    handleEdit={() => handleClickOpen(item, EDIT)}
                    handleDelete={() => handleClickOpen(item, DELETE)}
                  />
                ))}
              </TableBody> }
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[rowsPerPage]}
                    colSpan={0}
                    count={totalItems}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
            <EditDialog
              handleClose={handleClose}
              type={open.type}
              open={open.status && (open.type === ADD || open.type === EDIT)}
              auditGroup={curItem}
              onSubmit={handleGroup}
              handleChange={handleChange}
              item={item}
              handleInputChange={handleInputChange}
              setSelectedBU={setSelectedBU}
            />
            <DeleteDialog
              handleClose={handleClose}
              open={open.status && open.type === DELETE}
              handleDeleteGroup={handleDeleteGroup}
              auditGroup={curItem}
            />
          </div>
        </ListLoader>
      </Paper>
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  groupList: state.groupList.group,
  query: ownProps.location.query,
  BUList: state.BUListReducer.bu,
})

const mapDispatchToProps = (dispatch) => ({
  loadGroupList: params => dispatch(loadGroupList(params)),
  editAuditGroup: (id, body) => dispatch(editAuditGroup(id, body)),
  addNewGroup: (body) => dispatch(addNewGroup(body)),
  deleteAuditGroup: (id) => dispatch(deleteGroup(id)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editAuditGroup', message))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)