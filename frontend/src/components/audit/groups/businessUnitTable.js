import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';
import Fab from '@material-ui/core/Fab';
import { withRouter } from 'react-router';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';

const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    icon: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    detailHeader: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 500,     
        marginLeft: 64
    },
    details: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 300,        
        marginLeft: 64,
    },
    detailRow: {        
        width: '500%',
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
    }
}));

const messages = {
    detailHeaders: 'Business Unit'
}

function BUDetails(props) {
    const {
        expanded,
        handleExpand,
        item,
        handleEdit,
        handleDelete
    } = props;

    const classes = useStyles();

    return (
        <React.Fragment>
            <TableRow key={item.name}>
                <TableCell style={{width: '20%'}} className={classes.tableCell} component="th" scope="row">
                    {(expanded) ?
                        <ExpandLess className={classes.icon} onClick={handleExpand()} /> :
                        <ExpandMore className={classes.icon} onClick={handleExpand()} />
                    }
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {item.name}
                </TableCell>
                <TableCell align="right">
                    <Tooltip title={"Delete"}>
                    <Fab size='small' className={classes.fab} onClick={handleDelete} >
                      <DeleteIcon />
                    </Fab>
                    </Tooltip>
                    <Tooltip title={"Edit"}>
                        <Fab color="primary" size='small' className={classes.fab} onClick={handleEdit} style={{ backgroundColor: '#0072BC' }}>
                            <EditIcon />
                        </Fab>
                    </Tooltip>
                  </TableCell>
            </TableRow>
            {(expanded) ?
                <TableRow>
                    <td>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders} 
                            </div>
                            {item.business_units.map((bu, index)=> (<div key={index} className={classes.details}>{bu.code}</div>))}
                        </div>
                    </td>
                </TableRow> : null}
        </React.Fragment>
    );
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname,
    auditList: state.auditList.list
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(BUDetails);

export default withRouter(connected);
