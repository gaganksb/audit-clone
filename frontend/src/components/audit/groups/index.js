import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadGroupList } from '../../../reducers/audits/groups/groupList';
import WorksplitGroupsTable from './groupTable';
import R from 'ramda';
import AuditGroupsFilter from './auditGroupFilter';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';


  const messages = {
    title: 'Auditor Selection',
    tableHeader: 'Worksplit Groups',
    error: 'Could not complete the action'
  }


const AuditGroup = props => {
  const { groupList, loadGroupList, loading, totalCount, query, reset, BUList, loadBUList } = props

  const [params, setParams] = useState({page: 0}); 
  const [key, setKey] = useState('default');
  const [selectedBU, setSelectedFilterBU] = useState([])
  const [item, setItem] = React.useState([]);

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field, 
    order: 'desc'
  })

  useEffect(() => {
    loadGroupList(query);
  }, [query]);

  
  useEffect(() => {
    setItem(BUList);
  }, [BUList]);

  function goTo(item) {
    history.push(item);
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleSort(field, order) {
    const {pathName, query} = props;
   
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
  
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order,
      }),
    })
  }

  function resetFilter() {
    const {pathName} = props;
    setKey(date.getTime())
    setParams({page: 0});
    setSelectedFilterBU([]);
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }
  var date = new Date();
  function handleSearch(values) {
    const {pathName, query} = props;
    const business_units = selectedBU.map(bu => bu.id).toString()
    const {name} = values;

    setParams({page: 0});
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        name,
        business_units
      }),
    })
  }
  
  function handleChange(e, v) {
    if (e) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }


  return (
    <div>
      <AuditGroupsFilter
        onSubmit={handleSearch}
        key={key} 
        setSelectedBU={bu => setSelectedFilterBU(bu)}
        handleChange={handleChange}
        resetFilter={resetFilter}
        item={item}
        handleSearch={handleSearch}
      />
      <WorksplitGroupsTable
        messages={messages}
        title={messages.tableHeader}
        sort={sort}
        items={groupList}
        loading={loading}
        totalItems={totalCount}
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  groupList: state.groupList.group,
  totalCount: state.groupList.totalCount,
  loading: state.groupList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  groupList: state.groupList.group,
  BUList: state.BUListReducer.bu,
});

const mapDispatch = dispatch => ({
    loadGroupList : params => dispatch(loadGroupList(params)),
    loadBUList: (params) => dispatch(loadBUList(params)),
});

const connectedGroups = connect(mapStateToProps, mapDispatch)(AuditGroup);
export default withRouter(connectedGroups);