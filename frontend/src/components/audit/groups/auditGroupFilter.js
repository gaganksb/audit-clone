import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import { Field, reduxForm, reset } from 'redux-form'
import {buttonType, filterButtons} from '../../../helpers/constants';
import CustomizedButton from '../../common/customizedButton';
import CustomAutoCompleteField from '../../common/fields/autocompleteField';
import InputLabel from '@material-ui/core/InputLabel';
import { connect } from 'react-redux'
import {loadGroupListDropdown} from '../../../reducers/audits/groups/groupList'
import renderTypeAhead from '../../common/fields/commonTypeAhead';
import { renderTextField } from '../../common/renderFields';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  }, 
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
    },  
    typeAhead: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
      marginTop: 2,
      width: '90%',
    },
autoCompleteField: {
  marginTop: 2,
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(1),
  width: '85%',
  display: "inline-block"
},
  container: {
    display: 'flex',
    paddingBottom: 8
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '92%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  fab: {
    margin: theme.spacing(1),
  },  
  formControl: {
    minWidth: 100,
    padding: 16,
    width: '85%',
    top: 17
  },
  formControlHeading:{
    paddingBottom : 10,
    fontSize: 14,
    left: 42,
    top: 10,
    position: 'relative'
  }
}))

function AuditGroupFilter(props) {
  const classes = useStyles()
  const { handleSubmit, reset, resetFilter, handleChange, item, setSelectedBU, loadGroupLists, groupList, key } = props

  const handleValueChange = val => {
    setSelectedBU(val)
}

  
const handleGroupChange = async (e,v) => {
  if(e) {
    let params = {
      name: v
    }
    loadGroupLists(params)
  }
}

const handleInputChange = (e) =>{

}

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={5} >
          <InputLabel className={classes.inputLabel}>Name</InputLabel>
            <Field
              name='name'
              margin='normal'
              component={renderTypeAhead}              
              key={key}
              className={classes.typeAhead}
              onChange={handleGroupChange}
              handleInputChange={handleInputChange}
              options={groupList ? groupList: []}
              getOptionLabel={(option => option.name)}
              multiple={false}
            />
          </Grid>
          <Grid item sm={5} >
            <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
            <Field
                  name='business_units'           
                  key={key}
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChange}
                  items={item}
                  multiple={true}
                />
          </Grid>
          <Grid item sm={2} className={classes.buttonGrid}>
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={filterButtons.search}
              type="submit" />
            <CustomizedButton buttonType={buttonType.submit}
              reset={true}
              clicked={() => { reset(); resetFilter(); }}
              buttonText={filterButtons.reset}
            />
          </Grid>
        </form>
      </Paper>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    groupList: state.groupList.groupList,
    
  }
}

const mapDispatch = dispatch => ({
  loadGroupLists: (params) => dispatch(loadGroupListDropdown(params))
})

const groupFilterForm = reduxForm({
  form: 'auditGroupFilter',
  enableReinitialize: true
})(AuditGroupFilter)

const connected = connect(
  mapStateToProps, mapDispatch
)(groupFilterForm)

export default connected