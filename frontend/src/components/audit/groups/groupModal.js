import React from 'react'
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import { makeStyles } from '@material-ui/core/styles'
import { Field, reduxForm } from 'redux-form'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import InputLabel from '@material-ui/core/InputLabel'
import { renderTextField } from '../../common/renderFields';
import commonTypeAhead from '../../common/fields/commonTypeAhead';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';

var errors = {}
const validate = (values, props) => {
  errors = {}
  const requiredFields = [
    'name',
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '100%',
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    },
  autoCompleteField: {
    marginTop: 2,
    marginRight: theme.spacing(1),
    width: '100%',
    display: "inline-block"
  },
  button: {
    marginTop: theme.spacing(1),
  },  
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
    marginTop: theme.spacing(3)
  },
}))

const EDIT = 'EDIT'
const ADD = 'ADD'

const messages = {
  submit: 'Submit',
  cancel: 'Cancel'
}

function AuditGroupModal(props) {
  const {
    open, handleClose, handleSubmit, type, auditGroup ,
    item, setSelectedBU, reset, initialValues, handleInputChange, BUList, loadBUList
  } = props
  const classes = useStyles()

  const handleChange = (e, v) => {
    loadBUList({code: v})
  }
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={() => { handleClose(); reset('editGroup'); }}
        fullWidth
      >
        <DialogTitle >{type === EDIT ? "Edit Worksplit Group" : "Create Worksplit Group"}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit} >
            <Grid
              container
              direction="column"
              justify="flex-start"
              alignItems="flex-start"
            >
              <InputLabel className={classes.inputLabel}>Name</InputLabel>
              <Field
                name='name'
                margin='normal'
                component={renderTextField}
                className={classes.textField}
              />
              <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
              <Field
                name="business_units"
                component={commonTypeAhead}
                onChange={handleChange}
                handleInputChange={handleInputChange}
                options={BUList}
                getOptionLabel={(option => option.code)}
                multiple={true}
                fullWidth
                defaultValue={initialValues&&initialValues.business_units ? initialValues.business_units : []}
                className={classes.autoCompleteField}
              />
              <Grid className={classes.buttonGrid}> 
                <CustomizedButton clicked={handleClose} 
                buttonText={messages.cancel} 
                buttonType={buttonType.cancel} />  
                <CustomizedButton buttonType={buttonType.submit} 
                buttonText={messages.submit} 
                type = 'submit' />
              </Grid>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

const auditGroupModal = reduxForm({
  form: 'editGroup',
  validate,
  enableReinitialize: true,
})(AuditGroupModal)

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.auditGroup
  return {
    initialValues,
    BUList: state.BUListReducer.bu
  }
}

const mapDispatchToProps = dispatch => ({
  loadBUList: (params) => dispatch(loadBUList(params)),
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(auditGroupModal)

export default withRouter(connected)
