import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid'
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(2),
  },  
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
    // marginLeft: theme.spacing(1),
    // marginRight: theme.spacing(1),
    marginTop: theme.spacing(3)
  },
}));

const messages ={
  yes: 'Yes',
  no: 'No'
}

function AlertDialog(props) {
  const {open, handleClose, handleDeleteGroup, auditGroup } = props;

  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{"Delete Worksplit Group"}</DialogTitle>
        <Divider />
        {(auditGroup && auditGroup.name) ? <DialogContent className={classes.dialogContentText} >
            Please notice by clicking on SUBMIT the selected Worksplit Group named {auditGroup.name} will be deleted
        
          <Grid className={classes.buttonGrid}> 
          <CustomizedButton buttonType={buttonType.cancel} 
                buttonText={messages.no} 
                clicked={handleClose} />
          <CustomizedButton buttonType={buttonType.submit} 
                buttonText={messages.yes} 
                clicked={handleDeleteGroup} />
          </Grid>
          </DialogContent> : null}
      </Dialog>
    </div>
  );
}

export default AlertDialog;
