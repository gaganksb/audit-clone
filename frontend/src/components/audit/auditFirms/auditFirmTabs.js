import React, { useState } from 'react'
import { browserHistory as history, withRouter } from 'react-router'
import HeaderTabs from '../../common/headerTabs';
import Title from '../../common/customTitle';

const messages = {
  title: 'Audit',
  tabTitle: ['FIRMS', 'GROUPS', 'PROJECTS ASSIGNMENTS'],
  error: 'Error'
}


const tabs = {
  items: [
    { index: 0, label: 'FIRMS', path: '/auditFirms' },
    { index: 1, label: 'GROUPS', path: '/auditFirms/groups' },
    { index: 2, label: 'PROJECTS ASSIGNMENTS', path: '/auditFirms/assignments' }
  ]
}

const auditFirmTabs = ({ children }) => {
  var selectedTab;
  
  switch(window.location.pathname.split('/').slice(-1)[0]) {
    case 'firms' : 
          selectedTab = 0;
          break;
    case 'groups' : 
          selectedTab = 1;
          break;
    case 'assignments': 
          selectedTab = 2;
          break;
  }

  const [value, setValue] = useState(selectedTab)

  const handleChange = (event, value) => {
    setValue(value)
  }

  const goTo = () => item => {
    history.push(item)
  }

  return (
    <React.Fragment>
       <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
          <Title show={false} title={messages.title} />
          <HeaderTabs tabs={tabs} handleChange={handleChange} goTo={goTo()} value={value} />
        { children }
      </div>
    </React.Fragment>
  )
}

export default withRouter(auditFirmTabs)
