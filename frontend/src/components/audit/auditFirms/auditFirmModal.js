import React from 'react'
import CustomizedButton from '../../common/customizedButton'
import {buttonType} from '../../../helpers/constants'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import { makeStyles } from '@material-ui/core/styles'
import { renderTextField } from '../../common/renderFields';
import { Field, reduxForm } from 'redux-form'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import InputLabel from '@material-ui/core/InputLabel'
import TypeAheadField from '../../forms/TypeAheadField'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import FormHelperText from '@material-ui/core/FormHelperText'
import commonTypeAhead from '../../common/fields/commonTypeAhead';
import { loadCountries } from '../../../reducers/countries';

var errors = {}
const validate = (values) => {
  errors = {}
  const requiredFields = [
    'legal_name',
    'address_1',
    'city',
    'country'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: theme.spacing(3),
    marginBottom: 4
    },
    textField: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  button: {
    marginTop: theme.spacing(1),
  },
  autoCompleteField: {
    marginTop: 2,
    marginRight: theme.spacing(1),
    width: '100%',
    display: "inline-block"
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
    marginTop: theme.spacing(3)
  },
}))

const EDIT = 'EDIT'
const ADD = 'ADD'
const messages = {
  submit: 'Submit',
  cancel: 'Cancel'
}

function AuditFirmModal(props) {
  const {
    open, getCountries, handleClose, reset, handleSubmit, type, auditFirm,
    countries, handleChange, handleInputChange, initialValues
  } = props;
  const classes = useStyles()

  const handleCountryChange = (e, v) => {
    getCountries({name: v})
  }

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={()=>{handleClose(); reset('auditFirmModal');}}
        fullWidth
      >
        <DialogTitle >{type === EDIT ? "Edit Audit Firm" : "Create Audit Firm"}</DialogTitle>
        <Divider />
        <DialogContent>
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
              <InputLabel className={classes.inputLabel}>Legal Name</InputLabel>
              <Field
                name='legal_name'
                className={classes.textField}
                margin='normal'
                component={renderTextField}
              />
            <InputLabel className={classes.inputLabel}>Address 1</InputLabel>
            <Field
              name='address_1'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>Address 2</InputLabel>
            <Field
              name='address_2'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>City</InputLabel>
            <Field
              name='city'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>Focal Contact Number</InputLabel>
            <Field
              name='telephone'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>Focal Person Fullname</InputLabel>
            <Field
              name='fullname'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>Focal Person Email</InputLabel>
            <Field
              name='email'
              className={classes.textField}
              margin='normal'
              component={renderTextField}
            />
            <InputLabel className={classes.inputLabel}>Country</InputLabel>
                <Field
                    name="country"
                    component={commonTypeAhead}
                    onChange={handleCountryChange}
                    handleInputChange={handleInputChange}
                    options={countries.results ? countries.results : initialValues&&initialValues.country ? [initialValues.country]: []}
                    getOptionLabel={(option => option.name)}
                    multiple={false}
                    fullWidth
                    defaultValue={initialValues ? initialValues.country : null}
                    className={classes.autoCompleteField}
                />
                <Grid className={classes.buttonGrid}>
                <CustomizedButton clicked={handleClose} 
                buttonText={messages.cancel} 
                buttonType={buttonType.cancel} />   
                <CustomizedButton buttonType={buttonType.submit} 
                 buttonText={messages.submit}
                 type="submit" />
                 </Grid>
              </Grid>
            </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

const auditFirmModal = reduxForm({
  form: 'auditFirmModal',
  validate,
  enableReinitialize: true,
})(AuditFirmModal)

const mapStateToProps = (state, ownProps) => {
  const initialValues = ownProps.auditFirm
  return {
    initialValues,
    countries: state.countries
  }
}

const mapDispatchToProps = dispatch => ({
  getCountries: (params) => dispatch(loadCountries(params)),
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(auditFirmModal)

export default withRouter(connected)
