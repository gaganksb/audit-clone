import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import { Field, reduxForm } from 'redux-form'
import {buttonType, filterButtons} from '../../../helpers/constants';
import CustomizedButton from '../../common/customizedButton';
import { renderTextField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import { loadAuditListDropdown } from '../../../reducers/audits/auditList';
import { connect } from 'react-redux'
import renderTypeAhead from '../../common/fields/commonTypeAhead';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 8
  }, 
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '92%',
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
    },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  fab: {
    margin: theme.spacing(1),
  },  
  formControl: {
    minWidth: 100,
    padding: 16,
    width: '85%',
    top: 17
  },
  
typeAhead: {
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(1),
  marginTop: 2,
  width: '90%',
},
  formControlHeading:{
    paddingBottom : 10,
    fontSize: 14,
    left: 42,
    top: 10,
    position: 'relative'
  }
}))


function AuditFirmsFilter(props) {
  const classes = useStyles()
  const { handleSubmit, reset, resetFilter, loadAuditLists, firmList, key } = props

  
const handleFirmChange = async (e,v) => {
  if(e) {
    let params = {
      legal_name: v
    }
    loadAuditLists(params)
  }
}

const handleInputChange = (e) =>{

}

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={8} >
          <InputLabel className={classes.inputLabel}>Legal Name</InputLabel>
            <Field
              name='legal_name'
              margin='normal'
              component={renderTypeAhead}              
              key={key}
              className={classes.typeAhead}
              onChange={handleFirmChange}
              handleInputChange={handleInputChange}
              options={firmList ? firmList: []}
              getOptionLabel={(option => option.legal_name)}
              multiple={false}
            />
          </Grid>
          <Grid item sm={4} className={classes.buttonGrid}>
            <CustomizedButton buttonType={buttonType.submit}
              buttonText={filterButtons.search}
              type="submit" />
            <CustomizedButton buttonType={buttonType.submit}
              reset={true}
              clicked={() => { reset(); resetFilter(); }}
              buttonText={filterButtons.reset}
            />
          </Grid>
        </form>
      </Paper>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    firmList: state.auditList.firmList,
    
  }
}

const mapDispatch = dispatch => ({
  loadAuditLists: (params) => dispatch(loadAuditListDropdown(params))
})

const FirmFilterForm =  reduxForm({
  form: 'auditFirmsFilter',
  enableReinitialize: true
})(AuditFirmsFilter)

const connected = connect(
  mapStateToProps, mapDispatch
)(FirmFilterForm)

export default connected
