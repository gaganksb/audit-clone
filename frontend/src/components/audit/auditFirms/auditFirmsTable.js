import React, {useEffect} from 'react'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../common/customizedButton'
import {buttonType} from '../../../helpers/constants'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import Grid from '@material-ui/core/Grid'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import ListLoader from '../../common/listLoader'
import TablePaginationActions from '../../common/tableActions';
import { loadCountries } from '../../../reducers/countries';
import Fab from '@material-ui/core/Fab'
import EditIcon from '@material-ui/icons/Edit'
import EditDialog from './auditFirmModal'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'
import { loadAuditList } from '../../../reducers/audits/auditList'
import FirmDetails from './firmDetails';
import { editAuditFirm } from '../../../reducers/audits/editAuditFirm'
import { addAuditFirm } from '../../../reducers/audits/addAuditFirm'
import R from 'ramda';
import Tooltip from '@material-ui/core/Tooltip'
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { reset, reduxForm } from 'redux-form';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  noDataRow: {
    margin: 16,
    width: '498%',
    textAlign: 'center'
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(3)
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8,
    backgroundColor: '#0072BC'
  },
}))

const EDIT = 'EDIT'
const ADD = 'ADD'

const message = {
  success : {
    edit: 'Firm Updated Successfully !',
    add: 'Firm Added Successfully !'
  }
}

const rowsPerPage = 10

const button = {
  addButton: 'ADD AUDIT FIRM'
}
function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort,
    loadAuditList,
    editAuditFirm,
    addAuditFirm,
    postError,
    countries,
    successReducer,
    getCountries
  } = props

  
  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }

  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }


  const classes = useStyles()
  const [open, setOpen] = React.useState({ status: false, type: null })
  const [auditFirm, setAuditFirm] = React.useState({})  
  const [expanded, setExpanded] = React.useState(initialExpanded())
  const [firmCountry, setCountries] = React.useState([]);

  useEffect(() => {
    setExpanded(initialExpanded())
  }, [items]);

  function handleAudit(values) {
    const { 
      legal_name, address_1, address_2, city, country, telephone, fullname, email
    } = values;
    
    const { countries } = props;
    let selectedCountry = country.id ? country : countries.results.find(item=>item.name===country);
    
    const body = {
      legal_name, address_1, address_2, city,
      country: selectedCountry.id, telephone, fullname, email 
    };
    
    const { query } = props
    if (open.type === EDIT) {
      return editAuditFirm(auditFirm.id, body).then(() => {
        handleClose()
          loadAuditList(query)
          successReducer(message.success.edit)
        }).catch((error) => {
          postError(error, messages.error)
          throw new SubmissionError({
            ...error.response.data,
            _error: messages.error
          })
        })
    } else {
      return addAuditFirm(body).then(() => {
        handleClose()
          loadAuditList(query)
          successReducer(message.success.add)
        }).catch((error) => {
          postError(error, messages.error)
          throw new SubmissionError({
            ...error.response.data,
            _error: messages.error
          })
        })
    }
  }

  function handleClickOpen(item, action) {
    setAuditFirm(item)
    setOpen({ status: true, type: action })
  }

  function handleClose() {
    setOpen({ status: false, type: null })
  }

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }

  const handleInputChange = (e,v)=>{
  }

  return (
    <React.Fragment>
      <Grid className={classes.buttonGrid}>
      <CustomizedButton buttonType={buttonType.submit} 
                 buttonText={button.addButton}
                 clicked={() => handleClickOpen(null, ADD)} />
      </Grid>
      <Paper className={classes.root}>
        <ListLoader loading={loading}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>
              List of Audit Firms
            </div>
            <Table className={classes.table}>
              <TableHead className={classes.headerRow}>
                <TableRow>
                  {messages.tableHeader.map((item, key) =>
                    <TableCell style={{width: item.width}} align={item.align} key={key} className={classes.tableHeader}>
                      {(item.title)?<TableSortLabel
                        active={item.field === sort.field}
                        direction={sort.order}
                        onClick={() => handleSort(item.field, sort.order)}
                      >
                        {item.title}
                      </TableSortLabel>: null}
                    </TableCell>
                  )}
                </TableRow>
              </TableHead>
              {(items.length == 0 && !loading)? 
                <div className= {classes.noDataRow}>No data for the selected criteria</div> :
              <TableBody>
                {items.map((item, key) => (
                <FirmDetails
                    key={key}
                    item={item} 
                    expanded={expanded[key]} 
                    handleExpand={() => handleExpand(key)}
                    handleEdit={() => handleClickOpen(item, EDIT)}
                     /> 
                ))}
              </TableBody>
                }
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[rowsPerPage]}
                    colSpan={0}
                    count={totalItems}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      native: true,
                    }}
                    onChangePage={handleChangePage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
            <EditDialog 
              handleClose={handleClose}
              type={open.type}
              open={open.status}
              auditFirm={auditFirm}
              onSubmit={handleAudit}
              countries={firmCountry}
              handleInputChange={handleInputChange}
            />
          </div>
        </ListLoader>
      </Paper>
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  auditList: state.auditList.list,
  countries: state.countries
})

const mapDispatchToProps = (dispatch) => ({
  loadAuditList: params => dispatch(loadAuditList(params)),
  editAuditFirm: (id, body) => dispatch(editAuditFirm(id, body)),
  addAuditFirm: body => dispatch(addAuditFirm(body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editAssessmentRating', message)),  
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)
