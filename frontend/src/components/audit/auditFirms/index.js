import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import R from 'ramda'
import { browserHistory as history, withRouter } from 'react-router'
import { loadAuditList } from '../../../reducers/audits/auditList'
import AuditFirmsFilter from './auditFirmsFilter'
import { loadUser } from '../../../reducers/users/userData'
import AuditFirmsTable from './auditFirmsTable'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  paper: {
    color: theme.palette.text.secondary,
    minHeight: 150
  }
}))

const messages = {
  tableHeader: [
    { title: '', align: 'left',  width: '20%'},
    { title: 'Legal Name', width: '40%', align: 'left', field: '' },
    { title: '', align: 'left', width: '40%', }
  ],
  error: 'Could not complete the action'
}

const AuditFirms = props => {
  const {
    loading,
    totalCount,
    query,
    loadUserData,
    loadAuditList,
    auditList
  } = props
  const classes = useStyles()
  const [params, setParams] = useState({ page: 0 })  
  const [key, setKey] = useState('default')

  useEffect(() => {
    loadAuditList(query)
    loadUserData()
  }, [query])

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field,
    order: 'desc'
  })

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage
    }))
  }

  function handleSort(field, order) {
    const { pathName, query } = props

    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order
      })
    })
  }

  function resetFilter() {
    const { pathName } = props

    setKey(date.getTime())
    setParams({ page: 0 })
    history.push({
      pathname: pathName,
      query: {
        page: 1
      }
    })
  }

  function handleSearch(values) {
    const { pathName, query } = props
    const { legal_name } = values
 
    setParams({ page: 0 })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        legal_name
      })
    })
  }

  function handleChange(e) {
    if (e.inputValue) {
      let params = {
        code: e.inputValue,
      }
    }
  }
  var date = new Date();
  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <AuditFirmsFilter onSubmit={handleSearch} resetFilter={resetFilter} handleChange={handleChange} key={key} />
      <AuditFirmsTable
        messages={messages}
        sort={sort}
        items={auditList}
        loading={loading}
        totalItems={totalCount}
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  totalCount: state.auditList.totalCount,
  loading: state.auditList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
  membership: state.userData.data.membership,
  auditList: state.auditList.list
})

const mapDispatch = dispatch => ({
  loadUserData: () => dispatch(loadUser()),
  loadAuditList: (params) => dispatch(loadAuditList(params))
})

const connectedAuditFirms = connect(mapStateToProps, mapDispatch)(AuditFirms)
export default withRouter(connectedAuditFirms)
