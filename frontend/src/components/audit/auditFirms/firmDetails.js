import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';


const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    icon: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    detailHeader: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 500
    },
    details: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563',
        margin: theme.spacing(1),
        fontWeight: 300,
    },
    detailRow: {        
        // width: '450%',
        marginLeft: 64,
        // borderBottom: '1px solid rgba(224, 224, 224, 1)',
    }
}));

const messages = {
    detailHeaders: [
        'Address 1',
        'Address 2',
        'City',
        'Country',
        'Focal Contact Number',
        'Focal Person Fullname',
        'Focal Person Email'
    ]
}

function FirmDetails(props) {
    const {
        expanded,
        handleExpand,
        item,
        handleEdit,
    } = props;

    const classes = useStyles();

    return (
        <React.Fragment>
            <TableRow key={item.legal_name}>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {(expanded) ?
                        <ExpandLess className={classes.icon} onClick={handleExpand()} /> :
                        <ExpandMore className={classes.icon} onClick={handleExpand()} />
                    }
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {item.legal_name}
                </TableCell>
                <TableCell align='right'>
                    <Tooltip title={"Edit"}>
                        <Fab color="primary"  size='small' className={classes.fab} onClick={handleEdit} style={{ backgroundColor: '#0072BC' }} >
                            <EditIcon />
                        </Fab>
                    </Tooltip>
                </TableCell>
            </TableRow>
            {(expanded) ?
                <TableRow>
                    <td container>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[0]} 
                            </div>
                            <div className= {classes.details}>
                                {item.address_1}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[1]} 
                            </div>
                            <div className= {classes.details}>
                                {item.address_2}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[2]} 
                            </div>
                            <div className= {classes.details}>
                                {item.city}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[3]} 
                            </div>
                            <div className= {classes.details}>
                                {(item.country)?item.country.name: ' '}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[4]} 
                            </div>
                            <div className= {classes.details}>
                                {(item.telephone)}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[5]} 
                            </div>
                            <div className= {classes.details}>
                                {(item.fullname)}
                            </div>
                        </div>
                        <div className={classes.detailRow}>
                            <div className= {classes.detailHeader}>
                            {messages.detailHeaders[6]} 
                            </div>
                            <div className= {classes.details}>
                                {(item.email)}
                            </div>
                        </div>
                    </td>
                </TableRow> : null}
        </React.Fragment>
    );
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname,
    auditList: state.auditList.list
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FirmDetails);

export default withRouter(connected);
