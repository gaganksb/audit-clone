import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import Typography from '@material-ui/core/Typography';
import { browserHistory as history } from 'react-router';
import _ from 'lodash';
import Tooltip from '@material-ui/core/Tooltip';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Warning from '@material-ui/icons/Warning';
import PublishIcon from '@material-ui/icons/Publish';
import GetAppIcon from '@material-ui/icons/GetApp';
import Questionnaire from './icq/index';
import { AUDIT } from '../../helpers/constants';
import CustomizedButton from '../common/customizedButton';
import { downloadURI } from '../../helpers/others';
import { buttonType } from '../../helpers/constants';
import { errorToBeAdded } from '../../reducers/errorReducer';
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { SubmissionError } from 'redux-form';
import { withRouter } from 'react-router';
import { editICQ } from '../../reducers/reports/icqQuestionnaire/uploadIcq'
import { loadReportList } from '../../reducers/reports/managementReports/getReports';
import { loadProject } from '../../reducers/projects/projectsReducer/getProjectDetails';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  root: {
    margin: 24,
    width: '95%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    fontWeight: 'bold',
    display: 'flex',
    alignItems: 'center'
  },
  colorClass: {
    color: 'green',
    marginLeft: theme.spacing(2),
    position: 'absolute',
    right: 22,
    fontSize: 17
  },
  warning: {
    color: '#FFC400',
    marginLeft: theme.spacing(2),
    position: 'absolute',
    right: 22,
    fontSize: 17
  },
  icqClass: {
    marginLeft: theme.spacing(2),
    position: 'absolute',
    right: 22,
    fontSize: 17,
    display: 'flex'
  },
  panel: {
    padding: 16,
    '&:hover': {
      cursor: 'pointer'
    },
  }
}));

const SimpleExpansionPanel = props => {
  const classes = useStyles();
  const {
    id,
    status,
    project,
    user,
    editICQ,
    postError,
    successReducer,
    loadReportList,
    loadProjectDetails
  } = props;
  const [checkboxError, setCheckboxError] = React.useState(false);
  const focal = _.get(project.agreement_member, ['auditing', 'focal', 'id'], '');
  const alt_focal = _.get(project.agreement_member, ['auditing', 'alternate_focal', 'id'], '');
  const reviewers = _.get(project.agreement_member, ['auditing', 'reviewers'], '');
  const [relatedProjects, updateRelatedProject] = React.useState({});
  const [showIcq, updateIcq] = React.useState(false)
  let url = `/api/icq/download-icq-template/`
  const handleClose = () => {
    updateIcq(false)
  }
  const year = project.cycle && project.cycle.budget_ref
  const success = {
    edit: 'ICQ Questionnaire Updated !'
  }

  function handleEditICQPartner(values) {
    const projects = []
    Object.keys(relatedProjects).map(item=>{
      if(relatedProjects[item] === true){
        projects.push(item)
      }
    })
    const { report_doc } = values;
    const body = {
      report_doc,
      year,
      partner: project && project.partner && project.partner.id,
      business_unit: project && project.bu
    };

    const formData = new FormData();
    if(projects.length === 0){
      setCheckboxError(true)
      return false
    }else if (projects.length > 0){
      const projectList = projects.join(",")
      formData.append("related_projects", projectList)
    }
    for (var key in body) {
      formData.append(key, body[key]);
    }

    return editICQ(formData).then(() => {
      handleClose();
      successReducer(success.edit)
      loadReportList(props.id)
      loadProjectDetails(props.id)
    }).catch((error) => {
      postError(error, 'Error while uploading ICQ Questionnaire');
      if(error && error.response && error.response.data){
        throw new SubmissionError({
          ...error.response.data,
          _error: 'Error while uploading ICQ Questionnaire',
        });  
      }
    });
  }

  return (
    <div className={classes.root}>
      <ExpansionPanel className={classes.panel}>
        <Typography className={classes.heading} onClick={(status.audit_report == 'published' || ((user.user_type == AUDIT) && ((focal == user.userId)
          || (alt_focal == user.userId) || (reviewers && reviewers.length != 0 && reviewers.some(audit => audit.id == user.userId))))) ? () => history.push("/reports/" + `${id}`) : null} >AUDIT REPORT
          {(status.audit_report == 'published') ? <Tooltip title='Report Published'>
            <CheckCircle className={classes.colorClass}>
              <circle cx="12" cy="12" r="8" />
            </CheckCircle>
          </Tooltip> :
            <Tooltip title='Report still being prepared'>
              <Warning className={classes.warning}>
                <circle cx="12" cy="12" r="8" />
              </Warning>
            </Tooltip>}
        </Typography>
      </ExpansionPanel>
      <ExpansionPanel className={classes.panel}>
        <Typography className={classes.heading} >ICQ QUESTIONNAIRE
          <div className={classes.icqClass}>
            {<div>
              <CustomizedButton buttonType={buttonType.submit}
                buttonText="Template"
                clicked={() => downloadURI(url, 'ICQ Template')} />
            </div>}
            {<PublishIcon onClick={() => updateIcq(!showIcq)} style={{ marginLeft: 16, marginRight: 16, marginTop: 8 }} />}
            {
              project &&
              project.icq_report &&
              project.icq_report.report_doc && 
              project.icq_report.report_doc.filepath && 
              <GetAppIcon onClick={() => downloadURI(project.icq_report.report_doc.filepath, 'ICQ report')} style={{ marginTop: 8 }} />
            }
          </div>
        </Typography>
      </ExpansionPanel>
      {showIcq &&
        <Questionnaire
          open={showIcq}
          handleClose={handleClose}
          project={project}
          relatedProjects={relatedProjects}
          updateRelatedProject={updateRelatedProject}
          onSubmit={handleEditICQPartner}
          checkboxError={checkboxError}
          setCheckboxError={setCheckboxError}
        />
      }
      <ExpansionPanel className={classes.panel}>
        <Typography className={classes.heading} onClick={(status.mgmt_letter == 'published' || (user.user_type == AUDIT && project.icq_report ))? 
          () => history.push("/management-report/" + `${year}` + "/" + `${id}`): null}>
          MANAGEMENT LETTER
          {(status.mgmt_letter === 'published') ? <Tooltip title='Report Published'>
            <CheckCircle className={classes.colorClass}>
              <circle cx="12" cy="12" r="8" />
            </CheckCircle>
          </Tooltip> :
            (!project.icq_report && user.user_type == AUDIT)? 
            <Tooltip title='Need to upload ICQ questionnaire before proceeding to management letter'> 
            <Warning className={classes.warning}>
            <circle cx="12" cy="12" r="8" />
          </Warning>
          </Tooltip>: <Tooltip title='Management letter still being prepared'>
              <Warning className={classes.warning}>
                <circle cx="12" cy="12" r="8" />
              </Warning>
            </Tooltip>}
        </Typography>
      </ExpansionPanel>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  mgmtReport: state.managementReport.report
});

const mapDispatchToProps = (dispatch) => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'ICQuestionnaire', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  editICQ: (body) => dispatch(editICQ(body)),
  loadReportList: (ID, params) => dispatch(loadReportList(ID, params)),
  loadProjectDetails: id => dispatch(loadProject(id))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SimpleExpansionPanel);

export default withRouter(connected);