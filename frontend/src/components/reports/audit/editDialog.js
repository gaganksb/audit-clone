import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton'
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderRadioField } from '../../../helpers/formHelper';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { RadioOptions } from '../../../helpers/constants';
import { renderTextArea } from '../../common/renderFields';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    marginTop: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#0072bc'
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  },
  buttonWrapper: {
    marginTop: theme.spacing(2),
    position: 'relative',
    overflow: 'hidden',
    display: 'inline-block',

    '& > input': {
    fontSize: 100,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0,
    }
  },
  inputFile: {
    position: 'relative',
    top: -9,
    minHeight: 29,
    borderRadius: 5,
    minWidth: 308
  },
  btn: {
    border: 'none',
    color: 'white',
    backgroundColor: '#EDB57E',
    borderRadius: 5,
    minWidth: 94,
    font: '14px/19px Open Sans',
    height: 30,
    fontWeight: 'bold'
  },
  dialogTitle: {
    font: 'bold 18px/24px Roboto',
    color: '#606060',

  }
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'overall_opinion',
    'audit_opinion',
    'basis_for_opinion',
    'mgt_responsibilities',
    'auditor_responsibilities',
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

export class FieldFileInput  extends React.Component{
  constructor(props) {
    super(props)
    this.onChange = this.onChange.bind(this)
    this.state ={
      text: _.get(this.props.report,['attachments','0','filename'], null)
    }
  }
  
  onChange(e) {
    const { input: { onChange } } = this.props
    this.setState({
      text: e.target.files[0].name
    })
    onChange(e.target.files[0])
  }

  render(){
    
    const { input: { value }, classes, report } = this.props
    return(
     <div>     
     <input type='text' className={classes.inputFile} value={this.state.text? this.state.text: 'N/A'} readOnly />
     <span className={classes.buttonWrapper}>
       <button className={classes.btn}>{(this.state.text)? 'Upload other file' : 'Upload'}</button>
       <input
        type='file'
        onChange={this.onChange}
       />
     </span>
     </div>
    )
}
}



function ReportDialog(props) {
  const classes = useStyles();
  const { handleSubmit, handleClose, open } = props;
    return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle className={classes.dialogTitle}>{"AUDIT REPORT"}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
            <div style={{ width: '100%' }}>
              <div className={classes.subText}>OVERALL OPINION</div>
              <Field
                name="overall_opinion"
                component={renderRadioField}
                className={classes.radio}
                margin='normal'
              >
                {RadioOptions.map((option, index) => (
                  <FormControlLabel
                    key={index}
                    value={option.value}
                    control={<Radio color="default" />}
                    label={option.label}
                    labelPlacement="end"
                  />
                ))}
              </Field>
              <div className={classes.subText}>AUDIT OPINION</div>
              <Field
                name='audit_opinion'
                component={renderTextArea}
              />
              <div className={classes.subText}>BASIS FOR OPINION</div>
              <Field
                name='basis_for_opinion'
                component={renderTextArea}
              />
              <div className={classes.subText}>EMPHASIS OF MATTER</div>
              <Field
                name='emphasis_of_matter'
                component={renderTextArea}
              />
              <div className={classes.subText}>RESPONSIBILITIES FOR THE MANAGEMENT</div>
              <Field
                name='mgt_responsibilities'
                component={renderTextArea}
              />
              <div className={classes.subText}>AUDITOR'S RESPONSIBILITIES FOR THE AUDIT OF THE PROJECT FINANCIAL REPORT</div>
              <Field
                name='auditor_responsibilities'
                component={renderTextArea}
              />
              <div className={classes.subText}>ADDITIONAL COMMENTS (optional)</div>
              <Field
                name='additional_comment'
                component={renderTextArea}
              />
              <div className={classes.subText}>ATTACHMENT (optional)</div>
                <Field name="attachment" component={FieldFileInput} classes={classes} report={props.report}/>
             <div className={classes.buttonGrid}>
             <CustomizedButton
                clicked={handleClose}
                buttonType={buttonType.cancel}
                buttonText={buttonType.cancel}
              />
              <CustomizedButton
                type='submit'
                buttonType={buttonType.submit}
                buttonText='Save'
              />
              </div>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const reportDialogForm = reduxForm({
  form: 'reportDialog',
  validate,
  enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.report
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);