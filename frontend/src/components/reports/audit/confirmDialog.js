import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CustomizedButton from '../../common/customizedButton'
import { buttonType } from '../../../helpers/constants';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  },
}));

function AlertDialog(props) {
  const { open, handleClose, updateReport } = props;

  const classes = useStyles();

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{"AUDIT REPORT"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText component={'div'} className={classes.dialogContentText} >
            The AUDIT REPORT is ready for publishing
           <div>
              By clicking on CONFIRM, a notification will be sent to UNHCR and Partner Focal Points to inform them that they can start reviewing the report.
           </div>
          </DialogContentText>
        </DialogContent>
        <Divider />
        <DialogActions>
          <div className={classes.buttonGrid}>
          <CustomizedButton
                clicked={handleClose}
                buttonType={buttonType.cancel}
                buttonText={buttonType.cancel}
              />
               <CustomizedButton
                clicked={updateReport}
                buttonType={buttonType.submit}
                buttonText='Confirm'
              />
          </div>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialog;