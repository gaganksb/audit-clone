import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import CustomizedButton from "../../common/customizedButton";
import { buttonType } from "../../../helpers/constants";
import Grid from "@material-ui/core/Grid";
import Radio from "@material-ui/core/Radio";
import { loadReportList } from "../../../reducers/reports/auditReports/getReports";
import { editAuditReport } from "../../../reducers/reports/auditReports/editReport";
import Paper from "@material-ui/core/Paper";
import EditDialog from "./editDialog";
import { reset } from "redux-form";
import ConfirmDialog from "./confirmDialog";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { RadioOptions, auditNote, AUDIT } from "../../../helpers/constants";
import { downloadURI } from "../../../helpers/others";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { SubmissionError } from "redux-form";
import Tooltip from "@material-ui/core/Tooltip";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Warning from "@material-ui/icons/Warning";
import { showSuccessSnackbar } from '../../../reducers/successReducer';

const EDIT = "EDIT";
const CONFIRM = "CONFIRM";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  radio: {
    marginLeft: theme.spacing(2),
    "& > span ": {
      padding: 4,
      fontSize: 14,
      fontWeight: "bold",
      color: "#6F716F !important",
    },
  },
  button: {
    marginTop: theme.spacing(1),
    backgroundColor: "#EDB57E",
    minWidth: 94,
    borderRadius: 5,
    height: 30,
    marginLeft: 16,
    fontFamily: "Open Sans",
    fontSize: 14,
    lineHeight: "12px",
    color: "white",
    textTransform: "capitalize",
    "&:hover": {
      backgroundColor: "#EDB57E",
    },
  },
  publish: {
    minWidth: 94,
    borderRadius: 5,
    height: 30,
    marginLeft: 16,
    fontFamily: "Open Sans",
    fontSize: 14,
    lineHeight: "12px",
    color: "white",
    textTransform: "capitalize",
  },
  buttonGrid: {
    display: "flex",
    width: "100%",
    paddingRight: theme.spacing(2),
    justifyContent: "flex-end",
  },
  detailsContainer: {
    marginTop: 40,
    padding: 10,
  },
  typography: {
    font: "bold 14px/19px Roboto",
    color: "#344563",
    paddingLeft: 5,
  },
  typographybold: {
    font: "bold 14px/19px Roboto",
    color: "#0072bc",
    paddingLeft: 16,
  },
  titleContainer: {
    padding: "15px 30px",
    minHeight: "unset",
  },
  titleWrapper: {
    display: "flex",
    alignItems: "center",
    "& > svg": {
      width: 20,
      height: 20,
      cursor: "pointer",
      marginRight: 15,
    },
  },
  colorClass: {
    color: "green",
    marginLeft: theme.spacing(2),
  },
  warning: {
    color: "#FFC400",
    marginLeft: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    color: "#606060",
    font: "bold 16px/24px Roboto",
  },
  container: {
    borderBottom: "1px solid #E9ECEF",
    marginBottom: theme.spacing(2),
  },
  columns: {
    borderRight: "1px solid #E9ECEF",
    marginBottom: 15,
  },
}));

const AuditReportOverview = (props) => {
  const classes = useStyles();
  const {
    report,
    loadReportList,
    project,
    reset,
    editAuditReport,
    pathName,
    postError,
    user,
    successReducer
  } = props;
  const [open, setOpen] = React.useState({ type: null, status: false });

  const ID = pathName.split("/").slice(-2)[1];

  useEffect(() => {
    loadReportList(ID);
  }, []);

  function handleClose() {
    setOpen({ ...open, type: "EDIT", status: false });
  }

  function handleClickOpen() {
    reset();
    setOpen({ ...open, type: "EDIT", status: true });
  }

  function handlePublish() {
    setOpen({ ...open, type: "CONFIRM", status: true });
  }

  function handleCloseDialog() {
    setOpen({ ...open, type: "CONFIRM", status: false });
  }

  function handleSubmit(values) {
    const id = report.id;
    const {
      attachment,
      overall_opinion,
      audit_opinion,
      basis_for_opinion,
      emphasis_of_matter,
      mgt_responsibilities,
      auditor_responsibilities,
      additional_comment,
    } = values;

    const body = {
      common_file: attachment,
      overall_opinion,
      audit_opinion,
      basis_for_opinion,
      emphasis_of_matter,
      mgt_responsibilities,
      auditor_responsibilities,
      additional_comment,
      published: false,
    };

    return editAuditReport(id, body)
      .then(() => {
        handleClose();
        loadReportList(ID);
        successReducer("The report has been saved, Please click publish to make it available for everyone")
      })
      .catch((error) => {
        postError(error, "Form upload failed");
        throw new SubmissionError({
          ...error.response.data,
          _error: "Form upload failed",
        });
      });
  }

  function publishReport() {
    const id = report.id;
    const body = { published: true };
    return editAuditReport(id, body)
      .then(() => {
        handleClose();
        loadReportList(ID);
      })
      .catch((error) => {
        postError(error, messages.error);
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error,
        });
      });
  }

  return (
    <div>
      <Paper className={`${classes.root} ${classes.titleContainer}`}>
        <div className={classes.titleWrapper}>
          <svg
            onClick={() => window.history.back()}
            fill="#0072bc"
            version="1.1"
            id="Capa_1"
            x="0px"
            y="0px"
            width="459px"
            height="459px"
            viewBox="0 0 459 459"
            style={{ enableBackground: "new 0 0 459 459" }}
            xmlSpace="preserve"
          >
            <g>
              <g id="reply">
                <path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" />
              </g>
            </g>
          </svg>
          <Typography className={classes.title}>
            AUDIT REPORT - {project.title}
          </Typography>
          {!report.published && report.can_be_published && (
            <CustomizedButton
              clicked={handlePublish}
              buttonType={buttonType.submit}
              buttonText="PUBLISH"
            />
          )}
          {report.published ? (
            <Tooltip title="Published">
              <CheckCircle className={classes.colorClass}>
                <circle cx="12" cy="12" r="8" />
              </CheckCircle>
            </Tooltip>
          ) : (
            <Tooltip title="Unpublished">
              <Warning className={classes.warning}>
                <circle cx="12" cy="12" r="8" />
              </Warning>
            </Tooltip>
          )}
        </div>
      </Paper>
      <Paper className={`${classes.root} ${classes.detailsContainer}`}>
        <Grid container spacing={3} className={classes.container}>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>
              OVERALL OPINION
            </Typography>
          </Grid>
          {RadioOptions.map((option, index) => (
            <FormControlLabel
              key={index}
              value={option.value}
              control={
                <Radio
                  color="default"
                  checked={report.overall_opinion == option.value}
                />
              }
              label={option.label}
              disabled={true}
              className={classes.radio}
              labelPlacement="end"
            />
          ))}
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              AUDIT OPINION
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.audit_opinion ? report.audit_opinion : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              BASIS FOR OPINION
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.basis_for_opinion ? report.basis_for_opinion : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              EMPHASIS OF MATTER
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.emphasis_of_matter ? report.emphasis_of_matter : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              RESPONSIBILITIES FOR THE MANAGEMENT
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.mgt_responsibilities
                ? report.mgt_responsibilities
                : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              AUDITOR'S RESPONSIBILITIES FOR THE AUDIT OF THE PROJECT FINANCIAL
              REPORT
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.auditor_responsibilities
                ? report.auditor_responsibilities
                : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              ADDITIONAL COMMENTS
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>
              {report.additional_comment ? report.additional_comment : "N/A"}
            </Typography>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>
              ATTACHMENT
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.typography}>
              {report.attachments
                ? report.attachments.map((attachments, index) => (
                  <div
                    key={index}
                    onClick={() =>
                      window.open(attachments.filepath, "_blank")
                    }
                  >
                    {attachments.filename}
                  </div>
                ))
                : "N/A"}
            </div>
          </Grid>
          <hr style={{ width: "98%", color: "#9A9A9A" }}></hr>

          <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
            <Typography className={classes.typographybold}>Note</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              className={classes.typography}
              style={{ color: "#0072bc" }}
            >
              {auditNote}
            </Typography>
          </Grid>
          {user.user_type == AUDIT ? (
            <div className={classes.buttonGrid}>
              <Grid
                style={{
                  maxWidth: "19%",
                  display: "flex",
                  justifyContent: "flex-end",
                }}
              >
                <CustomizedButton
                  clicked={() => handleClickOpen()}
                  buttonType={buttonType.submit}
                  buttonText="EDIT"
                />
              </Grid>
            </div>
          ) : null}

          <EditDialog
            open={open.type === EDIT && open.status === true}
            handleClose={handleClose}
            report={report}
            onSubmit={handleSubmit}
          />
          <ConfirmDialog
            open={open.type == CONFIRM && open.status == true}
            handleClose={handleCloseDialog}
            updateReport={publishReport}
          />
        </Grid>
      </Paper>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => ({
  report: state.auditReport.report,
  project: state.projectDetails.project,
  pathName: ownProps.location.pathname,
  user: state.session,
});

const mapDispatch = (dispatch) => ({
  loadReportList: (ID, params) => dispatch(loadReportList(ID, params)),
  reset: () => dispatch(reset("reportDialog")),
  editAuditReport: (id, body) => dispatch(editAuditReport(id, body)),
  postError: (error, message) =>
    dispatch(errorToBeAdded(error, "auditingReport", message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
});

const connectedProjectOverview = connect(
  mapStateToProps,
  mapDispatch
)(AuditReportOverview);
export default withRouter(connectedProjectOverview);
