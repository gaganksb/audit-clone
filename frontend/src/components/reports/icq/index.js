import React, { useState, useEffect } from 'react';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { renderTextField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { withRouter } from 'react-router';
import FieldFileInput from '../../common/fields/fileField';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const type_to_search = 'partner__legal_name';

var errors = {}
const validate = values => {
    errors = {}
    const requiredFields = [
        'year',
        "report_doc"
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    return errors
}

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        display: 'flex',
    },
    textField: {
        marginRight: theme.spacing(1),
        marginTop: 2,
        width: '100%',
    },
    autoCompleteField: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: "100%",
        minWidth: "100%"
    },
    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: 18,
    },
    radio: {
        '& > span ': {
            fontSize: 14,
            fontWeight: 'bold',
            color: '#6F716F !important'
        }
    },
    radioButton: {
        '&.Mui-checked': {
            color: '#0072BC !important'
        }
    },
    button: {
        marginTop: theme.spacing(1),
    },
    paper: {
        color: theme.palette.text.secondary,
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: theme.spacing(2),
        width: '97%'
    },
    dialogContentText: {
        marginTop: theme.spacing(3),
    },
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
    formControl: {
        minWidth: 160,
    },
    inputFile: {
        position: 'relative',
        top: -9,
        minHeight: 29,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5,
        minWidth: 308
    },
    btn: {
        border: 'none',
        color: 'white',
        backgroundColor: '#EDB57E',
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        minWidth: 94,
        font: '14px/19px Open Sans',
        height: 30,
        fontWeight: 'bold'
    },
    buttonWrapper: {
        marginTop: theme.spacing(2),
        position: 'relative',
        overflow: 'hidden',
        display: 'inline-block',

        '& > input': {
            fontSize: 100,
            position: 'absolute',
            left: 0,
            top: 0,
            opacity: 0,
        }
    },
}));

const messages = {
    submit: 'Submit',
    cancel: 'Cancel',
    title: 'ICQ Questionnaire',
    text: 'Your Audit Firm, this year, is reviewing the following projects related to this same Partner. Please select from below, the projects for which you want to upload the same ICQ Questionnaire:',
    required: '(required)'
}

function ICQDialog(props) {
    const classes = useStyles();
    const {
        open,
        handleClose,
        handleChange,
        handleSubmit,
        relatedProjects,
        bu,
        setBu,
        project,
        updateRelatedProject,
        checkboxError,
        setCheckboxError,
        addICQQuestionnaireForm
    } = props;

    const handleCheckbox = (event) => {
        updateRelatedProject({ ...relatedProjects, [event.target.name]: event.target.checked });
        setCheckboxError(false)
    };

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogTitle >{messages.title}</DialogTitle>
                <Divider />
                <DialogContent>
                    <DialogContentText >
                        <form className={classes.container} onSubmit={handleSubmit} >
                            <Grid
                                container
                                direction="column"
                                justify="flex-start"
                                alignItems="flex-start"
                            >
                                <DialogContent style={{ paddingLeft: 10, paddingRight: 10 }}>
                                    <DialogContentText>
                                        {messages.text}
                                    </DialogContentText>
                                    {checkboxError}
                                    {
                                        project &&
                                        project.project_icqs &&
                                        project.project_icqs.map(item => {
                                            return (
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            name={item.agreement_id}
                                                            onChange={handleCheckbox}
                                                        />
                                                    }
                                                    label={`${item.business_unit__code}${item.number}`}
                                                />
                                            )
                                        })
                                    }
                                    {checkboxError && <span style={{color: "red"}}> Please select atleast one project </span>}
                                    <Field
                                        name="report_doc"
                                        component={FieldFileInput}
                                        classes={classes}
                                        error={errors['report_doc']}
                                    />
                                </DialogContent>
                                <Grid className={classes.buttonGrid}>
                                    <CustomizedButton
                                        clicked={handleClose}
                                        buttonText={messages.cancel}
                                        buttonType={buttonType.cancel} />
                                    <CustomizedButton
                                        type='submit'
                                        buttonText={messages.submit}
                                        submitting={addICQQuestionnaireForm && addICQQuestionnaireForm.submitting}
                                        buttonType={buttonType.submit} />
                                </Grid>
                            </Grid>
                        </form>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const addICQForm = reduxForm({
    form: 'addICQQuestionnaire',
    validate,
    enableReinitialize: true,
})(ICQDialog);


const mapStateToProps = (state, ownProps) => ({
    addICQQuestionnaireForm: state.form.addICQQuestionnaire 
});

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(addICQForm);

export default withRouter(connected);
