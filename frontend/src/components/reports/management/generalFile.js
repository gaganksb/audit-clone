import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Grid from "@material-ui/core/Grid";
import EditDialog from './editGeneralFile';
import { editGeneralReports } from '../../../reducers/reports/managementReports/editGeneralReport';
import { editMgmtReports } from '../../../reducers/reports/managementReports/patchReport';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import { AUDIT } from '../../../helpers/constants';
import _ from 'lodash';
import { reset } from 'redux-form';
import { showSuccessSnackbar } from '../../../reducers/successReducer';

const EDIT = 'EDIT'

const useStyles = makeStyles(theme => ({
  button: {
    marginTop: theme.spacing(1),
    backgroundColor: '#EDB57E',
    minWidth: 94,
    borderRadius: 5,
    height: 30,
    marginLeft: 16,
    fontFamily: 'Open Sans',
    fontSize: 14,
    lineHeight: '12px',
    color: 'white',
    textTransform: 'capitalize',
    '&:hover': {
      backgroundColor: '#EDB57E'
    }
  },
  buttonGrid: {
    display: 'flex',
    width: '100%',
    paddingRight: theme.spacing(2),
    justifyContent: 'flex-end',
  },
  detailsContainer: {
    padding: theme.spacing(3)
  },
  typography: {
    font: 'bold 14px/19px Roboto',
    color: '#606060',
    paddingLeft: 5
  },
  typographybold: {
    font: 'bold 14px/19px Roboto',
    color: '#0072bc',
    paddingLeft: 16
  },
  container: {
    borderBottom: '1px solid #E9ECEF',
    marginTop: theme.spacing(1),
    '& > div': {
      marginBottom: 12,
      paddingLeft: 60,
      '& > div': {
        marginBottom: '3px'
      },
      '&:first-child': {
        paddingLeft: 0
      }
    }
  },
}));

const GeneralReportOverview = props => {
  const classes = useStyles();
  const { 
    report,
    updateReport,
    editGeneralReport,
    postError,
    reset,
    loading,
    session,
    id,
    editMgmtReports,
    hideButton,
    successReducer
  } = props;
  const [open, setOpen] = React.useState({ type: null, status: false })


  function handleClickOpen() {
    reset();
    setOpen({ ...open, type: 'EDIT', status: true });
  }

  function handleClose() {
    setOpen({ ...open, type: 'EDIT', status: false });
  }

  const removeEmptyBody = (body) => {
    Object
    .entries(body)
    .forEach(([k, v]) => {
        if (v && typeof v === 'object') {
          removeEmptyBody(v);
        }
        if (v && typeof v === 'object' && !Object.keys(v).length || v === null || v === undefined) {            
                delete body[k];
        }
    });
return body;

  }

  async function handleSubmit(values) {
    const { introduction, scope_of_work, audit_objectives, executive_summary } = values;
    const body = { general: { introduction, scope_of_work, audit_objectives, executive_summary } };
    if(Object.keys(removeEmptyBody(body.general)).length===0) {
        return postError('error', 'All the fields are mandatory !');
    }

    if (report.general && report.general.length === 0) {
      const mgmtId = report.id
      return editMgmtReports(id, mgmtId, body).then(() => {
        handleClose();
        updateReport();
        successReducer("The report has been saved, Please click publish to make it available for everyone");
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response.data,
          _error: 'Report upload failed',
        });
      });
    }
    else {
      const mgmtId = report.general[0].id;
      return editGeneralReport(mgmtId, body.general).then(() => {
        handleClose();
        updateReport();
        successReducer("The report has been saved, Please click publish to make it available for everyone");
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response.data,
          _error: 'Report upload failed',
        });
      });
    }
  }

  return (
    <div className={classes.detailsContainer}>
      <Grid container spacing={3} className={classes.container}>
        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >INTRODUCTION</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.typography}>{_.get(report.general, '[0].introduction', 'N/A')}</Typography>
        </Grid>
        <hr style={{ width: '98%', color: '#9A9A9A' }}></hr>

        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >SCOPE OF WORK</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.typography}>{_.get(report.general, '[0].scope_of_work', 'N/A')}</Typography>
        </Grid>
        <hr style={{ width: '98%', color: '#9A9A9A' }}></hr>

        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >AUDIT OBJECTIVES</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.typography}>{_.get(report.general, '[0].audit_objectives', 'N/A')}</Typography>
        </Grid>
        <hr style={{ width: '98%', color: '#9A9A9A' }}></hr>

        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >EXECUTIVE SUMMARY</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.typography}>{_.get(report.general, '[0].executive_summary', 'N/A')}</Typography>
        </Grid>
        <hr style={{ width: '98%', color: '#9A9A9A' }}></hr>

        {!hideButton ? (session.user_type=== AUDIT &&  <div className={classes.buttonGrid}>
          <Grid style={{ maxWidth: '19%', display: 'flex', justifyContent: 'flex-end' }}>
            <CustomizedButton
              clicked={() => handleClickOpen()}
              buttonText={"Edit"}
              buttonType={buttonType.action} />
          </Grid>
        </div>):null}

        <EditDialog open={open.type === EDIT && open.status === true} handleClose={handleClose} report={report} onSubmit={handleSubmit} />
      </Grid>
    </div>
  )
};

const mapStateToProps = (state, ownProps) => ({
  session: state.session,
});

const mapDispatch = dispatch => ({
  editGeneralReport: (id, body) => dispatch(editGeneralReports(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'auditingReport', message)),
  reset: () => dispatch(reset('generalReportDialog')),
  editMgmtReports: (id, mgmtId, body) => dispatch(editMgmtReports(id, mgmtId, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
});

const connectedProjectOverview = connect(mapStateToProps, mapDispatch)(GeneralReportOverview);
export default withRouter(connectedProjectOverview);