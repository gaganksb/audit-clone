import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(2),
  },
  
  button: {
    marginRight: theme.spacing(1)
  }
}));

const messages ={ 
    yes: 'Submit',
    no: 'Cancel'}

function AlertDialog(props) {
  const {open, handleClose, handleDelete, selectedEntry } = props;

  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{`Delete ${selectedEntry}` }</DialogTitle>
        <Divider />
        <DialogContent className={classes.dialogContentText} >
            Please notice by clicking on SUBMIT the selected entry will be deleted
        </DialogContent>
        <DialogActions>
        <CustomizedButton 
          clicked={handleClose} 
          buttonText={messages.no} 
          buttonType={buttonType.cancel} />      
          <CustomizedButton 
          clicked={handleDelete} 
          buttonText={messages.yes} 
          buttonType={buttonType.submit} />
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialog;