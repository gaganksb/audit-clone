import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderTextArea } from '../../common/renderFields';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    marginTop: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#0072bc'
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#1F80C5'
    }
  },
  dialogTitle: {
    font: 'bold 18px/24px Roboto',
    color: '#606060',

  }
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'introduction',
    'scope_of_work',
    'audit_objectives',
    'executive_summary',
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
    // else if(values[field].length < 2000) {
    //   errors[field] = 'Atleast 2000 characters required!'
    // }
  })
  return errors
}

function ReportDialog(props) {
  const classes = useStyles();
  const { handleSubmit, handleClose, open } = props;
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle className={classes.dialogTitle}>{"MANAGEMENT LETTER - SECTION 1"}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
            <div style={{ width: '100%' }}>
              <div className={classes.subText}>
                INTRODUCTION
                </div>
              <Field
                name='introduction'
                component={renderTextArea}
              />
              <div className={classes.subText}>
                SCOPE OF WORK
                </div>
              <Field
                name='scope_of_work'
                component={renderTextArea}
              />
              <div className={classes.subText}>
                OBJECTIVES
                </div>
              <Field
                name='audit_objectives'
                component={renderTextArea}
              />
              <div className={classes.subText}>
                EXECUTIVE SUMMARY
                </div>
              <Field
                name='executive_summary'
                component={renderTextArea}
              />

              <div className={classes.buttonGrid}>

                <CustomizedButton
                  clicked={handleClose}
                  buttonText={"Cancel"}
                  buttonType={buttonType.cancel} />
                <CustomizedButton
                  type="submit"
                  buttonText={"Save"}
                  buttonType={buttonType.submit} />
              </div>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const reportDialogForm = reduxForm({
  form: 'generalReportDialog',
  validate,
  enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
  let initialValues = (ownProps.report.general) ? ownProps.report.general[0] : null
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);