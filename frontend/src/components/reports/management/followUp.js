import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Grid from "@material-ui/core/Grid";
import FollowupTable from './followupTable';
import { editFollowUps } from '../../../reducers/reports/managementReports/patchFollowUp';
import { editMgmtReports } from '../../../reducers/reports/managementReports/patchReport';
import { deleteFollowupRequest} from '../../../reducers/reports/managementReports/deleteFollowup';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import AddDialog from './editFollowUp'
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import { reset } from 'redux-form';
import DeleteDialog from './deleteDialog';
import { makeStyles } from '@material-ui/core/styles';

const ADD = 'ADD'
const EDIT = 'EDIT'
const DELETE = 'DELETE'
const messages = {
  heading: 'FOLLOW UP OF THE IMPLEMENTATION OF PREVIOUS AUDIT RECOMMENDATIONS',
  tableHeader: [
    { title: 'ID', align: 'left', width: '10%' },
    { title: 'Previous Recommendation', align: 'left', width: '15%' },
    { title: 'Criticality', align: 'left', width: '15%' },
    { title: 'Summary', align: 'left', width: '15%' },
    { title: 'Implemented', align: 'left', width: '15%' },
    // { title: 'Comments', width: '20%', align: 'left'},
    { title: '', align: 'right', width: '10%', }
  ],
  error: 'Could not complete the action',
  success: 'Table Updated Successfully'
}

const useStyles = makeStyles(theme => ({
  button: {
    marginTop: theme.spacing(1),
    backgroundColor: '#EDB57E',
    minWidth: 94,
    borderRadius: 5,
    height: 30,
    marginLeft: 16,
    fontFamily: 'Open Sans',
    fontSize: 14,
    lineHeight: '12px',
    color: 'white',
    textTransform: 'capitalize',
    '&:hover': {
      backgroundColor: '#EDB57E'
    }
  },
  buttonGrid: {
    display: 'flex',
    width: '100%',
    paddingRight: theme.spacing(2),
    justifyContent: 'flex-end',
  },
  detailsContainer: {
    padding: theme.spacing(3)
  },
  typography: {
    font: 'bold 14px/19px Roboto',
    color: '#606060',
    paddingLeft: 5
  },
  typographybold: {
    font: 'bold 14px/19px Roboto',
    color: '#0072bc',
    paddingLeft: 16
  },
  container: {
    borderBottom: '1px solid #E9ECEF',
    marginTop: theme.spacing(1),
    '& > div': {
      marginBottom: 12,
      paddingLeft: 60,
      '& > div': {
        marginBottom: '3px'
      },
      '&:first-child': {
        paddingLeft: 0
      }
    }
  },
}));
const FollowUpReport = props => {
  const {
    loading,
    totalCount,
    editFollowUps,
    editMgmtReports,
    query,
    id,
    session,
    reset,
    deleteFollowup,
    updateReport,
    report,
    successReducer,
    postError,
    hideButton
  } = props

  const classes = useStyles();
  const [open, setOpen] = React.useState({ type: null, status: false })
  const [followup, setfollowup] = React.useState(null)
  const [key, setKey] = React.useState('default')
  var date = new Date();

  function handleClickOpen(item, action) {
    if(action === 'ADD') {      
      setKey(date.getTime())  
    }
    reset();    
    setOpen({ status: true, type: action })
    setfollowup(item)
  }
  function handleDeletefollowup() {
    deleteFollowup(followup.id).then(()=> {
      handleClose();
      updateReport();
      successReducer(messages.success);
    }).catch((error) => {
      postError(error, 'Delete failed');
      throw new SubmissionError({
        ...error.response,
        _error: 'Delete failed',
      });
    })
  }

  function handleSubmit(values) {
    const { control_attribute, prev_recommendation, criticality, summary, recommendation } = values

    if (!followup) {
      const follow_up = { follow_up: { control_attribute, prev_recommendation, criticality, summary, recommendation } }
      const mgmtId = report.id
      return editMgmtReports(id, mgmtId, follow_up).then(() => {
        handleClose();
        updateReport();
        successReducer(messages.success);
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response,
          _error: 'Report upload failed',
        });
      });
    } else {
      return editFollowUps(followup.id, values).then(() => {
        handleClose();
        updateReport();
        successReducer(messages.success);
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response,
          _error: 'Report upload failed',
        });
      });
    }
  }

  function handleClose() {
    setOpen({ ...open, type: 'ADD', status: false });
  }

  return (
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
       <FollowupTable
          messages={messages}
          items={report.follow_up}
          loading={loading}
          session={session}
          totalItems={totalCount}          
          handleEdit={(item) => handleClickOpen(item, EDIT)}
          handleDelete={(item) => handleClickOpen(item, DELETE)}
        />
         {session && session.user_type === 'AUDITOR' && <div className={classes.buttonGrid}>
          {!hideButton ? <Grid style={{ maxWidth: '19%', display: 'flex', justifyContent: 'flex-end' , marginTop: 24}}>
            <CustomizedButton
              clicked={() => handleClickOpen(null, ADD)}
              buttonText={"Add"}
              buttonType={buttonType.submit} />
          </Grid> : null}
        </div>}
         <AddDialog open={open.type === EDIT || open.type === ADD && open.status === true} followUp = {followup} handleClose={handleClose} onSubmit={handleSubmit} key={key} />
         <DeleteDialog open={open.type === DELETE && open.status === true} followUp = {followup} handleClose={handleClose} selectedEntry={"Followup Entry"} handleDelete={handleDeletefollowup} />
      </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  session: state.session
})

const mapDispatch = dispatch => ({
  loadUserData: () => dispatch(loadUser()),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'followUpReport', message)),
  editFollowUps: (id, body) => dispatch(editFollowUps(id, body)),
  editMgmtReports: (id, mgmtId, body) => dispatch(editMgmtReports(id, mgmtId, body)),
  reset: () => dispatch(reset('followUpReportDialog')),  
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  deleteFollowup: (id)=> dispatch(deleteFollowupRequest(id))
})

const connectedFollowUpReport = connect(mapStateToProps, mapDispatch)(FollowUpReport)
export default withRouter(connectedFollowUpReport)