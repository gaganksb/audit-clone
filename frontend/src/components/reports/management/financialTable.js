import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import FinancialDetails from './financialDetails';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import EditDialog from './editFinancialTable';
import { reset } from 'redux-form';
import { editFinancialReports } from '../../../reducers/reports/managementReports/editFinancialReport';
import {editMgmtReports } from '../../../reducers/reports/managementReports/patchReport';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import _ from 'lodash';
import { showSuccessSnackbar } from '../../../reducers/successReducer';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  button: {
    width: 40,
    marginLeft: 10,
    padding: 0,
    fontFamily: 'Open Sans',
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
  },
  table: {
    minWidth: 500,
  },
  noDataRow: {
    margin: 16,
    width: '498%',
    width: 'inherit',
    marginLeft: '139%',
    textAlign: 'center'
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderRight: '1px solid #E9ECEF',
  },
  tableHeader: {
    color: '#8898AA',
    fontFamily: 'Roboto',
    fontSize: 12,
    '&:first-child' : {
      color: '#2B72B6',
      fontSize: 14,
      fontWeight: 'bold'
    }
  },
  tableSubHeader: {
    color: '#8898AA',
    fontFamily: 'Roboto',
    fontSize: 12,
    '&:nth-child(even)': {
      borderRight: '1px solid #E9ECEF',
    },
    '&:last-child': {
      paddingLeft: 2,
      paddingRight: 1
    }
  },
  tableTitle: {
    padding: 20,
    fontWeight: 'bold'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '12px/16px Roboto',
    letterSpacing: 0,
    color: '#8898AA'
  },
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#606060',
    font: 'bold 18px/24px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
  fab: {
    margin: theme.spacing(1),
    backgroundColor: '#1F80C5'
  },
  customizedButton: {
    marginLeft: theme.spacing(3),
    marginTop: theme.spacing(2),
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
    height: 30,
    minWidth: 100,
    fontSize: 12,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#1F80C5'
    }
  }
}))

const EDIT = 'EDIT'
const rowsPerPage = 15

function CustomPaginationActionsTable(props) {
  const {
    items,
    loading,
    messages,
    editFinancialReport,
    editMgmtReports,
    total,
    reset,
    id,
    report,
    postError,
    updateReport,
    hideButton,
    successReducer
  } = props

  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }

  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }


  const classes = useStyles()
  const [open, setOpen] = React.useState(false)
  const [financialFinding, setFinancialFinding] = React.useState({})
  const [expanded, setExpanded] = React.useState(initialExpanded())
  const [usdTotal, updateTotal] = React.useState(0)

  useEffect(() => {
    setExpanded(initialExpanded())
  }, [items]);

  function handleClickOpen(item, action) {
    reset()
    setFinancialFinding(item)
    setOpen(true)
    reset()
  }

  function handleClose() {
    setOpen(false)
  }

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }

  const removeEmptyBody = (body) => {
    Object
    .entries(body)
    .forEach(([k, v]) => {
        if (v && typeof v === 'object') {
          removeEmptyBody(v);
        }
        if (v && typeof v === 'object' && !Object.keys(v).length || v === null || v === undefined) {            
                delete body[k];
        }
    });
return body;

  }

  async function handleSubmit(values) {
    let required_recovery={local_currency: values.RRlocal_currency, usd: values.RRusd}
    let further_review = {local_currency: values.FRlocal_currency, usd: values.FRusd}
    let no_recovery = {local_currency: values.NRlocal_currency, usd: values.NRusd}
    const {description, unhcr_comments, partner_response, auditors_disagreement_comment } = values;
    let body = { 
      type: financialFinding.type,
      required_recovery,
      description,
      further_review,
      no_recovery,
      unhcr_comments,
      partner_response,
      auditors_disagreement_comment: auditors_disagreement_comment
    }; 
     body =await removeEmptyBody(body)
    if(Object.keys(body).length == 1) {
      handleClose();
    } else if(Object.keys(body).length > 1) {
    if(financialFinding.id) {
      return editFinancialReport(financialFinding.id, body).then(() => {
        handleClose();
        updateReport();
        successReducer("The report has been saved, Please click publish to make it available for everyone");
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response.data,
          _error: 'Report upload failed',
        });
      });
    }
    else {
      const mgmtId = report.id
      return editMgmtReports(id, mgmtId, {fin_findings: body}).then(() => {
        handleClose();
        updateReport();
        successReducer("The report has been saved, Please click publish to make it available for everyone");
      }).catch((error) => {
        postError(error, 'Report upload failed');
        throw new SubmissionError({
          ...error.response.data,
          _error: 'Report upload failed',
        });
      });
    }
    }
  }

  return (
    <React.Fragment>
      <Paper className={classes.root}>
        <ListLoader loading={loading}>
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
                <TableRow className={classes.tableRow}>
                    {messages.tableHeader.map((item, key) =>
                      <TableCell style={{width: item.width, borderRight: '1px solid #E9ECEF',}} align={item.align} key={key} className={classes.tableHeader} colSpan= {2}>
                        {(item.title)?
                          item.title
                        : null}
                      </TableCell>
                    )}
                  </TableRow>
                  <TableRow>  
                  {messages.subHeader.map((item, key) =>
                      <TableCell style={{width: item.width}} align={item.align} key={key} className={classes.tableSubHeader}>
                        {(item.title)?
                          item.title
                        : null}
                      </TableCell>
                    )}
                  </TableRow>
                </TableHead>
                {(items == undefined || items && items.length == 0 && !loading) ?
                  <div className={classes.noDataRow}>No data for the selected criteria</div> :
                  <TableBody>
                    {items && items.map((item, key) => (
                      <FinancialDetails
                        key={key}
                        hideButton={hideButton}
                        item={item}
                        expanded={expanded[key]}
                        handleExpand={() => handleExpand(key)}
                        handleEdit={() => handleClickOpen(item, EDIT)}
                      />
                    ))}
                    <TableRow>
                        <TableCell style={{textAlign: 'center', color: '#8898AA'}}>
                          Total
                        </TableCell>
                        <TableCell>
                        </TableCell>
                        <TableCell style={{textAlign: 'left', color: '#8898AA'}}>
                          {_.get(total,['required_recovery','total_local'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'right', color: '#8898AA'}}>
                          {_.get(total,['required_recovery','total_usd'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'left', color: '#8898AA'}}>
                          {_.get(total,['further_review','total_local'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'right', color: '#8898AA'}}>
                          {_.get(total,['further_review','total_usd'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'left', color: '#8898AA'}}>
                          {_.get(total,['no_recovery','total_local'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'right', color: '#8898AA'}}>
                          {_.get(total,['no_recovery','total_usd'], 0)}
                        </TableCell>
                        <TableCell style={{textAlign: 'right', color: '#8898AA'}}>
                          { report && report.fin_findings_calc && report.fin_findings_calc.total }$
                        </TableCell>
                    </TableRow>
                  </TableBody>
                }               
              </Table>
              {
                JSON.stringify(financialFinding) !== '{}' && (
                  <EditDialog 
                    open={open} 
                    handleClose={handleClose} 
                    financialFinding={financialFinding} 
                    initialValues={{
                      RRlocal_currency: financialFinding && financialFinding.required_recovery && financialFinding.required_recovery.local_currency,
                      RRusd: financialFinding && financialFinding.required_recovery && financialFinding.required_recovery.usd,
                      FRlocal_currency: financialFinding && financialFinding.further_review && financialFinding.further_review.local_currency,
                      FRusd: financialFinding && financialFinding.further_review && financialFinding.further_review.usd,
                      NRlocal_currency: financialFinding && financialFinding.no_recovery && financialFinding.no_recovery.local_currency,
                      NRusd: financialFinding && financialFinding.no_recovery && financialFinding.no_recovery.usd,
                      description: financialFinding && financialFinding.description && financialFinding.description,
                      partner_response: financialFinding && financialFinding.partner_response && financialFinding.partner_response,
                      unhcr_comments: financialFinding && financialFinding.unhcr_comments && financialFinding.unhcr_comments,
                      auditors_disagreement_comment: financialFinding && financialFinding.auditors_disagreement_comment && financialFinding.auditors_disagreement_comment
                    }}
                    onSubmit={handleSubmit}
                  />
                )
              }
            </div>
          </ListLoader>
        </Paper>
      </React.Fragment>
    )
  }

  const mapStateToProps = (state, ownProps) => ({
  })

  const mapDispatchToProps = (dispatch) => ({    
    editFinancialReport: (id, body) => dispatch(editFinancialReports(id, body)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'FinancialReport', message)),
    editMgmtReports: (id, mgmtId, body) => dispatch(editMgmtReports(id, mgmtId, body)),        
    reset: () => dispatch(reset('financialReportDialog')),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  })
  
  const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(CustomPaginationActionsTable)
  
  export default withRouter(connected)  