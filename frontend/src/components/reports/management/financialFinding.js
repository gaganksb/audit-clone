import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { loadIcqList } from '../../../reducers/projects/icqReducer/icqDetailedList';
import FinancialTable from './financialTable'
import { FINANCIAL_FINDINGS_TYPES } from '../../../helpers/constants';
import R from 'ramda'

const messages = {
  tableHeader: [
    { title: 'FINANCIAL FINDINGS', align: 'left', width: '20%' },
    { title: 'Require Recovery', width: '20%', align: 'center' },
    { title: 'Further Review', align: 'center', width: '20%', },
    { title: 'No Recovery Required', align: 'center', width: '20%', },
    { title: '', align: 'left', width: '20%', }
  ],
  subHeader: [
    { title: 'Type', align: 'left', width: '30%' },
    { title: '', align: 'left', width: '6%', },
    { title: 'Local Currency', align: 'left', width: '12%' },
    { title: 'US $', align: 'right', width: '12%' },
    { title: 'Local Currency', align: 'left', width: '12%' },
    { title: 'US $', align: 'right', width: '12%' },
    { title: 'Local Currency', align: 'left', width: '12%' },
    { title: 'US $', align: 'right', width: '12%' },
    { title: 'Total $', align: 'left', width: '12%' },
  ],
  error: 'Could not complete the action'
}


const FinancialReport = props => {
  const {
    loading,
    totalCount,
    query,
    id,
    icqDetails,
    financialFinding,
    icqDetailList,
    updateReport,
    hideButton
  } = props

  const [financialFindingData, updateFinancialFinding] = React.useState([])
  useEffect(() => {
    let finFinding = financialFinding.fin_findings
    let recoveryLocal = 0;
    let recoveryUsd = 0;
    let furtherLocal = 0;
    let furtherusd = 0;
    let noRecoveryLocal = 0;
    let noRecoveryUsd = 0
    const mergeById = FINANCIAL_FINDINGS_TYPES.map(itm => ({
      ...finFinding && finFinding.find((item) => (item.type === itm.type) && item),
      ...itm
    }));
    updateFinancialFinding(mergeById)

  }, [financialFinding.fin_findings])

  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <FinancialTable
        messages={messages}
        id={id}
        items={financialFindingData}
        loading={loading}
        report={financialFinding}
        total={financialFinding.fin_findings_calc}
        updateReport={updateReport}
        hideButton={hideButton}
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  icqDetails: state.icqDetailList.icq,
  totalCount: state.icqDetailList.totalCount,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
})

const mapDispatch = dispatch => ({
})

const connectedFinancialReport = connect(mapStateToProps, mapDispatch)(FinancialReport)
export default withRouter(connectedFinancialReport)