import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Paper from '@material-ui/core/Paper';
import CenteredTabs from '../../common/fullwidthTabs';
import Tooltip from '@material-ui/core/Tooltip';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Warning from '@material-ui/icons/Warning';
import { loadReportList } from '../../../reducers/reports/managementReports/getReports';
import { loadProject } from '../../../reducers/projects/projectsReducer/getProjectDetails';
import GeneralFile from './generalFile';
import FinancialReport from './financialFinding';
import MajorFindingReport from './majorFinding'
import OverallAssessment from './overallAssessment';
import Followup from './followUp';
import { editMgmtReports } from '../../../reducers/reports/managementReports/patchReport';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    detailsContainer: {
        marginTop: 40
    },
    button: {
        marginTop: theme.spacing(1),
        backgroundColor: '#EDB57E',
        minWidth: 94,
        borderRadius: 5,
        height: 30,
        marginLeft: 16,
        fontFamily: 'Open Sans',
        fontSize: 14,
        lineHeight: '12px',
        color: 'white',
        textTransform: 'capitalize',
        '&:hover': {
            backgroundColor: '#EDB57E'
        }
    },
    publish: {
        minWidth: 94,
        borderRadius: 5,
        height: 30,
        marginLeft: 16,
        fontFamily: 'Open Sans',
        fontSize: 14,
        lineHeight: '12px',
        color: 'white',
        textTransform: 'capitalize',
    },
    typography: {
        font: 'bold 14px/19px Roboto',
        color: '#606060',
        paddingLeft: 5
    },
    titleContainer: {
        padding: '15px 30px',
        minHeight: 'unset'
    },
    titleWrapper: {
        display: 'flex',
        alignItems: 'center',
        '& > svg': {
            width: 20,
            height: 20,
            cursor: 'pointer',
            marginRight: 15
        }
    },
    colorClass: {
        color: 'green',
        marginLeft: theme.spacing(2)
    },
    warning: {
        color: '#FFC400',
        marginLeft: theme.spacing(2)
    },
    title: {
        flexGrow: 1,
        color: '#606060',
        font: 'bold 16px/24px Roboto',
    }
}));

const MgmtReportTabs = props => {
    const classes = useStyles();
    const { report, project, loadReportList, pathName, loading, loadProjectDetails, editMgmtReports } = props;
    const [value, setValue] = useState(0);

    const ID = pathName.split('/').slice(-2)[1];
    const year = pathName.split('/').slice(-2)[0];

    useEffect(() => {
        loadProjectDetails(ID)
        loadReportList(ID)
    }, []);

    function updateReport() {
        loadReportList(ID);
    }

    const tabs = {
        items: [
            { label: 'SECTION 1' },
            { label: 'SECTION 2' },
            { label: 'SECTION 3' },
            { label: 'SECTION 4' },
            { label: 'SECTION 5' }
        ]
    };

    function handleChange(event, value) {
        setValue(value);
    }

    const handlePublish = () => {
        editMgmtReports(report.agreement, report.id, {published: true}).then(res=>{
            loadProjectDetails(ID)
            loadReportList(ID)
        })
    }

    return (
        <div>
            <Paper className={`${classes.root} ${classes.titleContainer}`}>
                <div className={classes.titleWrapper}>
                    <svg onClick={() => window.history.back()} fill="#0072bc" version="1.1" id="Capa_1" x="0px" y="0px" width="459px" height="459px" viewBox="0 0 459 459" style={{ enableBackground: 'new 0 0 459 459' }} xmlSpace="preserve">
                        <g><g id="reply"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" /></g></g>
                    </svg>
                    <Typography className={classes.title} >MANAGEMENT LETTER - {project.title}</Typography>
                    {(!report.published) &&
                        <CustomizedButton
                            buttonText={"Publish"}
                            buttonType={buttonType.submit}
                            disabled={report && report.can_be_published == true ? false : true}
                            clicked={handlePublish}
                            enableTooltip={(!report.published && report.can_be_published === false) ? true: false}
                            tooltipText={"Please complete section 1 and section 2 of management letter to enable pushlish button"}
                        />
                    }
                    {(report.published) &&
                        <CustomizedButton
                            buttonText={"Export"}
                            buttonType={buttonType.submit}
                            clicked={() => window.open(`/api/icq/export-mgmt-letter/${ID}/`, "_blank")}
                            enableTooltip={true}
                            tooltipText={"Export management letter"}
                        />
                    }
                    {(report.published) ? <Tooltip title='Published'>
                        <CheckCircle className={classes.colorClass}>
                            <circle cx="12" cy="12" r="8" />
                        </CheckCircle>
                    </Tooltip> :
                        <Tooltip title='Unpublished'>
                            <Warning className={classes.warning}>
                                <circle cx="12" cy="12" r="8" />
                            </Warning>
                        </Tooltip>}
                </div>
            </Paper>
            <Paper className={`${classes.root} ${classes.detailsContainer}`}>
                <div>
                    <CenteredTabs tabs={tabs} handleChange={handleChange} value={value} />
                    {value === 0 && <GeneralFile report={report} updateReport={updateReport} loading={loading} id={ID} />}
                    {value === 1 && <FinancialReport financialFinding={report} updateReport={updateReport} loading={loading} id={ID} />}
                    {value === 2 && <MajorFindingReport updateReport={updateReport} loading={loading} year={year} id={ID} />}
                    {value === 3 && <OverallAssessment overallAssessment={report} updateReport={updateReport} loading={loading} id={ID} />}
                    {value === 4 && <Followup report={report} updateReport={updateReport} loading={loading} id={ID} />}
                </div>
            </Paper>
        </div>
    )
};

const mapStateToProps = (state, ownProps) => ({
    project: state.projectDetails.project,
    pathName: ownProps.location.pathname,
    report: state.managementReport.report,
    loading: state.managementReport.loading,
});

const mapDispatch = dispatch => ({
    loadReportList: (ID, params) => dispatch(loadReportList(ID, params)),
    loadProjectDetails: id => dispatch(loadProject(id)),
    editMgmtReports: (id, mgmtId, body) => dispatch(editMgmtReports(id, mgmtId, body))
});

const connectedProjectOverview = connect(mapStateToProps, mapDispatch)(MgmtReportTabs);
export default withRouter(connectedProjectOverview);