import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import MajorFindingTable from './majorFindingTable'
import {loadIcqList} from '../../../reducers/projects/icqReducer/icqDetailedList';
import {loadMajorReportList} from '../../../reducers/reports/managementReports/getMajorFinding';

const messages = {
  heading: 'MAJOR FINDINGS RELATED TO THE MANAGEMENT CONTROL AND COMPLIANCE WITH THE TERMS OF AGREEMENT',
  tableHeader: [
    { title: '', align: 'right', width: '20%', },
    { title: 'ICQ Key', align: 'left',  width: '20%'},
    { title: 'Process', width: '20%', align: 'left'},
    { title: 'Criticality (High, Medium, Low)', align: 'left', width: '20%', },
    { title: '', align: 'right', width: '20%', }
  ],
    error: 'Could not complete the action',
    add: 'Add Major Finding'
  }


  const MajorFindingReport = props => {
    const {
      loading,
      query,
      icqDetailList,
      icqDetails,
      year,
      id,
      majorFinding,
      pathName,
      loadMajorReportList,
      updateReport,
      hideButton
    } = props
  
      
  useEffect(() => {
    loadMajorReportList(id)
}, [])

    return (
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
       <MajorFindingTable
          messages={messages}
          items={majorFinding}
          loading={loading}
          year={year}
          updateReport={updateReport}
          loadMajorReportList={loadMajorReportList}
          mgmtLetterId={id}
          hideButton={hideButton}
        />
      </div>
    )
  }
  
  const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    icqDetails: state.icqDetailList.icq,
    totalCount: state.icqDetailList.totalCount,
    majorFinding: state.majorFinding.majorFindingreport,
    location: ownProps.location,
    pathName: ownProps.location.pathname,
  })
  
  const mapDispatch = dispatch => ({
    icqDetailList: (id, year, params) => dispatch(loadIcqList(id, year, params)),
    loadMajorReportList:(id, params) => dispatch(loadMajorReportList(id, params))
  })
  
  const connectedMajorFindingReport = connect(mapStateToProps, mapDispatch)(MajorFindingReport)
  export default withRouter(connectedMajorFindingReport)