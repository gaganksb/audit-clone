import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    tableCell: {
        font: '12px/16px Roboto',
        letterSpacing: 0,
        color: '#8898AA'
    },
    icon: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    detailHeader: {
        font: '16px/16px Roboto',
        letterSpacing: 0,
        color: '#567393',
        fontWeight: 500,
        // minWidth: 1000,
        paddingTop: "32px",
        marginLeft: "16px",
        marginBottom: "5px"
    },
    details: {
        font: '14px/14px Roboto',
        letterSpacing: 0,
        color: '#8898AA',
        minWidth: 400,
        fontWeight: 300,
        // marginTop: "8px",
        marginLeft: "16px",
        position: 'absolute',
        paddingBottom: 8
        // marginBottom: "30px",
    },
}));

const messages = {
    detailHeaders: [
        'Description',
        'Partner Reponse',
        'UNHCR Comments',
        'Auditor\'s Comment (in case of disagreement)'
    ]
}

function FinancialDetails(props) {
    const {
        expanded,
        handleExpand,
        item,
        handleEdit,
        session,
        hideButton
    } = props;

    const classes = useStyles();
    const totalRow = parseInt(_.get(item, ['required_recovery', 'usd'], 0)) + parseInt(_.get(item, ['further_review', 'usd'], 0)) + parseInt(_.get(item, ['no_recovery', 'usd'], 0))

    return (
        <React.Fragment>
            <TableRow >
                <TableCell className={classes.tableCell} component="th" scope="row">
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <span style={{ marginRight: 16 }}>
                                {(expanded) ?
                                    <ExpandLess className={classes.icon} onClick={handleExpand()} /> :
                                    <ExpandMore className={classes.icon} onClick={handleExpand()} />
                                }
                            </span>
                            <span>
                                {_.get(item, 'value', 'N/A')}
                            </span>
                        </div>
                        </div>
                        
                </TableCell>
                
                <TableCell>
                    {!hideButton ? <div>
                        {
                            session &&
                            session.user_type !== "HQ" &&
                            <div >
                                <Tooltip title={"Edit"}>
                                    <EditIcon onClick={handleEdit} style={{cursor: 'pointer'}} />
                                </Tooltip>
                            </div>
                        }
                    </div> : null}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, ['required_recovery', 'local_currency'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                    {_.get(item, ['required_recovery', 'usd'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, ['further_review', 'local_currency'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                    {_.get(item, ['further_review', 'usd'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, ['no_recovery', 'local_currency'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                    {_.get(item, ['no_recovery', 'usd'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                    {totalRow + '$'}
                </TableCell>
            </TableRow>
            {(expanded) ?
                <TableRow>
                    <td container>
                        <div className={classes.detailHeader}>
                            {"Type"}
                        </div>
                        <div className={classes.details}>
                            {_.get(item, 'value', 'N/A')}
                        </div>
                        <div className={classes.detailHeader}>
                            {messages.detailHeaders[0]}
                        </div>
                        <div className={classes.details}>
                            {_.get(item, 'description', 'N/A')}
                        </div>
                        <div className={classes.detailHeader}>
                            {messages.detailHeaders[1]}
                        </div>
                        <div className={classes.details}>
                            {_.get(item, 'partner_response', 'N/A')}
                        </div>
                        <div className={classes.detailHeader}>
                            {messages.detailHeaders[2]}
                        </div>
                        <div className={classes.details}>
                            {_.get(item, 'unhcr_comments', 'N/A')}
                        </div>
                        <div className={classes.detailHeader}>
                            {messages.detailHeaders[3]}
                        </div>
                        <div className={classes.details}>
                            {_.get(item, 'auditors_disagreement_comment', 'N/A')}
                        </div>
                    </td>
                </TableRow> : null}
        </React.Fragment>
    );
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname,
    session: state.session
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FinancialDetails);

export default withRouter(connected);