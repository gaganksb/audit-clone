import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import ListLoader from '../../common/listLoader'
import MajorDetails from './majorFindingDetails'
import { reset } from 'redux-form';
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import EditDialog from './editMajorFinding'
import DeleteDialog from './deleteDialog'
import { SubmissionError } from 'redux-form';
import { deleteMajorFindingRequest } from '../../../reducers/reports/managementReports/deleteMajorFinding';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import { editICQ } from '../../../reducers/reports/managementReports/editICQ';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  customizedButton: {
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    position: 'absolute',
    right: 38,
    marginTop: -6,
    '&:hover': {
      backgroundColor: '#1F80C5'
    }
  },
  table: {
    minWidth: 500,
  },
  noDataRow: {
    margin: 16,
    width: 'inherit',
    marginLeft: '139%',
    textAlign: 'center'
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableHeader: {
    color: '#8898AA',
    fontFamily: 'Roboto',
    fontSize: 12,
  },
  tableTitle: {
    padding: 20,
    fontWeight: 'bold'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '12px/16px Roboto',
    letterSpacing: 0,
    color: '#8898AA'
  },
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#2B72B6',
    font: 'bold 14px/24px Roboto',
    letterSpacing: 0,
    paddingBottom: 20,
    display: 'flex'
  },
  fab: {
    margin: theme.spacing(1),
    backgroundColor: '#1F80C5'
  },
}))

const EDIT = 'EDIT';
const DELETE = 'DELETE';
const ADD = 'ADD';

const rowsPerPage = 200

function CustomPaginationActionsTable(props) {
  const {
    items,
    loading,
    updateReport,
    messages,
    icqDetails,
    reset,
    deleteMajorFinding,
    editICQ,
    loadMajorReportList,
    mgmtLetterId,
    session,
    hideButton
  } = props



  const initialExpanded = () => {
    let res = []
    for (let i = 0; i < rowsPerPage; i++) {
      res.push(false)
    }
    return res
  }

  const expandRow = (row) => {
    let res = []
    for (let i = 0; i < expanded.length; i++) {
      if (i == row) {
        res.push(!expanded[i])
      } else {
        res.push(expanded[i])
      }
    }
    return res
  }


  const classes = useStyles()
  const [open, setOpen] = React.useState({ status: false, type: null })
  const [majorFinding, setMajorFinding] = React.useState({})
  const [expanded, setExpanded] = React.useState(initialExpanded())
  const selectedEntry = 'Major Finding'

  useEffect(() => {
    setExpanded(initialExpanded())
  }, [items]);

  function handleClickOpen(item, action) {
    setMajorFinding(item)
    setOpen({ status: true, type: action })
    reset()
  }

  function handleClose() {
    setOpen({ status: false, type: null })
  }

  const handleExpand = (key) => () => {
    setExpanded(expandRow(key))
  }

  function handleDelete() {
    const id = majorFinding.id;

    return deleteMajorFinding(id).then(() => {
      handleClose();
      updateReport();
    }).catch((error) => {
      throw new SubmissionError({
        ...error.response.data,
        _error: 'Unable to delete entry',
      });
    });
  }

  const handleSubmit = (data) => {
    
    let body = {}
    Object.keys(data).forEach(item=>{
      if(session && session.user_type === "FO"){
        body['unhcr_comments'] = data.unhcr_comments
      } else if (session && session.user_type === "AUDITOR"){
        body['auditors_disagreement_comment'] = data.auditors_disagreement_comment
        body['risk'] = data.risk
        body['mgmt_letter_finding'] = data.mgmt_letter_finding
        body['auditor_recommendation'] = data.auditor_recommendation
      } else if(session && session.user_type === "PARTNER"){
        body['partner_response'] = data.partner_response
      }
    })
    
    editICQ(data.id, body).then(res=>{
      loadMajorReportList(mgmtLetterId)
      handleClose()
    })
  }

  return (
    <React.Fragment>
      <Paper className={classes.root}>
        <span className={classes.heading}>
          {messages.heading}
        </span>
        <ListLoader loading={loading}>
          <div className={classes.tableWrapper}>
            <Table className={classes.table}>
              <TableHead className={classes.headerRow}>
                <TableRow >
                  {messages.tableHeader.map((item, key) =>
                    <TableCell style={{ width: item.width }} align={item.align} key={key} className={classes.tableHeader} >
                      {(item.title) ?
                        item.title
                        : null}
                    </TableCell>
                  )}
                </TableRow>
              </TableHead>
              {(items && items.length == 0) ?
                <div className={classes.noDataRow}>No data for the selected criteria</div> :
                <TableBody>
                  {items && items.length > 0 && items.map((item, key) => (
                    <MajorDetails
                      hideButton={hideButton}
                      key={key}
                      item={item}
                      expanded={expanded[key]}
                      handleExpand={() => handleExpand(key)}
                      handleEdit={() => handleClickOpen(item, EDIT)}
                      handleDelete={() => handleClickOpen(item, DELETE)}
                    />
                  ))}
                </TableBody>
              }
            </Table>
            {session && session.user_type !== "HQ" ? <EditDialog
              open={open.type === EDIT || open.type === ADD && open.status === true}
              handleClose={handleClose}
              majorFinding={majorFinding}
              onSubmit={handleSubmit}
            /> : null}
            <DeleteDialog open={open.type === DELETE && open.status === true} handleClose={handleClose} selectedEntry={selectedEntry} handleDelete={handleDelete} />
          </div>
        </ListLoader>
      </Paper>
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  session: state.session
})

const mapDispatchToProps = (dispatch) => ({
  deleteMajorFinding: (id) => dispatch(deleteMajorFindingRequest(id)),
  editICQ: (id, body)=>dispatch(editICQ(id, body)),    
  reset: () => dispatch(reset('majorReportDialog')),

})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)  