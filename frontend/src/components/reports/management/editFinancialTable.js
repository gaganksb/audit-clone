import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import { renderTextArea, renderTextField, textArea } from '../../common/renderFields';
import _ from 'lodash'

const title = {
    heading: "MANAGEMENT LETTER - SECTION 2",
    type: "TYPE",
    requireRecovery: "Require Recovery",
    furtherReview: "Further Review",
    noRecovery: "No Recovery Required",
    description: 'Description',
    localCurrency: "Local Currency",
    usd: "US $",
    partner: "Partner Response",
    unhcr: "UNHCR Representation comments",
    audit: "Auditor's comment (in case of disagreement)"
}
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3)
    },
    container: {
        display: 'flex',
        width: '100%'
    },
    subText: {
        marginTop: theme.spacing(2),
        font: 'bold 14px/19px Roboto',
        color: '#0072bc'
    },
    buttonGrid: {
        margin: theme.spacing(1),
        marginRight: 0,
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54
    },
    textField: {
        margin: '16px 16px 16px 0px',
        minWidth: '48%',
    },
    dialogTitle: {
        font: 'bold 18px/24px Roboto',
        color: '#606060',

    },

    label: {
        fontSize: '12px',
        color: '#757582'
    },
    value: {
        fontSize: '16px',
        color: 'black'
    },
    grid: {
        display: 'inline-block',
        width: '90%'
    }
}));

const validate = values => {
    const errors = {}
    const requiredFields = [
        'RRlocal_currency',
        'RRusd',
        'FRlocal_currency',
        'FRusd',
        'NRlocal_currency',
        'NRusd',
        'description'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
        else if (field == 'RRlocal_currency' || field == 'RRusd' || field == 'FRlocal_currency' || field == 'FRusd' || field == 'NRlocal_currency' || field == 'NRusd') {
            if (isNaN(values[field])) {
                errors[field] = 'Should be a numeric value'
            }
        }
    })
    return errors
}

function ReportDialog(props) {
    const classes = useStyles();
    const { handleSubmit, handleClose, open, financialFinding, session } = props;

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth
                maxWidth={'md'}
            >
                <DialogTitle className={classes.dialogTitle}>{title.heading}</DialogTitle>
                <Divider />
                <DialogContent>
                    <form className={classes.container} onSubmit={handleSubmit}>
                        <div style={{ width: '100%' }}>
                            <div>
                                <p className={classes.label}>{title.type}</p>
                                <p className={classes.value}>{financialFinding.type}</p>
                            </div>
                            <div className={classes.subText}>
                                {title.requireRecovery}
                            </div>
                            <Grid item sm={6} className={classes.grid}>
                                {session && session.user_type === 'AUDITOR' ? <Field
                                    name='RRlocal_currency'
                                    label={title.localCurrency}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.required_recovery && financialFinding.required_recovery.local_currency}
                                    fullWidth={false}
                                    className={classes}
                                    style={{ paddingRight: 32 }}
                                />: <div>
                                <p className={classes.label}>{title.localCurrency}</p>
                                <p className={classes.value}>{_.get( financialFinding,['required_recovery','local_currency'], 'N/A')}</p>
                            </div>}
                            </Grid>
                            <Grid item sm={6} className={classes.grid}>
                                {session && session.user_type === 'AUDITOR' ? <Field
                                    name='RRusd'
                                    label={title.usd}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.required_recovery && financialFinding.required_recovery.usd}
                                    fullWidth={false}
                                    className={classes}
                                    style={{ marginLeft: 16, paddingRight: 15 }}
                                />: <div>
                                <p className={classes.label}>{title.usd}</p>
                                <p className={classes.value}>{_.get( financialFinding,['required_recovery','usd'], 'N/A')}</p>
                            </div>
                                }
                            </Grid>
                            <div className={classes.subText}>
                                {title.furtherReview}
                            </div>
                            <Grid item sm={6} className={classes.grid}>
                                {session && session.user_type === 'AUDITOR' ? <Field
                                    name='FRlocal_currency'
                                    label={title.localCurrency}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.further_review && financialFinding.further_review.local_currency}
                                    fullWidth={false}
                                    className={classes}
                                    style={{ paddingRight: 32 }}
                                />: <div>
                                <p className={classes.label}>{title.localCurrency}</p>
                                <p className={classes.value}>{_.get( financialFinding,['further_review-','localCurrency'], 'N/A')}</p>
                            </div>}
                            </Grid>
                            <Grid item sm={6} className={classes.grid}>
                            {session && session.user_type === 'AUDITOR' ? <Field
                                    name='FRusd'
                                    label={title.usd}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.further_review && financialFinding.further_review.usd}
                                    fullWidth={false}
                                    className={classes}
                                    style={{ marginLeft: 16, paddingRight: 15 }}
                                /> :<div>
                                <p className={classes.label}>{title.usd}</p>
                                <p className={classes.value}>{_.get(financialFinding,['further_review-','usd'], 'N/A')}</p>
                            </div>
                            }</Grid>
                            <div className={classes.subText}>
                                {title.noRecovery}
                            </div>
                            <Grid item sm={6} className={classes.grid}>
                                {session && session.user_type === 'AUDITOR' ? <Field
                                    name='NRlocal_currency'
                                    label={title.localCurrency}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.no_recovery && financialFinding.no_recovery.local_currency}
                                    className={classes}
                                    style={{ paddingRight: 32 }}
                                />: <div>
                                <p className={classes.label}>{title.localCurrency}</p>
                                <p className={classes.value}>{_.get(financialFinding,['no_recovery','localCurrency'], 'N/A')}</p>
                            </div>}
                            </Grid>
                            <Grid item sm={6} className={classes.grid}>
                                {session && session.user_type === 'AUDITOR' ? <Field
                                    name='NRusd'
                                    label={title.usd}
                                    margin='normal'
                                    component={renderTextField}
                                    defaultValue={financialFinding && financialFinding.no_recovery && financialFinding.no_recovery.usd}
                                    fullWidth={false}
                                    className={classes}
                                    style={{ marginLeft: 16, paddingRight: 15 }}
                                />: <div>
                                <p className={classes.label}>{title.usd}</p>
                                <p className={classes.value}>{_.get(financialFinding,['no_recovery','usd'], 'N/A')}</p>
                            </div>}
                            </Grid>
                            {session && session.user_type === 'AUDITOR' ? <div><div className={classes.subText}>
                                {title.description}
                            </div>
                            <Field
                                name='description'
                                component={textArea}
                                defaultValue={financialFinding && financialFinding.description}
                            /></div>: 
                            <div>
                                <p className={classes.label}>{title.description}</p>
                                <p className={classes.value}>{_.get(financialFinding,'description', 'N/A')}</p>
                            </div>
                        }
                            {session && session.user_type === "PARTNER" && <div>
                                <div className={classes.subText}>
                                    {title.partner}
                                </div>
                                <Field
                                    name='partner_response'
                                    component={textArea}
                                    defaultValue={financialFinding && financialFinding.partner_response}
                                />
                            </div>}
                            {session && session.user_type === "FO" && <div>
                                <div className={classes.subText}>
                                    {title.unhcr}
                                </div>
                                <Field
                                    name='unhcr_comments'
                                    component={textArea}
                                    defaultValue={financialFinding && financialFinding.unhcr_comments}
                                />
                            </div>}
                            {session && session.user_type === "AUDITOR" && <div>
                                <div className={classes.subText}>
                                    {title.audit}
                                </div>
                                <Field
                                    name='auditors_disagreement_comment'
                                    component={textArea}
                                    defaultValue={financialFinding && financialFinding.auditors_disagreement_comment}
                                />
                            </div>}
                            <div className={classes.buttonGrid}>
                                <CustomizedButton
                                    clicked={handleClose}
                                    buttonText={"Cancel"}
                                    buttonType={buttonType.cancel} />
                                <CustomizedButton
                                    buttonText={"Save"}
                                    type="submit"
                                    buttonType={buttonType.submit} />
                            </div>
                        </div>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const reportDialogForm = reduxForm({
    form: 'financialReportDialog',
    validate,
    enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
    let initialValues = {
        RRlocal_currency: ownProps.financialFinding && ownProps.financialFinding.required_recovery && ownProps.financialFinding.required_recovery.local_currency,
        RRusd: ownProps.financialFinding && ownProps.financialFinding.required_recovery && ownProps.financialFinding.required_recovery.usd,
        FRlocal_currency: ownProps.financialFinding && ownProps.financialFinding.further_review && ownProps.financialFinding.further_review.local_currency,
        FRusd: ownProps.financialFinding && ownProps.financialFinding.further_review && ownProps.financialFinding.further_review.usd,
        NRlocal_currency: ownProps.financialFinding && ownProps.financialFinding.no_recovery && ownProps.financialFinding.no_recovery.local_currency,
        NRusd: ownProps.financialFinding && ownProps.financialFinding.no_recovery && ownProps.financialFinding.no_recovery.usd,
        description: ownProps.financialFinding && ownProps.financialFinding.description,
        partner_response: ownProps.financialFinding && ownProps.financialFinding.partner_response,
        unhcr_comments: ownProps.financialFinding && ownProps.financialFinding.unhcr_comments,
        auditors_disagreement_comment: ownProps.financialFinding && ownProps.financialFinding.auditors_disagreement_comment
        
    }
    return {
        session: state.session,
        initialValues
    }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);