import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import Paper from '@material-ui/core/Paper'
import ListLoader from '../../common/listLoader'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import _ from 'lodash';
import EditDialog from './editOverallAssessment';
import DeleteIcon from '@material-ui/icons/Delete';
import DeleteDialog from './deleteDialog';
import CustomizedButton from '../../common/customizedButton';
import { buttonType, Process } from '../../../helpers/constants';
import { SubmissionError } from 'redux-form';
import {deleteOverallAssessmentRequest} from '../../../reducers/reports/managementReports/deleteOverallAssessment';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    table: {
        minWidth: 500,
    },
    noDataRow: {
        margin: 16,
        width: 'inherit',
        marginLeft: '139%',
        textAlign: 'center'
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    tableHeader: {
        color: '#8898AA',
        fontFamily: 'Roboto',
        fontSize: 12,
    },    
    icon: {
        margin: theme.spacing(2),
        '&:hover': {
            cursor: 'pointer'
        }
    },
    headerRow: {
        backgroundColor: '#F6F9FC',
        border: '1px solid #E9ECEF',
        height: 60,
        font: 'medium 12px/16px Roboto'
    },
    tableCell: {
        font: '12px/16px Roboto',
        letterSpacing: 0,
        color: '#8898AA'
    },
    heading: {
        paddingLeft: 22,
        paddingTop: 19,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#2B72B6',
        font: 'bold 14px/24px Roboto',
        letterSpacing: 0,
        paddingBottom: 20,
        display: 'flex'
    },
    fab: {
        margin: theme.spacing(1),
        backgroundColor: '#1F80C5'
    },
    customizedButton: {
      backgroundColor: '#1F80C5',
      color: 'white',
      textTransform: 'capitalize',
      borderRadius: 5,
      height: 30,
      minWidth: 100,
      fontFamily: 'Open Sans',
      lineHeight: '12px',
      position: 'absolute',
      right: 38,
      marginTop: -6,
      '&:hover': {
        backgroundColor: '#1F80C5'
      }
    },
}))

const EDIT = 'EDIT'
const DELETE = 'DELETE'
const ADD = 'ADD'
const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
    const {
        items,
        loading,
        messages,
        updateReport,
        deleteOverallAssessment
    } = props

    const classes = useStyles()
    const [open, setOpen] = React.useState({ status: false, type: null })
    const [overallAssessment, setOverallAssessmen] = React.useState({})
    const selectedEntry = 'Overall Assessment'

    function handleClickOpen(item, action) {
        setOverallAssessmen(item)
        setOpen({ status: true, type: action })
    }

    function handleClose() {
        setOpen({ status: false, type: null })
    }

    function handleDelete() {
        const id = overallAssessment.id;
       
        return deleteOverallAssessment(id).then(() => {
          handleClose();
          updateReport();
        }).catch((error) => {
          throw new SubmissionError({
            ...error.response.data,
            _error: 'Unable to delete entry',
          });
        });
      }

    return (
        <React.Fragment>
            <Paper className={classes.root}>
            <span className={classes.heading}>
                {messages.heading}
            </span>
                <ListLoader loading={loading}>
                    <div className={classes.tableWrapper}>
                        <Table className={classes.table}>
                            <TableHead className={classes.headerRow}>
                                <TableRow >
                                    {messages.tableHeader.map((item, key) =>
                                        <TableCell style={{ width: item.width }} align={item.align} key={key} className={classes.tableHeader} >
                                            {(item.title) ?
                                                item.title
                                                : null}
                                        </TableCell>
                                    )}
                                </TableRow>
                            </TableHead>
                            {(items == undefined || items && items.length == 0 && !loading) ?
                                <div className={classes.noDataRow}>No data for the selected criteria</div> :
                                <TableBody>
                                    {items && items[0] && items[0].overall_rating && Object.keys(items[0].overall_rating).map((item, key) => (
                                        <TableRow key={key}>
                                            <TableCell className={classes.tableCell}>{item}</TableCell>
                                            <TableCell className={classes.tableCell}>{Process[item]}</TableCell>
                                            <TableCell className={classes.tableCell}>{items[0].overall_rating[item]}</TableCell>
                                        </TableRow>
                                    ))}
                                    {items.length > 0 && <TableRow >
                                        <TableCell className={classes.tableCell}>
                                            <strong>
                                                Overall Assessment
                                            </strong>
                                        </TableCell>
                                        <TableCell className={classes.tableCell}>{""}</TableCell>
                                        <TableCell className={classes.tableCell}>
                                            <strong>{items.length > 0 && items[0].icq_report && items[0].icq_report.overall_score_perc }  ({items.length > 0 && items[0].overall_assessment}</strong>)
                                        </TableCell>
                                    </TableRow>}
                                </TableBody>
                            }

                        </Table>
                        <EditDialog open={open.type === EDIT || open.type === ADD && open.status === true} handleClose={handleClose} overallAssessment={overallAssessment} />
                        <DeleteDialog open={open.type === DELETE && open.status === true} handleClose={handleClose} selectedEntry={selectedEntry} handleDelete={handleDelete} />
                    </div>
                </ListLoader>
            </Paper>
        </React.Fragment>
    )
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query
})

const mapDispatchToProps = (dispatch) => ({
    deleteOverallAssessment: (id) => dispatch(deleteOverallAssessmentRequest(id)),
})

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(CustomPaginationActionsTable)

    export default withRouter(connected)  