import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import { buttonType, icqAttribute } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import { renderSelectField, renderTextField, textArea } from '../../common/renderFields';

const title = {
    heading: "MANAGEMENT LETTER - SECTION 5",
    icq: "ICQ control attribute",
    recommendation: "Previous Recommendation",
    criticality: "Criticality",
    summary: "Summary",
    recommend: "Recommendation Implemented"
}
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3)
    },
    container: {
        display: 'flex',
        width: '100%'
    },
    subText: {
        marginTop: theme.spacing(2),
        font: 'bold 14px/19px Roboto',
        color: '#0072bc'
    },
    formControl: {
        minWidth: 100,
        paddingRight: theme.spacing(2),
        width: '100%',
        color: '#606060',
        marginTop: 1,
        paddingTop: 0,
        paddingBottom: 0
      },
      menu: {
        padding: 8,
        '&.Mui-selected': {
          backgroundColor: '#E5F1F8',
          borderStyle: 'hidden'
        },
        '&:hover': {
          backgroundColor: '#E5F1F8',
          borderStyle: 'hidden'
        }
      },
    buttonGrid: {
        margin: theme.spacing(1),
        marginRight: 0,
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54
    },
    textField: {
        margin: '16px 16px 16px 0px',
        minWidth: '48%',
    },
    dialogTitle: {
        font: 'bold 18px/24px Roboto',
        color: '#606060',

    },
    
    label: {
        fontSize: '12px',
        color: '#757582'
     },
    value: {
     fontSize: '16px',
     color: 'black'
    },
    grid: {
        display: 'inline-block',
        width: '90%'
    }
}));

const validate = values => {
    const errors = {}
    const requiredFields = [
        'control_attribute',
        'prev_recommendation',
        'criticality',
        'summary',
        'recommendation'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
       
    })
    return errors
}

function ReportDialog(props) {
    const classes = useStyles();
    const { handleSubmit, handleClose, open, followUp, key } = props;
    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth
                maxWidth={'md'}
            >
                <DialogTitle className={classes.dialogTitle}>{title.heading}</DialogTitle>
                <Divider />
                <DialogContent>
                    <form className={classes.container} onSubmit={handleSubmit}>
                        <div style={{ width: '100%' }}>
                        
                        <InputLabel className={classes.subText}>{title.icq}</InputLabel>
                            <Field
                                name="control_attribute"
                                className={classes.formControl}
                                key={key}
                                component={renderSelectField}
                            >
                                {
                                    icqAttribute.map((item, index) => {
                                        return (<option className={classes.menu} key={item.key} value={item.key}>{item.value}</option>)
                                    })
                                }
                            </Field>
                            <div className={classes.subText}>
                                {title.recommendation}
                            </div>                            
                            <Field
                                name='prev_recommendation'
                                component={textArea}
                                defaultValue= {followUp && followUp.prev_recommendation}
                            />
                            <InputLabel className={classes.subText}>{title.criticality}</InputLabel>
                            <Field
                                name="criticality"
                                className={classes.formControl}
                                key={key}
                                component={renderSelectField}
                            >
                                {
                                    [{key:'high', value: 'High'}, {key: 'low', value: 'Low'}, {key: 'medium', value: 'Medium'}].map((item, index) => {
                                        return (<option className={classes.menu} key={item.key} value={item.key}>{item.value}</option>)
                                    })
                                }
                            </Field>
                            <div className={classes.subText}>
                                {title.summary}
                            </div>
                            <Field
                                name='summary'
                                component={textArea}
                                defaultValue= {followUp && followUp.summary}
                            />
                            <div className={classes.subText}>
                                {title.recommend}
                            </div>
                            <Field
                            name='recommendation'
                            component={textArea}
                            defaultValue= {followUp && followUp.recommendation}
                            />
                            <div className={classes.subText}>
                                {title.audit}
                            </div>

                            <div className={classes.buttonGrid}>
                                <CustomizedButton
                                    clicked={handleClose}
                                    buttonText={"Cancel"}
                                    buttonType={buttonType.cancel} />
                                <CustomizedButton
                                    buttonText={"Save"}
                                    type="submit"
                                    buttonType={buttonType.submit} />
                            </div>
                        </div>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const reportDialogForm = reduxForm({
    form: 'financialReportDialog',
    validate,
    enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
    let initialValues = ownProps.followUp
    return {
        initialValues
    }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);