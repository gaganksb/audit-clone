import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import OverallAssessmentTable from './overallAssessmentTable'
import { loadOverallReportList, loadOverallReportListExp } from '../../../reducers/reports/managementReports/getOverallRating'

const messages = {
  heading: 'OVERALL ASSESSMENT OF PARTNER\'S FINANCIAL MANAGEMENT CAPACITY FINDINGS',
  tableHeader: [
    { title: 'ICQ Key', align: 'left', width: '25%' },
    { title: 'Process', width: '40%', align: 'left' },
    { title: 'ICQ Overall Score', align: 'left', width: '35%', },
  ],
  error: 'Could not complete the action',
  add: 'Add Overall Assessment'
}

const MajorFindingReport = props => {
  const {
    loading,
    query,
    id,
    overallAssessment,
    loadOverallReportList,
    overallReport,
    updateReport,
    hideButton,
    loadOverallReportListExp
  } = props

  useEffect(() => {
    if(hideButton){
      loadOverallReportListExp(id, {})
    }else{
      loadOverallReportList(id, {})
    }
  }, [])

  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <OverallAssessmentTable
        messages={messages}
        items={overallReport}
        loading={loading}
        updateReport={updateReport}
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  location: ownProps.location,
  overallReport: state.overallFinding.overallreport,
  pathName: ownProps.location.pathname,
})

const mapDispatch = dispatch => ({
  loadOverallReportList: (id, params) => dispatch(loadOverallReportList(id, params)),
  loadOverallReportListExp: (id, params) => dispatch(loadOverallReportListExp(id, params))
  
})

const connectedMajorFindingReport = connect(mapStateToProps, mapDispatch)(MajorFindingReport)
export default withRouter(connectedMajorFindingReport)