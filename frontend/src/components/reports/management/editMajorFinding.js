import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import { renderTextArea, renderTextField, renderSelectField } from '../../common/renderFields';
import majorFinding from './majorFinding';

const title = {
  heading: "MANAGEMENT LETTER - SECTION 3",
  process: "Process",
  criticality: "Criticality",
  description: 'Description',
  risk: "Risk",
  key: 'ICQ Key',
  auditorRecommendation: "Auditor\'s Recommendation",
  auditorDissComment: "Auditor\'s Comment (in case of disagreement)",
  partnerComments: "Partner Response",
  UNHCRComments: "UNHCR Representation Comments",
  
}
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    marginTop: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#0072bc'
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#1F80C5'
    }
  },  
  label: {
    fontSize: '12px',
    color: '#757582'
},
value: {
    fontSize: '16px',
    color: 'black'
},
  textField: {
    margin: '16px 16px 16px 0px',
    minWidth: '48%',
  },
  dialogTitle: {
    font: 'bold 18px/24px Roboto',
    color: '#606060',

  }
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'partner_response',
    'unhcr_comments',
    // 'auditors_disagreement_comment'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

function ReportDialog(props) {
  const classes = useStyles();
  const { handleSubmit, handleClose, open, session, majorFinding } = props;
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'sm'}
      >
        <DialogTitle className={classes.dialogTitle}>{title.heading}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
            <div style={{ width: '100%' }}>
              <div>
                <p className={classes.label}>{title.key}</p>
                <p className={classes.value}>{majorFinding && majorFinding.key_control_attr && majorFinding.key_control_attr.title}</p>
              </div>
              <div>
                <p className={classes.label}>{title.process}</p>
                <p className={classes.value}>{majorFinding && majorFinding.process_title && majorFinding.process_title.title}</p>
              </div>
              <div>
                <p className={classes.label}>{title.criticality}</p>
                <p className={classes.value}>{majorFinding && majorFinding.risk_criticality}</p>
              </div>
              <div className={classes.subText}>
                {session && session.user_type === "PARTNER" && title.partnerComments}
                {session && session.user_type === "FO" && title.UNHCRComments}
              </div>
              {session && session.user_type === "PARTNER" && <Field
                name='partner_response'
                component={renderTextArea}
              />}
              {session && session.user_type === "FO" && <Field
                name='unhcr_comments'
                component={renderTextArea}
              />}
              <div className={classes.subText}>
                {session && session.user_type === "AUDITOR" && title.description}
              </div>
              {session && session.user_type === "AUDITOR" && <Field
                name='mgmt_letter_finding'
                component={renderTextArea}
              />}
              <div className={classes.subText}>
                {session && session.user_type === "AUDITOR" && title.risk}
              </div>
              {session && session.user_type === "AUDITOR" && <Field
                name='risk'
                component={renderTextArea}
              />}
              <div className={classes.subText}>
                {session && session.user_type === "AUDITOR" && title.auditorRecommendation}
              </div>
              {session && session.user_type === "AUDITOR" && <Field
                name='auditor_recommendation'
                component={renderTextArea}
              />}
              <div className={classes.subText}>
                {session && session.user_type === "AUDITOR" && title.auditorDissComment}
              </div>
              {session && session.user_type === "AUDITOR" && <Field
                name='auditors_disagreement_comment'
                component={renderTextArea}
              />}
              <div className={classes.buttonGrid}>
                <CustomizedButton
                  clicked={handleClose}
                  buttonText={"Cancel"}
                  buttonType={buttonType.cancel} />
                <CustomizedButton
                  type="submit"
                  buttonText={buttonType.submit}
                  buttonType={buttonType.submit} />
              </div>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const reportDialogForm = reduxForm({
  form: 'majorReportDialog',
  validate,
  enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.majorFinding
  return {
    initialValues,
    session: state.session
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);