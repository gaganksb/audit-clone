import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import Divider from '@material-ui/core/Divider';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderTextField, renderSelectField } from '../../common/renderFields';

const title = {
  heading: "MANAGEMENT LETTER - SECTION 4",
  key: "ICQ Key",
  process: "Process",
  overallRating: "ICQ Overall Rating",
}
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    marginTop: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#0072bc'
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#1F80C5',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#1F80C5'
    }
  },
  textField: {
    margin: '16px 16px 16px 0px',
    minWidth: '48%',
  },
  dialogTitle: {
    font: 'bold 18px/24px Roboto',
    color: '#606060',

  }
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'icqKey',
    'process',
    'rating'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

function ReportDialog(props) {
  const classes = useStyles();
  const { handleSubmit, handleClose, open } = props;
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'sm'}
      >
        <DialogTitle className={classes.dialogTitle}>{title.heading}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
            <div style={{ width: '100%' }}>
              <div className={classes.subText}>
                {title.key}
              </div>
              <Field
                name='icqKey'
                margin='normal'
                readOnly={true}
                component={renderTextField}
                fullWidth={true}
                className={classes}
                style={{ marginTop: 0 }}
              />
              <div className={classes.subText}>
                {title.process}
              </div>
              <Field
                name='process'
                margin='normal'
                readOnly={true}
                component={renderTextField}
                fullWidth={true}
                className={classes}
              />
              <div className={classes.subText}>
                {title.overallRating}
              </div>
              <Field
                name="rating"
                className={classes.formControl}
                component={renderSelectField}
                style={{ minWidth: '39vw' }}
              >
                {
                  ['', 'satisfactory', 'not satisfactory', 'TBD'].map((item, index) => {
                    return (<option key={item} value={item}>{item}</option>)
                  })
                }
              </Field>
              <div className={classes.buttonGrid}>

                <CustomizedButton
                  clicked={handleClose}
                  buttonText={buttonType.cancel}
                  buttonType={buttonType.cancel} />
                <CustomizedButton
                  type="submit"
                  buttonText={buttonType.submit}
                  buttonType={buttonType.submit} />
              </div>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const reportDialogForm = reduxForm({
  form: 'overallAssessmentDialog',
  validate,
  enableReinitialize: true,
})(ReportDialog);

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.overallAssessment
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(reportDialogForm);

export default withRouter(connected);