import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Paper from '@material-ui/core/Paper'
import { icqAttribute } from '../../../helpers/constants';
import ListLoader from '../../common/listLoader'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    table: {
        minWidth: 500,
    },
    noDataRow: {
        margin: 24,
        width: 'max-content',
        marginLeft: '226%',
        textAlign: 'center'
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    tableHeader: {
        color: '#8898AA',
        fontFamily: 'Roboto',
        fontSize: 12,
    },
    headerRow: {
        backgroundColor: '#F6F9FC',
        border: '1px solid #E9ECEF',
        height: 60,
        font: 'medium 12px/16px Roboto'
    },
    tableCell: {
        font: '12px/16px Roboto',
        letterSpacing: 0,
        color: '#8898AA'
    },
    heading: {
        paddingLeft: 22,
        paddingTop: 19,
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#2B72B6',
        font: 'bold 14px/24px Roboto',
        letterSpacing: 0,
        paddingBottom: 20
    },
    fab: {
        margin: theme.spacing(1),
        backgroundColor: '#1F80C5'
    },
}))

const rowsPerPage = 10

const EDIT = 'EDIT'
const DELETE = 'DELETE'
function CustomPaginationActionsTable(props) {
    const {
        items,
        loading,
        totalItems,
        session,
        handleEdit,
        handleDelete,
        messages
    } = props

    const classes = useStyles()
    const [open, setOpen] = React.useState({ status: false, type: null })
    return (
        <React.Fragment>
            <Paper className={classes.root}>
                    <div className={classes.heading}>
                        {messages.heading}
                    </div>
                <ListLoader loading={loading}>
                    <div className={classes.tableWrapper}>
                        <Table className={classes.table}>
                            <TableHead className={classes.headerRow}>
                                <TableRow >
                                    {messages.tableHeader.map((item, key) =>
                                        <TableCell style={{ width: item.width }} align={item.align} key={key} className={classes.tableHeader} >
                                            {(item.title) ?
                                                item.title
                                                : null}
                                        </TableCell>
                                    )}
                                </TableRow>
                            </TableHead>
                            {(!items || (items && items.length == 0) && !loading) ?
                                <div className={classes.noDataRow}>No data for the selected criteria</div> :
                                <TableBody>
                                    {items && items.map((item, key) => (
                                        
                                        <TableRow key={key}>
                                            <TableCell className={classes.tableCell}>{(item && item.control_attribute)? icqAttribute.find((attribute) => {if(attribute.key == item.control_attribute) return attribute.value}).value: 'N/A'}</TableCell>
                                            <TableCell className={classes.tableCell}>{_.get(item, 'prev_recommendation', 'N/A')}</TableCell>
                                            <TableCell className={classes.tableCell}>{_.get(item, 'criticality', 'N/A')}</TableCell>
                                            <TableCell className={classes.tableCell}>{_.get(item, 'summary', 'N/A')}</TableCell>
                                            <TableCell className={classes.tableCell}>{_.get(item, 'recommendation', 'N/A')}</TableCell>
                                            {session && session.user_type== 'AUDITOR' && <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                                                <Tooltip title={"Edit"}>                                                    
                                                    <EditIcon onClick={() => handleEdit(item, EDIT)} style={{marginRight: 15}}/>                                                    
                                                </Tooltip>
                                                <Tooltip title={"Delete"}>                                                    
                                                    <DeleteIcon onClick={() => handleDelete(item, DELETE)}/>                                                    
                                                </Tooltip>
                                            </TableCell>}
                                        </TableRow>
                                    ))}
                                </TableBody>
                            }

                        </Table>
                    </div>
                </ListLoader>
            </Paper>
        </React.Fragment>
    )
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query
})

const mapDispatchToProps = (dispatch) => ({
})

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)  