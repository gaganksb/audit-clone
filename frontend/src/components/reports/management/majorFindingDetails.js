import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    tableCell: {
        font: '12px/16px Roboto',
        letterSpacing: 0,
        color: '#8898AA'
    },
    icon: {
        margin: theme.spacing(2),
        '&:hover': {
            cursor: 'pointer'
        }
    },
    detailHeader: {
        font: '16px/16px Roboto',
        letterSpacing: 0,
        color: '#567393',
        fontWeight: 500,
        paddingTop: 39,
        // position: 'absolute',
        minWidth: 325,
        marginTop: "16px",
        marginLeft: "16px",
        marginBottom: "5px"
    },
    details: {
        font: '14px/18px Roboto',
        letterSpacing: 0,
        color: '#8898AA',
        minWidth: 400,
        fontWeight: 300,
        paddingTop: "4px",
        marginLeft: "16px",
        position: 'absolute',
        paddingBottom: 4
        // marginBottom: "30px",
    },
    detailContainer: {
        marginTop: "10px",
        marginBottom: "40px",
    },
    tableSpanHeader: {
        width: "100%",
        fontWeight: "900",
        fontSize: "18px",
        display: "inline-block",
    },
    tableDiv: {
        width: "490%",
        margin: "5px 5px 5px 15px",
        color: "#567393"
    },
    tableDescDiv: {
        display: "inline-block"
    },
    tableDescSpan: {
        display: "block",
        marginBottom: "20px",
        marginBottom: "20px"
    },
    tableSpanDesc: {
        color: "#8898AA",
        fontSize: "11.5px"
    }
}));

const messages = {
    detailHeaders: [
        'Description',
        'Risk',
        'Auditor\'s Recommendations',
        'Partner Response',
        'UNHCR Representation Comments',
        'Auditor\'s Comment (in case of disagreement)'
    ]
}

function MajorDetails(props) {
    const {
        expanded,
        handleExpand,
        item,
        icqDetails,
        handleEdit,
        handleDelete,
        session,
        hideButton
    } = props;

    const classes = useStyles();

    return (
        <React.Fragment>
            <TableRow >
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {(expanded) ?
                        <ExpandLess className={classes.icon} onClick={handleExpand()} /> :
                        <ExpandMore className={classes.icon} onClick={handleExpand()} />
                    }
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, ['key_control_attr', 'title'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, ['process_title', 'title'], 'N/A')}
                </TableCell>
                <TableCell className={classes.tableCell} component="th" scope="row">
                    {_.get(item, 'risk_criticality', 'N/A')}
                </TableCell>
                {session && session.user_type !== "HQ" ? <TableCell className={classes.tableCell} component="th" scope="row" align="right">
                    {!hideButton ? <Tooltip title={"Edit"}>
                        <span className={classes.icon} onClick={handleEdit}>
                            <EditIcon />
                        </span>
                    </Tooltip> : null}
                </TableCell> : <TableCell></TableCell>}
            </TableRow>
            {(expanded) ?
                <div className={classes.tableDiv}>
                    <div container className={classes.tableDescDiv}>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[0]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'mgmt_letter_finding', 'N/A')}
                            </span>
                        </span>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[1]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'risk', 'N/A')}
                            </span>
                        </span>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[2]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'auditor_recommendation', 'N/A')}
                            </span>
                        </span>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[3]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'management_proposed_actions', 'N/A')}
                            </span>
                        </span>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[4]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'unhcr_comments', 'N/A')}
                            </span>
                        </span>
                        <span className={classes.tableDescSpan}>
                            <span className={classes.tableSpanHeader}>
                                {messages.detailHeaders[5]}
                            </span>
                            <span className={classes.tableSpanDesc}>
                                {_.get(item, 'auditors_disagreement_comment', 'N/A')}
                            </span>
                        </span>
                    </div>
                </div> : null}
        </React.Fragment>
    );
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname,
    session: state.session
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MajorDetails);

export default withRouter(connected);