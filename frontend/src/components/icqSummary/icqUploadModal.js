import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { makeStyles } from "@material-ui/core/styles";
import { Divider } from "@material-ui/core";
import CustomizedButton from '../common/customizedButton';
import { renderTextField } from '../common/renderFields';
import { buttonType } from '../../helpers/constants';
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    container: {
        display: "flex"
    },
    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: 18
    },
    textField: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    buttonGrid: {
        display: "flex",
        justifyContent: "flex-end",
        width: "100%",
        marginTop: theme.spacing(2)
    },
    subText: {
        marginTop: theme.spacing(2),
        fontWeight: "bolder"
    }
}));

const validate = values => {
    const errors = {};
    const requiredFields = ["year", "icq_zip_file"];
    requiredFields.forEach(field => {
        if (!values[field] || values[field] === undefined) {
            errors[field] = "Required";
        }else if(field === "year" && (values[field] < 2020 || values[field] > new Date().getFullYear() + 2)){
            errors[field] = "Invalid Year for ICQ"
        }
    });
    return errors;
};

const messages = {
    cancel: 'Cancel',
    apply: 'Apply'
}

const adaptFileEventToValue = delegate => e => delegate(e.target.files[0]);

const FileInput = ({
  input: { value: omitValue, onChange, onBlur, ...inputProps },
  meta: omitMeta,
  ...props
}) => {
  return (
    <input
      onChange={adaptFileEventToValue(onChange)}
      onBlur={adaptFileEventToValue(onBlur)}
      type="file"
      {...props.input}
      {...props}
    />
  );
};

const ICQUpload = (props) => {
    const {
        open,
        handleClose,
        handleSubmit
    } = props;
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Dialog open={open} onClose={handleClose} fullWidth>
                <DialogTitle>Assign Selected Projects</DialogTitle>
                <Divider />
                <DialogContent>
                    <form onSubmit={handleSubmit}>
                        <div className={classes.subText}>Year</div>
                            <Field
                                name='year'
                                type="number"
                                component={renderTextField}
                            />
                        <div className={classes.subText}>Attachment</div>
                            <Field
                                name="icq_zip_file"
                                component={FileInput}
                                type="file"
                            />
                        <div className={classes.buttonGrid}>
                            <CustomizedButton
                                clicked={handleClose}
                                buttonType={buttonType.cancel}
                                buttonText={buttonType.cancel}
                            />
                            <CustomizedButton
                                type='submit'
                                buttonType={buttonType.submit}
                                buttonText='Save'
                            />
                        </div>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const mapState = (state, ownProps) => ({})

const mapDispatch = (dispatch) => ({})


const ICQUploadModal = reduxForm({
    form: "ICQUpload",
    validate,
    enableReinitialize: true
})(ICQUpload);


export default connect(mapState, mapDispatch)(ICQUploadModal);