import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../common/listLoader';
import TablePaginationActions from '../common/tableActions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import TableSortLabel from '@material-ui/core/TableSortLabel';
// import GetAppIcon from '@material-ui/icons/GetApp';
// import { SubmissionError } from 'redux-form';
import { loadICQError } from '../../reducers/reports/icqQuestionnaire/getICQErrors';
import _ from 'lodash';
import { normalizeDate } from '../../helpers/dates';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';

const messages = {
    icqErrortableHeader: [
        { title: 'Errors', align: 'left', field: 'created_date', width: '70%' },
        { title: 'Date', align: 'left', field: 'errors', width: '15%' },
        { title: 'User', align: 'left', field: 'user', width: '15%' },
    ],
}

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    tableHeader: {
        color: '#344563',
        fontFamily: 'Roboto',
        fontSize: 14,
    },
    headerRow: {
        backgroundColor: '#F6F9FC',
        border: '1px solid #E9ECEF',
        height: 60,
        font: 'medium 12px/16px Roboto'
    },
    noDataRow: {
        margin: 16,
        width: '100%',
        position: 'absolute',
        textAlign: 'center'
    },
    tableIncludeRow: {
        backgroundColor: '#F5DADA'
    },
    tableExcludeRow: {
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    heading: {
        padding: theme.spacing(3),
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#344563',
        font: 'bold 16px/21px Roboto',
        letterSpacing: 0,
    },
    table: {
        minWidth: 500,
        maxWidth: '100%'
    },
    tableWrapper: {
        width: (window.navigator.userAgent.includes("Windows")) ? '95vw' : '97vw',
        maxHeight: 600,
        overflow: 'auto',
        borderRadius: 6
    },
    tableRow: {
        borderStyle: 'hidden',
    },
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
}));

const rowsPerPage = 10

function ICQBulkUploadErrorTable(props) {
    const {
        totalItems,
        loading,
        page,
        handleChangePage,
        handleSort,
        sort,
        loadICQError,
        icqErrors,
    } = props;

    const classes = useStyles();
    const [expanded, setExpanded] = useState({})

    const handleExpandOpen = (id) => {
        setExpanded({ id })
    }

    const handleExpandClose = (id) => {
        setExpanded({})
    }

    useEffect(() => {
        loadICQError()
    }, [])

    return (
        <Paper className={classes.root}>
            <ListLoader loading={loading} >
                <div className={classes.tableWrapper}>
                    <div className={classes.heading}>
                        ICQ Upload Errors
                    </div>
                    <Table className={classes.table}>
                        <TableHead className={classes.headerRow}>
                            <TableRow>
                                {messages.icqErrortableHeader.map((item, key) =>
                                    <TableCell align={item.align} key={key} className={classes.tableHeader} style={{ width: item.width }} >
                                        {(item.title) ? <TableSortLabel>
                                            {item.title}
                                        </TableSortLabel> : item.title}
                                    </TableCell>
                                )}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {icqErrors && icqErrors.length ?
                                icqErrors.map((item, key) => (
                                    <TableRow key={item.id}>
                                        <TableCell className={classes.tableCell}>
                                            {(expanded.id === item.id) ?
                                                <ExpandLess className={classes.icon} onClick={() => handleExpandClose(item.id)} /> :
                                                <ExpandMore className={classes.icon} onClick={() => handleExpandOpen(item.id)} />
                                            }
                                            Total <strong> {item.errors && item.errors.length} </strong> Errors in the upload
                                            {
                                                expanded.id === item.id &&
                                                item.errors.map(i => (
                                                    <ul style={{
                                                        borderBottom: "1px solid grey",
                                                        paddingBottom: "20px",
                                                        width: "145%"
                                                    }}
                                                    >
                                                        <li>
                                                            <div dangerouslySetInnerHTML={{ __html: i }}></div>
                                                        </li>
                                                    </ul>
                                                ))
                                            }
                                        </TableCell>
                                        <TableCell component="th" scope="row" className={classes.tableCell}>
                                            {normalizeDate(_.get(item, ['created_date'], 'N/A'))}
                                        </TableCell>
                                        <TableCell className={classes.tableCell}>
                                            {item && item.user && item.user.fullname}
                                        </TableCell>
                                    </TableRow>))
                                : <div className={classes.noDataRow}>No data for the selected criteria</div>}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[rowsPerPage]}
                                    colSpan={3}
                                    count={totalItems}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    SelectProps={{
                                        native: true,
                                    }}
                                    style={{ overflow: 'hidden' }}
                                    onChangePage={handleChangePage}
                                    ActionsComponent={TablePaginationActions}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </ListLoader>
        </Paper>
    );
}

const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    icqErrors: state.icqErrors.results,
    totalItems: state.icqErrors.totalItems
});

const mapDispatchToProps = (dispatch) => ({
    loadICQError: (params) => (dispatch(loadICQError(params)))
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(ICQBulkUploadErrorTable);

export default withRouter(connected);