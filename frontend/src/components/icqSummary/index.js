import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadIcqList } from '../../reducers/projects/icqReducer/icqList';
import ICQSummaryFilter from './icqSummaryFilter';
import ICQSummaryTable from './icqSummarytable'
import R from 'ramda';
import Title from '../common/customTitle';
import { loadBUList } from '../../reducers/projects/oiosReducer/buList';
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import { downloadURI } from '../../helpers/others';
import ICQUploadModal from './icqUploadModal';
import {bulkICQUpload} from "../../reducers/reports/icqQuestionnaire/uploadIcq";
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { errorToBeAdded } from "../../reducers/errorReducer";

const messages = {
  title: 'ICQ Summary',
  icqUpload: 'Upload ICQ',
  tableHeader: [
    { title: '', align: 'left', width: '2%' },
    { title: 'Partner Code', align: 'left', field: 'business_unit__code', width: '5%' },
    { title: 'Partner Name', align: 'left', field: 'partner__legal_name', width: '5%' },
    { title: 'Business Unit', align: 'left', field: 'business_unit__code', width: '5%' },
    { title: 'Overall Score', align: 'left', field: 'overall_score_perc', width: '5%' },
    { title: 'A: Payment and Bank management', align: 'left', field: 'A', width: '10%' },
    { title: 'B: Procurement', align: 'left', field: 'B', width: '8%' },
    { title: 'C: Management of activities subcontracted to sub-partners', align: 'left', field: 'C', width: '10%' },
    { title: 'D: Personnel engagement and payment', align: 'left', field: 'D', width: '10%' },
    { title: 'E: Budget', align: 'left', field: 'E', width: '5%' },
    { title: 'F: Goods and Property', align: 'left', field: 'F', width: '10%' },
    { title: 'G: General control environment', align: 'left', field: 'G', width: '10%' },
    { title: 'H: Compliance with other terms of the partnership agreement', align: 'left', field: 'H', width: '10%' },
  ],
  icqErrortableHeader: [
    { title: 'Date', align: 'left', field: 'created_date', width: '5%' },
    { title: 'Errors', align: 'left', field: 'errors', width: '5%' },
    { title: 'User', align: 'left', field: 'user', width: '5%' },
  ],
  error: 'Could not complete the action'
}

const currentYear = new Date().getFullYear()
const ICQSummary = props => {
  const {
    icq,
    loadIcq,
    loading,
    totalCount,
    query,
    BUList,
    loadBUList,
    session,
    bulkICQUpload,
    postError,
    successReducer,
  } = props

  const [params, setParams] = useState({ page: 0 });
  const [bu, setBu] = React.useState([])
  const [items, setItems] = useState([]);
  const [year, setYear] = useState(currentYear);
  const [key, setKey] = useState('default')
  const [open, setOpen] = useState(false);
  var date = new Date()

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field,
    order: 'desc'
  })

  useEffect(() => {
    setItems(BUList);
  }, [BUList]);

  useEffect(() => {

    const { pathName } = props;
    if ("year" in query === false) {
      setYear(currentYear);
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          year: currentYear
        })
      })
    } else {
      setYear(query.year)
      loadIcq(query)
    }
  }, [query]);

  function goTo(item) {
    history.push(item);
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  const handleChange = (e, v) => {

    if (v) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      }),
    })
  }

  function resetFilter() {
    const { pathName } = props;
    setBu([])
    setKey(date.getTime())
    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  function handleSearch(values) {
    const { pathName, query } = props;
    const { business_unit_code, partner_legal_name, year } = values;

    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        business_unit_code,
        partner_legal_name,
        year
      }),
    })
  }

  const handleSubmit = (value) => {
    const formData = new FormData();
    formData.append("icq_zip_file", value.icq_zip_file)
    const year = value.year
    bulkICQUpload(year, formData).then(res=>{
      setOpen(false)
      successReducer(res.message)
    }).catch(error=>{
      setOpen(false)
      postError(error, "Form upload failed");
      throw new SubmissionError({
        ...error.response.data,
        _error: error.response.message,
      });
    })
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <Title show={false} title={messages.title} />
      <ICQSummaryFilter
        onSubmit={handleSearch}
        bu={bu}
        setBu={setBu}
        key={key}
        year={year}
        items={items}
        setYear={setYear}
        handleChange={handleChange}
        resetFilter={resetFilter}
      />
      {session && session.user_type === 'HQ' && <div
        style={{ padding: "9px" }}
      >
        <CustomizedButton
          clicked={() => downloadURI(`/api/icq/export-icqsummary/${year}/`, "icqsummary")}
          buttonType={buttonType.submit}
          buttonText={'Export'}
        />
        <CustomizedButton
          clicked={()=>setOpen(true)}
          buttonType={buttonType.submit}
          buttonText={'Upload ICQ'}
        />
        <CustomizedButton
          clicked={()=>{history.push("/icq-upload-erros")}}
          buttonType={buttonType.submit}
          buttonText={'ICQ Upload Errors'}
        />
      </div>}
      <ICQSummaryTable
        messages={messages}
        sort={sort}
        items={icq}
        loading={loading}
        totalItems={totalCount}
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
      <ICQUploadModal open={open} onSubmit={handleSubmit} handleClose={handleClose}/>
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  icq: state.icqList.icq,
  totalCount: state.icqList.totalCount,
  loading: state.icqList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
  session: state.session
});

const mapDispatch = dispatch => ({
  loadIcq: params => dispatch(loadIcqList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  bulkICQUpload: (year, body) => dispatch(bulkICQUpload(year, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, "auditingReport", message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
});

const connectedICQSummary = connect(mapStateToProps, mapDispatch)(ICQSummary);
export default withRouter(connectedICQSummary);