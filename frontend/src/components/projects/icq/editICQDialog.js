import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { renderTextField } from '../../common/renderFields';
import { withRouter } from 'react-router';
import FieldFileInput from '../../common/fields/fileField';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  inputFile: {
    position: 'relative',
    top: -9,
    minHeight: 29,
    borderRadius: 5,
    minWidth: 308
  },
  btn: {
    border: 'none',
    color: 'white',
    backgroundColor: '#EDB57E',
    borderRadius: 5,
    minWidth: 94,
    font: '14px/19px Open Sans',
    height: 30,
    fontWeight: 'bold'
  },
  buttonWrapper: {
    marginTop: theme.spacing(2),
    position: 'relative',
    overflow: 'hidden',
    display: 'inline-block',

    '& > input': {
    fontSize: 100,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0,
    }
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  button: {
    marginTop: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  label: {
    fontSize: '12px'
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
}));

var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    "attachment"
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const messages = {
  submit: 'Submit',
  cancel: 'Cancel'
}

function EditDialog(props) {
  const { open, handleClose, handleSubmit, partner } = props;
  const classes = useStyles();
  
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{"Edit ICQ Partner"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <div>
                  <p className={classes.label}>Partner</p>
                  <p className={classes.value}>{partner && partner.partner && partner.partner.legal_name}</p>
                </div>
                <div>
                  <p className={classes.label}>Business Unit</p>
                  <p className={classes.value}>{partner && partner.business_unit && partner.business_unit.code}</p>
                </div>
                <div>
                  <p className={classes.label}>Implementer Number</p>
                  <p className={classes.value}>{partner && partner.partner && partner.partner.number}</p>
                </div>
                <div>
                  <p className={classes.label}>Overall Score</p>
                  <p className={classes.value}>{partner && partner.partner && partner.overall_score_perc}</p>
                </div>
                <div>
                  <p className={classes.label}>Report Date</p>
                  <p className={classes.value}>{partner && partner.partner && partner.report_date}</p>
                </div>
                <div>
                  <p className={classes.label}>Year</p>
                  <p className={classes.value}>{partner && partner.partner && partner.year}</p>
                </div>
                {/* <Field
                  name='overall_score_perc'
                  label='Overall Score'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  InputProps={{
                    inputProps: {
                      step: 0.01,
                      min: 0
                    }
                  }}
                /> */}
                {/* <Field
                  name='report_date'
                  label='Report Date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                /> */}
                {/* <Field 
                  name='year'
                  label='Year'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  InputProps={{
                    inputProps: {
                      min: 2014
                    }
                  }}
                /> */}
                <Field
                  name="attachment"
                  component={FieldFileInput}
                  classes={classes}
                  report={partner}
                  error={errors['attachment']}
                />
                <Grid className={classes.buttonGrid} >
                  <CustomizedButton
                    clicked={handleClose}
                    buttonText={messages.cancel}
                    buttonType={buttonType.cancel} />
                  <CustomizedButton
                    type='submit'
                    buttonText={messages.submit}
                    buttonType={buttonType.submit} />
                </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const editICQForm = reduxForm({
  form: 'editICQPartner',
  validate,
  enableReinitialize: true,
})(EditDialog);


const mapStateToProps = (state, ownProps) => {
  let initialValues = { ...ownProps.partner, attachment: "somed" }
  // initialValues.partner.attachment = "checkthis.pdg"
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(editICQForm);

export default withRouter(connected);
