import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import AddICQDialog from './addICQDialog';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { reset } from 'redux-form';
import { withRouter } from 'react-router';
import { loadPartnerList } from '../../../reducers/partners/partnerList';
import { addNewICQ } from '../../../reducers/projects/icqReducer/addNewIcqPartner';
import { SubmissionError } from 'redux-form';
import R from 'ramda';
import { showSuccessSnackbar, successReducer } from '../../../reducers/successReducer';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  }
}));

const ADD_ICQ = 'ADD_ICQ';

const messages = {
  error: 'Error',
  success: 'ICQ Partner Added Successfully !',
  add: 'Add ICQ'
}

function AddICQ(props) {
  const classes = useStyles();
  const {postError, loadPartnerList, partnerList, postICQ, loadBUList, BUList, successReducer } = props;
  const [open, setOpen] = useState({ status: false, type: null });
  const [items, setItems] = useState([]);
  const [BUItems, setBUItems] = useState([]);
  const [bu, setBu] = React.useState([])
  
  useEffect(() => {
    setItems(partnerList);
  }, [partnerList]);

  function handleClose() {
    setOpen({ status: false, type: null });
  }

  function handleOpen(action) {
    setOpen({ status: true, type: action });
  }

  function handleSubmit(values) {
    const {partner, business_unit, report_date, overall_score_perc, year, attachment } = values;
    let partner_selected = R.find(R.propEq('legal_name', partner), items);
    let bu_selected = R.find(R.propEq('code', business_unit), BUItems);

    let body;
    // if (partner_selected && bu_selected) {
    //   body = {
    //     partner: partner_selected.id,
    //     business_unit: bu_selected.id,
    //     report_date,
    //     overall_score_perc,
    //     year,
    //     report_file: attachment
    //   }
    // }
    
    body = {
      year,
      report_file: attachment
    }
    const formData = new FormData();
    for ( var key in body ) {
      formData.append(key, body[key]);
    }

    return postICQ(formData).then(() => {
      handleClose();
      successReducer(messages.success)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response,
        _error: messages.error,
      });
    });
  }

  function handleChange(e, v) {
    let params = {};
    if (e.target.name === "business_unit") {
      params['code'] = v
      loadBUList(params);
    }else if(e.target.name === "partner"){
      params['legal_name'] = v
      loadPartnerList(params);
    }
  }

  useEffect(() => {
    setBUItems(BUList);
  }, [BUList]);

  return (
    <div className={classes.root}> 
    <span style={{marginLeft: -16}}> 
      <CustomizedButton buttonType={buttonType.submit} 
            buttonText={messages.add} 
            clicked={() => handleOpen(ADD_ICQ)} />
      </span>
      <AddICQDialog 
        open={open.status && open.type === ADD_ICQ} 
        onSubmit={handleSubmit} 
        items={items}
        BUItems={BUItems}
        bu={bu}
        setBu={setBu}
        handleChange={handleChange}
        handleClose={handleClose} />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  partnerList: state.partnerList.list,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
});

const mapDispatch = dispatch => ({
  postICQ: body => dispatch(addNewICQ(body)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'addICQ', message)),
  reset: () => dispatch(reset('addICQPartner')),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  loadBUList: (params) => dispatch(loadBUList(params))
});

const connected = connect(mapStateToProps, mapDispatch)(AddICQ);
export default withRouter(connected);