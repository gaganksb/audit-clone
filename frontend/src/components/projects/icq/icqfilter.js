import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField } from '../../common/renderFields';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import InputLabel from '@material-ui/core/InputLabel';
import CustomizedButton from '../../common/customizedButton';
import {buttonType, filterButtons} from '../../../helpers/constants';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {renderRadioField} from '../../../helpers/formHelper';
import Radio from '@material-ui/core/Radio'
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import {connect} from 'react-redux';
import { loadPartnerList } from '../../../reducers/partners/partnerList';

const messages = {
  radio: {
    all: {
      value: 'all',
      label: 'All'
    },
    included: {
      value: 'uploaded',
      label: 'Uploaded'
    },
    excluded: {
      value: 'not_uploaded',
      label: 'Not Uploaded'
    }
  }
}

const type_to_search = 'business_unit__code'
const partner_type_to_search = 'partner__legal_name';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 6
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '90%',
    marginTop: 2
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    width: '93%',
    marginTop: 2
},
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop : theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
  },
  radio: {
    marginLeft: theme.spacing(2),
  },
  radioButton : {
  '&.Mui-checked': {
      color: '#0072BC'
    }
  },
}));


function ICQFilter(props) {
  const classes = useStyles();
  const { handleSubmit, reset, resetFilter, handleChange, items, bu, setBu, key, partnerList, loadPartnerList, year, setYear } = props;
  const [partnerLists, setPartnerList] = useState([])
  const [partner, setSelectedPartner] = useState([])

  function handleChangePartner(e,v) {
    if (v) {
      let params = {
        legal_name: v,
      }
      loadPartnerList(params);
    }
  }

  
  useEffect(() => {
    setPartnerList(partnerList);
  }, [partnerList]);

  const radioOptions = (() => {
    let res = []
    for (let prop in messages.radio) {
      res.push(messages.radio[prop])
    }
    return res
  })()

  const handleValueChange = val => {
    setBu(val)
  }

  const handlePartnerValueChange = val => {
    setSelectedPartner(val)
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid container sm={10} >
          <Grid item sm={3}>
          <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
            <Field
            name='business_unit'
            key={key}
            className={classes.autoCompleteField}
            component={CustomAutoCompleteField}
            handleValueChange={handleValueChange}
            onChange={handleChange}
            items={items}
            comingValue= {bu}
            type_to_search={type_to_search}
          />
          </Grid>
          <Grid item sm={2}>
           <InputLabel className={classes.inputLabel}>Year</InputLabel>
            <Field
              name='year'
              margin='normal'
              type='number'
              component={renderTextField}
              className={classes.textField}
              onChange={(e)=>{setYear(e.target.value)}}
              InputProps={{
                inputProps: {
                  min: 2014,
                  value: year
                }
              }}
            />
            </Grid>
            <Grid item sm={3}>
            <InputLabel className={classes.inputLabel}>Partner</InputLabel>
                <Field
                  name='partner'
                  key={key}
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handlePartnerValueChange}
                  onChange={handleChangePartner}
                  items={partnerLists}
                  comingValue={partner}
                  type_to_search={partner_type_to_search}
                />
              </Grid>
              <Grid item sm={3}>
              <InputLabel className={classes.inputLabel}>Implementer Number</InputLabel>
            <Field 
              name='number'
              className={classes.textField}
              margin='normal'
              type='number'
              component={renderTextField}
            />
            </Grid>
            <Grid item sm={5}>
            <Field
              name="view_items"
              component={renderRadioField}
              // checkSelection={checkSelection}
              className={classes.radio}
              margin='normal'
            >
              {radioOptions.map((option, index) => (
                <FormControlLabel
                  key={index}
                  style={{ display: "inline-block"}}
                  value={option.value}
                  control={
                    <Radio
                      className={classes.radioButton}
                      style={{ display: "inline-block" }}
                      // onClick={checkSelection}
                    />
                  }
                  label={option.label}
                  labelPlacement="end"
                />
              ))}
            </Field>
            </Grid>
            </Grid>
          <Grid item sm={2}>
          <div item className={classes.buttonGrid}>
                <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type="submit" />
                <CustomizedButton buttonType={buttonType.submit}                  
                  reset = {true}
                  clicked = {() => { reset(); resetFilter(); }}
                  buttonText={filterButtons.reset}
                />
              </div>
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

// export default reduxForm({
//   form: 'icqFilter',
//   enableReinitialize: true,
// })(ICQFilter);

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.year
  return {
    partnerList: state.partnerList.list,
    initialValues
  }
}  

const mapDispatch = dispatch => ({
  loadPartnerList: (params) => dispatch(loadPartnerList(params))
})

const ICQFilterForm = reduxForm({
  form: 'icqFilter',
  enableReinitialize: true,
})(ICQFilter);

const connected = connect(
  mapStateToProps, mapDispatch
)(ICQFilterForm)

export default connected
