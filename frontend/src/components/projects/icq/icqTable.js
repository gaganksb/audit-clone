import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import Tooltip from '@material-ui/core/Tooltip';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import TablePaginationActions from '../../common/tableActions';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import DeleteDialog from './deleteICQDialog';
import { deleteICQPartnerRequest } from '../../../reducers/projects/icqReducer/deleteIcqPartner';
import { loadIcqList } from '../../../reducers/projects/icqReducer/icqList';
import { editICQPartner } from '../../../reducers/projects/icqReducer/editICQPartner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import EditDialog from './editICQDialog';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { SubmissionError } from 'redux-form';
import PublishIcon from '@material-ui/icons/Publish';
import GetAppIcon from '@material-ui/icons/GetApp';
import { downloadURI } from '../../../helpers/others';


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  noDataRow: {
    margin: 16,
    width: '100%',
    position: 'absolute',
    textAlign: 'center'
  },
  tableIncludeRow: {
    backgroundColor: '#F5DADA'
  },
  tableExcludeRow: {
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const DELETE = 'DELETE'
const EDIT = 'EDIT'
const success = {
  delete: 'ICQ Partner Deleted !',
  edit: 'ICQ Partner Updated !'
}

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort,
    deleteICQPartner,
    editICQPartner,
    loadIcq,
    successReducer
  } = props;

  const classes = useStyles();

  function handleDeleteICQPartner(values) {
    const { query } = props

    return deleteICQPartner(values).then(() => {
      loadIcq(query);
      handleClose();
      successReducer(success.delete)
    })
  }

  function handleEditICQPartner(values) {
    const { id, overall_score_perc, report_date, year, attachment } = values;
    const body = {
      overall_score_perc,
      report_date,
      year,
      report_file: attachment
    };
    const { query, postError } = props

    const formData = new FormData();
    for (var key in body) {
      formData.append(key, body[key]);
    }

    return editICQPartner(id, formData).then(() => {
      handleClose();
      loadIcq(query);
      successReducer(success.edit)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error,
      });
    });
  }

  const [open, setOpen] = React.useState({ status: false, type: null })
  const [partner, setPartner] = React.useState({});

  function handleClickOpen(item, action) {
    setPartner(item);
    setOpen({ status: true, type: action })
  }

  function handleClose() {
    setOpen({ status: false, type: null });
  }

  const handleClassName = (item) => {
    if (item && item.is_default === true) {
      return classes.tableIncludeRow
    } else {
      return classes.tableExcludeRow
    }
  }

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
      <div className={classes.tableWrapper}>
          <div className={classes.heading}>
          ICQ
        </div>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key} className={classes.tableHeader} style={{width: item.width}} >
                    <TableSortLabel
                      active={item.field === sort.field}
                      direction={sort.order}
                      onClick={() => handleSort(item.field, sort.order)}
                    >
                      {item.title}
                    </TableSortLabel>
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.length ?
                items.map(item => (
                  <TableRow key={item.email} className={handleClassName(item)}>
                    <TableCell component="th" scope="row" className={classes.tableCell}>
                      {item.business_unit.code}
                    </TableCell>
                    <TableCell component="th" scope="row" className={classes.tableCell}>
                      {item.year}
                    </TableCell>
                    <TableCell component="th" scope="row" className={classes.tableCell}>
                      {item.partner.legal_name}
                    </TableCell>
                    <TableCell component="th" scope="row" className={classes.tableCell}>
                      {item.partner.number}
                    </TableCell>
                    <TableCell className={classes.tableCell}>{item.report_date}</TableCell>
                    <TableCell className={classes.tableCell}>{item.user && item.user.email}</TableCell>
                    <TableCell className={classes.tableCell}>{item.overall_score_perc}</TableCell>
                    <TableCell align='right' className={classes.tableCell}>
                      <Tooltip title={"Delete ICQ"}>
                      <Fab size='small' className={classes.fab} onClick={() => handleClickOpen(item, DELETE)} >
                        <DeleteIcon />
                      </Fab>
                      </Tooltip>
                      <Tooltip title={item.report_doc ? "Download ICQ" : "Upload ICQ"}>
                        {
                          item.report_doc ? (
                            <Fab color="primary" size='small' className={classes.fab} onClick={() => downloadURI(item.report_doc.filepath, item.report_doc.filename)} style={{backgroundColor: '#0072BC'}} >
                            <GetAppIcon />
                            </Fab>
                          ) : (
                            <Fab color="primary" size='small' className={classes.fab}  onClick={() => handleClickOpen(item, EDIT)} style={{backgroundColor: '#0072BC'}} >
                              <PublishIcon />
                              </Fab>
                            )
                        }
                      </Tooltip>
                    </TableCell>
                  </TableRow>))
                : <div className={classes.noDataRow}>No data for the selected criteria</div>}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={8}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
          <DeleteDialog
            handleClose={handleClose}
            open={open.status && open.type === DELETE}
            handleDeleteICQPartner={handleDeleteICQPartner}
            partner={partner}
          />
          <EditDialog
            handleClose={handleClose}
            open={open.status && open.type === EDIT}
            partner={partner}
            onSubmit={handleEditICQPartner}
          />
        </div>
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
});

const mapDispatchToProps = (dispatch) => ({
  loadIcq: params => dispatch(loadIcqList(params)),
  deleteICQPartner: (id) => dispatch(deleteICQPartnerRequest(id)),
  editICQPartner: (id, body) => dispatch(editICQPartner(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editICQ', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable);

export default withRouter(connected);





