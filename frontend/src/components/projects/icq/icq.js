import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadIcqList } from '../../../reducers/projects/icqReducer/icqList';
import ICQFilter from './icqfilter';
import ICQTable from './icqTable';
import AddICQ from './addICQ';
import R from 'ramda';
import Title from '../../common/customTitle';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';

const messages = {
  title: 'ICQ',
  tableHeader: [
    { title: 'Business Unit', align: 'left', field: 'business_unit__code', width: '10%' },
    { title: 'Year', align: 'left', field: 'year',  width: '10%'  },
    { title: 'Partner', align: 'left', field: 'partner__legal_name',  width: '20%' },
    { title: 'Implementer Number', align: 'left', field: 'partner__number',  width: '10%'  },
    { title: 'Date', align: 'left', field: 'report_date',  width: '10%'  },
    { title: 'Author', align: 'left', field: 'user__email', width: '12%'  },
    { title: 'Overall Score', align: 'left', field: 'overall_score_perc', width: '10%'  },
    { title: '', align: 'left', width: '18%'  }
  ],
  error: 'Could not complete the action'
}

const currentYear = new Date().getFullYear()
const ICQ = props => {
  const { icq, loadIcq, loading, totalCount, query, BUList, loadBUList } = props

  const [params, setParams] = useState({ page: 0 });
  const [bu, setBu] = React.useState([])
  const [items, setItems] = useState([]);
  const [year, setYear] = useState(currentYear);

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field,
    order: 'desc'
  })

  useEffect(() => {
    setItems(BUList);
  }, [BUList]);

  useEffect(() => {

    const { pathName } = props;
    if ("year" in query === false) {
      setYear(currentYear);
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          year: currentYear
        })
      })
    } else {
      setYear(query.year)
      loadIcq(query)
    }
  }, [query]);

  function goTo(item) {
    history.push(item);
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  const handleChange = (e, v) => {
    
    if (v) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      }),
    })
  }

  function resetFilter() {
    const { pathName } = props;

    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  function handleSearch(values) {
    const { pathName, query } = props;
    const { business_unit, partner, year, number, view_items } = values;

    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        business_unit,
        partner,
        year,
        number,
        view_items
      }),
    })
  }

  return (
    <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
        <Title show={false} title={messages.title} />
      <ICQFilter
        onSubmit={handleSearch}
        bu={bu}
        setBu={setBu}
        year={year}
        items={items}
        setYear={setYear}
        handleChange={handleChange}
        resetFilter={resetFilter}
      />
      {/* <AddICQ/> */}
      <ICQTable
        messages={messages}
        sort={sort}
        items={icq}
        loading={loading}
        totalItems={totalCount}
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  icq: state.icqList.icq,
  totalCount: state.icqList.totalCount,
  loading: state.icqList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
});

const mapDispatch = dispatch => ({
  loadIcq: params => dispatch(loadIcqList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
});

const connectedICQ = connect(mapStateToProps, mapDispatch)(ICQ);
export default withRouter(connectedICQ);