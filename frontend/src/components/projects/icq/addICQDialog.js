import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { renderTextField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import { withRouter } from 'react-router';
import FieldFileInput from '../../common/fields/fileField';

const type_to_search = 'partner__legal_name';

var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    // 'partner',
    // 'overall_score_perc',
    // 'report_date',
    // 'business_unit',
    'year',
    "attachment"
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '100%',
  },
  autoCompleteField: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    width: "100%",
    minWidth: "100%"
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
  },
  button: {
    marginTop: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  formControl: {
    minWidth: 160,
  },
  inputFile: {
    position: 'relative',
    top: -9,
    minHeight: 29,
    borderRadius: 5,
    minWidth: 308
  },
  btn: {
    border: 'none',
    color: 'white',
    backgroundColor: '#EDB57E',
    borderRadius: 5,
    minWidth: 94,
    font: '14px/19px Open Sans',
    height: 30,
    fontWeight: 'bold'
  },
  buttonWrapper: {
    marginTop: theme.spacing(2),
    position: 'relative',
    overflow: 'hidden',
    display: 'inline-block',

    '& > input': {
    fontSize: 100,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0,
    }
  },
}));

const messages = {
  submit: 'Submit',
  cancel: 'Cancel',
  title: 'Create ICQ Partner'
}

function AddICQDialog(props) {
  const {
    open, handleClose, handleChange, handleSubmit, items, BUItems, bu, setBu 
  } = props;

  const classes = useStyles();
  const handleValueChange = val => {
    setBu(val)
  }

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >{messages.title}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                {/* <Field
                  name='partner'
                  label='Partner'
                  className={classes.autoCompleteField}
                  style={{width: "100%", minWidth:"100%"}}
                  margin='normal'
                  fullWidth
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChange}
                  items={items}
                  comingValue={bu}
                  type_to_search={'partner__legal_name'}
                />
                <Field
                  name='business_unit'
                  label='Business Unit'
                  className={classes.autoCompleteField}
                  style={{width: "100%", minWidth:"100%"}}
                  margin='normal'
                  fullWidth
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChange}
                  items={BUItems}
                  comingValue={bu}
                  type_to_search={'business_unit__code'}
                />
                <Field
                  name='overall_score_perc'
                  label='Overall Score'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  InputProps={{
                    inputProps: {
                      step: 0.01,
                      min: 0
                    }
                  }}
                />
                <Field 
                  name='report_date'
                  label='Report Date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                /> */}
                <InputLabel className={classes.inputLabel}>Year</InputLabel>
                <Field
                  name='year'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  className={classes.textField}
                  InputProps={{
                    inputProps: {
                      min: 2014,
                    }
                  }}
                />
                <Field 
                  name="attachment"
                  component={FieldFileInput}
                  classes={classes}
                  error={errors['attachment']}
                />
                <Grid className={classes.buttonGrid}>
                  <CustomizedButton
                    clicked={handleClose}
                    buttonText={messages.cancel}
                    buttonType={buttonType.cancel} />
                  <CustomizedButton
                    type='submit'
                    buttonText={messages.submit}
                    buttonType={buttonType.submit} />
                </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const addICQForm = reduxForm({
  form: 'addICQPartner',
  validate,
  enableReinitialize: true,
})(AddICQDialog);


const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(addICQForm);

export default withRouter(connected);
