import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../../common/customizedButton';
import {buttonType} from '../../../../helpers/constants'
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import AddCommentDialog from '../../common/AddCommentDialog'
import { CYCLE_STATUS, BODY_PARAMS, LIST_TYPE } from '../../../../helpers/constants'
import { reset } from 'redux-form';
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  title: 'Go to the next Stage',
  required: 'Comment is Required',
  success: 'Final List Successfully Approved !',
  message: 'Do you really want to promote the Project Selection to the next Stage?',
  error: 'Error',
  button: {
    FIN01: 'SEND FOR APPROVAL',
    FIN02: 'FINALIZE',
    LOAD: 'Loading...',
  },
  modal: {
    FIN01: {
      title: 'Send Final List for Approval',
      description: 'Please, provide a comment before sending the Final List to the next stage'
    },
    FIN02: {
      title: 'Finalize the Final List',
      description: 'Please, provide a comment before finalising the Final List'
    }
  }
}

const useStyles = makeStyles(theme => ({
}))

const Forward = (props) => {
  const {
    cycle,
    loadCycle,
    loading,
    postError,
    modifyProject,
    loadActivityList,
    successReducer,
    membership,
    reset
  } = props
  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const [text, setText] = useState({
    actionTitle: '',
    modalTitle: '',
    modalDesc: ''
  })
  const isHQReviewer = membership && membership.role === 'REVIEWER' && membership.type === 'HQ'
  const isHQApprover = membership && membership.role === 'APPROVER' && membership.type === 'HQ'

  useEffect(() => {
    setTitles()
    if (cycle && cycle.status) {
      if (
        ([CYCLE_STATUS.FIN01.code].includes(cycle.status.code) && isHQReviewer) ||
        ([CYCLE_STATUS.FIN02.code].includes(cycle.status.code) && isHQApprover)
      ) {
        setShow(true)
      } else {
        setShow(false)
      }
    }
  }, [cycle])

  const handleOpen = () => {
    setDisabled(false)
    setOpen(true)
  }

  const handleClose = () => {
    reset();
    setOpen(false)
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleForward = ({ comment }) => {
    if(!comment) {
    postError(messages.error, messages.required)
    return
  }
    setDisabled(true)
    if ([CYCLE_STATUS.FIN01.code, CYCLE_STATUS.FIN02.code].includes(cycle.status.code)) {
      let body = {
        status: cycle.status.id + 1,
        list_type: LIST_TYPE.final,
        comment,
        request_type: "promote"
      }
      modifyProject(cycle.year, body)
        .then(() => {
          loadActivityList(cycle.year, { page: 1 })
          loadCycle(cycle.year)
          successReducer(messages.success)
        })
        .catch((error) => { postError(error, messages.error) })
        .finally(() => { handleClose() })
    } else { postError(messages.error, messages.error) }
    handleClose()
  }

  const setTitles = () => {
    if (loading || !cycle) {
      setText({
        ...text,
        actionTitle: messages.button.LOAD
      })
    }
    if (cycle && cycle.status) {
      if (cycle.status.code == CYCLE_STATUS.FIN01.code) {
        setText({
          actionTitle: messages.button.FIN01,
          modalTitle: messages.modal.FIN01.title,
          modalDesc: messages.modal.FIN01.description,
        })
      } else if (cycle.status.code == CYCLE_STATUS.FIN02.code) {
        setText({
          actionTitle: messages.button.FIN02,
          modalTitle: messages.modal.FIN02.title,
          modalDesc: messages.modal.FIN02.description,
        })
      }
    }
  }

  return (
    <Fragment>
      {show && <CustomizedButton 
       clicked={handleOpen}
       buttonType={buttonType.submit} 
       buttonText={text.actionTitle} 
       disabled={loading} /> 
       }

      <AddCommentDialog
        title={text.modalTitle}
        description={text.modalDesc}
        onSubmit={handleForward}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Forward.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  loading: state.cycle.loading,
  membership: state.userData.data.membership
})

const mapDispatch = dispatch => ({
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'forward', message)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  reset: () => dispatch(reset('AddCommentDialogForm')),  
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Forward))
