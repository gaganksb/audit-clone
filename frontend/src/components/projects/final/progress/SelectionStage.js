import React, { Fragment } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import Forward from './Forward'
import Reject from './Reject'
import { CYCLE_STATUS } from '../../../../helpers/constants'

const SelectionStage = (props) => {
  const { cycle } = props
  return (
    <Fragment>
      <div>
        <Forward/>
        {(cycle && cycle.status && [CYCLE_STATUS.FIN02.id].includes(cycle.status.id)) && <Reject/>}
      </div>
    </Fragment>
  )
}

SelectionStage.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  membership: state.userData.data.membership
})

const mapDispatch = dispatch => ({
})

export default withRouter(connect(mapStateToProps, mapDispatch)(SelectionStage))
