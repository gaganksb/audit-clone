import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../../common/customizedButton';
import {buttonType} from '../../../../helpers/constants'
import { reset } from 'redux-form'
import AddCommentDialog from '../../common/AddCommentDialog'
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { CYCLE_STATUS, BODY_PARAMS, LIST_TYPE  } from '../../../../helpers/constants'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  button: 'REJECT',
  required: 'Comment is Required',
  title: 'Reject to previous Stage',
  message: 'Do you really want to reject the Project Selection to the previous Stage?',
  error: 'Error',
  modalTitle: 'Reject Final List',
  success: 'Final List Successfully Reverted !',
  modalDesc: 'Please, provide a comment before sending the Final List back to the previous stage'
}

const useStyles = makeStyles(theme => ({
}))

const Reject = (props) => {
  const {
    cycle,
    loadCycle,
    postError,
    reset,
    modifyProject,
    loadActivityList,
    membership,
    successReducer
  } = props

  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const isHQApprover = membership && membership.role === 'APPROVER' && membership.type === 'HQ'
  const isHQReviewer = membership && membership.role === 'REVIEWER' && membership.type === 'HQ'

  useEffect(() => {
    if (cycle && cycle.status) {
      if ([CYCLE_STATUS.FIN02.code].includes(cycle.status.code) && isHQApprover ||
      [CYCLE_STATUS.FIN01.code].includes(cycle.status.code) && isHQReviewer 
      ) { 
        setShow(true)
      } else {
        setShow(false)
      }
    }
  }, [cycle])

  const handleOpen = () => {
    setOpen(true)
    setDisabled(false)
    reset()
  }

  const handleClose = () => {
    setOpen(false)
    setDisabled(true)
    reset()
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleSubmit = (values) => {
    const { comment } = values
    if(!comment) {
      postError(messages.error, messages.required)
      return
    }
    setDisabled(true)

    if (cycle.status.code === CYCLE_STATUS.FIN02.code || cycle.status.code === CYCLE_STATUS.FIN01.code) {
      let body = {
        status: cycle.status.id - 1,
        list_type: LIST_TYPE.final,
        comment,
        request_type: "reject"
      }
      modifyProject(cycle.year, body)
        .then(() => {
          loadCycle(cycle.year)
          loadActivityList(cycle.year, { page: 1 })
          successReducer(messages.success)
        })
        .catch((error) => { postError(error, messages.error) })
        .finally(() => { handleClose() })
    } else { postError(messages.error, messages.error) }
    handleClose()
  }

  return (
    <Fragment>
      {show &&
        <CustomizedButton 
        clicked={handleOpen}
        buttonType={buttonType.cancel} 
        buttonText={messages.button}  /> }
      <AddCommentDialog
        title={messages.modalTitle}
        description={messages.modalDesc}
        onSubmit={handleSubmit}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Reject.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  query: ownProps.location.query,
  membership: state.userData.data.membership
})

const mapDispatch = dispatch => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'reject', message)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  reset: () => dispatch(reset('ExcludeProjectForm')),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Reject))
