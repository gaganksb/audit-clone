import React, { useState, useEffect, Fragment } from 'react'
import R from 'ramda'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle'
import { loadUser } from '../../../reducers/users/userData'
import { loadCommentList } from '../../../reducers/projects/projectsReducer/getCommentList'
import { loadActivityList } from '../../../reducers/projects/projectsReducer/getActivityList'
import FinalTable from './finalTable'
import ActivityTable from '../common/activityTable/index'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import CustomInfo from '../../common/customInfo'
import { CYCLE_STATUS } from '../../../helpers/constants'
import ButtonGroup from './buttonGroup'
import Title from '../../common/customTitle';
import ProjectsFilter from '../common/ProjectsFilter'
import { loadProjectList, importDeltas } from '../../../reducers/projects/projectsReducer/getProjectList';
import { loadSettings } from '../../../reducers/projects/projectsReducer/getSettings'
import ListLoader from '../../common/listLoader';
import ImportDeltaProjectsDialog from './ImportDeltaProjectsDialog';
import { showSuccessSnackbar } from "../../../reducers/successReducer";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { downloadURI } from '../../../helpers/others';

const headers = {
  reason: "Reason",
  bu: "Business Unit",
  agrNum: 'Agreement Number',
  country: "Country",
  partner: 'Partner',
  implNum: 'Implementer Number',
  budget: 'Budget (in USD)',
  installmentsPaid: 'Installments Paid',
  description: 'Description',
  impType: 'Implementer Type',
  projStartDate: 'ProjectStart Date',
  projEndDate: 'ProjectEnd Date',
  agrType: 'Agreement Type',
  agrDate: 'Agreement Date',
  operation: 'Operation'
}

const messages = {
  title: 'Final List',
  tableHeader: [
    { title: '', align: 'left', field: '' },
    { title: 'Comments(in manual modification)', align: 'left', field: 'comments' },
    { title: 'Business Unit', align: 'left', field: 'business_unit__code' },
    { title: 'Agreement Number', align: 'left', field: 'number' },
    { title: 'Is Delta', align: 'left', field: 'delta' },
    { title: 'Country', align: 'left', field: 'country__name' },
    { title: 'Partner', align: 'left', field: 'partner__legal_name' },
    { title: 'Implementer Number', align: 'left', field: 'partner__number' },
    { title: 'Budget', align: 'left', field: 'budget' },
    { title: 'Installments Paid', align: 'left', field: 'installments_paid' },
    { title: 'Description', align: 'left', field: 'description' },
    { title: 'Implementer Type', align: 'left', field: 'partner__implementer_type__implementer_type' },
    { title: 'ProjectStart Date', align: 'left', field: 'start_date' },
    { title: 'ProjectEnd Date', align: 'left', field: 'end_date' },
    { title: 'Agreement Type', align: 'left', field: 'agreement_type' },
    { title: 'Agreement Date', align: 'left', field: 'agreement_date' },
    { title: 'Operation', align: 'left', field: 'operation__code' },
    { title: 'Reason', align: 'left', field: 'pre_sense_check__reason' },
  ],
  error: 'Error',
  info: {
    notReady: 'The selected Final List is not yet ready for review',
    fin1: 'The selected Final List is under review',
    fin2: 'The Auditor Selection process have been started'
  }
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  infoRoot: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  infoPaper: {
    padding: theme.spacing(2),
  },
  buttonGrid: {
    margin: '24px 0 24px',
    display: 'flex',
    minWidth: 100,
    marginLeft: -16
  },
}))

const FinalList = (props) => {
  const {
    finalList,
    loading,
    loadFinalList,
    cycle,
    latestCycle,
    loadLatestCycle,
    loadCycle,
    loadUser,
    loadCommentList,
    loadActivityList,
    loadSettings,
    settings,
    membership,
    loadingLatestCycle,
    totalCount,
    query
  } = props
  const classes = useStyles()
  const [params, setParams] = useState({ page: 0 })
  const [sort, setSort] = useState({
    field: '',
    order: 'asc'
  })
  const [year, setYear] = useState(null)
  const [key, setKey] = useState('default')
  const [shouldLatestCycleLoaded, setShouldLatestCycleLoaded] = useState(true)
  const [readyForFinal, setReadyForFinal] = useState(false)
  const currentYear = new Date().getFullYear()
  const [delta, setDelta] = useState(false);
  const [openImportDialog, setOpenImportDialog] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [importLink, setImportLink] = useState("");

  useEffect(() => {
    loadUser()
  }, [])

  useEffect(() => {
    if (cycle && cycle.status) {
      setReadyForFinal(cycle.status.id === CYCLE_STATUS.FIN01.id)
      cycle.status.id === CYCLE_STATUS.FIN01.id && loadCommentList(query.year)
    }
  }, [cycle])

  useEffect(() => {
    const { pathName } = props;
    if (query.year) {
      setYear(query.year)
      loadCycle(query.year)
      loadActivityList(query.year, query)
      loadFinalList(query);
      loadSettings(query)
    }
    if ("year" in query === false) {
      setDelta(false);
      setYear(currentYear)
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          year: currentYear,
          status: "fin",
        })
      })
    }
  }, [query]);

  // useEffect(() => {
  //   if (!query.year && shouldLatestCycleLoaded) {
  //     loadLatestCycle(query)
  //       .then(setShouldLatestCycleLoaded(false))
  //   }
  //   if (query.year) {
  //     loadCycle(query.year, query)
  //     loadFinalList(query.year, query)
  //     readyForFinal && loadCommentList(query.year)
  //   } else if (year) {
  //     loadCycle(year, query)
  //     loadFinalList(year, query)
  //     readyForFinal && loadCommentList(year)
  //   }
  // }, [query])

  // useEffect(() => {
  //   if (query.year) {
  //     setYear(query.year)
  //   } else if (latestCycle) {
  //     setYear(latestCycle.year)
  //   }
  // }, [latestCycle])

  // useEffect(() => {
  //   if (year) {
  //     loadFinalList(year, query)
  //       .then(loadCycle(year, query))
  //     readyForFinal && loadCommentList()
  //   }
  // }, [year])

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }))
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ["asc", "desc"])[0]
    });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      })
    });
  }

  const parseReasons = (reasons) => {
    let res = 'fin__'
    if (reasons && reasons.length > 0) {
      for (let i = 0; i < reasons.length; i++) {
        res = res + reasons[i] + ','
      }
      res = res.slice(0, -1)
    }
    return res
  }

  function handleSearch(values) {
    const { pathName, query } = props
    const { partner, year, view_items, reasons, business_unit__code, number, } = values

    const ViewItems = 'fin__'.concat(view_items)

    const parsedReasons = parseReasons(reasons)
    setParams({ page: 0 })
    setYear(year)

    if (!reasons) {
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          partner,
          view_items: ViewItems,
          year,
          status: "fin",
          business_unit__code,
          number,
          delta: delta ? delta : undefined,
        }),
      })
    } else {
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          partner,
          view_items: ViewItems,
          year,
          reason: reasons,
          status: "fin",
          business_unit__code,
          number,
          delta: delta ? delta : undefined,
        }),
      })
    }
  }

  function handleInfo() {
    if (!cycle || [1, 2, 3, 4, 5].includes(cycle.status)) {
      return messages.info.notReady
    } else if (cycle.status === 6) {
      return messages.info.fin1
    } else if (cycle.status === 7) {
      return messages.info.fin2
    }
  }

  function onDeltaChange(e) {
    setDelta(e.target.checked)
  }

  function resetFilter() {
    const { pathName } = props

    setParams({ page: 0 })
    setDelta(false)
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        delta: undefined,
      },
    })
  }

  const closeImportDialog = () => {
    setOpenImportDialog(false);
  }

  const downloadSampleTemplate = () => {
    downloadURI('/api/common/data-import-template/', 'data-import-template.xlsx');
  }

  const setFileOnSelect = (value) => {
    setImportLink("");
    setSelectedFile(value);
  }

  const onUpload = () => {
    setOpenImportDialog(false);
    let body = new FormData();

    if (selectedFile != null && selectedFile != "") {
      body.append("import_file", selectedFile);
    } else if (importLink != null && importLink.trim() != "") {
      body.append("import_link", importLink);
    }

    body.append("year", year);
    body.append("data_types[0]", "msrp");
    body.append("is_delta", true);

    props.importDeltas(body).then(response => {
      setSelectedFile(null);
      setImportLink("")
      props.successReducer("Uploaded successfully !");
    }).catch(error => {
      setSelectedFile(null);
      setImportLink("")
      props.postError(error, "Error in upload !");
    })
  }

  const openDialog = () => {
    setOpenImportDialog(true);
  }

  const changeImportLink = (e) => {
    setSelectedFile(null);
    setImportLink(e.target.value);
  }

  return (
    <React.Fragment>
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
        <Title show={false} title={messages.title} />
        <div className={classes.root}>
          <ListLoader loading={loading}>
            <ProjectsFilter
              onSubmit={handleSearch}
              key={key}
              setKey={setKey}
              year={year}
              cycle={cycle}
              headers={headers}
              resetFilter={resetFilter}
              delta={delta}
              onDeltaChange={onDeltaChange}
            />
          </ListLoader>
          <Grid className={classes.buttonGrid}>
            {(totalCount > 0) && <ButtonGroup year={year} cycle={cycle} headers={headers} openDialog={openDialog} />}
          </Grid>

          <FinalTable
            messages={messages}
            sort={sort}
            items={finalList}
            loading={loading}
            totalItems={totalCount}
            page={params.page}
            handleChangePage={handleChangePage}
            handleSort={handleSort}
            year={year}
            membership={membership}
            settings={settings}
          />

          {/* <ActivityTable
          items={activities}
          count={totalActivityCount}
          loading={loadingActivity}
          loadData={loadActivityList}
        /> */}
        </div>
      </div>
      <ImportDeltaProjectsDialog open={openImportDialog}
        setFileOnSelect={setFileOnSelect}
        changeImportLink={changeImportLink}
        selectedFile={selectedFile}
        importLink={importLink}
        onClose={closeImportDialog} onUpload={onUpload}
        downloadSampleTemplate={downloadSampleTemplate} />
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  // finalList: state.finalList.final,
  // totalCount: state.finalList.totalCount,
  // loading: state.finalList.loading,
  finalList: state.projectList.project,
  settings: state.settings.details,
  totalCount: state.projectList.totalCount,
  loading: state.projectList.loading,
  cycle: state.cycle.details,
  latestCycle: state.latestCycle.details,
  loadingLatestCycle: state.latestCycle.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  membership: state.userData.data.membership,
  activities: state.activityList.activities,
  totalActivityCount: state.activityList.totalCount,
  loadingActivity: state.activityList.loading
})

const mapDispatch = dispatch => ({
  // loadFinalList: (year, params) => dispatch(loadFinalList(year, params)),
  // loadLatestCycle: (params) => dispatch(loadLatestCycle(params)),
  loadSettings: (params) => dispatch(loadSettings(params)),
  importDeltas: (body) => dispatch(importDeltas(body)),
  loadFinalList: (params) => dispatch(loadProjectList(params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  loadUser: () => dispatch(loadUser()),
  loadCommentList: (year, params) => dispatch(loadCommentList(year, params)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Final list', message)),
})

const connectedFinalList = connect(mapStateToProps, mapDispatch)(FinalList)
export default withRouter(connectedFinalList)
