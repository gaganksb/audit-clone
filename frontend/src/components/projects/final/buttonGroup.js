import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import SelectionStage from './progress/SelectionStage'
import CustomizedButton from '../../common/customizedButton';
import { buttonType, CYCLE_STATUS } from '../../../helpers/constants'
import { loadCsv } from '../../../reducers/projects/projectsReducer/downloadCSVs';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'

const useStyles = makeStyles(theme => ({
  buttonGrid: {
    display: 'flex',
    minWidth: 100
  },
  customizedButton: {
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 0,
    marginLeft: 16,
    marginRight: 16,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  }
}))


const exportText = "Export"
const successMessage = "Please check your inbox, An email will be sent with project list attached soon !"
const errors = "Something went wrong. Please try again !"

function ButtonGroup(props) {
  const classes = useStyles()
  const { year, csv, loadFinalCSV, postError, successReducer, loadCsvs, query, cycle } = props

  function loadCSVs() {
    let params = cycle && cycle.status && cycle.status.id;
    let url = '';

    // url =`/api/projects/export/${year}/?report_type=${params}`
    // downloadURI(url, 'Function Project Assignments')
    loadCsvs(year, params)
      .then((response) => {
        successReducer(successMessage)
      }).catch((error) => {
        postError(error, error.response && error.response.data || errors)
        throw new SubmissionError({
          _error: errors
        })
      })
  }

  return (
    <Grid className={classes.buttonGrid}>
      <CustomizedButton
        buttonType={buttonType.submit}
        clicked={(loadCSVs)}
        buttonText={exportText} />
      <SelectionStage />
    </Grid>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  csv: state.downloadCSV.csv,
  cycle: state.cycle.details,
  membership: state.userData.data.membership,
})

const mapDispatch = dispatch => ({
  loadCsvs: (year, type) => dispatch(loadCsv(year, type)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'csvError', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
})

const connected = connect(mapStateToProps, mapDispatch)(ButtonGroup)
export default withRouter(connected)
