import React, { useState, useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import IconButton from '@material-ui/core/IconButton';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';

const useStyles = makeStyles(theme => ({
    template: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(1),
        marginTop: '8',
    }
}))

const ImportDeltaProjectsDialog = (props) => {
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const [selectedFile, setSelectedFile] = useState("");
    const [importLink, setImportLink] = useState("");
    const [radioValue, setRadioValue] = useState('a');

    const handleChange = (e) => {
        setRadioValue(e.target.value)
        setSelectedFile("");
        setImportLink("");
    };

    useEffect(() => {
        setSelectedFile(props.selectedFile);
    }, [props.selectedFile])

    useEffect(() => {
        setImportLink(props.importLink);
    }, [props.importLink])

    const onFileChange = event => {
        props.setFileOnSelect(event.target.files[0]);
    };

    const isSubmitDisabled = () => {
        if (radioValue == 'a' && selectedFile && selectedFile != "") {
            return false;
        } else if (radioValue == 'b' && importLink && importLink.trim() != "") {
            return false;
        }
        return true;
    }

    return (
        <React.Fragment>
            <Dialog
                open={props.open}
                onClose={props.onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullScreen={fullScreen}
            >
                <DialogTitle id="alert-dialog-title">
                    <Typography variant="h6" color="primary" align="center">Import Delta Projects</Typography>
                    <Divider />
                </DialogTitle>
                <DialogContent>
                    <DialogContentText >
                        <Grid container spacing={2}>
                            <Grid item xs={12} container spacing={2}>
                                <Grid item xs={12} container>
                                    <Grid item xs={4}>

                                    </Grid>
                                    <Grid item xs={8}>
                                        <FormControl component="fieldset">
                                            <FormLabel component="legend">Choose import option</FormLabel>
                                            <RadioGroup aria-label="importOption" name="importOption" row value={radioValue} onChange={handleChange}>
                                                <FormControlLabel value="a" control={<Radio />} label="File upload" />
                                                <FormControlLabel value="b" control={<Radio />} label="Using link" />
                                            </RadioGroup>
                                        </FormControl>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} container spacing={2}>

                                    {radioValue == 'a' ?
                                        <React.Fragment>
                                            <Grid item xs={2}>

                                            </Grid>
                                            <Grid item xs={10} align="center">
                                                <input type="file" onChange={onFileChange}
                                                    id="contained-button-file" />
                                            </Grid>
                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            <Grid item xs={12} align="center">
                                                <input type="text" onChange={props.changeImportLink}
                                                    value={importLink}
                                                    id="contained-button-file" />
                                            </Grid>
                                        </React.Fragment>
                                    }
                                    <Grid item xs={12} align="center">
                                        <CustomizedButton
                                            buttonType={buttonType.cancel}
                                            buttonText={"Cancel"}
                                            type="submit"
                                            clicked={props.onClose}
                                        />
                                        <CustomizedButton
                                            buttonType={buttonType.submit}
                                            buttonText={radioValue == 'a' ? "Upload" : "submit"}
                                            type="submit"
                                            clicked={props.onUpload}
                                            disabled={isSubmitDisabled()}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <div className={classes.template}>
                                    <Typography variant="subtitle1" color="primary">Download Sample Template</Typography>
                                    <IconButton aria-label="download" size="large"
                                        onClick={props.downloadSampleTemplate} style={{ marginTop: -8 }}>
                                        <GetAppIcon fontSize="inherit" style={{ color: '#344563' }} />
                                    </IconButton>
                                </div>
                            </Grid>
                        </Grid>

                    </DialogContentText>
                </DialogContent>
            </Dialog>

        </React.Fragment>
    )
}

export default ImportDeltaProjectsDialog;
