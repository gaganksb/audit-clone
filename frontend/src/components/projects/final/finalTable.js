import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ListLoader from "../../common/listLoader";
import TablePaginationActions from "../../common/tableActions";
import { connect } from "react-redux";
import { withRouter, browserHistory as history } from "react-router";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { reset } from "redux-form";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { CYCLE_STATUS, LIST_TYPE } from "../../../helpers/constants";
import IncludeProject from "../common/IncludeProject";
import ExcludeProject from "../common/ExcludeProject";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  noDataRow: {
    margin: 16,
    width: "100%",
    textAlign: "center",
  },
  table: {
    minWidth: 500,
    maxWidth: "100%",
  },
  highRiskRow: {
    backgroundColor: "#ef8588 ",
  },
  headerRow: {
    backgroundColor: "#F6F9FC",
    border: "1px solid #E9ECEF",
    height: 60,
    font: "medium 12px/16px Roboto",
  },
  tableCell: {
    font: "14px/16px Roboto",
    letterSpacing: 0,
    color: "#344563",
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: "1px solid #E9ECEF",
    color: "#344563",
    font: "bold 16px/21px Roboto",
    letterSpacing: 0,
  },
  tableWrapper: {
    width: window.navigator.userAgent.includes("Windows") ? "95vw" : "97vw",
    maxHeight: 600,
    overflow: "auto",
    borderRadius: 6,
    marginRight: "-3px !important",
  },
  tableIncludeRow: {
    backgroundColor: "#F5DADA",
  },
  viewProject: {
    fontSize: 14,
    fontFamily: "Roboto",
    color: "#344563",
    borderBottom: "1px solid #E9ECEF",
    padding: "10px 26px 10px 24px",
  },
  tableRow: {
    "&:hover": {
      cursor: "pointer",
    },
  },
  tableHeader: {
    color: "#344563",
    fontFamily: "Roboto",
    fontSize: 14,
    transform: "translateY(-1px)",
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8,
  },
  tableCell: {
    font: "14px/16px Roboto",
    letterSpacing: 0,
    color: "#344563",
    borderBottom: "1px solid rgb(112,112,112)",
  },
  paginationSpacer: {
    flex: "none",
  },
  paginationCaption: {
    marginRight: theme.spacing(6),
  },
}));

const rowsPerPage = 10;

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    sort,
    messages,
    year,
    membership,
    settings,
    cycle,
  } = props;

  const classes = useStyles();
  const [headers, setHeaders] = React.useState(messages.tableHeader.slice(1));

  React.useEffect(() => {
    if (
      membership &&
      membership.role === "REVIEWER" &&
      cycle &&
      cycle.status &&
      cycle.status.id === CYCLE_STATUS.FIN01.id
    ) {
      setHeaders(messages.tableHeader);
    } else {
      setHeaders(messages.tableHeader.slice(1));
    }
  }, [membership, cycle]);

  const handleClassName = (item) => {
    if (item && item.fin_sense_check) {
      return classes.tableExcludeRow; // the naming convention is wrong
    } else {
      return classes.tableIncludeRow; // the naming convention is wrong
    }
  };

  const handleAction = (item) => {
    const include = (
      <TableCell className={classes.viewProject}>
        <IncludeProject listType={LIST_TYPE.final} project={item} year={year} />
      </TableCell>
    );
    const exclude = (
      <TableCell className={classes.viewProject}>
        <ExcludeProject listType={LIST_TYPE.final} project={item} year={year} />
      </TableCell>
    );

    if (item.fin_sense_check) {
      return exclude;
    } else {
      return include;
    }
  };

  const handleReason = (item) => {
    if (item && item.fin_sense_check) {
      return item.fin_sense_check.description;
    } else {
      if (
        settings &&
        settings.results &&
        settings.results[0] &&
        item.overall_risk_rating &&
        settings.results[0].risk_threshold <= item.overall_risk_rating
      ) {
        return "Project above risk threshold";
      }
      if (
        settings &&
        settings.results &&
        settings.results[0] &&
        item.overall_risk_rating &&
        settings.results[0].risk_threshold > item.overall_risk_rating
      ) {
        return "Project below risk threshold";
      } else if (!item.overall_risk_rating) {
        return "Risk rating not available Project included by default";
      }
    }
  };

  const repeatRows = (n) => {
    let items = [];
    for (let i = 1; i < n; i++) {
      items.push(i);
    }
    return items.map((item) => (
      <TableRow key={item}>
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
      </TableRow>
    ));
  };

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <Table className={classes.table}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>{messages.title}</div>
            <TableHead>
              <TableRow className={classes.headerRow}>
                {headers.map((item, key) => (
                  <TableCell
                    className={classes.tableHeader}
                    align={item.align}
                    key={key}
                    style={{
                      position: "sticky",
                      top: 0,
                      backgroundColor: "#F6F9FC",
                      zIndex: 2,
                    }}
                  >
                    {item.title == "" || item.title == "Sense Check" ? (
                      item.title
                    ) : (
                      <TableSortLabel
                        active={item.field === sort.field}
                        direction={sort.order}
                        onClick={() => handleSort(item.field, sort.order)}
                      >
                        {item.title}
                      </TableSortLabel>
                    )}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {!items || (items && items.length == 0 && !loading) ? (
              <div className={classes.noDataRow}>
                No data for the selected criteria
              </div>
            ) : (
              <TableBody>
                {items.map((item) => (
                  <TableRow key={item.id} className={handleClassName(item)}>
                    {membership &&
                      membership.role === "REVIEWER" &&
                      cycle &&
                      cycle.status &&
                      cycle.status.id === CYCLE_STATUS.FIN01.id
                      ? handleAction(item)
                      : null}
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.fin_comment ? item.fin_comment.message : "N/A"}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.business_unit}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.number}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.delta ? "Yes" : "No"}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.country}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      component="th"
                      scope="row"
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.partner.legal_name}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.partner.number}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      ${item.budget}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      ${item.installments_paid}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.description}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.partner.implementer_type.implementer_type}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.start_date}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.end_date}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.agreement_type}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.agreement_date}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {item.operation && item.operation.code}
                    </TableCell>
                    <TableCell
                      className={classes.viewProject}
                      style={{ whiteSpace: "nowrap" }}
                      onClick={() => history.push("/projects/" + `${item.id}`)}
                    >
                      {membership && membership.type === "HQ" && item.reason && item.reason.description}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            )}
          </div>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={1}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                classes={{
                  spacer: classes.paginationSpacer,
                  caption: classes.paginationCaption,
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
  membership: state.userData.data.membership,
});

const mapDispatchToProps = (dispatch) => ({
  resetCommentForm: () => dispatch(reset("commentDialog")),
  postError: (error, message) =>
    dispatch(errorToBeAdded(error, "editFinalProject", message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomPaginationActionsTable);

export default withRouter(connected);
