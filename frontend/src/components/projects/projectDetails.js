import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { loadProject } from '../../reducers/projects/projectsReducer/getProjectDetails';
import ListLoader from '../common/listLoader';
import ProjectOverview from './projectOverview';
import { loadUser } from '../../reducers/users/userData';
import ProjectDetailsTabs from './preliminary/ProjectDetailsTabs';
import { loadReportStatus } from '../../reducers/reports/getReportStatus.js'
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import { loadCycle } from '../../reducers/projects/projectsReducer/getCycle'
import EditProjectDetails from './editProjectDetails';
import { loadUsersList } from "../../reducers/users/usersList";
import { editProject } from '../../reducers/projects/projectsReducer/editProjectDetail'
import { editProjectGov } from '../../reducers/projects/projectsReducer/editGovDetails'
import { showSuccessSnackbar } from '../../reducers/successReducer';
import { errorToBeAdded } from '../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'
import { newLocations } from '../../reducers/projects/projectsReducer/postLocation'
import { reset } from 'redux-form';
import { editLocation } from '../../reducers/projects/projectsReducer/addLocation'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    minHeight: '240px',
  },
  typography: {
    marginLeft: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#606060'
  },
  typographybold: {
    font: 'bold 14px/19px Roboto',
    color: '#0072bc',
  },
  title: {
    flexGrow: 1,
    color: '#606060',
    font: 'bold 18px/24px Roboto',
  },
  overviewContainer: {
    padding: '25px 30px 15px',
    minHeight: 'unset'
  },
  detailsContainer: {
    marginTop: 40
  },
  titleContainer: {
    padding: '15px 30px',
    minHeight: 'unset'
  },
  titleWrapper: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      width: 20,
      height: 20,
      cursor: 'pointer',
      marginRight: 15
    }
  }
}));

const ProjectDetails = props => {
  const classes = useStyles();
  const { pathName, query, loadProjectDetails, loadUsersList, successReducer, postError, newLocations,
    project, loading, loadUserData, loadReportStatus, user, status, reset, editLocation, editProjectGov,
    loadCycle, year, cycle, editProjectDetail } = props;
  const id = pathName.split('/').slice(-2)[1];
  const [open, setOpen] = useState(false);
  const [formData, setFormData] = useState({});
  const [locations, locationUpdate] = useState([])
  const [locationList, updateLocationList] = useState([])

  const [initialBox, setInitial] = useState(JSON.stringify(project.audited_by_gov) == 'true' ? true : false)

  const messages = {
    success: 'Project Detail Successfully updated !',
    edit: 'Edit Project',
    error: 'Project Details Update failed !'
  }
  useEffect(() => {
    loadUserData();
    loadProjectDetails(id);
    loadReportStatus(id)
  }, [query]);

  useEffect(() => {
    if (project && project.cycle && project.cycle.budget_ref) {
      loadCycle(project.cycle.budget_ref)
    }
    if (project && project.locations) {
      locationUpdate(project.locations)
    }
    setInitial(JSON.stringify(project.audited_by_gov) == 'true' ? true : false)
  }, [project]);

  function handleClose() {
    setOpen(false)
  }

  const setInitialFormData = () => {

    //unhcr focal user
    if (project.agreement_member && project.agreement_member.unhcr && project.agreement_member.unhcr.focal) {
      setFormData(formData=>({
        ...formData,
        unhcr_focal: project.agreement_member.unhcr.focal
      }))
    }

    //alternate focal user
    if (project.agreement_member && project.agreement_member.unhcr && project.agreement_member.unhcr.alternate_focal) {
      setFormData(formData=>({
        ...formData,
        unhcr_focal_alternate: project.agreement_member.unhcr.alternate_focal
      }))
    }

    //partner focal user
    if (project.agreement_member && project.agreement_member.partner && project.agreement_member.partner.focal) {
      setFormData(formData=>({
        ...formData,
        partner_focal: project.agreement_member.partner.focal
      }))
    }

    //alternate partner focal user
    if (project.agreement_member && project.agreement_member.partner && project.agreement_member.partner.alternate_focal) {
      setFormData(formData=>({
        ...formData,
        partner_focal_alternate: project.agreement_member.partner.alternate_focal
      }))
    }
  }

  const handleSubmit = (values) => {
    let body = {}
    let auditor_reviewers = []
    let partner_reviewers = []
    if (JSON.stringify(values.audited_by_gov)) {
      body['audited_by_gov'] = JSON.stringify(values.audited_by_gov) == 'true' ? true : false
    }


    for (let [key, value] of Object.entries(formData)) {
      if (key === "auditor_reviewers") {
        value.forEach(item => {
          auditor_reviewers.push(item.id)
        })
      }
      else {
        if (value) {
          body[`${key}`] = `${value.id}`
        }
      }
    }
    (auditor_reviewers.length > 0) ? body.auditor_reviewers = auditor_reviewers : null;
    (partner_reviewers.length > 0) ? body.partner_reviewers = partner_reviewers : null;

    return editProjectDetail(id, body).then(() => {
      handleClose();
      successReducer(messages.success)
      loadProjectDetails(id);

      editLocation(id, { locations: locationList }).then(() => {
        updateLocationList([]);
        loadProjectDetails(id);
      })
    }).catch((error) => {
      postError(error, error.response && error.response.data.detail || messages.error)
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error
      })
    })
  }

  const handleChange = async (e, v) => {
    if (e && ['unhcr_focal', 'unhcr_focal_alternate'].includes(e.target.name)) {
      let params = { fullname: v, office_id: project.field_office }
      loadUsersList(params)
    }
    else if (e && ['partner_focal', 'partner_focal_alternate', 'partner_reviewers'].includes(e.target.name)) {
      let params = { fullname: v, partner: project.partner.id }
      loadUsersList(params)
    }

    else if (e && ['auditor_reviewers'].includes(e.target.name)) {
      let params = { fullname: v }
      loadUsersList(params)
    }
  }

  const handleInputChange = async (e, v, label) => {
    setFormData({
      ...formData,
      [label]: v
    })
  }

  return (
    <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
      <Paper className={`${classes.root} ${classes.titleContainer}`}>
        <div className={classes.titleWrapper}>
          <svg onClick={() => window.history.back()} fill="#0072bc" version="1.1" id="Capa_1" x="0px" y="0px" width="459px" height="459px" viewBox="0 0 459 459" style={{ enableBackground: 'new 0 0 459 459' }} xmlSpace="preserve">
            <g><g id="reply"><path d="M178.5,140.25v-102L0,216.75l178.5,178.5V290.7c127.5,0,216.75,40.8,280.5,130.05C433.5,293.25,357,165.75,178.5,140.25z" /></g></g>
          </svg>
          <Typography className={classes.title} >{project.title}</Typography>
          <span>
            {!loading && user && project &&
              (user.user_type === 'HQ' || (user.user_type === 'FO') || ((project.agreement_member && project.agreement_member.unhcr
                && project.agreement_member.unhcr.focal &&
                project.agreement_member.unhcr.focal.id) === user.userId))
              && <CustomizedButton
                buttonType={buttonType.submit}
                buttonText={messages.edit}
                clicked={() => { setOpen(true); reset(); setInitialFormData(); }}
              />}
          </span>
        </div>
      </Paper>
      <Paper className={`${classes.root} ${classes.overviewContainer}`}>
        <ListLoader loading={loading}>
          <ProjectOverview
            loading={loading}
            project={project}
          />
        </ListLoader>
      </Paper>
      <EditProjectDetails
        open={open}
        cycle={cycle}
        id={id}
        handleChange={handleChange}
        handleInputChange={handleInputChange}
        updateLocationList={updateLocationList}
        locationList={locationList}
        handleClose={handleClose}
        onSubmit={handleSubmit}
        loadProjectDetails={loadProjectDetails}
        locations={locations}
        locationUpdate={locationUpdate}
        initialBox={initialBox}
        setInitial={setInitial}
        project={project} />
      <Paper className={`${classes.root} ${classes.detailsContainer}`}>
        <ProjectDetailsTabs id={id} agreement={project.number} title={project.title} status={status}
          project={project} user={user} />
      </Paper>
    </div>
  )
};

const mapStateToProps = (state, ownProps) => ({
  project: state.projectDetails.project,
  loading: state.projectDetails.loading,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  status: state.reportStatus.status,
  cycle: state.cycle.details,
  user: state.session
});

const mapDispatch = dispatch => ({
  loadUserData: () => dispatch(loadUser()),
  loadProjectDetails: id => dispatch(loadProject(id)),
  loadReportStatus: (id, params) => dispatch(loadReportStatus(id, params)),
  loadUsersList: params => dispatch(loadUsersList(params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'projectDetailsError', message)),
  editProjectDetail: (id, body) => dispatch(editProject(id, body)),
  editProjectGov: (id, body) => dispatch(editProjectGov(id, body)),
  newLocations: (body) => dispatch(newLocations(body)),
  reset: () => dispatch(reset('ProjectDetails')),
  editLocation: (id, body) => dispatch(editLocation(id, body))
});

const connectedProjectDetails = connect(mapStateToProps, mapDispatch)(ProjectDetails);
export default withRouter(connectedProjectDetails);