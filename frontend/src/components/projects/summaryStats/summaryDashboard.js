import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import CustomLineChart from '../../common/customLineChart';
import SummaryDashboardTable from './summaryDashboardTable';
import { getSummaryData } from '../../../helpers/api/api';

const messages = {
  title: 'Summary Dashboard',
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '30%',
  },
  label: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
  },
  paper1: {
    color: theme.palette.text.secondary,
    height: '360px',
    marginBottom: '30px',
    borderRadius: '5px'
  },
  paper2: {
    color: theme.palette.text.secondary,
    overflow: 'auto',
    maxHeight: '350px',
    maxWidth: '100%',
    borderRadius: '5px'
  },
  lineTypes: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    '& > div': {
      marginRight: '15px',
      fontSize: '13px'
    },
    '& > div:nth-child(3)': {
      background: '#c3c6d1'
    },
    '& > .line-type': {
      marginRight: '15px',
      width: '50px',
      height: '3px',
      '&:first-child': {
        background: '#0070bf'
      },
    },
  },
  chartBottom: {
    height: '71px',
    backgroundColor: '#f6f9fc',
    marginTop: '-45px',
    borderTop: 'solid 1px #e9ecef',
    borderRadius: '0 0 10px 10px'
  }
}));

let tableRowHeadings = [
  "Projects implemented by all Partners",
  "Projects implemented by National NGOs international NGOs and Government Partners (excluding National Fundrasing Partners) - Auditable Projects",
  "Projects with risk ratings equal to or above the risk tolerance",
  "Remaining Projects, having Adverse or Disclaimer of Opinion in the past"
];
const tableRowTypes = ['all', 'ngo', 'ge_risk', 'adverse'];

const getFormattedData = (data) => {
  /* formatted data format
  const chartData = [
    [
      { name: 'Sept 2019', total: 123, pTotal: '', value: 234, pValue: '' },
      { name: 'Sept 2018', total: 345, pTotal: '', value: 464, pValue: '' },
      { name: 'Sept 2017', total: 345, pTotal: '', value: 464, pValue: '' },
      { name: 'Sept 2016', total: 345, pTotal: '', value: 464, pValue: '' },
    ],
    ...
  ]; */
  const month = 'Sept';
  const sortedData = data.sort((a, b) => a.year < b.year ? 1 : -1);
  let chartXValues = [];
  let chartYValues = [];
  let chartDataKeys = [
    {
      key: 'total',
      color: '#0070bf'
    }, {
      key: 'value',
      color: '#c3c6d1'
    }
  ];
  let tableHeaders = [''];
  let years = data.map(item => item.year);
  years.sort((a, b) => a < b ? 1 : -1);
  tableRowHeadings[3] = `Remaining Projects, having Adverse or Disclaimer of Opinion in the past, ${years[0] - 5} to ${years[0] - 1}`;
  let tableData = [
    [tableRowHeadings[0]],
    [tableRowHeadings[1]],
    [tableRowHeadings[2]],
    [tableRowHeadings[3]],
  ];
  let chartData = [[], [], [], []];
  let domain = [];
  let minVal = 1000000;
  let maxVal = 0;

  sortedData.forEach(item => {
    const keys = Object.keys(item);
    keys.forEach(key => {
      if (key !== 'year') {
        if (minVal > item[key].total) minVal = item[key].total;
        if (maxVal < item[key].total) maxVal = item[key].total;
        if (minVal > item[key].value) minVal = item[key].value;
        if (maxVal < item[key].value) maxVal = item[key].value;
        chartData[tableRowTypes.indexOf(key)].push({
          name: `${month} ${item.year}`, total: item[key].total, pTotal: '', value: item[key].value, pValue: ''
        });
        tableData[tableRowTypes.indexOf(key)].push({
          name: `${month} ${item.year}`, total: item[key].total, pTotal: '', value: item[key].value, pValue: ''
        });
      }
    });
  });

  // get table y-axis domain
  minVal = Math.floor(minVal / 500) * 500;
  maxVal = Math.ceil(maxVal / 500) * 500;
  domain = [minVal, maxVal];

  // get chart x-axis values
  years.forEach(year => {
    chartXValues.push({
      key: `${month} ${year}`,
      color: '#c3c6d1',
      label: ''
    });
  });
  
  // get chart y-axis values
  for (let yVal = minVal, i = 0; yVal <= maxVal; yVal += 500, i++) {
    chartYValues.push({
      key: yVal,
      color: '#eceff1',
      label: ''
    });
  }

  // get table headers
  tableHeaders = ['', ...chartXValues];

  return {
    chartXValues,
    chartYValues,
    chartDataKeys,
    tableHeaders,
    chartData,
    tableData,
    domain
  };
};

const summaryDashboard = () => {
  const [curItem, setCurItem] = useState(0);
  const loading = false;
  const classes = useStyles();
  const [values, setValues] = useState(getFormattedData([]));

  useEffect(() => {
    setValues(getFormattedData(getSummaryData()));
  }, []);

  return (
    <div className={classes.root}>
      <ListLoader loading={loading} >
        <Paper className={classes.paper1}>
          <div
            style={{
              paddingLeft: 22,
              paddingRight: 22,
              paddingTop: 19,
              borderBottom: '1px solid rgba(224, 224, 224, 1)',
              borderRadius: '10px 10px 0 0',
              color: '#606060',
              font: 'bold 18px/24px Roboto',
              letterSpacing: 0,
              backgroundColor: 'white',
              paddingBottom: 20,
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            {messages.title}
            <div className={classes.lineTypes}>
              <div className="line-type"/>
              <div># of PA</div>
              <div className="line-type"/>
              <div># Value of PAs</div>
            </div>
          </div>
          <CustomLineChart
            width="100%"
            height={270}
            data={values.chartData[curItem]}
            dataKeyX={'name'}
            dataKeys={values.chartDataKeys}
            xValues={values.chartXValues}
            yValues={values.chartYValues}
            domain={values.domain}
          />
          <div className={classes.chartBottom} />
        </Paper>
        <Paper className={classes.paper2}>
          <SummaryDashboardTable
            tableHeaders={values.tableHeaders}
            tableItems={values.tableData}
            curItem={curItem}
            onClick={(val) => setCurItem(val)}
          />
        </Paper>
      </ListLoader>
    </div>
  )
};

export default summaryDashboard;
