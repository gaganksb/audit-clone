import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { reset } from 'redux-form';
import { editProject } from '../../../reducers/projects/projectsReducer/editProject';
import { loadPreliminaryList } from '../../../reducers/projects/projectsReducer/getPreliminaryList';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  table: {
    minWidth: 500,
    maxWidth: '100%',
    '& thead tr': {
      height: 32,
      '& th': {
        padding: '0 10px',
        width: '28%',
        borderBottom: '1px solid #e9ecef',
        fontWeight: 600,
        '&:first-child': {
          width: '16%',
          textAlign: 'right',
          color: '#a5b1bf',
        }
      },
    },
    '& tbody tr': {
      height: 32,
      '& td': {
        padding: '5px 10px',
        width: '28%',
        borderLeft: 'solid 1px #c3c6d1',
        borderRight: 'solid 1px #c3c6d1',
        borderBottom: 'none',
        '&:first-child': {
          width: '16%',
          textAlign: 'right',
          fontWeight: 600
        },
        '&:last-child': {
          borderRight: 'none'
        }
      },
    },
    '& .tableFooterRow1': {
      backgroundColor: '#f6f9fc',
      borderTop: 'solid 1px #E9ECEF',
      borderLeft: 'solid 1px #E9ECEF',
      borderRight: 'solid 1px #E9ECEF',
      fontWeight: 600,
      '& > td': {
        padding: '10px 10px 0',
        border: 'none !important',
        '&:first-child': {
          textAlign: 'left'
        }
      }
    },
    '& .tableFooterRow2': {
      backgroundColor: '#f6f9fc',
      borderBottom: 'solid 1px #E9ECEF',
      borderLeft: 'solid 1px #E9ECEF',
      borderRight: 'solid 1px #E9ECEF',
      fontWeight: 600,
      '& > td': {
        padding: '0 10px 10px',
        border: 'none !important',
        '&:first-child': {
          textAlign: 'left'
        }
      }
    }
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 32,
    font: 'medium 12px/16px Roboto'
  },
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
  tableWrapper: {
    width: '95vw'
  },
  tableHeaderInnerWrapper: {
    display: 'flex',
    '& > div:nth-child(1)': {
      width: '35%',
    },
    '& > div:nth-child(2)': {
      width: '15%',
    },
    '& > div:nth-child(3)': {
      width: '15%',
    },
    '& > div:nth-child(4)': {
      width: '20%',
    },
    '& > div:nth-child(5)': {
      width: '15%',
    },
  },
  tableCellInnerWrapper: {
    display: 'flex',
    alignItems: 'center',
    '& > div:nth-child(1)': {
      width: '35%',
    },
    '& > div:nth-child(2)': {
      width: '15%',
    },
    '& > div:nth-child(3)': {
      width: '15%',
      color: '#60a6d7'
    },
    '& > div:nth-child(4)': {
      width: '20%',
    },
    '& > div:nth-child(5)': {
      width: '15%',
    },
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  tableRow: {
    height: 32,
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#a5b1bf'
  },
  columnHeadingCell: {
    color: '#a5b1bf'
  },
  tableCellBarContainer: {
    '& > div:last-child': {
      backgroundColor: '#344563'
    }
  },
  tableCellBar: {
    backgroundColor: '#0070bf',
    borderRadius: '0 5px 5px 0',
    marginLeft: -10,
    height: 10
  }
}));

const CustomTableHeaderCell = ({ data, classes }) => (
  data ?
    typeof data === "string" ? (
      <TableCell>{data}</TableCell>
    ) : (
    <TableCell className={classes.tableCell}>
      <div className={classes.tableHeaderInnerWrapper}>
        <div>{data[0]}</div>
        <div>{data[1]}</div>
        <div>{data[2]}</div>
        <div>{data[3]}</div>
        <div>{data[4]}</div>
      </div>
    </TableCell>
  ) : (
    <TableCell/>
  )
);

const CustomTableBodyCell = ({ data, classes }) => (
  typeof data !== 'string' ? (
    <TableCell className={classes.tableCell}>
      <div className={classes.tableCellInnerWrapper}>
        <div className={classes.tableCellBarContainer}>
          <div style={{ width: `${data.pTotal}%` }} className={classes.tableCellBar} />
          <div style={{ width: `${data.pValue}%` }} className={classes.tableCellBar} />
        </div>
        <div>{data.total}</div>
        <div>{data.pTotal}%</div>
        <div>{data.value}</div>
        <div>{data.pValue}%</div>
      </div>
    </TableCell>
  ) : (
    <TableCell className={classes.columnHeadingCell}>{ data }</TableCell>
  )
);

const CustomTableFooterCell = ({ data, classes }) => (
  typeof data !== 'string' ? (
    <TableCell className={classes.tableCell}>
      <div className={classes.tableCellInnerWrapper}>
        <div/>
        <div>{data.total}</div>
        <div/>
        <div>{data.value}</div>
        <div/>
      </div>
    </TableCell>
  ) : (
    <TableCell className={classes.columnHeadingCell}>{ data }</TableCell>
  )
);

function AnalysisRangeTable(props) {
  const {
    tableItems,
    tableHeaders,
    tableFooters
  } = props;

  const repeatRows = (n) => {
    let items = [];
    for (let i = 1; i < n; i++) {
      items.push(i)
    }
    return (
      items.map(item => (
        <TableRow key={item} >
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
        </TableRow>
      ))
    );
  };
  const classes = useStyles();

  return (
    <Table className={classes.table}>
      <TableHead className={classes.headerRow}>
        <TableRow>
          {tableHeaders.map((item, index) => (
            <CustomTableHeaderCell key={index} data={item} classes={classes} />
          ))}
        </TableRow>
      </TableHead>
      {
        (tableItems && tableItems.length) ?
          <TableBody>
            {tableItems.map((row, index) => (
              <TableRow
                key={index}
                className={classes.tableRow}
              >
                {row.map((item, index) => (
                  <CustomTableBodyCell key={index} data={item} classes={classes} />
                ))}
              </TableRow>
            ))}

            {(tableFooters && tableFooters.length) ?
              tableFooters.map((row, index) => (
                <TableRow
                  key={index}
                  className={`tableFooterRow${index + 1}`}
                >
                  {row.map((item, index) => (
                    <CustomTableFooterCell key={index} data={item} classes={classes} />
                  ))}
                </TableRow>
              )) : repeatRows(2)
            }
          </TableBody>
        : repeatRows(6)
      }
    </Table>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
});

const mapDispatchToProps = (dispatch) => ({
  editProject: (id, body) => dispatch(editProject(id, body)),
  loadPreliminary: (year, params) => dispatch(loadPreliminaryList(year, params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editProject', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AnalysisRangeTable);

export default withRouter(connected);
