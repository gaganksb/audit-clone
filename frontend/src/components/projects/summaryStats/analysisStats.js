import React, { useState, useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import ListLoader from "../../common/listLoader";
import AnalysisRangeTable from "./analysisRangeTable";
import AnalysisRiskTable from "./analysisRiskTable";

const messages = {
  title: 'Analysis Stats 2018 Dashboard',
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '30%',
  },
  label: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
  },
  paper1: {
    color: theme.palette.text.secondary,
    height: 400,
    marginBottom: 30,
    borderRadius: 5
  },
  paper2: {
    color: theme.palette.text.secondary,
    overflow: 'auto',
    maxHeight: 300,
    borderRadius: 5
  },
  tableWrapper: {
    overflowX: 'auto'
  },
  filterContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  svgContainer: {
    marginTop: 5,
    cursor: 'pointer',
    marginLeft: 10,
    height: '20px'
  },
  formControl: {
    minWidth: 120,
    fontSize: '12px',
    marginTop: -15
  }
}));

const rawData = [
  {
    "year": 2018,
    "budget_range": {
      "l_100k": {
        "total": 403,
        "value": 21.89
      },
      "ge_100k__l_500k": {
        "total": 403,
        "value": 21.89
      },
      "ge_500k__l_1M": {
        "total": 403,
        "value": 21.89
      },
      "ge_1M__l_5M": {
        "total": 403,
        "value": 21.89
      },
      "ge_5M__l_10M": {
        "total": 403,
        "value": 21.89
      },
      "ge_10M": {
        "total": 403,
        "value": 21.89
      }
    },
    "risk_range": {
      "min": 3.0,
      "max": 3.2,
      "stats": [
        {
          "threshold": 3.0,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.1,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.2,
          "total": 403,
          "value": 21.89
        }
      ]
    }
  }, {
    "year": 2017,
    "budget_range": {
      "l_100k": {
        "total": 403,
        "value": 21.89
      },
      "ge_100k__l_500k": {
        "total": 403,
        "value": 21.89
      },
      "ge_500k__l_1M": {
        "total": 403,
        "value": 21.89
      },
      "ge_1M__l_5M": {
        "total": 403,
        "value": 21.89
      },
      "ge_5M__l_10M": {
        "total": 403,
        "value": 21.89
      },
      "ge_10M": {
        "total": 403,
        "value": 21.89
      }
    },
    "risk_range": {
      "min": 3.0,
      "max": 3.2,
      "stats": [
        {
          "threshold": 3.0,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.1,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.2,
          "total": 403,
          "value": 21.89
        }
      ]
    }
  }, {
    "year": 2016,
    "budget_range": {
      "l_100k": {
        "total": 403,
        "value": 21.89
      },
      "ge_100k__l_500k": {
        "total": 403,
        "value": 21.89
      },
      "ge_500k__l_1M": {
        "total": 403,
        "value": 21.89
      },
      "ge_1M__l_5M": {
        "total": 403,
        "value": 21.89
      },
      "ge_5M__l_10M": {
        "total": 403,
        "value": 21.89
      },
      "ge_10M": {
        "total": 403,
        "value": 21.89
      }
    },
    "risk_range": {
      "min": 3.0,
      "max": 3.2,
      "stats": [
        {
          "threshold": 3.0,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.1,
          "total": 403,
          "value": 21.89
        },
        {
          "threshold": 3.2,
          "total": 403,
          "value": 21.89
        }
      ]
    }
  }
];
const yearList = rawData.map(item => item.year);

const rangeTableData = [
  [
    ">= 0 - < 1000,100", // "l_100k",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 16
    }, {
      year: 2017,
      total: 403,
      pTotal: 56,
      value: 21.89,
      pValue: 32
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
  [
    ">= 100,000 - < 500,000",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    }, {
      year: 2017,
      total: 403,
      pTotal: 45,
      value: 21.89,
      pValue: 67
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
  [
    ">= 500,000 - < 1,000,000",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 33
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 22
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
  [
    ">= 1,000,000 - < 5,000,000",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 44
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 21
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 58
    },
  ],
  [
    ">= 5,000,000 - < 10,000,000",
    {
      year: 2018,
      total: 403,
      pTotal: 89,
      value: 21.89,
      pValue: 45
    }, {
      year: 2017,
      total: 403,
      pTotal: 26,
      value: 21.89,
      pValue: 1
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 66
    },
  ],
  [
    ">= 10,000,000",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
];
const rangeTableFooterData = [
  [
    "Projects by INGO, NGO, GO", // "l_100k",
    {
      year: 2018,
      total: 403,
      value: 21.89,
    }, {
      year: 2017,
      total: 403,
      value: 21.89,
    },
    {
      year: 2016,
      total: 403,
      value: 21.89,
    },
  ],
  [
    "All Projects (incl. UN partners)",
    {
      year: 2018,
      total: 403,
      value: 21.89,
    }, {
      year: 2017,
      total: 403,
      value: 21.89,
    },
    {
      year: 2016,
      total: 403,
      value: 21.89,
    },
  ],
];
const genRangeTableHeader = () => (
  [
    'Range',
    ...yearList.map(year => ([
      year,
      '#',
      '%',
      'Value (Mil $)',
      '%'
    ]))
  ]
);

const riskTableData = [
  [
    "Risk Threshould 4.0",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    },
  ],
  [
    "Risk Threshould 3.5",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
  [
    "Risk Threshould 3.4",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
    },
  ],
  [
    "Risk Threshould 3.3",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#fd860e'
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#fd860e'
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#20bf0c'
    },
  ],
  [
    "Risk Threshould 3.2",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#ff4040'
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#ff4040'
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1,
      color: '#ff4040'
    },
  ],
  [
    "Risk Threshould 3.1",
    {
      year: 2018,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    }, {
      year: 2017,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
    {
      year: 2016,
      total: 403,
      pTotal: 23,
      value: 21.89,
      pValue: 1
    },
  ],
];
const genRiskTableHeader = () => (
  [
    'PAs by National NGOs, Intl. NGOs and Government Partners',
    ...yearList.map(year => ([
      year,
      'Number',
      '% of total',
      '#of projects',
      `${year} Value`,
      `(Mil$)`,
      '% of total',
      'Project Value',
    ]))
  ]
);

const analysisStats = () => {
  const loading = false;
  const classes = useStyles();
  const [labelWidth, setLabelWidth] = React.useState(0);
  const inputLabel = React.useRef(null);

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  return (
    <div className={classes.root}>
      <ListLoader loading={loading} >
        <Paper className={classes.paper1}>
          <div
            style={{
              paddingLeft: 22,
              paddingRight: 22,
              paddingTop: 19,
              borderBottom: '1px solid rgba(224, 224, 224, 1)',
              borderRadius: '10px 10px 0 0',
              color: '#606060',
              font: 'bold 18px/24px Roboto',
              letterSpacing: 0,
              backgroundColor: 'white',
              paddingBottom: 20,
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
          >
            {messages.title}
            <div className={classes.filterContainer}>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel style={{ fontSize: '13px', padding: 5, color: '#0072bc' }} ref={inputLabel} htmlFor="outlined-age-simple">
                  Preliminary List
                </InputLabel>
                <Select
                  onChange={() => {}}
                  style={{ width: 160, fontSize: '13px' }}
                  inputProps={{
                    name: 'confidenceLevel',
                    id: 'outlined-age-simple',
                  }}
                >
                  <MenuItem value="default">
                    <em>None</em>
                  </MenuItem>
                  <MenuItem value={1}>1</MenuItem>
                  <MenuItem value={2}>2</MenuItem>
                  <MenuItem value={3}>3</MenuItem>
                </Select>
              </FormControl>
              <div className={classes.svgContainer}>
                <svg height="22px" id="Layer_1" style={{enableBackground: 'new 0 0 32 32'}} version="1.1" viewBox="0 0 32 32" width="32px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"><path fill="#344563" d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
              </div>
              <div className={classes.svgContainer}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path fill="#344563" d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/></svg>
              </div>
            </div>
          </div>
          <div className={classes.tableWrapper}>
            <AnalysisRangeTable
              tableHeaders={genRangeTableHeader()}
              tableItems={rangeTableData}
              tableFooters={rangeTableFooterData}
            />
          </div>
        </Paper>
        <Paper className={classes.paper2}>
          <AnalysisRiskTable
            tableHeaders={genRiskTableHeader()}
            tableItems={riskTableData}
          />
        </Paper>
      </ListLoader>
    </div>
  )
};

export default analysisStats;
