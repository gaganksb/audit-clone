import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { reset } from 'redux-form';
import { editProject } from '../../../reducers/projects/projectsReducer/editProject';
import { loadPreliminaryList } from '../../../reducers/projects/projectsReducer/getPreliminaryList';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  table: {
    minWidth: 500,
    maxWidth: '100%',
    '& th': {
      padding: '20px',
      width: '260px',
      borderBottom: '1px solid #e9ecef',
      fontWeight: '600',
      position: 'sticky',
      top: '0px',
      backgroundColor: 'rgb(246, 249, 252)',
      zIndex: 2
    },
    '& td': {
      padding: '20px',
      minWidth: '260px',
      borderLeft: 'solid 1px #c3c6d1',
      borderRight: 'solid 1px #c3c6d1',
      borderBottom: '1px solid #e9ecef'
    },
    '& tbody tr td:first-child': {
      minWidth: '360px',
      padding: '20px 30px',
    },
    '& tbody tr td:last-child': {
      borderRight: 'none',
    }
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
  tableWrapper: {
    width: '95vw'
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14
  },
  tableRow: {
    cursor: 'pointer',
    '&:hover': {
      background: '#f2f3f8',
    }
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#a5b1bf'
  },
  tableCellInnerWrapper: {
    display: 'flex',
    '& > div': {
      width: '50%',
      display: 'flex',
      '& > div': {
        width: '50%',
      }
    }
  },
  columnHeadingCell: {
    color: '#a5b1bf'
  },
  tableActiveRow: {
    background: '#e4e5ea',
    '& > td': {
      fontWeight: '600',
    }
  }
}));

const CustomTableHeaderCell = ({ data, classes }) => (
  data ? (
    <TableCell className={classes.tableCell}>
      <div>
        <div>
          Final List - {data.key}
        </div>
        <div className={classes.tableCellInnerWrapper}>
          <div># of PA %</div>
          <div>Value of PAs %</div>
        </div>
      </div>
    </TableCell>
  ) : (
    <TableCell/>
  )
);

const CustomTableBodyCell = ({ data, classes }) => (
  typeof data !== 'string' ? (
    <TableCell className={classes.tableCell}>
      <div className={classes.tableCellInnerWrapper}>
        <div>
          <div>{data.total}</div>
          <div>{data.pTotal}</div>
        </div>
        <div>
          <div>{data.value}</div>
          <div>{data.pValue}</div>
        </div>
      </div>
    </TableCell>
  ) : (
    <TableCell className={classes.columnHeadingCell}>{ data }</TableCell>
  )
);

function SummaryDashboardTable(props) {
  const {
    tableItems,
    tableHeaders,
    curItem,
    onClick,
  } = props;

  const repeatRows = (n) => {
    let items = [];
    for (let i = 1; i < n; i++) {
      items.push(i)
    }
    return (
      items.map(item => (
        <TableRow key={item} >
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
        </TableRow>
      ))
    );
  };
  const classes = useStyles();

  return (
    <Table className={classes.table}>
      <div className={classes.tableWrapper}>
        <TableHead className={classes.headerRow}>
          <TableRow>
            {tableHeaders.map((item, index) => (
              <CustomTableHeaderCell key={index} data={item} classes={classes} />
            ))}
          </TableRow>
        </TableHead>
        {
          (tableItems && tableItems.length) ?
            <TableBody>
            {tableItems.map((row, index) => (
              <TableRow
                key={index}
                className={`${classes.tableRow} ${curItem === index ? classes.tableActiveRow : ''}`}
                onClick={() => onClick(index)}
              >
                {row.map((item, index) => (
                  <CustomTableBodyCell key={index} data={item} classes={classes} />
                ))}
              </TableRow>
            ))}
            </TableBody>
          : repeatRows(5)
        }
      </div>
    </Table>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
});

const mapDispatchToProps = (dispatch) => ({
  editProject: (id, body) => dispatch(editProject(id, body)),
  loadPreliminary: (year, params) => dispatch(loadPreliminaryList(year, params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editProject', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SummaryDashboardTable);

export default withRouter(connected);
