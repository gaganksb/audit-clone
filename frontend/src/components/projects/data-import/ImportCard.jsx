import React, { useState } from 'react';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import { downloadURI } from '../../../helpers/others';
import { showSuccessSnackbar } from "../../../reducers/successReducer";
import { errorToBeAdded } from "../../../reducers/errorReducer";
import { importDeltas } from '../../../reducers/projects/projectsReducer/getProjectList';
import ListLoader from "../../common/listLoader";

const useStyles = makeStyles(theme => ({
  template: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: theme.spacing(1),
    marginTop: '4',
  }
}))

const currentYear = new Date().getFullYear();

const ImportCard = (props) => {
  const classes = useStyles();
  const [selectedFile, setSelectedFile] = useState(null);
  const [year, setYear] = useState(currentYear);
  const onFileChange = event => {
    setSelectedFile(event.target.files[0]);
  };
  const onYearChange = event => {
    setYear(event.target.value);
  };

  const onSubmit = () => {
    let body = new FormData();

    if (selectedFile != null && selectedFile != "") {
      body.append("import_file", selectedFile);
    }

    if (props.importType) {
      body.append("import_type", props.importType);
    }

    body.append("year", year);
    body.append("data_types[0]", props.dataType);
    if (props.isDelta) {
      body.append("is_delta", props.isDelta);
    }

    props.importDeltas(body).then(response => {
      setSelectedFile(null);
      props.successReducer("Uploaded successfully !");
    }).catch(error => {
      setSelectedFile(null);
      props.postError(error, "Error in upload !");
    })
  }

  const onCancel = () => {
    setSelectedFile(null);
  }

  const isSubmitDisabled = () => {
    if (selectedFile && selectedFile != "") {
      return false;
    }
    return true;
  }

  const downloadSampleTemplate = () => {
    downloadURI(props.templateLink, props.fileName);
  }

  return (
    <div style={{ width: 300, margin: 30, }}>
      <ListLoader loading={props.loading}>
        <Card>
          <CardHeader title={props.title} />
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <InputLabel>Year</InputLabel>
                <input placeholder="Year" label="Year" type="number" name="year" value={year}
                  min="2014" onChange={onYearChange} />

              </Grid>
              <Grid item xs={12}>
                <input type="file" onChange={onFileChange} />
              </Grid>

              <Grid item xs={12}>
                <div className={classes.template}>
                  <Typography variant="subtitle1" color="primary">Download Template</Typography>
                  <IconButton aria-label="download" size="large"
                    onClick={downloadSampleTemplate} style={{ marginTop: -8 }}>
                    <GetAppIcon fontSize="inherit" style={{ color: '#344563' }} />
                  </IconButton>
                </div>
              </Grid>
            </Grid>
          </CardContent>
          <CardActions>
            <Grid container>
              <Grid item xs={12}>
                <div style={{ marginLeft: -8 }}>
                  <CustomizedButton
                    buttonType={buttonType.cancel}
                    buttonText={"Cancel"}
                    type="submit"
                    clicked={onCancel}
                    disabled={isSubmitDisabled()}
                  />
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Submit"}
                    type="submit"
                    clicked={onSubmit}
                    disabled={isSubmitDisabled()}
                  />
                </div>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </ListLoader>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  loading: state.projectList.loading,
})

const mapDispatch = dispatch => ({
  importDeltas: (body) => dispatch(importDeltas(body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Final list', message)),
})

export default connect(mapStateToProps, mapDispatch)(ImportCard);
