import React from 'react';
import Grid from '@material-ui/core/Grid';
import ImportCard from './ImportCard';

const DataImportSections = props => {
  return <React.Fragment>
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6} md={4}>
        <ImportCard title={"Add Delta Projects"} isDelta
          templateLink={"/api/common/data-import-template/?type=msrp"}
          fileName={"data-import-template.xlsx"}
          dataType={"msrp"} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <ImportCard title={"Add Projects"} isDelta={false}
          templateLink={"/api/common/data-import-template/?type=msrp"}
          fileName={"data-import-template.xlsx"}
          dataType={"msrp"} />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <ImportCard title={"Add Survey Questions"}
          templateLink={"/api/common/data-import-template/?type=survey"}
          fileName={"survey-questions-template.xlsx"}
          dataType={"survey"}
          importType={"import_survey"} />
      </Grid>

      <Grid item xs={12} sm={6} md={4}>
        <ImportCard title={"Add Users"}
          templateLink={"/api/common/data-import-template/?type=users"}
          fileName={"users.xlsx"}
          dataType={"users"} />
      </Grid>
    </Grid>
  </React.Fragment>
}

export default DataImportSections;