import React, { Component } from 'react';
import Title from '../common/customTitle';
import { browserHistory as history, withRouter } from 'react-router';

const messages = {
  title: 'Project management',
}


class ProjectsWrapper extends Component {
  
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.goTo = this.goTo.bind(this);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  goTo = () => item => {
    history.push(item);
  }

  render() {
    const { children } = this.props;

    return (
      <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
        { children }
      </div>
    );
  }
}

  
  
export default withRouter(ProjectsWrapper);
