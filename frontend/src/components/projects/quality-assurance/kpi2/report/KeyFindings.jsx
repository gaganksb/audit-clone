import React, { useState, useEffect } from 'react';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { makeStyles } from "@material-ui/core/styles";
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import TablePaginationActions from '../../kpi3/report/TablePaginationActions';
import TablePagination from '@material-ui/core/TablePagination';
import listLoader from '../../../../common/listLoader';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  label: {
    color: "#0072bc !important",
  },
  tabContainer: {
    '& button': {
      color: '#0072bc !important',
    },
    '& > div > div > span': {
      backgroundColor: '#0072bc !important'
    },
    '& .Mui-selected': {
      backgroundColor: '#0072bc !important',
      color: '#FFFFFF !important'
    }
  }
}));


const Row = (props) => {
  const [open, setOpen] = useState(false);
  const [categories, setCategories] = useState([]);
  const [tabValue, setTabValue] = React.useState(0);
  const [row, setRow] = useState({});
  const classes = useStyles();

  useEffect(() => {
    if (props.row) {
      setRow(props.row);
    }
  }, [props.row])

  useEffect(() => {
    findCategories();
  }, [])

  const findCategories = () => {
    if (row && row.key_findings && row.key_findings.length > 0) {
      let identifiedCategories = new Set();
      for (let item in row.key_findings) {
        let question = row.key_findings[item];
        if (question) {
          identifiedCategories.add(question.question__category__title);
        }
      }
      setCategories([...identifiedCategories]);
      setTabValue(identifiedCategories)
    }
  }

  useEffect(() => {
    if (categories && categories.length > 0) {
      setTabValue(categories[0]);
    } else {
      findCategories();
    }
  }, [categories])

  const getCategoryQuestions = (category) => {
    return row.key_findings.filter(question => question.question__category__title == category)
  }

  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };


  const setupCategoryWiseTable = () => {
    if (categories && categories.length == 0) {
      findCategories();
    }
    return <TableRow>
      <TableCell style={{ paddingBottom: 0, paddingTop: 0, }} colSpan={5}>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <Box sx={{ margin: 1 }}>
            {row.key_findings && row.key_findings.length == 0 ?
              <React.Fragment>
                No data found
              </React.Fragment>
              :
              <React.Fragment>
                <Paper className={classes.root}>
                  <Tabs onChange={handleTabChange} value={tabValue}
                    indicatorColor="primary"
                    textColor="primary"
                    className={classes.tabContainer}
                    variant="fullWidth">
                    {categories.map((item, index) => <Tab key={index} label={item} value={item} style={{ border: "1px solid gray", margin: 5, }} />)}
                  </Tabs>

                  <Table size="small" aria-label="purchases">
                    <TableHead>
                      <TableRow>
                        {getCategoryQuestions(tabValue).map((question, index) => <TableCell key={index}>{question.question__title__english}</TableCell>)}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        {getCategoryQuestions(tabValue).map((question, index) => <TableCell key={index}>{question.answer.answer}</TableCell>)}
                      </TableRow>
                    </TableBody>
                  </Table>
                </Paper>
              </React.Fragment>
            }

          </Box>
        </Collapse>
      </TableCell>
    </TableRow>

  }


  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.business_unit && row.business_unit.code}</TableCell>
        <TableCell align="center">{row.number}</TableCell>
        <TableCell align="center">{row.business_unit && row.business_unit.field_office && row.business_unit.field_office.name}</TableCell>
        <TableCell align="center">{row.partner && row.partner.legal_name}</TableCell>
      </TableRow>
      {open && setupCategoryWiseTable()}
    </React.Fragment>
  );
}

const CollapsibleKeyFindingsTable = (props) => {
  const [keyFindings, setKeyFindings] = useState([]);
  const [rowsPerPage] = useState(10);
  const [count, setCount] = useState(0);
  useEffect(() => {
    if (props.keyFindings) {
      setKeyFindings([...props.keyFindings]);
      setCount(props.count);
    }
  }, [props.keyFindings, props.count]);
  const classes = useStyles();

  return (
    <listLoader loading={props.loading}>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell align="center" className={classes.label}>Business Unit</TableCell>
              <TableCell align="center" className={classes.label}>Agreement Number</TableCell>
              <TableCell align="center" className={classes.label}>Field Office</TableCell>
              <TableCell align="center" className={classes.label}>Partner</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {keyFindings.map((row, index) => (
              <Row key={index} row={row} />
            ))}
          </TableBody>
        </Table>
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <TablePagination
            rowsPerPageOptions={[10]}
            colSpan={1}
            count={count}
            rowsPerPage={rowsPerPage}
            page={props.page}
            SelectProps={{
              native: true,
            }}
            onChangePage={props.handleChangePage}
            ActionsComponent={TablePaginationActions}
          />
        </div>
      </TableContainer>
    </listLoader>
  );
}

export default CollapsibleKeyFindingsTable;
