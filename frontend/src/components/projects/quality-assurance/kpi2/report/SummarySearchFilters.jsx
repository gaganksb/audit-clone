import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Field, reduxForm, change, } from "redux-form";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";

import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType, filterButtons } from "../../../../../helpers/constants";
import ListLoader from "../../../../common/listLoader";
import CustomAutoCompleteField from "../../../../forms/TypeAheadFields";
import renderTypeAhead from "../../../../common/fields/commonTypeAhead";
import { renderTextField } from "../../../../common/renderFields";

import { loadPartnerList } from "../../../../../reducers/partners/partnerList";
import { loadBUList } from "../../../../../reducers/projects/oiosReducer/buList";
import { SENSE_CHECKS } from "../../../../../helpers/constants";
import {
  renderMultiSelectField,
  renderRadioField,
} from "../../../../../helpers/formHelper";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(6),
  },
  header: {
    marginTop: theme.spacing(3),
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: "93%",
    marginTop: 2,
  },
  inputLabel: {
    fontSize: 14,
    color: "#344563",
    marginTop: 18,
    paddingLeft: 14,
  },
  typeAhead: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    marginBottom: 8,
    width: "90%",
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: "92%",
  },
  btnPadding: {
    marginTop: theme.spacing(3),
    marginLeft: -theme.spacing(2),
    display: 'flex',
    justifyContent: 'flex-start',
  },
  radio: {
    marginLeft: theme.spacing(2),
  },
  radioButton: {
    "&.Mui-checked": {
      color: "#0072BC",
    },
  },
}))

const messages = {
  radio: {
    included: {
      value: "included",
      label: "Included",
    },
    excluded: {
      value: "excluded",
      label: "Excluded",
    },
    all: {
      value: "all",
      label: "All",
    },
  },
};

const updateOptionsList = (view_items) => {
  let res = [];
  for (let prop in SENSE_CHECKS) {
    if (
      view_items == messages.radio.included.value &&
      SENSE_CHECKS[prop] == SENSE_CHECKS.manually_excluded
    ) {
      continue;
    } else if (
      view_items == messages.radio.excluded.value &&
      SENSE_CHECKS[prop] != SENSE_CHECKS.manually_excluded
    ) {
      continue;
    } else {
      res.push(SENSE_CHECKS[prop]);
    }
  }
  return res;
};

const SummarySearchFilter = props => {
  const classes = useStyles();
  const [partner, setSelectedPartner] = useState([]);
  const [partnerLists, setPartnerList] = useState([]);
  const { key, loading, handleSubmit, partnerList, BUList, loadBUList,
    loadPartnerList, onResetFilter, reasons, } = props;
  const partner_type_to_search = "partner__legal_name";
  const [optionsList, setOptionsList] = useState(
    updateOptionsList(messages.radio.all.value)
  );
  const [selectedReasons, setSelectedReasons] = useState([]);
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());

  const handleValueChange = (val) => {
    setSelectedPartner(val);
  };

  const handleBuChange = async (e, v) => {
    if (e) {
      let params = {
        code: v,
      };
      const bus = await loadBUList(params);
    }
  };

  function handleChangePartner(e, v) {
    if (v) {
      let params = {
        legal_name: v,
      };
      loadPartnerList(params);
    }
  }

  const handleInputChange = (e) => { };

  const handleSelect = (event) => {
    setSelectedReasons(event.target.value);
    reasons(event.target.value);
  };

  const handleYearChange = (e) => {
    setSelectedYear(e.target.value);
  };

  useEffect(() => {
    setPartnerList(partnerList);
  }, [partnerList]);

  const getDescriptionEval = (p) => {
    var yearExpr = "";
    var mySubString = p.substring(p.lastIndexOf("{") + 1, p.lastIndexOf("}"));
    if (mySubString != "") {
      var myArr = mySubString
        .replaceAll("current_year", selectedYear)
        .split(";");

      var startYearArr = myArr[0].split("-");
      var startYear = startYearArr[0] - startYearArr[1];
      yearExpr = startYear;

      if (myArr.length > 1) {
        var endYearArr = myArr[1].split("-");
        var endYear = endYearArr[0] - endYearArr[1];
        yearExpr = yearExpr + "-" + endYear;
      }
      p = p.replace("{", "");
      p = p.replace("}", "");
      p = p.replace(mySubString, yearExpr);
    }

    return p;
  };

  const updateReasons = (view_items, reasons) => {
    let res = [];
    if (view_items == messages.radio.included.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] != SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i]);
        }
      }
    } else if (view_items == messages.radio.excluded.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] == SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i]);
        }
      }
    } else {
      res = reasons;
    }
    return res;
  };

  const checkSelection = (event) => {
    let updatedReasons = updateReasons(event.target.value, selectedReasons);
    setSelectedReasons(updatedReasons);
    reasons(updatedReasons);
    setOptionsList(updateOptionsList(event.target.value));
  };

  const radioOptions = (() => {
    let res = [];
    for (let prop in messages.radio) {
      res.push(messages.radio[prop]);
    }
    return res;
  })();

  return (
    <ListLoader loading={loading}>
      <React.Fragment>
        <Paper className={classes.root}>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>

              <Grid item xs={12} sm={12} md={3}>
                <InputLabel className={classes.inputLabel}>
                  <Typography variant="subtitle2" color="primary">Partner Name</Typography>
                </InputLabel>
                <Field
                  name="partner"
                  key={key}
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChangePartner}
                  items={partnerLists}
                  comingValue={partner}
                  type_to_search={partner_type_to_search}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={3}>
                <InputLabel className={classes.inputLabel}>
                  <Typography variant="subtitle2" color="primary">Year</Typography>
                </InputLabel>
                <Field
                  name="year"
                  margin="normal"
                  type="number"
                  key={key}
                  component={renderTextField}
                  className={classes.textField}
                  onChange={handleYearChange}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={3}>
                <InputLabel className={classes.inputLabel}>
                  <Typography variant="subtitle2" color="primary">Business Unit</Typography>
                </InputLabel>
                <Field
                  name="business_unit__code"
                  margin="normal"
                  component={renderTypeAhead}
                  key={key}
                  className={classes.typeAhead}
                  onChange={handleBuChange}
                  handleInputChange={handleInputChange}
                  options={BUList ? BUList : []}
                  getOptionLabel={(option) => option.code}
                  multiple={false}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={3}>
                <InputLabel className={classes.inputLabel}>
                  <Typography variant="subtitle2" color="primary">Agreement Number</Typography>
                </InputLabel>
                <Field
                  name="number"
                  margin="normal"
                  type="number"
                  key={key}
                  component={renderTextField}
                  className={classes.textField}
                />
              </Grid>

              <Grid item xs={12} sm={4}>
                <Field
                  name="view_items"
                  component={renderRadioField}
                  checkSelection={checkSelection}
                  className={classes.radio}
                  margin="normal"
                >
                  {radioOptions.map((option, index) => (
                    <FormControlLabel
                      key={index}
                      style={{ display: "inline-block" }}
                      value={option.value}
                      control={
                        <Radio
                          className={classes.radioButton}
                          style={{ display: "inline-block" }}
                          onClick={checkSelection}
                        />
                      }
                      label={option.label}
                      labelPlacement="end"
                    />
                  ))}
                </Field>
              </Grid>

              <Grid item xs={12} sm={5}>
                <InputLabel className={classes.inputLabel}>
                  <Typography variant="subtitle2" color="primary">Reasons</Typography>
                </InputLabel>
                <Field
                  name="reasons"
                  multiple={false}
                  style={{ width: "470px" }}
                  options={selectedReasons}
                  handleChange={handleSelect}
                  component={renderMultiSelectField}
                  className={classes.formControl}
                  margin="normal"
                >
                  {optionsList.map((option) => (
                    <MenuItem
                      key={option.id}
                      value={option.code}
                      style={{ fontSize: "12.5px" }}
                      className={classes.menu}
                    >
                      {getDescriptionEval(option.description)}
                    </MenuItem>
                  ))}
                </Field>
              </Grid>
            </Grid>

            <div className={classes.btnPadding}>
              <CustomizedButton
                buttonType={buttonType.submit}
                buttonText={filterButtons.search}
                clicked={handleSubmit}
              />
              <CustomizedButton
                buttonType={buttonType.submit}
                reset={true}
                clicked={() => onResetFilter()}
                buttonText={filterButtons.reset}
              />
            </div>

          </form>

        </Paper>
      </React.Fragment>
    </ListLoader>
  )
}

const Kpi2SearchFilterForm = reduxForm({
  form: "Kpi2SearchFilterForm",
})(SummarySearchFilter);

const mapStateToProps = (state) => {
  return {
    initialValues: {

    },
    partnerList: state.partnerList.list,
    BUList: state.BUListReducer.bu,

  };
};

const mapDispatchToProps = (dispatch) => ({
  reasons: (value) => dispatch(change("Kpi2SearchFilterForm", "reasons", value)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Kpi2SearchFilterForm);

export default connected;