import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues } from "redux-form";
import Paper from "@material-ui/core/Paper";
import Typography from '@material-ui/core/Typography';
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { browserHistory as history } from "react-router";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ListLoader from "../../../../common/listLoader";
import FinalizeDialog from '../../kpi4/report/FinalizeDialog';
import SummarySearchTable from "./SummarySearchTable";
import SummarySearchFilter from './SummarySearchFilters';
import { updateSummaryReportApi, getKpi2SummaryReportApi, } from "../../../../../reducers/qualityAssurance/kpi2Reducer";


const useStyles = makeStyles((theme) => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  headerItems: {
    display: "flex",
    alignItems: "center",
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "flex-end",
  },
  backTick: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
    resize: 'vertical',
  },
  label: {
    color: "#0072bc !important",
  },
}));


const required = value => value ? undefined : 'Required*'

const renderTextAreaField = ({ input, label, type, disabled, rows, className, meta: { touched, error, warning } }) => (
  <React.Fragment>
    <div>
      {touched && ((error && <span style={{ color: 'red' }}>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
    <div>
      <label>{label}</label>
      <div>
        <textarea {...input} type={type} rows={rows} className={className} disabled={disabled} />
      </div>
    </div>
  </React.Fragment>
)

const Kpi2SummaryReport = (props) => {
  const classes = useStyles();
  const [openFinalDialog, setOpenFinalDialog] = useState(false);
  const [formValues, setFormValues] = useState(null);
  const { session, handleSubmit, updateSummaryReportApi, getKpi2SummaryReportApi, successReducer, } = props;
  const [isFinalized, setIsFinalized] = useState(false);

  const backHandler = () => {
    history.goBack();
  };

  useEffect(() => {
    const reportId = props.location.query.report_id;
    if (reportId) {
      getKpi2SummaryReportApi(reportId).then(response => {
        if (response) {
          props.initialize({
            introduction: response.introduction,
            purpose: response.purpose,
            overall_summary: response.overall_summary,
          });
          setIsFinalized(response.finalized);
        }
      });
    }
  }, [])

  const save = (values) => {
    saveOrFinalize(values, "save")
  };

  const saveOrFinalize = (values, action) => {
    let body = {
      "introduction": values.introduction,
      "purpose": values.purpose,
      "overall_summary": values.overall_summary,
      action,
    }

    if (action === "finalize") {
      body.finalized = true;
    }

    const reportId = props.location.query.report_id;

    if (reportId) {
      updateSummaryReportApi(reportId, body).then(response => {
        successReducer(`${action} successful !`);
        if (action === "finalize") {
          setIsFinalized(response.finalized);
        }
      });
    } else {
      postError(error, `${action} failed`);
    }
  }

  const onFinalizeDialogOpen = (values) => {
    setFormValues(values)
    setOpenFinalDialog(true);
  }

  const onCloseFinalizeDialog = () => {
    setFormValues(null)
    setOpenFinalDialog(false)
  }

  const onFinalize = () => {
    setOpenFinalDialog(false)
    saveOrFinalize(formValues, "finalize")
  }


  const notify = () => {
  }

  const exportReport = () => {

  }

  const isHQUser = session.user_type == "HQ";
  const searchSubmitHandler = values => {
    console.log(values);
  }


  return (
    <ListLoader loading={props.loading}>
      <React.Fragment>
        <Paper className={classes.header} variant="outlined">
          <div className={classes.headerItems}>
            <div className={classes.backTick}>
              <ArrowBackIosIcon onClick={backHandler} />
              <label>
                {props.location.query.firm_name} - Final Report {props.location.query.year}
              </label>
            </div>

            {isHQUser && (
              <div className={classes.btnGrpAlign}>
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Notify"}
                  clicked={notify}
                  disabled={isFinalized}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Export"}
                  clicked={exportReport}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Save"}
                  clicked={handleSubmit(save)}
                  disabled={isFinalized}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Finalize"}
                  clicked={handleSubmit(onFinalizeDialogOpen)}
                  disabled={isFinalized}
                />
              </div>
            )}
          </div>
        </Paper>

        <Paper className={classes.root} variant="outlined">
          <SummarySearchFilter onSubmit={searchSubmitHandler} />
          <SummarySearchTable />
        </Paper>

        <Paper className={classes.root} variant="outlined">
          <form>
            <Grid container spacing={3}>
              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="introduction"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Introduction'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="purpose"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Purpose'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="overall_summary"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Overall Summary of Audit Results'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

            </Grid>
          </form>
        </Paper>


        <div className={classes.root}>
          <Typography variant="subtitle2">Modified opinions  and financial findings by region</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Region
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Reports Year
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Audited year US$
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Modified
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          %
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Unsupported Amount US$
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          %
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Timing or Budget compliance amounts US$
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          %
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Total Year US$
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          %
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>
                      <TableRow>
                        <TableCell align="center">Africa</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Asia and Pacific</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Americas</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Europe</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>Total</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Audit opinions by type and region</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Type of opinion
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Africa
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Asia and Pacific
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Americas
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Europe
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          Year
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>
                      <TableRow>
                        <TableCell align="center">unmodified</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Qualified</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Adverse</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Disclaimer</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>Total Audits Year</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>% Modified Year</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>Total Audits Previous Year</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>% Modified Previous Year</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Financial findings by region</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Region
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Reports
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Findings
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total Findings
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>
                      <TableRow>
                        <TableCell align="center">Africa</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Asia and Pacific</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Americas</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Europe</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center">Mena</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell align="center" className={classes.label}>Total</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Internal control findings by type</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Internal control finding type
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Findings
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total Findings
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>

                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Compliance findings by region</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Region
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Findings
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total Findings
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>

                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Other findings by region</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Region
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Reports
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Findings
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total Findings
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>

                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>

        <div className={classes.root}>
          <Typography variant="subtitle2">Other findings by type</Typography>
          <Paper variant="outlined">
            <Grid container>
              <Grid item xs={12}>
                <TableContainer>
                  <Table aria-label="simple table">
                    <TableHead>
                      <TableRow style={{ backgroundColor: "#F8F8F8" }}>

                        <TableCell align="center" className={classes.label}>
                          Region
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          # Findings
                        </TableCell>
                        <TableCell align="center" className={classes.label}>
                          % of Total Findings
                        </TableCell>
                      </TableRow>
                    </TableHead>

                    <TableBody>

                    </TableBody>
                  </Table>
                </TableContainer>
              </Grid>
            </Grid>
          </Paper>
        </div>


        <FinalizeDialog open={openFinalDialog} onClose={onCloseFinalizeDialog} onFinalize={onFinalize} />
      </React.Fragment>
    </ListLoader >

  );
};

const Kpi2SummaryReportForm = reduxForm({
  form: "Kpi2SummaryReport",
})(Kpi2SummaryReport);

const mapStateToProps = (state) => {
  return {
    session: state.session,
    formValues: getFormValues('Kpi2SummaryReport')(state),
  };
};

const mapDispatchToProps = (dispatch) => ({
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
  updateSummaryReportApi: (id, body) => dispatch(updateSummaryReportApi(id, body)),
  getKpi2SummaryReportApi: (id) => dispatch(getKpi2SummaryReportApi(id)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Kpi2SummaryReportForm);

export default connected;
