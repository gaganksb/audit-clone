import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues, reset, } from "redux-form";
import Paper from "@material-ui/core/Paper";
import Typography from '@material-ui/core/Typography';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { browserHistory as history } from "react-router";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ListLoader from "../../../../common/listLoader";
import FinalizeDialog from '../../kpi4/report/FinalizeDialog';
import {
  updateFinalReportApi, getKpi2FinalReportApi,
  getKpi2KeyFindingsApi,
} from "../../../../../reducers/qualityAssurance/kpi2Reducer";
import CollapsibleKeyFindingsTable from "./KeyFindings";
import PartnerFilterForm from "../report/PartnerFilterForm";


const useStyles = makeStyles((theme) => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  headerItems: {
    display: "flex",
    alignItems: "center",
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "flex-end",
  },
  backTick: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
    resize: 'vertical',
  },
  label: {
    color: "#0072bc !important",
  },
}));


const required = value => value ? undefined : 'Required*'

const renderTextAreaField = ({ input, label, type, disabled, rows, className, meta: { touched, error, warning } }) => (
  <React.Fragment>
    <div>
      {touched && ((error && <span style={{ color: 'red' }}>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
    <div>
      <label>{label}</label>
      <div>
        <textarea {...input} type={type} rows={rows} className={className} disabled={disabled} />
      </div>
    </div>
  </React.Fragment>
)


const Kpi2Report = (props) => {
  const classes = useStyles();
  const [openFinalDialog, setOpenFinalDialog] = useState(false);
  const [formValues, setFormValues] = useState(null);
  const { session, handleSubmit, updateFinalReportApi, postError,
    getKpi2FinalReportApi, successReducer, getKpi2KeyFindingsApi, } = props;
  const [isFinalized, setIsFinalized] = useState(false);
  const [responseData, setResponseData] = useState({});
  const [keyFindings, setKeyFindings] = useState([]);
  const [key, setKey] = useState("default");
  const [count, setCount] = useState(0);
  const [page, setPage] = React.useState(0);

  const backHandler = () => {
    history.goBack();
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    const firmId = props.location.query.firm_id;
    let params = { page: newPage + 1, firm_id: firmId };
    loadFindings(params);
  };

  useEffect(() => {
    const reportId = props.location.query.report_id;
    if (reportId) {
      getKpi2FinalReportApi(reportId).then(response => {
        if (response) {
          setResponseData(response);
          props.initialize({
            background: response.background,
            scope_and_objective: response.scope_and_objective,
            methodology: response.methodology,
          });
          setIsFinalized(response.finalized);
        }
      });
    }

    const firmId = props.location.query.firm_id;
    if (firmId) {
      let params = { firm_id: firmId, page: 1, }
      loadFindings(params);
    }
  }, [])

  const loadFindings = params => {
    getKpi2KeyFindingsApi(params).then(response => {
      if (response && response.results) {
        setKeyFindings(response.results);
        setCount(response.count);
      } else {
        setCount(0);
        postError(error, "Key findings fetch failed !");
      }

    }).catch(error => {
      setCount(0);
      postError(error, "Key findings fetch failed !");
    })
  }

  const save = (values) => {
    saveOrFinalize(values, "save");
  };

  const saveOrFinalize = (values, action) => {

    let body = {
      "background": values.background,
      "scope_and_objective": values.scope_and_objective,
      "methodology": values.methodology,
      action,
    }

    if (action === "finalize") {
      body.finalized = true;
    }

    const reportId = props.location.query.report_id;

    if (reportId) {
      updateFinalReportApi(reportId, body).then(response => {
        successReducer(`${action} successful !`);
        if (action === "finalize") {
          setIsFinalized(response.finalized);
        }
      });
    } else {
      postError(error, `${action} failed`);
    }


  }

  const onFinalizeDialogOpen = (values) => {
    setFormValues(values)
    setOpenFinalDialog(true);
  }

  const onCloseFinalizeDialog = () => {
    setFormValues(null)
    setOpenFinalDialog(false)
  }

  const onFinalize = () => {
    setOpenFinalDialog(false)
    saveOrFinalize(formValues, "finalize")
  }


  const notify = () => {
  }

  const exportReport = () => {

  }

  const isHQUser = session.user_type == "HQ";

  const getTotalValue = (workingPapers, propertyName) => {
    let total = 0;
    if (workingPapers) {
      workingPapers.map(record => {
        let weekName = Object.getOwnPropertyNames(record);
        total = total + record[weekName][propertyName];
      })
    }
    return total;
  }

  const handlePartnerSearch = (values) => {
    const { business_unit__code, number, partner } = values;
    const firmId = props.location.query.firm_id;
    let params = { business_unit__code, number, partner, firm_id: firmId, page: 1, };
    loadFindings(params);
    setPage(0);
  }

  const handlePartnerReset = () => {
    setKey(Math.floor(Math.random() * 10000));
    const firmId = props.location.query.firm_id;
    let params = { firm_id: firmId };
    props.resetPartnerFilterForm();
    loadFindings(params);
    setPage(0);
  }


  return (

    <ListLoader loading={props.loading}>
      <React.Fragment>
        <Paper className={classes.header} variant="outlined">
          <div className={classes.headerItems}>
            <div className={classes.backTick}>
              <ArrowBackIosIcon onClick={backHandler} />
              <label>
                {props.location.query.firm_name} - Final Report {props.location.query.year}
              </label>
            </div>

            {isHQUser && (
              <div className={classes.btnGrpAlign}>
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Notify"}
                  clicked={notify}
                  disabled={isFinalized}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Export"}
                  clicked={exportReport}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Save"}
                  clicked={handleSubmit(save)}
                  disabled={isFinalized}
                />
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Finalize"}
                  clicked={handleSubmit(onFinalizeDialogOpen)}
                  disabled={isFinalized}
                />
              </div>
            )}
          </div>
        </Paper>

        <Paper className={classes.root} variant="outlined">
          <form>
            <Grid container spacing={3}>
              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="background"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Background'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="scope_and_objective"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Scope & Objectives'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

              <Grid container item>
                <Grid item xs={12}>
                  <Field
                    name="methodology"
                    component={renderTextAreaField}
                    type="text"
                    rows="7"
                    className={classes.input}
                    validate={required}
                    label={'Methodology and Sampling Selection'}
                    disabled={isFinalized}
                    validate={[required]}
                  />
                </Grid>
              </Grid>

            </Grid>
          </form>
        </Paper>


        <div className={classes.root}>
          <Typography variant="subtitle2">Key findings and Recommendations</Typography>
          <Paper variant="outlined">
            <PartnerFilterForm onSubmit={handlePartnerSearch} onResetFilter={handlePartnerReset} key={key} />
            <CollapsibleKeyFindingsTable keyFindings={keyFindings}
              loadFindings={loadFindings} count={count}
              handleChangePage={handleChangePage}
              page={page}
              loading={props.loading} />
          </Paper>
        </div>


        <div className={classes.root}>
          <Paper variant="outlined">
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow style={{ backgroundColor: "#F8F8F8" }}>
                    <TableCell></TableCell>
                    <TableCell align="center" className={classes.label}>Report to review / new reports issued  during WK</TableCell>
                    <TableCell align="center" className={classes.label}>Number of WP's received</TableCell>
                    <TableCell align="center" className={classes.label}>WP's completed QA review</TableCell>
                    <TableCell align="center" className={classes.label}>Number of WP's under progress</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    {responseData && responseData.working_paper_progress && responseData.working_paper_progress.map((record, index) => {
                      const weekName = Object.getOwnPropertyNames(record)
                      return <React.Fragment key={index}>
                        <TableCell align="center" className={classes.label}>{weekName[0]}</TableCell>
                        <TableCell align="center">{record[weekName].requested}</TableCell>
                        <TableCell align="center">{record[weekName].received}</TableCell>
                        <TableCell align="center">{record[weekName].reviewed}</TableCell>
                        <TableCell align="center">{record[weekName].total_wp_in_prog}</TableCell>
                      </React.Fragment>
                    })}
                  </TableRow>
                  <TableRow>
                    <TableCell align="center" className={classes.label}>Total</TableCell>
                    <TableCell align="center">{getTotalValue(responseData.working_paper_progress, "requested")}</TableCell>
                    <TableCell align="center">{getTotalValue(responseData.working_paper_progress, "received")}</TableCell>
                    <TableCell align="center">{getTotalValue(responseData.working_paper_progress, "reviewed")}</TableCell>
                    <TableCell align="center">{getTotalValue(responseData.working_paper_progress, "total_wp_in_prog")}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </div>

        <FinalizeDialog open={openFinalDialog} onClose={onCloseFinalizeDialog} onFinalize={onFinalize} />
      </React.Fragment>
    </ListLoader >

  );
};

const Kpi2FinalReportForm = reduxForm({
  form: "Kpi2FinalReportForm",
})(Kpi2Report);

const mapStateToProps = (state) => {
  return {
    session: state.session,
    formValues: getFormValues('Kpi2FinalReportForm')(state),
  };
};

const mapDispatchToProps = (dispatch) => ({
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
  updateFinalReportApi: (id, body) => dispatch(updateFinalReportApi(id, body)),
  getKpi2FinalReportApi: (id) => dispatch(getKpi2FinalReportApi(id)),
  getKpi2KeyFindingsApi: (params) => dispatch(getKpi2KeyFindingsApi(params)),
  resetPartnerFilterForm: () => dispatch(reset('Kpi2PartnerFilterForm')),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Kpi2FinalReportForm);

export default connected;
