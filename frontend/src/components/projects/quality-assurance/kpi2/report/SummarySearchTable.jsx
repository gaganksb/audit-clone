import React, { useState, useEffect } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import TablePaginationActions from '../../kpi3/report/TablePaginationActions';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const SummarySearchTable = props => {
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = React.useState(0);
  const [count, setCount] = useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  return <React.Fragment>
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Business Unit</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Agreement Number</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Country</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Partner</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Implementer Number</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Budget(in USD)</Typography>
            </TableCell>
            <TableCell align="center">
              <Typography variant="subtitle2" color="primary">Installments Paid</Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>


        </TableBody>
      </Table>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <TablePagination
          rowsPerPageOptions={[5]}
          colSpan={1}
          count={count}
          rowsPerPage={rowsPerPage}
          page={page}
          SelectProps={{
            native: true,
          }}
          onChangePage={handleChangePage}
          ActionsComponent={TablePaginationActions}
        />
      </div>
    </TableContainer>
  </React.Fragment>
}

export default SummarySearchTable;