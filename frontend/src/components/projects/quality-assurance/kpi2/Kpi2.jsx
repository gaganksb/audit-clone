import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import SearchFilters from './SearchFilters';
import SearchTable from './SearchTable';
import CustomizedButton from "../../../common/customizedButton";
import { buttonType } from "../../../../helpers/constants";
import ListLoader from "../../../common/listLoader";
import { startKpi2SurveyApi, getKpi2AuditFirmsApi, checkSurveyUploadStatusApi, } from '../../../../reducers/qualityAssurance/kpi2Reducer';
import { showSuccessSnackbar } from "../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../reducers/errorReducer";
import MuiAlert from '@material-ui/lab/Alert';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(3),
  },
  buttonGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: -theme.spacing(2),
    display: "flex",
    width: "100%",
  },
}));

const defaultYear = new Date().getFullYear();

const KPI2 = (props) => {
  const { startKpi2SurveyApi, postError, successReducer, loading, getKpi2AuditFirmsApi, checkSurveyUploadStatusApi, } = props;
  const classes = useStyles();
  const [year, setYear] = useState(defaultYear);
  const [key, setKey] = useState("default");
  const [firmResults, setFirmResults] = useState([]);
  const [isSurveyQuestionsUploaded, setIsSurveyQuestionsUploaded] = useState(true);

  useEffect(() => {
    let values = { legal_name: null, year: defaultYear };
    handleSearch(values);
  }, [])

  useEffect(() => {
    checkSurveyUploadStatusApi(year).then(response => {
      let isAuditQualitySurveyQuestionsUploaded = response.audit_quality_survey;
      setIsSurveyQuestionsUploaded(isAuditQualitySurveyQuestionsUploaded);
    })
  }, [year])

  const reportHandler = (reportType, firm_id, firm_name, report_id,) => {
    history.push(
      `/quality-assurance/kpi2-report/${reportType}/?year=${year}&firm_id=${firm_id}&firm_name=${firm_name}&report_id=${report_id}`
    );
  };


  const handleProjectSampling = () => {
    startKpi2SurveyApi(year).then(response => {
      successReducer("Survey started !");
      let values = { legal_name: null, year: defaultYear };
      handleSearch(values);
    }).catch(error => {
      postError(error, "KPI2 survey start failed");
    });
  }

  const handleSearch = values => {
    const { legal_name, year } = values;
    setYear(year);
    getKpi2AuditFirmsApi(legal_name, year).then(response => {
      if (response && response.length > 0) {
        setFirmResults([...response]);
      } else {
        setFirmResults([]);
      }

    }).catch(error => {
      setFirmResults([]);
    });
  }

  const resetFilterForm = () => {
    setKey(Math.floor(Math.random() * 10000));
    setYear(defaultYear);
  };

  return <ListLoader loading={loading}>
    <div>
      <SearchFilters onSubmit={handleSearch} year={year}
        resetFilterForm={resetFilterForm}
        key={key} />

      {(!isSurveyQuestionsUploaded && !loading) ?
        <Alert severity="warning">Please complete audit work before starting quality assurance !</Alert>
        :
        <React.Fragment>
          {!loading && props.session.user_type == "HQ" && (!firmResults || firmResults.length == 0) &&
            <div className={classes.buttonGrid}>
              <CustomizedButton
                buttonType={buttonType.submit}
                clicked={handleProjectSampling}
                buttonText={"Sample Projects"}
              />
            </div>
          }
          <SearchTable reportHandler={reportHandler} firmResults={firmResults} />
        </React.Fragment>
      }


    </div>
  </ListLoader>
};

const mapStateToProps = (state) => {
  return {
    loading: state.Kpi2Reducer.loading,
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  startKpi2SurveyApi: (year) =>
    dispatch(startKpi2SurveyApi(year)),
  getKpi2AuditFirmsApi: (auditFirmName, year) =>
    dispatch(getKpi2AuditFirmsApi(auditFirmName, year)),
  checkSurveyUploadStatusApi: (year) =>
    dispatch(checkSurveyUploadStatusApi(year)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'KPI2-Error', message)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(KPI2);

export default connected;
