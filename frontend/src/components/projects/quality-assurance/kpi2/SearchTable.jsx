import React, { useState, useEffect } from 'react';
import Tooltip from "@material-ui/core/Tooltip";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import NoData from '../../preliminary/noData';
import { buttonType, } from "../../../../helpers/constants";
import CustomizedButton from "../../../common/customizedButton";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(8),
  },
  label: {
    color: "#0072bc !important",
  },
  listFlexContainer: {
    display: "flex",
    flexDirection: "row",
    padding: 0,
    "overflow-wrap": "break-word",
  },
}));

const Kpi2SearchTable = (props) => {
  const classes = useStyles();
  const [firmData, setFirmData] = useState([]);

  useEffect(() => {
    if (props.firmResults) {
      setFirmData([...props.firmResults])
    }
  }, [props.firmResults])


  return (<React.Fragment>
    <TableContainer className={classes.root} component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center" className={classes.label}>
              Audit Firm
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Final Report
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Summary Report
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Status
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {firmData && firmData.length > 0
            ? firmData.map((row, index) => (
              <TableRow key={index}>
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  width="20%"
                >
                  {row.audit_agency.legal_name}
                </TableCell>
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  width="10%">
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"View"}
                    clicked={() => props.reportHandler("final", row.audit_agency.id, row.audit_agency.legal_name, row.final_reports.id)}
                  />
                </TableCell>
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  width="10%">
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"View"}
                    clicked={() => props.reportHandler("summary", row.audit_agency.id, row.audit_agency.legal_name, row.final_reports.id)}
                  />
                </TableCell>
                <TableCell align="center" width="10%">
                  {row.status == "todo" ? (
                    <Tooltip title={row.status}>
                      <ReportProblemIcon style={{ color: "yellow" }}>
                        {row.status}
                      </ReportProblemIcon>
                    </Tooltip>
                  ) : row.status == "done" ? (
                    <Tooltip title={row.status}>
                      <CheckCircleIcon style={{ color: "green" }}>
                        {row.status}
                      </CheckCircleIcon>
                    </Tooltip>
                  ) : (
                    row.status
                  )}
                </TableCell>
              </TableRow>
            ))
            : <TableRow><TableCell colSpan={3}><NoData noData /></TableCell></TableRow>}
        </TableBody>
      </Table>
    </TableContainer>
  </React.Fragment>)
}

export default Kpi2SearchTable;