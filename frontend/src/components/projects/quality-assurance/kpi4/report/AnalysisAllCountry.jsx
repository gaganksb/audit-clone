import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import PerCountry from "./PerCountry";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
}));

const AnalysisResult = (props) => {
  const classes = useStyles();
  const [countries, setCountries] = useState([]);
  const [criteriaList, setCriteriaList] = useState([]);
  useEffect(() => {
    if (props.countries && props.countries.length > 0) {
      setCountries((oldArray) => [...oldArray, ...props.countries]);
    }
  }, [props.countries]);

  useEffect(() => {
    if (props.criterias && props.criterias.length > 0) {
      setCriteriaList((oldArray) => [...oldArray, ...props.criterias]);
    }
  }, [props.criterias]);

  let countryItems = countries.map((record, index) => (
    <PerCountry
      key={index}
      country={record.country}
      team_composition_id={record.team_composition}
      team_composition_country_criterias={record.team_composition_country_criterias}
      criteriaList={criteriaList}
    />
  ));
  return (
    <div className={classes.root}>
      <Typography variant="h6" gutterBottom>
        Team Composition Analysis Results per Country Audited
      </Typography>
      {countryItems}
    </div>
  );
};

export default AnalysisResult;
