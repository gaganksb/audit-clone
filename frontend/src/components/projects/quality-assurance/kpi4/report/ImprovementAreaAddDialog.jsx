import React, { useState } from 'react';

import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles(theme => ({
    inputTextArea: {
        resize: "vertical",
        width: "100%",
        height: "100%",
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        padding: theme.spacing(1),
        color: "#0072bc !important",
    },
    dialogContent: {
        margin: theme.spacing(3),
    },
    btnPosition: {
        marginLeft: -theme.spacing(2),
    }
}))

const ImprovementAreaAddDialog = (props) => {
    const classes = useStyles();
    const { isOpen, onClose, onAdd } = props;
    const [improvementAreaText, setImprovementAreaText] = useState("");
    const [errorText, setErrorText] = useState("");
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

    const add = () => {
        if (!improvementAreaText || "" == improvementAreaText.trim()) {
            setErrorText("Improvement area text is required")
        } else {
            let text = improvementAreaText;
            setImprovementAreaText("");
            onAdd(text);
        }

    }

    const handleTextChange = e => {
        if ("" !== e.target.value.trim()) {
            setErrorText("")
        }

        setImprovementAreaText(e.target.value);
    }

    return (
        <React.Fragment>
            <Dialog open={isOpen} onClose={onClose} fullScreen={fullScreen}
                fullWidth>
                <DialogTitle id="add-improvement-area-title" className={classes.heading}>
                    <Grid container alignItems="center">
                        <Grid item xs={3}>

                        </Grid>
                        <Grid item xs={5}>
                            <Typography variant="subtitle1">Add Improvement Area</Typography>
                        </Grid>
                        <Grid item xs={3}>

                        </Grid>
                        <Grid item xs={1}>
                            <Tooltip title={"Close"}>
                                <IconButton onClick={onClose}>
                                    <CloseIcon />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    </Grid>
                </DialogTitle>
                <Divider />
                <div className={classes.dialogContent}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            {errorText !== "" ? <Typography color="error">{errorText}</Typography> : null}
                        </Grid>
                        <Grid item xs={12}>
                            <textarea
                                className={classes.inputTextArea}
                                type="textarea"
                                name="improvementArea"
                                rows="4"
                                cols="50"
                                onChange={handleTextChange}
                                value={improvementAreaText}
                            />
                        </Grid>
                        <Grid item xs={12} container>
                            <Grid item xs={10}>

                            </Grid>
                            <Grid item xs={2} className={classes.btnPosition}>
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Add"}
                                    type="submit"
                                    clicked={add}
                                />
                            </Grid>
                        </Grid>

                    </Grid>
                </div>
            </Dialog>
        </React.Fragment>
    );
}

export default ImprovementAreaAddDialog;