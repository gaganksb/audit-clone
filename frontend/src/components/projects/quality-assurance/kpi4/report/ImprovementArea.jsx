import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from '@material-ui/icons/Save';
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import ImprovementAreaAddDialog from "./ImprovementAreaAddDialog";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import ImprovementAreaDeleteDialog from "./ImprovementAreaDeleteDialog";
import {
  addNewImprovementArea,
  updateNewImprovementArea,
  deleteNewImprovementArea,
} from "../../../../../reducers/qualityAssurance/AuditFirmCountryList";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
  },
  header: {
    display: "flex",
  },
  iconBtns: {
    display: "flex",
    justifyContent: "flex-end",
  },
  addBtnPos: {
    marginLeft: "auto",
  },
  inputTextArea: {
    resize: "vertical",
    width: "100%",
    height: "100%",
  },
}));

const messages = {
  success: {
    add: "Added successfully",
    save: "Saved successfully",
    delete: "Deleted successfully"
  }
}

const ImprovementArea = (props) => {
  const { team_composition_id, team_composition_improvement_areas, session } = props;
  const classes = useStyles();
  const [improvementAreas, setImprovementAreas] = useState([]);
  const [openAddDialog, setOpenAddDialog] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [currentDeleteRecordId, setCurrentDeleteRecordId] = useState(0);

  useEffect(() => {
    if (team_composition_improvement_areas) {
      let new_team_composition_improvement_areas = team_composition_improvement_areas.map(element => {
        element.edit = false;
        return element;
      })
      setImprovementAreas([...new_team_composition_improvement_areas]);
    } else {
      setImprovementAreas([]);
    }
  }, [team_composition_improvement_areas]);

  const handleOnOkDelete = () => {
    setOpenDeleteDialog(true);
    let updatedRecords = [...improvementAreas];
    props.deleteNewImprovementArea(currentDeleteRecordId).then(response => {
      updatedRecords = updatedRecords.filter(e => e.id !== currentDeleteRecordId);
      setImprovementAreas([...updatedRecords]);
      props.successReducer(messages.success.delete);
    }).catch(error => {
      props.postError(error, "delete improvement area failed");
    });
    setCurrentDeleteRecordId(0);
    setOpenDeleteDialog(false);
  };

  const handleDeleteDialogClose = () => {
    setOpenDeleteDialog(false);
  };

  const onAddNewArea = (text) => {
    let newArea = {
      team_composition: team_composition_id,
      improvement_area: text,
      auditor_comment: "",
      edit: false,
    }
    props.addNewImprovementArea(newArea).then(response => {
      newArea.id = response.id;
      setImprovementAreas(prev => [...prev, newArea]);
      props.successReducer(messages.success.add);
    }).catch(error => {
      props.postError(error, "Adding new improvement area failed");
    })

    setOpenAddDialog(false);
  }

  const onDialogClose = () => {
    setOpenAddDialog(false);
  }

  const handleIAreaTextChange = (e, id) => {
    let value = e.target.value;
    let updatedRecords = [...improvementAreas];
    updatedRecords = updatedRecords.map(element => {
      if (element.id == id) {
        element = { ...element, improvement_area: value }
      }
      return element;
    });

    setImprovementAreas([...updatedRecords]);

  }

  const handleAuditorCommentChange = (e, id) => {
    let value = e.target.value;
    let updatedRecords = [...improvementAreas];
    updatedRecords = updatedRecords.map(element => {
      if (element.id == id) {
        let newElement = { ...element };
        let newAuditorComment = { ...newElement.auditor_comment, message: value };
        element = { ...newElement, auditor_comment: newAuditorComment }
      }
      return element;
    });

    setImprovementAreas([...updatedRecords]);
  }


  const allowEdit = (id) => {
    let updatedRecords = [...improvementAreas];
    updatedRecords = updatedRecords.map(element => {
      if (element.id == id) {
        element = { ...element, edit: true }
      }
      return element;
    });

    setImprovementAreas([...updatedRecords]);
  }

  const saveArea = (id) => {
    let updatedRecords = [...improvementAreas];

    let element = updatedRecords.find(element => element.id == id);

    let body = {
      "improvement_area": element.improvement_area,
      "auditor_comment": element.auditor_comment ? element.auditor_comment.message : "",
    }
    props.updateNewImprovementArea(id, body).then(response => {

      updatedRecords = updatedRecords.map(element => {
        if (element.id == id) {
          element = { ...element, edit: false }
        }
        return element;
      });

      setImprovementAreas([...updatedRecords]);
      props.successReducer(messages.success.save);
    }).catch(error => {
      props.postError(error, "saving improvement area failed");
    });

  }

  const areas = improvementAreas && improvementAreas.length > 0 && improvementAreas.map((record, index) => {
    return <Grid item xs={12} container key={index} variant="outlined" spacing={2}>
      <Grid item xs={12} container>
        <Grid item xs={12}>
          <Typography variant="subtitle2" gutterBottom>
            Improvement Area:
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {record.edit && session.user_type == "HQ" ?

            <textarea
              className={classes.inputTextArea}
              type="textarea"
              name="improvementArea"
              rows="4"
              cols="50"
              onChange={e => handleIAreaTextChange(e, record.id)}
              value={record.improvement_area}
            />
            :
            <Typography variant="body2" gutterBottom>
              {record.improvement_area}
            </Typography>
          }
        </Grid>
      </Grid>
      <Grid container item xs={12}>
        <Grid item xs={12}>
          <Typography variant="subtitle2" gutterBottom>
            {props.auditor_name} Comments:
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {record.edit && session.user_type == "AUDITOR" && session.user_role == "ADMIN" ?
            <textarea
              className={classes.inputTextArea}
              type="textarea"
              name="auditor_comment"
              rows="4"
              cols="50"
              onChange={e => handleAuditorCommentChange(e, record.id)}
              value={record.auditor_comment ? record.auditor_comment.message : ""}
            />
            :
            <Typography variant="body2" gutterBottom>
              {record.auditor_comment ? record.auditor_comment.message : ""}
            </Typography>
          }
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <div className={classes.iconBtns}>
          {!record.edit &&
            <Tooltip title={"Edit"}>
              <IconButton aria-label="edit" onClick={() => allowEdit(record.id)}>
                <EditIcon />
              </IconButton>
            </Tooltip>}

          {record.edit &&
            <Tooltip title={"Save"}>
              <IconButton aria-label="save" onClick={() => saveArea(record.id)}>
                <SaveIcon />
              </IconButton>
            </Tooltip>}

          {session.user_type == "HQ" &&
            <Tooltip title={"Delete"}>
              <IconButton aria-label="delete"
                onClick={() => { setOpenDeleteDialog(true); setCurrentDeleteRecordId(record.id) }}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          }
        </div>
        <Divider />
      </Grid>
    </Grid>
  });


  return (
    <Paper className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <div className={classes.header}>
            <Typography variant="h6">Improvement Areas</Typography>
            {session.user_type == "HQ" && <div className={classes.addBtnPos}>
              <CustomizedButton
                buttonType={buttonType.submit}
                buttonText={"Add new improvement area"}
                clicked={() => setOpenAddDialog(true)}
              />
            </div>
            }
          </div>
        </Grid>
        <ImprovementAreaAddDialog onAdd={onAddNewArea} onClose={onDialogClose} isOpen={openAddDialog} />
        <ImprovementAreaDeleteDialog open={openDeleteDialog} onOk={handleOnOkDelete} onClose={handleDeleteDialogClose} />
        <Divider />
        {areas}
      </Grid>
    </Paper>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.AuditFirmCountryList.loading,
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addNewImprovementArea: (body) =>
    dispatch(addNewImprovementArea(body)),
  deleteNewImprovementArea: (id) =>
    dispatch(deleteNewImprovementArea(id)),
  updateNewImprovementArea: (id, body) =>
    dispatch(updateNewImprovementArea(id, body)),

  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Improvement-Area-Error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(ImprovementArea);

export default connected;
