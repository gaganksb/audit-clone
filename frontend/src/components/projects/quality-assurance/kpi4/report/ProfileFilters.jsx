import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm, reset } from "redux-form";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import { renderTextField } from "../../../../common/renderFields";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  filters: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(3),
  },
  buttonGrid: {
    marginLeft: -theme.spacing(1),
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "space-between",
  },
}));

const ProfileFilters = (props) => {
  const classes = useStyles();
  const { handleSubmit, onSearchClick, onResetClick, } = props;


  return (
    <div className={classes.root}>
      <Typography variant="h6" gutterBottom>
        Team Composition Profiles
      </Typography>
      <Paper className={classes.filters} variant="outlined">
        <form onSubmit={handleSubmit(onSearchClick)}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={3} lg={2}>
              <label htmlFor="name">Name</label>
              <Field name="fullname" component={renderTextField} type="text" />
            </Grid>
            <Grid item xs={12} sm={12} md={3} lg={2}>
              <label htmlFor="role">Role</label>
              <Field name="role" component={renderTextField} type="text" />
            </Grid>
            <Grid item xs={12} sm={12} md={1} lg={1}></Grid>

            <Grid item xs={12} sm={12} md={8} lg={2}>
              <div className={classes.btnGrpAlign}>
                <div className={classes.buttonGrid}>
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Search"}
                    clicked={handleSubmit(onSearchClick)}
                  />
                </div>
                <div className={classes.buttonGrid}>
                  <CustomizedButton
                    buttonType={buttonType.cancel}
                    buttonText={"Reset"}
                    clicked={() => { reset(); onResetClick(); }}
                  />
                </div>
              </div>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </div>
  );
};

ProfileFilters.propTypes = {
  handleSubmit: PropTypes.func,
  loading: PropTypes.bool,
};

const mapStateToProps = (state) => {
  return {
    initialValues: {
      fullname: "",
      role: "",
    },
  };
};

const mapDispatch = (dispatch) => ({
});

const TeamCompositionprofileFiltersForm = reduxForm({
  form: "TeamCompositionprofileFiltersForm",
  enableReinitialize: true,
})(ProfileFilters);

export default connect(mapStateToProps, mapDispatch)(TeamCompositionprofileFiltersForm);
