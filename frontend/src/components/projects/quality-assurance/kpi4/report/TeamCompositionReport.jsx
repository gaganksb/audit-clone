import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues } from "redux-form";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { browserHistory as history } from "react-router";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import TeamCompositionProfiles from "./TeamCompositionProfiles";
import AnalysisResultForm from "./AnalysisAllCountry";
import ImprovementArea from "./ImprovementArea";
import ListLoader from "../../../../common/listLoader";
import FinalizeDialog from "./FinalizeDialog";
import { downloadURI } from '../../../../../helpers/others';

import {
  saveTeamCompositionReport,
  getTeamCompositionReport,
  getTeamCompositionCountryCriteriasAPI,
  sendTeamCompositionNotificationAPI,
} from "../../../../../reducers/qualityAssurance/AuditFirmCountryList";

const useStyles = makeStyles((theme) => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  headerItems: {
    display: "flex",
    alignItems: "center",
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "space-between",
  },
  backTick: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
  },
  additionalTexts: {
    margionTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
}));

const messages = {
  error: "Error occurred",
  success: {
    saved: "Saved successfully !",
    finalized: "Finalized successfully !",
    notified: "Notification sent !",
  },
};

let TeamCompositionReportForm = (props) => {
  const { handleSubmit, auditFirmCountries, session, getCountryCriterias, sendTeamCompositionNotification } = props;
  const [country_criterias, setCountryCriterias] = useState([]);
  const [openFinalizeDialog, setOpenFinalizeDialog] = useState(false);
  const classes = useStyles();

  const [data, setData] = useState({});

  const backHandler = () => {
    history.goBack();
  };

  useEffect(() => {

    let team_composition_id = props.location.query.id;

    if (team_composition_id) {
      team_composition_id = parseInt(team_composition_id);
      props
        .getTeamCompositionReport(team_composition_id)
        .then((response) => {
          setData(response);
          let additional_comments = response.comments && response.comments.length > 0 ? response.comments[0].message : "";
          props.initialize({
            introduction: response.introduction,
            scope_and_objective: response.scope_and_objective,
            methodology: response.methodology,
            overall_conclusion: response.overall_conclusion,
            additional_comments: additional_comments,
          });
        })
        .catch((error) => {
          props.postError(error, "Team composition fetch failed");
        });
    }

    getCountryCriterias().then(response => {
      setCountryCriterias([...response]);
    }).catch(error => {
      props.postError(error, "Country criteria fetch failed");
    })
  }, []);

  const validate = (values) => {
    let { introduction, scope_and_objective, methodology, overall_conclusion, additional_comments } =
      values;
    if (!introduction || introduction.trim() == "") {
      let error = {
        introduction: "required",
      };
      props.postError(error, "Introduction field is required");
      return false;
    }
    if (!scope_and_objective || scope_and_objective.trim() == "") {
      let error = {
        scope_and_objective: "required",
      };
      props.postError(error, "Scope and Objective field is required");
      return false;
    }
    if (!methodology || methodology.trim() == "") {
      let error = {
        methodology: "required",
      };
      props.postError(error, "Methodology field is required");
      return false;
    }

    if (!overall_conclusion || overall_conclusion.trim() == "") {
      let error = {
        overall_conclusion: "required",
      };
      props.postError(error, "Overall Conclusion field is required");
      return false;
    }

    return true;
  }

  const save = (values) => {
    let team_composition_id = data.id;

    let { introduction, scope_and_objective, methodology, overall_conclusion, additional_comments } =
      values;
    if (validate(values)) {
      let body = {
        introduction,
        scope_and_objective,
        methodology,
        overall_conclusion,
      };

      if (additional_comments) {
        body.comments = [];
        body.comments.push({ message: additional_comments })
      }
      props
        .saveTeamCompositionReport(team_composition_id, body)
        .then((response) => {
          props.successReducer(messages.success.saved);
        })
        .catch((error) => {
          props.postError(error, messages.error);
        });
    }
  };

  const onFinalizeDialogOpen = () => {
    setOpenFinalizeDialog(true);
  }

  const onCloseFinalizeDialog = () => {
    setOpenFinalizeDialog(false);
  }

  const onFinalize = () => {
    let values = props.formValues;
    finalize(values);
    setOpenFinalizeDialog(false);
  }

  const finalize = (values) => {
    let team_composition_id = data.id;

    let { introduction, scope_and_objective, methodology, overall_conclusion, additional_comments } =
      values;
    if (validate(values)) {
      let body = {
        introduction,
        scope_and_objective,
        methodology,
        overall_conclusion,
      };

      if (additional_comments) {
        body.comments = [];
        body.comments.push({ message: additional_comments })
      }

      body.finalized = true;

      props
        .saveTeamCompositionReport(team_composition_id, body)
        .then((response) => {
          props.successReducer(messages.success.finalized);
        })
        .catch((error) => {
          props.postError(error, messages.error);
        });
    }
  };

  const notifyForProfileCompletion = () => {
    let year = props.location.query.year;
    if (year) {
      year = parseInt(year);
      let body = {
        year
      }
      sendTeamCompositionNotification(body).then(response => {
        props.successReducer(messages.success.notified);
      }).catch(error => {
        props.postError(error, messages.error);
      });
    }
  }

  const exportReport = () => {
    let year = props.location.query.year;
    downloadURI(`/api/quality-assurance/kpi4/team-composition-export/${props.location.query.id}/?year=${year}`, 'team-composition-report.pdf');
  }


  return (

    <ListLoader loading={props.loading}>
      <React.Fragment>
        <Paper className={classes.header} variant="outlined">
          <div className={classes.headerItems}>
            <div className={classes.backTick}>
              <ArrowBackIosIcon onClick={backHandler} />
              <label>
                {data.audit_agency && data.audit_agency.legal_name} - Team
                composition report
              </label>
            </div>

            {session.user_type == "HQ" && (
              <div className={classes.btnGrpAlign}>
                <div>
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Notify"}
                    clicked={notifyForProfileCompletion}
                  />
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Export"}
                    clicked={exportReport}
                  />
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Save"}
                    clicked={handleSubmit(save)}
                  />
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Finalize"}
                    clicked={onFinalizeDialogOpen}
                  />
                </div>
              </div>
            )}
          </div>
        </Paper>
        <Paper className={classes.root} variant="outlined">
          {session.user_type == "HQ" && (
            <form onSubmit={handleSubmit(save)}>
              <Grid container spacing={3}>
                <Grid container item>
                  <Grid item xs={12}>
                    <label htmlFor="introduction">
                      Introduction & Background
                    </label>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      name="introduction"
                      component="textarea"
                      type="text"
                      rows="7"
                      className={classes.input}
                      defaultValue={data.introduction}
                    />
                  </Grid>
                </Grid>

                <Grid container item>
                  <Grid item xs={12}>
                    <label htmlFor="scope_and_objective">
                      Scope & Objectives
                    </label>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      name="scope_and_objective"
                      component="textarea"
                      type="text"
                      rows="7"
                      className={classes.input}
                    />
                  </Grid>
                </Grid>

                <Grid container item>
                  <Grid item xs={12}>
                    <label htmlFor="methodology">
                      Methodology & Sampling selection
                    </label>
                  </Grid>
                  <Grid item xs={12}>
                    <Field
                      name="methodology"
                      component="textarea"
                      type="text"
                      rows="7"
                      className={classes.input}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </form>
          )}

          <TeamCompositionProfiles
            team_composition_id={props.location.query.id}
            team_composition_users={data.team_composition_users}
            year={parseInt(props.location.query.year)}
          />
          <AnalysisResultForm countries={data.team_composition_countries} criterias={country_criterias} />

          {session.user_type == "HQ" && (
            <Grid container item xs={12} className={classes.additionalTexts}>
              <Grid item xs={12}>
                <label>Overall Conclusion</label>
              </Grid>

              <Grid item xs={12}>
                <Field
                  name="overall_conclusion"
                  component="textarea"
                  type="text"
                  rows="7"
                  className={classes.input}
                />
              </Grid>
            </Grid>
          )}

          <ImprovementArea team_composition_id={props.location.query.id}
            team_composition_improvement_areas={data.team_composition_improvement_areas}
            auditor_name={data.audit_agency && data.audit_agency.legal_name} />

          {session.user_type == "HQ" && (
            <Grid container item xs={12} className={classes.additionalTexts}>
              <Grid item xs={12}>
                <label>Additional Comments</label>
              </Grid>

              <Grid item xs={12}>
                <Field
                  name="additional_comments"
                  component="textarea"
                  type="text"
                  rows="7"
                  className={classes.input}
                />
              </Grid>
            </Grid>
          )}
        </Paper>
        <FinalizeDialog open={openFinalizeDialog} onClose={onCloseFinalizeDialog} onFinalize={onFinalize} />
      </React.Fragment>
    </ListLoader>

  );
};

TeamCompositionReportForm = reduxForm({
  form: "TeamCompositionReportForm",
})(TeamCompositionReportForm);

const mapStateToProps = (state) => {
  return {
    auditFirmCountries: state.AuditFirmCountryList.auditFirmCountries,
    loading: state.AuditFirmCountryList.loading,
    session: state.session,
    formValues: getFormValues('TeamCompositionReportForm')(state),
  };
};

const mapDispatchToProps = (dispatch) => ({
  saveTeamCompositionReport: (id, body) =>
    dispatch(saveTeamCompositionReport(id, body)),
  getTeamCompositionReport: (id, body) =>
    dispatch(getTeamCompositionReport(id, body)),
  sendTeamCompositionNotification: (body) =>
    dispatch(sendTeamCompositionNotificationAPI(body)),
  getCountryCriterias: () =>
    dispatch(getTeamCompositionCountryCriteriasAPI()),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Team-Composition-Error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamCompositionReportForm);

export default connected;
