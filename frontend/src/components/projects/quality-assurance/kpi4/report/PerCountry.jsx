import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";

import { saveTeamCompositionReport } from "../../../../../reducers/qualityAssurance/AuditFirmCountryList";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
  },
  inputTextArea: {
    resize: "vertical",
    width: "100%",
    height: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    color: "#0072bc !important",
  },
  buttonGrid: {
    marginLeft: -theme.spacing(2),
  },
}));

const messages = {
  save: "Country analysis saved successfully !",
  error: "Error in country analysis save !"
}

const PerCountryAnalysis = (props) => {
  const classes = useStyles();
  const { country, team_composition_id, team_composition_country_criterias, criteriaList, session } = props;
  const [criteriaData, setCriteriaData] = useState([]);

  useEffect(() => {
    if (criteriaList && criteriaList.length > 0 && team_composition_country_criterias && team_composition_country_criterias.length > 0) {
      let fetchedCriteria = [];
      team_composition_country_criterias.map(record => {
        let criteria = criteriaList.filter(e => e.id == record.criteria.id);
        fetchedCriteria.push({
          criteria: record.criteria, remarks: record.remarks.message,
          auditor_comment: record.auditor_comment.message,
          description: criteria.length > 0 ? criteria[0].description : "",
        })
      })
      setCriteriaData([...fetchedCriteria]);
    } else {
      if (criteriaList && criteriaList.length > 0) {
        let initialCriterias = [];
        criteriaList.map(record => {
          initialCriterias.push({ criteria: record.id, remarks: "", auditor_comment: "", ...record });
        })
        setCriteriaData([...initialCriterias]);
      }

    }
  }, [team_composition_country_criterias, criteriaList]);


  const save = () => {

    let team_composition_country_criteria = [];
    criteriaData.map(criteria => {
      let record = {
        team_composition_country: country.id,
        ...criteria
      }
      team_composition_country_criteria.push(record);
    })

    let body = {
      team_composition_country_criteria
    }

    props.saveTeamCompositionReport(team_composition_id, body).then(response => {
      props.successReducer(messages.save);
    }).catch(error => {
      props.postError(error, messages.error);
    });
  };


  const handleChange = (e, criteriaId) => {
    let updatedRecord = [...criteriaData];
    updatedRecord = updatedRecord.map(c => {
      if (c.criteria == criteriaId) {
        c = { ...c, [e.target.name]: e.target.value }
      }
      return c;

    });
    setCriteriaData([...updatedRecord]);
  };

  return (
    <Paper>
      <ExpansionPanel>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          className={classes.panelSummary}
        >
          <Typography className={classes.heading}>
            {country.name}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Grid container spacing={3}>
            {criteriaData.map((record, index) => (
              <Grid container item spacing={2} key={index}>
                <Grid item xs={12}>
                  <label>
                    <b>Criteria:</b> {record.description}
                  </label>
                </Grid>
                <Grid container item xs={12}>
                  <Grid item xs={12}>
                    <Typography variant="subtitle2">Remarks:</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    {session.user_type == "HQ" ?
                      <textarea
                        className={classes.inputTextArea}
                        type="textarea"
                        name="remarks"
                        rows="4"
                        cols="50"
                        onChange={(e) => handleChange(e, record.criteria)}
                        value={record.remarks}
                      />
                      :
                      <Typography variant="body2">{record.remarks}</Typography>
                    }
                  </Grid>
                </Grid>

                <Grid container item xs={12}>
                  <Grid item xs={12}>
                    <Typography variant="subtitle2">Auditor comments:</Typography>
                  </Grid>

                  <Grid item xs={12}>
                    {session.user_type == "AUDITOR" && session.user_role == "ADMIN" ?
                      <textarea
                        name="auditor_comment"
                        type="textarea"
                        rows="4"
                        cols="50"
                        className={classes.inputTextArea}
                        onChange={(e) => handleChange(e, record.criteria)}
                        value={record.auditor_comment}
                      />
                      :
                      <Typography variant="body2">{record.auditor_comment}</Typography>
                    }
                  </Grid>
                </Grid>
              </Grid>
            ))}

            <Grid container item xs={12}>
              <Grid item xs={11}></Grid>

              <Grid item xs={1}>
                <div className={classes.buttonGrid}>
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"Save"}
                    type="submit"
                    clicked={save}
                  />
                </div>
              </Grid>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Paper>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.AuditFirmCountryList.loading,
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  saveTeamCompositionReport: (id, body) =>
    dispatch(saveTeamCompositionReport(id, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Team-Composition-Error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(PerCountryAnalysis);

export default connected;
