import React, { useState, useEffect } from "react";
import { connect } from 'react-redux'
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ProfileFilters from "./ProfileFilters";
import AuditorProfile from "./AuditorProfile";
import { getTeamCompositionMembersByNameRole } from '../../../../../reducers/qualityAssurance/AuditFirmCountryList'
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    backgroundColor: "lightblue",
  },
  inputLabel: {
    fontSize: 14,
    color: "darkblue",
  },
  heightAdjust: {
    maxHeight: '500px',
    'overflow-y': 'scroll',
  },
}));

const profiles = (props) => {
  const classes = useStyles();
  const [profiles, setProfiles] = useState([]);
  const [filterKey, setFilterKey] = useState("default");

  useEffect(() => {
    if (props.team_composition_users && props.team_composition_users.length > 0) {
      setProfiles([...props.team_composition_users]);
    } else {
      setProfiles([]);
    }
  }, [props.team_composition_users]);

  const getProfilesFromAPI = (name, role) => {
    let validName = null;
    let validRole = null;

    if (name && name != "") {
      validName = name;
    }
    if (role && role != "") {
      validRole = role;
    }

    let team_composition_id = props.team_composition_id;
    let year = props.year;

    props.getTeamCompositionMembersByNameRole(team_composition_id, year, validName, validRole)
      .then(response => {
        if (response && response.length > 0) {
          setProfiles([...response[0].team_composition_users])
        } else {
          setProfiles([])
        }
      })
      .catch(error => {
        props.postError(error, "Team composition profile fetch failed");
      })
  }

  const onSearchClick = values => {
    let fullname = values.fullname;
    let role = values.role;
    getProfilesFromAPI(fullname, role);
  }

  const onResetClick = () => {
    getProfilesFromAPI(null, null);
    setFilterKey(Math.floor(Math.random() * 1000000));
  }

  const profilesDisplay = profiles && profiles.length > 0 ? profiles.filter(item=>item.agreement_member && item.agreement_member.reviewer === true).map((profile, index) => {
    return <AuditorProfile key={index} profile={profile} year={props.year} />
  }) : "No data found"



  return (
    <React.Fragment>
      <ProfileFilters onSearchClick={onSearchClick} onResetClick={onResetClick} key={filterKey} />
      <Paper variant="outlined" className={classes.heightAdjust}>
        <Grid container className={classes.root}>
          <Grid item xs={12} sm={12} md={1}></Grid>
          <Grid item xs={12} sm={12} md={2}>
            <label className={classes.inputLabel}>Name</label>
          </Grid>
          <Grid item xs={12} sm={12} md={2}>
            <label className={classes.inputLabel}>Role</label>
          </Grid>
          <Grid item xs={12} sm={12} md={2}>
            <label className={classes.inputLabel}>
              Duration spent (in days)
            </label>
          </Grid>
        </Grid>
        {profilesDisplay}
      </Paper>
    </React.Fragment>
  );
};


const mapStateToProps = (state) => {
  return {
    loading: state.AuditFirmCountryList.loading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getTeamCompositionMembersByNameRole: (id, year, name, role) =>
    dispatch(getTeamCompositionMembersByNameRole(id, year, name, role)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Team-Composition-Error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(profiles);

export default connected;
