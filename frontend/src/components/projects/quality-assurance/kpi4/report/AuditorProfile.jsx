import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import Grid from "@material-ui/core/Grid";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { MemberTeamType } from "../../../../../helpers/constants";

import {
  getPPAbyMemberIdApi,
} from '../../../../../reducers/qualityAssurance/AuditFirmCountryList';



const getTeamType = (value) => {
  if (value) {
    let type = MemberTeamType.filter(m => m.value == value);
    if (type && type.length > 0) {
      return type[0].code;
    }
  }
}

const AuditorProfile = (props) => {
  const [showDetails, setShowDetails] = useState(false);
  const [ppa, setPpa] = useState([]);
  const { profile } = props;

  useEffect(() => {
    if (showDetails) {
      getPpaDetails(profile && profile.agreement_member && profile.agreement_member.id)
    }
  }, [showDetails])


  const getPpaDetails = (id) => {
    let year = props.year;
    if (year) {
      props.getPpas(id, year).then(response => {
        if (response && response.length > 0) {
          setPpa(response);
        }
      }).catch(error => {
        throw error;
      })
    }

  }

  return (
    <div>
      <Grid container>
        <Grid item xs={12} sm={12} md={1}>
          <ExpandMoreIcon onClick={() => setShowDetails(!showDetails)} />
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          {profile.agreement_member && profile.agreement_member.user.fullname}
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          {profile.agreement_member && profile.agreement_member.role}
        </Grid>
        <Grid item xs={12} sm={12} md={2}>
          {profile.agreement_member && profile.agreement_member.duration_spent_in_days}
        </Grid>

        {showDetails && (
          <Grid item xs={12} container spacing={2}>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Field/HQ Work Team</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && getTeamType(profile.agreement_member.team_type)}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Task Completed</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.tasks_completed && profile.agreement_member.tasks_completed.toString()}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Brief Profile Summary</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.profile_summary}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Professional and Academic Qualifications</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.qualifications}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Years of Experience</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.years_of_experience}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Examples of Clients Audited</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.clients_audited_example && profile.agreement_member.clients_audited_example.toString()}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Experience in ISAs Application</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.ISA_experience}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Language Skills</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.language && profile.agreement_member.language.toString()}</Grid>
            </Grid>

            <Grid item xs={12} container>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}><b>Additional Comments</b></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={11}>{profile.agreement_member && profile.agreement_member.comments}</Grid>
            </Grid>

            {ppa && ppa.length > 0 &&
              <Grid item xs={12} container>
                <Grid item xs={1}></Grid>
                <Grid item xs={11}><b>PPA</b></Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={11}>{ppa && ppa.toString()}</Grid>
              </Grid>
            }

          </Grid>
        )}
      </Grid>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => ({
  getPpas: (id, year) =>
    dispatch(getPPAbyMemberIdApi(id, year)),
});

const connected = connect(
  null,
  mapDispatchToProps
)(AuditorProfile);

export default connected;
