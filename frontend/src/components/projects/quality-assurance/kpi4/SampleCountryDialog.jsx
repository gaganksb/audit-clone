import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Chip from "@material-ui/core/Chip";
import CustomizedButton from "../../../common/customizedButton";
import { buttonType, filterButtons } from "../../../../helpers/constants";
import ListLoader from "../../../common/listLoader";
import { showSuccessSnackbar } from "../../../../reducers/successReducer";
import {
  loadSampleAuditFirmCountriesList,
  confirmedAuditFirmCountriesList,
} from "../../../../reducers/qualityAssurance/SampleAuditFirmCountryList";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
  label: {
    color: "#0072bc !important",
  },
  buttonGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: -theme.spacing(2),
    display: "flex",
    width: "100%",
    justifyContent: "flex-end",
  },
}));

const messages = {
  error: "Error occurred",
  success: {
    reSampled: "Countries sampled !",
    confirmed: "Sampling cofirmed !",
  },
};

const SampleCountryDialog = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const {
    onClose,
    confirmSuccessClose,
    open,
    year,
    sampleLoading,
    sampleAuditFirmCountries,
  } = props;

  const handleClose = () => {
    onClose();
  };

  const reSampleHandler = () => {
    props
      .loadSampleAuditFirmCountriesList(year)
      .then((response) => props.successReducer(messages.success.reSampled))
      .catch((err) => {
        props.successReducer(messages.error)
      });
  };

  const confirmHandler = () => {
    let body = [];
    sampleAuditFirmCountries.map((record) => {
      body.push({
        audit_agency: record.auditFirmId,
        countries: record.countryIds,
      });
    });
    props
      .confirmedAuditFirmCountriesList(year, body)
      .then((response) => {
        props.successReducer(messages.success.confirmed);
        confirmSuccessClose();
      })
      .catch((err) => {
        props.successReducer(messages.error)
      });
  };

  const displayCountries = countries => {
    let countryArr = countries.split(",");
    if (countryArr) {
      let countries = countryArr.map((record, index) => {
        return <Chip key={index}
          label={record}
        />
      })
      return countries;
    }
  }


  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="sample-country-dialog"
      open={open}
      fullScreen={fullScreen}
      fullWidth
    >
      <DialogTitle id="sample-country-dialog">Sampled Countries</DialogTitle>
      <Divider />
      <ListLoader loading={sampleLoading}>
        <div>
          <TableContainer className={classes.root} component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center" className={classes.label}>
                    Audit Firm
                  </TableCell>
                  <TableCell align="center" className={classes.label}>
                    Country
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {sampleAuditFirmCountries && sampleAuditFirmCountries.length > 0
                  ? sampleAuditFirmCountries.map((row) => (
                    <TableRow key={row.auditFirmId}>
                      <TableCell
                        component="th"
                        scope="row"
                        align="center"
                        width="20%"
                      >
                        {row.auditFirmName}
                      </TableCell>
                      <TableCell align="center" width="80%">
                        {displayCountries(row.countries)}
                      </TableCell>
                    </TableRow>
                  ))
                  : !sampleLoading
                    ? " Countries not sampled "
                    : null}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </ListLoader>
      <div className={classes.buttonGrid}>
        <CustomizedButton
          buttonType={buttonType.Cancel}
          clicked={handleClose}
          buttonText={"Cancel"}
          reset={true}
        />
        <CustomizedButton
          buttonType={buttonType.submit}
          clicked={reSampleHandler}
          buttonText={"Re-Sample"}
        />
        <CustomizedButton
          buttonType={buttonType.submit}
          clicked={confirmHandler}
          buttonText={"Confirm"}
        />
      </div>
    </Dialog>
  );
};

SampleCountryDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  return {
    sampleAuditFirmCountries:
      state.SampleAuditFirmCountryList.sampleAuditFirmCountries,
    sampleLoading: state.SampleAuditFirmCountryList.loading,
    year: ownProps.year,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loadSampleAuditFirmCountriesList: (params) =>
    dispatch(loadSampleAuditFirmCountriesList(params)),
  confirmedAuditFirmCountriesList: (params, body) =>
    dispatch(confirmedAuditFirmCountriesList(params, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(SampleCountryDialog);

export default connected;
