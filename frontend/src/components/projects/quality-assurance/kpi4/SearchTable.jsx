import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Chip from "@material-ui/core/Chip";
import { browserHistory as history } from "react-router";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CustomizedButton from "../../../common/customizedButton";
import { buttonType } from "../../../../helpers/constants";
import NoDataTable from "../../preliminary/noData";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(4),
  },
  label: {
    color: "#0072bc !important",
  },
  listFlexContainer: {
    display: "flex",
    flexDirection: "row",
    padding: 0,
    "overflow-wrap": "break-word",
  },
}));

const SearchAuditFirmTable = (props) => {
  const { auditFirmCountriesList, viewBtnDisplay } = props;
  const classes = useStyles();

  const reportHandler = (team_composition_id) => {
    if (team_composition_id) {
      history.push(
        `/quality-assurance/team-composition?id=${team_composition_id}&year=${props.year}`
      );
    }
  };

  const displayCountries = (countriesArr) => {
    let countries = "Countries not sampled";
    if (countriesArr && countriesArr.length > 0) {
      countries = countriesArr
        .map((record, index) =>

          <Chip key={index}
            label={record.country.name}
          />
        )
    }
    return countries;
  };

  return (
    <TableContainer className={classes.root} component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center" className={classes.label}>
              Audit Firm
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Country
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Report
            </TableCell>
            <TableCell align="center" className={classes.label}>
              Status
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {auditFirmCountriesList && auditFirmCountriesList.length > 0
            ? auditFirmCountriesList.map((row) => (
              <TableRow key={row.audit_agency.id}>
                <TableCell
                  component="th"
                  scope="row"
                  align="center"
                  width="20%"
                >
                  {row.audit_agency.legal_name}
                </TableCell>
                <TableCell align="center" width="60%">
                  {displayCountries(row.team_composition_countries)}
                </TableCell>
                <TableCell align="center" width="10%">
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={"View"}
                    type="submit"
                    clicked={() => reportHandler(row.id)}
                    disabled={viewBtnDisplay}
                  />
                </TableCell>
                <TableCell align="center" width="10%">
                  {row.status == "todo" ? (
                    <Tooltip title={row.status}>
                      <ReportProblemIcon style={{ color: "yellow" }}>{row.status}</ReportProblemIcon>
                    </Tooltip>
                  ) : row.status == "done" ? (<Tooltip title={row.status}>
                    <CheckCircleIcon style={{ color: "green" }}>{row.status}</CheckCircleIcon>
                  </Tooltip>) : (
                    row.status
                  )}
                </TableCell>
              </TableRow>
            ))
            : <TableRow><TableCell colSpan={4}><NoDataTable showMessage={true} message={"No data found. Please check if audit firms/reviewers are assigned to projects."} /></TableCell></TableRow>}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default SearchAuditFirmTable;
