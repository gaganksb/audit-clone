import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Kpi4Filter from "./Kpi4Filter";
import SearchAuditFirmTable from "./SearchTable";
import CustomizedButton from "../../../common/customizedButton";
import { buttonType } from "../../../../helpers/constants";
import ListLoader from "../../../common/listLoader";
import { getCurrentAuditFirmCountriesList } from "../../../../reducers/qualityAssurance/AuditFirmCountryList";
import { loadSampleAuditFirmCountriesList } from "../../../../reducers/qualityAssurance/SampleAuditFirmCountryList";
import SampleCountryDialog from "./SampleCountryDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(3),
  },
  buttonGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: -theme.spacing(2),
    display: "flex",
    width: "100%",
  },
}));

const currentYear = new Date().getFullYear();

const KPI4 = (props) => {
  const classes = useStyles();
  const [year, setYear] = useState(currentYear);
  const [key, setKey] = useState("default");
  const [openDlg, setOpenDlg] = useState(false);
  const [sampleCountriesPresentFlag, setSampleCountryPresentFlag] = useState(true);

  useEffect(() => {
    if (year) {
      props.getCurrentAuditFirmCountriesList(year);
    }
  }, [year]);

  useEffect(() => {

    if (props.auditFirmCountries && props.auditFirmCountries.length > 0) {
      let checkIfNoCountries = props.auditFirmCountries.some(e => e.team_composition_countries.length > 0);
      setSampleCountryPresentFlag(checkIfNoCountries);
    } else {
      setSampleCountryPresentFlag(true);
    }

  }, [props.auditFirmCountries])

  const onYearChange = (e) => {
    setYear(e.target.value);
  };

  const handleFilterSubmit = (values) => {
    let params = {
      year: values.year,
      audit_agency: values.id,
    };
    props.getCurrentAuditFirmCountriesList(params);
  };

  const resetFilterForm = () => {
    setKey(Math.floor(Math.random() * 10000));
    setYear(currentYear);
    let params = {
      year: currentYear,
    };
    props.getCurrentAuditFirmCountriesList(params);
  };

  const sampleCountriesHandler = () => {
    props.getSampleAuditFirmCountriesList(year);
    setOpenDlg(true);
  };

  const confirmSuccessClose = () => {
    setOpenDlg(false);

    let params = {
      year,
    };
    props.getCurrentAuditFirmCountriesList(params);
  };

  const onCloseDialog = () => {
    setOpenDlg(false);
  };



  return (
    <div className={classes.root}>
      <ListLoader loading={props.loading}>
        <Kpi4Filter
          year={year}
          handleFilterSubmit={handleFilterSubmit}
          key={key}
          resetFilterForm={resetFilterForm}
          onYearChange={onYearChange}
        />

        {!sampleCountriesPresentFlag && props.session.user_type == "HQ" &&
          <div className={classes.buttonGrid}>
            <CustomizedButton
              buttonType={buttonType.submit}
              clicked={sampleCountriesHandler}
              buttonText={"Sample Countries"}
            />
          </div>
        }


        <SampleCountryDialog
          open={openDlg}
          onClose={onCloseDialog}
          year={year}
          confirmSuccessClose={confirmSuccessClose}
        ></SampleCountryDialog>

        {!props.loading ? (
          <SearchAuditFirmTable
            auditFirmCountriesList={props.auditFirmCountries}
            viewBtnDisplay={!sampleCountriesPresentFlag}
            year={year}
          />
        ) : null}
      </ListLoader>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auditFirmCountries: state.AuditFirmCountryList.auditFirmCountries,
    totalCount: state.AuditFirmCountryList.totalCount,
    loading: state.AuditFirmCountryList.loading,
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCurrentAuditFirmCountriesList: (params) =>
    dispatch(getCurrentAuditFirmCountriesList(params)),
  getSampleAuditFirmCountriesList: (params) =>
    dispatch(loadSampleAuditFirmCountriesList(params)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(KPI4);

export default connected;
