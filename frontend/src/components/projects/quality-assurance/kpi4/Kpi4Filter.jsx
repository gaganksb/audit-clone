import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { renderTextField } from "../../../common/renderFields";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import InputLabel from "@material-ui/core/InputLabel";
import { Field, reduxForm } from "redux-form";
import { buttonType, filterButtons } from "../../../../helpers/constants";
import renderTypeAhead from "../../../common/fields/commonTypeAhead";
import CustomizedButton from "../../../common/customizedButton";
import { loadAuditList } from "../../../../reducers/audits/auditList";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3),
  },
  inputLabel: {
    fontSize: 14,
    color: "#344563",
    marginTop: 18,
    paddingLeft: 14,
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: "90%",
    marginTop: 2,
  },
  typeAhead: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: "90%",
  },
  container: {
    display: "flex",
    paddingBottom: 8,
  },
  paper: {
    color: theme.palette.text.secondary,
    marginTop: "2%",
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: "flex",
    justifyContent: "flex-end",
  },
}));

function Kpi4Filter(props) {
  const classes = useStyles();
  const [formData, setFormData] = useState({});
  const [keyData, setKeyData] = useState(props.key);

  useEffect(() => {
    setKeyData(props.key);
  }, [props.key]);

  const {
    handleSubmit,
    handleFilterSubmit,
    resetFilterForm,
    key,
    firmList,
    loadAuditLists,
    onYearChange,
    year,
  } = props;

  const handleInputChange = (e, v, name) => {
    if (v) {
      setFormData({ ...formData, id: v.id, legal_name: v.legal_name });
    } else {
      setFormData({});
    }
  };

  const searchAuditFirmSampleCountry = (values) => {
    const year = values.year;
    let formValues = {
      year: year,
      id: formData.id,
      name: formData.legal_name,
    };
    handleFilterSubmit(formValues);
  };

  const handleFirmChange = async (e, v) => {
    if (e) {
      let params = {
        legal_name: v,
      };
      loadAuditLists(params);
    }
  };

  return (
    <div>
      <Paper className={classes.paper}>
        <form
          className={classes.container}
          onSubmit={handleSubmit(searchAuditFirmSampleCountry)}
        >
          <Grid item sm={3}>
            <InputLabel className={classes.inputLabel}>Audit Firm</InputLabel>
            <Field
              name="legal_name"
              margin="normal"
              component={renderTypeAhead}
              key={keyData}
              className={classes.typeAhead}
              onChange={handleFirmChange}
              handleInputChange={handleInputChange}
              options={firmList ? firmList : []}
              getOptionLabel={(option) => option.legal_name}
              multiple={false}
            />
          </Grid>
          <Grid item sm={2}>
            <InputLabel className={classes.inputLabel}>Year</InputLabel>
            <Field
              name="year"
              margin="normal"
              type="number"
              key={keyData}
              component={renderTextField}
              className={classes.textField}
              InputProps={{
                inputProps: {
                  min: 2014,
                },
              }}
              onChange={onYearChange}
            />
          </Grid>
          <Grid item sm={4}>
            <div item className={classes.buttonGrid}>
              <CustomizedButton
                buttonType={buttonType.submit}
                buttonText={filterButtons.search}
              />
              <CustomizedButton
                buttonType={buttonType.submit}
                buttonText={filterButtons.reset}
                reset={true}
                clicked={resetFilterForm}
              />
            </div>
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

Kpi4Filter.propTypes = {
  handleSubmit: PropTypes.func,
  loading: PropTypes.bool,
};

const mapStateToProps = (state, ownProps) => {
  return {
    firmList: state.auditList.list,
    initialValues: {
      year: ownProps.year,
    },
  };
};

const mapDispatch = (dispatch) => ({
  loadAuditLists: (params) => dispatch(loadAuditList(params)),
});

const Kpi4FilterForm = reduxForm({
  form: "kpiFilterForm",
  enableReinitialize: true,
  destroyOnUnmount: false,
})(Kpi4Filter);

const connected = connect(mapStateToProps, mapDispatch)(Kpi4FilterForm);

export default connected;
