import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import GetAppIcon from '@material-ui/icons/GetApp';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import CustomizedButton from "../../../common/customizedButton";
import { buttonType } from "../../../../helpers/constants";

const useStyles = makeStyles(theme => ({
    template: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(1),
        marginTop: '8',
    }
}))

const QuestionsUploadDialog = (props) => {
    const classes = useStyles();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

    const onFileChange = event => {
        props.setFileOnSelect(event.target.files[0]);
    };

    const isFileNotSelected = () => {
        if (props.selectedFile) {
            return false;
        }
        return true;
    }

    return (
        <React.Fragment>
            <Dialog
                open={props.open}
                onClose={props.onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullScreen={fullScreen}
            >
                <DialogTitle id="alert-dialog-title">
                    <Typography variant="h6" color="primary" align="center">Upload Questions File</Typography>
                    <Divider />
                </DialogTitle>
                <DialogContent>
                    <DialogContentText >
                        <Grid container spacing={2}>
                            <Grid item xs={12} container spacing={2}>
                                <Grid item xs={12} container spacing={2}>
                                    <Grid item xs={12} align="center">
                                        Year: {props.year}
                                    </Grid>
                                    <Grid item xs={2}>
                                    </Grid>
                                    <Grid item xs={10} align="center">
                                        <input type="file" onChange={onFileChange}
                                            id="contained-button-file" />
                                    </Grid>
                                    <Grid item xs={12} align="center">
                                        <CustomizedButton
                                            buttonType={buttonType.cancel}
                                            buttonText={"Cancel"}
                                            type="submit"
                                            clicked={props.onClose}
                                        />
                                        <CustomizedButton
                                            buttonType={buttonType.submit}
                                            buttonText={"Upload"}
                                            type="submit"
                                            clicked={props.onUpload}
                                            disabled={isFileNotSelected()}
                                        />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <div className={classes.template}>
                            <Typography variant="subtitle1" color="primary">Download Sample Template</Typography>
                            <IconButton aria-label="download" size="large"
                                onClick={props.downloadSampleTemplate} style={{ marginTop: -8 }}>
                                <GetAppIcon fontSize="inherit" style={{ color: '#344563' }} />
                            </IconButton>
                        </div>
                    </DialogContentText>
                </DialogContent>
            </Dialog>

        </React.Fragment>
    )
}

export default QuestionsUploadDialog;
