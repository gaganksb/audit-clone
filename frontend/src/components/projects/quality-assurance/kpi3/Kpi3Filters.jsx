import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import { makeStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import { Field, reduxForm } from "redux-form";
import { buttonType, filterButtons } from "../../../../helpers/constants";
import renderTypeAhead from "../../../common/fields/commonTypeAhead";
import CustomizedButton from "../../../common/customizedButton";
import Paper from "@material-ui/core/Paper";
import InputLabel from "@material-ui/core/InputLabel";
import { renderTextField } from "../../../common/renderFields";
import { loadAuditList } from "../../../../reducers/audits/auditList";

const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(3),
    },
    inputLabel: {
        fontSize: 14,
        color: "#344563",
        marginTop: 18,
        paddingLeft: 14,
    },
    textField: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(1),
        width: "90%",
        marginTop: 2,
    },
    typeAhead: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(1),
        marginTop: 2,
        width: "90%",
    },
    container: {
        display: "flex",
        paddingBottom: 8,
    },
    paper: {
        color: theme.palette.text.secondary,
        marginTop: "2%",
    },
    buttonGrid: {
        margin: theme.spacing(2),
        marginTop: theme.spacing(3),
        display: "flex",
        justifyContent: "flex-end",
    },
}))

const Kpi3Filters = (props) => {
    const classes = useStyles();
    const { handleSubmit, key, resetFilterForm, firmList,
        loadAuditLists, } = props;
    const [keyData, setKeyData] = useState(props.key);

    useEffect(() => {
        setKeyData(key);
    }, [key]);

    const handleFirmChange = async (e, v) => {
        if (e) {
            let params = {
                legal_name: v,
            };
            loadAuditLists(params);
        }
    };

    const handleInputChange = (e, v, name) => {

    };


    return (<React.Fragment>
        <Paper className={classes.paper} variant="outlined">
            <form
                className={classes.container}
                onSubmit={handleSubmit}
            >
                {props.session.user_type == "HQ" &&
                    <Grid item sm={3}>
                        <InputLabel className={classes.inputLabel}>Audit Firm</InputLabel>
                        <Field
                            name="legal_name"
                            margin="normal"
                            component={renderTypeAhead}
                            key={keyData}
                            className={classes.typeAhead}
                            onChange={handleFirmChange}
                            handleInputChange={handleInputChange}
                            options={firmList ? firmList : []}
                            getOptionLabel={(option) => option.legal_name}
                            multiple={false}
                        />
                    </Grid>
                }
                <Grid item sm={2}>
                    <InputLabel className={classes.inputLabel}>Year</InputLabel>
                    <Field
                        name="year"
                        margin="normal"
                        type="number"
                        key={keyData}
                        component={renderTextField}
                        className={classes.textField}
                        InputProps={{
                            inputProps: {
                                min: 2014,
                            },
                        }}
                    />
                </Grid>
                <Grid item sm={4}>
                    <div className={classes.buttonGrid}>
                        <CustomizedButton
                            buttonType={buttonType.submit}
                            buttonText={filterButtons.search}
                            clicked={handleSubmit}
                        />
                        <CustomizedButton
                            buttonType={buttonType.submit}
                            buttonText={filterButtons.reset}
                            reset={true}
                            clicked={resetFilterForm}
                        />
                    </div>
                </Grid>
            </form>
        </Paper>
    </React.Fragment>)
}

const mapStateToProps = (state, ownProps) => {
    return {
        firmList: state.auditList.list,
        session: state.session,
        initialValues: {
            year: ownProps.year,
        },
    };
};

const mapDispatch = (dispatch) => ({
    loadAuditLists: (params) => dispatch(loadAuditList(params)),
});

const Kpi3FilterForm = reduxForm({
    form: "kpi3FilterForm",
    enableReinitialize: true,
    destroyOnUnmount: false,
})(Kpi3Filters);

const connected = connect(mapStateToProps, mapDispatch)(Kpi3FilterForm);

export default connected;