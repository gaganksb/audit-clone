import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Field, reduxForm, } from "redux-form";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";

import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType, filterButtons } from "../../../../../helpers/constants";
import ListLoader from "../../../../common/listLoader";
import CustomAutoCompleteField from "../../../../forms/TypeAheadFields";
import renderTypeAhead from "../../../../common/fields/commonTypeAhead";
import { renderTextField } from "../../../../common/renderFields";

import { loadPartnerList } from "../../../../../reducers/partners/partnerList";
import { loadBUList } from "../../../../../reducers/projects/oiosReducer/buList";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(6),
    },
    header: {
        marginTop: theme.spacing(3),
    },
    autoCompleteField: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(1),
        width: "93%",
        marginTop: 2,
    },
    inputLabel: {
        fontSize: 14,
        color: "#344563",
        marginTop: 18,
        paddingLeft: 14,
    },
    typeAhead: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(1),
        marginTop: 2,
        marginBottom: 8,
        width: "90%",
    },
    textField: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(1),
        marginTop: 2,
        width: "92%",
    },
    btnPadding: {
        marginTop: theme.spacing(3),
        marginLeft: -theme.spacing(2),
        display: 'flex',
        justifyContent: 'flex-end',
    },
}))


const CommentsFilter = props => {
    const classes = useStyles();
    const [partner, setSelectedPartner] = useState([]);
    const [partnerLists, setPartnerList] = useState([]);
    const { key, loading, handleSubmit, partnerList, BUList, loadBUList, loadPartnerList, survey_type, onResetFilter } = props;
    const partner_type_to_search = "partner__legal_name";

    const handleValueChange = (val) => {
        setSelectedPartner(val);
    };

    const handleBuChange = async (e, v) => {
        if (e) {
            let params = {
                code: v,
            };
            const bus = await loadBUList(params);
        }
    };

    function handleChangePartner(e, v) {
        if (v) {
            let params = {
                legal_name: v,
            };
            loadPartnerList(params);
        }
    }

    const handleInputChange = (e) => { };

    useEffect(() => {
        setPartnerList(partnerList);
    }, [partnerList]);

    return (
        <ListLoader loading={loading}>
            <React.Fragment>
                <Typography variant="subtitle1" className={classes.header} color="primary">
                    <b>Comments</b>
                </Typography>
                <Paper className={classes.root}>
                    <form>

                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12} md={3}>
                                <InputLabel className={classes.inputLabel}>
                                    <Typography variant="subtitle2" color="primary">Business Unit</Typography>
                                </InputLabel>
                                <Field
                                    name="business_unit__code"
                                    margin="normal"
                                    component={renderTypeAhead}
                                    key={key}
                                    className={classes.typeAhead}
                                    onChange={handleBuChange}
                                    handleInputChange={handleInputChange}
                                    options={BUList ? BUList : []}
                                    getOptionLabel={(option) => option.code}
                                    multiple={false}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={3}>
                                <InputLabel className={classes.inputLabel}>
                                    <Typography variant="subtitle2" color="primary">Agreement Number</Typography>
                                </InputLabel>
                                <Field
                                    name="number"
                                    margin="normal"
                                    type="number"
                                    key={key}
                                    component={renderTextField}
                                    className={classes.textField}
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={3}>
                                <InputLabel className={classes.inputLabel}>
                                    <Typography variant="subtitle2" color="primary">Answer Option</Typography>
                                </InputLabel>
                                <Field
                                    name="answer_option"
                                    margin="normal"
                                    type="text"
                                    key={key}
                                    component={renderTextField}
                                    className={classes.textField}
                                />
                            </Grid>
                            {survey_type == "partner" &&
                                <Grid item xs={12} sm={12} md={3}>
                                    <InputLabel className={classes.inputLabel}>
                                        <Typography variant="subtitle2" color="primary">Partner Name</Typography>
                                    </InputLabel>
                                    <Field
                                        name="partner"
                                        key={key}
                                        className={classes.autoCompleteField}
                                        component={CustomAutoCompleteField}
                                        handleValueChange={handleValueChange}
                                        onChange={handleChangePartner}
                                        items={partnerLists}
                                        comingValue={partner}
                                        type_to_search={partner_type_to_search}
                                    />
                                </Grid>
                            }
                        </Grid>

                        <div className={classes.btnPadding}>
                            <CustomizedButton
                                buttonType={buttonType.submit}
                                buttonText={filterButtons.search}
                                clicked={handleSubmit}
                            />
                            <CustomizedButton
                                buttonType={buttonType.submit}
                                reset={true}
                                clicked={() => onResetFilter()}
                                buttonText={filterButtons.reset}
                            />
                        </div>

                    </form>

                </Paper>
            </React.Fragment>
        </ListLoader>
    )
}

const Kpi3CommentsFilterForm = reduxForm({
    form: "Kpi3CommentsFilterForm",
})(CommentsFilter);

const mapStateToProps = (state) => {
    return {
        initialValues: {

        },
        partnerList: state.partnerList.list,
        BUList: state.BUListReducer.bu,

    };
};

const mapDispatchToProps = (dispatch) => ({
    loadPartnerList: (params) => dispatch(loadPartnerList(params)),
    loadBUList: (params) => dispatch(loadBUList(params)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(Kpi3CommentsFilterForm);

export default connected;