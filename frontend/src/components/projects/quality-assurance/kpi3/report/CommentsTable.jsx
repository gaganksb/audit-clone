import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import TablePaginationActions from './TablePaginationActions';

import { getKpi3QuestionCommentsApi, } from "../../../../../reducers/qualityAssurance/Kpi3Reducer";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import ListLoader from "../../../../common/listLoader";
import CommentsFilter from './CommentsFilter'
import NoDataTable from '../../../preliminary/noData';


function Row(props) {
    const { row, survey_type } = props;
    const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row" align="center">
                    {row.bu_code}
                </TableCell>
                <TableCell align="center">{row.number}</TableCell>
                <TableCell align="center">{row.answer && row.answer.answer}</TableCell>
                {survey_type == "partner" &&
                    <TableCell align="center">{row.partner_name}</TableCell>
                }
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography variant="subtitle2" gutterBottom component="div">
                                Comment
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableBody>
                                    <TableRow>
                                        <TableCell>{row.answer && row.answer.comment ? row.answer.comment : "No data found"}</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

const CommentsTable = props => {
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [page, setPage] = React.useState(0);
    const [tableData, setTableData] = useState([]);
    const [count, setCount] = useState(0);
    const [key, setKey] = useState(0);
    const { year, survey_type, loading, } = props;

    useEffect(() => {
        loadComments(page)
    }, [])

    const loadComments = (page, business_unit__code, number, answer_option, partner) => {
        if (year && survey_type) {
            props.getKpi3QuestionCommentsApi(year, survey_type, page = page + 1, business_unit__code, number, answer_option, partner).then(response => {
                if (response && response.results && response.results.length > 0) {
                    setTableData([...response.results]);
                    setCount(response.count)
                } else {
                    setTableData([])
                    setCount(0)
                }

            }).catch(error => {
                setTableData([]);
                setCount(0)
            })
        }
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
        loadComments(newPage)
    };

    const onSearchClick = values => {
        const { business_unit__code, number, answer_option, partner } = values;
        loadComments(page, business_unit__code, number, answer_option, partner)
    }

    const onResetFilter = () => {
        setKey(Math.floor(Math.random() * 100000000));
        let page = 0;
        setPage(page);
        loadComments(page);
    }

    return (
        <ListLoader loading={loading}>
            <CommentsFilter survey_type={survey_type} onSubmit={onSearchClick} key={key} onResetFilter={onResetFilter} />
            <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell />
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Business Unit</Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Agreement Number</Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Answer Option</Typography>
                            </TableCell>
                            {survey_type == "partner" &&
                                <TableCell align="center">
                                    <Typography variant="subtitle2" color="primary">Partner Name</Typography>
                                </TableCell>
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {tableData && tableData.length > 0 ?
                            tableData
                                .map(row => {
                                    return <Row key={row.id} row={row} survey_type={survey_type} />
                                })
                            :
                            <TableRow>
                                <TableCell colSpan={6} >
                                    <NoDataTable noData />
                                </TableCell>
                            </TableRow>
                        }
                    </TableBody>
                </Table>
                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <TablePagination
                        rowsPerPageOptions={[5]}
                        colSpan={1}
                        count={count}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        SelectProps={{
                            native: true,
                        }}
                        onChangePage={handleChangePage}
                        ActionsComponent={TablePaginationActions}
                    />
                </div>
            </TableContainer>


        </ListLoader>
    );
}

const mapStateToProps = (state) => {
    return {
        loading: state.Kpi3Reducer.loading,
    };
};

const mapDispatchToProps = (dispatch) => ({
    getKpi3QuestionCommentsApi: (year, survey_type, page, bu_code, agreement_number, answer_option, partner_name) => dispatch(getKpi3QuestionCommentsApi(year, survey_type, page, bu_code, agreement_number, answer_option, partner_name)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(CommentsTable);

export default connected;