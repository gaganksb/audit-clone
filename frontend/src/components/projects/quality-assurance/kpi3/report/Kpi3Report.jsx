import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues } from "redux-form";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { browserHistory as history } from "react-router";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import { saveKpi3ReportApi, notifySurveyApi, getKpi3ReportApi, } from "../../../../../reducers/qualityAssurance/Kpi3Reducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ListLoader from "../../../../common/listLoader";
import { downloadURI } from '../../../../../helpers/others';
import SurveyResults from "../survey-results/SurveyResults";
import CommentsTable from "./CommentsTable";
import FinalizeDialog from '../../kpi4/report/FinalizeDialog';


const useStyles = makeStyles((theme) => ({
    header: {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        margin: theme.spacing(3),
    },
    headerItems: {
        display: "flex",
        alignItems: "center",
    },
    btnGrpAlign: {
        display: "flex",
        marginLeft: "auto",
        justifyContent: "flex-end",
    },
    backTick: {
        display: "flex",
        alignItems: "center",
        cursor: "pointer",
    },
    root: {
        padding: theme.spacing(3),
        margin: theme.spacing(3),
    },
    input: {
        width: "100%",
        height: "100%",
        resize: 'vertical',
    },
    additionalTexts: {
        margionTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
}));


const required = value => value ? undefined : 'Required*'

const renderTextAreaField = ({ input, label, type, disabled, rows, className, meta: { touched, error, warning } }) => (
    <React.Fragment>
        <div>
            {touched && ((error && <span style={{ color: 'red' }}>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
        <div>
            <label>{label}</label>
            <div>
                <textarea {...input} type={type} rows={rows} className={className} disabled={disabled} />
            </div>
        </div>
    </React.Fragment>
)

const Kpi3Report = (props) => {
    const classes = useStyles();
    const [openFinalDialog, setOpenFinalDialog] = useState(false);
    const [reportId, setReportId] = useState(null);
    const [formValues, setFormValues] = useState(null);
    const { session, handleSubmit } = props;
    const [fieldQuestions, setFieldQuestions] = useState([]);
    const [partnerQuestions, setPartnerQuestions] = useState([]);
    const [auditorQuestions, setAuditorQuestions] = useState([]);
    const [response, setResponse] = useState(null);
    const [isFinalized, setIsFinalized] = useState(false);

    const backHandler = () => {
        history.goBack();
    };

    useEffect(() => {
        const year = props.location.query.year;
        const firm_id = props.location.query.firm_id;
        props.getKpi3ReportApi(year, firm_id).then(response => {
            const { id, introduction, scope_and_objective, methodology, conclusion, comments, questions, finalized, } = response;

            setResponse({ introduction, scope_and_objective, methodology, conclusion, comments })
            setIsFinalized(finalized);

            props.initialize({
                introduction, scope_and_objective, methodology, conclusion,
            })

            if (questions) {
                setFieldQuestions([...questions.field_questions]);
                setPartnerQuestions([...questions.partner_questions]);
                setAuditorQuestions([...questions.auditor_questions]);
            }

            setReportId(id);

        }).catch(error => {
            props.postError(error, error.response && error.response.data && error.response.data.detail);
        })
    }, [])

    const save = (values) => {
        saveOrFinalize(values, "save")
    };

    const saveOrFinalize = (values, action) => {
        let { introduction, scope_and_objective, methodology, conclusion, comments } = values;

        let additional_comments = [];
        if (comments && comments.trim() != "") {
            let userType = props.session.user_type == "HQ" ? "UNHCR" : props.session.user_type == "AUDITOR" ? "AUDITOR" : "";
            comments = userType + ": " + comments;
            additional_comments.push({ message: comments });
            if (response.comments) {
                let oldComments = [...response.comments];
                oldComments.push({ message: comments })
                setResponse({ ...response, comments: oldComments })
            }
        }

        let body = {};

        if (isAuditorUser) {
            body = {
                comments: additional_comments,
                action: action,
            }
        } else if (isHQUser) {
            body = {
                introduction,
                scope_and_objective,
                methodology,
                conclusion,
                comments: additional_comments,
                action: action,
            }
        }
        if (action == "finalize") {
            body["finalized"] = true;
        }
        props.saveKpi3ReportApi(reportId, body).then(response => {
            if (action == "finalize") {
                props.successReducer("Survey report finalized !")
                setIsFinalized(true);
            } else if (action == "save") {
                props.successReducer("Survey report saved !")
            }
        }).catch(error => {
            props.postError(error, error.response && error.response.data && error.response.data.message);
        })

        setFormValues(null);
    }

    const onFinalizeDialogOpen = (values) => {
        setFormValues(values)
        setOpenFinalDialog(true);
    }

    const onCloseFinalizeDialog = () => {
        setFormValues(null)
        setOpenFinalDialog(false)
    }

    const onFinalize = () => {
        setOpenFinalDialog(false)
        saveOrFinalize(formValues, "finalize")
    }


    const notify = () => {
        const audit_firm_id = parseInt(props.location.query.audit_firm_id);
        const year = parseInt(props.location.query.year);

        let body = {
            year,
            audit_firm_id,
        }
        props.notifySurveyApi(body).then(response => {
            props.successReducer("Survey notification sent !");
        }).catch(error => {
            props.postError(error, error.response && error.response.data && error.response.data.message);
        })
    }

    const exportReport = () => {
        let year = props.location.query.year;
        downloadURI(`/api/quality-assurance/agreement-surveys/export/?year=${year}`, 'KPI3-Report.pdf');
    }

    const isHQUser = session.user_type == "HQ";
    const isAuditorUser = session.user_type == "AUDITOR";

    const showComments = response => {
        if (response && response.comments && response.comments.length > 0) {
            return <React.Fragment>
                <Typography variant="subtitle2">
                    Additional Comments
                </Typography>
                <Typography variant="body2">
                    <ul>
                        {response.comments.map((c, index) => {
                            return <li key={index}>{c.message}</li>
                        })}
                    </ul>
                </Typography>
            </React.Fragment>

        }
    }


    return (

        <ListLoader loading={props.loading}>
            <React.Fragment>
                <Paper className={classes.header} variant="outlined">
                    <div className={classes.headerItems}>
                        <div className={classes.backTick}>
                            <ArrowBackIosIcon onClick={backHandler} />
                            <label>
                                {props.location.query.firm_name} - Survey Report {props.location.query.year}
                            </label>
                        </div>

                        {isHQUser && (
                            <div className={classes.btnGrpAlign}>
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Notify"}
                                    clicked={notify}
                                    disabled={isFinalized}
                                />
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Export"}
                                    clicked={exportReport}
                                />
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Save"}
                                    clicked={handleSubmit(save)}
                                    disabled={isFinalized}
                                />
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Finalize"}
                                    clicked={handleSubmit(onFinalizeDialogOpen)}
                                    disabled={isFinalized}
                                />
                            </div>
                        )}

                        {isAuditorUser && (
                            <div className={classes.btnGrpAlign}>
                                <CustomizedButton
                                    buttonType={buttonType.submit}
                                    buttonText={"Save"}
                                    clicked={handleSubmit(save)}
                                    disabled={isFinalized}
                                />
                            </div>
                        )}
                    </div>
                </Paper>

                <Paper className={classes.root} variant="outlined">
                    <form>
                        <Grid container spacing={3}>
                            <Grid container item>
                                <Grid item xs={12}>
                                    {isHQUser ?
                                        <Field
                                            name="introduction"
                                            component={renderTextAreaField}
                                            type="text"
                                            rows="7"
                                            className={classes.input}
                                            validate={required}
                                            label={'Introduction & Background'}
                                            disabled={isFinalized}
                                        /> :
                                        <React.Fragment>
                                            <Typography variant="subtitle1">
                                                Introduction
                                            </Typography>
                                            <Typography variant="body2">
                                                {response && response.introduction ? response.introduction : "No data"}
                                            </Typography>
                                        </React.Fragment>
                                    }
                                </Grid>
                            </Grid>

                            <Grid container item>
                                <Grid item xs={12}>
                                    {isHQUser ?
                                        <Field
                                            name="scope_and_objective"
                                            component={renderTextAreaField}
                                            type="text"
                                            rows="7"
                                            className={classes.input}
                                            validate={required}
                                            label={'Scope & Objectives'}
                                            disabled={isFinalized}
                                        /> :
                                        <React.Fragment>
                                            <Typography variant="subtitle1">
                                                Scope & Objectives
                                            </Typography>
                                            <Typography variant="body2">
                                                {response && response.scope_and_objective ? response.scope_and_objective : "No data"}
                                            </Typography>
                                        </React.Fragment>
                                    }
                                </Grid>
                            </Grid>

                            <Grid container item>
                                <Grid item xs={12}>
                                    {isHQUser ?
                                        <Field
                                            name="methodology"
                                            component={renderTextAreaField}
                                            type="text"
                                            rows="7"
                                            className={classes.input}
                                            validate={required}
                                            label={'Methodology for assessing overall performance'}
                                            disabled={isFinalized}
                                        /> :
                                        <React.Fragment>
                                            <Typography variant="subtitle1">
                                                Methodology for assessing overall performance
                                            </Typography>
                                            <Typography variant="body2">
                                                {response && response.methodology ? response.methodology : "No data"}
                                            </Typography>
                                        </React.Fragment>
                                    }

                                </Grid>
                            </Grid>

                            <Grid container item>
                                <Grid item xs={12}>
                                    {isHQUser ?
                                        <Field
                                            name="conclusion"
                                            component={renderTextAreaField}
                                            type="text"
                                            rows="7"
                                            className={classes.input}
                                            validate={required}
                                            label={'Conclusions'}
                                            disabled={isFinalized}
                                        /> :
                                        <React.Fragment>
                                            <Typography variant="subtitle1">
                                                Conclusions
                                            </Typography>
                                            <Typography variant="body2">
                                                {response && response.conclusion ? response.conclusion : "No data"}
                                            </Typography>
                                        </React.Fragment>
                                    }
                                </Grid>
                            </Grid>

                            <Grid container item>
                                <Grid item xs={12}>
                                    {(isAuditorUser || isHQUser) &&
                                        <React.Fragment>

                                            {showComments(response)}

                                            <Field
                                                name="comments"
                                                component={renderTextAreaField}
                                                type="text"
                                                rows="7"
                                                className={classes.input}
                                                label={'Additional Comments'}
                                                disabled={isFinalized}
                                            />
                                        </React.Fragment>
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>


                < Paper className={classes.root} variant="outlined">
                    <Typography variant="subtitle1" color="primary"> Partner's Survey Results</Typography>
                    <SurveyResults questions={partnerQuestions}
                        year={parseInt(props.location.query.year)}
                        survey_type="partner" />
                    <CommentsTable year={parseInt(props.location.query.year)}
                        survey_type="partner" />
                </Paper>

                <Paper className={classes.root} variant="outlined">
                    <Typography variant="subtitle1" color="primary"> Field Office's Survey Results</Typography>
                    <SurveyResults questions={fieldQuestions}
                        year={parseInt(props.location.query.year)}
                        survey_type="field" />
                    <CommentsTable year={parseInt(props.location.query.year)}
                        survey_type="field" />
                </Paper>

                <Paper className={classes.root} variant="outlined">
                    <Typography variant="subtitle1" color="primary"> Auditor's Survey Results</Typography>
                    <SurveyResults questions={auditorQuestions}
                        year={parseInt(props.location.query.year)}
                        survey_type="auditor" />
                    <CommentsTable year={parseInt(props.location.query.year)}
                        survey_type="auditor" />
                </Paper>

                <FinalizeDialog open={openFinalDialog} onClose={onCloseFinalizeDialog} onFinalize={onFinalize} />
            </React.Fragment>
        </ListLoader >

    );
};

const Kpi3ReportForm = reduxForm({
    form: "Kpi3ReportForm",
})(Kpi3Report);

const mapStateToProps = (state) => {
    return {
        session: state.session,
        formValues: getFormValues('Kpi3ReportForm')(state),
        loading: state.Kpi3Reducer.loading,
    };
};

const mapDispatchToProps = (dispatch) => ({
    getKpi3ReportApi: (year, firm_id) => dispatch(getKpi3ReportApi(year, firm_id)),
    saveKpi3ReportApi: (id, body) => dispatch(saveKpi3ReportApi(id, body)),
    notifySurveyApi: (body) => dispatch(notifySurveyApi(body)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(Kpi3ReportForm);

export default connected;
