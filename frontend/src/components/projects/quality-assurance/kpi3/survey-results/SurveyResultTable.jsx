import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    table: {
        minWidth: '50%',
    },
});


const SurveyResultTable = (props) => {
    const classes = useStyles();

    const [responseData, setResponseData] = useState([...props.responseData]);

    useEffect(() => {
        if (props.responseData) {
            setResponseData([...props.responseData])
        } else {
            setResponseData([])
        }
    }, [props.responseData])

    return (
        <React.Fragment>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Options</Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Response Count</Typography>
                            </TableCell>
                            <TableCell align="center">
                                <Typography variant="subtitle2" color="primary">Response Percentage</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {responseData && responseData.length > 0 && responseData.map((row, index) => (
                            <TableRow key={index}>
                                <TableCell align="center">{row.option}</TableCell>
                                <TableCell align="center">{row.response_count}</TableCell>
                                <TableCell align="center">{row.response_percentage}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </React.Fragment>
    )
}

export default SurveyResultTable;