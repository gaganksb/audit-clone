import React, { useState, useEffect } from 'react';
import { PieChart, Pie, Tooltip, Legend, } from 'recharts';


const colors = [
    "#90EE90", "#ADD8E6", "#FF7F7F", "#FFFF66", "#FFD580", "#90EE90", "#ADD8E6", "#FF7F7F", "#FFFF66", "#FFD580",
]

const SurveyGraph = (props) => {
    const [data, setData] = useState([...props.responseData]);

    useEffect(() => {
        if (props.responseData) {
            props.responseData.map((e, index) => { e.fill = colors[index]; return e; })
            setData([...props.responseData])
        } else {
            setData([])
        }
    }, [props.responseData])

    const isDataValid = data && data.length > 0 && data.some(d => d.response_count != 0);

    return (
        <React.Fragment>
            {isDataValid ?
                <PieChart width={400} height={400}>
                    <Pie
                        dataKey="response_count"
                        isAnimationActive={true}
                        data={data}
                        cx="50%"
                        cy="50%"
                        outerRadius={80}
                        fill="#8884d8"
                        label
                        nameKey="option"
                    />
                    <Tooltip />
                    <Legend verticalAlign="bottom" />
                </PieChart>
                : "No response found"
            }
        </React.Fragment>
    );
}

export default SurveyGraph;
