import React, { useState, useEffect } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
    formControl: {
        marginBottom: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    input: {
        whiteSpace: 'normal',
    },
}));


const SelectQuestion = (props) => {

    const [questions, setQuestions] = useState([props.questions])

    const [selectedQuestion, setSelectedQuestion] = useState(null)

    useEffect(() => {
        if (props.questions) {
            setQuestions([...props.questions])

            if (selectedQuestion == null || !selectedQuestion) {
                if (props.questions && props.questions.length > 0) {
                    let firstQuestion = props.questions.find(q => allowedQuestionTypes.includes(q.question_type))
                    if (firstQuestion) {
                        let firstQuestionId = firstQuestion.question;
                        if (firstQuestionId) {
                            setSelectedQuestion(firstQuestionId)
                            props.changeQuestionHandler(firstQuestionId);
                        }
                    }
                }
            }
        }
    }, [props.questions])

    const classes = useStyles();

    const handleChange = (event) => {
        let questionId = event.target.value;
        if (questionId) {
            setSelectedQuestion(questionId);
            props.changeQuestionHandler(questionId);
        }
    };

    const allowedQuestionTypes = ["radio", "select", "input", "multi_select"]
    return (
        <React.Fragment>
            <InputLabel>Question</InputLabel>
            <FormControl className={classes.formControl} fullWidth>
                <Select
                    value={selectedQuestion}
                    onChange={handleChange}
                    inputProps={{
                        className: classes.input,
                    }}
                >
                    {questions && questions.length > 0 && questions.map(q => {
                        if (allowedQuestionTypes.includes(q.question_type)) {
                            return <MenuItem key={q.question}
                                className={classes.input}
                                value={q.question}>{q.question_title}</MenuItem>
                        }
                    })}
                </Select>
            </FormControl>
        </React.Fragment>
    )
}

export default SelectQuestion;