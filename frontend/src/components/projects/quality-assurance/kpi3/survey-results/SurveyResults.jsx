import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import SelectQuestion from './SelectQuestion';
import SurveyGraph from './SurveyGraph';
import SurveyResultTable from './SurveyResultTable';

import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import { getKpi3QuestionResponseApi } from "../../../../../reducers/qualityAssurance/Kpi3Reducer";
import ListLoader from "../../../../common/listLoader";

const SurveyResults = (props) => {

    const [questions, setQuestions] = useState([props.questions])
    const [responseData, setResponseData] = useState([]);

    useEffect(() => {
        if (props.questions) {
            setQuestions([...props.questions])
        }
    }, [props.questions])

    const changeQuestionHandler = (questionId) => {
        const year = props.year;
        const survey_type = props.survey_type;
        props.getKpi3QuestionResponseApi(year, questionId, survey_type)
            .then(response => {
                if (response && response.length > 0) {
                    setResponseData(response)
                } else {
                    setResponseData([])
                }
            })
            .catch(error => {
                setResponseData([])
                props.postError(error, error.response && error.response.data && error.response.data.message);
            })
    }

    return (
        <ListLoader loading={props.loading}>
            <Grid container spacing={1}>
                <Grid item xs={12} container spacing={1}>
                    <Grid item xs={4}></Grid>
                    <Grid item xs={8}><SurveyGraph responseData={responseData} /></Grid>
                </Grid>
                <Grid item xs={12}><SelectQuestion questions={questions} changeQuestionHandler={changeQuestionHandler} /></Grid>
                <Grid item xs={12}><SurveyResultTable responseData={responseData} /></Grid>
            </Grid>
        </ListLoader>
    )
}

const mapStateToProps = (state) => {
    return {
        loading: state.Kpi3Reducer.loading,
    };
};

const mapDispatchToProps = (dispatch) => ({
    getKpi3QuestionResponseApi: (year, questionId, survey_type) => dispatch(getKpi3QuestionResponseApi(year, questionId, survey_type)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3-error', message)),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(SurveyResults);

export default connected;