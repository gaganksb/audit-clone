import React, { useState, useEffect, } from "react";
import { connect } from 'react-redux'
import { makeStyles } from "@material-ui/core/styles";
import { browserHistory as history } from "react-router";

import Kpi3Filters from './Kpi3Filters';
import Kpi3SearchTable from "./Kpi3SearchTable";
import { buttonType } from "../../../../helpers/constants";
import CustomizedButton from "../../../common/customizedButton";

import { startSurveyApi, getSurveyRecordsApi, checkSurveyQuestionsUploadedApi, uploadSurveyQuestionsApi, } from "../../../../reducers/qualityAssurance/Kpi3Reducer";
import { showSuccessSnackbar } from "../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../reducers/errorReducer";
import { loadUser } from '../../../../reducers/users/userData'
import ListLoader from "../../../common/listLoader";
import { downloadURI } from '../../../../helpers/others';
import QuestionsUploadDialog from "./QuestionsUploadDialog";
import { loadAuditList } from "../../../../reducers/audits/auditList";



const useStyles = makeStyles(theme => ({
  surveyBtn: {
    margin: theme.spacing(2),
    marginLeft: theme.spacing(-2),
  },
}));

const Kpi3 = (props) => {
  const classes = useStyles();
  const [year, setYear] = useState(new Date().getFullYear());
  const [surveyData, setSurveyData] = useState([]);
  const [key, setKey] = useState("default");
  const [openSurveyStartDialog, setOpenSurveyStartDialog] = useState(false);
  const [showStartSurveyBtn, setShowStartSurveyBtn] = useState(false);
  const [showUploadQuestionsBtn, setShowUploadQuestionsBtn] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null)

  useEffect(() => {
    if (props.session.user_type == "HQ") {
      getSurveys(year);
      checkSurveyBtnStatus(year);
    } else {
      props.loadUser();
    }
  }, [])



  const setFileOnSelect = (value) => {
    setSelectedFile(value);
  }

  const getSurveys = (year, audit_agency) => {
    props.getSurveyRecordsApi(year, audit_agency).then(response => {
      if (response && response.length > 0) {
        if (props.session.user_type == "AUDITOR") {
          let auditAgencyId = props.userData.data.membership.audit_agency.id
          response = response.filter(r => r.audit_agency_id == auditAgencyId);
        }
        setSurveyData([...response]);
      } else {
        setSurveyData([]);
      }
    }).catch(error => {
      props.postError(error, error.response && error.response.data && error.response.data.message);
    })
  }

  const searchSurveyRecords = values => {
    if (values.year) {
      setYear(values.year)
      if (values.legal_name && values.legal_name != "") {
        if (props.firmList && props.firmList.length > 0) {
          let element = props.firmList.find(f => f.legal_name == values.legal_name);
          if (element) {
            getSurveys(values.year, element.id);
          }
        } else {
          let params = {
            legal_name: values.legal_name,
          };
          props.loadAuditLists(params).then(response => {
            if (props.firmList && props.firmList.length > 0) {
              let element = props.firmList.find(f => f.legal_name == values.legal_name);
              if (element) {
                getSurveys(values.year, element.id);
              }
            }
          })
        }
      } else {
        getSurveys(values.year, null);
      }
      checkSurveyBtnStatus(values.year)
    }
  }

  const checkSurveyBtnStatus = (year) => {
    if (props.session.user_type == "HQ") {
      props.checkSurveyQuestionsUploadedApi(year).then(response => {
        if (response && response.questions_uploaded) {
          setShowStartSurveyBtn(true);
          setShowUploadQuestionsBtn(false)
        } else {
          setShowStartSurveyBtn(false);
          setShowUploadQuestionsBtn(true)
        }
      }).catch(error => {
        setShowStartSurveyBtn(false);
        setShowUploadQuestionsBtn(false)
        props.postError(error, error.response && error.response.data && error.response.data.message);
      })
    }
  }

  // const onYearChange = (e) => {
  //   setYear(e.target.value);
  // };

  const resetFilterForm = () => {
    setKey(Math.floor(Math.random() * 10000));
    let year = new Date().getFullYear();
    setYear(year);
    checkSurveyBtnStatus(year)
    getSurveys(year);
  };

  const onClose = () => {
    setOpenSurveyStartDialog(false);
  }

  const onOpenDialog = () => {
    setOpenSurveyStartDialog(true);
  }

  const startSurveyClick = () => {
    props.startSurvey(year).then(response => {
      props.successReducer("Survey started !");
    }).catch(error => {
      props.postError(error, error.response && error.response.data && error.response.data.message);
    })
  }

  const onUpload = () => {
    setOpenSurveyStartDialog(false);
    let body = new FormData();
    body.append("import_file", selectedFile);
    body.append("import_type", "import_survey");
    body.append("year", year);
    body.append("data_types[0]", "survey");

    props.uploadSurveyQuestionsApi(body).then(response => {
      setSelectedFile(null);
      props.successReducer("Survey questions uploaded successfully !");
      setShowUploadQuestionsBtn(false)
      setShowStartSurveyBtn(true)
    }).catch(error => {
      setSelectedFile(null);
      props.postError(error, "Error in upload !");
    })
  }

  const downloadSampleTemplate = () => {
    downloadURI('/api/quality-assurance/survey-template-download/', 'survey-questions.xlsx');
  }

  const reportHandler = (audit_agency_id, firm_name) => {
    history.push(
      `/quality-assurance/kpi3-report/?year=${year}&firm_name=${firm_name}&firm_id=${audit_agency_id}`
    );
  };


  return <ListLoader loading={props.loading}>
    <React.Fragment>
      <React.Fragment>
        <Kpi3Filters year={year} resetFilterForm={resetFilterForm}
          key={key} onSubmit={searchSurveyRecords} />

        {showUploadQuestionsBtn && props.session.user_type == "HQ" && (
          <div className={classes.surveyBtn}>
            <CustomizedButton
              buttonType={buttonType.submit}
              buttonText={"Upload Questions"}
              clicked={onOpenDialog}
            />
          </div>)}
        {showStartSurveyBtn && props.session.user_type == "HQ" && (
          <div className={classes.surveyBtn}>
            <CustomizedButton
              buttonType={buttonType.submit}
              buttonText={"Start Survey"}
              clicked={startSurveyClick}
            />
          </div>)}
        < Kpi3SearchTable surveyFirmData={surveyData} reportHandler={reportHandler} />
        <QuestionsUploadDialog open={openSurveyStartDialog} onClose={onClose}
          downloadSampleTemplate={downloadSampleTemplate}
          onUpload={onUpload}
          selectedFile={selectedFile}
          setFileOnSelect={setFileOnSelect}
          year={year} />
      </React.Fragment>
    </React.Fragment>
  </ListLoader>;
};

const mapStateToProps = (state) => {
  return {
    loading: state.Kpi3Reducer.loading,
    session: state.session,
    userData: state.userData,
    firmList: state.auditList.list,
  };
};

const mapDispatchToProps = (dispatch) => ({
  startSurvey: (year) =>
    dispatch(startSurveyApi(year)),
  uploadSurveyQuestionsApi: (body) =>
    dispatch(uploadSurveyQuestionsApi(body)),
  getSurveyRecordsApi: (year, audit_agency) =>
    dispatch(getSurveyRecordsApi(year, audit_agency)),
  checkSurveyQuestionsUploadedApi: (year) =>
    dispatch(checkSurveyQuestionsUploadedApi(year)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi3', message)),
  loadUser: () => dispatch(loadUser()),
  loadAuditLists: (params) => dispatch(loadAuditList(params)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Kpi3);

export default connected;

