import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    color: "#0072bc !important",
  },
}));

const Kpi1 = (props) => {
  const classes = useStyles();

  return (
    <ExpansionPanel>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        className={classes.panelSummary}
      >
        <Typography className={classes.heading}>
          KPI 1 - Timeline of Audit Process
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        KPI1
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default Kpi1;
