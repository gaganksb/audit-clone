import React from "react";
import { browserHistory as history } from "react-router";
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

const useStyles = makeStyles((theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    color: "#0072bc !important",
  },
  kpi2Item: {
    color: "#0072bc !important",
    cursor: "pointer",
  },
}));


const Kpi2 = (props) => {
  const classes = useStyles();
  const { surveyId, projectId, sampled } = props;

  const showQualityTests = () => {
    history.push(`/quality-assurance/kpi2/quality/?surveyId=${surveyId}&projectId=${projectId}`)
  }

  return (
    <React.Fragment>
      {sampled ? <div className={classes.kpi2Item}
        onClick={showQualityTests}>
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<NavigateNextIcon />}
            className={classes.panelSummary}
          >
            <Typography className={classes.heading}>
              KPI 2 - Audit Quality
            </Typography>
          </ExpansionPanelSummary>
        </ExpansionPanel>
      </div> : <div className={classes.kpi2Item}>
        <ExpansionPanel disabled>
          <ExpansionPanelSummary
            expandIcon={<NavigateNextIcon />}
            className={classes.panelSummary}
          >
            <Typography className={classes.heading} style={{color: "red"}}>
              KPI 2 - Project Not Sampled For Audit Quality
            </Typography>
          </ExpansionPanelSummary>
        </ExpansionPanel>
      </div>}
    </React.Fragment>
  );
};

export default Kpi2;
