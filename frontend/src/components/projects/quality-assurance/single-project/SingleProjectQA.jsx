import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";

import ListLoader from "../../../common/listLoader";
import { getCurrentAuditFirmCountriesList } from "../../../../reducers/qualityAssurance/AuditFirmCountryList";

import Kpi1 from "./kpi1";
import Kpi2 from "./kpi2";
import Kpi3 from "./kpi3/kpi3";
import Kpi4 from "./kpi4";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    padding: theme.spacing(3),
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    color: "#0072bc !important",
  },
}));

const SingleProjectQA = (props) => {
  const [auditUsers, setAuditUsers] = useState([]);
  const [auditFirmCountries, setAuditFirmCountries] = useState([]);
  const [teamCompositionId, setTeamCompositionId] = useState([]);

  const [fieldSurveyId, setFieldSurveyId] = useState(null);
  const [auditorSurveyId, setAuditorSurveyId] = useState(null);
  const [partnerSurveyId, setPartnerSurveyId] = useState(null);
  const [auditQualitySurveyId, setAuditQualitySurveyId] = useState(null);
  const [auditTimelineSurveyId, setAuditTimelineSurveyId] = useState(null);

  const classes = useStyles();

  const {
    project,
    getCurrentAuditFirmCountriesList,
    loading,
    loadingProject,
  } = props;

  useEffect(() => {
    if (project && project.audit_firm && project.audit_firm.id) {
      let year = project.cycle.year;
      let params = {
        year: year,
        audit_agency: project.audit_firm.id,
      };
      getCurrentAuditFirmCountriesList(params).then(response => {
        setAuditFirmCountries(response);
      }).catch(error => {
        throw error;
      });

      if (project.agreement_survey && project.agreement_survey.length > 0) {

        for (let item in project.agreement_survey) {
          let survey = project.agreement_survey[item]
          if (survey.survey.type == "field") {
            setFieldSurveyId(survey.id)
          } else if (survey.survey.type == "auditor") {
            setAuditorSurveyId(survey.id)
          } else if (survey.survey.type == "partner") {
            setPartnerSurveyId(survey.id)
          } else if (survey.survey.type == "audit_quality_survey") {
            setAuditQualitySurveyId(survey.id)
          }
          else if (survey.survey.type == "audit_timeline_survey") {
            setAuditTimelineSurveyId(survey.id)
          }
        }
      }
    }
  }, [project]);

  useEffect(() => {
    if (auditFirmCountries && auditFirmCountries.length > 0 && project && project.audit_firm) {
      let filteredList = auditFirmCountries.filter(
        (record) => record.audit_agency.id == project.audit_firm.id
      );
      if (filteredList && filteredList.length > 0) {
        let users = filteredList[0].team_composition_users;
        setTeamCompositionId(filteredList[0].id)
        setAuditUsers([...users]);
      }
    }
  }, [auditFirmCountries]);

  return (
    <div className={classes.root}>
      <ListLoader loading={loading || loadingProject}>
        <Kpi1 surveyId={auditTimelineSurveyId} />
        {
          project &&
          project.agreement_survey &&
          project.agreement_survey.find(item => item.survey && item.survey.type === "audit_quality_survey") ? (
            <Kpi2 surveyId={auditQualitySurveyId} projectId={project.id} sampled={true}/>
          ): <Kpi2 surveyId={auditQualitySurveyId} projectId={project.id} sampled={false}/>}
        <Kpi3 fieldSurveyId={fieldSurveyId} auditorSurveyId={auditorSurveyId} partnerSurveyId={partnerSurveyId} />
        <Kpi4 auditUsers={auditUsers} teamCompositionId={teamCompositionId} year={project && project.cycle && project.cycle.year} />
      </ListLoader>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    auditFirmCountries: state.AuditFirmCountryList.auditFirmCountries,
    project: state.projectDetails.project,
    loading: state.AuditFirmCountryList.loading,
    loadingProject: state.projectDetails.loading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCurrentAuditFirmCountriesList: (params) =>
    dispatch(getCurrentAuditFirmCountriesList(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleProjectQA);
