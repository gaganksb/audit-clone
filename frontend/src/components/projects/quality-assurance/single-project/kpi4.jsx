import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ReportProblemIcon from "@material-ui/icons/ReportProblem";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { browserHistory as history, } from "react-router";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles((theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    paddingTop: theme.spacing(0),
    color: "#0072bc !important",
  },

  gridBorder: {
    border: "1px solid gray",
    height: "50px",
  },
  userLabel: {
    fontSize: theme.typography.pxToRem(12),
    padding: theme.spacing(1),
    color: "#0072bc !important",
    cursor: 'pointer',
    "pointer-events": "auto",
  },
}));

const Kpi4 = (props) => {
  const [activeAuditUsers, setActiveUsers] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    setActiveUsers([...props.auditUsers]);
    console.log("auditUsers=", props.auditUsers)
  }, [props.auditUsers]);

  const showMemberDetails = (userId) => {
    history.push(`/quality-assurance/team-composition/member-profile?team=${props.teamCompositionId}&user=${userId}&year=${props.year}`)
  }

  let getProfiles = () => {
    return activeAuditUsers && activeAuditUsers.length > 0 ? (
      <Grid container fullwidth>
        {activeAuditUsers.filter(item=>item.agreement_member && item.agreement_member.reviewer === true).map((record) => {
          return (
            <Grid item xs={12} container className={classes.gridBorder}>
              <Grid item xs={11}>
                <div onClick={() => showMemberDetails(record.agreement_member.id)}>
                  <Typography className={classes.userLabel}>
                    <u>{record.agreement_member && record.agreement_member.user.fullname}</u>
                  </Typography>
                </div>

              </Grid>
              <Grid item xs={1} align="right" justifyContent="center" style={{ paddingTop: '8px', paddingRight: '8px', }}>
                {record.status == "todo" ? (
                  <Tooltip title={record && record.status}>
                    <ReportProblemIcon style={{ color: "yellow", }}>
                      {record.status}
                    </ReportProblemIcon>
                  </Tooltip>
                ) : record.status == "done" ? (<Tooltip title={record.status}>
                  <CheckCircleIcon style={{ color: "green", }}>{record.status}</CheckCircleIcon>
                </Tooltip>) : (
                  record.status
                )}
              </Grid>

            </Grid>
          );
        })}
      </Grid >
    ) : (
      "No data found"
    );
  }


  return (
    <ExpansionPanel>
      <ExpansionPanelSummary
        expandIcon={<ExpandMoreIcon />}
        className={classes.panelSummary}
      >
        <Typography className={classes.heading}>
          KPI 4 - Audit Team Composition
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{getProfiles()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default Kpi4;
