/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';

const filter = createFilterOptions();

export default function FreeSoloCreateOption(props) {
    const [value, setValue] = useState("");
    const [key, setKey] = useState(1);
    useEffect(() => {
        setValue(props.current)
    }, [props.current])
    return (
        <Autocomplete
            value={value}
            key={key}
            name={props.name}
            label={props.label}
            onChange={(event, newValue) => {
                if (newValue) {
                    props.onAdd(newValue.inputValue);
                    setKey(prev => prev + 1)
                }
            }}
            filterOptions={(options, params) => {
                const filtered = filter(options, params);

                // Suggest the creation of a new value
                if (params.inputValue !== '') {
                    filtered.push({
                        inputValue: params.inputValue,
                        title: `Add "${params.inputValue}"`,
                    });
                }

                return filtered;
            }}
            selectOnFocus
            id={props.id}
            options={[]}
            getOptionLabel={(option) => {
                // Value selected with enter, right from the input
                if (typeof option === 'string') {
                    return option;
                }
                // Add "xxx" option created dynamically
                if (option.inputValue) {
                    return option.inputValue;
                }
                // Regular option
                return option.title;
            }}
            renderOption={(option) => option.title}
            style={{ width: "100%" }}
            freeSolo
            renderInput={(params) => (
                <TextField {...params} label={props.label}
                    onKeyDown={e => {
                        if (e.keyCode === 13 && e.target.value && e.target.value.trim() != "") {
                            let newValue = e.target.value;
                            if (value) {
                                newValue = newValue.concat(newValue)
                            }
                            props.onAdd(newValue);
                        }
                    }} />
            )}
        />
    );
}

