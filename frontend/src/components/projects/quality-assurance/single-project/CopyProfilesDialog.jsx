import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from "../../../common/customizedButton";
import { buttonType } from "../../../../helpers/constants";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing(3),
    },
    btnGrpAlign: {
        display: "flex",
        marginLeft: "auto",
        justifyContent: "space-between",
    },
}));

export default function CopyProfilesDialog(props) {
    const classes = useStyles();

    const [state, setState] = React.useState([]);

    useEffect(() => {
        if (props.ppa) {
            let projects = [];
            props.ppa.forEach(element => {
                projects.push({ name: element, checked: true })
            })
            setState([...projects])
        }
    }, [props.ppa])

    const handleChange = (event) => {
        let currentState = [...state];
        currentState = currentState.map(s => {
            if (s.name == event.target.name) {
                s.checked = event.target.checked;
                return s;
            } else {
                return s;
            }
        });
        setState([...currentState]);
    };

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));


    let controlLabels = state && state.map((record, index) => {
        return <FormControlLabel key={index}
            control={<Checkbox checked={record.checked} onChange={handleChange} name={record.name} />}
            label={record.name}
        />
    })
    return (
        <div>
            <Dialog
                open={props.open}
                onClose={props.onClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullScreen={fullScreen}
            >
                <DialogTitle id="alert-dialog-title">{"Profile will be copied to the following projects"}</DialogTitle>
                <DialogContent dividers>

                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormLabel component="legend">Projects</FormLabel>
                        <FormGroup>
                            {controlLabels}
                        </FormGroup>
                    </FormControl>

                </DialogContent>
                <DialogActions>
                    <div className={classes.btnGrpAlign}>
                        <div>
                            <CustomizedButton
                                buttonType={buttonType.cancel}
                                buttonText={"Cancel"}
                                clicked={props.onClose}
                            />
                            <CustomizedButton
                                buttonType={buttonType.submit}
                                buttonText={"Submit"}
                                clicked={() => props.onOk(state)}
                            />
                        </div>
                    </div>
                </DialogActions>
            </Dialog>
        </div>
    );
}
