import React from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const QuestionHeader = props => {
  return <React.Fragment>
    <Grid container spacing={2} style={{ marginBottom: 16 }}>
      <Grid item xs={12} md={9}>
        <Typography variant="subtitle1">QA TESTS</Typography>
      </Grid>
    </Grid>
  </React.Fragment >
}

export default QuestionHeader;