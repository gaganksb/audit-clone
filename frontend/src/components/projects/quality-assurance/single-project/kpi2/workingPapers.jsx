import React, { useState, useEffect, } from 'react';
import { connect } from "react-redux";
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import RequestNewPaper from './requestNewPaper';
import EditPaper from './editPaper';
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import {
  addNewWorkingPaper, updateWorkingPaper,
  uploadWorkingPaperApi,
  deleteWorkingPaperApi,
} from "../../../../../reducers/qualityAssurance/kpi2Reducer";
import { downloadURI } from '../../../../../helpers/others';
import DeleteWorkingPaperDialog from './deleteWorkingPaperDialog';
import {normalizeDate} from "../../../../../helpers/dates"

function Row(props) {
  const { row } = props;
  const [open, setOpen] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);

  const [openEditPaper, setOpenEditPaper] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);

  const onFileChange = event => {
    setSelectedFile(event.target.files[0]);
  };

  const onDeleteOk = () => {
    setOpenDeleteDialog(false);
    props.onDeleteWorkingPaper(row.id);
  }

  const onDeleteCancel = () => {
    setOpenDeleteDialog(false);
  }

  const onCloseEditPaper = () => {
    setOpenEditPaper(false);
  }

  const openEditPaperDlg = () => {
    setOpenEditPaper(true)
  }

  const uploadFile = () => {
    let id = row.id;

    let body = new FormData();
    body.append("file", selectedFile);

    props.uploadWorkingPaper(id, body);
    setSelectedFile(null);
  }

  const onSubmitEditPaper = (name, description, reviewed) => {
    setOpenEditPaper(false);

    let id = row.id;
    let body = {
      ...row,
      title: name,
      description: description,
      reviewed: reviewed,
    }

    props.updateWorkingPaper(id, body);
  }

  const downloadImage = path => {
    downloadURI(`${path}`, 'working-paper');
  }

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell align="center">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row" align="center">
          {row.title}
        </TableCell>
        <TableCell align="center">{row.status}</TableCell>
        <TableCell align="center">
          {row.status == "requested" && props.isAuditorUser ?
            <React.Fragment>
              <input type="file"
                onChange={onFileChange} />
              {selectedFile &&
                <CustomizedButton
                  buttonType={buttonType.submit}
                  buttonText={"Upload"}
                  clicked={uploadFile}
                />
              }
            </React.Fragment>
            :
            row.status == "requested" && "N/A"
          }
          {(row.status == "received" || row.status == "reviewed") &&
            <CustomizedButton
              buttonType={buttonType.submit}
              buttonText={"Download"}
              clicked={() => downloadImage(row.file.filepath)}
            />
          }
        </TableCell>
        <TableCell align="center">
          <div style={{ display: "flex", justifyContent: "space-evenly" }}>
            {props.isHQUser ?
              <React.Fragment>
                <EditIcon onClick={openEditPaperDlg} style={{ cursor: "pointer" }} />
                <DeleteIcon style={{ cursor: "pointer" }} onClick={() => setOpenDeleteDialog(true)} />
              </React.Fragment>
              :
              "N/A"
            }
          </div>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell colSpan={4} align="center">Description</TableCell>
                    <TableCell align="center">Date Requested</TableCell>
                    <TableCell align="center">Date Received</TableCell>
                    <TableCell align="center">Date Reviewed</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row" colSpan={4} align="center">
                      {row.description}
                    </TableCell>
                    <TableCell align="center">{normalizeDate(row.created_date)}</TableCell>
                    <TableCell align="center">{row.date_received}</TableCell>
                    <TableCell align="center">{row.date_reviewed}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
      <EditPaper open={openEditPaper} currentPaper={row}
        onClose={onCloseEditPaper} onSubmitClick={onSubmitEditPaper} />
      <DeleteWorkingPaperDialog open={openDeleteDialog} onClose={onDeleteCancel} onOk={onDeleteOk} />
    </React.Fragment>
  );
}


const WorkingPapers = (props) => {
  const [openNewPaper, setOpenNewPaper] = useState(false);
  const [workingPapers, setWorkingPapers] = useState([]);
  const isHQUser = props.session.user_type == "HQ";
  const isAuditorUser = props.session.user_type == "AUDITOR";

  useEffect(() => {
    if (props.workingPapers) {
      setWorkingPapers(props.workingPapers);
    }
  }, [props.workingPapers])

  const onCloseNewPaper = () => {
    setOpenNewPaper(false);
  }

  const openNewPaperDlg = () => {
    setOpenNewPaper(true)
  }

  const onSubmitNewPaper = (name, description) => {
    setOpenNewPaper(false);
    let body = {
      title: name,
      description: description,
      agreement: props.agreement,
    }
    props.addNewWorkingPaper(body).then(response => {
      props.successReducer("New working paper requested !");
      setWorkingPapers([...workingPapers, response]);
    }).catch(error => {
      props.postError(error, "Requesting new working paper failed !")
    })

  }

  const uploadFile = (id, body) => {
    props.uploadWorkingPaperApi(id, body).then(response => {
      props.successReducer("working paper uploaded !");
      let updatedWorkingPapers = [...workingPapers];
      updatedWorkingPapers = updatedWorkingPapers.map(paper => {
        if (paper.id == id) {
          return response;
        }
        return paper;

      });
      setWorkingPapers([...updatedWorkingPapers]);
    }).catch(error => {
      props.postError(error, "Upload working paper failed !")
    })
  }

  const onSubmitEditPaper = (id, body) => {
    props.updateWorkingPaper(id, body).then(response => {
      props.successReducer("working paper updated !");
      let updatedWorkingPapers = [...workingPapers];
      updatedWorkingPapers = updatedWorkingPapers.map(paper => {
        if (paper.id == id) {
          return response;
        }
        return paper;

      });
      setWorkingPapers([...updatedWorkingPapers]);
    }).catch(error => {
      props.postError(error, "Updating working paper failed !")
    })
  }

  const onDeleteWorkingPaper = (id) => {
    props.deleteWorkingPaperApi(id).then(response => {
      props.successReducer("working paper deleted !");
      let updatedWorkingPapers = workingPapers.filter(paper => paper.id !== id);
      setWorkingPapers([...updatedWorkingPapers]);
    }).catch(error => {
      props.postError(error, "Delete working paper failed !");
    })
  }

  return (
    <React.Fragment>
      <div style={{ display: "flex", justifyContent: "space-between", marginBottom: 32, }}>
        <Typography variant="subtitle1">WORKING PAPERS</Typography>
        {isHQUser &&
          <CustomizedButton
            buttonType={buttonType.submit}
            buttonText={"Request New"}
            clicked={openNewPaperDlg}
          />
        }
      </div>
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell align="center" />
              <TableCell align="center">Name</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">File</TableCell>
              <TableCell align="center" colSpan={2}>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {workingPapers && workingPapers.length > 0 ? workingPapers.map((row, index) => (
              <Row key={index} row={row} updateWorkingPaper={onSubmitEditPaper}
                uploadWorkingPaper={uploadFile}
                onDeleteWorkingPaper={onDeleteWorkingPaper}
                isHQUser={isHQUser} isAuditorUser={isAuditorUser} />
            )) :
              <TableRow>
                <TableCell align="center">No data found </TableCell>
              </TableRow>
            }
          </TableBody>
        </Table>
      </TableContainer>
      <RequestNewPaper open={openNewPaper}
        onClose={onCloseNewPaper} onSubmitClick={onSubmitNewPaper} />
    </React.Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    session: state.session,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addNewWorkingPaper: (body) => dispatch(addNewWorkingPaper(body)),
  updateWorkingPaper: (id, body) => dispatch(updateWorkingPaper(id, body)),
  deleteWorkingPaperApi: (id) => dispatch(deleteWorkingPaperApi(id)),
  uploadWorkingPaperApi: (id, body) => dispatch(uploadWorkingPaperApi(id, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'working-papers-error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkingPapers);

export default connected;
