import React, { useState, useEffect, } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { useTheme } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";


const EditPaper = (props) => {
  const { onClose, onSubmitClick, open, currentPaper, } = props;

  useEffect(() => {
    if (currentPaper) {
      setName(currentPaper.title);
      setDescription(currentPaper.description);
      setReviewed(currentPaper.reviewed);
    }
  }, [currentPaper])

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [reviewed, setReviewed] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  const areFieldsValid = () => {
    return (name && name.trim() !== "" && description && description.trim() !== "");
  }

  return (
    <Dialog onClose={onClose} open={open} fullScreen={fullScreen}>
      <DialogTitle>Edit Working Paper</DialogTitle>
      <Divider />
      <DialogContent>
        <Grid container spacing={3}>
          <Grid item xs={12} container>
            <Grid item xs={12}><label>Name</label></Grid>
            <Grid item xs={12}>
              <Input
                type="text"
                value={name}
                onChange={e => setName(e.target.value)}
                variant="outlined"
                style={{ width: "100%", }}
              />
            </Grid>
          </Grid>

          <Grid item xs={12} container>
            <Grid item xs={12}><label>Description</label></Grid>
            <Grid item xs={12}>
              <TextareaAutosize
                aria-label="Description"
                style={{ width: "100%", height: 100 }}
                value={description}
                onChange={e => setDescription(e.target.value)}
              />
            </Grid>
          </Grid>

          <Grid item xs={12} container>
            <Grid item xs={12}>
              Reviewed: <Checkbox
                checked={reviewed}
                onChange={e => setReviewed(e.target.checked)}
              />
            </Grid>
          </Grid>
        </Grid>

      </DialogContent>
      <DialogActions>
        <CustomizedButton
          buttonType={buttonType.cancel}
          buttonText={"Cancel"}
          clicked={onClose}
        />
        <CustomizedButton
          buttonType={buttonType.submit}
          buttonText={"Submit"}
          clicked={() => onSubmitClick(name, description, reviewed)}
          disabled={!areFieldsValid()}
        />
      </DialogActions>
    </Dialog>
  );
}

export default EditPaper;
