import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import { browserHistory as history } from "react-router";
import { loadProject } from '../../../../../reducers/projects/projectsReducer/getProjectDetails';
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";
import { getSurveyDetails, postSurveyResponse, } from "../../../../../reducers/qualityAssurance/Kpi3Reducer";
import CustomizedButton from "../../../../common/customizedButton";
import { buttonType, getLocalLanguage } from "../../../../../helpers/constants";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Question from "./question";
import QuestionHeader from "./questionHeader";
import WorkingPapers from "./workingPapers";

const useStyles = makeStyles((theme) => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  headerItems: {
    display: "flex",
    alignItems: "center",
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "flex-end",
  },
  backTick: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
    resize: 'vertical',
  },
  label: {
    color: "#0072bc !important",
  },
}));

const Kpi2QATests = props => {
  const { session, handleSubmit, project, loadProjectDetails, } = props;
  const classes = useStyles();
  const [surveyDetails, setSurveyDetails] = useState(null);
  const isHQUser = session.user_type == "HQ";
  const lang = navigator.language;
  const localLang = getLocalLanguage(lang);
  const [categories, setCategories] = useState([]);
  const [workingPapers, setWorkingPapers] = useState([]);
  const [errors, setErrors] = useState([]);
  const [isEditable, setIsEditable] = useState(false);

  useEffect(() => {
    if (project && project.working_papers) {
      setWorkingPapers(project.working_papers);
    }
  }, [project])

  useEffect(() => {
    if (!project || !project.working_papers) {
      if (props.location.query.projectId) {
        let projectId = parseInt(props.location.query.projectId);
        loadProjectDetails(projectId);
      }
    }
  }, [])

  useEffect(() => {
    if (session.user_type == "HQ") {
      let surveyId = props.location.query.surveyId;
      if (surveyId) {
        props.getSurveyDetails(surveyId).then(response => {
          setSurveyDetails(response);
          let initialValues = {};
          if (response && response.survey && response.survey.survey_questions) {
            let identifiedCategories = new Set();
            let questions = response.survey.survey_questions;
            for (let item in questions) {
              let question = questions[item];
              if (question) {
                identifiedCategories.add(question.category.title);

                //retrieve answers and comments
                if (question.survey_response && question.survey_response.length > 0) {
                  initialValues["id_" + question.id] = question.survey_response[0].answer.answer;
                  if (question.survey_response[0].answer.comment) {
                    initialValues["id_" + "comments_" + question.id] = question.survey_response[0].answer.comment;
                    initialValues["id_" + "auditor_comments_" + question.id] = question.survey_response[0].answer.auditor_comment;
                  }
                }
              }
            }

            props.initialize({
              ...initialValues,
            });
            setCategories(identifiedCategories);

            let isFinalized = response.status != "done";
            setIsEditable(isFinalized && isHQUser);
          }
        }).catch(error => {
          props.postError(error, "Survey details loading failed !");
        })
      }
    }
  }, [])

  const backHandler = () => {
    history.goBack();
  };

  const onSave = values => {
    saveResults(values, "save");
  }

  const onFinalize = values => {
    saveResults(values, "finalize");
  }

  const saveResults = (values, action) => {
    setErrors([]);
    let findErrors = [];

    let responses = [];

    surveyDetails.survey.survey_questions.map(question => {
      let elementId = "id_" + question.id;
      let value = values[elementId];
      if (question.required && question.id && question.id != "" && (!value || value == null || value.trim() == "")) {
        findErrors.push(question.id);
      } else if (value && value.trim() != "") {

        let responseQuestion = question.id;
        let answer = value;
        let comment = null;
        let auditor_comment = null

        if (answer == "No" || answer == "Partially") {
          let commentElementId = "id_" + "comments_" + question.id;
          comment = values[commentElementId];
          let auditorCommentElementId = "id_" + "auditor_comments_" + question.id;
          auditor_comment = values[auditorCommentElementId];
          if (!auditor_comment || auditor_comment == null || auditor_comment.trim() == "") {
            findErrors.push(question.id);
          }
        } else {
          let commentElementId = "id_" + "comments_" + question.id;
          comment = values[commentElementId];
          let auditorCommentElementId = "id_" + "auditor_comments_" + question.id;
          auditor_comment = values[auditorCommentElementId];
        }
        responses.push({ question: responseQuestion, answer: { answer, comment, auditor_comment } })

      }
    })

    if (findErrors && findErrors.length > 0) {
      setErrors([...findErrors])
      return;
    }

    let body = {
      "action": action,
      responses,
    }

    const surveyId = props.location && props.location.query && props.location.query.surveyId;

    props.postSurveyResponse(surveyId, body).then(response => {
      if (action == "save" || action == "update") {
        props.successReducer("Survey response saved !");
      }
      if (action == "finalize") {
        setIsEditable(false)
        props.successReducer("Survey response finalized !")
      }
    }).catch(error => {
      action == "save" || action == "update" ? props.postError(error, "Survey save failed !") : props.postError(error, "Survey finalized failed !");
    });
  }

  const showQuestions = () => {

    if (surveyDetails && surveyDetails.survey
      && surveyDetails.survey.survey_questions && surveyDetails.survey.survey_questions.length > 0) {

      let categoryArray = [...categories];
      let categoriesDisplay = categoryArray.map((categoryItem, index) => {
        let questions = surveyDetails.survey.survey_questions.filter(q => q.category.title == categoryItem);
        let questionsDisplay = questions.map((question, index) => {
          let isQuestionHasError = errors.includes(question.id);
          let answer = question.survey_response && question.survey_response.length > 0
            && question.survey_response[0].answer.answer;
          question.selectedValue = answer;
          return <Question session={session} question={question} lang={localLang} key={index}
            isQuestionHasError={isQuestionHasError} edit={isEditable} />

        })
        return <React.Fragment key={index}>
          <Typography variant="subtitle1" style={{ marginTop: 8, marginBottom: 8, }}>{categoryItem}</Typography>
          {questionsDisplay}
        </React.Fragment>

      }
      )
      return categoriesDisplay;
    }
  }


  return <React.Fragment>
    <Paper className={classes.header} variant="outlined">
      <div className={classes.headerItems}>
        <div className={classes.backTick}>
          <ArrowBackIosIcon onClick={backHandler} />
          <label>
            Project Agreement Title - KPI 2: Audit Quality
          </label>
        </div>

        {isHQUser && surveyDetails && (
          <div className={classes.btnGrpAlign}>
            <CustomizedButton
              buttonType={buttonType.submit}
              buttonText={"Save"}
              clicked={handleSubmit(onSave)}
            />
            <CustomizedButton
              buttonType={buttonType.submit}
              buttonText={"Finalize"}
              clicked={handleSubmit(onFinalize)}
            />
          </div>
        )}
      </div>
    </Paper>

    {errors && errors.length > 0 &&
      <Paper className={classes.header} variant="outlined">
        <Typography variant="subtitle2" color="secondary">Please submit all required questions.</Typography>
      </Paper>
    }

    <Paper className={classes.header} variant="outlined">
      <WorkingPapers workingPapers={workingPapers} agreement={props.location.query.projectId} />
    </Paper>

    {isHQUser &&
      <Paper className={classes.header} variant="outlined">

        <form style={{ marginTop: 16, }}>
          <QuestionHeader />
          {showQuestions()}

        </form>
      </Paper>
    }

  </React.Fragment >
}


const SurveyForm = reduxForm({
  form: "auditQualitySurveyForm",
})(Kpi2QATests);

const mapStateToProps = (state) => {
  return {
    session: state.session,
    project: state.projectDetails.project,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getSurveyDetails: (surveyId) =>
    dispatch(getSurveyDetails(surveyId)),
  postSurveyResponse: (id, body) =>
    dispatch(postSurveyResponse(id, body)),
  loadProjectDetails: id => dispatch(loadProject(id)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'kpi2-error', message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(SurveyForm);

export default connected;