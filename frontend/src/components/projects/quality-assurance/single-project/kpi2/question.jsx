import React, { useState, useEffect } from 'react';
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import { makeStyles } from "@material-ui/core/styles";
import { Field } from 'redux-form';


const useStyles = makeStyles((theme) => ({
  inputTextArea: {
    width: "100%",
    resize: "vertical",
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  checked: {},
}));


const Question = props => {
  const classes = useStyles();
  const [question, setQuestion] = useState(props.question);
  const { lang, edit, isQuestionHasError, session } = props;

  useEffect(() => {
    setQuestion(props.question);
  }, [props.question])

  const handleChange = e => {
    let newSelectedValue = e.target.value;
    if (newSelectedValue) {
      setQuestion(old => ({ ...old, selectedValue: newSelectedValue }));
    }
  }


  const bgColor = isQuestionHasError ? "#ffcccc" : "";

  return <div style={{ backgroundColor: bgColor, }}>
    <Grid container spacing={2}>
      <Grid item xs={12} md={9}>
        {isQuestionHasError && <p style={{ color: "red" }}>Required * </p>}{question.title[lang]}
      </Grid>
      {question && question.option && question.option[lang] && question.option[lang].length > 0 && question.option[lang].map((option, index) => {
        return <Grid item xs={4} md={1} align="center" key={index}>
          <label>
            <Field name={"id_" + question.id} component="input" type="radio" value={option}
              checked={question.selectedValue == option}
              onChange={handleChange}
              disabled={!edit}
            />
            {option}
          </label>
        </Grid>
      })
      }

      {(question.selectedValue === "No" || question.selectedValue === "Partially")
        &&
        <Grid item xs={12}>
          {!(question.survey_response && question.survey_response.length > 0 && question.survey_response[0].answer.comment)
            && <p style={{ color: "red" }}>{question.comment_request_msg[lang]}</p>}
          HQ Comment:
          <Field
            name={"id_" + "comments_" + question.id}
            component="textarea"
            value={question.comments}
            className={classes.inputTextArea}
            disabled={!edit || (session && session.user_type === "AUDITOR")}
          />
          Auditor Comment:
          <Field
            name={"id_" + "auditor_comments_" + question.id}
            component="textarea"
            value={question.comments}
            className={classes.inputTextArea}
            disabled={!edit || (session && session.user_type === "HQ")}
          />
        </Grid>
      }

      <Grid item xs={12}>
        <Divider />
      </Grid>
    </Grid>
  </div>
}

export default Question;