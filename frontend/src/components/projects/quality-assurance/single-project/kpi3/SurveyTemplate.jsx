import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormLabel from "@material-ui/core/FormLabel";
import AnswerOptions from "./AnswerOptions";
import Divider from "@material-ui/core/Divider";


const useStyles = makeStyles((theme) => ({
  header: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  labelColor: {
    color: "#0072bc !important",
  },
  radio: {
    "&$checked": {
      color: "#4B8DF8",
    },
  },
  checked: {},
}));

const SurveyTemplate = (props) => {
  const classes = useStyles();
  const { lang } = props;
  const [surveyData] = useState(props.questionData);
  const [subQuestion] = useState(props.subQuestion);

  return (
    <React.Fragment>
      <div className={classes.header}>
        <Grid container>
          <Grid item xs={12} container spacing={2}>
            <Grid item xs={12}>
              <FormLabel component="legend">
                <Typography variant="subtitle2" className={classes.labelColor}>
                  {surveyData.title[lang]}
                </Typography>
              </FormLabel>
            </Grid>
            <Grid item xs={12} container>
              <AnswerOptions question_options={surveyData.option}
                id={"id_" + surveyData.id}
                lang={lang}
                type={surveyData.type}
                edit={props.edit} />
            </Grid>

            {
              subQuestion &&
              <Grid item xs={12} container className={classes.subQuestion}>
                <Grid item xs={12}>
                  <Typography variant="subtitle2" className={classes.labelColor}>
                    {subQuestion.title[lang]}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <AnswerOptions question_options={subQuestion.option}
                    id={"id_" + subQuestion.id}
                    lang={lang}
                    type={subQuestion.type}
                    edit={props.edit} />
                </Grid>
              </Grid>
            }

          </Grid>
        </Grid>
      </div>
      <Divider />
    </React.Fragment>
  );
};

export default SurveyTemplate;
