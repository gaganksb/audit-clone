import React from 'react';
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Field } from 'redux-form';


const useStyles = makeStyles((theme) => ({
    labelColor: {
        color: "#0072bc !important",
    },
    inputTextArea: {
        width: "100%",
        height: "100%",
        minHeight: "50px",
        padding: theme.spacing(1),
        resize: "vertical",
    },

    radio: {
        "&$checked": {
            color: "#4B8DF8",
        },
    },
    checked: {},
}));


const AnswerOptions = props => {
    const classes = useStyles();
    const { id, type, question_options, answer, lang, edit } = props;

    let options = null;

    if (question_options) {
        if (type == "radio") {
            options = question_options[lang].map((item, index) => {
                return <Grid item xs={12} md={3} key={index}>
                    <label>
                        <Field name={id} component="input" type="radio" value={item}
                            disabled={!edit} />
                        {item}
                    </label>
                </Grid>

            });
        } else if (type == "text" || type == "textarea") {
            options = <Grid item xs={12}>
                <Field name={id} component="textarea"
                    value={answer} rows="7" className={classes.inputTextArea} disabled={!edit} />
            </Grid>
        } else if (type == "select" || type == "multiselect") {
            options = <Grid item xs={12} md={3}>
                <label>
                    <Field name={id} component="select"
                        value={answer} multiple={type == "multi_select"} disabled={!edit}>
                        <option
                            value="">{type == "select" ? "Select One" : "Multiple Select"}</option>
                        {question_options[lang].map((name, index) => {
                            return <option key={index}
                                value={name}>{name}</option>
                        })}
                    </Field>
                </label>
            </Grid>
        }
    }
    return (
        <React.Fragment>
            {options}
        </React.Fragment>
    )
}

export default AnswerOptions;