import React from "react";
import { connect } from 'react-redux';
import { makeStyles } from "@material-ui/core/styles";
import { browserHistory as history } from "react-router";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import { getLocalLanguage } from "../../../../../helpers/constants";

const useStyles = makeStyles((theme) => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  kpi3Item: {
    color: "#0072bc !important",
    cursor: "pointer",
  },
  panelSummary: {
    flexDirection: "row-reverse",
    padding: theme.spacing(1),
    color: "#0072bc !important",
  },
}));


const Kpi3 = (props) => {
  const { fieldSurveyId, auditorSurveyId, partnerSurveyId, session, project, } = props;
  const classes = useStyles();

  const lang = navigator.language;
  const localLang = getLocalLanguage(lang);


  const showSurvey = (id, version, survey_type) => {
    let edit = isSurveyEditable(survey_type);
    history.push(`/quality-assurance/kpi3/survey/?id=${id}&lang=${localLang}&version=${version}&edit=${edit}`)
  }

  const isSurveyEditable = (survey_type) => {
    if (project) {
      if (survey_type == "FO") {
        if (project.agreement_member.unhcr.focal || project.agreement_member.unhcr.alternate_focal) {
          return true;
        }
      } else if (survey_type == "AUDITOR") {
        if (project.agreement_member.auditing) {
          if (project.agreement_member.auditing.reviewers) {
            let isUserAuditReviewer = project.agreement_member.auditing.reviewers.some(r => r.id == session.userId)
            return isUserAuditReviewer;
          }
        }
      } else if (survey_type == "PARTNER") {
        if (project.agreement_member.partner.focal || project.agreement_member.partner.alternate_focal) {
          return true;
        }
      }
    }
    return false;
  }


  return (
    <React.Fragment>
      {session.user_type == "PARTNER" && partnerSurveyId &&
        <div className={classes.kpi3Item} onClick={() => showSurvey(partnerSurveyId, 3.1, "PARTNER")}>


          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<NavigateNextIcon />}
              className={classes.panelSummary}
            >
              <Typography className={classes.heading}>
                KPI 3.1 - Partner Survey
              </Typography>
            </ExpansionPanelSummary>
          </ExpansionPanel>
        </div>
      }

      {session.user_type == "FO" && fieldSurveyId &&
        <div className={classes.kpi3Item} onClick={() => showSurvey(fieldSurveyId, 3.2, "FO")}>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<NavigateNextIcon />}
              className={classes.panelSummary}
            >
              <Typography className={classes.heading}>
                KPI 3.2 - Field Office Survey
              </Typography>
            </ExpansionPanelSummary>
          </ExpansionPanel>
        </div>
      }

      {session.user_type == "AUDITOR" && auditorSurveyId &&
        <div className={classes.kpi3Item} onClick={() => showSurvey(auditorSurveyId, 3.3, "AUDITOR")}>
          <ExpansionPanel>
            <ExpansionPanelSummary
              expandIcon={<NavigateNextIcon />}
              className={classes.panelSummary}
            >
              <Typography className={classes.heading}>
                KPI 3.3 - Auditor Survey
              </Typography>
            </ExpansionPanelSummary>
          </ExpansionPanel>
        </div>
      }

    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    session: state.session,
    project: state.projectDetails.project,
  };
};

export default connect(mapStateToProps, null)(Kpi3);
