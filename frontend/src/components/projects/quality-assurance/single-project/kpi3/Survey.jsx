import React, { useState, useEffect } from "react";
import { connect } from 'react-redux';
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { browserHistory as history } from "react-router";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import CustomizedButton from "../../../../common/customizedButton";
import { buttonType } from "../../../../../helpers/constants";
import SurveyTemplate from "./SurveyTemplate";
import { getSurveyDetails, postSurveyResponse, } from "../../../../../reducers/qualityAssurance/Kpi3Reducer";
import { showSuccessSnackbar } from "../../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../../reducers/errorReducer";

import ListLoader from "../../../../common/listLoader";
import { reduxForm } from "redux-form";
import FinalizeDialog from '../../kpi4/report/FinalizeDialog';


const useStyles = makeStyles((theme) => ({
  header: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    margin: theme.spacing(3),
  },
  bodyHeader: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),

    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    margin: theme.spacing(3),
  },
  headerItems: {
    display: "flex",
    alignItems: "center",
  },
  btnGrpAlign: {
    display: "flex",
    marginLeft: "auto",
    justifyContent: "space-between",
  },
  backTick: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },
  root: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
  },
  input: {
    width: "100%",
    height: "100%",
  },
  labelColor: {
    color: "#0072bc !important",
  },
}));

const Survey = (props) => {

  const { loading, handleSubmit, } = props;
  const classes = useStyles();
  const [surveyDetails, setSurveyDetails] = useState(null);
  const [lang, setLang] = useState('english');
  const [formValues, setFormValues] = useState(null);
  const [openFinalDialog, setOpenFinalDialog] = useState(false);
  const [isEditable, setIsEditable] = useState(false);

  const [errors, setErrors] = useState([]);

  const backHandler = () => {
    history.goBack();
  };

  useEffect(() => {
    const surveyId = props.location && props.location.query && props.location.query.id;
    props.getSurveyDetails(surveyId).then(response => {
      setSurveyDetails(response);

      if (response && response.survey && response.survey.survey_questions && response.survey.survey_questions.length > 0) {
        let initialValues = {};
        for (let element in response.survey.survey_questions) {
          let question = response.survey.survey_questions[element];
          if (question.survey_response && question.survey_response.length > 0) {
            initialValues["id_" + question.id] = question.survey_response[0].answer.answer;

            let isSubquestion = response.survey.survey_questions.some(q => q.parent == question.id);
            if (isSubquestion) {
              let subQuestionElement = response.survey.survey_questions.filter(q => q.parent == question.id);
              let subQuestionElementId = "id_" + subQuestionElement[0].id;
              initialValues[subQuestionElementId] = question.survey_response[0].answer.comment;
            }
          }
        }

        props.initialize({
          ...initialValues,
        });

        let isUserFocalPoint = props.location && props.location.query && props.location.query && props.location.query.edit == "true";
        let isFinalized = response.status != "done";
        setIsEditable(isUserFocalPoint && isFinalized)
      }

    }).catch(error => {
      props.postError(error, "Survey details loading failed !");
    })
    if (props.location && props.location.query && props.location.query.lang) {
      setLang(props.location.query.lang)
    }

  }, [])

  const finalize = values => {
    setFormValues(values);
    setOpenFinalDialog(true);
  }

  const onFinalize = () => {
    setOpenFinalDialog(false);
    saveResults(formValues, "finalize")
  }

  const save = values => {
    saveResults(values, "save")
  }

  const saveResults = (values, action) => {
    setErrors([]);
    let findErrors = [];

    let responses = [];

    surveyDetails.survey.survey_questions.map(question => {
      if (question.parent == null) {
        let elementId = "id_" + question.id;
        let value = values[elementId];
        if (question.required && question.id && question.id != "" && (!value || value == null || value.trim() == "")) {
          findErrors.push(elementId);
        } else if (value && value.trim() != "") {

          let responseQuestion = question.id;
          let answer = value;
          let comment = null;

          let isSubquestion = surveyDetails.survey.survey_questions.some(q => q.parent == question.id);
          if (isSubquestion) {
            let subQuestionElement = surveyDetails.survey.survey_questions.filter(q => q.parent == question.id);
            let subQuestionElementId = "id_" + subQuestionElement[0].id;
            let subQuestionValue = values[subQuestionElementId];
            if (subQuestionValue && subQuestionValue.trim() != "") {
              comment = subQuestionValue;
            }
          }

          responses.push({ question: responseQuestion, answer: { answer, comment } })

        }
      }
    })

    if (findErrors && findErrors.length > 0) {
      setErrors([...findErrors])
      return;
    }

    let body = {
      "action": action,
      responses,
    }

    const surveyId = props.location && props.location.query && props.location.query.id;

    props.postSurveyResponse(surveyId, body).then(response => {
      if (action == "save" || action == "update") {
        props.successReducer("Survey response saved !");
      }
      if (action == "finalize") {
        setIsEditable(false)
        props.successReducer("Survey response finalized !")
      }
    }).catch(error => {
      action == "save" || action == "update" ? props.postError(error, "Survey save failed !") : props.postError(error, "Survey finalized failed !");
    });
  }

  const checkForRequiredField = id => {
    if (errors && errors.length > 0) {
      if (errors.includes("id_" + id)) {
        return true;
      }
    }
    return false;
  }

  const closeFinalizeDialog = () => {
    setOpenFinalDialog(false);
  }


  return (
    <ListLoader loading={loading}>
      {
        props.location && props.location.query && !loading
          && ((props.location.query.id.trim() == 'null')
            || (!surveyDetails))
          ?
          <React.Fragment>
            <div className={classes.backTick} style={{ marginLeft: '45%', marginTop: '5%' }}>
              <ArrowBackIosIcon onClick={backHandler} />
              <Typography variant="subtitle2" color="secondary" alignItems="center">
                No data found
              </Typography>
            </div>
          </React.Fragment>
          :
          <React.Fragment>

            <Paper className={classes.header} variant="outlined">
              <div className={classes.headerItems}>
                <div className={classes.backTick}>
                  <ArrowBackIosIcon onClick={backHandler} />
                  <label className={classes.labelColor}>
                    Project Agreement Title - KPI {props.location && props.location.query && props.location.query.version}: {surveyDetails && surveyDetails.survey && surveyDetails.survey.title[lang]}
                  </label>
                </div>

                {isEditable ?
                  <div className={classes.btnGrpAlign}>
                    <div>
                      <CustomizedButton
                        buttonType={buttonType.submit}
                        buttonText={"Save"}
                        clicked={handleSubmit(save)}
                      />
                      <CustomizedButton
                        buttonType={buttonType.submit}
                        buttonText={"Finalize"}
                        clicked={handleSubmit(finalize)}
                      />
                    </div>
                  </div>
                  :
                  <div className={classes.btnGrpAlign}>
                    <Typography variant="subtitle2" color="secondary">Content is read only</Typography>
                  </div>
                }
              </div>
            </Paper>
            {errors && errors.length > 0 &&
              <Paper className={classes.bodyHeader} variant="outlined">
                <Typography variant="subtitle2" color="secondary">Please submit all required questions.</Typography>
              </Paper>
            }
            <Paper className={classes.bodyHeader}
              variant="outlined"
              style={{ backgroundColor: isEditable ? "" : '#D3D3D3' }}
            >
              {surveyDetails && surveyDetails.survey
                && surveyDetails.survey.survey_questions
                && surveyDetails.survey.survey_questions.length > 0
                &&
                <form>
                  {surveyDetails.survey.survey_questions.map((q, questionNumber) => {

                    if (q.parent == null) {
                      let isSubquestion = surveyDetails.survey.survey_questions.some(item => item.parent == q.id)

                      let subQuestion = null;
                      if (isSubquestion) {
                        let subQuestionItem = surveyDetails.survey.survey_questions.filter(item => item.parent == q.id)
                        subQuestion = subQuestionItem[0];
                      }

                      let questionData = {
                        ...q,
                        questionNumber: questionNumber + 1,
                      };
                      return (
                        <div key={questionNumber + 1} style={{ backgroundColor: checkForRequiredField(q.id) ? '#ffebeb' : '' }}>
                          {checkForRequiredField(q.id) &&
                            <Typography variant="subtitle2" color="secondary">Required</Typography>
                          }
                          <SurveyTemplate
                            questionData={questionData}
                            lang={lang}
                            subQuestion={subQuestion}
                            edit={isEditable}
                          />
                        </div>
                      );
                    }

                  })
                  }
                </form>
              }

              <Divider />
              <Typography variant="subtitle2" color="primary">Note:</Typography>
              <Typography variant="body2">For each question, please indicate what best reflects your rating for each assessment criteria.</Typography>
              <br />
              <Typography variant="subtitle2" color="primary">Rating:</Typography>
              <Typography variant="body2">Satisfactory (completely fulfilled)</Typography>
              <Typography variant="body2">Partially Satisfactory (needs improvement)</Typography>
              <Typography variant="body2">Satisfactory (not fulfilled)</Typography>
            </Paper>

            <FinalizeDialog open={openFinalDialog} onClose={closeFinalizeDialog} onFinalize={onFinalize} />
          </React.Fragment>
      }
    </ListLoader>
  );
};

const SurveyForm = reduxForm({
  form: "surveyForm",
})(Survey);

const mapStateToProps = (state) => {
  return {
    session: state.session,
    loading: state.Kpi3Reducer.loading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getSurveyDetails: (surveyId) =>
    dispatch(getSurveyDetails(surveyId)),
  postSurveyResponse: (id, body) =>
    dispatch(postSurveyResponse(id, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'Kpi3-Error', message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SurveyForm);
