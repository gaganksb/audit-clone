import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues, } from "redux-form";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { browserHistory as history } from "react-router";
import { showSuccessSnackbar } from "../../../../reducers/successReducer";
import { errorToBeAdded } from "../../../../reducers/errorReducer";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';

import CustomizedButton from "../../../common/customizedButton";
import { buttonType, MemberTeamType } from "../../../../helpers/constants";
import ListLoader from "../../../common/listLoader";
import { renderTextField } from "../../../common/renderFields";
import FreeSoloCreateOption from './FreeTextAutocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import CopyProfilesDialog from './CopyProfilesDialog';

import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'

import {
    getTeamCompositionReport,
    getPPAbyMemberIdApi,
    updateMemberProfileApi,
    copyProfilesApi,
} from "../../../../reducers/qualityAssurance/AuditFirmCountryList";

const useStyles = makeStyles(theme => ({
    paperPadding: {
        padding: theme.spacing(3),
        margin: theme.spacing(3),
    },
    backTick: {
        display: "flex",
        alignItems: "center",
        cursor: "pointer",
    },
    inputTextArea: {
        width: "100%",
        height: "100%",
        resize: 'vertical',
    },
    btnGrpAlign: {
        display: "flex",
        marginLeft: "auto",
        justifyContent: "space-between",
    },
    headerItems: {
        display: "flex",
        alignItems: "center",
    },
    flexRow: {
        display: 'flex',
        flexDirection: 'column',
    }

}))

const messages = {
    error: "Error occurred",
    success: {
        saved: "Saved successfully !",
        finalized: "Finalized successfully !",
    },
};


const MemberProfileInfo = (props) => {
    const classes = useStyles();

    const [teamType, setTeamType] = useState({ code: 'Field Work', value: 'field_work' });

    const [tasks, setTasks] = useState([]);
    const [currentEditedTask, setCurrentEditedTask] = useState("");

    const [langs, setLangs] = useState([]);
    const [currentLang, setCurrentLang] = useState("");

    const [clients, setClients] = useState([]);
    const [currentClient, setCurrentClient] = useState("");

    const [user, setUser] = useState({});

    const [openCopyDialog, setOpenCopyDialog] = useState(false);

    const [copyProfileContent, setCopyProfileContent] = useState({});
    const [ppa, setPpa] = useState([]);

    useEffect(() => {

        let team_composition_id = props.location.query.team;
        let userId = props.location.query.user;

        if (team_composition_id) {
            props.getTeamComposition(team_composition_id).then(response => {
                let users = response.team_composition_users;
                if (users) {
                    let matchedUser = users.filter(user => user.agreement_member.id == userId);
                    let user = matchedUser[0]
                    setUser(user);

                    setTasks(user.agreement_member.tasks_completed);
                    setLangs(user.agreement_member.language)
                    setClients(user.agreement_member.clients_audited_example)

                    let team_type_code = getTeamTypeCodeByValue(user.agreement_member.team_type);
                    let team_type = {
                        code: team_type_code,
                        value: user.agreement_member.team_type
                    }
                    setTeamType(team_type)
                    props.initialize({
                        profile_summary: user.agreement_member.profile_summary,
                        qualifications: user.agreement_member.qualifications,
                        ISA_experience: user.agreement_member.ISA_experience,
                        comments: user.agreement_member.comments,
                    })
                }
            }).catch(error => {
                props.postError(error, "Data not found");
            })
        }
    }, [])

    const backHandler = () => {
        history.goBack();
    }

    const cancelCopyDialog = () => {
        setOpenCopyDialog(false);
    }

    const onCopyOk = (ppa) => {
        setOpenCopyDialog(false);
        if (ppa) {
            let selected_ppas = ppa.filter(p => p.checked == true);
            selected_ppas = selected_ppas.map(p => p.name);

            let body = {
                ppa: selected_ppas,
                agreement_member: user.agreement_member.id,
                data: {
                    ...copyProfileContent,
                }
            }
            props.copyProfile(body).then(response => {
                props.successReducer("Profile copied");
            }).catch(error => {
                props.postError(error, messages.error);
            })
            setCopyProfileContent({})
            setPpa([])
        }
    }

    const save = (values) => {
        let body = {
            "team_type": teamType.value,
            "role": user.agreement_member.role,
            "duration_spent_in_days": user.agreement_member.duration_spent_in_days,
            "tasks_completed": tasks,
            "profile_summary": values.profile_summary,
            "qualifications": values.qualifications,
            "years_of_experience": user.agreement_member.years_of_experience,
            "language": langs,
            "clients_audited_example": clients,
            "ISA_experience": values.ISA_experience,
            "comments": values.comments,
        }

        let agreement_member_id = user.agreement_member.id;
        if (agreement_member_id) {
            props.updateMemberProfile(agreement_member_id, body)
                .then(response => {
                    props.successReducer(messages.success.saved);
                })
                .catch(error => {
                    props.postError(error, messages.error);
                })
        } else {
            props.postError(error, "Error in save");
        }

    }

    const onFinalize = (values) => {

        if (!teamType || !teamType.value || teamType.value == "") {
            let error = {
                team_type: "required",
            };
            props.postError(error, "Team type is required");
            return;
        }

        if (!user.agreement_member.role || user.agreement_member.role == "") {
            let error = {
                role: "required",
            };
            props.postError(error, "Role is required");
            return;
        }

        let body = {
            "team_type": teamType.value,
            "role": user.agreement_member.role,
            "duration_spent_in_days": user.agreement_member.duration_spent_in_days,
            "tasks_completed": tasks,
            "profile_summary": values.profile_summary,
            "qualifications": values.qualifications,
            "years_of_experience": user.agreement_member.years_of_experience,
            "language": langs,
            "clients_audited_example": clients,
            "ISA_experience": values.ISA_experience,
            "comments": values.comments,
            "finalized": true,
        }

        let agreement_member_id = user.agreement_member.id;
        if (agreement_member_id) {
            props.updateMemberProfile(agreement_member_id, body)
                .then(response => {
                    props.successReducer(messages.success.finalized);

                    let year = props.location.query.year;
                    if (year) {
                        props.getPPA(agreement_member_id, parseInt(year))
                            .then(response => {
                                if (response && response.length > 0) {
                                    setPpa(response);
                                    setOpenCopyDialog(true);
                                    setCopyProfileContent(body);
                                }

                            }).catch(error => {
                                props.postError(error, messages.error);
                            })

                    }

                })
                .catch(error => {
                    props.postError(error, messages.error);
                })
        } else {
            props.postError(error, "Error in finalized");
        }

    }

    const handleInputChange = (e) => { }

    const getTeamList = () => {
        return MemberTeamType;
    }


    const getTeamTypeCodeByValue = value => {
        let teamlList = getTeamList();
        let match = teamlList.filter(t => t.value == value);
        if (match && match.length == 1) {
            return match[0].code;
        }
    }

    const getTeamTypeByCode = code => {
        let teamlList = getTeamList();
        let match = teamlList.filter(t => t.code == code);
        if (match && match.length == 1) {
            return match[0];
        }
    }


    const handleTaskItemDelete = (taskItem) => {
        let filteredItems = tasks.filter(item => item != taskItem);
        setTasks(filteredItems);
    }

    const chipTasks = tasks.map((data, index) => {

        return (
            <Chip key={index}
                label={data}
                onDelete={() => handleTaskItemDelete(data)}
                variant="outlined"
            />
        );
    })

    //lang

    const handleLangItemDelete = (langItem) => {
        let filteredItems = langs.filter(item => item != langItem);
        setLangs(filteredItems);
    }

    const chipLangs = langs.map((data, index) => {
        return (
            <Chip key={index}
                label={data}
                onDelete={() => handleLangItemDelete(data)}
                variant="outlined"
            />
        );
    })
    //

    //clients
    const handleClientItemDelete = (clientItem) => {
        let filteredItems = clients.filter(item => item != clientItem);
        setClients(filteredItems);
    }

    const chipClients = clients.map((data, index) => {
        return (
            <Chip key={index}
                label={data}
                onDelete={() => handleClientItemDelete(data)}
                variant="outlined"
            />
        );
    })
    //

    //solo text additions
    const addFreeTask = (newTask) => {
        if (newTask && newTask.trim() != "" && !tasks.includes(newTask)) {
            setTasks(old => [...old, newTask])
            setCurrentEditedTask("")
        }
    }

    const addFreeLang = (newLang) => {
        if (newLang && newLang.trim() != "" && !langs.includes(newLang)) {
            setLangs(old => [...old, newLang])
            setCurrentLang("")
        }
    }

    const addFreeClient = (newClient) => {
        if (newClient && newClient.trim() != "" && !clients.includes(newClient)) {
            setClients(old => [...old, newClient])
            setCurrentClient("")
        }
    }
    //

    const { handleSubmit, loading } = props;

    const changeRole = e => {
        let newRole = e.target.value;
        let copyUser = { ...user };
        copyUser.agreement_member.role = newRole;
        setUser(copyUser);
    }

    const changeDays = e => {
        let newDays = e.target.value;
        let copyUser = { ...user };
        copyUser.agreement_member.duration_spent_in_days = newDays;
        setUser(copyUser);
    }


    const changeTeamTypeByValue = (e) => {
        let value = e.target.value;
        if (value && value != "") {
            let code = getTeamTypeCodeByValue(value);

            let type = {
                value: value,
                code: code
            }

            let copyUser = { ...user };
            copyUser.agreement_member.team_type = type.value;
            setUser(copyUser);

            let team_type = {
                code: type.code,
                value: type.value
            }
            setTeamType(team_type)
        }


    }


    const onExpYears = e => {
        let newExpYears = e.target.value;
        let copyUser = { ...user };
        copyUser.agreement_member.years_of_experience = newExpYears;
        setUser(copyUser);
    }

    const renderFromHelper = ({ touched, error }) => {
        if (!(touched && error)) {
            return
        } else {
            return <FormHelperText>{touched && error}</FormHelperText>
        }
    }



    const renderSelectField = ({
        input,
        label,
        meta: { touched, error },
        children,
        ...custom
    }) => (
        <FormControl error={touched && error} style={{ width: '100%' }}>
            <InputLabel htmlFor="team-type-simple">Field/HQ Work Team</InputLabel>
            <Select
                native
                {...input}
                {...custom}
                inputProps={{
                    name: 'team_type',
                    id: 'team-type-simple',
                    value: `${teamType.value}`
                }}
                onChange={changeTeamTypeByValue}
            >
                {children}
            </Select>
            {renderFromHelper({ touched, error })}
        </FormControl >
    )


    return (<ListLoader loading={loading}>
        <Paper variant="outlined" className={classes.paperPadding}>
            <div className={classes.headerItems}>
                <div className={classes.backTick}>
                    <ArrowBackIosIcon onClick={backHandler} />
                    <Typography variant="subtitle1">Project Agreement Title - KPI 4: Audit Team Composition</Typography>

                </div>

                <div className={classes.btnGrpAlign}>
                    <div>
                        <CustomizedButton
                            buttonType={buttonType.submit}
                            buttonText={"Save"}
                            clicked={handleSubmit(save)}
                        />
                        <CustomizedButton
                            buttonType={buttonType.submit}
                            buttonText={"Finalize"}
                            clicked={handleSubmit(onFinalize)}
                        />
                    </div>
                </div>
            </div>
        </Paper>

        <Paper variant="outlined" className={classes.paperPadding}>
            <form onSubmit={handleSubmit}>
                <Grid container spacing={4} >
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={12} container>
                            <Grid item xs={2} />
                            <Grid item xs={8}>
                                <Typography variant="subtitle2" style={{ color: "rgba(0, 0, 0, 0.54)" }}><b>Member Name: {user && user.agreement_member && user.agreement_member.user.fullname}</b></Typography>
                            </Grid>
                        </Grid>
                        <Grid item xs={12} />
                        <Grid item xs={2}></Grid>
                        <Grid item xs={4}>
                            <Field
                                classes={classes}
                                name="team_type"
                                component={renderSelectField}
                                label="Field/HQ Work Team"
                            >
                                <option value="" />
                                <option value={'field_work'}>Field Work</option>
                                <option value={'other'}>Other</option>
                            </Field>
                        </Grid>
                        <Grid item xs={4}>
                            <Field name="role" component={renderTextField}
                                onChange={changeRole}
                                label="Role"
                                InputProps={{
                                    inputProps: {
                                        readOnly: false,
                                        value: user && user.agreement_member && user.agreement_member.role,
                                    },
                                }}>
                            </Field>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid xs={12} item />

                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={4}>
                            <Field name="duration_spent_in_days" component={renderTextField}
                                type="number"
                                onChange={changeDays}
                                label="Duration Spent in Days"
                                InputProps={{
                                    inputProps: {
                                        min: 0,
                                        value: user && user.agreement_member && user.agreement_member.duration_spent_in_days,
                                    },
                                }}>
                            </Field>
                        </Grid>

                        <Grid item xs={4}>
                            <Field name="years_of_experience" component={renderTextField}
                                onChange={onExpYears}
                                type="number"
                                label="Years of Experience"
                                InputProps={{
                                    inputProps: {
                                        min: 0,
                                        value: user && user.agreement_member && user.agreement_member.years_of_experience,
                                    },
                                }}>
                            </Field>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid xs={12} item />

                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8} className={classes.flexRow}>
                            <div>{chipTasks}</div>
                            <FreeSoloCreateOption id={'task'}
                                onAdd={addFreeTask} current={currentEditedTask} name={"tasks"} label="Task Completed" />
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>


                    <Grid xs={12} item />
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8} className={classes.flexRow}>
                            <div>{chipLangs}</div>
                            <FreeSoloCreateOption id={'langs'}
                                onAdd={addFreeLang} current={currentLang} name={"langs"} label="Language Skills" />
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>


                    <Grid xs={12} item />

                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8} className={classes.flexRow}>
                            <div>{chipClients}</div>
                            <FreeSoloCreateOption id={'client'}
                                onAdd={addFreeClient} current={currentClient} name={"client"} label="Examples of Clients Audited" />
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>




                    <Grid xs={12} item />
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <Typography variant="subtitle2" style={{ color: "rgba(0, 0, 0, 0.54)" }}>Brief Profile Summary</Typography>
                            <div>
                                <Field
                                    name="profile_summary"
                                    component="textarea"
                                    type="text"
                                    rows="7"
                                    className={classes.inputTextArea}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid xs={12} item />
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <Typography variant="subtitle2" style={{ color: "rgba(0, 0, 0, 0.54)" }}>
                                Professional and Academic Qualifications</Typography>
                            <div>
                                <Field
                                    name="qualifications"
                                    component="textarea"
                                    type="text"
                                    rows="7"
                                    className={classes.inputTextArea}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>



                    <Grid xs={12} item />
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <Typography variant="subtitle2" style={{ color: "rgba(0, 0, 0, 0.54)" }}>
                                Experience in ISAs Application</Typography>
                            <div>
                                <Field
                                    name="ISA_experience"
                                    component="textarea"
                                    type="text"
                                    rows="7"
                                    className={classes.inputTextArea}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>

                    <Grid xs={12} item />
                    <Grid item xs={12} container spacing={3}>
                        <Grid item xs={2}></Grid>
                        <Grid item xs={8}>
                            <Typography variant="subtitle2" style={{ color: "rgba(0, 0, 0, 0.54)" }}>Additional Comments</Typography>
                            <div>
                                <Field
                                    name="comments"
                                    component="textarea"
                                    type="text"
                                    rows="7"
                                    className={classes.inputTextArea}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}></Grid>
                    </Grid>

                </Grid>
            </form>
        </Paper>

        <CopyProfilesDialog open={openCopyDialog}
            onClose={cancelCopyDialog} onOk={onCopyOk}
            ppa={ppa} />

    </ListLoader>)
}

const MemberProfileInfoForm = reduxForm({
    form: "MemberProfileInfoForm",
})(MemberProfileInfo);

const mapStateToProps = (state) => {
    return {
        formValues: getFormValues('MemberProfileInfoForm')(state),
        loading: state.AuditFirmCountryList.loading,
    };
};

const mapDispatchToProps = (dispatch) => ({
    updateMemberProfile: (id, body) =>
        dispatch(updateMemberProfileApi(id, body)),

    getTeamComposition: (id) =>
        dispatch(getTeamCompositionReport(id)),

    getPPA: (id, year) =>
        dispatch(getPPAbyMemberIdApi(id, year)),

    copyProfile: (body) =>
        dispatch(copyProfilesApi(body)),

    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'Member-Profile-Error', message)),
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps
)(MemberProfileInfoForm);

export default connected;