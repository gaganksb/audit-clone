import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import MuiAlert from '@material-ui/lab/Alert';
import { connect } from "react-redux";
import { browserHistory as history } from "react-router";
import CenteredTabs from "../../common/fullwidthTabs";
import KPI1 from "./kpi1/Kpi1";
import KPI2 from "./kpi2/Kpi2";
import KPI3 from "./kpi3/Kpi3";
import KPI4 from "./kpi4/Kpi4";
import { checkIfAuditWorkCompletedApi } from "../../../reducers/qualityAssurance/kpi2Reducer";
import ListLoader from "../../common/listLoader";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const useStyles = makeStyles((theme) => ({
  headerRow: {
    backgroundColor: "#F6F9FC",
    "& th": {
      color: "#a0adbc",
      padding: "18px 25px",
    },
  },
  heading: {
    paddingLeft: 24,
    padding: "10px 20px",
    color: "#344563",
    font: "bold 16px/21px Roboto",
    display: "flex",
    justifyContent: "space-between",
  },
  tabs: {
    paddingLeft: 12,
    paddingRight: 12,
  },
  tabsContent: {
    paddingLeft: 24,
    paddingRight: 24,
  },
}));

const tabs = {
  items: [
    { key: "kpi1", label: "KPI1 - Timelines of Audit" },
    { key: "kpi2", label: "KPI2 - Audit Quality" },
    { key: "kpi3", label: "KPI3 - Client Surveys" },
    { key: "kpi4", label: "KPI4 - Team Composition" },
  ],
};

const year = new Date().getFullYear();

const QualityAssurance = (props) => {
  const [value, setValue] = useState(0);
  const [isAuditCompleted, setIsAuditCompleted] = useState(false);

  useEffect(() => {
    props.checkIfAuditWorkCompletedApi(year).then(response => {
      if (response) {
        setIsAuditCompleted(response.audit_work_completed);
      } else {
        setIsAuditCompleted(false);
      }
    }).catch(error => {
      setIsAuditCompleted(false);
    });
  }, [])

  useEffect(() => {
    let tab = props.location.query.tab;
    if (tab) {
      setValue(parseInt(tab));
    }
  }, []);

  function handleTabChange(event, value) {
    setValue(value);
    history.push(`/quality-assurance?tab=${value}`);
  }

  const classes = useStyles();

  return (
    <ListLoader loading={props.loading}>
      <div className={classes.heading}>
        <h4>Quality Assurance</h4>
      </div>
      {isAuditCompleted ?
        <Alert severity="warning">Please complete audit work before starting quality assurance !</Alert>
        :
        <React.Fragment>
          <div className={classes.tabs}>
            <CenteredTabs
              tabs={tabs}
              handleChange={handleTabChange}
              value={value}
            />
          </div>
          <div className={classes.tabsContent}>
            {value === 0 && <KPI1 />}
            {value === 1 && <KPI2 />}
            {value === 2 && <KPI3 />}
            {value === 3 && <KPI4 />}
          </div>
        </React.Fragment>
      }
    </ListLoader>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.Kpi2Reducer.loading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  checkIfAuditWorkCompletedApi: (params) =>
    dispatch(checkIfAuditWorkCompletedApi(params)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(QualityAssurance);

export default connected;
