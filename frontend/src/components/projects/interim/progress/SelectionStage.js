import React, { Fragment } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Forward from './Forward'
import Reject from './Reject'
import { USERS_TYPES, CYCLE_STATUS } from '../../../../helpers/constants'

const messages = {
}

const useStyles = makeStyles(theme => ({
}))

const SelectionStage = (props) => {
  const { cycle, membership } = props
  const classes = useStyles()

  if (cycle && cycle.status && [CYCLE_STATUS.PRE01.id, CYCLE_STATUS.PRE02.id, CYCLE_STATUS.PRE03.id, CYCLE_STATUS.FIN01.id, CYCLE_STATUS.FIN02.id].includes(cycle.status.id)) {
    return null
  } 
  else if (cycle && cycle.status && [CYCLE_STATUS.INT01.id, CYCLE_STATUS.INT02.id].includes(cycle.status.id)) {
   {
    return (
      <Fragment>
        <Forward />
        {([CYCLE_STATUS.INT02.id].includes(cycle.status.id)) && <Reject />}
      </Fragment>
    )}
  } else {
    return null
  }
}

SelectionStage.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  membership: state.userData.data.membership
})

const mapDispatch = dispatch => ({
})

export default withRouter(connect(mapStateToProps, mapDispatch)(SelectionStage))
