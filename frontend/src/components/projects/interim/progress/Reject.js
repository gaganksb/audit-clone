import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../../common/customizedButton';
import {buttonType} from '../../../../helpers/constants'
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { CYCLE_STATUS, MEMBERSHIPS, BODY_PARAMS, LIST_TYPE  } from '../../../../helpers/constants'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import { loadCycleFO } from '../../../../reducers/projects/projectsReducer/getCycleFO'
import AddCommentDialog from '../../common/AddCommentDialog'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { reset } from 'redux-form'
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  button: 'REJECT',
  required: 'Comment is Required',
  title: 'Reject Interim List',
  success: 'Interim List Successfully Reverted !',
  description: 'Please, provide a comment before sending the Preliminary List back to the previous stage',
  error: 'Error'
}

const useStyles = makeStyles(theme => ({
}))

const Reject = (props) => {
  const {
    cycleFO,
    cycle,
    postError,
    loadCycle,
    modifyProject,
    loadActivityList,
    loadCycleFO,
    membership,
    reset,
    successReducer
  } = props

  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const isFOApprover = membership && membership.role === 'APPROVER' && membership.type === 'FO'
  const isFOReviewer = membership && membership.role === 'REVIEWER' && membership.type === 'FO'

  useEffect(() => {
      if (
        ([CYCLE_STATUS.INT01.code].includes(cycle.status.code) && isFOReviewer) ||
        ([CYCLE_STATUS.INT02.code].includes(cycle.status.code) && isFOApprover)
      ) {
        setShow(true)
      } else {
        setShow(false)
      }
  }, [cycle])

  const handleOpen = () => {
    setDisabled(false)
    setOpen(true)
  }

  const handleClose = () => {
    reset()
    setOpen(false)
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleReject = ({ comment }) => {
    if(!comment) {
      postError(messages.error, messages.required)
      return
    }
    setDisabled(true)
    if (cycle && cycle.status && (cycle.status.code === CYCLE_STATUS.INT02.code || cycle.status.code === CYCLE_STATUS.INT01.code)) {
      let body = {
        status: cycle.status.id - 1,
        list_type: LIST_TYPE.interim,
        comment,
        request_type: "reject"
      }
    modifyProject(cycle.year, body)
    .then(() => {      
      loadCycle(cycle.year)
      loadActivityList(cycle.year, { page: 1 })
      successReducer(messages.success)
    })
    .catch((error) => {
      postError(error, messages.error) })
    .finally(() => { handleClose() })
}
    handleClose()
  }

  return (
    <Fragment>
      {show &&
        <CustomizedButton 
        clicked={handleOpen}
        buttonType={buttonType.cancel} 
        buttonText={messages.button}  /> }
      <AddCommentDialog
        title={messages.title}
        description={messages.description}
        onSubmit={handleReject}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Reject.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  cycleFO: state.cycleFO.details,
  membership: state.userData.data.membership,
  query: ownProps.location.query
})

const mapDispatch = dispatch => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'reject', message)),
  loadCycleFO: (year, foId) => dispatch(loadCycleFO(year, foId)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Reject))
