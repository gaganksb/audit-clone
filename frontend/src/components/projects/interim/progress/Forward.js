import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../../common/customizedButton';
import {buttonType} from '../../../../helpers/constants'
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { editCycle } from '../../../../reducers/projects/projectsReducer/editCycle'
import { loadCycleFO } from '../../../../reducers/projects/projectsReducer/getCycleFO'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { initializeFinalFO } from '../../../../reducers/projects/projectsReducer/initializeInterimList'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { CYCLE_STATUS, MEMBERSHIPS, BODY_PARAMS, LIST_TYPE  } from '../../../../helpers/constants'
import AddCommentDialog from '../../common/AddCommentDialog'
import { reset } from 'redux-form'
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  error: 'Error',
  required: 'Comment is Required',
  success: 'Interim List is Successfully Approved !',
  button: {
    INT01: 'SEND FOR APPROVAL',
    INT02: 'FINALIZE',
    LOAD: 'LOADING...',
  },
  modal: {
    INT01: {
      title: 'Send Interim List for Approval',
      description: 'Please, provide a comment before sending the Interim List to the next stage'
    },
    INT02: {
      title: 'Finalize the Interim List',
      description: 'Please, provide a comment before finalising the Interim List'
    }
  }
}

const useStyles = makeStyles(theme => ({
}))

const Forward = (props) => {
  const {
    cycleFO,
    cycle,
    initializeFinalFO,
    loadCycleFO,
    loadCycle,
    loading,
    membership,
    postError,
    modifyProject,
    reset,
    loadActivityList,
    successReducer
  } = props

  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const [text, setText] = useState({
    actionTitle: '',
    modalTitle: '',
    modalDesc: ''
  })
  const isFOReviewer = membership && membership.role === 'REVIEWER' && membership.type === 'FO'
  const isFOApprover = membership && membership.role === 'APPROVER' && membership.type === 'FO'

  // useEffect(() => {
  //   setTitles()
  //   if (cycleFO && cycleFO.cycle) {
  //     if (
  //       ([CYCLE_STATUS.INT01.code].includes(cycleFO.cycle.status.code) && isFOReviewer) ||
  //       ([CYCLE_STATUS.INT02.code].includes(cycleFO.cycle.status.code) && isFOApprover)
  //     ) {
  //       setShow(true)
  //     } else {
  //       setShow(false)
  //     }
  //   }
  // }, [cycleFO])

  useEffect(() => {
    setTitles()
      if (
        ([CYCLE_STATUS.INT01.code].includes(cycle.status.code) && isFOReviewer) ||
        ([CYCLE_STATUS.INT02.code].includes(cycle.status.code) && isFOApprover)
      ) {
        setShow(true)
      } else {
        setShow(false)
      }
  }, [cycle])

  const handleOpen = () => {
    setDisabled(false)
    setOpen(true)
  }

  const handleClose = () => {
    reset()
    setOpen(false)
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleForward = ({ comment }) => {
    if(!comment) {
      postError(messages.error, messages.required)
      return
    }
    setDisabled(true)
      if (cycle.status.code == CYCLE_STATUS.INT01.code || cycle.status.code == CYCLE_STATUS.INT02.code) {
        let body = {
          status: cycle.status.id + 1,
          list_type: LIST_TYPE.interim,
          comment,
          request_type: "promote"
        }
        modifyProject(cycle.year, body)
          .then(() => {
            loadCycle(cycle.year)
            loadActivityList(cycle.year, { page: 1 })
            successReducer(messages.success)
          })
          .catch((error) => {
            postError(error, messages.error) })
          .finally(() => { handleClose() })
      }
    handleClose()
  }

  // const setTitles = () => {
  //   if (loading) {
  //     setText({
  //       ...text,
  //       actionTitle: messages.button.LOAD
  //     })
  //   }
  //   if (cycle && cycleFO.cycle) {
  //     if (cycleFO.cycle.status_code == CYCLE_STATUS.INT01.code) {
  //       setText({
  //         actionTitle: messages.button.INT01,
  //         modalTitle: messages.modal.INT01.title,
  //         modalDesc: messages.modal.INT01.description
  //       })
  //     } else if (cycleFO.cycle.status_code == CYCLE_STATUS.INT02.code) {
  //       setText({
  //         actionTitle: messages.button.INT02,
  //         modalTitle: messages.modal.INT02.title,
  //         modalDesc: messages.modal.INT02.description
  //       })
  //     }
  //   }
  // }

  const setTitles = () => {
    if (loading) {
      setText({
        ...text,
        actionTitle: messages.button.LOAD
      })
    }
      if (cycle.status.code == CYCLE_STATUS.INT01.code) {
        setText({
          actionTitle: messages.button.INT01,
          modalTitle: messages.modal.INT01.title,
          modalDesc: messages.modal.INT01.description
        })
      } else if (cycle.status.code == CYCLE_STATUS.INT02.code) {
        setText({
          actionTitle: messages.button.INT02,
          modalTitle: messages.modal.INT02.title,
          modalDesc: messages.modal.INT02.description
        })
      }
  }

  return (
    <Fragment>
      {show &&
       <CustomizedButton 
       clicked={handleOpen}
       buttonType={buttonType.submit} 
       buttonText={text.actionTitle} 
       disabled={loading} />
      }
      <AddCommentDialog
        title={text.modalTitle}
        description={text.modalDesc}
        onSubmit={handleForward}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Forward.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  cycleFO: state.cycleFO.details,
  membership: state.userData.data.membership,
  loading: state.cycleFO.loading
})

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  loadCycleFO: (year, foId) => dispatch(loadCycleFO(year, foId)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  initializeFinalFO: (year, foId) => dispatch(initializeFinalFO(year, foId)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'forward', message)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),  
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Forward))
