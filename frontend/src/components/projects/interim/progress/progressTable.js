import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory as history } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import { Table, TableHead, TableRow, TableCell, TableBody, TableFooter, TablePagination } from '@material-ui/core'
import { Paper, Typography } from '@material-ui/core';
import CustomizedButton from '../../../common/customizedButton'
import { buttonType } from '../../../../helpers/constants';
import { showSuccessSnackbar } from '../../../../reducers/successReducer';
import Grid from '@material-ui/core/Grid'
import classNames from 'classnames';
import { sendFOReminder } from '../../../../reducers/projects/projectsReducer/remindFO';
import { errorToBeAdded } from '../../../../reducers/errorReducer';
import TablePaginationActions from '../../../common/secondTableAction';
import ListLoader from '../../../common/listLoader';
import RemindAll from '../../../assessment/remindAllDialog';


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
    },
    table: {
        minWidth: 500,
    },
    tableRow: {
        borderStyle: 'hidden',
    },
    tableHeader: {
        color: '#344563',
        fontFamily: 'Roboto',
        fontSize: 14,
    },
    headerRow: {
        backgroundColor: '#F6F9FC',
        border: '1px solid #E9ECEF',
        height: 60,
        font: 'medium 12px/16px Roboto'
    },
    tableCell: {
        font: '14px/16px Roboto',
        letterSpacing: 0,
        color: '#344563'
    },
    tableCellRed: {
        backgroundColor: "#F5DADA"
    },
    heading: {
        padding: theme.spacing(3),
        borderBottom: '1px solid rgba(224, 224, 224, 1)',
        color: '#344563',
        font: 'bold 16px/21px Roboto',
        letterSpacing: 0,
    },
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8,
        backgroundColor: '#0072BC'
    },
    expandIconWrapper: {
        cursor: 'pointer',
        display: 'inline-flex'
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    expandHeading: {
        fill: "#a7b1be",
        fontSize: "17px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    expandText: {
        fill: "black",
        color: "black",
        fontSize: "14px",
        fontFamily: "Roboto-Regular, Roboto"
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54,
        width: '100%'
    },
    commentBox: {
        width: "100%"
    }
}))

const messages = {
    tableHeader: ["Field Office", "Progress", "Completed", ""],
    remind: "Remind",
    subject: "Remind to complete your Field Assessments",
    success: {
        reminder: "Reminder Sent !"
    },
    title: "Project Selection Progress",
    dialogTitle: "Send Reminder"
}
const rowsPerPage = 10

const FOProgress = (props) => {

    const {
        items,
        loading,
        totalItems,
        page,
        handleChangePage,
        fieldList,
        loadFieldList,
        year,
        sendFOReminder,
        successReducer,
        postRemindError
    } = props

    const classes = useStyles()
    const [showDialogueBox, setDialogueBox] = useState(false)
    const [selectedOffice, setSelectedOffice] = useState(null)
    const [expand, setExpand] = React.useState(false)
    const [activeItem, setActiveItem] = useState(-1)
    const [searchItems, setSearchItems] = useState([])
    const [selectedUsers, setSelectedUsers] = useState([])
    const [errorMsg, updateErrorMsg] = useState(null)

    const dialogText = (selectedOffice) ? "Confirm that you want to remind " + `${selectedOffice.business_unit}` + " to complete the assessement by clicking on <b>YES</b>" :
        "Confirm that you want to remind selected FO to complete the assessement by clicking on <b>YES</b>"
    useEffect(() => {
        setSearchItems(fieldList)
    }, [fieldList])

    const handleDialogueBox = (args) => {
        setDialogueBox(args)
    }

    const sendReminder = () => {
        const body = {
            year: year,
            field_office: selectedOffice.id
        }
        sendFOReminder(body).then(res => {
            setDialogueBox(false)
            successReducer(messages.success.reminder)
        }).catch(error => {
            postRemindError(error, error.response && error.response.data.detail);
            throw error
        })
    }

    const handleClose = () => {
        updateErrorMsg(null)
        setDialogueBox(false)
    }

    function handleTableCellStyle(item) {
        if (item.total === item.completed) {
            return classes.tableCell
        } else {
            return classNames(classes.tableCell, classes.tableCellRed)
        }
    }

    return (
        <React.Fragment>
            <Paper>
                <ListLoader loading={loading}>
                    <div className={classes.tableWrapper}>
                        <div className={classes.heading}>
                            {messages.title}
                        </div>
                        <Table className={classes.table}>
                            <TableHead className={classes.headerRow}>
                                <TableRow>
                                    {messages.tableHeader.map((item, index) => {
                                        return (
                                            <TableCell key={index} id={index}>
                                                {item}
                                            </TableCell>
                                        )
                                    })}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {items && items.map(item => (
                                    <React.Fragment key={item.id}>
                                        <TableRow>
                                            <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                                                {item.business_unit}
                                            </TableCell>
                                            <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                                                {item.progress}
                                            </TableCell>
                                            <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                                                {["INT01", "INT02", "PRE01", "PRE02", "PRE03"].includes(item.progress) ? "NO" : "YES"}
                                            </TableCell>
                                            <TableCell className={handleTableCellStyle(item)} component="th" scope="row">
                                                {item.progress == "INT01" || item.progress == "INT02" ? <Grid className={classes.buttonGrid}>
                                                    <CustomizedButton buttonType={buttonType.submit} buttonText={messages.remind} clicked={() => { handleDialogueBox(true); setSelectedOffice(item); }} />
                                                </Grid>
                                                    : null}
                                            </TableCell>
                                        </TableRow>
                                    </React.Fragment>
                                )
                                )}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[rowsPerPage]}
                                        colSpan={0}
                                        count={totalItems}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        SelectProps={{
                                            native: true,
                                        }}
                                        onChangePage={handleChangePage}
                                        ActionsComponent={TablePaginationActions}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                        <RemindAll
                            open={showDialogueBox}
                            sendReminder={sendReminder}
                            handleClose={handleClose}
                            dialogText={dialogText}
                            dialogTitle={messages.dialogTitle}
                        />
                    </div>
                </ListLoader>
            </Paper>
        </React.Fragment>
    )
}

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
    postRemindError: (error, message) => dispatch(errorToBeAdded(error, 'remind', message)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    sendFOReminder: (body) => dispatch(sendFOReminder(body))
});

const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(FOProgress);

export default withRouter(connected);
