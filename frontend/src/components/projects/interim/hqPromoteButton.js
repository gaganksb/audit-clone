import React from 'react';
import { connect } from 'react-redux';
import { SubmissionError } from 'redux-form';
import { withRouter } from 'react-router';
import { buttonType } from '../../../helpers/constants';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import CustomizedButton from '../../common/customizedButton';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { modifyProjectHQ } from '../../../reducers/projects/projectsReducer/modifyProjectHQ';
import { loadSelectionProgressListHQ } from '../../../reducers/projects/projectsReducer/getSelectionProgressHQ';
import { loadSelectionProgressList } from '../../../reducers/projects/projectsReducer/getSelectionProgress';
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle'
import AlertDialog from './hqPromoteDialogue';

const messages = {
    finalize: "Promote To Final",
    success: "All listed projects promoted to FIN01",
    error: "Error while promoting projects to final"
}

const HQPromote = (props) => {

    const {
        successReducer, modifyProjectHQ, postError, query,
        loadSelectionProgressHQ, loadCycle, loadSelectionProgressList
    } = props;

    const [open, setOpen] = React.useState(false)

    function handleClose() {
        setOpen(false);
    }

    const sendReminder = () => {
        modifyProjectHQ(query.year, { comment: "Promote To Final by HQ" }).then(res => {
            loadSelectionProgressHQ(query.year, { page: 1 })
            loadSelectionProgressList(query.year)
            successReducer(messages.success)
            loadCycle(query.year)
            setOpen(false)
        }).catch(error => {
            postError(error, messages.error)
        })
    }

    return (
        <React.Fragment>
            <CustomizedButton
                clicked={()=>setOpen(true)}
                buttonType={buttonType.submit}
                buttonText={messages.finalize}
            />
            <AlertDialog
                sendReminder={sendReminder}
                open={open}
                handleClose={handleClose}
            />
        </React.Fragment>
    )
}



const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    membership: state.userData.data.membership,
});

const mapDispatch = dispatch => ({
    addProgress: (body) => dispatch(addFieldAssessmentProgress(body)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'setDeadline', message)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    successReducer: (message) => dispatch(showSuccessSnackbar(message)),
    modifyProjectHQ: (year, body) => dispatch(modifyProjectHQ(year, body)),
    loadSelectionProgressHQ: (year, params) => dispatch(loadSelectionProgressListHQ(year, params)),
    loadCycle: (year, params) => dispatch(loadCycle(year, params)),
    loadSelectionProgressList: (year, params) => dispatch(loadSelectionProgressList(year, params))
});

const connected = connect(mapStateToProps, mapDispatch)(HQPromote);
export default withRouter(connected);