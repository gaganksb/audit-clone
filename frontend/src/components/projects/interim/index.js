import React, { Fragment, useState, useEffect } from 'react'
import { browserHistory as history, withRouter } from 'react-router'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import R from 'ramda'
import SelectionStage from './progress/SelectionStage'
import Title from '../../common/customTitle';
import InterimListTable from './read/InterimListTable'
import ActivityTable from '../common/activityTable/index'
// import ListProgressItems from './read/ListProgressItems'
import { loadInterimList } from '../../../reducers/projects/projectsReducer/getInterimList'
import { loadSettings } from '../../../reducers/projects/projectsReducer/getSettings'
import { loadInterimListInProgress } from '../../../reducers/projects/projectsReducer/getInterimListInProgress'
import { loadUser } from '../../../reducers/users/userData'
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle'
import { loadCycleFO } from '../../../reducers/projects/projectsReducer/getCycleFO'
import { loadLatestCycle } from '../../../reducers/projects/projectsReducer/getLatestCycle'
import { loadActivityList } from '../../../reducers/projects/projectsReducer/getActivityList'
import { exportCSVFile } from '../../../utils/csvGenerator'
import ProjectsFilter from '../common/ProjectsFilter'
import { loadProjectList } from '../../../reducers/projects/projectsReducer/getProjectList';
import { loadSelectionProgressList } from '../../../reducers/projects/projectsReducer/getSelectionProgress';
import ProgressTable from './progress/progressTable';
import HQPromote from './hqPromoteButton';
import { loadSelectionProgressListHQ } from '../../../reducers/projects/projectsReducer/getSelectionProgressHQ';
import ListLoader from '../../common/listLoader';

// const csvHeaders = {
//   reason: "Reason",
//   bu: "Business Unit",
//   agrNum: 'Agreement Number',
//   country: "Country",
//   partner: 'Partner',
//   implNum: 'Implementer Number',
//   budget: 'Budget (in USD)',
//   installmentsPaid: 'Installments Paid',
//   description: 'Description',
//   impType: 'Implementer Type',
//   projStartDate: 'ProjectStart Date',
//   projEndDate: 'ProjectEnd Date',
//   agrType: 'Agreement Type',
//   agrDate: 'Agreement Date',
//   operation: 'Operation',
// }

const messages = {
  table: {
    title: 'Interim List',
    headersFO: [
      {
        title: 'Include/Exclude Project',
        field: 'exclude__agreement',
      },
      {
        title: 'Comments(in manual modification)',
        field: 'comments'
      },
      {
        title: 'Business Unit',
        field: 'business_unit__code',
      },
      {
        title: 'Agreement Number',
        field: 'number',
      },
      {
        title: 'Is Delta',
        field: 'delta',
      },
      {
        title: 'Country',
        field: 'country__name',
      },
      {
        title: 'Partner',
        field: 'partner__legal_name',
      },
      {
        title: 'Implementer Number',
        field: 'partner__number',
      },
      {
        title: 'Budget (in USD)',
        field: 'budget',
      },
      {
        title: 'Installments Paid',
        field: 'installments_paid',
      },
      {
        title: 'Description',
        field: 'description',
      },
      {
        title: 'Implementer Type',
        field: 'partner__implementer_type__implementer_type',
      },
      {
        title: 'ProjectStart Date',
        field: 'start_date',
      },
      {
        title: 'ProjectEnd Date',
        field: 'end_date',
      },
      {
        title: 'Agreement Type',
        field: 'agreement_type',
      },
      {
        title: 'Agreement Date',
        field: 'agreement_date',
      },
      {
        title: 'Operation',
        field: 'operation__code',
      },
    ],
    headers: [
      {
        title: 'Include/Exclude Project',
        field: 'exclude__agreement',
      },
      // {
      //   title: 'Reason',
      //   field: 'int_sense_check__reason',
      // },
      {
        title: 'Comments(in manual modification)',
        field: 'comments'
      },
      {
        title: 'Business Unit',
        field: 'business_unit__code',
      },
      {
        title: 'Agreement Number',
        field: 'number',
      },
      {
        title: 'Is Delta',
        field: 'delta',
      },
      {
        title: 'Country',
        field: 'country__name',
      },
      {
        title: 'Partner',
        field: 'partner__legal_name',
      },
      {
        title: 'Implementer Number',
        field: 'partner__number',
      },
      {
        title: 'Budget (in USD)',
        field: 'budget',
      },
      {
        title: 'Installments Paid',
        field: 'installments_paid',
      },
      {
        title: 'Description',
        field: 'description',
      },
      {
        title: 'Implementer Type',
        field: 'partner__implementer_type__implementer_type',
      },
      {
        title: 'ProjectStart Date',
        field: 'start_date',
      },
      {
        title: 'ProjectEnd Date',
        field: 'end_date',
      },
      {
        title: 'Agreement Type',
        field: 'agreement_type',
      },
      {
        title: 'Agreement Date',
        field: 'agreement_date',
      },
      {
        title: 'Operation',
        field: 'operation__code',
      },
      { title: 'Reason', field: 'pre_sense_check__reason' },
    ],
  },
  info: {
    PRE: 'The selected Interim List is not yet ready for review',
    INT: 'The selected Interim List is under preparation',
    FIN: 'The selected Interim List have been finalized'
  },
  error: 'Error',
  success: {
    reminder: 'Reminder Sent !'
  }
}

const messagesIP = {
  title: 'Interim List Progress',
  headers: [
    {
      title: 'Office Name',
      field: 'name',
    },
    {
      title: 'Focal Person Name',
      field: 'focal_person_name',
    },
    {
      title: 'Focal Person Email',
      field: 'focal_person_email',
    },
    {
      title: 'Focal Person Role',
      field: 'focal_person_role',
    },
    {
      title: 'Alternate Focal Person Name',
      field: 'alternate_focal_person_name',
    },
    {
      title: 'Alternate Focal Person Email',
      field: 'alternate_focal_person_email',
    },
    {
      title: 'Alternate Focal Person Role',
      field: 'alternate_focal_person_role',
    },
    {
      title: 'Interim List Status',
      field: 'interim_list_status',
    }
  ]
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  buttonGrid: {
    margin: '24px 0 24px',
    display: 'flex',
    minWidth: 100,
    marginLeft: -16
  },
  customizedButton: {
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 0,
    marginRight: 16,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  }
}))

let csvItem = [{}]

const InterimView = (props) => {
  const {
    interim,
    interimInProgress,
    totalItems,
    totalItemsInProgress,
    loading,
    loadingLatestCycle,
    loadLatestCycle,
    loadInterimList,
    loadingInProgress,
    loadInterimListInProgress,
    loadUser,
    loadCycle,
    loadCycleFO,
    loadILCSV,
    loadActivityList,
    activities,
    totalActivityCount,
    loadingActivity,
    csv,
    cycle,
    cycleFO,
    loadSettings,
    settings,
    membership,
    query,
    pathName,
    loadSelectionProgressList,
    selectionProgressList,
    totalselectionProgressCount,
    loadSelectionProgressHQ,
    totalFIN01,
    FIN01,
    HQResults
  } = props

  const classes = useStyles()
  const [page, setPage] = useState(0)
  const [sort, setSort] = useState({ field: null, order: 'desc' })
  const [sortInProgress, setSortInProgress] = useState({ field: null, order: 'desc' })
  const [itemsInProgress, setItemsInProgress] = useState([])
  const [year, setYear] = useState(null)
  const [key, setKey] = useState('default')
  const [shouldLatestCycleLoaded, setShouldLatestCycleLoaded] = useState(true)
  const [params, setParams] = useState({ page: 0 });
  const [delta, setDelta] = useState(false)

  const currentYear = new Date().getFullYear()

  const agreement_type = 'interim_list'

  const parseInterimInProgress = () => {
    return interimInProgress && interimInProgress.map(item => ({
      name: item.office.name,
      focal_person_name: item.office.focal_person.name,
      focal_person_email: item.office.focal_person.email,
      focal_person_role: item.office.focal_person.role,
      alternate_focal_person_name: item.office.alternate_focal_person.name,
      alternate_focal_person_email: item.office.alternate_focal_person.email,
      alternate_focal_person_role: item.office.alternate_focal_person.role,
      interim_list_status: item.interim_list_status
    }))
  }

  const isFOUser = () => {
    return membership && Object.keys(membership).includes('field_office')
  }

  useEffect(() => {
    loadUser()
  }, [])

  useEffect(() => {
    loadSelectionProgressList(query.year, { page: params.page + 1 })
  }, [query, params])

  useEffect(() => {
    const { pathName } = props;
    if (query.year) {
      setYear(query.year)
      loadSettings(query)
      loadCycle(query.year)
      membership && membership.type === "FO" && loadActivityList(query.year, { page: 1 })
      membership && membership.type === "HQ" && loadSelectionProgressList(query.year, { page: 1 })
      loadInterimList(query).then(response => {
        membership && membership.type === "HQ" && loadSelectionProgressHQ(query.year, { page: 1 })
      });
    }
    if ("year" in query === false) {
      setYear(currentYear)
      setDelta(false);
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          year: currentYear,
          status: "int"
        })
      })
    }
  }, [query, membership]);

  const parseReasons = (reasons) => {
    let res = 'int__'
    if (reasons && reasons.length > 0) {
      for (let i = 0; i < reasons.length; i++) {
        res = res + reasons[i] + ','
      }
      res = res.slice(0, -1)
    }
    return res
  }

  function resetFilter() {
    const { pathName } = props

    setPage(0)
    setDelta(false)
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        delta: undefined,
      },
    })
  }

  const handleSearch = (values) => {
    const { partner, year, reasons, view_items, business_unit__code, number, } = values

    const ViewItems = 'int__'.concat(view_items)

    setPage(0)
    setYear(year)

    const parsedReasons = parseReasons(reasons)
    if (!reasons) {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          view_items: ViewItems,
          business_unit__code,
          number,
          year,
          status: "int",
          delta: delta ? delta : undefined,
        }
      })
    } else {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          view_items: ViewItems,
          business_unit__code,
          number,
          year,
          reason: reasons,
          status: "int",
          delta: delta ? delta : undefined,
        }
      })
    }
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ["asc", "desc"])[0]
    });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      })
    });
  }

  const handleSortInProgress = (field, order) => {
    setSortInProgress({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
  }

  function handleProgressChangePage(event, newPage) {
    setParams({ page: newPage })
  }

  function onDeltaChange(e) {
    setDelta(e.target.checked)
  }

  return (
    <React.Fragment>
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
        <Title show={false} title={messages.table.title} />

        <div className={classes.root}>
          <ListLoader loading={loading}>
            <ProjectsFilter
              onSubmit={handleSearch}
              resetFilter={resetFilter}
              year={year}
              key={key}
              setKey={setKey}
              membership={membership}
              delta={delta}
              onDeltaChange={onDeltaChange}
            />
          </ListLoader>

          <Grid className={classes.buttonGrid}>
            {totalItems > 0 && <SelectionStage />}
          </Grid>
          {
            membership && membership.type === "HQ" && cycle && cycle.status && (cycle.status.code === "INT01" || cycle.status.code === "INT02") &&
            totalFIN01 > 0 && <div className={classes.buttonGrid} style={{ marginTop: -16 }}> <HQPromote /> </div>
          }

          <InterimListTable
            messages={messages}
            sort={sort}
            items={interim}
            loading={loading}
            totalItems={totalItems}
            page={page}
            handleChangePage={handleChangePage}
            handleSort={handleSort}
            year={year}
            cycleFO={cycleFO}
            settings={settings}
            membership={membership}
          />
          {membership && membership.type === "FO" ? <ActivityTable
            items={activities}
            count={totalActivityCount}
            loading={loadingActivity}
            loadData={loadActivityList}
            year={year}
          /> : <ProgressTable items={selectionProgressList}
            handleChangePage={handleProgressChangePage}
            page={params.page}
            year={year}
            totalItems={totalselectionProgressCount} />}
        </div>
      </div>
    </React.Fragment>
  )
}

InterimView.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  cycleFO: state.cycleFO.details,
  interim: state.projectList.project,
  interimInProgress: state.interimListInProgress.interim,
  totalItems: state.projectList.totalCount,
  totalItemsInProgress: state.interimListInProgress.totalCount,
  loading: state.projectList.loading,
  loadingInProgress: state.interimListInProgress.loading,
  loadingLatestCycle: state.latestCycle.loading,
  latestCycle: state.latestCycle.details,
  membership: state.userData.data.membership,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  settings: state.settings.details,
  // csv: state.interimListCSV.ILCSV,
  activities: state.activityList.activities,
  totalActivityCount: state.activityList.totalCount,
  loadingActivity: state.activityList.loading,
  selectionProgressList: state.selectionProgress.progress,
  totalselectionProgressCount: state.selectionProgress.totalCount,
  totalFIN01: state.selectionProgressReducerHQ.totalCount,
  FIN01: state.selectionProgressReducerHQ.FIN01,
  HQResults: state.selectionProgressReducerHQ.results
})

const mapDispatch = dispatch => ({
  loadInterimList: (params) => dispatch(loadProjectList(params)),
  loadInterimListInProgress: (year, params) => dispatch(loadInterimListInProgress(year, params)),
  loadUser: () => dispatch(loadUser()),
  loadSettings: (params) => dispatch(loadSettings(params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  loadCycleFO: (year, foId, params) => dispatch(loadCycleFO(year, foId, params)),
  // loadILCSV: (year, params) => dispatch(loadILCSV(year, params)),
  loadLatestCycle: (params) => dispatch(loadLatestCycle(params)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  loadSelectionProgressList: (year, params) => dispatch(loadSelectionProgressList(year, params)),
  loadSelectionProgressHQ: (year, params) => dispatch(loadSelectionProgressListHQ(year, params))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(InterimView))
