import React, { useState, useEffect, } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { connect } from "react-redux";
import { browserHistory as history, withRouter } from "react-router";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { reset } from "redux-form";
import { SubmissionError } from "redux-form";
import ListLoader from "../../../common/listLoader";
import TablePaginationActions from "../../../common/tableActions";
import { editProject } from "../../../../reducers/projects/projectsReducer/editProject";
import { errorToBeAdded } from "../../../../reducers/errorReducer";
import { formatMoney } from "../../../../utils/currencyFormatter";
import {
  SENSE_CHECKS,
  LIST_TYPE,
  CYCLE_STATUS,
} from "../../../../helpers/constants";
import IncludeProject from "../../common/IncludeProject";
import ExcludeProject from "../../common/ExcludeProject";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  table: {
    minWidth: 500,
    maxWidth: "100%",
  },
  headerRow: {
    backgroundColor: "#F6F9FC",
    border: "1px solid #E9ECEF",
    height: 60,
    font: "medium 12px/16px Roboto",
  },
  noDataRow: {
    margin: 16,
    width: "100%",
    textAlign: "center",
  },
  highRiskRow: {
    backgroundColor: "#ef8588 ",
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: "1px solid #E9ECEF",
    color: "#344563",
    font: "bold 16px/21px Roboto",
    letterSpacing: 0,
  },
  tableWrapper: {
    // width: '58.5%',
    width: window.navigator.userAgent.includes("Windows") ? "95vw" : "97vw",
    maxHeight: 600,
    overflow: "auto",
    borderRadius: 6,
    marginRight: "-3px !important",
  },
  tableIncludeRow: {
    backgroundColor: "#F5DADA",
  },
  viewProject: {
    fontSize: 14,
    fontFamily: "Roboto",
    color: "#344563",
    // '&:hover': {
    //   cursor: 'pointer'
    // },
    borderBottom: "1px solid #E9ECEF",
    padding: "10px 26px 10px 24px",
  },
  deletedRow: {
    backgroundColor: "red",
  },
  tableHeader: {
    color: "#344563",
    fontFamily: "Roboto",
    fontSize: 14,
    transform: "translateY(-1px)",
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8,
  },
  tableCell: {
    font: "14px/16px Roboto",
    letterSpacing: 0,
    color: "#344563",
    borderBottom: "1px solid rgb(112,112,112)",
  },
  controlCell: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paginationSpacer: {
    flex: "none",
  },
  paginationCaption: {
    marginRight: theme.spacing(6),
  },
}));

const rowsPerPage = 10;

const EXCLUDE = "EXCLUDE";

function CustomPaginationActionsTable(props) {
  const {
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    sort,
    messages,
    postError,
    resetCommentForm,
    editProject,
    cycle,
    year,
    settings,
    membership,
  } = props;

  const [items, setItems] = useState(props.items);

  useEffect(() => {
    if (props.items) {
      setItems(props.items);
    }
  }, [props.items])



  const classes = useStyles();
  const [open, setOpen] = React.useState({ status: false, type: null });
  const [project, setProject] = React.useState({});
  const [headers, setHeaders] = React.useState(messages.table.headers.slice(1));
  // const isFOReviewer = membership && membership.role === 'REVIEWER'
  const isFOReviewer =
    membership && membership.role === "REVIEWER" && membership.type === "FO";
  const isFOApprover =
    membership && membership.role === "APPROVER" && membership.type === "FO";

  React.useEffect(() => {
    if (membership && membership.type === "FO") {
      if (
        isFOReviewer &&
        cycle &&
        cycle.status &&
        cycle.status.code === CYCLE_STATUS.INT01.code
      ) {
        setHeaders(messages.table.headersFO);
      } else {
        setHeaders(messages.table.headersFO.slice(1));
      }
    }
    if (membership && membership.type === "HQ") {
      setHeaders(messages.table.headers.slice(1));
    }
  }, [membership, cycle]);

  function handleClose() {
    setOpen({ status: false, type: null });
    resetCommentForm();
  }

  function handleExcludePA(values) {
    const { id, comment } = values;
    const body = { comment, int_sense_check: "12" };

    return editProject(id, body)
      .then(() => {
        handleClose();
      })
      .catch((error) => {
        if (error.validationErrors) {
          throw new SubmissionError({
            ...error.response.data,
            _error: messages.error,
          });
        } else {
          postError(error, messages.error);
        }
      });
  }

  const handleClassName = (item) => {
    if (item && item.int_sense_check) {
      return classes.tableExcludeRow; // the naming convention is wrong
    } else {
      return classes.tableIncludeRow; // the naming convention is wrong
    }
  };

  const handleAction = (item) => {
    const include = (
      <TableCell className={classes.viewProject}>
        <IncludeProject
          listType={LIST_TYPE.interim}
          project={item}
          year={year}
        />
      </TableCell>
    );
    const exclude = (
      <TableCell className={classes.viewProject}>
        <ExcludeProject
          listType={LIST_TYPE.interim}
          project={item}
          year={year}
        />
      </TableCell>
    );
    if (item.int_sense_check) {
      return exclude;
    } else {
      return include;
    }
  };

  const repeatRows = (n) => {
    let items = [];
    for (let i = 1; i < n; i++) {
      items.push(i);
    }
    return items.map((item) => (
      <TableRow key={item}>
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
        <TableCell className={classes.tableCell} component="th" scope="row" />
      </TableRow>
    ));
  };


  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <Table className={classes.table}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>{messages.table.title}</div>
            <TableHead>
              <TableRow className={classes.headerRow}>
                {headers.map((item, key) =>
                  cycle &&
                    cycle.status &&
                    cycle.status.id < 4 &&
                    item.title == "" ? null : (
                    <TableCell
                      align={item.align}
                      key={key}
                      className={classes.tableHeader}
                      style={{
                        position: "sticky",
                        top: 0,
                        backgroundColor: "#F6F9FC",
                        zIndex: 2,
                      }}
                    >
                      {item.title == "" || item.title == "Sense Check" ? (
                        item.title
                      ) : (
                        <TableSortLabel
                          active={item.field === sort.field}
                          direction={sort.order}
                          onClick={() => handleSort(item.field, sort.order)}
                        >
                          {item.title}
                        </TableSortLabel>
                      )}
                    </TableCell>
                  )
                )}
              </TableRow>
            </TableHead>
            {items && items.length == 0 && !loading ? (
              <div className={classes.noDataRow}>
                No data for the selected criteria
              </div>
            ) : (
              <TableBody>
                {items &&
                  items.map((item, index) => (
                    <TableRow key={index} className={handleClassName(item)}>
                      {cycle &&
                        isFOReviewer &&
                        cycle.status &&
                        cycle.status.code === CYCLE_STATUS.INT01.code &&
                        handleAction(item)}
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.int_comment ? item.int_comment.message : "N/A"}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.business_unit}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.number}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.delta ? "Yes" : "No"}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.country}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.partner.legal_name}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.partner.number}
                      </TableCell>
                      <TableCell
                        className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        ${formatMoney(item.budget)}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        ${formatMoney(item.installments_paid)}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.description}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.partner.implementer_type.implementer_type}
                      </TableCell>
                      <TableCell className={classes.viewProject}>
                        {item.start_date}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.end_date}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.agreement_type}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.agreement_date}
                      </TableCell>
                      <TableCell className={classes.viewProject}
                        onClick={() =>
                          history.push("/projects/" + `${item.id}`)
                        }
                      >
                        {item.operation.code}
                      </TableCell>
                      {membership && membership.type === "HQ" &&
                        <TableCell
                          className={classes.viewProject}
                          style={{ whiteSpace: "nowrap" }}
                        >
                          {item.reason && item.reason.description}
                        </TableCell>
                      }
                    </TableRow>
                  ))}
              </TableBody>
            )}
          </div>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={1}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                classes={{
                  spacer: classes.paginationSpacer,
                  caption: classes.paginationCaption,
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
});

const mapDispatchToProps = (dispatch) => ({
  resetCommentForm: () => dispatch(reset("commentDialog")),
  editProject: (id, body) => dispatch(editProject(id, body)),
  postError: (error, message) =>
    dispatch(errorToBeAdded(error, "editProject", message)),
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomPaginationActionsTable);

export default withRouter(connected);
