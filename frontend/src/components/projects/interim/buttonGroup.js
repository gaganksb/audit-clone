import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import { editCycle } from '../../../reducers/projects/projectsReducer/editCycle'
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle'
import { editProject } from '../../../reducers/projects/projectsReducer/editProject'
import { exportCSVFile } from '../../../utils/csvGenerator'
import { withRouter } from 'react-router'
import { initializeInterim } from '../../../reducers/projects/projectsReducer/initializeInterimList'
import SelectionStage from './progress/SelectionStage'

const useStyles = makeStyles(theme => ({
  buttonGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: -theme.spacing(2),
    display: 'flex',
    minWidth: 100
  },
  customizedButton: {
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 0,
    marginLeft: 16,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  }
}))

let csvItem = [{}]

function ButtonGroup(props) {
  const classes = useStyles()
  const { year, csv, loadPreCSV, cycle, headers, settingsClicked, finalized, query, totalCount } = props

  // csvItem = (csv && csv.result) ? csv.result.map(item => ({
  //   "reason": item.sense_check.description,
  //   "businessUnit": item.aggrement.business_unit,
  //   "agreementNumber": item.aggrement.number,
  //   "country": item.aggrement.conutry,
  //   "legalName": item.aggrement.partner.partner_legal_name,
  //   "number": item.aggrement.partner.partner_number,
  //   "budget": item.aggrement.budget,
  //   "installmentsPaid": item.aggrement.installments_paid,
  //   "description": item.aggrement.description,
  //   "implementerType": item.aggrement.partner.implementer_type.implementer_type,
  //   "agreementStartDate": item.aggrement.start_date,
  //   "agreementEndDate": item.aggrement.end_date,
  //   "agreementType": item.aggrement.agreement_type,
  //   "agreementDate": item.aggrement.agreement_date,
  //   "operation": item.aggrement.operation
  // })) : null

  // function loadCSV() {
  //   loadPreCSV(year, query).then(() => { 
  //     // exportCSVFile(headers, csvItem, 'Preliminary')
  //    })
  // }

  return (
    <Grid className={classes.buttonGrid}>
      {totalCount > 0 && <Button onClick={loadCSV} className={classes.customizedButton} variant="contained">EXPORT</Button>}
      <SelectionStage/>
    </Grid>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
  // csv: state.preliminaryCSV.preCSV,
  totalCount: state.interimList.totalCount
})

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  // loadPreCSV: (year, params) => dispatch(loadPreCSV(year, params)),
  initializeInterim: (year) => dispatch(initializeInterim(year)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  editProject: (id, body) => dispatch(editProject(id, body))
})

const connected = connect(mapStateToProps, mapDispatch)(ButtonGroup)
export default withRouter(connected)
