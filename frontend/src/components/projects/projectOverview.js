import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import { formatMoney } from '../../utils/currencyFormatter';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { renderMultiSelectField, renderRadioField } from '../../helpers/formHelper';
import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  typography: {
    marginLeft: theme.spacing(2),
    font: 'bold 14px/19px Roboto',
    color: '#606060'
  },
  radio: {
      '& > span ': {
      fontSize: 14,
      fontWeight: 'bold',
      color: '#6F716F !important'
    }
    },
    radioButton : {
    '&.Mui-checked': {
        color: '#0072BC !important'
      }
    },
  typographybold: {
    font: 'bold 14px/19px Roboto',
    color: '#0072bc',
  },
  title: {
    flexGrow: 1,
    color: '#606060',
    font: 'bold 18px/24px Roboto',
  },
  container: {
    borderBottom: '1px solid #E9ECEF',
    '& > div': {
      marginBottom: 12,
      paddingLeft: 60,
      '& > div': {
        marginBottom: '3px'
      },
      '&:first-child': {
        paddingLeft: 0
      }
    }
  },
  
  columns: {
    borderRight: '1px solid #E9ECEF',
    marginBottom: 15,
  },
  titleWrapper: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 20,
    marginLeft: -18,
    '& > svg': {
      width: 20,
      height: 20,
      cursor: 'pointer',
      marginRight: 15
    }
  }
}));

const ProjectOverview = props => {
  const classes = useStyles();
  const { project } = props;
    
  const radioOptions = [{label: 'Audited By Government', value: 'true'}, {label: 'Not Audited By Government', value: 'false'}]

  return (
    <div>
      <div className={classes.titleWrapper}>
        <Typography className={classes.title} style={{ marginLeft: 8 }} >Project Details</Typography>
      </div>
      <Grid container spacing={3} className={classes.container}>
        <Grid item xs={4} className={classes.columns}>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Agreement Number</Typography>
            </Grid>
            <Grid item xs={6}>
            <Typography className={classes.typography}>{project.number}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Business Unit</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.business_unit}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Agreement Type</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.agreement_type}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Region Name</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.region}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Country</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.country}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Start Date</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.start_date}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Completion Date</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{project.end_date}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={4} className={classes.columns}>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Budget</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(project.budget)}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Installments</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(project.installments_paid)}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Receivables</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report,'receivables',''))}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Cancellations</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report,'cancellations', ''))}</Typography>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={4}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Adjustments</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report,'adjustments', ''))}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >IPFR</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report, 'ipfr',''))}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Write Off</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report,'write_off',''))}</Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Refund</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>${formatMoney(_.get(project.agreement_report,'refund', ''))}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={4} style={{ padding: 0, marginBottom: 0 }}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >UNHCR Audit Focal Point</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{_.get(project.agreement_member,'unhcr.focal.fullname','')}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >UNHCR Audit Alternate Focal Point</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{_.get(project.agreement_member,'unhcr.alternate_focal.fullname','')}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography className={classes.typographybold} >Location of Project Documents for Auditor's Visit</Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography className={classes.typography}>{(project.locations!=undefined) && (project.locations.map((location) =>{ if(location.location_type== 'document' && location.country) return(<div> {location.country.name} {location.city && `, ${location.city}`} {location.address && `, ${location.address}`} </div>)} ))}</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography className={classes.typographybold} >Location of Project Activities for Auditor's Visit</Typography>
            </Grid>
            <Grid item xs={12}>
            <Typography className={classes.typography}>{(project.locations!=undefined) && (project.locations.map((location) =>{ if(location.location_type== 'site' && location.country) return(<div> {location.country.name} {location.city && `, ${location.city}`} {location.address && `, ${location.address}`} </div>)} ))}</Typography>
            </Grid>
          </Grid>
          
        </Grid>
        <Grid container spacing={2} style={{paddingLeft: 0}}>
            <Grid item xs={12}>
            <div>
              <FormControlLabel
                  style={{ display: "inline-block"}}
                  control={
                    <Checkbox
                      className={classes.radioButton}
                      style={{ display: "inline-block" }}
                      disabled= {true}
                      checked = {JSON.stringify(project.audited_by_gov) == 'true' }
                    />
                  }
                  className={classes.radio}
                  label='Audited By Government'
                  labelPlacement="end"
                />
          </div>
            </Grid>
          </Grid>
        <hr style={{ width: '100%' }}></hr>
        <Grid item xs={12}>
          <Typography className={classes.typographybold} >Description</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography className={classes.typography}>{project.description}</Typography>
        </Grid>
        <hr style={{ width: '100%' }}></hr>
        
        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >Partner Details</Typography>
        </Grid>
        <Grid item xs={4}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Legal Name</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{_.get(project.partner,'legal_name', '')}</Typography>
            </Grid>
          </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Number</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.partner,'number', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Implementer Type</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.partner,'implementer_type.implementer_type', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Focal Point</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.agreement_member,'partner.focal.fullname','')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Alternate Focal Point</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.agreement_member,'partner.alternate_focal.fullname','')}</Typography>
              </Grid>
            </Grid>
          </Grid>
          <hr style={{ width: '100%' }}></hr>

        <Grid item xs={12} style={{ padding: 0, marginBottom: 0 }}>
          <Typography className={classes.typographybold} >Auditor Details</Typography>
        </Grid>
        <Grid item xs={4}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Firm</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>{_.get(project.audit_firm,'legal_name','')}</Typography>
            </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Country</Typography>
              </Grid>
              <Grid item xs={6}>
                 <Typography className={classes.typography}>{_.get(project.audit_firm,'country.name', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Address 1</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.audit_firm, 'address_1', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Address 2</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.audit_firm,'address_2', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >City</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{_.get(project.audit_firm, 'city', '')}</Typography>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <Typography className={classes.typographybold} >Reviewers</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography className={classes.typography}>{(project.agreement_member != undefined && project.agreement_member.auditing.reviewers != undefined) && (project.agreement_member.auditing.reviewers.map((reviewer) => <div> {reviewer.fullname} </div> ))}</Typography>
              </Grid>
            </Grid>
          </Grid>   
        </Grid>
    </div>
  )
};

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatch = dispatch => ({
});

const connectedProjectOverview = connect(mapStateToProps, mapDispatch)(ProjectOverview);
export default withRouter(connectedProjectOverview);
