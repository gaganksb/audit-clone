import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Divider from '@material-ui/core/Divider'
import CustomizedButton from '../../common/customizedButton'
import { buttonType } from '../../../helpers/constants';
import { renderTextArea } from '../../../helpers/formHelper';

const messages = {
  dialogTitle: 'Exclude Project from the List',
  commentField: 'Provide the reason for which the selected Project Agreement should be excluded from the List',
  cancel: 'Cancel',
  save: 'Submit'
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    fontSize: 14,
    color: '#3A8FCC',
    marginTop: 24,
    marginLeft: 14
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'comment',
  ]
  requiredFields.forEach(field => {
    if (!values[field] || values[field].trim() == '') {
      errors[field] = 'Required'
    }
  })
  return errors
}


const commentLabel = (project) => {
  return (project && project.agreement) ? 'Provide the reason for which Project Agreement number ' + project.agreement.number + ' should be excluded from the list' : messages.commentField
}

function ExcludeProjectForm(props) {
  const { handleSubmit, handleClose, handleChange, disabled, open, project } = props
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle>{messages.dialogTitle}</DialogTitle>
        <Divider />
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
            <div style={{ width: '100%' }}>
              <Field
                name='comment'
                label={commentLabel(project)}
                className={classes.textField}
                component={renderTextArea}
                onChange={handleChange}
              />
              <div className={classes.buttonGrid}>
                <CustomizedButton clicked={handleClose} buttonText={messages.cancel} buttonType={buttonType.cancel} />
                <CustomizedButton type="submit" buttonText={messages.save} buttonType={buttonType.submit} disabled={disabled} />
              </div>
            </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

ExcludeProjectForm.propTypes = {
}

const ExcludeProjectReduxForm = reduxForm({
  form: 'ExcludeProjectReduxForm',
  validate,
  enableReinitialize: true,
})(ExcludeProjectForm)

const mapStateToProps = (state, ownProps) => {
}

const mapDispatchToProps = dispatch => ({
  reset: () => dispatch(reset('ExcludeProjectReduxForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ExcludeProjectReduxForm))