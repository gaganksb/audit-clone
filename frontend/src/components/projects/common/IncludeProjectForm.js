import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'
import Dialog from '@material-ui/core/Dialog'
import { withRouter } from 'react-router'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import { SENSE_CHECKS } from '../../../helpers/constants'
import { renderSelectField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel'
import CustomizedButton from '../../common/customizedButton'
import {buttonType} from '../../../helpers/constants'
import {renderTextArea} from '../../../helpers/formHelper';

const messages = {
  dialogTitle: 'Include Project in the List',
  reasonLabel: 'Reason',
  commentLabel: 'Provide the reason for which this Project Agreement should be included in the List',
  dialogContent: 'Complete the form below to include the Project Agreement in the List',
  cancel: 'Cancel',
  submit: 'Submit'
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    fontSize: 14,
    color: '#3A8FCC',
    marginTop: 24,
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  menu: {
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18
    },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  formControl: {
    marginTop: 1,
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(2),
    width: '100%',
  },
}))

var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    'comment'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}



const dialogContent = (project) => {
  return (project && project.agreement) ? <span>Complete the form below to include the Project Agreement number {project.agreement.number} in the List</span> : <span>{messages.dialogContent}</span>
}

function IncludeProjectForm(props) {
  const { 
    handleSubmit, 
    handleClose, 
    open,
    project,
    membership,
    pristine, 
    submitting
   } = props;

   const setOptionsList = () => {
    let res = []
    for (let prop in SENSE_CHECKS) {
      if(membership && membership.type === "FO") {
        if (SENSE_CHECKS[prop] == SENSE_CHECKS.manually_included) {
          res.push(SENSE_CHECKS[prop])
        }
      }
      else if (SENSE_CHECKS[prop] == SENSE_CHECKS.manually_excluded) {
        continue
      } else {
        res.push(SENSE_CHECKS[prop])
      }
    }
    return res
  }
  const classes = useStyles()
  const optionsList = setOptionsList()
  
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle>{messages.dialogTitle}</DialogTitle>
        <Divider />
        <DialogContent>
          <div style={{marginTop: 15}}>
            {dialogContent(project)}
          </div>
          <Grid container >
            <form className={classes.container} onSubmit={handleSubmit}>
              <div style={{width:'100%'}}> 
              {/* <InputLabel className={classes.inputLabel}>{messages.reasonLabel}</InputLabel>
              <Field
                  name='reason'
                  label={messages.reasonLabel}
                  component={renderSelectField}
                  className={classes.formControl}
                  margin='normal'
                >
                  {optionsList.map(option => (
                    <option key={option.id} value={option.code} style={{ fontSize: '12.5px' }} className={classes.menu}>
                      {option.description}
                    </option>
                  ))}
                </Field> */}
                <Field 
                  name='comment'
                  label={messages.commentLabel}
                  className={classes.textField}
                  component={renderTextArea}
                />
                <div className={classes.buttonGrid}>
                  <CustomizedButton clicked={handleClose} buttonText={messages.cancel} buttonType={buttonType.cancel} />
                  <CustomizedButton type="submit" buttonText={messages.submit} buttonType={buttonType.submit} disabled={pristine || submitting} />
                </div>
              </div>
            </form>
          </Grid>
        </DialogContent>
      </Dialog>
    </div>
  );
}

IncludeProjectForm.propTypes = {
}

const IncludeProjectReduxForm = reduxForm({
  form: 'IncludeProjectForm',
  validate,
  enableReinitialize: true,
})(IncludeProjectForm)

const mapStateToProps = (state, ownProps) => ({  
  membership: state.userData.data.membership
})

const mapDispatchToProps = dispatch => ({
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IncludeProjectReduxForm))