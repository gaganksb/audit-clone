import React from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ListLoader from '../../../common/listLoader'
import TablePaginationActions from './tableActions'
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { ActivityDetail } from './activityDetail'

const useStyles = makeStyles(theme => ({
  root: {
    marginBottom: theme.spacing(2),
    borderRadius: 6,
    border: '1px solid rgb(112,112,112)',
    maxHeight: 364,
    overflowY: 'auto'
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgb(112, 112, 112)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
  viewProject: {
    fontSize: 14,
    fontFamily: 'Roboto',
    color: '#344563',
    borderBottom: '1px solid rgb(112,112,112)',
    padding: '10px 24px'
  },
  expandIcon: {
    verticalAlign: 'top',
    width: 65,
    '& > svg': {
      marginTop: 7
    }
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563',
    borderBottom: '1px solid rgb(112,112,112)'
  },
  paginationSpacer: {
    flex: 'none'
  },
  paginationCaption: {
    marginRight: theme.spacing(6)
  },
  icon: {
    '&:hover': {
      cursor: 'pointer'
    }
  },  
  noDataRow: {
    margin: 16,
    width: '100%',
    textAlign: 'center'
  },
}))

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items,
    count,
    loading,
    loadData,
    year
  } = props

  const classes = useStyles()
  const [sort, setSort] = React.useState({
    field: '',
    order: 'asc'
  })
  const [page, setPage] = React.useState(0)
  const [activeItem, setActiveItem] = React.useState(-1)

  const repeatRows = (n) => {
    let items = []
    for (let i = 1; i < n; i++) {
      items.push(i)
    }
    return (
      items.map(item => (
        <TableRow key={item} >
          <TableCell className={classes.tableCell} component="th" scope="row" />
          <TableCell className={classes.tableCell} component="th" scope="row" />
        </TableRow>
      ))
    )
  }

  const handleChangePage = (event, newPage) => {
    loadData(year, { page: newPage + 1, ...sort })
    setPage(newPage)
    setActiveItem(-1)
  }

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <div className={classes.heading}>
          {`Activity Stream`}
        </div>
        <Table className={classes.table}>
          <TableBody>
            {items && items.length ?
              items.map((item, index) => (
                <TableRow
                  key={item.id}
                >
                  <TableCell className={`${classes.viewProject} ${classes.expandIcon}`}>
                    {activeItem === index ? (
                      <ExpandLess className={classes.icon} onClick={() => setActiveItem(-1)} />
                    ) : (
                      <ExpandMore className={classes.icon} onClick={() => setActiveItem(index)} />
                    )}
                  </TableCell>
                  <TableCell className={classes.viewProject}>
                    <ActivityDetail item={item} open={activeItem === index} />
                  </TableCell>
                </TableRow>
              )
            ) : <div className= {classes.noDataRow}>No data for the selected criteria</div>}
          </TableBody>
          {/* <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={5}
                count={count}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                classes={{
                  spacer: classes.paginationSpacer,
                  caption: classes.paginationCaption
                }}
              />
            </TableRow>
          </TableFooter> */}
        </Table>
      </ListLoader>
    </Paper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query
})

const mapDispatchToProps = (dispatch) => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editProject', message))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)
