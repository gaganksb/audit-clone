import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import { ACTIVITY_MSG } from "../../../../helpers/constants";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
    color: "black",
    fontSize: 15,
  },
  title: {
    fontSize: 15,
    marginBottom: 10,
  },
  top: {
    marginBottom: 30,
    "& > p": {
      margin: "0 0 5px",
    },
  },
  bottom: {
    "& > div": {
      marginBottom: 5,
    },
  },
  label: {
    color: "rgb(167,177,190)",
    fontSize: 16,
  },
}));

export const ActivityDetail = ({ item, open }) => {
  const classes = useStyles();
  const [active, setActive] = React.useState(open);

  React.useEffect(() => {
    setActive(open);
  }, [open]);

  return (
    <div className={classes.root}>
      <div className={classes.title}>
        {/* {item &&
          item.comment_type &&
          item.comment_type.change_type &&
          `${ACTIVITY_MSG[item.comment_type.change_type][0]} ${
            ACTIVITY_MSG[item.comment_type.change_type][1]
          }`} */}
      </div>
      {active && (
        <React.Fragment>
          <div className={classes.top}>
            <p className={classes.label}>Comment</p>
            <span>{item.message}</span>
          </div>
          <div className={classes.bottom}>
            <div>
              <span className={classes.label}>Author: </span>
              <span>
                {item.user.fullname} ({item.user.email})
              </span>
            </div>
            <div>
              <span className={classes.label}>Date: </span>
              <span>
                {moment(new Date(item.created_date)).format(
                  "YYYY-MM-DD HH:mm:ss"
                )}
              </span>
            </div>
          </div>
        </React.Fragment>
      )}
    </div>
  );
};
