import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Divider from '@material-ui/core/Divider'
import CustomizedButton from '../../common/customizedButton';
import { buttonType } from '../../../helpers/constants';
import { renderTextArea } from '../../../helpers/formHelper';

const messages = {
  cancel: 'CANCEL',
  accept: 'SUBMIT',
  reject: 'SUBMIT'
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
    fontSize: 14,
    color: '#3A8FCC',
    marginTop: 24,
    marginLeft: 14
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54,
    marginTop: 20
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}))

const validate = values => {
  const errors = {}
  const requiredFields = [
    'comment',
  ]
  requiredFields.forEach(field => {
    if (!values[field] || values[field].trim() == '') {
      errors[field] = 'Required'
    }
  })
  return errors
}


function AddCommentDialog(props) {
  const { handleSubmit, handleClose, handleChange, disabled, open, isAccept, title, description, cycle, membership } = props
  const classes = useStyles()
  const [deltaCheck, setDeltaCheck] = useState(false);

  useEffect(() => {
    setDeltaCheck(false);
  }, [open])

  const isHQReviewer = membership && membership.role === 'REVIEWER' && membership.type === 'HQ';

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      fullWidth
      maxWidth={'md'}
    >
      <DialogTitle>{title}</DialogTitle>
      <Divider />
      <DialogContent>
        <form className={classes.container} onSubmit={handleSubmit}>
          <div style={{ width: '100%' }}>
            <p>{description}</p>
            <Field
              name='comment'
              label="Comment"
              className={classes.textField}
              component={renderTextArea}
              onChange={handleChange}
            />
            <div className={classes.buttonGrid}>
              <CustomizedButton clicked={handleClose} buttonText={messages.cancel} buttonType={buttonType.cancel} />
              <CustomizedButton
                type="submit"
                buttonText={isAccept ? messages.accept : messages.reject}
                buttonType={isAccept ? buttonType.submit : buttonType.cancel}
              />
            </div>
          </div>
        </form>
      </DialogContent>
    </Dialog>
  )
}

const AddCommentDialogForm = reduxForm({
  form: 'AddCommentDialogForm',
  validate,
  enableReinitialize: true,
})(AddCommentDialog)

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  membership: state.userData.data.membership
})

const mapDispatchToProps = dispatch => ({
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddCommentDialogForm))
