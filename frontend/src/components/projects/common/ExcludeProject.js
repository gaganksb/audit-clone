import React, { Fragment, useState } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import DeleteIcon from '@material-ui/icons/Delete';
import ExcludeProjectForm from './ExcludeProjectForm'
import Tooltip from '@material-ui/core/Tooltip'
import { reset } from 'redux-form'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { SENSE_CHECKS, LIST_TYPE } from '../../../helpers/constants'
import { loadProjectList } from '../../../reducers/projects/projectsReducer/getProjectList';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { editProject, editILProject, editFLProject } from '../../../reducers/projects/projectsReducer/editProject'

const messages = {
  deleteTooltip: 'Exclude Project',
  error: 'Error',
  success: 'Project Excluded !'
}
const useStyles = makeStyles(theme => ({
  icon: {
    color: 'black',
    '&:hover': {
      cursor: 'pointer'
    },
  }
}))

const ExcludeProject = (props) => {
  const {
    query, 
    listType, 
    reset, 
    project, 
    excludeProject,
    loadList,
    successReducer,
    year, postError} = props
  const classes = useStyles()

  const [open, setOpen] = useState(false)
  const [disabled, setDisabled] = useState(true)

  const handleClick = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
    setDisabled(true)
    reset()
  }
  const handleChange = (event, newValue, previousValue, name) => {
    (newValue) ? setDisabled(false) : setDisabled(true)
  }
  const handleSubmit = (values) => {
    const { comment } = values
    const body = {
      comment,
      list_type: listType,
      action_type: "exclude",
      agreements: [project.id],
    };

    setDisabled(true)
      return excludeProject(year, body).then(() => {
        successReducer(messages.success)
        reset()
      }).catch((error) => {
        postError(error, messages.error)
      }).finally(() => {
        loadList(query)
        handleClose()
      })

  }

  return (
    <Fragment>
      <Tooltip title={messages.deleteTooltip} className={classes.icon}>
        <DeleteIcon onClick={handleClick} fontSize="medium" />
      </Tooltip>
      <ExcludeProjectForm 
        onSubmit={handleSubmit} 
        handleClose={handleClose} 
        handleChange={handleChange} 
        open={open} 
        project={project} 
        disabled={disabled}
      />
    </Fragment>
  )
}

ExcludeProject.propTypes = {
};

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
});

const mapDispatch = dispatch => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'ExcludeProject', message)),
  reset: () => dispatch(reset('ExcludeProjectReduxForm')),
  excludeProject: (id, body) => dispatch(editProject(id, body)),
  loadList: (params) => dispatch(loadProjectList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

export default withRouter(connect(mapStateToProps, mapDispatch)(ExcludeProject));
