import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Field, reduxForm, change } from "redux-form";
import { connect } from "react-redux";
import ListLoader from "../../common/listLoader";
import MenuItem from "@material-ui/core/MenuItem";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { SENSE_CHECKS } from "../../../helpers/constants";
import CustomAutoCompleteField from "../../forms/TypeAheadFields";
import {
  renderMultiSelectField,
  renderRadioField,
} from "../../../helpers/formHelper";
import CustomizedButton from "../../common/customizedButton";
import renderTypeAhead from "../../common/fields/commonTypeAhead";
import { renderTextField } from "../../common/renderFields";
import { loadBUList } from "../../../reducers/projects/oiosReducer/buList";
import { buttonType, filterButtons } from "../../../helpers/constants";
import InputLabel from "@material-ui/core/InputLabel";
import { loadPartnerList } from "../../../reducers/partners/partnerList";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3),
  },
  inputLabel: {
    fontSize: 14,
    color: "#344563",
    marginTop: 18,
    paddingLeft: 14,
  },
  container: {
    display: "flex",
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: "92%",
  },
  label: {
    marginRight: theme.spacing(1),
  },
  yearField: {
    width: "95%",
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: "flex",
    justifyContent: "flex-end",
  },
  typeAhead: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    marginBottom: 8,
    width: "90%",
  },
  menu: {
    "&.Mui-selected": {
      backgroundColor: "#E5F1F8",
    },
  },
  radio: {
    marginLeft: theme.spacing(2),
  },
  radioButton: {
    "&.Mui-checked": {
      color: "#0072BC",
    },
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  select: {
    width: "100%",
  },
  formControl: {
    marginTop: 1,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: "92%",
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: "93%",
    marginTop: 2,
  },
  filterTop: {
    padding: "0 16px",
  },
  delta: {
    display: 'flex',
    marginTop: 12,
  },
}));

const messages = {
  radio: {
    included: {
      value: "included",
      label: "Included",
    },
    excluded: {
      value: "excluded",
      label: "Excluded",
    },
    all: {
      value: "all",
      label: "All",
    },
  },
};

const partner_type_to_search = "partner__legal_name";

const updateOptionsList = (view_items) => {
  let res = [];
  for (let prop in SENSE_CHECKS) {
    if (
      view_items == messages.radio.included.value &&
      SENSE_CHECKS[prop] == SENSE_CHECKS.manually_excluded
    ) {
      continue;
    } else if (
      view_items == messages.radio.excluded.value &&
      SENSE_CHECKS[prop] != SENSE_CHECKS.manually_excluded
    ) {
      continue;
    } else {
      res.push(SENSE_CHECKS[prop]);
    }
  }
  return res;
};

function ProjectsFilter(props) {
  const classes = useStyles();
  const {
    handleSubmit,
    loading,
    reasons,
    reset,
    resetFilter,
    partnerList,
    key,
    setKey,
    membership,
    loadPartnerList,
    loadBUList,
    BUList,
    delta,
    onDeltaChange,
  } = props;
  const [optionsList, setOptionsList] = useState(
    updateOptionsList(messages.radio.all.value)
  );
  const [selectedReasons, setSelectedReasons] = useState([]);
  const [partnerLists, setPartnerList] = useState([]);
  const [partner, setSelectedPartner] = useState([]);
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());

  var date = new Date();

  const handleChange = async (e, v) => {
    if (e) {
      let params = {
        code: v,
      };
      const bus = await loadBUList(params);
    }
  };

  function handleChangePartner(e, v) {
    if (v) {
      let params = {
        legal_name: v,
      };
      loadPartnerList(params);
    }
  }

  const handleInputChange = (e) => { };

  useEffect(() => {
    setPartnerList(partnerList);
  }, [partnerList]);

  const updateReasons = (view_items, reasons) => {
    let res = [];
    if (view_items == messages.radio.included.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] != SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i]);
        }
      }
    } else if (view_items == messages.radio.excluded.value) {
      for (let i = 0; i < reasons.length; i++) {
        if (reasons[i] == SENSE_CHECKS.manually_excluded.code) {
          res.push(reasons[i]);
        }
      }
    } else {
      res = reasons;
    }
    return res;
  };

  const radioOptions = (() => {
    let res = [];
    for (let prop in messages.radio) {
      res.push(messages.radio[prop]);
    }
    return res;
  })();

  const handleSelect = (event) => {
    setSelectedReasons(event.target.value);
    reasons(event.target.value);
  };

  const checkSelection = (event) => {
    let updatedReasons = updateReasons(event.target.value, selectedReasons);
    setSelectedReasons(updatedReasons);
    reasons(updatedReasons);
    setOptionsList(updateOptionsList(event.target.value));
  };

  const onResetFilter = () => {
    reset();
    resetFilter();
    setKey(date.getTime());
    setSelectedReasons([]);
  };

  const handleValueChange = (val) => {
    setSelectedPartner(val);
  };

  const handleYearChange = (e) => {
    setSelectedYear(e.target.value);
  };

  const getDescriptionEval = (p) => {
    var yearExpr = "";
    var mySubString = p.substring(p.lastIndexOf("{") + 1, p.lastIndexOf("}"));
    if (mySubString != "") {
      var myArr = mySubString
        .replaceAll("current_year", selectedYear)
        .split(";");

      var startYearArr = myArr[0].split("-");
      var startYear = startYearArr[0] - startYearArr[1];
      yearExpr = startYear;

      if (myArr.length > 1) {
        var endYearArr = myArr[1].split("-");
        var endYear = endYearArr[0] - endYearArr[1];
        yearExpr = yearExpr + "-" + endYear;
      }
      p = p.replace("{", "");
      p = p.replace("}", "");
      p = p.replace(mySubString, yearExpr);
    }

    return p;
  };

  return (
    <div>
      <ListLoader loading={loading}>
        <Paper className={classes.paper}>
          <form onSubmit={handleSubmit}>
            <Grid container>
              <Grid item sm={3}>
                <InputLabel className={classes.inputLabel}>Partner</InputLabel>
                <Field
                  name="partner"
                  key={key}
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChangePartner}
                  items={partnerLists}
                  comingValue={partner}
                  type_to_search={partner_type_to_search}
                />
              </Grid>
              <Grid item sm={3}>
                <InputLabel className={classes.inputLabel}>Year</InputLabel>
                <Field
                  name="year"
                  margin="normal"
                  type="number"
                  component={renderTextField}
                  className={classes.textField}
                  InputProps={{
                    inputProps: {
                      min: 2014,
                    },
                  }}
                  onChange={handleYearChange}
                />
              </Grid>
              {((membership && membership.type != "FO") || !membership) && (
                <Grid item sm={5}>
                  <InputLabel className={classes.inputLabel}>
                    Reasons
                  </InputLabel>
                  <Field
                    name="reasons"
                    multiple={false}
                    style={{ width: "470px" }}
                    options={selectedReasons}
                    handleChange={handleSelect}
                    component={renderMultiSelectField}
                    className={classes.formControl}
                    margin="normal"
                  >
                    {optionsList.map((option) => (
                      <MenuItem
                        key={option.id}
                        value={option.code}
                        style={{ fontSize: "12.5px" }}
                        className={classes.menu}
                      >
                        {getDescriptionEval(option.description)}
                      </MenuItem>
                    ))}
                  </Field>
                </Grid>
              )}
              <br />
              <Grid item sm={3}>
                <InputLabel className={classes.inputLabel}>
                  Business Unit
                </InputLabel>
                <Field
                  name="business_unit__code"
                  margin="normal"
                  component={renderTypeAhead}
                  key={key}
                  className={classes.typeAhead}
                  onChange={handleChange}
                  handleInputChange={handleInputChange}
                  options={BUList ? BUList : []}
                  getOptionLabel={(option) => option.code}
                  multiple={false}
                />
              </Grid>
              <Grid item sm={3}>
                <InputLabel className={classes.inputLabel}>
                  Agreement Number
                </InputLabel>
                <Field
                  name="number"
                  margin="normal"
                  type="number"
                  component={renderTextField}
                  className={classes.textField}
                />
              </Grid>
              <Grid item sm={3}>
                <Field
                  name="view_items"
                  component={renderRadioField}
                  checkSelection={checkSelection}
                  className={classes.radio}
                  margin="normal"
                >
                  {radioOptions.map((option, index) => (
                    <FormControlLabel
                      key={index}
                      style={{ display: "inline-block" }}
                      value={option.value}
                      control={
                        <Radio
                          className={classes.radioButton}
                          style={{ display: "inline-block" }}
                          onClick={checkSelection}
                        />
                      }
                      label={option.label}
                      labelPlacement="end"
                    />
                  ))}
                </Field>
              </Grid>

              <Grid item sm={2}>
                <div className={classes.delta}>
                  <InputLabel className={classes.inputLabel}>Show Delta </InputLabel>
                  <Field name="delta" id="delta" component="input" style={{ marginTop: 16, width: 20, height: 20 }}
                    margin='normal'
                    type="checkbox" checked={delta} onChange={onDeltaChange} />
                </div>
              </Grid>

              <Grid item xs={12} container style={{ paddingTop: 8, paddingBottom: 8 }}>
                <Grid item xs={6} sm={4} md={1}>
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    buttonText={filterButtons.search}
                    clicked={handleSubmit}
                  />
                </Grid>

                <Grid item xs={6} sm={4} md={1} style={{ paddingLeft: 24 }}>
                  <CustomizedButton
                    buttonType={buttonType.submit}
                    reset={true}
                    clicked={onResetFilter}
                    buttonText={filterButtons.reset}
                  />
                </Grid>
              </Grid>
            </Grid>
          </form>
        </Paper>
      </ListLoader>
    </div>
  );
}

ProjectsFilter.propTypes = {
  handleSubmit: PropTypes.func,
  loading: PropTypes.bool,
  reasons: PropTypes.func,
};

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: {
      year: ownProps.year,
      view_items: messages.radio.all.value,
      selected_reasons: "",
    },
    partnerList: state.partnerList.list,
    BUList: state.BUListReducer.bu,
  };
};

const mapDispatch = (dispatch) => ({
  reasons: (value) => dispatch(change("ProjectsFilterForm", "reasons", value)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
});

const ProjectsFilterForm = reduxForm({
  form: "ProjectsFilterForm",
  enableReinitialize: true,
})(ProjectsFilter);

const connected = connect(mapStateToProps, mapDispatch)(ProjectsFilterForm);

export default connected;
