import React, { Fragment, useState } from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import IncludeProjectForm from './IncludeProjectForm'
import { reset } from 'redux-form'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import AddCircle from '@material-ui/icons/AddCircle'
import Tooltip from '@material-ui/core/Tooltip'
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { loadProjectList } from '../../../reducers/projects/projectsReducer/getProjectList';
import { editProject, editILProject, editFLProject } from '../../../reducers/projects/projectsReducer/editProject'
import { SENSE_CHECKS, LIST_TYPE, LISTS } from '../../../helpers/constants'

const messages = {
  include: 'Include Project in the List',
  error: 'Operation failed',  
  success: 'Project Included !'
}

const useStyles = makeStyles(theme => ({
  icon: {
    color: 'black',
    '&:hover': {
      cursor: 'pointer'
    },
  }
}))

const IncludeProject = (props) => {
  const { 
    query, 
    listType, 
    reset,
    project, 
    includeProject,
    loadList,
    successReducer,
    year, postError } = props
  const classes = useStyles()

  const [open, setOpen] = useState(false)

  const handleClick = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
    reset()
  }
  const parseReason = (reason) => {
    for (let prop in SENSE_CHECKS) {
      if (SENSE_CHECKS[prop].code == reason) {
        return SENSE_CHECKS[prop].id
      }
    }
  }
  const handleSubmit = (values) => {
    const { comment } = values;
    const body = {
      comment,
      list_type: listType,
      action_type: "include",
      agreements: [project.id],
    };

  includeProject(year, body).then(() => {
      successReducer(messages.success)
      reset()
    }).catch((error) => {
      postError(error, messages.error)
    }).finally(() => {
      loadList(query)
      handleClose()
    })
 
    handleClose()
    reset()
  }

  return (
    <Fragment>
      <Tooltip title={messages.include}>
        <AddCircle fontSize='medium' onClick={handleClick} className={classes.icon} />
      </Tooltip>
      <IncludeProjectForm
        onSubmit={handleSubmit}
        handleClose={handleClose}
        open={open}
        project={project}
      />
    </Fragment>
  )
}

IncludeProject.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
})

const mapDispatch = dispatch => ({
  postError: (error, message) => dispatch(errorToBeAdded(error, 'IncludeProject', message)),
  reset: () => dispatch(reset('IncludeProjectForm')),
  includeProject: (year, body) => dispatch(editProject(year, body)),
  loadList: (params) => dispatch(loadProjectList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(IncludeProject))
