import React, { useState, useEffect } from 'react';
import ExcludedPATable from './excludedTable';
import { connect } from 'react-redux';
import { loadPAExcludedList } from '../../../reducers/projects/projectsReducer/getPAExcludedList';
import { loadLatestCycle } from '../../../reducers/projects/projectsReducer/getLatestCycle';
import { browserHistory as history, withRouter } from 'react-router';
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle';
import R from 'ramda';

const messages = {
  title: 'PA Not Selected',
    tableHeader:[
      { title: 'Business Unit', align: 'left', field: 'agreement__business_unit'},
      { title: 'Country', align: 'left', field: 'agreement__country' },
      { title: 'Partner', align: 'left', field: 'agreement__partner__legal_name' },
      { title: 'Implementer Number', align: 'left', field: 'agreement__partner__number' },
      { title: 'Budget (in USD)', align: 'left', field: 'agreement__budget'},
      { title: 'Installments Paid', align: 'left', field: 'agreement__installments_paid'},
      { title: 'Description', align: 'left', field: 'agreement__description' },
      { title: 'Implementer Type', align: 'left', field: 'agreement__partner__implementer_type__implementer_type' },
      { title: 'Agreement Number', align: 'left', field: 'agreement__number' },
      { title: 'ProjectStart Date', align: 'left', field: 'agreement__start_date'},
      { title: 'ProjectEnd Date', align: 'left', field: 'agreement__end_date'},
      { title: 'Agreement Type', align: 'left', field: 'agreement__agreement_type' },
      { title: 'Agreement Date', align: 'left', field: 'agreement__agreement_date' },
      { title: 'Operation', align: 'left', field: 'agreement__operation'},
    ],
 }

 const ExcludedPAList = (props) => {
  const {  
    finalized,
    totalCount,
    paExcludedList,
    loadPAExcludedList,
    query,
    latestCycle,
    loadLatestCycle,
    loading
   } = props;

   const [params, setParams] = useState({ page: 0 });
   const [sort, setSort] = useState({
    field: messages.tableHeader[1].field,
    order: 'asc'
  })
  
  const [year, setYear] = useState(null);

  useEffect(() => {
    if (!query.year) {
      loadLatestCycle(query)
    }
    if(year || query.year) {
      loadPAExcludedList(year, query);
    }
  }, [query]);

  useEffect(() => {
    if (query.year) {
      setYear(query.year)
    } else if (latestCycle) {
      setYear(latestCycle.year)
    }
  }, [latestCycle]);

  useEffect(() => {
    if (year) {
      loadPAExcludedList(year, query);
    }
  }, [year])

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order,
      }),
    })
  }

  return (
      <div>
        <div style={{paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#606060',
    font: 'bold 18px/24px Roboto',
    letterSpacing: 0,
    backgroundColor: 'white',
    paddingBottom: 20}}>
          Excluded PA List
        </div>
      {/* <h4 style={{ marginLeft:28, color:'blue'}}>{messages.title}</h4> */}
      <ExcludedPATable
          messages={messages}
          sort={sort}
          finalized= {finalized}
          items={paExcludedList}
          loading={loading}
          totalItems={totalCount}
          page={params.page}
          handleChangePage={handleChangePage}
          handleSort={handleSort}
          year={year}
                   />
      </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.project
  return {
    initialValues,
    loading: state.paExcludedList.loading,
    paExcludedList: state.paExcludedList.list,
    totalCount: state.paExcludedList.totalCount,
    query: ownProps.location.query,
    location: ownProps.location,
    pathName: ownProps.location.pathname,
    finalized: state.cycle.finalized,
    latestCycle: state.latestCycle.details
  }
};

const mapDispatchToProps = dispatch => ({
  loadPAExcludedList: (year, params) => dispatch(loadPAExcludedList(year, params)),
  loadLatestCycle: (params) => dispatch(loadLatestCycle(params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExcludedPAList);

export default withRouter(connected)