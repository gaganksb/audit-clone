import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import TablePaginationActions from '../../common/tableActions';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import { formatMoney } from '../../../utils/currencyFormatter';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
    heading: {
      paddingLeft: 22,
      paddingTop: 19,
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
      color: '#344563',
      font: 'bold 16px/21px Roboto',
      letterSpacing: 0,
      paddingBottom: 20
    },
  tableWrapper: {
    width: '95vw',
    overflowX: 'auto'
  },
  tableRow: {
    '&:hover': {
      cursor: 'pointer'
    },
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const rowsPerPage = 10;

function ExcludedPATable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    sort,
    messages } = props;


  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        {/* <div className={classes.heading}>
          {<h3>Excluded PA List</h3>}
        </div> */}
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) => 
                  <TableCell align={item.align} key={key} className={classes.tableHeader}>
                  <TableSortLabel
                   active={item.field === sort.field}
                   direction={sort.order}
                   onClick={() => handleSort(item.field, sort.order)}
                 >
                   {item.title}
                 </TableSortLabel>                    
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map(item => (
                <TableRow key={item.id} className={classes.tableRow} onClick={() => history.push("/projects/" + `${item.id}`)}>                
                  <TableCell className={classes.tableCell}>
                    {item.agreement.business_unit}
                  </TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.country}</TableCell>
                  <TableCell className={classes.tableCell} component="th" scope="row">{item.agreement.partner.legal_name}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.partner.number}</TableCell>
                  <TableCell className={classes.tableCell}>${formatMoney(item.agreement.budget)}</TableCell>
                  <TableCell className={classes.tableCell}>${formatMoney(item.agreement.installments_paid)}</TableCell> 
                  <TableCell className={classes.tableCell}>{item.agreement.description}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.partner.implementer_type.implementer_type}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.number}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.start_date}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.end_date}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.agreement_type}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.agreement_date}</TableCell>
                  <TableCell className={classes.tableCell}>{item.agreement.operation}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
              <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={1}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExcludedPATable);

export default withRouter(connected);
