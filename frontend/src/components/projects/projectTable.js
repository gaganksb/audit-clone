import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../common/listLoader';
import TablePaginationActions from '../common/tableActions';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import Radio from '@material-ui/core/Radio';
import Select from '@material-ui/core/Select';
import RadioGroup from '@material-ui/core/RadioGroup';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    borderRadius: 6
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  headerRow: {
    backgroundColor: '#F6F9FC'
  },
  heading: {
    paddingLeft: 24,
    paddingTop: 6,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
  },
  tableWrapper: {
    // width: '95vw',
    overflowX: 'auto'
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const rowsPerPage = 10;

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    messages,
    handleChange,
    selectedValue,
    handleChangeSelect,
    selectValues,
    selectedId
     } = props;

  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        <div className={classes.heading}>
          {<h3>Excluded PA List</h3>}
        </div>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key}>
                    {item.title}
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map(item => (
                <TableRow key={item.id} className={classes.tableRow} >
                  <TableCell>
                    <Radio
                      checked={selectedValue == item.id}
                      onChange={()=>handleChange(event,item.agreement)}
                      value={item.id}
                      name="radio-button-demo"
                      inputProps={{ 'aria-label': 'A' }}/>
                  </TableCell>
                  <TableCell>
                    <Select value={(selectedId== item.id)?selectValues:null}
                    onChange={()=>handleChangeSelect(event, item.id)}
                    style={{width:180}}                    
                    >
                    {item.applicable_sense_checks.map((sense, index) => {
                      return <option value={sense[2]} id={sense[0]}> {sense[2]} </option>})}
                    </Select>
                  </TableCell>
                  <TableCell>
                    {item.agreement.business_unit}
                  </TableCell>
                  <TableCell>{item.agreement.country}</TableCell>
                  <TableCell component="th" scope="row">{item.agreement.partner.legal_name}</TableCell>
                  <TableCell>{item.agreement.partner.number}</TableCell>
                  <TableCell>${item.agreement.budget}</TableCell>
                  <TableCell>${item.agreement.installments_paid}</TableCell>
                  <TableCell>{item.agreement.description}</TableCell>
                  <TableCell>{item.agreement.partner.implementer_type.implementer_type}</TableCell>
                  <TableCell>{item.agreement.number}</TableCell>
                  <TableCell>{item.agreement.start_date}</TableCell>
                  <TableCell>{item.agreement.end_date}</TableCell>
                  <TableCell>{item.agreement.agreement_type}</TableCell>
                  <TableCell>{item.agreement.agreement_date}</TableCell>
                  <TableCell>{item.agreement.operation}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={1}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = (dispatch) => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable);

export default withRouter(connected);
