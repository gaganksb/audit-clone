import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Field, reduxForm, getFormValues, change, reset } from "redux-form";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Divider } from "@material-ui/core";
import CustomizedButton from '../common/customizedButton';
import { buttonType } from '../../helpers/constants';
import InputLabel from '@material-ui/core/InputLabel';
import { Dialog, DialogContent, DialogTitle } from "@material-ui/core";
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { USERS_TYPES } from '../../helpers/constants';
import commonTypeAhead from '../common/fields/commonTypeAhead';
import { renderFixedTextField, renderSelectField } from '../common/renderFields';
import { loadCountries } from '../../reducers/countries';
import AddIcon from '@material-ui/icons/Add';
import { newLocations } from '../../reducers/projects/projectsReducer/postLocation'
import RemoveIcon from '@material-ui/icons/Remove';
import Fab from '@material-ui/core/Fab';
import { deleteLocationRequest } from '../../reducers/projects/projectsReducer/removeLocation'
import renderTypeAhead from '../common/fields/commonTypeAhead';
import Checkbox from '@material-ui/core/Checkbox';
import { renderRadioField } from '../../helpers/formHelper'
import { loadUsersList } from "../../reducers/users/usersList";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },

    radio: {
        '& > span ': {
            fontSize: 14,
            fontWeight: 'bold',
            color: '#6F716F !important'
        }
    },
    radioButton: {
        '&.Mui-checked': {
            color: '#0072BC !important'
        }
    },

    container: {
        display: "flex",
        height: "fit-content"
    },
    textFields: {
        marginTop: '0px !important'
    },
    textField: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        width: "100%"
    },
    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: 18
    },
    inputSubLabel: {
        marginTop: 18,
        display: 'inline-block',
        width: '29%',
        marginRight: 18,
        '&:last-child': {
            marginRight: 0,
            width: '38%'
        }
    },
    fabIcon: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: 16
    },
    locationDiv: {
        border: '1px solid #E9ECEF',
        marginTop: 16,
        marginBottom: 16,
        padding: 16,
        paddingTop: 0,
        borderRadius: 16
    },
    menu: {
        padding: 8,
        '&.Mui-selected': {
            backgroundColor: '#E5F1F8',
            borderStyle: 'hidden'
        },
        '&:hover': {
            backgroundColor: '#E5F1F8',
            borderStyle: 'hidden'
        }
    },
    formControl: {
        width: '100%',
        marginRight: theme.spacing(1),
    },
    buttonGrid: {
        display: "flex",
        justifyContent: "flex-end",
        width: "100%",
    },
    preIcon: {
        color: 'white'
    },
    pre: {
        backgroundColor: '#BF0301',
        marginRight: 16
    },
    add: {
        backgroundColor: '#0072BC',
        marginRight: 16
    },
    error: {
        color: 'red',
        fontSize: 12,
        maxWidth: 'max-content',
        position: 'relative',
        top: '6.5%',
    },
    listFieldLabel: {
        display: "block",
    },
    listFieldValue: {
        display: "block",
        fontWeight: "bold",
        marginTop: ".5rem",
    },
}));

const validate = values => {
    const errors = {};
    const requiredFields = ["firm"];
    requiredFields.forEach(field => {
        if (!values[field] || values[field] === undefined) {
            errors[field] = "Required";
        }
    });
    return errors;
};

const messages = {
    cancel: 'Cancel',
    apply: 'Apply'
}

const initialLocationData = {
    id: "",
    country: "",
    city: "",
    address: "",
    location_type: ""
};

const EditProjectDetails = (
    {
        open,
        projects,
        handleClose,
        handleSubmit,
        handleInputChange,
        initialOption,
        countries,
        getCountries,
        cycle,
        membership,
        project,
        newLocations,
        id,
        userList,
        loadProjectDetails,
        deleteLocationRequest,
        locations,
        updateLocationList,
        locationList,
        locationUpdate,
        reset,
        clearLocationForm,
        initialBox,
        setInitial,
        loadUsersList,
    }
) => {
    const [key, setKey] = useState('default')
    const [formData, setFormData] = useState({})
    const [customError, setCustomError] = useState({})

    const [unhcrAuditFocalUsers, setUnhcrAuditFocalUsers] = useState([])
    const [unhcrAuditAlternateFocalUsers, setUnhcrAuditAlternateFocalUsers] = useState([])
    const [partnerFocalUsers, setPartnerFocalUsers] = useState([])
    const [partnerAlternateFocalUsers, setPartnerAlternateFocalUsers] = useState([])
    const [auditReviewers, setAuditReviewers] = useState([])

    const [stateMonitor, setStateMonitor] = useState({})

    const types = [{ 'ID': 'site', 'NAME': 'Implementation site' }, { 'ID': 'document', 'NAME': 'Project documents for audit' }]
    var date = new Date();
    const classes = useStyles();
    const radioOptions = [{ label: 'Audited By Government', value: 'true' }, { label: 'Not Audited By Government', value: 'false' }]


    useEffect(() => {
        setStateMonitor(locations);
    }, [locations]);

    const handleChange = async (e, v) => {
        if (e && ['unhcr_focal'].includes(e.target.name)) {
            let params = { fullname: v, office_id: project.field_office }
            loadUsersList(params).then(response => {
                if (response && response.results && response.results.length > 0)
                    setUnhcrAuditFocalUsers([...response.results])
                else {
                    setUnhcrAuditFocalUsers([])
                }

            }).catch(error => {
                setUnhcrAuditFocalUsers([])
            })
        } else if (e && ['unhcr_focal_alternate'].includes(e.target.name)) {
            let params = { fullname: v, office_id: project.field_office }
            loadUsersList(params).then(response => {
                if (response && response.results && response.results.length > 0)
                    setUnhcrAuditAlternateFocalUsers([...response.results])
                else {
                    setUnhcrAuditAlternateFocalUsers([])
                }

            }).catch(error => {
                setUnhcrAuditAlternateFocalUsers([])
            })
        }
        else if (e && ['partner_focal'].includes(e.target.name)) {
            let params = { fullname: v, partner: project.partner.id }
            loadUsersList(params).then(response => {
                if (response && response.results && response.results.length > 0)
                    setPartnerFocalUsers([...response.results])
                else {
                    setPartnerFocalUsers([])
                }

            }).catch(error => {
                setPartnerFocalUsers([])
            })
        } else if (e && ['partner_focal_alternate'].includes(e.target.name)) {
            let params = { fullname: v, partner: project.partner.id }
            loadUsersList(params).then(response => {
                if (response && response.results && response.results.length > 0)
                    setPartnerAlternateFocalUsers([...response.results])
                else {
                    setPartnerAlternateFocalUsers([])
                }

            }).catch(error => {
                setPartnerAlternateFocalUsers([])
            })

        } else if (e && ['auditor_reviewers'].includes(e.target.name)) {
            let params = { fullname: v }
            loadUsersList(params).then(response => {
                if (response && response.results && response.results.length > 0)
                    setAuditReviewers([...response.results])
                else {
                    setAuditReviewers([])
                }
            }).catch(error => {
                setAuditReviewers([])
            })
        }

    }

    const handleCountryChange = (event, name) => {
        getCountries({ name });
    }

    const handleLocationChange = (event, item, name) => {
        setFormData(
            {
                ...formData,
                [name]: item && item.id ? item.id : item
            }
        );
    }


    const updateCheckbox = () => {
        setInitial(!initialBox)
    }

    const parse = (locationType) => {
        switch (locationType) {
            case "site":
                return "Implementation site";
            case "document":
                return "Project documents for audit";
            default:
                return "-"
        }
    }

    const removeLocation = (location) => {
        if (location.id) {
            deleteLocationRequest(location.id).then(() => loadProjectDetails(id))

        }
        else {
            setKey(date.getTime())
            reset()
        }
    }

    const handleNewLocation = () => {
        if (
            formData &&
            formData.country &&
            formData.city &&
            formData.location_type
        ) {
            return newLocations({ ...formData, is_valid: false }).then((response) => {
                updateLocationList([...locationList, response.id]);
                locationUpdate([...locations, response]);
                clearLocationForm();
                setFormData({});
                setKey(date.getTime())
                setCustomError({});
            })
        } else {
            const newError = {}
            Object.keys(initialLocationData).map(item => {
                if (item !== "address") {
                    if (!formData[item]) {
                        newError[item] = "Required"
                    } else {
                        newError[item] = null
                    }
                }

            })
            setCustomError({ ...customError, ...newError })
            return false
        }

    }

    const handleSubmitLocation = (e) => {
        setCustomError({});
        if (Object.keys(formData).length === 0) {
            handleSubmit(e)
        } else {
            if (Object.keys(formData).length < 4) {
                const newError = {}
                Object.keys(initialLocationData).map(item => {
                    if (item !== "address") {
                        if (!formData[item]) {
                            newError[item] = "Required"
                        } else {
                            newError[item] = null
                        }
                    }

                })
                setCustomError({ ...customError, ...newError })
            } else {
                handleNewLocation().then((response) => {
                    if (response == false) {
                        return;
                    }
                    setFormData({})
                    setCustomError({})
                    handleSubmit(e)
                })
            }
        }
    }

    const getOptionLabel = option => {
        if (option.name && option.email) {
            let label = option.name + " - " + option.email;
            return label;
        } else {
            return option.fullname;
        }
    }

    return (
        <div className={classes.root}>
            <Dialog open={open} onClose={handleClose} fullWidth maxWidth={'md'} >
                <DialogTitle>Edit Project Details</DialogTitle>
                <Divider />
                <DialogContent>
                    <form className={classes.container} onSubmit={handleSubmit}>
                        <Grid
                            container
                            direction="column"
                        >
                            {(membership && (membership.type === USERS_TYPES.HQ || membership.type === USERS_TYPES.FO)) &&
                                <div>
                                    <InputLabel className={classes.inputLabel}>UNHCR Audit Focal Point</InputLabel>
                                    <Field
                                        name="unhcr_focal"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={unhcrAuditFocalUsers ? unhcrAuditFocalUsers : []}
                                        getOptionLabel={getOptionLabel}
                                        fullWidth
                                        className={classes.textField}
                                        defaultValue={project.agreement_member && project.agreement_member.unhcr && project.agreement_member.unhcr.focal}
                                    />
                                    <InputLabel className={classes.inputLabel}>UNHCR Audit Alternate Focal Point</InputLabel>
                                    <Field
                                        name="unhcr_focal_alternate"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={unhcrAuditAlternateFocalUsers ? unhcrAuditAlternateFocalUsers : []}
                                        getOptionLabel={getOptionLabel}
                                        fullWidth
                                        className={classes.textField}
                                        defaultValue={project.agreement_member && project.agreement_member.unhcr && project.agreement_member.unhcr.alternate_focal}
                                    />
                                </div>
                            }
                            {(membership && (membership.type === USERS_TYPES.PARTNER || membership.type === USERS_TYPES.HQ || membership.type === USERS_TYPES.FO)) &&
                                <div>
                                    <InputLabel className={classes.inputLabel}>
                                        Partner Focal Person
                                    </InputLabel>
                                    <Field
                                        name="partner_focal"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={partnerFocalUsers ? partnerFocalUsers : []}
                                        getOptionLabel={getOptionLabel}
                                        fullWidth
                                        className={classes.textField}
                                        defaultValue={project.agreement_member && project.agreement_member.partner && project.agreement_member.partner.focal}
                                    />
                                    <InputLabel className={classes.inputLabel}>
                                        Partner Alternate Focal Person
                                    </InputLabel>
                                    <Field
                                        name="partner_focal_alternate"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={partnerAlternateFocalUsers ? partnerAlternateFocalUsers : []}
                                        getOptionLabel={getOptionLabel}
                                        fullWidth
                                        className={classes.textField}
                                        defaultValue={project.agreement_member && project.agreement_member.partner && project.agreement_member.partner.alternate_focal}
                                    />
                                </div>
                            }
                            <InputLabel className={classes.inputLabel}>Locations</InputLabel>
                            <div className={classes.locationDiv}>
                                {
                                    locations.map((location) => (
                                        <div key={`location_item_${location.id}`}>
                                            <Grid item xs={4} className={classes.inputSubLabel}>
                                                <span className={classes.listFieldLabel}>Country</span>
                                                <span className={classes.listFieldValue}>{location.country && location.country.name}</span>
                                            </Grid>
                                            <Grid item xs={4} className={classes.inputSubLabel}>
                                                <span className={classes.listFieldLabel}>City</span>
                                                <span className={classes.listFieldValue}>{location.city}</span>
                                            </Grid>
                                            <Grid item xs={4} className={classes.inputSubLabel} style={{ minWidth: '34%', marginRight: 0 }}>
                                                <span className={classes.listFieldLabel}>Address</span>
                                                <span className={classes.listFieldValue}>{location.address}</span>
                                            </Grid>
                                            <Grid item xs={4} className={classes.inputSubLabel} style={{ minWidth: '34%', marginRight: 0 }}>
                                                <span className={classes.listFieldLabel}>Type</span>
                                                <span className={classes.listFieldValue}>{parse(location.location_type)}</span>
                                            </Grid>
                                            <div className={classes.fabIcon}>
                                                <Fab size='small' className={classes.pre} align='right' onClick={() => removeLocation(location)}>
                                                    <RemoveIcon className={classes.preIcon} />
                                                </Fab>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div>
                                    <Grid item xs={4} className={classes.inputSubLabel}>
                                        <InputLabel className={classes.inputLabel}>Country</InputLabel>
                                        <Field
                                            name="country"
                                            component={commonTypeAhead}
                                            onChange={handleCountryChange}
                                            handleInputChange={(e, v) => handleLocationChange(e, v, 'country', "new_location")}
                                            options={countries.results ? countries.results : initialOption && initialOption.country ? [initialOption.country] : []}
                                            getOptionLabel={(option => option.name)}
                                            multiple={false}
                                            key={key}
                                            fullWidth
                                            className={classes.autoCompleteField}
                                        />
                                        <span className={classes.error}>{customError.country}</span>
                                    </Grid>

                                    <Grid item xs={4} className={classes.inputSubLabel}>
                                        <InputLabel className={classes.inputLabel}>City</InputLabel>
                                        <Field
                                            name="city"
                                            className={classes.textFields}
                                            margin='normal'
                                            component={renderFixedTextField}
                                            onChange={(e, v) => handleLocationChange(e, v, 'city', "new_location")}
                                        />
                                        <span className={classes.error}>{customError.city}</span>
                                    </Grid>

                                    <Grid item xs={4} className={classes.inputSubLabel} style={{ minWidth: '34%', marginRight: 0 }}>
                                        <InputLabel className={classes.inputLabel}>Address</InputLabel>
                                        <Field
                                            name="address"
                                            className={classes.textFields}
                                            margin='normal'
                                            component={renderFixedTextField}
                                            onChange={(e, v) => handleLocationChange(e, v, 'address', "new_location")}
                                        />
                                        <span className={classes.error}>{""}</span>
                                    </Grid>

                                    <Grid item xs={4} className={classes.inputSubLabel} style={{ minWidth: '34%', marginRight: 0 }}>
                                        <InputLabel className={classes.inputLabel}>Type</InputLabel>
                                        {initialLocationData && initialLocationData.location_type ? (
                                            <Field
                                                name="location_type"
                                                className={classes.textFields}
                                                margin='normal'
                                                component={renderFixedTextField}
                                            />
                                        ) : (<Field
                                            name="location_type"
                                            onChange={(e, v) => handleLocationChange(e, v, 'location_type', "new_location")}
                                            component={renderSelectField}
                                            className={classes.formControl}
                                        >
                                            {types.map((item) => <option key={item.ID}
                                                value={item.ID} className={classes.menu}>
                                                {item.NAME}
                                            </option>)}
                                        </Field>)}
                                        <span className={classes.error} >{customError.location_type}</span>
                                    </Grid>

                                    <div className={classes.fabIcon}>
                                        <Fab size='small' className={classes.pre} align='right' onClick={() => removeLocation(location)}>
                                            <RemoveIcon className={classes.preIcon} />
                                        </Fab>
                                        <Fab size='small' onClick={handleNewLocation} className={classes.add} align='right'>
                                            <AddIcon className={classes.preIcon} />
                                        </Fab>
                                    </div>
                                </div>
                            </div>
                            {
                                (membership && (membership.type === USERS_TYPES.HQ || membership.type === USERS_TYPES.AUDITOR)
                                    && (cycle && cycle.status && cycle.status.id >= 8)) &&
                                <div>
                                    <InputLabel className={classes.inputLabel}>Auditor Reviewers</InputLabel>
                                    <Field
                                        name="auditor_reviewers"
                                        margin="normal"
                                        component={renderTypeAhead}
                                        onChange={handleChange}
                                        handleInputChange={handleInputChange}
                                        options={auditReviewers ? auditReviewers : []}
                                        getOptionLabel={getOptionLabel}
                                        multiple={true}
                                        fullWidth
                                        className={classes.textField}
                                        style={{ width: "100%" }}
                                        defaultValue={project.agreement_member && project.agreement_member.auditing && project.agreement_member.auditing.reviewers}
                                    />
                                </div>
                            }
                            <div>
                                <Field
                                    name="audited_by_gov"
                                    component={renderRadioField}
                                    className={classes.radioButton}
                                    margin='normal'
                                >
                                    <FormControlLabel
                                        style={{ display: "inline-block", marginRight: 191 }}
                                        control={
                                            <Checkbox
                                                className={classes.radioButton}
                                                style={{ display: "inline-block" }}
                                                onClick={() => updateCheckbox()}
                                                checked={initialBox}
                                            />
                                        }
                                        className={classes.radio}
                                        label='Audited By Government'
                                        labelPlacement="end"
                                    />
                                </Field>
                            </div>
                            <div className={classes.buttonGrid}>
                                <CustomizedButton buttonType={buttonType.cancel}
                                    buttonText={messages.cancel}
                                    clicked={handleClose} />
                                <CustomizedButton buttonType={buttonType.submit}
                                    clicked={(e) => handleSubmitLocation(e)}
                                    buttonText={messages.apply}
                                />
                            </div>
                        </Grid>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        membership: state.userData.data.membership,
        countries: state.countries,
        userList: state.usersList.users,
        initialValues: {
            audited_by_gov: ownProps.initialBox,
        },
    }
}

const mapDispatchToProps = (dispatch) => ({
    newLocations: (body) => dispatch(newLocations(body)),
    values: () => dispatch(getFormValues('ProjectDetails')),
    getCountries: (params) => dispatch(loadCountries(params)),
    loadUsersList: params => dispatch(loadUsersList(params)),
    deleteLocationRequest: (id) => dispatch(deleteLocationRequest(id)),
    clearLocationForm: () => {
        dispatch(change("ProjectDetails", "country", initialLocationData.country));
        dispatch(change("ProjectDetails", "city", initialLocationData.city));
        dispatch(change("ProjectDetails", "address", initialLocationData.address));
        dispatch(change("ProjectDetails", "location_type", initialLocationData.location_type));
    },
})

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: "ProjectDetails", validate, enableReinitialize: true })(EditProjectDetails));
