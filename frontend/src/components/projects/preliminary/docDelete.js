import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  }
}));

function AlertDialog(props) {
  const {open, handleClose, item, deleteDoc} = props;

  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogContent>
         <DialogContentText className={classes.dialogContentText} >
            Confirm that you want to delete <b>{(item) ?item.filename: null}</b> by clicking on <b>YES</b>
          </DialogContentText>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button onClick={handleClose} color="primary" >
            No
          </Button>
          <Button onClick={deleteDoc} color="primary" >
            Yes 
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialog;
