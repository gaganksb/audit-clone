import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadMessages } from '../../../reducers/projects/messagesReducer/loadMessage';
import MessageList from './messageList';
import PostMessage from './postMessage';
import { SubmissionError } from 'redux-form';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {addNewMessage} from '../../../reducers/projects/messagesReducer/addMessage';
import R from 'ramda';
import {deleteMessageRequest} from '../../../reducers/projects/messagesReducer/deleteMessage';

const Messages = props => {
  const { id, loading, totalCount, editMessage, query, loadMessages, messages, membership, addNewMessage, deleteMessage, postError } = props

  const [params, setParams] = useState({page: 0}); 

  useEffect(() => {
    loadMessages(id, query);
  }, [query]);



  function handleSubmit(values) {
    const {message} = values;
    const body = {
      message,
    }
    return addNewMessage(id, body).then(() => loadMessages(id, query))
    .catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error,
      });
    });
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleDeleteMessage(id, msgId) {
    const { query } = props
    
    return deleteMessage(id, msgId).then(() => loadMessages(id, query))
  }


  return (
    <div>
      <PostMessage onSubmit ={handleSubmit}/>
      <MessageList 
      messages={messages} 
      loading={loading} 
      totalCount={totalCount}   
      page={params.page}
      membership={membership}
      handleChangePage={handleChangePage}
      rowId={id}
      deleteMessage={(id, msgId) =>handleDeleteMessage(id, msgId)} />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  messages: state.messages.message,
  totalCount: state.messages.totalCount,
  loading: state.messages.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  membership: state.userData.data,
});

const mapDispatch = dispatch => ({
    loadMessages: (id, params) => dispatch(loadMessages(id, params)),
    addNewMessage: (id, body) => dispatch(addNewMessage(id, body)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'addMessage', message)),
    deleteMessage: (id, msgId) => dispatch(deleteMessageRequest(id, msgId))
});

const connectedGroups = connect(mapStateToProps, mapDispatch)(Messages);
export default withRouter(connectedGroups);