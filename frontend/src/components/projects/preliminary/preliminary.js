import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import { SubmissionError } from 'redux-form'
import R from 'ramda'
// import 'jspdf-autotable'
import 'jspdf-autotable'
import Paper from '@material-ui/core/Paper'
import { editProject } from '../../../reducers/projects/projectsReducer/editProject'
import { makeStyles } from '@material-ui/core/styles'
import { loadProjectList } from '../../../reducers/projects/projectsReducer/getProjectList';
import { loadSettings } from '../../../reducers/projects/projectsReducer/getSettings'
import { editSettings } from '../../../reducers/projects/projectsReducer/editSettings'
import { loadCycle } from '../../../reducers/projects/projectsReducer/getCycle'
import { loadLatestCycle } from '../../../reducers/projects/projectsReducer/getLatestCycle'
import { loadActivityList } from '../../../reducers/projects/projectsReducer/getActivityList'
import { loadUser } from '../../../reducers/users/userData'
import PreliminaryTable from './preliminaryTable'
import ActivityTable from '../common/activityTable/index'
import SettingModal from './settingModal'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import ButtonGroup from './buttonGroup'
import { loadPAExcludedList } from '../../../reducers/projects/projectsReducer/getPAExcludedList'
import ProjectsFilter from '../common/ProjectsFilter'
import Title from '../../common/customTitle';
import { loadPreliminaryStats } from '../../../reducers/projects/projectsReducer/statsReducer'
import _ from 'lodash'
import Grid from '@material-ui/core/Grid'
import { loadBgJob } from '../../../reducers/bgTaskReducer'
import { reset } from 'redux-form';
import { formatMoney } from '../../../utils/currencyFormatter'
import NoDataTable from './noData';
import { setDefaultAccount } from '../../../helpers/api/api'
import ListLoader from '../../../components/common/listLoader';

const headers = {
  reason: "Reason",
  bu: "Business Unit",
  agrNum: 'Agreement Number',
  country: "Country",
  partner: 'Partner',
  implNum: 'Implementer Number',
  budget: 'Budget (in USD)',
  installmentsPaid: 'Installments Paid',
  description: 'Description',
  impType: 'Implementer Type',
  projStartDate: 'ProjectStart Date',
  projEndDate: 'ProjectEnd Date',
  agrType: 'Agreement Type',
  agrDate: 'Agreement Date',
  operation: 'Operation'
}

const messages = {
  title: 'Preliminary List',
  tableHeader: [
    { title: '', align: 'left' },
    { title: 'Risk Rating', align: 'left', field: 'risk_rating' },
    { title: 'Comments(in manual modification)', align: 'left', field: 'comments' },
    { title: 'Business Unit', align: 'left', field: 'business_unit__code' },
    { title: 'Agreement Number', align: 'left', field: 'number' },
    { title: 'Is Delta', align: 'left', field: 'delta' },
    { title: 'Country', align: 'left', field: 'country__name' },
    { title: 'Partner', align: 'left', field: 'partner__legal_name' },
    { title: 'Implementer Number', align: 'left', field: 'partner__number' },
    { title: 'Budget', align: 'left', field: 'budget' },
    { title: 'Installments Paid', align: 'left', field: 'installments_paid' },
    { title: 'Description', align: 'left', field: 'description' },
    { title: 'Implementer Type', align: 'left', field: 'partner__implementer_type__implementer_type' },
    { title: 'ProjectStart Date', align: 'left', field: 'start_date' },
    { title: 'ProjectEnd Date', align: 'left', field: 'end_date' },
    { title: 'Agreement Type', align: 'left', field: 'agreement_type' },
    { title: 'Agreement Date', align: 'left', field: 'agreement_date' },
    { title: 'Operation', align: 'left', field: 'operation__code' },
    { title: 'Reason', align: 'left', field: 'pre_sense_check__reason' },
  ],
  error: 'Error',
  success: 'Settings Applied !',
  paIncluded: 'PA included in the Preliminary List',
  totalPA: 'Total PA',
  budget: 'Total Budget of the PAs included',
  totalBudget: 'Total Budget of all the PAs',
  update_risk_rating_bg: "update_risk_rating_bg",
  totalError: "Sum of all settings parameter should be exactly 1"
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  statsRoot: {
    flexGrow: 1,
    margin: theme.spacing(3),
    width: '40%',
    marginLeft: 0
  },
  paper: {
    color: theme.palette.text.secondary,
    minHeight: 150
  },
}))
const PreliminaryList = props => {
  const {
    preliminary,
    loadPreliminary,
    loadPAExcludedList,
    loading,
    items,
    totalCount,
    query,
    loadSettings,
    settings,
    editSettings,
    cycle,
    loadBgJob,
    finalized,
    successReducer,
    loadCycle,
    editProject,
    loadLatestCycle,
    loadingLatestCycle,
    loadActivityList,
    loadUser,
    activities,
    totalActivityCount,
    loadingActivity,
    stats,
    loadStats,
    reset,
    bgParams,
    postError
  } = props

  const classes = useStyles()
  const [params, setParams] = useState({ page: 0 })
  const [settingmodal, showModal] = useState({ show: false })
  const [selected, setSelected] = React.useState([]);
  const [key, setKey] = useState('default')
  const [startMonitoring, shouldStartMonitoring] = useState(false)
  const [formData, setFormData] = useState({})
  const [allSelected, updateSelected] = useState(false)
  const [editSelected, updateEditSelected] = useState(false)
  const [delta, setDelta] = useState(false)
  var date = new Date()

  const [sort, setSort] = useState({
    field: '',
    order: 'asc'
  })
  const [year, setYear] = useState(null)
  const currentYear = new Date().getFullYear()

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  useEffect(() => {
    loadUser();
  }, [])


  useEffect(() => {
    const { pathName } = props;
    if (query.year) {
      setYear(query.year)
      loadCycle(query.year)
      loadPreliminary(query);
      loadActivityList(query.year, { page: 1 })
      loadSettings({ year: query.year })
      loadStats(query.year)
    }
    if ("year" in query === false) {
      setDelta(false);
      setYear(currentYear)
      history.push({
        pathname: pathName,
        query: R.merge(query, {
          page: 1,
          year: currentYear,
          status: "pre",
        })
      })
    }
  }, [query]);

  const handleChange = (e, v) => {
    setFormData({
      ...formData,
      [e.target.name]: v
    })
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }))
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ["asc", "desc"])[0]
    });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      })
    });
  }

  function onDeltaChange(e) {
    setDelta(e.target.checked)
  }

  // const parseReasons = (reasons) => {
  //   let res = ''
  //   if (reasons && reasons.length > 0) {
  //     for (let i = 0; i < reasons.length; i++) {
  //       res = res + reasons[i] + ','
  //     }
  //     res = res.slice(0, -1)
  //   }
  //   return res
  // }

  const handleSearch = (values) => {
    const { pathName } = props
    const { partner, year, reasons, view_items, business_unit__code, number, } = values

    setParams({ page: 0 })
    setYear(year)
    // const parsedReasons = parseReasons(reasons)
    const ViewItems = 'pre__'.concat(view_items)
    // const ViewItems = view_items
    // console.log("PARSED REASONS", parsedReasons, reasons)
    if (!reasons) {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          view_items: ViewItems,
          year,
          business_unit__code,
          number,
          status: "pre",
          delta: delta ? delta : undefined,
        }
      })
    } else {
      history.push({
        pathname: pathName,
        query: {
          page: 1,
          partner,
          view_items: ViewItems,
          year,
          business_unit__code,
          number,
          reason: reasons,
          status: "pre",
          delta: delta ? delta : undefined,
        }
      })
      console.log("QUERYIS", query)
    }
  }

  function handleSettingsModal() {
    reset();
    setFormData();
    (showModal({ show: true }))
  }

  function handleClose() {
    reset();
    showModal({
      show: false
    })
  }

  const handleCloseDialog = () => {
    updateEditSelected(false)
    setSelected([])
    setKey(date.getTime())
  }

  const handleSelectAllClick = (allSelected) => {
    if (allSelected) {
      const newSelecteds = items && items.map((item) => item);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };


  function handleSubmit(values) {
    let body = {}

    const { query } = props

    const { status, view_items, year, reason, business_unit__code, number, partner } = query;

    console.log("PROJECTS+>", selected)
    body["list_type"] = "PRE";
    body["action_type"] = values.include_project;
    body["comment"] = values.comment;
    if (!allSelected) {
      body["filter_params"] = null;
      const agreements = selected.map((agreement) => agreement.id);
      body["agreements"] = agreements

    }
    else {
      body["agreements"] = null
      const filter_params = { status, view_items, year, reason: reason, business_unit__code, number, partner }
      body["filter_params"] = filter_params
    }

    return editProject(year, body).then(() => {
      handleCloseDialog()
      loadPreliminary(query);
    }).catch((error) => {
      if (error.validationErrors) {
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error,
        })
      } else {
        postError(error, messages.error)
      }
    })
  }

  function handleEditSettings(values) {
    const { audit_perc, cash_perc, emergency_perc, oios_perc, procurement_perc, risk_threshold, staff_cost_perc, field_assessment_perc } = values
    let body = {}
    let callBg = true
    if (!formData || Object.keys(formData).length === 0) {
      showModal({
        show: false
      })
      return;

    }
    else if (parseFloat(audit_perc) + parseFloat(cash_perc) + parseFloat(emergency_perc) + parseFloat(oios_perc) + parseFloat(procurement_perc) + parseFloat(staff_cost_perc) + parseFloat(field_assessment_perc) != 1) {
      return postError('error', messages.totalError)
    }
    else {
      if (Object.keys(formData).length === 1 && Object.keys(formData).includes("risk_threshold")) {
        callBg = false
      }
      for (let [key, value] of Object.entries(formData)) {
        body[`${key}`] = `${value}`
      }
    }
    return editSettings(year, body).then((response) => {
      successReducer(messages.success)
      { callBg && loadBgJob({ "pending_task": messages.risk_rating_update }) }
      handleClose()
      shouldStartMonitoring(callBg)
      loadPreliminary(query);
      loadSettings({ year: query.year })
    }).catch((error) => {
      postError(error, error.response && error.response.data.risk_threshold || messages.error)
      throw new SubmissionError({
        _error: messages.error
      })
    })
  }
  function resetFilter() {
    const { pathName } = props

    setParams({ page: 0 })
    setDelta(false)
    history.push({
      pathname: pathName,
      query: {
        page: 1,
        delta: undefined,
      },
    })
  }

  return (
    <React.Fragment>
      {/* <BGTaskLoader
        pendingTaskName={messages.risk_rating_update}
        shouldStartMonitoring={shouldStartMonitoring}
      /> */}
      <div style={{ position: 'relative', paddingBottom: 16, marginBottom: 16 }}>
        <Title show={false} title={messages.title} />
        <div className={classes.root}>
          <ListLoader loading={loading}>
            <ProjectsFilter
              onSubmit={handleSearch}
              settingsClicked={handleSettingsModal}
              loading={loadingLatestCycle}
              year={year}
              setKey={setKey}
              headers={headers}
              resetFilter={resetFilter}
              delta={delta}
              onDeltaChange={onDeltaChange}
            />
          </ListLoader>

          {(!finalized && totalCount) ? <ButtonGroup
            startMonitoring={startMonitoring}
            year={year}
            cycle={cycle}
            updateEditSelected={updateEditSelected}
            editSelected={editSelected}
            headers={headers}
            handleClick={handleClick}
            allSelected={allSelected}
            handleSelectAllClick={handleSelectAllClick}
            updateSelected={updateSelected}
            settingsClicked={handleSettingsModal}
            selected={selected}
          />
            : <div style={{ margin: 32 }}></div>}
          {
            (items && items.length == 0 && !loading) ?
              <NoDataTable year={year} />
              :
              <PreliminaryTable
                messages={messages}
                allSelected={allSelected}
                sort={sort}
                loading={loading}
                totalItems={totalCount}
                page={params.page}
                handleChangePage={handleChangePage}
                handleClick={handleClick}
                handleSort={handleSort}
                key={key}
                year={year}
                handleSubmit={handleSubmit}
                settings={settings}
                selected={selected}
                updateEditSelected={updateEditSelected}
                editSelected={editSelected}
                setSelected={setSelected}
                handleCloseDialog={handleCloseDialog}
                pendingTaskName={messages.update_risk_rating_bg}
                shouldStartMonitoring={shouldStartMonitoring}
                startMonitoring={startMonitoring}
              />}
          {/* <ActivityTable
            items={activities}
            count={totalActivityCount}
            loading={loadingActivity}
            loadData={loadActivityList}
            year={year}
          /> */}
          {(preliminary && preliminary.length > 0) && <div className={classes.statsRoot}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <div style={{ marginBottom: 30 }}>
                    <div style={{ margin: 24 }}>
                      <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light', paddingTop: 20 }}>
                        {messages.paIncluded}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold' }}>{_.get(stats, ['summary_stats', 'included'], 'NA')}</span>
                      </p>
                      <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light' }}>
                        {messages.totalPA}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold' }}>{_.get(stats, ['summary_stats', 'included'], '0') + _.get(stats, ['summary_stats', 'excluded'], '0')}</span>
                      </p>
                      <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light' }}>
                        {messages.budget}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold' }}>${formatMoney(_.get(stats, ['budget_stats', 'included'], 'NA'))}</span>
                      </p>
                      <p style={{ textAlign: 'left', fontSize: 16, fontWeight: 'light' }}>
                        {messages.totalBudget}: <span style={{ float: 'right', color: '#344563', fontSize: 14, fontWeight: 'bold', paddingBottom: 24 }}>${formatMoney(_.get(stats, ['budget_stats', 'included'], '0') + _.get(stats, ['budget_stats', 'excluded'], '0'))}</span>
                      </p>
                    </div>
                  </div>
                </Paper>
              </Grid>
            </Grid>
          </div>}
        </div>
        <div style={{ minWidth: 750 }}>
          <SettingModal
            open={settingmodal.show && settings}
            onSubmit={handleEditSettings}
            handleClose={handleClose}
            handleChange={handleChange}
            settings={settings}
          />
        </div>

      </div>
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  preliminary: state.projectList.project,
  totalCount: state.projectList.totalCount,
  loading: state.projectList.loading,
  settings: state.settings.details,
  cycle: state.cycle.details,
  items: state.projectList.project,
  finalized: state.cycle.finalized,
  latestCycle: state.latestCycle.details,
  loadingLatestCycle: state.latestCycle.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  activities: state.activityList.activities,
  totalActivityCount: state.activityList.totalCount,
  loadingActivity: state.activityList.loading,
  stats: state.preliminaryStats.stats,
  // bgParams: state.bgParams.details
})

const mapDispatch = dispatch => ({
  loadPreliminary: (params) => dispatch(loadProjectList(params)),
  loadSettings: (params) => dispatch(loadSettings(params)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  loadLatestCycle: (params) => dispatch(loadLatestCycle(params)),
  loadUser: () => dispatch(loadUser()),
  editProject: (id, body) => dispatch(editProject(id, body)),
  editSettings: (year, body) => dispatch(editSettings(year, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editSettings', message)),
  loadPAExcludedList: (year, params) => dispatch(loadPAExcludedList(year, params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  loadStats: (year, params) => dispatch(loadPreliminaryStats(year, params)),
  loadBgJob: (params) => dispatch(loadBgJob(params)),
  reset: () => dispatch(reset('settingModal'))
})

const connectedPreliminaryList = connect(mapStateToProps, mapDispatch)(PreliminaryList)
export default withRouter(connectedPreliminaryList)
