import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import ListLoader from '../../common/listLoader'
import TablePaginationActions from '../../common/tableActions'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import EditMessage from './messageEdit';
import { SubmissionError } from 'redux-form';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import {editMessage} from '../../../reducers/projects/messagesReducer/editMessage'
import { loadMessages } from '../../../reducers/projects/messagesReducer/loadMessage';


const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
    minHeight: 600
  },
  tableRow: {
    borderBottom: '1px solid #E9ECEF'
  },
  fab: {
    margin: theme.spacing(1),
  },
  tableData: {
    padding: 8
  },
  icon: {
    margin: 8,
    '&:hover': {
      cursor: 'pointer'
    }
  }
}))

const rowsPerPage = 10
function MessageList(props) {
    const {
      messages,
      totalCount,
      loading,
      page,
      membership,
      handleChangePage,
      deleteMessage,
      loadMessages,
      editMessage,
      query,
      postError,
      rowId
    } = props
    
    const classes = useStyles()
    const [open, setOpen] = React.useState(false);    
  const [msgs, setMessage] = React.useState({});

    function handleClose() {
      setOpen(false);
    }

    function handleClickOpen(item) {
      setMessage(item);
      setOpen(true)
    }
    
    function handleEdit(values) {
      const {query} = props;
      const body = {
        message: values.message
      }
      return editMessage(rowId, msgs.id, body).then(() => {
        loadMessages(rowId, query);
        handleClose();
      })
      .catch((error) => {
        postError(error, messages.error);
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error,
        });
      });
    }
    return (
      <Paper className={classes.root}>
        <ListLoader loading={loading}>
          <Table className={classes.table}>
          <TableBody>
              {messages.map(msg => (
                <TableRow key={msg.email} className={classes.tableRow}>
                  <td className={classes.tableData}>
                  <div>
                        <p>{msg.message}</p>
                        <p><b>Author: </b>{msg.user.fullname} ({msg.user.email})</p>
                        <b>Date: </b>{msg.created_date}
                        {(membership.id == msg.user.id) ? <span style={{float: 'right'}}>
                          <EditIcon onClick={() => handleClickOpen(msg)} className={classes.icon}/>
                          <DeleteIcon onClick={() =>deleteMessage(rowId, msg.id)} className={classes.icon} /></span> : null}
                    </div>
                  </td>
                </TableRow>))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={5}
                  count={totalCount}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
          <EditMessage handleClose={handleClose} open={open} message={msgs.message} onSubmit={handleEdit}/>
        </ListLoader>
      </Paper>
    )
  }
  
  const mapStateToProps = (state, ownProps) => ({
    query: ownProps.location.query,
    pathName: ownProps.location.pathname,
  })
  
  const mapDispatchToProps = (dispatch) => ({
    loadMessages: (id, params) => dispatch(loadMessages(id, params)),
    editMessage: (rowId, id, body) => dispatch(editMessage(rowId, id, body)),
    postError: (error, message) => dispatch(errorToBeAdded(error, 'editMessages', message)),
  })
  
  const connected = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(MessageList)
  
  export default withRouter(connected)
  