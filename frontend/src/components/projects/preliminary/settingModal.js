import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import { buttonType } from '../../../helpers/constants'
import CustomizedButton from '../../common/customizedButton';


const BootstrapInput = withStyles(theme => ({
  input: {
    borderRadius: 20,
    backgroundColor: '#F0F2F7',
    border: '1px solid #E3E8F0',
    fontSize: 14,
    width: 'auto',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 20,
      borderColor: '#E3E8F0',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  form: {
    display: 'flex',
    flexDirection: 'row',
    margin: 'auto',
    width: 'fit-content',
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  header: {
    color: '#606060',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 18
  },
  gridCol: {
    marginRight: 30
  },
  heading: {
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    paddingBottom: 8
  },
  noHeading: {
    paddingTop: 20
  },
  input: {
    paddingBottom: 15
  },
  formControl: {
    minWidth: 750,
  },
}));

const validate = values => {
  const errors = { settings: {} }
  const requiredFields = [
    'risk_threshold',
    'procurement_perc',
    'cash_perc',
    'emergency_perc',
    'staff_cost_perc',
    'audit_perc',
    'oios_perc',
  ]
  requiredFields.forEach(field => {
    if (values.settings && !values.settings[field]) {
      errors.settings[field] = 'Required'
    }
    else if (values.settings && isNaN(values.settings[field])) {
      errors.settings[field] = 'Invalid Number'
    }
  })
  return errors
}

function renderTextField({
  label,
  className,
  margin,
  input,
  value,
  readOnly,
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <span>
      <BootstrapInput
        label={label}
        className={className.textField}
        margin={margin}
        InputLabelProps={{
          shrink: true,
        }}
        InputProps={{
          readOnly: readOnly,
        }}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
        fullWidth
      />
      {touched && ((error && <div style={{ color: "red", fontSize: "12px", fontWeight: "bold" }}>{error}</div>))}
    </span>
  );
}

const buttonText = {
  cancel: 'Cancel',
  apply: 'Apply'
}
function SettingModal(props) {
  const classes = useStyles();
  const { open, handleClose, handleSubmit, settings, handleChange } = props;

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth={false}
        maxWidth={'md'}
      >
        <form onSubmit={handleSubmit}>
          <DialogContent>
            <DialogContentText className={classes.form}>
              <Grid xl={4}
                alignItems="flex-start"
              >
                {/* <InputLabel className={classes.header}>Risk Threshold</InputLabel> */}
                <div className={classes.gridCol} style={{ marginTop: 20 }}>
                  <InputLabel className={classes.heading}>Risk Threshold</InputLabel>
                  <div>
                    <Field
                      name='risk_threshold'
                      label='Risk Threshold'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.gridCol} style={{ marginTop: 20 }}>
                  <InputLabel className={classes.heading}>OIOS Cut Off</InputLabel>
                  <div>
                    <Field
                      name='oios_cutoff'
                      label='OIOS Cut Off'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.gridCol} style={{ marginTop: 20 }}>
                  <InputLabel className={classes.heading}>Cost Benefit</InputLabel>
                  <div>
                    <Field
                      name='cost_benefit'
                      label='Cost Benefit'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
              </Grid>
              <Grid xl={4}>
                {/* <InputLabel className={classes.header}>Test Perc.</InputLabel> */}
                <div className={classes.gridCol}>
                  <div className={classes.input} style={{ marginTop: 20 }}>
                    <InputLabel className={classes.heading}>Procurement</InputLabel>
                    <div>
                      <Field
                        name='procurement_perc'
                        defaultValue='procurement_perc'
                        label='Procurement'
                        margin='normal'
                        component={renderTextField}
                        onChange={handleChange}
                        className={classes}
                      />
                    </div>
                  </div>
                </div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>Cash</InputLabel>
                  <div>
                    <Field
                      name='cash_perc'
                      label='Cash'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>Emergency Percentage</InputLabel>
                  <div>
                    <Field
                      name='emergency_perc'
                      label='Emergency Percentage'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>Staff Cost</InputLabel>
                  <div>
                    <Field
                      name='staff_cost_perc'
                      label='Staff Cost'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
              </Grid>
              <Grid xl={4}>
                <div className={classes.noHeading}></div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>Audit Test</InputLabel>
                  <div>
                    <Field
                      name='audit_perc'
                      label='Audit Test'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>OIOS</InputLabel>
                  <div>
                    <Field
                      name='oios_perc'
                      label='OIOS'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
                <div className={classes.input}>
                  <InputLabel className={classes.heading}>Field Assessment</InputLabel>
                  <div>
                    <Field
                      name='field_assessment_perc'
                      label='Field Assessment'
                      margin='normal'
                      component={renderTextField}
                      onChange={handleChange}
                      className={classes}
                    />
                  </div>
                </div>
              </Grid>
            </DialogContentText>
            <div className={classes.buttonGrid}>
              <CustomizedButton
                clicked={handleClose}
                buttonType={buttonType.cancel}
                buttonText={buttonText.cancel} />
              <CustomizedButton
                type="submit"
                buttonType={buttonType.submit}
                buttonText={buttonText.apply} />
            </div>
          </DialogContent>
        </form>
      </Dialog>
    </div>
  );
}

const settingModalForm = reduxForm({
  form: 'settingModal',
  validate,
  enableReinitialize: true,
})(SettingModal);


const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.settings && ownProps.settings.results && ownProps.settings.results[0]
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({

});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(settingModalForm);

export default withRouter(connected);
