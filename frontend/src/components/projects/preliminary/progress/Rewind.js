import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import {buttonType} from '../../../../helpers/constants'
import CustomizedButton from '../../../common/customizedButton';
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { CYCLE_STATUS, BODY_PARAMS, LIST_TYPE } from '../../../../helpers/constants'
import { editCycle } from '../../../../reducers/projects/projectsReducer/editCycle'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import AddCommentDialog from '../../common/AddCommentDialog'
import { reset } from 'redux-form'
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  error: 'Error',
  success: 'Preliminary List Successfully Reverted !',
  required: 'Comment is Required',
  button: {
    PRE: 'REJECT',
    LOAD: 'LOADING...',
  },
  modal: {
    PRE02: {
      title: 'Reject Preliminary List',
      description: 'Please, provide a comment before sending the Preliminary List back to the previous stage'
    },
    PRE03: {
      title: 'Reject Preliminary List',
      description: 'Please, provide a comment before sending the Preliminary List back to the previous stage'
    }
  }
}
const useStyles = makeStyles(theme => ({
}))

const Rewind = (props) => {
  const { cycle, loadCycle, postError, modifyProject, loadActivityList, membership, loading, reset,
    successReducer } = props
  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const [text, setText] = useState({
    actionTitle: '',
    modalTitle: '',
    modalDesc: ''
  })
  const isHQReviewer = membership && membership.type === 'HQ' && membership.role === 'REVIEWER'
  const isHQApprover = membership && membership.type === 'HQ' && membership.role === 'APPROVER'

  useEffect(() => {
    setTitles()
    if (cycle) {
      if (CYCLE_STATUS.PRE02.code === cycle.status.code && isHQReviewer) {
        setShow(true)
      } else if (CYCLE_STATUS.PRE03.code === cycle.status.code && isHQApprover) {
        setShow(true)
      } else {
        setShow(false)
      }
    }
  }, [cycle])

  const handleOpen = () => {
    setDisabled(false)
    setOpen(true)
  }

  const handleClose = () => {
    reset()
    setOpen(false)
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleRewind = ({ comment }) => {
    if(!comment) {
    postError(messages.error, messages.required)
    return
  }
    setDisabled(true)
    if ([CYCLE_STATUS.PRE02.code, CYCLE_STATUS.PRE03.code].includes(cycle.status.code)) {
      let body = {
        status: cycle.status.id - 1,
        list_type: LIST_TYPE.preliminary,
        comment,
        request_type: "reject"
      }
      modifyProject(cycle.year, body)
        .then(() => {
          loadCycle(cycle.year)
          successReducer(messages.success)
          loadActivityList(cycle.year, { page: 1 })
        })
        .catch((error) => { 
          postError(error, messages.error)
        })
        .finally(() => { handleClose() })
    }
  }

  const setTitles = () => {
    if (loading || !cycle) {
      setText({
        ...text,
        actionTitle: messages.button.LOAD
      })
    }
    if (cycle) {
      if (cycle.status.code == CYCLE_STATUS.PRE02.code) {
        setText({
          actionTitle: messages.button.PRE,
          modalTitle: messages.modal.PRE02.title,
          modalDesc: messages.modal.PRE02.description
        })
      } else if (cycle.status.code == CYCLE_STATUS.PRE03.code) {
        setText({
          actionTitle: messages.button.PRE,
          modalTitle: messages.modal.PRE03.title,
          modalDesc: messages.modal.PRE03.description
        })
      }
    }
  }

  return (
    <Fragment>
      {show &&
        <CustomizedButton 
        clicked={handleOpen}
        buttonType={buttonType.cancel} 
        buttonText={text.actionTitle}  />
      }
      <AddCommentDialog
        title={text.modalTitle}
        description={text.modalDesc}
        onSubmit={handleRewind}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Rewind.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  loading: state.cycle.loading,
  membership: state.userData.data.membership
})

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'rewind', message)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)), 
  successReducer: (message) => dispatch(showSuccessSnackbar(message)), 
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Rewind))
