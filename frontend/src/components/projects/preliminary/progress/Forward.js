import React, { Fragment, useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import { buttonType } from '../../../../helpers/constants'
import CustomizedButton from '../../../common/customizedButton';
import { errorToBeAdded } from '../../../../reducers/errorReducer'
import { editCycle } from '../../../../reducers/projects/projectsReducer/editCycle'
import { modifyProject } from '../../../../reducers/projects/projectsReducer/modifyProject'
import { loadCycle } from '../../../../reducers/projects/projectsReducer/getCycle'
import { initializeInterim } from '../../../../reducers/projects/projectsReducer/initializeInterimList'
import AddCommentDialog from '../../common/AddCommentDialog'
import { CYCLE_STATUS, BODY_PARAMS, LIST_TYPE } from '../../../../helpers/constants'
import { loadActivityList } from '../../../../reducers/projects/projectsReducer/getActivityList'
import { reset } from 'redux-form';
import { showSuccessSnackbar } from '../../../../reducers/successReducer';

const messages = {
  error: 'Error',
  success: 'Preliminary List is Successfully Approved',
  button: {
    PRE01: 'SEND FOR REVIEW',
    PRE02: 'SEND FOR APPROVAL',
    PRE03: 'FINALIZE',
    LOAD: 'LOADING...',
  },
  modal: {
    PRE01: {
      title: 'Send Preliminary List for Review',
      description: 'Please, provide a comment before sending the Preliminary List to the next stage'
    },
    PRE02: {
      title: 'Send Preliminary List for Approval',
      description: 'Please, provide a comment before sending the Preliminary List to the next stage'
    },
    PRE03: {
      title: 'Finalize the Preliminary List',
      description: 'Please, provide a comment before finalising the Preliminary List'
    }
  }
}

const useStyles = makeStyles(theme => ({
}))

const Forward = (props) => {
  const {
    cycle,
    reset,
    initializeInterim,
    loadCycle,
    editCycle,
    loading,
    postError,
    modifyProject,
    loadActivityList,
    membership,
    successReducer
  } = props

  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)
  const [show, setShow] = useState(false)
  const [open, setOpen] = useState(false)
  const [text, setText] = useState({
    actionTitle: '',
    modalTitle: '',
    modalDesc: ''
  })
  const isHQPreparer = membership && membership.type === 'HQ' && membership.role === 'PREPARER'
  const isHQReviewer = membership && membership.type === 'HQ' && membership.role === 'REVIEWER'
  const isHQApprover = membership && membership.type === 'HQ' && membership.role === 'APPROVER'

  useEffect(() => {
    setTitles()
    if (cycle) {
      if (CYCLE_STATUS.PRE01.code === cycle.status.code && isHQPreparer) {
        setShow(true)
      } else if (CYCLE_STATUS.PRE02.code === cycle.status.code && isHQReviewer) {
        setShow(true)
      } else if (CYCLE_STATUS.PRE03.code === cycle.status.code && isHQApprover) {
        setShow(true)
      } else {
        setShow(false)
      }
    }
  }, [cycle])

  const handleOpen = () => {
    setDisabled(false)
    setOpen(true)
  }

  const handleClose = () => {
    reset();
    setOpen(false)
  }

  const handleChange = (event, newValue, previousValue, name) => {
    newValue ? setDisabled(false) : setDisabled(true)
  }

  const handleForward = ({ comment }) => {
    setDisabled(true)
    if ([CYCLE_STATUS.PRE01.code, CYCLE_STATUS.PRE02.code, CYCLE_STATUS.PRE03.code].includes(cycle.status.code)) {
      let body = {
        status: cycle.status.id + 1,
        list_type: LIST_TYPE.preliminary,
        comment,
        request_type: "promote"
      }
      modifyProject(cycle.year, body)
        .then(() => {
          loadCycle(cycle.year)
          loadActivityList(cycle.year, { page: 1 })
          successReducer(messages.success)
        })
        .catch((error) => { postError(error, messages.error) })
        .finally(() => { handleClose() })
    } else { postError(messages.error, messages.error) }
    handleClose()
  }

  const setTitles = () => {
    if (loading || !cycle) {
      setText({
        ...text,
        actionTitle: messages.button.LOAD
      })
    }
    if (cycle) {
      if (cycle.status.code == CYCLE_STATUS.PRE01.code) {
        setText({
          actionTitle: messages.button.PRE01,
          modalTitle: messages.modal.PRE01.title,
          modalDesc: messages.modal.PRE01.description
        })
      } else if (cycle.status.code == CYCLE_STATUS.PRE02.code) {
        setText({
          actionTitle: messages.button.PRE02,
          modalTitle: messages.modal.PRE02.title,
          modalDesc: messages.modal.PRE02.description
        })
      } else if (cycle.status.code == CYCLE_STATUS.PRE03.code) {
        setText({
          actionTitle: messages.button.PRE03,
          modalTitle: messages.modal.PRE03.title,
          modalDesc: messages.modal.PRE03.description
        })
      }
    }
  }

  return (
    <Fragment>
      {show &&
        <CustomizedButton
          clicked={handleOpen}
          buttonType={buttonType.submit}
          buttonText={text.actionTitle}
          disabled={loading} />
      }
      <AddCommentDialog
        title={text.modalTitle}
        description={text.modalDesc}
        onSubmit={handleForward}
        handleClose={handleClose}
        handleChange={handleChange}
        open={open}
        disabled={disabled}
        isAccept={true}
      />
    </Fragment>
  )
}

Forward.propTypes = {
}

const mapStateToProps = (state, ownProps) => ({
  cycle: state.cycle.details,
  membership: state.userData.data.membership,
  loading: state.cycle.loading
})

const mapDispatch = dispatch => ({
  editCycle: (year, body) => dispatch(editCycle(year, body)),
  loadCycle: (year, params) => dispatch(loadCycle(year, params)),
  initializeInterim: (year) => dispatch(initializeInterim(year)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'forward', message)),
  modifyProject: (year, body) => dispatch(modifyProject(year, body)),
  loadActivityList: (year, params) => dispatch(loadActivityList(year, params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
  reset: () => dispatch(reset('AddCommentDialogForm'))
})

export default withRouter(connect(mapStateToProps, mapDispatch)(Forward))
