import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import CustomizedButton from '../../common/customizedButton';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import { withRouter } from 'react-router'
import SelectionStage from './progress/SelectionStage'
import { buttonType } from '../../../helpers/constants'
import { loadCsv } from '../../../reducers/projects/projectsReducer/downloadCSVs';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'
import { downloadURI } from '../../../helpers/others';

const useStyles = makeStyles(theme => ({
  buttonGrid: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    marginLeft: - theme.spacing(2),
    display: 'flex',
    minWidth: 100
  },
}))

// let csvItem = [{}]
const buttonText = "Settings"
const exportText = "Export"
const successMessage = "Please check your inbox, An email will be sent with project list attached soon !"
const errors = "Something went wrong. Please try again"
function ButtonGroup(props) {
  const classes = useStyles()
  const { year, postError, csv, startMonitoring, cycle, successReducer, updateSelected, allSelected, handleSelectAllClick,
    settingsClicked, membership, query, totalCount, loadCsvs, updateEditSelected, selected } = props
  const isHQPreparer = membership && membership.type === 'HQ' && membership.role === 'PREPARER'

  const selectText = allSelected ? "De-Select All" : "Select All"
  const editText = "Edit Selected"

  function loadCSVs() {
    let params = {};
    (cycle && cycle.status && cycle.status.id <= 3) ? params.report_type = cycle.status.id : params.report_type = 3;
    loadCsvs(year, params.report_type)
      .then((response) => {
        successReducer(successMessage)
      }).catch((error) => {
        postError(error, error.response && error.response.data || errors)
        throw new SubmissionError({
          _error: errors
        })
      })
  }

  const selectTable = () => {
    handleSelectAllClick(!allSelected);
    updateSelected(!allSelected)
  }

  return (
    <Grid className={classes.buttonGrid} >
      {cycle && cycle.status && cycle.status.id === 1 && isHQPreparer && (
        <CustomizedButton
          buttonType={buttonType.submit}
          disabled={startMonitoring}
          clicked={settingsClicked}
          buttonText={buttonText} />
      )}
      <CustomizedButton
        buttonType={buttonType.submit}
        clicked={(loadCSVs)}
        buttonText={exportText} />
      <SelectionStage />
      {cycle && cycle.status && cycle.status.id === 1 && isHQPreparer &&
        <React.Fragment>
          <CustomizedButton
            buttonType={buttonType.submit}
            clicked={selectTable}
            buttonText={selectText} />


          <CustomizedButton
            buttonType={buttonType.submit}
            clicked={() => updateEditSelected(true)}
            disabled={!selected || selected.length == 0}
            buttonText={editText} />
        </React.Fragment>}
    </Grid>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  membership: state.userData.data.membership,
  totalCount: state.projectList.totalCount,
  csv: state.downloadCSV.csv
})

const mapDispatch = dispatch => ({
  loadCsvs: (year, type) => dispatch(loadCsv(year, type)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'csvError', message)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message)),
})

const connected = connect(mapStateToProps, mapDispatch)(ButtonGroup)
export default withRouter(connected)
