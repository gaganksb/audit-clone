import React from "react";
import { useDropzone } from "react-dropzone";
import CircularProgress from '@material-ui/core/CircularProgress';

export let documents = [];

export default function Plugin(props) {
  const [myFiles, setMyFiles] = React.useState([]);
  const [submitting, setSubmitting] = React.useState(false)
  const onDrop = React.useCallback((acceptedFiles) => {
    documents = [...documents, ...acceptedFiles];
    setMyFiles(documents);
  });
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
  });

  let files = null;

  files = myFiles.map((f) => <li key={f.name}>{f.name}</li>);

  function emptyDoc() {
    setSubmitting(false)
    documents = [];
    setMyFiles(documents);
  }

  function uploadFiles() {
    setSubmitting(true)
    props.item.length != 0
      ? props.updateRepository().then(() => emptyDoc())
      : props.addRepository().then(() => emptyDoc());
  }

  return (
    <section className="container">
      <div
        {...getRootProps({ className: "dropzone" })}
        style={{
          width: "100%",
          height: "170px",
          border: "1px dashed #3D91CD",
          borderRadius: "15px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "#0072BC",
          outline: "none",
          cursor: "pointer",
        }}
      >
        <input {...getInputProps()} />
        <p>Drag & drop your file here</p>
      </div>
      {files ? (
        <aside>
          <h4>Files</h4>
          <ul>{files}</ul>
        </aside>
      ) : null}
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          marginTop: 20,
        }}
      >
        <button
          style={{
            marginLeft: 25,
            border: "none",
            backgroundColor: "#0072BC",
            outline: "none",
            borderRadius: 5,
            padding: "8px 30px",
            color: "white",
            cursor: "pointer",
            height: "40px"
          }}
          onClick={emptyDoc}
        >
          Cancel
        </button>

        <button
          style={{
            marginLeft: 25,
            border: "none",
            backgroundColor: "#0072BC",
            outline: "none",
            borderRadius: 5,
            padding: "8px 30px",
            color: "white",
            cursor: "pointer",
            height: "40px"
          }}
          onClick={uploadFiles}
        >
          {submitting ? (
            <CircularProgress
              color="white"
              style={{
                width: "11px",
                height: "11px",
                marginLeft: "5px"
              }}
            />
          ) : "Save"}
        </button>
      </div>
    </section>
  );
}
