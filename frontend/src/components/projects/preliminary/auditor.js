import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  typography: {
    marginLeft: theme.spacing(7),
    color: '#606060',
    fontWeight: 'bold',
  },
  typographybold: {
    marginLeft: theme.spacing(7),
    color: '#0072bc',
    fontWeight: 'bold',
  },
  heading: {
    paddingLeft: 24,
    padding: '10px 20px',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    display: 'flex',
    justifyContent: 'space-between'
  },
  columns: {
    '& > div': {
      borderRight: 'none !important',
      '&:nth-child(2n)': {
        paddingBottom: 15
      }
    },
    '& > div > p': {
      fontSize: 14
    }
  },
  column: {
    borderRight: '2px solid #006FBD',
    '& > div:first-child': {
      height: 80
    },
    '& > div:nth-child(2)': {
      height: 80
    }
  },
  filterContainer: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      width: 20,
      height: 20,
      marginRight: 20,
      cursor: 'pointer'
    }
  },
  contentContainer: {
    padding: '10px 20px',
    '& > div': {
      '& > div': {
        paddingBottom: 4,
        paddingRight: 15,
        borderRight: '1px solid #E9ECEF',
      }
    },
  }
}));

export const Auditor = props => {
  const classes = useStyles();

  return (
    <div>
      <div className={classes.heading}>
        <h3>Auditor Details</h3>
        <div className={classes.filterContainer}>
          <svg version="1.1" id="Capa_1" x="0px" y="0px" width="459px" height="459px" viewBox="0 0 459 459" xmlSpace="preserve"><g><g id="filter"><path fill="#344563" d="M178.5,382.5h102v-51h-102V382.5z M0,76.5v51h459v-51H0z M76.5,255h306v-51h-306V255z"/></g></g></svg>
          <svg width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path fill="#344563" d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/></svg>
        </div>
      </div>

      <Grid container spacing={3} className={classes.contentContainer}>
        <Grid item xs={3}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Auditor Firm</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Address 1 (Audit Firm)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Address 2 (Audit Firm)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>City (Audit Firm)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Country (Audit Firm)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Report Date</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Date Received</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold} >Audit Opinion</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={3} className={classes.column}>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Comments (Audit)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
            <Typography className={classes.typographybold}>Comments (FCS)</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Audit Deadline</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Typography className={classes.typographybold}>Status</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.typography}>TBD</Typography>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={6} className={classes.columns}>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>Location of Project Documemts for Auditor's Field Visit (Name of City and Country)</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>Location of Project Implementation Site (Name of City and Country)</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>E-mail of UNHCR Audit Focal Person</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>E-mail of UNHCR Alternate Audit Focal Person</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>E-mail & Telephone Number of Partner's Audit Focal Person</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typographybold}>E-mail & Telephone Number of Partner's Alternate Audit Focal Person</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className={classes.typography}>TBD</Typography>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
};
