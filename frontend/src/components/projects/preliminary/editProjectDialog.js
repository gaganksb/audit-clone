import React from 'react'
import CustomizedButton from '../../common/customizedButton'
import { buttonType } from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import InputLabel from '@material-ui/core/InputLabel';
import { makeStyles } from '@material-ui/core/styles'
import { Field, reduxForm } from 'redux-form'
import Grid from '@material-ui/core/Grid'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { connect } from 'react-redux'
import { renderRadioField } from '../../../helpers/formHelper';
import Radio from '@material-ui/core/Radio';
import { textArea } from '../../common/renderFields'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    container: {
        display: 'flex',
    },

    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: 18,
        paddingLeft: 14
    },
    menu: {
        padding: 8,
        '&.Mui-selected': {
            backgroundColor: '#E5F1F8',
            borderStyle: 'hidden'
        },
        '&:hover': {
            backgroundColor: '#E5F1F8',
            borderStyle: 'hidden'
        }
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    paper: {
        color: theme.palette.text.secondary,
    },
    fab: {
        margin: theme.spacing(1),
        borderRadius: 8
    },
    radio: {
        marginLeft: theme.spacing(2),
    },
    '.MuiTypography-root': {
        font: 12,
        color: 'red',
        marginLeft: -12
    },
    radioButton: {
        paddingRight: '3px !important',
        '&.Mui-checked': {
            color: '#0072BC'
        },
        '&.MuiTypography-root': {
            font: 12,
            color: 'red',
            marginLeft: -12
        },
    },
    formControl: {
        marginTop: 2,
        marginRight: theme.spacing(1),
        width: '100%',
    },
    autoCompleteField: {
        marginTop: 2,
        marginRight: theme.spacing(1),
        width: '100%',
        display: "inline-block"
    },
    label: {
        fontSize: '12px'
    },
    inputLabel: {
        fontSize: 14,
        color: '#344563',
        marginTop: theme.spacing(3),
        marginBottom: 4
    },
    value: {
        fontSize: '16px',
        color: 'black'
    },
    commentLabel: {
        fontSize: '12px',
        color: 'grey'
    },
    buttonGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        minWidth: 54,
        marginTop: 20,
        width: '100%'
    },
}))

const validate = values => {
    const errors = {};
    const requiredFields = ["comment", "include_project"];
    requiredFields.forEach(field => {
        if (!values[field] || values[field] === undefined) {
            errors[field] = "Required";
        }
    });
    return errors;
};

function EditDialog(props) {
    const {
        open, handleClose, handleSubmit, key
    } = props;

    const classes = useStyles()
    const [score, setScore] = React.useState('1')
    const [comment, setComment] = React.useState('')
    const radioOptions = [{ label: 'Include Projects in the Preliminary List', value: 'include' }, { label: 'Exclude Projects in the Preliminary List', value: 'exclude' }]

    const messages = {
        submit: "Submit",
        comment: 'Comment',
        cancel: "Cancel"
    }

    const defaultValue = {
    }

    return (
        <div className={classes.root}>
            <Dialog
                open={open}
                onClose={handleClose}

                maxWidth={'xl'}
            // fullWidth
            >
                <DialogTitle>{"Edit Selected Project"}</DialogTitle>
                <Divider />
                <DialogContent>
                    <DialogContentText>
                        <form className={classes.container} onSubmit={handleSubmit}>
                            <Grid
                                container
                                direction="column"
                                justify="flex-start"
                                alignItems="flex-start"
                            >
                                <Field
                                    name="include_project"
                                    component={renderRadioField}
                                    className={classes.radioButton}
                                    margin='normal'
                                    key={key}
                                >
                                    {radioOptions.map((option, index) => (
                                        <FormControlLabel
                                            key={index}
                                            value={option.value}
                                            control={<Radio className={classes.radioButton} />}
                                            label={option.label}
                                            labelPlacement="end"
                                        />
                                    ))}
                                </Field>

                                <InputLabel className={classes.inputLabel}>Comment</InputLabel>
                                <Field
                                    name='comment'
                                    fullWidth
                                    label={messages.comment}
                                    className={classes.textField}
                                    component={textArea}
                                />
                                <Grid className={classes.buttonGrid}>
                                    <CustomizedButton
                                        clicked={handleClose}
                                        buttonType={buttonType.cancel}
                                        buttonText={messages.cancel}
                                    />
                                    <CustomizedButton buttonText={messages.submit} buttonType={buttonType.submit}
                                        type="submit" />
                                </Grid>
                            </Grid>
                        </form>
                    </DialogContentText>
                </DialogContent>
            </Dialog>
        </div>
    )
}

const editAssessment = reduxForm({
    form: 'editProjects',
    validate,
    enableReinitialize: true,
    destroyOnUnmount: true,
    shouldValidate: () => true,
})(EditDialog)

const mapStateToProps = (state, ownProps) => {

}

const mapDispatchToProps = dispatch => ({
})



export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(editAssessment)