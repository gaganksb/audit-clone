import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import Plugin, { documents } from './dropzone';
import Snackbar from '@material-ui/core/Snackbar';
import { uploadFile } from '../../../reducers/documents/documentUpload';
import { loadFile } from '../../../reducers/documents/patchDocument';
import { connect } from 'react-redux';
import { loadDocumentList } from '../../../reducers/documents/documentList';
import DeleteIcon from '@material-ui/icons/Delete'
import {deleteDocumentRequest} from '../../../reducers/documents/deleteDocument';
import {downloadURI} from '../../../helpers/others'
import DeleteDialog from './docDelete';

const messages = {
  tableHeader: [
    {title: ' ', align: 'left'},
    { title: 'Document Type', align: 'left' },
    { title: 'Name', align: 'left' },
    { title: 'Document Owner', align: 'left' },
    { title: 'Email', align: 'left' },
    { title: 'Date', align: 'left' }]
};



const useStyles = makeStyles(theme => ({
  documentWrapper: {
    display: 'flex',
    paddingBottom: 20
  },
  table: {
    maxWidth: '100%',
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    '& th': {
      color: '#a0adbc',
      padding: '18px 25px'
    }
  },
  tableWrapper: {
    overflowY: 'auto',
    height: 300
  },
  tableRow: {
    '& td': {
      color: '#8f9eaf',
      padding: '18px 25px',
      borderBottom: 'solid 2px #f3f5f6'
    }
  },
  tableCell: {
    '&:hover': {
      cursor: 'pointer'
    },
  },
  dropzone: {
    width: "100%",
    height: "20%",
    border: "1px solid black"
  },
  container: {
    display: 'flex',
  },
  heading: {
    paddingLeft: 24,
    padding: '10px 20px',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    display: 'flex',
    justifyContent: 'space-between'
  },
  filterContainer: {
    display: 'flex',
    alignItems: 'center',
    '& > svg': {
      width: 20,
      height: 20,
      marginRight: 20,
      cursor: 'pointer'
    }
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    fontSize: '12px'
  },
  saveDocContainer: {
    padding: '0 30px'
  },
  saveDocBottom: {
    marginTop: 20
  },
  selectorWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  typeSelector: {
    textAlign: 'center',
    '& > p': {
      color: '#0072BC',
      margin: '0 0 3px',
      fontSize: 14,
      fontWeight: 500,
    }
  },
  fileSelector: {
    height: 45,
    width: 250,
    display: 'flex',
    '& > div:first-child': {
      width: '62%',
      padding: '12px 15px',
      border: 'solid 1px #e3e8f0',
      borderRadius: '30px 0 0 30px',
      backgroundColor: '#f0f2f7',
      '& > input': {
        outline: 'none',
        border: 'none',
        color: '#8e9daf',
        fontSize: 14,
        background: 'none'
      }
    },
    '& > div:last-child': {
      width: '38%',
      padding: '12px 15px',
      color: 'white',
      textAlign: 'center',
      borderRadius: '0 30px 30px 0',
      backgroundColor: '#0072BC',
      border: 'solid 1px #0072BC',
      cursor: 'pointer'
    }
  },
  btnWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 20,
    '& > button': {
      marginLeft: 25,
      border: 'none',
      backgroundColor: '#0072BC',
      outline: 'none',
      borderRadius: 5,
      padding: '8px 30px',
      color: 'white',
      cursor: 'pointer'
    }
  },

  selectWrapper: {
    '&:before': {
      borderBottom: 'none',
    },
    '&:after': {
      borderBottom: 'none',
    },
    '&:hover': {
      borderBottom: 'none',
      '&:before': {
        borderBottom: 'none !important',
      },
    },
  },
  selectRoot: {
    width: 160,
    height: 45,
    border: 'solid 1px #e3e8f0',
    borderRadius: 30,
    outline: 'none',
    backgroundColor: '#f0f2f7'
  },
  select: {
  },
  selectMenu: {
    background: 'none !important',
    padding: '12px 15px',
    color: '#8e9daf',
    fontSize: 14,
    textAlign: 'left',
    '&:focus': {
      background: 'none !important',
    }
  },
  selectOutlined: {
  },
  selectIcon: {
    fill: '#0072BC',
    marginRight: 10
  },

  docTypeWrapper: {
    display: 'flex',
    alignItems: 'center',
    '& > div': {
      position: 'relative',
      '& > svg': {
        width: 28,
        transform: 'rotateY(150deg)'
      },
      '& > span': {
        position: 'absolute',
        fontSize: 6,
        color: 'white',
        marginLeft: -20,
        marginTop: 15
      }
    },
  }
}));

const rowsPerPage = 10;

const DocType = ({ type, classes }) => {
  let color = 'white';
  if (type === 'pdf') {
    color = '#ff4141';
  } else if (type === 'docx' || type === 'doc') {
    color = '#0070bf';
  } else if (type === 'xlsx' || type === 'xls' || type === 'csv') {
    color = '#3cc480';
  } else if (type === 'pptx' || type === 'ppt') {
    color = '#ff4141';
  }
  return (
    <div className={classes.docTypeWrapper}>
      <div>
        <svg fill={color} className="MuiSvgIcon-root" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
          <path d="M6 2c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6H6zm7 7V3.5L18.5 9H13z" />
        </svg>
        <span>{type === 'Word' ? 'DOC' : type === 'Excel' ? 'XLS' : type}</span>
      </div>
      <span>{type}</span>
    </div>
  );
};


function Documents(props) {

  const { uploadFile, id, files, loadDocumentList, loadFile, membership, deleteDocumentRequest } = props;
  const classes = useStyles();
  const [errorMsg, updateErrorMsg] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [doc, setDoc] = React.useState({});


  React.useEffect(() => {
    loadDocumentList(id);
  }, []);

  const uploadDoc = () => {
    const uploadedDoc = []
    documents.map(f => (
      uploadedDoc.push(f)
    ));
    let body = {
      common_file: uploadedDoc,
      agreement: id
    }
    return loadFile(id, files[0].id, body).then(() => loadDocumentList(id)).
    catch((error) => {
      if(error && error.response && error.response.data){
        updateErrorMsg(error.response.data.detail || 'Upload failed!')
      }else{
        updateErrorMsg('Upload failed!')
      }
    });
  }

  function openDelete(item) {
    setOpen(true);
    setDoc(item);
  }

  function handleDialogClose() {
    setOpen(false)
  }

  function deleteDoc() {
    let params ={ common_file_id: doc.common_file_id }
    return deleteDocumentRequest(id, files[0].id, params).then(() => {
      handleDialogClose();
      loadDocumentList(id);
    }).
    catch((error) => {
      updateErrorMsg(error.response.data.detail || 'Delete failed!')
    });
  }

  const addRepository = () => {
    const uploadedDoc = []
    documents.map(f => (
      uploadedDoc.push(f)
    ));
    let body = {
      common_file: uploadedDoc,
      agreement: id
    }
    return uploadFile(body).then(() => loadDocumentList(id)).
    catch((error) => {
      updateErrorMsg(error.response.data.detail || 'Upload failed!')
    });
  }

  const handleClose = () => {
    updateErrorMsg(null)
  }


  return (
    <Grid className={classes.documentWrapper}>
      <Grid item sm={7} className={classes.tables}>
        <div className={classes.heading}>
          <h3>Documents</h3>
        </div>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                {messages.tableHeader.map((item, key) =>
                  <TableCell align={item.align} key={key}>
                    {item.title}
                  </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {files[0] && files[0].common_file.map(item => {
                return(
                  <TableRow key={item.email} className={classes.tableRow}>
                    <TableCell>
                      {(membership.id === item.user.id)?<DeleteIcon onClick = {() =>openDelete(item)} className={classes.tableCell}/>: null}
                      </TableCell>
                    <TableCell className={classes.tableCell} onClick={() => window.open(item.filepath, "_blank")}>
                      <DocType type={(item.filename).split('.').pop()} classes={classes} />
                    </TableCell>
                    <TableCell className={classes.tableCell} onClick={() => downloadURI(item.filepath, item.filename)}>{item.filename}</TableCell>
                    <TableCell>{item.user.fullname}</TableCell>
                    <TableCell>{item.user.email}</TableCell>
                    <TableCell>{item.created_date}</TableCell>
                    </TableRow>
                )
              })
            }
            </TableBody>
                <DeleteDialog handleClose={handleDialogClose} open={open} item={doc} deleteDoc={() =>deleteDoc()}/>            
          </Table>
        </div>
      </Grid>
      <Grid item sm={5} className={classes.tables}>
        <div className={classes.heading}>
          <h3>New Document</h3>
        </div>
        <div className={classes.saveDocContainer}>
          <Plugin updateRepository={uploadDoc} item={files} addRepository={addRepository}/>
          <div>
          </div>
        </div>
      </Grid>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={errorMsg}
        message={errorMsg}
        autoHideDuration={4e3}
        onClose={handleClose}
      />
    </Grid>
    
  );
}

const mapStateToProps = (state, ownProps) => ({
  files: state.documentList.document,  
  membership: state.userData.data,
});

const mapDispatchToProps = dispatch => ({
  uploadFile: (file) => dispatch(uploadFile(file)),
  loadDocumentList: (id) => dispatch(loadDocumentList(id)),
  loadFile: (projectId, id, file) => dispatch(loadFile(projectId, id, file)),
  deleteDocumentRequest: (projectId, docId, params) => dispatch(deleteDocumentRequest(projectId, docId, params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Documents);