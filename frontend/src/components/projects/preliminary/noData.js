import React from 'react';
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'


const useStyles = makeStyles(theme => ({

    paper: {
        color: theme.palette.text.secondary,
    },
    heading: {
        fontWeight: 'bold',
        backgroundColor: '#FFEDF0',
        minHeight: 40,
        paddingLeft: 32,
        marginTop: 'auto',
        marginBottom: 'auto',
        lineHeight: 3
    },
    warning: {
        paddingLeft: 32,
        lineHeight: 3,
        fontWeight: 'bold'
    }
}));

function NoDataTable(props) {

    const classes = useStyles();
    return (
        <Paper className={classes.paper}>
            {props.showMessage ?
                <div className={classes.warning}>{props.message}</div> :
                props.noData ?
                    <div className={classes.warning}>No data found</div>
                    :
                    <React.Fragment>
                        <div className={classes.heading}>Warning</div>
                        <div className={classes.warning}>The Preliminary list for selected year {props.year} is not initialised.</div>
                    </React.Fragment>
            }
        </Paper>
    );
}

export default NoDataTable;