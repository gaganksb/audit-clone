import React, { useState, useEffect } from 'react';
import CenteredTabs from '../../common/fullwidthTabs';
import BudgetTable from './budget/index';
import Messages from './messages';
import Documents from './documentRepo';
import Reports from '../../reports';
import SingleProjectQA from "../quality-assurance/single-project/SingleProjectQA";

const tabs = {
  items: [
    { label: "BUDGET INFORMATION" },
    { label: "MESSAGES" },
    { label: "DOCUMENT REPOSITORY" },
    { label: "REPORTS" },
    { label: "QUALITY ASSURANCE" },
  ],
};

const ProjectDetailsTabs = (props) => {
  const [value, setValue] = useState(0);

  function handleChange(event, value) {
    setValue(value);
  }

  return (
    <div>
      <CenteredTabs tabs={tabs} handleChange={handleChange} value={value} />
      {value === 0 && <BudgetTable id={props.id} />}
      {value === 1 && <Messages id={props.id} />}
      {value === 2 && <Documents id={props.id} agreement={props.agreement} />}
      {value === 3 && (
        <Reports
          id={props.id}
          status={props.status}
          project={props.project}
          user={props.user}
        />
      )}
      {value === 4 && <SingleProjectQA id={props.id} />}
    </div>
  );
};
  
export default ProjectDetailsTabs;
