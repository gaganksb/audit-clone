import React from 'react'
import { reset } from 'redux-form'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import Checkbox from '@material-ui/core/Checkbox';
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import BGTaskLoader from '../../common/bgTaskLoader';
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import ListLoader from '../../common/listLoader'
import TablePaginationActions from '../../common/tableActions'
import { editProject } from '../../../reducers/projects/projectsReducer/editProject'
import { errorToBeAdded } from '../../../reducers/errorReducer'
import { SubmissionError } from 'redux-form'
import { formatMoney } from '../../../utils/currencyFormatter'
import { SENSE_CHECKS, LIST_TYPE } from '../../../helpers/constants'
import IncludeProject from '../common/IncludeProject'
import EditProject from './editProjectDialog'
import ExcludeProject from '../common/ExcludeProject'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3)
  },
  noDataRow: {
    margin: 16,
    width: '100%',
    textAlign: 'center'
  },
  table: {
    minWidth: 500,
    maxWidth: '100%'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    font: 'medium 12px/16px Roboto'
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  heading: {
    padding: theme.spacing(3),
    borderBottom: '1px solid #E9ECEF',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
  },
  tableWrapper: {
    // width: '58.5%',
    width: (window.navigator.userAgent.includes("Windows")) ? '95vw' : '97vw',
    maxHeight: 600,
    overflow: 'auto',
    borderRadius: 6,
    marginRight: '-3px !important'
  },
  tableIncludeRow: {
    backgroundColor: '#F5DADA'
  },
  highRiskRow: {
    backgroundColor: '#ef8588 '
  },
  tableExcludeRow: {
  },
  viewProject: {
    fontSize: 14,
    fontFamily: 'Roboto',
    color: '#344563',
    '&:hover': {
      cursor: 'pointer'
    },
    borderBottom: '1px solid #E9ECEF',
    padding: '10px 26px 10px 24px'
  },
  deletedRow: {
    backgroundColor: 'red'
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
    transform: 'translateY(-1px)'
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableCell: {
    font: '14px/16px Roboto',
    letterSpacing: 0,
    color: '#344563',
    borderBottom: '1px solid #E9ECEF'
  },
  paginationSpacer: {
    flex: 'none'
  },
  paginationCaption: {
    marginRight: theme.spacing(6)
  }
}))

const rowsPerPage = 10
const EXCLUDE = 'EXCLUDE'
const isItemSelected = ''

function CustomPaginationActionsTable(props) {
  const {
    items,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    sort,
    messages,
    postError,
    resetCommentForm,
    editProject,
    cycle,
    year,
    membership,
    handleCloseDialog,
    allSelected,
    pendingTaskName,
    shouldStartMonitoring,
    startMonitoring,
    settings,
    selected,
    handleSubmit,
    setSelected,
    handleClick,
    updateEditSelected,
    editSelected,
    key
  } = props

  const classes = useStyles()
  const isHQPreparer = membership && membership.type === 'HQ' && membership.role === 'PREPARER'
  const tableHeader = isHQPreparer && cycle && cycle.status && cycle.status.id === 1 ? messages.tableHeader : messages.tableHeader.slice(1)


  function handleClose() {
    resetCommentForm()
  }


  function handleExcludePA(values) {
    const { id, comment } = values
    const body = { comment, pre_sense_check: '12' }

    return editProject(id, body).then(() => {
      handleClose()
    }).catch((error) => {
      if (error.validationErrors) {
        throw new SubmissionError({
          ...error.response.data,
          _error: messages.error,
        })
      } else {
        postError(error, messages.error)
      }
    })
  }

  const handleClassName = (item) => {
    if (item && item.pre_sense_check) {
      return classes.tableExcludeRow // the naming convention is wrong
    } else {
      return classes.tableIncludeRow // the naming convention is wrong
    }
  }

  const handleAction = (item) => {
    const include = (
      <TableCell className={classes.viewProject} style={{ verticalAlign: 'bottom' }}>
        <IncludeProject listType={LIST_TYPE.preliminary} project={item} year={year} />
      </TableCell>
    )
    const exclude = (
      <TableCell className={classes.viewProject} style={{ verticalAlign: 'bottom' }}>
        <ExcludeProject listType={LIST_TYPE.preliminary} project={item} year={year} />
      </TableCell>
    )
    if (item && item.pre_sense_check) {
      return exclude
    } else {
      return include
    }
  }

  const handleReason = (item) => {
    if (item && item.pre_sense_check) {
      return item.pre_sense_check.description
    }
    else {
      if (settings && settings.results && settings.results[0] && item.overall_risk_rating && settings.results[0].risk_threshold <= item.overall_risk_rating) {
        return "Project above risk threshold"
      }
      if (settings && settings.results && settings.results[0] && item.overall_risk_rating && settings.results[0].risk_threshold > item.overall_risk_rating) {
        return "Project below risk threshold"
      }
      else if (!item.overall_risk_rating) {
        return "Risk rating not available Project included by default"
      }
    }
  }

  const isSelected = (id) => selected.indexOf(id) !== -1;

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        <Table className={classes.table}>
          <div className={classes.tableWrapper}>
            <div className={classes.heading}>
              {messages.title}
            </div>
            <EditProject open={editSelected} key={key} handleClose={handleCloseDialog} onSubmit={handleSubmit} />
            <TableHead>
              <TableRow className={classes.headerRow}>
                {tableHeader.map((item, key) =>
                  ((cycle && cycle.status && cycle.status.id >= 4) && item.title == '') ?
                    null :
                    <TableCell align={item.align} key={key} className={classes.tableHeader} style={{ position: 'sticky', top: 0, backgroundColor: '#F6F9FC', zIndex: 2 }}>
                      {(item.title == '' || item.title == 'Sense Check') ?
                        item.title : <TableSortLabel
                          active={item.field === sort.field}
                          direction={sort.order}
                          onClick={() => handleSort(item.field, sort.order)}
                        >
                          {item.title}
                        </TableSortLabel>
                      }
                    </TableCell>
                )}
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.map(item => (

                <TableRow
                  onClick={(event) => handleClick(event, item)}
                  role="checkbox"
                  aria-checked={isSelected(item)}
                  tabIndex={-1}
                  key={item.id}
                  className={handleClassName(item)}
                >
                  {cycle && cycle.status && cycle.status.id === 1 && isHQPreparer && !allSelected && (
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={isSelected(item)}
                      />
                    </TableCell>
                  )}
                  {
                    cycle && cycle.status && cycle.status.id === 1 && isHQPreparer && allSelected && <TableCell padding="checkbox">
                      <Checkbox
                        checked
                      />
                    </TableCell>
                  }
                  {startMonitoring === false ? <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.overall_risk_rating}
                  </TableCell> :
                    <TableCell className={classes.viewProject} >
                      <BGTaskLoader
                        pendingTaskName={pendingTaskName}
                        shouldStartMonitoring={shouldStartMonitoring}
                      />
                    </TableCell>}
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {(item.pre_comment) ? item.pre_comment.message : 'N/A'}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.business_unit}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.number}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.delta ? 'Yes' : 'No'}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>
                    {item.country}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.legal_name}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.number}
                  </TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>${formatMoney(item.budget)}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>${formatMoney(item.installments_paid)}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.description}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.partner.implementer_type.implementer_type}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.start_date}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.end_date}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.agreement_type}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.agreement_date}</TableCell>
                  <TableCell className={classes.viewProject}
                    onClick={() => history.push("/projects/" + `${item.id}`)}>{item.operation && item.operation.code}</TableCell>
                  <TableCell
                    className={classes.viewProject}
                    style={{ whiteSpace: "nowrap" }}
                    onClick={() => history.push("/projects/" + `${item.id}`)}
                  >
                    {membership && membership.type === "HQ" && item.reason && item.reason.description}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </div>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={1}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                classes={{
                  spacer: classes.paginationSpacer,
                  caption: classes.paginationCaption
                }}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </ListLoader>
    </Paper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  cycle: state.cycle.details,
  items: state.projectList.project,
  membership: state.userData.data.membership
})

const mapDispatchToProps = (dispatch) => ({
  resetCommentForm: () => dispatch(reset('commentDialog')),
  editProject: (id, body) => dispatch(editProject(id, body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editProject', message))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable)

export default withRouter(connected)
