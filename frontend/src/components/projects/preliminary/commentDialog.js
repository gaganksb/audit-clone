import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import InputBase from '@material-ui/core/InputBase';

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    minWidth: '90%'
    },
  },
  input: {
    marginTop: theme.spacing(1),
    borderRadius: 23,
    position: 'relative',
    backgroundColor: '#F0F2F7',
    border: '1px solid #E3E8F0',
    fontSize: 16,
    width: '98%',
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: theme.palette.primary.main,
    },
  },
}))(InputBase);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  subText: {
      color: '#0072BC',
      font: 'bold 14px/19px Roboto',
      marginTop: 24,
      marginLeft: 14
  },
  buttonGrid: {
    margin: theme.spacing(1),
    marginRight: 0,
    display: 'flex',
    justifyContent: 'flex-end',
    minWidth: 54
  },
  button: {
    margin: theme.spacing(1),
    marginRight: 0,
    minWidth: 94,
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    font: '14px/19px Open Sans',
    fontWeight: 600,
    '&:hover': {
      backgroundColor: '#0072BC'
    }
  },
  dialogTitle: {
    font: 'bold 18px/24px Roboto',
    color: '#606060',

  }
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'comment',
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

function renderTextArea({
  label, 
  className, 
  margin, 
  input, 
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <div style={{width:'100%'}}>
    <BootstrapInput
      style={{width:'100%'}}
      label={label}
      margin="normal"
      variant="outlined"
      variant="filled"
      InputLabelProps={{
        shrink: true
      }}
      multiline={true}
      rows={6}
      rowsMax={10}
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
    />
    {touched && ((error && <div style={{color:"red", fontSize:"12px", fontWeight:"bold"}}>{error}</div>))}
     </div> 
  );
}

function CommentDialog(props) {
  const classes = useStyles();
  const { handleSubmit, handleClose, open, project } = props;

  return (
    
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
        maxWidth={'md'}
      >
        <DialogTitle className={classes.dialogTitle}>{"Comments"}</DialogTitle>
        <DialogContent>
          <form className={classes.container} onSubmit={handleSubmit}>
          <div style={{width:'100%'}}> 
                <div className={classes.subText}>
                    Reason PA Excluded
                </div>
                <Field 
                  name = 'comment'
                  component={renderTextArea}
                />
                <div className={classes.buttonGrid}>
                  <Button variant="contained" className={classes.button} onClick={handleClose} >
                    Cancel
                  </Button>
                  <Button variant="contained" className={classes.button} type="submit">
                    Save
                  </Button>            
                </div> 
              </div>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const commentDialogForm = reduxForm({
  form: 'commentDialog',
  validate,
  enableReinitialize: true,
})(CommentDialog);

const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.project
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(commentDialogForm);

export default withRouter(connected);