import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import R from 'ramda';
import BudgetTable from './budgetTable';
import { loadBudgetList } from '../../../../reducers/projects/budgetReducer/getBudgetList';

const messages = {
    tableHeader: [
      { title: 'Account Type', align: 'left'},
      { title: 'Bgt Year', align: 'left'},
      { title: 'Parent Node Name', align: 'left' },
      { title: 'Account', align: 'left' },
      { title: 'Output', align: 'left' },
      { title: 'Cost Center', align: 'left' },
      { title: 'Goal', align: 'left' },
      { title: 'PPG', align: 'left' },
      { title: 'Situation', align: 'left' },
      { title: 'Site', align: 'left' },
      { title: 'Budget (USD)', align: 'left' }]
  };
  const Budget = (props) => {
    const { budget, loadBudget, loading, totalCount, query, id } = props
    const [params, setParams] = useState({page: 0});  
    useEffect(() => {
        loadBudget(id,query);
      }, [query]);

      
  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }
      return (
        <div>
          <BudgetTable 
            messages={messages} 
            items={budget} 
            loading={loading}
            totalItems={totalCount}  
            loading={loading}
            page={params.page}
            handleChangePage={handleChangePage}
          />

        </div>
      )
    }

    const mapStateToProps = (state, ownProps) => ({
        budget: state.budgetList.budget,
        totalCount: state.budgetList.totalCount,
        loading: state.budgetList.loading,
        query: ownProps.location.query,
        location: ownProps.location,
        pathName: ownProps.location.pathname,
      });
      
      const mapDispatch = dispatch => ({
        loadBudget: (id, params) => dispatch(loadBudgetList(id, params)),
      });
      
      const connectedBudget = connect(mapStateToProps, mapDispatch)(Budget);
      export default withRouter(connectedBudget);