import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderTextField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  }, 
  label: {
    fontSize: '12px'
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  button: {
    marginTop: theme.spacing(1),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    marginBottom: 2
    },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'overall_rating',
    'report_date'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const messages = {
  submit: 'Submit',
  cancel: 'Cancel'
}

function EditDialog(props) {
  const {open, handleClose, handleSubmit, partner} = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{"Edit OIOS Partner"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                
                <div>
                  <p className={classes.label}>Business Unit</p>
                  <p className={classes.value}>{partner && partner.business_unit_code}</p>
                </div>

                <InputLabel className={classes.inputLabel}>Overall Rating</InputLabel>
                <Field 
                  name='overall_rating'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  InputProps={{
                    inputProps: {
                      step: 0.01,
                      min: 0
                    }
                  }}
                />
                <InputLabel className={classes.inputLabel}>Report Date</InputLabel>
                <Field 
                  name='report_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                />
                  <Grid className={classes.buttonGrid} >
                  <CustomizedButton
                    clicked={handleClose}
                    buttonText={messages.cancel}
                    buttonType={buttonType.cancel} />
                  <CustomizedButton
                    type='submit'
                    buttonText={messages.submit}
                    buttonType={buttonType.submit} />
                    </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const editOIOSForm = reduxForm({
  form: 'editOIOSPartner',
  validate,
  enableReinitialize: true,
})(EditDialog);


const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.partner
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(editOIOSForm);

export default withRouter(connected);
