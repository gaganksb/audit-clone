import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadOiosList } from '../../../reducers/projects/oiosReducer/oiosList';
import OIOSFilter from './oiosFilter';
import OIOSTable from './oiosTable';
import AddOIOS from './addOIOS';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';
import R from 'ramda';
import Title from '../../common/customTitle';

const messages = {
  title: "OIOS",
  tableHeader: [
    { title: 'Business Unit', align: 'left', field: 'business_unit' },
    { title: 'Overall Rating', align: 'left', field: 'overall_rating' },
    { title: 'Report Date', align: 'left', field: 'report_date' },
    { title: '', align: 'left' }
  ],
}


const OIOS = props => {
  const { oios, loadOios, loading, totalCount, query, loadBUList, BUList } = props

  const [params, setParams] = useState({ page: 0 });
  const [bu, setBu] = React.useState([])
  const [items, setItems] = useState([]);
  const [key, setKey] = useState('default')
  
  var date= new Date()
  

  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field,
    order: 'desc'
  })

  useEffect(() => {
    loadOios(query);
  }, [query]);

  useEffect(() => {
    setItems(BUList);
  }, [BUList]);

  function goTo(item) {
    history.push(item);
  }

  const handleChange = (e, v) => {
    if (v) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleSort(field, order) {
    const { pathName, query } = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        ordering: order === "asc" ? field : `-${field}`
      }),
    })
  }

  function resetFilter() {
    const { pathName } = props;
    setBu([])
    setKey(date.getTime())

    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  function handleSearch(values) {
    const { pathName, query } = props;
    const { year } = values;
    setParams({ page: 0 });
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        business_unit: bu.code,
        year
      }),
    })
  }

  return (
    <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
      <Title show={false} title={messages.title} />
      <OIOSFilter 
      onSubmit={handleSearch}
      bu={bu}
      key={key}
      setBu={setBu}
      items={items}
      handleChange={handleChange}
       resetFilter={resetFilter} />
      <AddOIOS />
      <OIOSTable
        messages={messages}
        sort={sort}
        items={oios}
        loading={loading}
        totalItems={totalCount}
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  oios: state.oiosList.oios,
  totalCount: state.oiosList.totalCount,
  loading: state.oiosList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  BUList: state.BUListReducer.bu,
});

const mapDispatch = dispatch => ({
  loadOios: params => dispatch(loadOiosList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
});

const connectedOIOS = connect(mapStateToProps, mapDispatch)(OIOS);
export default withRouter(connectedOIOS);