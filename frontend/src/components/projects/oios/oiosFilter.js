import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField } from '../../common/renderFields';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import InputLabel from '@material-ui/core/InputLabel';
import {buttonType, filterButtons} from '../../../helpers/constants';
import CustomizedButton from '../../common/customizedButton';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import { connect } from 'react-redux';


const type_to_search = 'business_unit__code'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 6
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    marginTop: 2,
    width: '90%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  autoCompleteField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '90%',
    marginTop: 2
},
  fab: {
    margin: theme.spacing(1),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
  },
}));

function OIOSFilter(props) {
  const classes = useStyles();
  const {handleSubmit, reset, resetFilter, setBu, handleChange, items, bu, key} = props;


  const handleValueChange = val => {
    setBu(val)
  }
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={5} >
          <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
            <Field
            name='business_unit_code'I
            key={key}
            className={classes.autoCompleteField}
            component={CustomAutoCompleteField}
            handleValueChange={handleValueChange}
            onChange={handleChange}
            items={items}
            comingValue= {bu}
            type_to_search={type_to_search}
          />
        </Grid>
            <Grid item sm={5} >
            <InputLabel className={classes.inputLabel}>Year</InputLabel>
            <Field
              name='year'
              margin='normal'
              type='number'
              component={renderTextField}
              className={classes.textField}
              InputProps={{
                inputProps: {
                  min: 2014,
                }
              }}
            />
          </Grid>
          <Grid item sm={2} className={classes.buttonGrid}>
          <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type="submit" />
                <CustomizedButton buttonType={buttonType.submit}                  
                  reset = {true}
                  clicked = {() => { reset(); resetFilter(); }}
                  buttonText={filterButtons.reset}
                />
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
}

const mapDispatch = dispatch => ({
})

const OIOSFilterForm = reduxForm({
  form: 'oiosFilter',
  enableReinitialize: true,
})(OIOSFilter);

const connected = connect(
  mapStateToProps, mapDispatch
)(OIOSFilterForm)

export default connected