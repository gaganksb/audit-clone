import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import AddOIOSDialog from './addOIOSDialog';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { reset } from 'redux-form';
import { withRouter } from 'react-router';
import { loadBUList } from '../../../reducers/projects/oiosReducer/buList';
import { addNewOIOS } from '../../../reducers/projects/oiosReducer/addOios';
import { SubmissionError } from 'redux-form';
import { loadOiosList } from '../../../reducers/projects/oiosReducer/oiosList';
import R from 'ramda';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  customizedButton: {
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#0072BC'
    }

  }
}));

const ADD_OIOS = 'ADD_OIOS';

const messages = {
  error: 'Error',
  BUERROR : "Please select valid business units from dropdown",
  success: 'Successfully Added !',
  add: 'Add OIOS'
}

function AddOIOS(props) {
  const classes = useStyles();
  const {postError, loadBUList, BUList, postOIOS, loadOios, query, reset, successReducer} = props;
  const [open, setOpen] = useState({ status: false, type: null });
  const [items, setItems] = useState([]);
  const [bu, setBu] = React.useState()
  
  useEffect(() => {
    setItems(BUList);
  }, [BUList]);

  function handleClose() {
    setOpen({ status: false, type: null });
  }

  function handleOpen(action) {
    reset();
    setBu([]);
    setOpen({ status: true, type: action });
  }
  
  function handleSubmit(values) {
    const {report_date, overall_rating, business_unit_code} = values;
    let business_unit = R.find(R.propEq('code', business_unit_code), items);
    if(!business_unit || business_unit.length === 0){
      const error = new SubmissionError({ _error: messages.BUERROR });
      postError(error, messages.BUERROR);
      throw error;
    }
    let body;
    if (business_unit) {
      body = {
        business_unit: business_unit.id,
        report_date,
        overall_rating
      }
    }
    return postOIOS(body).then(() => {
      handleClose();
      loadOios(query);
      successReducer(messages.success)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error,
      });
    });
  }

  function handleChange(e,v) {
    if (v) {
      let params = {
        code: v,
      }
      loadBUList(params);
    }
  }
  return (
    <div className={classes.root}>      
    <span style={{marginLeft: -16}}> 
      <CustomizedButton buttonType={buttonType.submit} 
            buttonText={messages.add} 
            clicked={() => handleOpen(ADD_OIOS)} />
      </span>
      <AddOIOSDialog 
        open={open.status && open.type === ADD_OIOS} 
        onSubmit={handleSubmit} 
        handleChange={handleChange}
        setBu={setBu}
        bu={bu}
        items={items}
        handleClose={handleClose} />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  BUList: state.BUListReducer.bu,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
});

const mapDispatch = dispatch => ({
  postOIOS: body => dispatch(addNewOIOS(body)),
  loadOios: params => dispatch(loadOiosList(params)),
  loadBUList: (params) => dispatch(loadBUList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'addOIOS', message)),
  reset: () => dispatch(reset('addOIOSPartner')),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

const connected = connect(mapStateToProps, mapDispatch)(AddOIOS);
export default withRouter(connected);