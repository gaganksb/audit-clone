import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import Tooltip from '@material-ui/core/Tooltip';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ListLoader from '../../common/listLoader';
import TablePaginationActions from '../../common/tableActions';
import Fab from '@material-ui/core/Fab';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import EditDialog from './editOIOSDialog';
import DeleteDialog from './deleteOIOSDialog';
import { loadOiosList } from '../../../reducers/projects/oiosReducer/oiosList';
import { deleteOIOSPartnerRequest } from '../../../reducers/projects/oiosReducer/deleteOiosPartner';
import { editOIOSPartner } from '../../../reducers/projects/oiosReducer/editOiosPartner';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { SubmissionError } from 'redux-form';
import {reset} from 'redux-form';
import { showSuccessSnackbar } from '../../../reducers/successReducer';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
  tableRow: {
    borderStyle: 'hidden',
  },   
  noDataRow: {
    margin: 16,
    width: '200%',
    textAlign: 'center'
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const DELETE = 'DELETE'
const EDIT = 'EDIT'

const success = {
  delete: 'Successfully Deleted !',
  edit: 'Updated Successfully!'
}

const rowsPerPage = 10

function CustomPaginationActionsTable(props) {
  const {
    items, 
    totalItems, 
    loading, 
    page, 
    handleChangePage, 
    handleSort, 
    messages,
    sort,
    deleteOIOSPartner,
    editOIOSPartner,
    reset,
    successReducer,
    loadOios } = props;
 
  const classes = useStyles();

  function handleDeleteOIOSPartner(value) {
    const { query } = props
    
    return deleteOIOSPartner(value).then(() => {
      loadOios(query);
      handleClose();
      successReducer(success.delete)
    })
  }

  function handleEditOIOSPartner(values) {
    const { id, overall_rating, report_date } = values;
    const body = {overall_rating, report_date };
    const { query, postError } = props

    return editOIOSPartner(id, body).then(() => {
      handleClose();
      loadOios(query);
      successReducer(success.edit)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error,
      });
    });
  }

  const [open, setOpen] = React.useState({ status: false, type: null })
  const [partner, setPartner] = React.useState({});

  function handleClickOpen(item, action) {
    reset();
    setPartner(item);
    setOpen({ status: true, type: action })
  }

  function handleClose() {
    setOpen({ status: false, type: null });
  }

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading} >
        <div className={classes.heading}>
          OIOS
        </div>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead className={classes.headerRow}>
              <TableRow>
                { messages.tableHeader.map((item, key) => 
                  <TableCell align={item.align} key={key} className={classes.tableHeader}>
                    <TableSortLabel
                      active={item.field === sort.field}
                      direction={sort.order}
                      onClick={() => handleSort(item.field, sort.order)}
                    > 
                      {item.title} 
                    </TableSortLabel> 
                  </TableCell>
                  ) }
              </TableRow>
            </TableHead>
            <TableBody>
            {items && items.length ? 
               items.map(item => (
                <TableRow key={item.email}>
                  <TableCell className={classes.tableCell} component="th" scope="row">
                    {item.business_unit_code}
                  </TableCell>
                  <TableCell className={classes.tableCell}>{item.overall_rating}</TableCell>
                  <TableCell className={classes.tableCell}>{item.report_date}</TableCell>
                  <TableCell align='right'>
                  <Tooltip title={"Delete OIOS"}>
                    <Fab size='small' className={classes.fab} onClick={() => handleClickOpen(item, DELETE)} >
                      <DeleteIcon />
                    </Fab>
                    </Tooltip>
                    <Tooltip title={"Edit OIOS"}>
                    <Fab color="primary" size='small' className={classes.fab} onClick={() => handleClickOpen(item, EDIT)} style={{backgroundColor: '#0072BC'}} >
                      <EditIcon />
                    </Fab>
                    </Tooltip>
                  </TableCell>
                </TableRow>))
                 
                 : <div className= {classes.noDataRow}>No data for the selected criteria</div>}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[rowsPerPage]}
                  colSpan={4}
                  count={totalItems}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
          <DeleteDialog handleClose={handleClose} open={open.status && open.type === DELETE} handleDeleteOIOSPartner={handleDeleteOIOSPartner} partner={partner} />
          <EditDialog handleClose={handleClose} open={open.status && open.type===EDIT} partner={partner} onSubmit={handleEditOIOSPartner} />
        </div>
      </ListLoader>
    </Paper>
  );
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
});

const mapDispatchToProps = (dispatch) => ({
  loadOios: params => dispatch(loadOiosList(params)),
  deleteOIOSPartner: (id) => dispatch(deleteOIOSPartnerRequest(id)),
  editOIOSPartner: (id,body) => dispatch(editOIOSPartner(id,body)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'editOIOS', message)),
  reset: () => dispatch(reset('editOIOSPartner')),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomPaginationActionsTable);

export default withRouter(connected);
