import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import { withRouter } from 'react-router';

const type_to_search = 'business_unit__code'

var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    'business_unit_code',
    'overall_rating',
    'report_date'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    marginBottom: 2
    },
  button: {
    marginTop: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },  
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
  autoCompleteField: {
    marginLeft: 2,
    marginRight: theme.spacing(1),
    width: 535,
    marginTop: 14
},
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
}));

const messages = {
  submit: 'Submit',
  cancel: 'Cancel',
  title: 'Create OIOS Partner'
}


function AddOIOSDialog(props) {
  const {open, handleClose, handleChange, bu, setBu, handleSubmit, items, setSelectedBU} = props;
  const classes = useStyles();


  const handleValueChange = val => {
    setBu(val)
  }
  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{messages.title}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                 <InputLabel className={classes.inputLabel}>Business Unit</InputLabel>
                <Field
                  name='business_unit_code'
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChange}
                  items={items}
                  comingValue= {bu}
                  type_to_search={type_to_search}
                />
                
                <InputLabel className={classes.inputLabel}>Overall Rating</InputLabel>
                <Field 
                  name='overall_rating'
                  margin='normal'
                  type='number'
                  component={renderTextField}
                  InputProps={{
                    inputProps: {
                      step: 0.01,
                      min: 0
                    }
                  }}
                />
                <InputLabel className={classes.inputLabel}>Report Date</InputLabel>
                <Field 
                  name='report_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                />
                <Grid className={classes.buttonGrid}>
                  <CustomizedButton
                    clicked={handleClose}
                    buttonText={messages.cancel}
                    buttonType={buttonType.cancel} />
                  <CustomizedButton
                    type='submit'
                    buttonText={messages.submit}
                    buttonType={buttonType.submit} />
                </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const editOIOSForm = reduxForm({
  form: 'addOIOSPartner',
  validate,
  enableReinitialize: true,
})(AddOIOSDialog);


const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(editOIOSForm);

export default withRouter(connected);
