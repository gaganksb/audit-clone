import React, { useState } from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { renderTextField, renderSelectField } from '../../common/renderFields';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';
import { withRouter } from 'react-router';
import InputLabel from '@material-ui/core/InputLabel';

const type_to_search = 'partner__legal_name';

var errors = {}
const validate = values => {
  errors = {}
  const requiredFields = [
    'partner',
    'national_worldwide',
    'granted_date',
    'expiry_date'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  button: {
    marginTop: theme.spacing(1),
  },
  select: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  autoCompleteField: {
    marginRight: theme.spacing(1),
    width: 535,
    marginTop: 14
},
  paper: {
    color: theme.palette.text.secondary,
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
  formControl: {
    minWidth: 160,
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18
    },
}));



const messages = {
  submit: 'Submit',
  cancel: 'Cancel',
  title: 'Create PQP Partner'
}


function AddPQPDialog(props) {
  const {open, handleClose, handleSubmit, items, handleChange, setSelectedPartner, partner } = props;
  const classes = useStyles();

  
const handleValueChange = val => {
  setSelectedPartner(val)
}

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{messages.title}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <InputLabel className={classes.inputLabel}>Partner</InputLabel>
              <Field
                  name='partner'
                  className={classes.autoCompleteField}
                  component={CustomAutoCompleteField}
                  handleValueChange={handleValueChange}
                  onChange={handleChange}
                  items={items}
                  type_to_search={type_to_search}
                  comingValue= {partner}
                  errors={errors}
                />
 
              <InputLabel className={classes.inputLabel}>National Worldwide</InputLabel>
                <Field
                  name='national_worldwide'
                  label='National Worldwide'
                  margin='normal'
                  component={renderSelectField}
                  className={classes.select}
                >
                  <option value={'W'} className={classes.menu}>W</option>
                  <option value={'N'} className={classes.menu}>N</option>
                  <option value={'M'} className={classes.menu}>M</option>
                </Field>
                
                <InputLabel className={classes.inputLabel}>Granted Date</InputLabel>
                <Field 
                  name='granted_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                  className={classes}
                />
                
                <InputLabel className={classes.inputLabel}>Expiry Date</InputLabel>
                <Field 
                  name='expiry_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                  className={classes}
                />
                <Grid className={classes.buttonGrid}>
                  <CustomizedButton
                    clicked={handleClose}
                    buttonText={messages.cancel}
                    buttonType={buttonType.cancel} />
                  <CustomizedButton
                    type='submit'
                    buttonText={messages.submit}
                    buttonType={buttonType.submit} />
                </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const addPQPForm = reduxForm({
  form: 'addPQPPartner',
  validate,
  enableReinitialize: true,
})(AddPQPDialog);


const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = dispatch => ({  
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(addPQPForm);

export default withRouter(connected);
