import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  dialogContentText: {
    marginTop: theme.spacing(3),
  },
  button: {
    marginRight: theme.spacing(1)
  }
}));

function AlertDialog(props) {
  const {open, handleClose, handleDeletePQPPartner, partner} = props;

  const classes = useStyles();
  
const messages ={ 
  yes: 'YES',
  no: 'NO'
}
  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{"PQP Partner Deletion Alert"}</DialogTitle>
        <Divider />
        <DialogContent>
        { (partner.partner) ?<DialogContentText className={classes.dialogContentText} >
            Confirm that you want to delete <b>{partner.partner.legal_name}</b> by clicking on <b>YES</b>
          </DialogContentText> : null}
        </DialogContent>
        <DialogActions className={classes.button}>
        <CustomizedButton 
          clicked={handleClose} 
          buttonText={messages.no} 
          buttonType={buttonType.cancel} />      
          <CustomizedButton 
          clicked={() => handleDeletePQPPartner(partner.id)} 
          buttonText={messages.yes} 
          buttonType={buttonType.submit} />
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default AlertDialog;