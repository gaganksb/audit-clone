import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { browserHistory as history, withRouter } from 'react-router';
import { loadPqpList } from '../../../reducers/projects/pqpReducer/pqpList';
import PQPFilter from './pqpfilter';
import PQPTable from './pqpTable';
import { loadPartnerList } from '../../../reducers/partners/partnerList';
import R from 'ramda';
import AddPQP from './addPQP';
import Title from '../../common/customTitle';

const messages = {
  title: "PQP",
  tableHeader: [
    {title: 'Partner', align: 'left', field: 'partner__legal_name'},
    {title: 'National Worldwide', align: 'left', field: 'national_worldwide'},
    {title: 'Granted Date', align: 'left', field: 'granted_date'},
    {title: 'Expiry Date', align: 'left', field: 'expiry_date'},
    {title: '', align: 'left'}
  ],
}

const PQP = props => {
  const { pqp, loadPqp, loading, totalCount, query, partnerList, loadPartnerList } = props

  const [params, setParams] = useState({page: 0}); 
  const [partner, setSelectedPartner] = useState([])
  const [items, setItems] = useState([]);
  const [key, setKey] = useState('default')
  
  var date= new Date()
  
  useEffect(() => {
    setItems(partnerList);
  }, [partnerList]);


  const [sort, setSort] = useState({
    field: messages.tableHeader[0].field, 
    order: 'desc'
  })

  useEffect(() => {
    loadPqp(query);
  }, [query]);

  function goTo(item) {
    history.push(item);
  }

  function handleChangePage(event, newPage) {
    setParams(R.merge(params, {
      page: newPage,
    }));
  }

  function handleSort(field, order) {
    const {pathName, query} = props;
    setSort({
      field: field,
      order: R.reject(R.equals(order), ['asc', 'desc'])[0]
    })
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order,
      }),
    })
  }

  function resetFilter() {
    const {pathName} = props;
    setSelectedPartner([])
    setKey(date.getTime())
    setParams({page: 0});
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  function handleChange(e,v) {
    if (v) {
      let params = {
        legal_name: v,
      }
      loadPartnerList(params);
    }
  }

  function handleSearch(values) {
    const {pathName, query} = props;
    const name = partner.legal_name
    setParams({page: 0});
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        partner: name
      }),
    })
  }

  return (
    <div style={{position: 'relative', paddingBottom: 16, marginBottom: 16}}>
      <Title show={false} title={messages.title} />
      <PQPFilter 
      onSubmit={handleSearch}
      items={items}
      key={key}
      partner={partner}
      handleChange={handleChange}
      setSelectedPartner={setSelectedPartner} 
      resetFilter={resetFilter} />
      <AddPQP />
      <PQPTable 
        messages={messages} 
        sort={sort} 
        items={pqp} 
        loading={loading}
        totalItems={totalCount}  
        page={params.page}
        handleChangePage={handleChangePage}
        handleSort={handleSort}
      />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  pqp: state.pqpList.pqp,
  totalCount: state.pqpList.totalCount,
  loading: state.pqpList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
  partnerList: state.partnerList.list,
});

const mapDispatch = dispatch => ({
  loadPqp: params => dispatch(loadPqpList(params)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
});

const connectedPQP = connect(mapStateToProps, mapDispatch)(PQP);
export default withRouter(connectedPQP);