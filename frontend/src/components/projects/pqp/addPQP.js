import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import { connect } from 'react-redux';
import AddPQPDialog from './addPQPDialog';
import { errorToBeAdded } from '../../../reducers/errorReducer';
import { reset } from 'redux-form';
import { withRouter } from 'react-router';
import { loadPartnerList } from '../../../reducers/partners/partnerList';
import { addNewPQP } from '../../../reducers/projects/pqpReducer/addPqpPartner';
import { loadPqpList } from '../../../reducers/projects/pqpReducer/pqpList';
import { SubmissionError } from 'redux-form';
import { showSuccessSnackbar } from '../../../reducers/successReducer';
import R from 'ramda';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    borderRadius: 6
  },
  customizedButton: {
    backgroundColor: '#0072BC',
    color: 'white',
    textTransform: 'capitalize',
    borderRadius: 5,
    height: 30,
    minWidth: 100,
    fontFamily: 'Open Sans',
    lineHeight: '12px',
    '&:hover': {
      backgroundColor: '#0072BC'
    }

  }
}));

const ADD_PQP = 'ADD_PQP';

const messages = {
  error: 'Error',
  ERROR: 'Please select valid partner from dropdown',
  success: 'PQP Successfully Added !',
  add: 'Add PQP'
}

function AddPQP(props) {
  const classes = useStyles();

  const {postError, loadPartnerList, partnerList, postPQP, successReducer, reset, loadPqp, query} = props;
  const [open, setOpen] = useState({ status: false, type: null });
  const [items, setItems] = useState([]);
  const [partner, setSelectedPartner] = useState([])

  
  useEffect(() => {
    setItems(partnerList);
  }, [partnerList]);

  function handleClose() {
    setOpen({ status: false, type: null });
  }

  function handleOpen(action) {
    setSelectedPartner([])
    reset();
    setOpen({ status: true, type: action });
  }

  function handleSubmit(values) {
    const {national_worldwide, granted_date, expiry_date} = values;
    let body;
    if(!partner || partner.length === 0){
      const error = new SubmissionError({ _error: messages.ERROR });
      postError(error, messages.ERROR);
      throw error;
    }
    if (partner) {
      body = {
        partner_id: partner.id,
        national_worldwide,
        granted_date,
        expiry_date
      }
    }
    return postPQP(body).then(() => {
      handleClose();
      loadPqp(query);
      successReducer(messages.success)
    }).catch((error) => {
      postError(error, messages.error);
      throw new SubmissionError({
        ...error.response.data,
        _error: messages.error,
      });
    });
  }

  function handleChange(e,v) {
    if (v) {
      let params = {
        legal_name: v,
      }
      loadPartnerList(params);
    }
  }

  return (
    <div className={classes.root}>      
    <span style={{marginLeft: -16}}> 
    <CustomizedButton buttonType={buttonType.submit} 
          buttonText={messages.add} 
          clicked={() => handleOpen(ADD_PQP)} />
      </span>
      <AddPQPDialog 
        open={open.status && open.type === ADD_PQP} 
        onSubmit={handleSubmit} 
        items={items}
        partner={partner}
        handleChange={handleChange}
        setSelectedPartner={setSelectedPartner}
        handleClose={handleClose} />
    </div>
  )
}


const mapStateToProps = (state, ownProps) => ({
  partnerList: state.partnerList.list,
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
});

const mapDispatch = dispatch => ({
  postPQP: body => dispatch(addNewPQP(body)),
  loadPartnerList: (params) => dispatch(loadPartnerList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'addPQP', message)),
  reset: () => dispatch(reset('addPQPPartner')),
  loadPqp: params => dispatch(loadPqpList(params)),
  successReducer: (message) => dispatch(showSuccessSnackbar(message))
});

const connected = connect(mapStateToProps, mapDispatch)(AddPQP);
export default withRouter(connected);