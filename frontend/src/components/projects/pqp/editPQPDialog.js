import React from 'react';
import CustomizedButton from '../../common/customizedButton';
import {buttonType} from '../../../helpers/constants';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { renderTextField, renderSelectField } from '../../common/renderFields';
import InputLabel from '@material-ui/core/InputLabel';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    display: 'flex',
    width: '100%'
  },
  button: {
    marginTop: theme.spacing(1),
  },
  select: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    width: '100%'
  },
  menu: {
    padding: 8,
    '&.Mui-selected': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    },
    '&:hover': {
      backgroundColor: '#E5F1F8',
      borderStyle: 'hidden'
    }
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    marginBottom: 2
    },
  label: {
    fontSize: '12px'
  },
  value: {
    fontSize: '16px',
    color: 'black'
  },
  buttonGrid: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing(2),
    width: '100%'
  },
}));

const validate = values => {
  const errors = {}
  const requiredFields = [
    'national_worldwide',
    'granted_date',
    'expiry_date'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  return errors
}

const messages = {
  submit: 'Submit',
  cancel: 'Cancel'
}

function EditDialog(props) {
  const {open, handleClose, handleSubmit, partner} = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Dialog
        open={open}
        onClose={handleClose}
        fullWidth
      >
        <DialogTitle >{"Edit PQP Partner"}</DialogTitle>
        <Divider />
        <DialogContent>
          <DialogContentText >
            <form className={classes.container} onSubmit={handleSubmit} >
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <div>
                  <p className={classes.label}>Partner</p>
                  <p className={classes.value}>{partner && partner.partner && partner.partner.legal_name}</p>
                </div>
                <InputLabel className={classes.inputLabel}>National Worldwide</InputLabel>
                <Field
                  name='national_worldwide'
                  label='National Worldwide'
                  margin='normal'
                  component={renderSelectField}
                  className={classes.select}
                >
                  <option value={'W'} className={classes.menu}>W</option>
                  <option value={'N'} className={classes.menu}>N</option>
                  <option value={'M'} className={classes.menu}>M</option>
                </Field>
                <InputLabel className={classes.inputLabel}>Granted Date</InputLabel>
                <Field 
                  name='granted_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                  className={classes}
                />
                <InputLabel className={classes.inputLabel}>Expiry Date</InputLabel>
                <Field 
                  name='expiry_date'
                  margin='normal'
                  type='date'
                  component={renderTextField}
                  className={classes}
                />
                <Grid className={classes.buttonGrid} >
                <CustomizedButton
                  clicked={handleClose}
                  buttonText={messages.cancel}
                  buttonType={buttonType.cancel} />
                <CustomizedButton
                  type='submit'
                  buttonText={messages.submit}
                  buttonType={buttonType.submit} />
              </Grid>
              </Grid>
            </form>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const editPQPForm = reduxForm({
  form: 'editPQPPartner',
  validate,
  enableReinitialize: true,
})(EditDialog);


const mapStateToProps = (state, ownProps) => {
  let initialValues = ownProps.partner
  return {
    initialValues
  }
};

const mapDispatchToProps = dispatch => ({
});

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(editPQPForm);

export default withRouter(connected);
