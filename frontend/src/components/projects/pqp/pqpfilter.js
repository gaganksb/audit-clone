import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form';
import {buttonType, filterButtons} from '../../../helpers/constants';
import CustomizedButton from '../../common/customizedButton';
import InputLabel from '@material-ui/core/InputLabel';
import CustomAutoCompleteField from '../../forms/TypeAheadFields';

const type_to_search = 'partner__legal_name';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
    paddingBottom: 6
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '45%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop: theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    margin: theme.spacing(1),
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  inputLabel: {
    fontSize: 14,
    color: '#344563',
    marginTop: 18,
    paddingLeft: 14
    },
    autoCompleteField: {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(1),
      width: '70%',
      marginTop: 2
  },
}));

function PQPFilter(props) {
  const classes = useStyles();
  const { handleSubmit, reset, handleChange, resetFilter, setSelectedPartner, items, partner, key } = props;

  const handleValueChange = val => {
    setSelectedPartner(val)
  }
  
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={handleSubmit}>
          <Grid item sm={10} >
            <InputLabel className={classes.inputLabel}>Partner</InputLabel>
            <Field
                name='partner'
                key={key}
                className={classes.autoCompleteField}
                component={CustomAutoCompleteField}
                handleValueChange={handleValueChange}
                onChange={handleChange}
                items={items}
                comingValue= {partner}
                type_to_search={type_to_search}
              />
          </Grid>
          <Grid item sm={2} className={classes.buttonGrid}>
          <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type="submit" />
                <CustomizedButton buttonType={buttonType.submit}                  
                  reset = {true}
                  clicked = {() => { reset(); resetFilter(); }}
                  buttonText={filterButtons.reset}
                />
          </Grid>
        </form>
      </Paper>
    </div>
  );
}

export default reduxForm({
  form: 'pqpFilter',
  enableReinitialize: true,
})(PQPFilter);