import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import withStyles from '@material-ui/core/styles/withStyles';
import LoginForm from './loginForm';
import { LOGO_LOGIN } from '../../helpers/constants';
import { HeadPhone } from '../../img/svgIcons';

const styles = theme => ({
  main: {
    display: 'flex',
    width: '100%', 
    height: 'calc(100vh - 64px)',
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3 * 2))]: {
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(3),
  },
  loginLeft: {
    width: 'inherit',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0072bc'
  },
  innerWrapper: {
    width: 500
  },
  innerTop: {
    borderBottom: 'solid 2px white',
    '& img': {
      width: '100%'
    }
  },
  innerBottom: {
    textAlign: 'center',
    fontSize: 21,
    color: 'white',
    '& h2': {
      fontFamily: 'Lato',
      margin: '18px 0'
    }
  },
  loginRight: {
    width: '35%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(6)}px`,
    backgroundColor: 'white'
  },
  loginWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 0
  },
  supportWrapper: {
    position: 'absolute',
    bottom: 20,
    fontSize: 14,
    textAlign: 'center',
    color: '#333333',
    '& > div': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontWeight: 'bold',
      color: '#0072bc',
      fontSize: 16,
      '& > p': {
        margin: 5
      }
    },
    '& > p': {
      margin: 10
    },
  }
});

function SignIn(props) {
  const { classes } = props;

  return (
    <div className={classes.main}>
      <CssBaseline />
      <div className={classes.loginLeft}>
        <div className={classes.innerWrapper}>
          <div className={classes.innerTop}>
            <div className={classes.logo}>
              <img src={LOGO_LOGIN} />
            </div>
          </div>
          <div className={classes.innerBottom}>
            <h2>Partnership Management Module</h2>
          </div>
        </div>
      </div>
      <div className={classes.loginRight}>
        <div className={classes.loginWrapper}>
          <LoginForm />
        </div>
        <div className={classes.supportWrapper}>
          <div>
            <HeadPhone color="#0072bc" width="27px" />
            <p>Support</p>
          </div>
          {/* <p>support@unicc.org</p>
          <p>+41123456789</p> */}
          <p>www.unicc.org</p>
        </div>
      </div>
    </div>
  );
}


export default withStyles(styles)(SignIn);