import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory as history } from 'react-router';
import { connect } from 'react-redux';
import { loadUserData, loginSuccess } from '../../reducers/session';
import Loader from '../common/loader';
import Card from '@material-ui/core/Card';

const messages = {
  title: 'UNHCR - Partnership Management Module',
};

class LoginToken extends Component {

  componentDidMount() {
    const { params, loadUserInfo, loggedIn } = this.props;
    window.localStorage.setItem('token', params.token);
    loggedIn(params.token);
    loadUserInfo().then(() => {
      history.push('/');
    }).catch(() => {
      window.localStorage.removeItem('token');
      history.push('/registration');
    });
  }

  render() {
    return (
    <Card title={messages.title}>
        <Loader loading />
    </Card>);
  }
}

LoginToken.propTypes = {
  loadUserInfo: PropTypes.func,
  loggedIn: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  loadUserInfo: () => dispatch(loadUserData()),
  loggedIn: token => dispatch(loginSuccess({ user: '', token })),
});

export default connect(
    null,
    mapDispatchToProps,
  )(LoginToken);