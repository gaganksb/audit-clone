import React from 'react';
import { reduxForm, SubmissionError, clearSubmitErrors } from 'redux-form';
import PropTypes from 'prop-types';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';
import FormControl from '@material-ui/core/FormControl';
import withStyles from '@material-ui/core/styles/withStyles';
import Snackbar from '@material-ui/core/Snackbar';
import { loginUser } from '../../reducers/session';
import TextFieldForm from '../forms/textFieldForm';
import PasswordFieldForm from '../forms/passwordFieldForm';


const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', 
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(3)}px`,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: '#0072BC',
  },
  form: {
    width: '100%', 
    marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: theme.spacing(3),
    backgroundColor: '#0072BC',
    textTransform: 'none'
  },
});


const handleLogin = (values, dispatch) => dispatch(loginUser(values)).catch((error) => {
  const errorMsg = error.response.data.non_field_errors || 'Login failed!';
  throw new SubmissionError({
    ...error.response.data,
    _error: errorMsg,
  });
});


const messages = {
  login: 'Login'
}
const SignIn = (props) => {
  const { handleSubmit, error, classes, dispatch, form } = props;
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <FormControl margin="normal" required fullWidth>
          <TextFieldForm
              label="email"
              placeholder="Please use your email to login"
              fieldName="email"
            />
        </FormControl>
        <FormControl margin="normal" required fullWidth>
          <PasswordFieldForm
            label="password"
            fieldName="password"
          />
        </FormControl>
        <CustomizedButton buttonType={buttonType.submit} 
        fullWidth={true}
        buttonText={messages.login} 
        clicked={handleSubmit} />
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={error}
          message={error}
          autoHideDuration={4e3}
          onClose={() => dispatch(clearSubmitErrors(form))}
        />
      </form>
    </div>
  );
};


SignIn.propTypes = {
  classes: PropTypes.object,
  /**
   * callback for form submit
   */
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool,
  submitting: PropTypes.bool,
  error: PropTypes.string,
  form: PropTypes.string,
  dispatch: PropTypes.func,
};

const SignInForm = reduxForm({
  form: 'login',
  onSubmit: handleLogin,
})(SignIn);

export default withStyles(styles, { name: 'SignInForm' })(SignInForm);
