import React from 'react';
import CustomizedButton from '../common/customizedButton';
import {buttonType} from '../../helpers/constants';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import { BACKGROUND_IMG } from '../../helpers/constants';

const styles = theme => ({
  azureLoginWrapper: {
    height: 'calc(100% - 64px)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundImage: `url(${BACKGROUND_IMG})`,
    backgroundRepeat  : 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },
  buttonGrid: {
    display: 'flex',
    width: '97%',
    marginTop: theme.spacing(6),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
},
  azureLoginInnerWrapper: {
    width: '39%',
    height: 230,
    borderRadius: 0
  },
  innerTop: {
    height: '40%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#0072BC',
    '& > p': {
      fontSize: 17,
      fontWeight: 500,
      color: 'white'
    }
  },
  innerBottom: {
    height: '60%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    '& > p': {
      fontSize: 17,
      fontWeight: 500,
    }
  }
})

const messages = {
  title: 'UNHCR - Partnership Management Module',
  login: 'Login via Active Directory',
  signIn: 'Sign in',
};

const AzureLogin = (props, context) => {
  const { classes } = props;
  return (
    <div className={classes.azureLoginWrapper}>
      <Card className={classes.azureLoginInnerWrapper}>
        <div className={classes.innerTop}>
          <Typography type="headline">{messages.title}</Typography>
        </div>
        <div className={classes.innerBottom}>
          <Typography type="headline">{messages.login}</Typography>
          <Grid className={classes.buttonGrid}>
          <CustomizedButton buttonType={buttonType.submit} 
          clicked={() => { window.open('/api/accounts/social-login/azuread-b2c-oauth2/', '_self'); }}
          buttonText={messages.signIn} 
          fullWidth= {true} />
          </Grid>
        </div>
      </Card>
    </div>
  )
};

export default withStyles(styles)(AzureLogin);
