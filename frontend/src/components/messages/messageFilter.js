import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import { Field, reduxForm } from 'redux-form'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import DayPickerInput from "react-day-picker/DayPickerInput"
import dateFnsFormat from "date-fns/format"
import CustomizedButton from '../common/customizedButton';
import {buttonType, filterButtons} from '../../helpers/constants';
import MomentLocaleUtils, { formatDate, parseDate } from 'react-day-picker/moment'
import "react-day-picker/lib/style.css"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  container: {
    display: 'flex',
  },
  textField: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(1),
    width: '45%',
  },
  buttonGrid: {
    margin: theme.spacing(2),
    marginTop : theme.spacing(3),
    display: 'flex',
    justifyContent: 'flex-end',
  },
  paper: {
    color: theme.palette.text.secondary,
  },
  fab: {
    margin: theme.spacing(1),
  },
  // radio: {
  //   marginLeft: theme.spacing(1),
  //   marginTop: 0,
  //   marginBottom: 0
  // },
  
  radio: {
    marginLeft: theme.spacing(2),
  },
  radioButton : {
  '&.Mui-checked': {
      color: '#0072BC'
    }
  },
  messageFilter: {
    marginLeft: theme.spacing(2),
    display: 'flex',
    alignItems: 'center',
    '& > div': {
      borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      '&:hover': {
        borderBottom: '1px solid rgba(0, 0, 0, 0.87)'
      },
      '&:first-child': {
        marginRight: theme.spacing(2),
      },
      '& > input': {
        border: 'none',
        outline: 'none',
        padding: '5px 0'
      }
    }
  }
}))

const messages = {
  all: {
    value: 'all',
    label: 'All'
  },
  read: {
    value: 'read',
    label: 'Read'
  },
  unread: {
    value: 'unread',
    label: 'Unread'
  }
}

const renderRadioField = ({
  margin,
  className,
  handleChange,
  checkSelection,
  input
}) => (
  <FormControl component="fieldset" className={className.radio} margin={margin}>
    <RadioGroup aria-label="position" onChange={handleChange} row {...input}>
      <FormControlLabel
        value={messages.all.value}
        control={<Radio className={className.radioButton} onClick={checkSelection} />}
        label={messages.all.label}
        labelPlacement="end"
      />
      <FormControlLabel
        value={messages.read.value}
        control={<Radio className={className.radioButton} onClick={checkSelection} />}
        label={messages.read.label}
        labelPlacement="end"
      />
      <FormControlLabel
        value={messages.unread.value}
        control={<Radio className={className.radioButton} onClick={checkSelection} />}
        label={messages.unread.label}
        labelPlacement="end"
      />
    </RadioGroup>
  </FormControl>
)

function MessageFilter(props) {
  const classes = useStyles()
  const { handleSubmit, reset, resetFilter } = props
  const [startDate, setStartDate] = React.useState()
  const [endDate, setEndDate] = React.useState()
  const FORMAT = 'YYYY/MM/DD'

  const checkSelection = event => {
    console.log(event.target.value)
  }

  const handleDateChange = date => {
    setSelectedDate(date)
  }

  const onSubmit = (e) => {
    e.preventDefault()
    handleSubmit({
      view_items: e.target.view_items.value,
      startDate: startDate && startDate.replace(/\//g, '-'),
      endDate: endDate && endDate.replace(/\//g, '-')
    })
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <form className={classes.container} onSubmit={onSubmit}>
          <Grid item sm={10} style={{ display: 'flex', alignItems: 'center' }} >
            <div className={classes.messageFilter}>
              <DayPickerInput
                id="day-picker-input1"
                className="invalid"
                value={startDate}
                formatDate={formatDate}
                parseDate={parseDate}
                localeUtils={MomentLocaleUtils}
                placeholder="FROM"
                onDayChange={(v) => {
                  if (!v) {
                    return
                  }
                  setStartDate(dateFnsFormat(v, FORMAT))
                }}
                inputProps={{
                  className: startDate ? '' : 'invalid',
                  name: "startDate"
                }}
              />
              <DayPickerInput
                id="day-picker-input2"
                name="endDate"
                className="invalid"
                value={endDate}
                formatDate={formatDate}
                parseDate={parseDate}
                localeUtils={MomentLocaleUtils}
                placeholder="TO"
                onDayChange={(v) => {
                  if (!v) {
                    return
                  }
                  setEndDate(dateFnsFormat(v, FORMAT))
                }}
                inputProps={{
                  className: endDate ? '' : 'invalid',
                  name: "endDate"
                }}
              />
              <input type="hidden" name="hello" value="fff" />
              <Field 
                name="view_items" 
                component={renderRadioField}
                checkSelection={checkSelection}
                className={classes}
                margin='normal'
              />
            </div>
          </Grid>
          <Grid item sm={2}>
          <div item className={classes.buttonGrid}>
                <CustomizedButton buttonType={buttonType.submit}
                  buttonText={filterButtons.search}
                  type = "submit" />
                <CustomizedButton buttonType={buttonType.submit}                  
                  reset = {true}
                  clicked = {() => { reset(); resetFilter(); }}
                  buttonText={filterButtons.reset}
                />
              </div>
          </Grid>
        </form>
      </Paper>
    </div>
  )
}

export default reduxForm({
  form: 'messageFilter',
  enableReinitialize: true,
})(MessageFilter)
