import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { browserHistory as history, withRouter } from 'react-router'
import R from 'ramda'
import { isQueryChanged } from '../../helpers/apiHelper'
import { loadMessageList } from '../../reducers/messages/messageList'
import MessageFilter from './messageFilter'
import MessageTable from './messageTable'

const texts = {
  tableHeader: [
    {title: '', align: 'left', width: '5%'},
    {title: 'Date', align: 'left', width: '20%', field: 'date'},
    {title: 'Short Description', align: 'left', width: '60%', field: 'description'}
  ],
  tableTitle: 'Notification Management',
}

class MessageContainer extends Component {
  constructor(props) {
    super(props)
    this.resetFilter = this.resetFilter.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleChangePage = this.handleChangePage.bind(this)
    this.handleSort = this.handleSort.bind(this)
    this.state = {
      page: 0,
      sort: {
        field: texts.tableHeader[0].field, 
        order: 'asc'
      }
    }
  }

  componentDidMount() {
    const { query } = this.props
    let initialQuery = R.merge(query, {
        field: texts.tableHeader[0].field, 
        order: 'asc'
      }
    )
    this.props.loadMessages(initialQuery)
  }

  shouldComponentUpdate(nextProps) {
    const { query } = this.props

    if (isQueryChanged(nextProps, query)) {
      this.props.loadMessages(nextProps.location.query)
      return false
    }

    return true
  }

  handleSearch(values) {
    const { pathName } = this.props
    const { startDate, endDate, view_items } = values
    const searchParams = { page: 1 }
    if (startDate) {
      searchParams.start = startDate
    }
    if (endDate) {
      searchParams.end = endDate
    }
    if (view_items && view_items !== 'all') {
      searchParams.did_read = view_items === 'read' ? true : false
    }
    
    history.push({
      pathname: pathName,
      query: searchParams
    })
  }

  handleSort(field, order) {
    const { pathName, query } = this.props

    this.setState({
      sort: {
        field: field, 
        order: R.reject(R.equals(order), ['asc', 'desc'])[0]}
    })

    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: 1,
        field,
        order,
      }),
    })
  }

  resetFilter() {
    const { pathName } = this.props

    this.setState({page: 0})
    history.push({
      pathname: pathName,
      query: {
        page: 1,
      },
    })
  }

  handleChangePage(event, newPage) {
    const { pathName, query } = this.props
    
    this.setState({page: newPage})
    history.push({
      pathname: pathName,
      query: R.merge(query, {
        page: newPage,
      }),
    })
  }

  render() {
    const { messages, totalCount, loading, location } = this.props

    return (
      <div>
        <MessageFilter handleSubmit={this.handleSearch} resetFilter={this.resetFilter} />
        <MessageTable
          loading={loading}
          data={messages}
          totalItems={totalCount}
          page={this.state.page}
          handleChangePage={this.handleChangePage}
          messages={texts}
          sort={this.state.sort}
          handleSort={this.handleSort}
          openMsg={location && location.state && location.state.openMsg}
        />
      </div>
    )
  }
}

MessageContainer.propTypes = {
  messages: PropTypes.array.isRequired,
  loadMessages: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  query: PropTypes.object,
}

const mapStateToProps = (state, ownProps) => ({
  messages: state.messageList.messages,
  totalCount: state.messageList.totalCount,
  loading: state.messageList.loading,
  query: ownProps.location.query,
  location: ownProps.location,
  pathName: ownProps.location.pathname,
})

const mapDispatch = dispatch => ({
  loadMessages: params => dispatch(loadMessageList(params)),
})

const connectedMessageContainer = connect(mapStateToProps, mapDispatch)(MessageContainer)
export default withRouter(connectedMessageContainer)