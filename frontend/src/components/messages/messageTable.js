import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import ListLoader from '../common/listLoader'
import TablePaginationActions from '../common/tableActions'
import MessageDetails from './messageDetails'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
    minHeight: 600
  },
  tableRow: {
    borderStyle: 'hidden',
  },
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableHeader: {
    color: '#344563',
    fontFamily: 'Roboto',
    fontSize: 14,
  },
  headerRow: {
    backgroundColor: '#F6F9FC',
    border: '1px solid #E9ECEF',
    height: 60,
    font: 'medium 12px/16px Roboto'
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  heading: {
    paddingLeft: 22,
    paddingTop: 19,
    borderBottom: '1px solid rgba(224, 224, 224, 1)',
    color: '#344563',
    font: 'bold 16px/21px Roboto',
    letterSpacing: 0,
    paddingBottom: 20
  },
}))

const rowsPerPage = 10

function MessageTable(props) {
  const {
    data,
    totalItems,
    loading,
    page,
    handleChangePage,
    handleSort,
    messages,
    sort,
    query,
    openMsg
  } = props
  
  const classes = useStyles()
  const [activeMsg, setActiveMsg] = React.useState(openMsg)

  React.useEffect(() => {
    setActiveMsg(openMsg)
  }, [openMsg])

  return (
    <Paper className={classes.root}>
      <ListLoader loading={loading}>
        <div className={classes.heading}>
          {messages.tableTitle}
        </div>
        <Table className={classes.table}>
          {/* <div className={classes.tableWrapper}> */}
            <TableHead className={classes.headerRow}>
              <TableRow>
                { messages.tableHeader.map((item, key) => 
                  <TableCell style={{width: item.width}} align={item.align} key={key} className={classes.tableHeader}>
                    {(item.field) ?
                    <TableSortLabel
                      active={item.field === sort.field}
                      direction={sort.order}
                      onClick={() => handleSort(item.field, sort.order)}
                    > 
                      {item.title}
                    </TableSortLabel> : 
                    <TableSortLabel active={false}> {item.title} </TableSortLabel>
                    }
                  </TableCell>
                  ) }
              </TableRow>
            </TableHead>
            {(data && !loading) &&
              <TableBody>
                {data.map((item, key) => (
                  <MessageDetails
                    key={key}
                    data={item}
                    expanded={item.notification && activeMsg === item.notification.id}
                    setActiveMsg={(id) => setActiveMsg(id)}
                  />
                ))}
              </TableBody>
            }
          {/* </div> */}
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[rowsPerPage]}
                colSpan={5}
                count={totalItems}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  native: true,
                }}
                onChangePage={handleChangePage}
                ActionsComponent={TablePaginationActions}
                style={{width:'10px'}}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </ListLoader>
    </Paper>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  pathName: ownProps.location.pathname,
})

const mapDispatchToProps = (dispatch) => ({
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MessageTable)

export default withRouter(connected)