import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ExpandLess from '@material-ui/icons/ExpandLess'
import { getMessage } from '../../reducers/messages/getMessage'
import { editMessage } from '../../reducers/messages/editMessage'
import { loadNewMessageList } from '../../reducers/messages/messageList'
import { loadMessageList } from '../../reducers/messages/messageList'
import { errorToBeAdded } from '../../reducers/errorReducer'

const useStyles = makeStyles(theme => ({
  fab: {
    margin: theme.spacing(1),
    borderRadius: 8
  },
  tableCell: {
    font: '14px/16px Roboto', 
    letterSpacing: 0, 
    color: '#344563'},
  icon: {
    '&:hover': {
      cursor: 'pointer'
    }
  },
  bold: {
    fontWeight: 700
  }
}))

function MessageDetails(props) {
  const {
    expanded,
    data,
    key,
    editMessage,
    loadNewMessages,
    postError,
    loadMessages,
    setActiveMsg
  } = props

  const [open, setOpen] = React.useState(expanded)
  const classes = useStyles()

  React.useEffect(() => {
    setOpen(expanded)
  }, [expanded])

  return (
    <React.Fragment>
      <TableRow key={key}>
        <TableCell className={classes.tableCell} component="th" scope="row">
          {(open) ? 
            <ExpandLess className={classes.icon} onClick={() => setOpen(false)} /> : 
            <ExpandMore
              className={classes.icon}
              onClick={() => {
                editMessage(data.notification.id, { did_read: true })
                .then(msg => {
                  setActiveMsg(data.notification.id)
                  loadMessages()
                  loadNewMessages({ did_read: false })
                  setOpen(true)
                })
                .catch(error => {
                  postError(error, "Load Message Error")
                })
              }}
            />
          }
        </TableCell>
        <TableCell className={`${classes.tableCell} ${data.did_read ? '': classes.bold}`} component="th" scope="row">
          {data.notification.last_modified_date}
        </TableCell>
        <TableCell className={`${classes.tableCell} ${data.did_read ? '': classes.bold}`}>{data.notification.name}</TableCell>
      </TableRow>
      {open && (
        <TableRow>
          <TableCell colSpan={4} className={classes.tableCell}>
            <div style={{marginTop: 10}}>
              <div dangerouslySetInnerHTML={{__html: data.notification.description}}></div>
            </div>
          </TableCell>
        </TableRow>
      )}
    </React.Fragment>
  )
}

const mapStateToProps = (state, ownProps) => ({
  query: ownProps.location.query,
  pathName: ownProps.location.pathname
})

const mapDispatchToProps = (dispatch) => ({
  getMessage: id => dispatch(getMessage(id)),
  editMessage: (id, body) => dispatch(editMessage(id, body)),
  loadMessages: (params) => dispatch(loadMessageList(params)),
  loadNewMessages: (params) => dispatch(loadNewMessageList(params)),
  postError: (error, message) => dispatch(errorToBeAdded(error, 'message', message))
})

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MessageDetails)

export default withRouter(connected)