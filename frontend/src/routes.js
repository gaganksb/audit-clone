
import React, { Suspense, lazy } from 'react';
import { Router, Route, browserHistory, IndexRedirect } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import store from './store';
import Loader from './components/common/loader';
import Survey from './components/projects/quality-assurance/single-project/kpi3/Survey';
const DataImport = lazy(() =>
  import("./components/projects/data-import/DataImportSections")
);

const QualityAssurance = lazy(() =>
  import("./components/projects/quality-assurance/QualityAssurance")
);
const TeamCompositionReportForm = lazy(() =>
  import(
    "./components/projects/quality-assurance/kpi4/report/TeamCompositionReport"
  )
);

const MemberProfileInfo = lazy(() =>
  import(
    "./components/projects/quality-assurance/single-project/MemberProfileInfo"
  )
);

const Kpi3Report = lazy(() =>
  import(
    "./components/projects/quality-assurance/kpi3/report/Kpi3Report"
  )
);

const Kpi2FinalReport = lazy(() =>
  import(
    "./components/projects/quality-assurance/kpi2/report/Kpi2FinalReport"
  )
);

const Kpi2SummaryReport = lazy(() =>
  import(
    "./components/projects/quality-assurance/kpi2/report/Kpi2SummaryReport"
  )
);

const Kpi2QualityTests = lazy(() =>
  import(
    "./components/projects/quality-assurance/single-project/kpi2/qaTests"
  )
);

const auth = lazy(() => import("./components/auth"));
const main = lazy(() => import("./components/main"));
const login = lazy(() => import("./components/login/login"));
const mainLayout = lazy(() => import("./components/layout/mainLayout"));
const nonAuthMainLayout = lazy(() =>
  import("./components/layout/nonAuthMainLayout")
);
const landing = lazy(() => import("./components/layout/landing"));
const entryPoint = lazy(() => import("./components/dashboard/entryPoint"));
const profile = lazy(() => import("./components/profile/profile"));
const usersHeader = lazy(() => import("./components/users/users"));
const users = lazy(() => import("./components/users/usersList"));
const messages = lazy(() => import("./components/messages/messageList"));
const roles = lazy(() => import("./components/users/roles"));
const projectsHeader = lazy(() => import("./components/projects/projects"));
const preliminary = lazy(() =>
  import("./components/projects/preliminary/preliminary")
);
const interimList = lazy(() => import("./components/projects/interim/index"));
const final = lazy(() => import("./components/projects/final/index"));
const projectDetails = lazy(() =>
  import("./components/projects/projectDetails")
);
const icq = lazy(() => import("./components/projects/icq/icq"));
const pqp = lazy(() => import("./components/projects/pqp/pqp"));
const managementReport = lazy(() =>
  import("./components/reports/management/managementReport")
);
const oios = lazy(() => import("./components/projects/oios/oios"));
const summaryStats = lazy(() =>
  import("./components/projects/summaryStats/summaryStats")
);
const summaryDashboard = lazy(() =>
  import("./components/projects/summaryStats/summaryDashboard")
);
const analysisStats = lazy(() =>
  import("./components/projects/summaryStats/analysisStats")
);
const partners = lazy(() => import("./components/partners/partner"));
const icqSummary = lazy(() => import("./components/icqSummary/index"));
const partnerDetails = lazy(() =>
  import("./components/partners/partnerDetails")
);
const fieldAssessment = lazy(() =>
  import("./components/assessment/fieldAssessment")
);
const auditFirms = lazy(() => import("./components/audit/auditFirms"));
const auditGroups = lazy(() => import("./components/audit/groups"));
const auditAssignment = lazy(() =>
  import("./components/audit/projectAssignment")
);
const auditFirmTabs = lazy(() =>
  import("./components/audit/auditFirms/auditFirmTabs")
);
const auditTabs = lazy(() =>
  import("./components/audit/projectAssignment/auditTabs")
);
const error = lazy(() => import("./components/errorPage"));

const buProgress = lazy(() => import("./components/assessment/buProgress"));

const auditReport = lazy(() =>
  import("./components/reports/audit/auditReport")
);

const nonAuth = lazy(() => import("./components/nonAuth"));
const page404 = lazy(() => import("./components/page404"));
const test = lazy(() => import("./components/testComponent"));

const azureLogin = lazy(() => import("./components/login/azureLogin"));
const loginToken = lazy(() => import("./components/login/loginToken"));
const ICQBulkUploadErrors = lazy(() =>
  import("./components/icqSummary/icqBulkUploadErrors")
);
const history = syncHistoryWithStore(browserHistory, store);

function WaitingComponent(Component) {
  return (props) => (
    <Suspense fallback={<Loader loading={true} fullscreen />}>
      <Component {...props} />
    </Suspense>
  );
}

const allRoutes = () => (
  <Router history={history}>
    <Route component={WaitingComponent(auth)}>
      <Route component={WaitingComponent(main)}>
        <Route path="/" component={WaitingComponent(mainLayout)}>
          <IndexRedirect to="dashboard" />
          <Route path="dashboard" component={WaitingComponent(entryPoint)} />
          <Route path="profile" component={WaitingComponent(profile)} />
          <Route
            path="fieldAssessment"
            component={WaitingComponent(fieldAssessment)}
          />
          <Route
            path="fieldProgress"
            component={WaitingComponent(buProgress)}
          />
          <Route path="partner" component={WaitingComponent(partners)} />
          <Route
            path="partners/:id"
            component={WaitingComponent(partnerDetails)}
          />
          <Route path="users" component={WaitingComponent(usersHeader)}>
            <IndexRedirect to="list" />
            <Route>
              <Route path="list" component={WaitingComponent(users)} />
            </Route>
          </Route>
          <Route path="projects" component={WaitingComponent(projectsHeader)}>
            <IndexRedirect to="preliminary" />
            <Route>
              <Route
                path="preliminary"
                component={WaitingComponent(preliminary)}
              />
              <Route path="interim" component={WaitingComponent(interimList)} />
              <Route path="final" component={WaitingComponent(final)} />
              <Route path="icq" component={WaitingComponent(icq)} />
              <Route path="pqp" component={WaitingComponent(pqp)} />
              <Route path="oios" component={WaitingComponent(oios)} />
              <Route
                path="summaryStats"
                component={WaitingComponent(summaryStats)}
              >
                <IndexRedirect to="summaryDashboard" />
                <Route
                  path="summaryDashboard"
                  component={WaitingComponent(summaryDashboard)}
                />
                <Route
                  path="analysisStats"
                  component={WaitingComponent(analysisStats)}
                />
              </Route>
            </Route>
          </Route>
          <Route
            path="projects/:id"
            component={WaitingComponent(projectDetails)}
          />
          <Route path="/icqSummary" component={WaitingComponent(icqSummary)} />
          <Route
            path="/icq-upload-erros"
            component={WaitingComponent(ICQBulkUploadErrors)}
          />
          <Route path="messages" component={WaitingComponent(messages)} />
          <Route path="auditFirms" component={WaitingComponent(auditFirmTabs)}>
            <IndexRedirect to="firms" />
            <Route path="firms" component={WaitingComponent(auditFirms)} />
            <Route path="groups" component={WaitingComponent(auditGroups)} />
            <Route
              path="assignments"
              component={WaitingComponent(auditAssignment)}
            />
          </Route>
          <Route
            path="quality-assurance"
            component={WaitingComponent(QualityAssurance)}
          />
          <Route
            path="quality-assurance/team-composition"
            component={WaitingComponent(TeamCompositionReportForm)}
          />
          <Route
            path="quality-assurance/team-composition/member-profile"
            component={WaitingComponent(MemberProfileInfo)}
          />

          <Route
            path="quality-assurance/kpi3-report"
            component={WaitingComponent(Kpi3Report)}
          />

          <Route
            path="quality-assurance/kpi3/survey/"
            component={WaitingComponent(Survey)}
          />

          <Route
            path="quality-assurance/kpi2-report/final/"
            component={WaitingComponent(Kpi2FinalReport)}
          />

          <Route
            path="quality-assurance/kpi2-report/summary/"
            component={WaitingComponent(Kpi2SummaryReport)}
          />

          <Route
            path="quality-assurance/kpi2/quality/"
            component={WaitingComponent(Kpi2QualityTests)}
          />

          <Route
            path="data-import"
            component={WaitingComponent(DataImport)}
          />

          <Route path="reports/:id" component={WaitingComponent(auditReport)} />
          <Route
            path="management-report/:year/:id"
            component={WaitingComponent(managementReport)}
          />
          <Route path="audit" component={WaitingComponent(auditTabs)}>
            <IndexRedirect to="auditManagement" />
            <Route
              path="auditManagement"
              component={WaitingComponent(auditAssignment)}
            />
          </Route>
          <Route path="test" component={WaitingComponent(test)} />
          <Route path="error" component={WaitingComponent(error)} />
        </Route>
      </Route>
    </Route>
    <Route component={WaitingComponent(nonAuth)}>
      <Route component={WaitingComponent(main)}>
        <Route path="/" component={WaitingComponent(nonAuthMainLayout)}>
          <Route path="/landing" component={WaitingComponent(landing)} />
          <Route path="/direct-login" component={WaitingComponent(login)} />
          <Route path="/login" component={WaitingComponent(azureLogin)} />
          <Route
            path="/login/:token"
            component={WaitingComponent(loginToken)}
          />
        </Route>
      </Route>
    </Route>
    <Route path="*" exact={true} component={WaitingComponent(page404)} />
  </Router>
);
export default allRoutes;