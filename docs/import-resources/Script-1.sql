select row_to_json(T) 
from (select id as pk, 'common.country' as model
,(select row_to_json(d) from (select code, name, region_id as region from common_country tt where tt.id=common_country.id) d) as fields
from common_country) T;

select row_to_json(T) 
from (select id as pk, 'common.businessunit' as model
,(select row_to_json(d) 
	from (
	select code, name 
	,(select array_to_json(array_agg(dd.country_id)) from (select country_id from common_businessunit_countries where businessunit_id=tt.id) dd ) as countries
	from common_businessunit 
	tt where tt.id=common_businessunit.id) 
d
) 
as fields
from common_businessunit) T;


select row_to_json(T) 
from (select id as pk, 'partner.partnertype' as model
,(select row_to_json(d) from (select partner_type from partner_partnertype tt where tt.id=partner_partnertype.id) d) as fields
from partner_partnertype) T;

select row_to_json(T) 
from (select id as pk, 'partner.implementertype' as model
,(select row_to_json(d) from (select implementer_type from partner_implementertype tt where tt.id=partner_implementertype.id) d) as fields
from partner_implementertype) T;

select row_to_json(T) 
from (select id as pk, 'project.pillar' as model
,(select row_to_json(d) from (select code, description from project_pillar tt where tt.id=project_pillar.id) d) as fields
from project_pillar) T;


select row_to_json(T) 
from (select id as pk, 'project.agreementtype' as model
,(select row_to_json(d) from (select description from project_agreementtype tt where tt.id=project_agreementtype.id) d) as fields
from project_agreementtype) T;

select row_to_json(T) 
from (select id as pk, 'project.planningpopulationgroup' as model
,(select row_to_json(d) from (select code from project_planningpopulationgroup tt where tt.id=project_planningpopulationgroup.id) d) as fields
from project_planningpopulationgroup) T;


select row_to_json(T) 
from (select id as pk, 'common.costcenter' as model
,(select row_to_json(d) from (select code from common_costcenter tt where tt.id=common_costcenter.id) d) as fields
from common_costcenter) T;


select row_to_json(T) 
from (select id as pk, 'project.accounttype' as model
,(select row_to_json(d) from (select description from project_accounttype tt where tt.id=project_accounttype.id) d) as fields
from project_accounttype) T;


select row_to_json(T) 
from (select id as pk, 'project.accounttype' as model
,(select row_to_json(d) from (select code, description, account_type_id as account_type from project_account tt where tt.id=project_account.id) d) as fields
from project_account) T;

select row_to_json(T) 
from (select id as pk, 'project.situation' as model
,(select row_to_json(d) from (select code from project_situation tt where tt.id=project_situation.id) d) as fields
from project_situation) T;


select row_to_json(T) 
from (select id as pk, 'partner.partner' as model
,(select row_to_json(d) from (select legal_name,number,implementer_type_id as implementer_type from partner_partner tt where tt.id=partner_partner.id) d) as fields
from partner_partner) T;


create table tmp_agreements(
	title varchar(250),
	agreement_date date,
	number varchar(50),
	operation varchar(25),
	start date,
	compl_end date,
	a_type varchar(250),
	bu varchar(50),
	country varchar(20),
	parn_num varchar(20),
	pillar varchar(1)
);



INSERT INTO public.project_agreement
(title, agreement_date, "number", operation, start_date, end_date, agreement_type_id, business_unit_id, country_id, partner_id, pillar_id)
select a.title,a.agreement_date,a."number",a.operation,a."start",a.compl_end,t.id as agreement_type 
,bu.id as business_unit, c.id as country_id,p.id as partner_id,pp.id
from tmp_agreements a
 join project_agreementtype t on t.description = a.a_type
 join common_businessunit bu on bu.code = a.bu
 join common_country c on c.code = a.country
 join partner_partner p on p."number" = a.parn_num 
 join project_pillar pp on pp.code = a.pillar

select row_to_json(T) 
from (select id as pk, 'project.agreement' as model
,(select row_to_json(d) from (select agreement_date,agreement_type_id as agreement_type,business_unit_id as business_unit,country_id as country,end_date,"number",operation,partner_id as partner,pillar_id as pillar,start_date,title from project_agreement tt where tt.id=project_agreement.id) d) as fields
from project_agreement) T;



