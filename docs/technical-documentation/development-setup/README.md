# Development Setup

## Local Setup

1. Install [Python 3](https://www.python.org/) for your OS.

1. Install [Docker](https://docs.docker.com/engine/installation/) for your OS. 

2. Also install Fabric3 via `pip3 install Fabric3`

2. In the project root directory, create an .env file with the reference of `.env.example` or receive .env file from your team member.

3. In the backend directory, create another .env file taking as reference the corresponding `.env.example`, and ask your team to provide the value of the environment variables set as TBD.

4. Run `docker-compose up` to first build and then run the project (it takes several minutes the first time you run the command).

4. Go to [http://127.0.0.1:8080/](http://127.0.0.1:8080/) to see the React frontend running. The Django app is running under

   [http://127.0.0.1:8080/api/](http://127.0.0.1:8080/api/)

5. Run `fab reset_db` to
   * reset the database
   * create a superuser with credentials admin@unhcr.org/Passw0rd!
   * populate models with fake data
   
6. Run `fab db_shell:audit_db_1` to start psql on the db container, then run the following commands
```
postgres=# create extension unaccent;
postgres=# \q
```
The first command will create the unaccent extension needed to perform accent unsensitive search on the db. The second command allows you to exit from the db container.

6. Go to Django Admin site at [http://127.0.0.1:8080/api/admin/](http://127.0.0.1:8080/api/admin/) and login using the superuser's credentials. You should be able to see the list of project's models, included the list of Users. 

7. Logout from the Django Admin site, go to [http://127.0.0.1:8080/direct-login](http://127.0.0.1:8080/direct-login) and login as a superuser.

## Frontend layout manipulation

In order to test new components' layout, it is possible to use the `/test` route, on which a simple component can be placed to obtain immediate feedback on its shape and look.

## Build & Test React App

In order to build the React application and test the production build locally on your device, the following steps are needed:

1. Open a Terminal (or Command Prompt) in the root of the project, and than run the command:
```
python3 deployment.py
Choose an environment:
1) local
2) uat
Default 1): 
```
By choosing 1, the frontend will be built and incorporated within the backend app.
Notice that, depending on your OS, it might be the case to use `python` instead of `python3`.

2. Once the script has completed its execution, start the project and than visit the link [http://127.0.0.1:8082/](http://127.0.0.1:8082/). Notice that the port is different from the previous one (i.e., 8082 instead of 8080). You can see here that the react production build has been incorporated inside the Django app so that now the latter is mainly responsible for rendering also the UI.


## Fabric commands

For convenience some [fabric](http://www.fabfile.org/) commands are included, such as getting into Django app container

```text
fab ssh:backend
```

For the most up-to-date reference just check `fabfile.py` in the repository root.

## Windows Issues

When running on Windows, be sure to setup Docker to run in Linux mode. Moreover, it can happens that `git` automatically restyled the EOL (End Of Line) of bash files. To avoid this problem, run this global command
```
git config --global core.autocrlf input
```

This command avoids the EOL (End Of Line) replacement. The latter is due to git which automatically reformat the file based on the current OS. As a conseguence, Linux-style EOL are replaced with Windows-style EOL.

## Debug the application

- In order to debug the backend code, do the following:
    - Download VS Code for your OS
    - Install the Python Extension for VS Code
    - In the `./backend/.env` file set `DJANGO_DEBUGGER=True`
    - Start the project
    - Open VS Code
    - Go to Debug and start the `Debug - Backend` configuration

    After that, the backend container will run the Django development server and it will be possible to set breakpoints within the application.

    Please notice that, if the `DJANGO_DEBUGGER=False` (or, however, not `True`, the application will start normally without giving the chance to debug it). Frontend developers should prefer to have the `DJANGO_DEBUGGER=False`.
- In order to debug the frontend code, do the following:
    - Install the [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=it) extension for Chrome (it is useful to check how the store change when user interact with the application)
    - Install the [React DevTools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) extension for Chrome (it is useful to inspect props and state of the various components)
    - Exploit the Chrome DevTools as described [here](https://developers.google.com/web/tools/chrome-devtools/javascript#scope)