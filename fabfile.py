from __future__ import unicode_literals
from fabric.api import local
from fabric.context_managers import shell_env

def ssh(service):
    """
    ssh into running service container
    :param service: ['backend', 'frontend', 'proxy', 'db', 'redis']
    """
    if service == 'audit_redis_1':
        local('docker exec -it %s sh' % service)
    elif service == 'audit_db_1':
        local('docker exec -it %s psql -U postgres' % service)
    else:
        assert service in ['backend', 'frontend', 'proxy', 'db', 'backend-jobs'], "%s is unrecognized service"
        local('docker-compose exec %s bash' % service)

def db_shell(service):
    """
    ssh into audit_db_1
    :param service: ['audit_db_1']
    """
    local('docker exec -it %s psql -U postgres' % service)

# docker-compose exec db psql -U postgres -d postgres -f /home/HNIP998/HNIP998.sql
def managepy(command=''):
    """
    Run specified manage.py command
    """
    cmd = 'docker-compose exec backend python manage.py {}'.format(command)
    local(cmd)

def fakedata(clean_before=True):
    """
    Create mock data for the django backend.
    """
    cmd = 'docker-compose exec backend python manage.py fakedata'
    if clean_before:
        cmd += ' --clean_before'
    local(cmd)

def test_import():
    """
    Reset db, migrate and generate fixtures.
    Useful when changing branch with different migrations.
    """
    # schema creation
    local('docker-compose exec backend python manage.py reset_db')
    local('docker-compose exec backend python manage.py makemigrations')
    local('docker-compose exec backend python manage.py migrate')    

    # import from xlsx files
    response = input('Do you want to generate CSV from XLSX [N/y]: ')
    if response == 'y':
        local('docker-compose exec backend python excel_data/import_data.py') # executed only to create csv file from xlsx
    local('docker-compose exec db psql -U postgres -d postgres -f /home/HNIP998/HNIP998.sql')
    print('HNIP998 Imported')
    local('docker-compose exec db psql -U postgres -d postgres -f /home/HNIP999/HNIP999.sql')
    print('HNIP999 Imported')
    local('docker-compose exec db psql -U postgres -d postgres -f /home/AllTest.sql')
    print('AllTest Imported')

def reset_db():
    """
    Reset db, migrate and generate fixtures.
    Useful when changing branch with different migrations.
    """
    # schema creation
    local('docker-compose exec backend python manage.py reset_db')
    local('docker-compose exec backend python manage.py makemigrations')
    local('docker-compose exec backend python manage.py migrate')
    # initial data
    local('docker-compose exec backend python manage.py final_data_import 2021')
    local('docker-compose exec backend python manage.py project_agreement_import --type all --year 2021')

def update_permissions():
    local('docker-compose exec backend python manage.py update_permissions')

def tests(test_path=''):
    """
    Run unit tests.
    """
    local('docker-compose exec backend python manage.py test {} --parallel --noinput'.format(test_path))

def remove_untagged_images():
    """
    Delete all untagged (<none>) images
    """
    local('docker rmi $(docker images | grep "^<none>" | awk "{print $3}")')

def lint():
    """
    Run python code linter
    """
    local('docker-compose exec backend flake8 ./ --count')

def clean_pyc():
    """
    Cleanup pyc files
    """
    local('docker-compose exec backend find . -name \'*.pyc\' -delete')

def clean_migrations():
    """
    Cleanup pyc files
    """
    local('docker-compose exec backend find . -path \'*/migrations/*.py\' -not -name \'__init__.py\' -delete')

def fake_migrations():
    """
    Cleanup pyc files
    """
    local('docker-compose exec backend find . -name \'*.pyc\' -delete')
    local('docker-compose exec backend find . -path \'*/migrations/*.py\' -not -name \'__init__.py\' -delete')
    local('docker-compose exec db psql -U postgres -d postgres -f /home/django_migrations.sql')

    local('docker-compose exec backend python manage.py makemigrations')
    local('docker-compose exec backend python manage.py migrate --fake contenttypes')
    local('docker-compose exec backend python manage.py migrate --fake')

def clean_migrations_reset_db():
    """
    Cleanup pyc files, migration files and reset db
    """
    local('docker-compose exec backend find . -name \'*.pyc\' -delete')
    local('docker-compose exec backend find . -path \'*/migrations/*.py\' -not -name \'__init__.py\' -delete')
    reset_db()

def compose():
    with shell_env(DOCKER_CLIENT_TIMEOUT='300', COMPOSE_HTTP_TIMEOUT='300'):
        local(
            'docker-compose stop '
            '&& '
            'docker-compose up --build --abort-on-container-exit --remove-orphans'
        )

def restart():
    with shell_env(DOCKER_CLIENT_TIMEOUT='300', COMPOSE_HTTP_TIMEOUT='300'):
        local(
            'docker-compose stop '
            '&& '
            'docker-compose up'
        )

def notebook():
    """
    Start a jupyter notebook
    """
    # schema creation
    local('docker-compose exec backend python manage.py shell_plus --notebook')

def run_test(test_name):

    local('docker-compose exec backend python manage.py test {}'.format(test_name))


def reset_db_manual():
    """
    Reset db, migrate and generate fixtures.
    Useful when changing branch with different migrations.
    """
    # schema creation

    local('docker-compose exec db psql -U postgres -d postgres -f /home/drop_db.sql')
    local('docker-compose exec db psql -U postgres postgres < /home/dev_db.backup')

def formatter():
    """
    Perform linter check and fixes
    """
    local('docker-compose exec backend black . ')
    # local('docker-compose exec frontend yarn lint --fix')