import os 
from os.path import isfile, join
import sys
from shutil import copy, copyfile
from distutils.dir_util import copy_tree

def main():
    os.system('python manage.py reset_db')
    os.system('python manage.py makemigrations')
    os.system('python manage.py migrate')
    os.system('python manage.py loaddata --app common initial.json')
    os.system('python manage.py loaddata --app auditing initial.json')
    os.system('python manage.py loaddata --app field initial.json')
    os.system('python manage.py loaddata --app partner initial.json')
    os.system('python manage.py loaddata --app project initial.json')
    os.system('python manage.py loaddata --app unhcr initial.json')
    os.system('python manage.py fakedata')

if __name__ == '__main__':
    main()