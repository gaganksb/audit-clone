from __future__ import absolute_import
import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

STATIC_URL = '/static/'

# Development Storage for CSS, JavaScript, Images (aka post-collectstatic)
STATIC_ROOT = os.path.join(
                BASE_DIR,
                'static_cdn', 
                'static'
             )

# Local Storage for CSS, JavaScript, Images (aka pre-collectstatic)
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'audit', 'staticfiles')] 

#URL ROOT for Serving Uploaded Media (ie ImageField or FileField)
MEDIA_URL = '/media/'

# Storage Location for Uploaded Media (live or local; ImageField and/or FileField)
MEDIA_ROOT = os.path.join(
                BASE_DIR,
                'static_cdn', 
                'media'
                )

