import os
from os.path import isfile, join
import sys
from shutil import copy, copyfile, rmtree
from distutils.dir_util import copy_tree
import fileinput


def main():

    print("Choose an environment:")
    print("1) local")
    print("2) uat")
    print("3) dev")
    print("4) prod")

    env = input("Default 1): ")
    if env not in ["", "1", "2", "3", "4"]:
        print("[ERROR] Invalid environment")
    else:
        os.system("docker-compose up -d")
        # build the production ui bundle
        os.system("docker-compose exec frontend yarn run build")

        root = os.path.dirname(os.path.realpath(sys.argv[0]))
        frontend_css = root + "/frontend/build/static/css/"
        frontend_js = root + "/frontend/build/api/static/js/"
        fixtures = root + "/backend/fixtures/"
        # msrp = root + '/backend/MSRP'

        # rename css files
        css_files = [
            f for f in os.listdir(frontend_css) if isfile(join(frontend_css, f))
        ]
        for filename in css_files:
            if filename.split(".")[-1] == "map":
                os.remove(frontend_css + filename)
            elif filename.split(".")[0] == "main":
                os.rename(frontend_css + filename, frontend_css + "react.css")

        # rename js files and correct the main js based on the environment
        js_files = [f for f in os.listdir(frontend_js) if isfile(join(frontend_js, f))]
        for filename in js_files:
            if filename.split(".")[-1] == "map":
                os.remove(frontend_js + filename)
            elif filename.split(".")[0] in ["main", "react"]:
                if env in ["2"]:
                    cdn = '"https://d3ks7tc21a4fkt.cloudfront.net/static/js/"'
                    wrong_location = 't.p+"api/static/js/"'
                    with open(frontend_js + filename, "r") as file:
                        filedata = file.read()
                        file.close()

                    new_text = filedata.replace(wrong_location, cdn)
                    with open(frontend_js + filename, "w") as file:
                        file.write(new_text)
                        file.close()
                if env in ["3"]:
                    cdn = '"https://d10wjolh60a7gg.cloudfront.net/static/js/"'
                    wrong_location = 't.p+"api/static/js/"'
                    with open(frontend_js + filename, "r") as file:
                        filedata = file.read()
                        file.close()

                    new_text = filedata.replace(wrong_location, cdn)
                    with open(frontend_js + filename, "w") as file:
                        file.write(new_text)
                        file.close()
                if env in ["4"]:
                    cdn = '"https://d2jxu34bn7af54.cloudfront.net/static/js/"'
                    wrong_location = 't.p+"api/static/js/"'
                    with open(frontend_js + filename, "r") as file:
                        filedata = file.read()
                        file.close()

                    new_text = filedata.replace(wrong_location, cdn)
                    with open(frontend_js + filename, "w") as file:
                        file.write(new_text)
                        file.close()
                os.rename(frontend_js + filename, frontend_js + "react.js")

        # copy css, js, media and icon to backend's staticfiles directory
        if env in ["", "1"]:
            backend_css = root + "/backend/audit/staticfiles/css/"
            backend_img = root + "/backend/audit/staticfiles/img/"
            backend_js = root + "/backend/audit/staticfiles/js/"
            backend_fixtures = root + "/environments/dev/fixtures"

        elif env == "2":
            backend_css = root + "/environments/uat/audit/staticfiles/css/"
            backend_img = root + "/environments/uat/audit/staticfiles/img/"
            backend_js = root + "/environments/uat/audit/staticfiles/js/"
            backend_fixtures = root + "/environments/dev/fixtures"

            library = root + "/backend/library/"
            uat_library = root + "/environments/uat/library/"
            rmtree(uat_library)
            os.makedirs(uat_library)
            copy_tree(library, uat_library)

            utils = root + "/backend/utils/"
            uat_utils = root + "/environments/uat/utils/"
            rmtree(uat_utils)
            os.makedirs(uat_utils)
            copy_tree(utils, uat_utils)

            audit = root + "/backend/audit/"
            uat_audit = root + "/environments/uat/audit/"
            rmtree(uat_audit)
            os.makedirs(uat_audit)
            copy_tree(audit, uat_audit)

            uat = root + "/environments/uat/"
            manage_py = root + "/backend/manage.py"
            requirements = root + "/backend/requirements.txt"
            copy(manage_py, uat)
            copy(requirements, uat)

        elif env == "3":
            backend_css = root + "/environments/dev/audit/staticfiles/css/"
            backend_img = root + "/environments/dev/audit/staticfiles/img/"
            backend_js = root + "/environments/dev/audit/staticfiles/js/"
            backend_fixtures = root + "/environments/dev/fixtures"

            library = root + "/backend/library/"
            dev_library = root + "/environments/dev/library/"
            rmtree(dev_library)
            os.makedirs(dev_library)
            copy_tree(library, dev_library)

            utils = root + "/backend/utils/"
            dev_utils = root + "/environments/dev/utils/"
            rmtree(dev_utils)
            os.makedirs(dev_utils)
            copy_tree(utils, dev_utils)

            audit = root + "/backend/audit/"
            dev_audit = root + "/environments/dev/audit/"
            rmtree(dev_audit)
            os.makedirs(dev_audit)
            copy_tree(audit, dev_audit)

            dev = root + "/environments/dev/"
            manage_py = root + "/backend/manage.py"
            requirements = root + "/backend/requirements.txt"
            copy(manage_py, dev)
            copy(requirements, dev)

        elif env == "4":
            backend_css = root + "/environments/prod/audit/staticfiles/css/"
            backend_img = root + "/environments/prod/audit/staticfiles/img/"
            backend_js = root + "/environments/prod/audit/staticfiles/js/"
            backend_fixtures = root + "/environments/dev/fixtures"

            library = root + "/backend/library/"
            prod_library = root + "/environments/prod/library/"
            rmtree(prod_library)
            os.makedirs(prod_library)
            copy_tree(library, prod_library)

            utils = root + "/backend/utils/"
            prod_utils = root + "/environments/prod/utils/"
            rmtree(prod_utils)
            os.makedirs(prod_utils)
            copy_tree(utils, prod_utils)

            audit = root + "/backend/audit/"
            prod_audit = root + "/environments/prod/audit/"
            rmtree(prod_audit)
            os.makedirs(prod_audit)
            copy_tree(audit, prod_audit)

            prod = root + "/environments/prod/"
            manage_py = root + "/backend/manage.py"
            requirements = root + "/backend/requirements.txt"
            copy(manage_py, prod)
            copy(requirements, prod)

        favicon = root + "/frontend/build/favicon.ico"

        rmtree(backend_css)
        rmtree(backend_img)
        rmtree(backend_js)
        rmtree(backend_fixtures)

        os.makedirs(backend_css)
        os.makedirs(backend_img)
        os.makedirs(backend_js)
        os.mkdir(backend_fixtures)

        copy_tree(frontend_css, backend_css)
        copy_tree(frontend_js, backend_js)
        copy_tree(fixtures, backend_fixtures)
        copy(favicon, backend_img)

        # collect static within the backend
        os.system(
            "docker-compose exec backend python manage.py collectstatic --no-input"
        )

        # stop the project
        os.system("docker-compose stop")


if __name__ == "__main__":
    main()
